#include "headers.h"
#include "ovr_filesound.h"
#include <wx_base/wx_string.h>

namespace ovr_fmod
{
	std::vector<OvrFileSound*> OvrFileSound::littlemusic_list;
	FMOD_RESULT F_CALLBACK OvrFileSound::mycallback(FMOD_CHANNELCONTROL *a_chancontrol, FMOD_CHANNELCONTROL_TYPE a_controltype, FMOD_CHANNELCONTROL_CALLBACK_TYPE a_callbacktype, void *a_commanddata1, void *a_commanddata2)
	{
		if (a_controltype == FMOD_CHANNELCONTROL_CHANNEL)
		{
			FMOD::Channel *channel = (FMOD::Channel *)a_chancontrol;
			// Channel specific functions here...
			if (a_callbacktype == FMOD_CHANNELCONTROL_CALLBACK_END)
			{
				std::vector<OvrFileSound*>::iterator it = OvrFileSound::littlemusic_list.begin();
				for (; it != OvrFileSound::littlemusic_list.end();)
				{
					if ((*it)->fmod_channel == channel)
					{
						(*it)->Uninit();
						delete (*it);
						it = OvrFileSound::littlemusic_list.erase(it);
						break;
					}
					else
						it++;
				}
			}
		}
		else
		{
			FMOD::ChannelGroup *group = (FMOD::ChannelGroup *)a_chancontrol;
		}

		return FMOD_OK;
	}
	bool OvrFileSound::Play(float a_vol)
	{
		while (nullptr == fmod_channel)
		{
			FMOD_RESULT result = fmod_system->playSound(fmod_sound, 0, 0, &fmod_channel);
			if (result == FMOD_OK)
			{
				fmod_channel->setCallback(mycallback);
				break;
			}
#ifdef _WIN32
			Sleep(20);
#else
			usleep(20*1000);
#endif
		}
		if (nullptr == fmod_channel)
		{
			return false;
		}

		fmod_channel->setPaused(false);
		fmod_channel->setVolume(a_vol);
		return true;
	}

	bool OvrFileSound::Init(FMOD::System* a_system,const char* a_filepath, bool a_bloop)
	{
		fmod_system = a_system;
        OvrString out_utf8;
        OvrString::GbkToUtf8(a_filepath, out_utf8);
		if (FMOD_OK != fmod_system->createSound(out_utf8.GetStringConst(), FMOD_DEFAULT, 0, &fmod_sound))
		{
			return false;
		}
		fmod_sound->setMode(a_bloop ? FMOD_LOOP_NORMAL : FMOD_LOOP_OFF);
		
		return true;
	}
	void OvrFileSound::Uninit()
	{
		if (nullptr != fmod_sound)
		{
			fmod_sound->release();
			fmod_sound = nullptr;
		}
		fmod_channel = nullptr;
		fmod_system = nullptr;
	}
	
	bool OvrFileSound::PlayMusic(FMOD::System* a_system, const char* a_filepath, bool a_bloop, float a_vol)
	{
		OvrFileSound* sound = new OvrFileSound();
		if (!sound->Init(a_system,a_filepath,a_bloop))
		{
			sound->Uninit();
			delete sound;
			sound = nullptr;
			return false;
		}
		OvrFileSound::littlemusic_list.push_back(sound);
		return sound->Play(a_vol);
	}
	void OvrFileSound::StopAll()
	{
		std::vector<OvrFileSound*>::iterator it = OvrFileSound::littlemusic_list.begin();
		for (; it != OvrFileSound::littlemusic_list.end();)
		{
			(*it)->Uninit();
			delete (*it);
			it = OvrFileSound::littlemusic_list.erase(it);
		}
	}

}