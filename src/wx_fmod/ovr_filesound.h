#ifndef OVR_AV_MEDIA_OVR_FILESOUND_H_
#define OVR_AV_MEDIA_OVR_FILESOUND_H_
#include "headers.h"
#include <wx_plugin/wx_i_sound.h>

namespace ovr_fmod
{
	class OvrFileSound
	{
	public:
		OvrFileSound() {}
		~OvrFileSound() {}
	private:
		bool Init(FMOD::System* a_system,const char* filePath, bool bLoop);
		void Uninit();
		bool Play(float a_vol);
	public:
		static bool PlayMusic(FMOD::System* a_system, const char* a_filepath, bool a_bloop, float a_vol);
		static void StopAll();
	private:
		FMOD::System*	fmod_system = nullptr;
		FMOD::Sound*	fmod_sound = nullptr;
		FMOD::Channel*	fmod_channel = nullptr;

		static FMOD_RESULT F_CALLBACK mycallback(FMOD_CHANNELCONTROL *a_chancontrol, FMOD_CHANNELCONTROL_TYPE a_controltype, FMOD_CHANNELCONTROL_CALLBACK_TYPE a_callbacktype, void *a_commanddata1, void *a_commanddata2);
	public:
		static std::vector<OvrFileSound*> littlemusic_list;

	};
}

#endif
