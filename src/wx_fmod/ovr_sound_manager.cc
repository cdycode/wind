﻿#include "headers.h"
#include "ovr_sound_manager.h"
#include "ovr_sound.h"
#include "ovr_filesound.h"
namespace ovr_fmod 
{
	bool OvrSoundManager::Init()
	{
		bool bSuccess = false;
		FMOD_RESULT result = FMOD_RESULT_FORCEINT;
		result = FMOD::System_Create(&fmod_system);
		if (result != FMOD_OK)
		{
			return false;
		}

		unsigned int      version =0;
		result = fmod_system->getVersion(&version);
		if (result != FMOD_OK)
		{
			return false;
		}

		if (version < FMOD_VERSION)
		{
			return false;
		}

		result = fmod_system->init(50, FMOD_INIT_NORMAL, nullptr);
		Set3DListener(0.0f, 0.0f, 0.0f);

		return (result == FMOD_OK);
	}

	void OvrSoundManager::Uninit()
	{
		if (nullptr != fmod_system)
		{
			fmod_system->close();
			fmod_system->release();
			fmod_system = NULL;
		}
	}

	void OvrSoundManager::Update() 
	{
		if (nullptr != fmod_system)
		{
			fmod_system->update();
		}
	}

	wx_plugin::wxISound* OvrSoundManager::CreateSound(int	a_channel_num, int a_sample_rate, int a_sample_bits, int a_frame_ms,bool b3D)
	{
		OvrSound* new_sound = new OvrSound;
		if (nullptr == new_sound)
		{
			return nullptr;
		}
		if (!new_sound->Init(fmod_system, a_channel_num, a_sample_rate, a_sample_bits, a_frame_ms,b3D)) 
		{
			new_sound->Uninit();
			delete new_sound;
			new_sound = nullptr;
		}
		return new_sound;
	}

	void OvrSoundManager::DestorySound(wx_plugin::wxISound*& a_sound)
	{
		if (nullptr != a_sound)
		{
			OvrSound* sound = (OvrSound*)a_sound;
			sound->Uninit();
			delete sound;
			sound = nullptr;
		}
	}
	void OvrSoundManager::Set3DListener(float x, float y, float z)
	{
		if (fmod_system != nullptr)
		{
			FMOD_VECTOR pos = { x,y,z };
			FMOD_VECTOR vec = { 0,0,0 };
			FMOD_VECTOR forward = { 0,0,1 };
			FMOD_VECTOR upward = { 0,1,0 };
			fmod_system->set3DListenerAttributes(0, &pos, &vec, &forward, &upward);
		}
	}
	
	bool	OvrSoundManager::PlayMusic(const char* fileName, bool bLoop, float a_vol)
	{
		return OvrFileSound::PlayMusic(fmod_system, fileName, bLoop, a_vol);
	}
	void	OvrSoundManager::StopAllMusic()
	{
		OvrFileSound::StopAll();
	}
}

static ovr_fmod::OvrSoundManager* sg_sound_manager = nullptr;
OVR_FMOD_API wx_plugin::wxIPlugin* StartPlugin()
{
	if (nullptr != sg_sound_manager)
	{
		return sg_sound_manager;
	}
	sg_sound_manager = new ovr_fmod::OvrSoundManager;
	return sg_sound_manager;
}

OVR_FMOD_API void StopPlugin()
{
	if (nullptr != sg_sound_manager)
	{
		delete sg_sound_manager;
		sg_sound_manager = nullptr;
	}
}