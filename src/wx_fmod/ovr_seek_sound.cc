#include "headers.h"
#include "ovr_seek_sound.h"

namespace ovr_fmod
{
OvrSeekSound::OvrSeekSound(void)
:mpSystem(NULL)
,mCurSoundID(0)
,mSoundMode(FMOD_OPENRAW | FMOD_OPENMEMORY)
,mBuffSize(0)
{
	for (int i = 0;i<FMOD_SEEK_SOUND_COUNTER;i++)
	{
		mpFmodSound[i] = NULL;
	}
}

OvrSeekSound::~OvrSeekSound(void)
{
	Uninit();
}

bool OvrSeekSound::Init(FMOD::System* a_system, int a_channel_num, int a_sample_rate, int a_sample_bits, int a_frame_ms)
{
	bool bSuccess = true;
	FMOD_CREATESOUNDEXINFO soundInfo;
	memset(&soundInfo,0,sizeof(FMOD_CREATESOUNDEXINFO));
	soundInfo.cbsize    = sizeof(FMOD_CREATESOUNDEXINFO);
	soundInfo.numchannels = a_channel_num;
	soundInfo.defaultfrequency = a_sample_rate;
	soundInfo.length	= (a_frame_ms*a_sample_rate /1000)*a_channel_num*(a_sample_bits/8);

	switch(a_sample_bits)
	{
	case 8:
		soundInfo.format = FMOD_SOUND_FORMAT_PCM8;
		break;
	case 16:
		soundInfo.format = FMOD_SOUND_FORMAT_PCM16;
		break;
	case 24:
		soundInfo.format = FMOD_SOUND_FORMAT_PCM24;
		break;
	case 32:
		soundInfo.format = FMOD_SOUND_FORMAT_PCM32;
		break;
	default:
		soundInfo.format = FMOD_SOUND_FORMAT_NONE;
		break;
	}

	mBuffSize			= soundInfo.length;
	for (int i = 0;bSuccess && i<FMOD_SEEK_SOUND_COUNTER;i++)
	{
		bSuccess = false;
		mpAudioBuff[i] = new char[mBuffSize];
		if (mpAudioBuff[i] && FMOD_OK == a_system->createSound(mpAudioBuff[i],mSoundMode,&soundInfo,&(mpFmodSound[i])))
		{
			mpFmodSound[i]->setMode(FMOD_LOOP_OFF);
			bSuccess = true;
		}
	}
	mpSystem = a_system;
	mCurSoundID = 0;
	return bSuccess;
}
void OvrSeekSound::Uninit(void)
{
	if (mpSystem)
	{
		mpSystem = NULL;

		for (int i = 0;i<FMOD_SEEK_SOUND_COUNTER;i++)
		{
			if (mpFmodSound[i])
			{
				mpFmodSound[i]->release();
				mpFmodSound[i] = NULL;
			}
			if (mpAudioBuff[i])
			{
				delete [](mpAudioBuff[i]);
				mpAudioBuff[i] = nullptr;
			}
		}
	}
	
}

bool OvrSeekSound::FeedSoundData(void* pBuff,int dataSize)
{
	bool bSuccess = false;

	unsigned char*	pAudioPtr1 = NULL;
	unsigned char*	pAudioPtr2 = NULL;
	unsigned	dwAudioBytes1 = 0;
	unsigned	dwAudioBytes2 = 0;
	mpSystem->update();
	if ( FMOD_OK == mpFmodSound[mCurSoundID]->lock( 0 , dataSize , (void**)&pAudioPtr1 , (void**)&pAudioPtr2 , &dwAudioBytes1, &dwAudioBytes2 ))
	{
		unsigned copyBytes = std::min<unsigned>( dwAudioBytes1,dataSize);
		if( pAudioPtr1)
		{
			memset(pAudioPtr1,0,dwAudioBytes1 );
			memcpy(pAudioPtr1,pBuff,copyBytes );    
		}
		if( pAudioPtr2 )
		{			 
			memset( pAudioPtr2,0,dwAudioBytes2 ); 
			if( copyBytes < (unsigned)dataSize)
			{
				copyBytes = std::min<unsigned>( dataSize-copyBytes,dwAudioBytes2);
				memcpy(pAudioPtr2,( (unsigned char*)(pBuff) + dwAudioBytes1 ),copyBytes );
			}	
		}
		FMOD_RESULT result = mpFmodSound[mCurSoundID]->unlock( pAudioPtr1 , pAudioPtr2 , dwAudioBytes1 ,dwAudioBytes2);
		
		FMOD::Channel*	fmodChannel = NULL;
		result = mpSystem->playSound(mpFmodSound[mCurSoundID] , 0, 0 , &fmodChannel);
		result = fmodChannel->setPosition(0,FMOD_TIMEUNIT_PCMBYTES);
		result = fmodChannel->setPaused(false);
		bSuccess = true;
	}
	
	mCurSoundID = (mCurSoundID+1)%FMOD_SEEK_SOUND_COUNTER;
	return bSuccess;
}

}
