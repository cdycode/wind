﻿#ifndef OVR_AV_MEDIA_OVR_SOUND_H_
#define OVR_AV_MEDIA_OVR_SOUND_H_

#include <wx_plugin/wx_i_sound.h>

namespace ovr_fmod 
{
	class OvrSeekSound;
	class OvrSound : public wx_plugin::wxISound 
	{
	public:
		OvrSound() {}
		virtual ~OvrSound() {}

		bool	Init(FMOD::System* a_system, int a_channel_num, int a_sample_rate, int a_sample_bits, int a_frame_ms,bool b3D = false);
		void    Uninit();

		virtual void SetFillAudioDataFunc(void* a_param, wx_plugin::FillAudioData a_function) override;

		virtual bool Play() override;
		virtual void Pause() override;
		virtual bool FeedSoundData(void* a_data, int a_data_size) override;
		virtual void SetSoundPos(float x, float y, float z) override;
		virtual void SetVolume(float a_vol) override;
		virtual bool IsPlaying() override;
	private:
		static FMOD_RESULT F_CALLBACK PcmReadCallback(FMOD_SOUND *a_sound, void *a_data, unsigned int a_data_len);
		static FMOD_RESULT F_CALLBACK PcmSetPosCallback(FMOD_SOUND *a_sound, int a_sub_sound, unsigned int a_position, FMOD_TIMEUNIT a_postype);

	private:
		FMOD::System*	fmod_system = nullptr;
		FMOD::Sound*	fmod_sound = nullptr;
		FMOD::Channel*	fmod_channel = nullptr;

		wx_plugin::FillAudioData	fill_audio_data_func = nullptr;
		void*			function_param = nullptr;

		bool			b3DSound = false;
		//seek sound
		bool				is_seek = true;
		OvrSeekSound*		seek_sound = nullptr;
	};
}

#endif
