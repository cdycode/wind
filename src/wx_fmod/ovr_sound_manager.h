﻿#ifndef OVR_FMOD_OVR_SOUND_MANAGER_H_
#define OVR_FMOD_OVR_SOUND_MANAGER_H_

#ifdef _WIN32
#	ifdef WXFMOD_EXPORTS
#		define OVR_FMOD_API __declspec(dllexport)
#	else
#		define OVR_FMOD_API __declspec(dllimport)
#	endif
#else
#	define OVR_FMOD_API  
#endif

#include <wx_plugin/wx_i_sound.h>

namespace ovr_fmod 
{
	class OvrSoundManager : public wx_plugin::wxISoundManager
	{
	public:
		OvrSoundManager() { display_name = "fmod"; }
		virtual ~OvrSoundManager() {}

		virtual	bool				Init() override;
		virtual void				Uninit() override;

	public:

		virtual void	Update()override;

		virtual wx_plugin::wxISound*	CreateSound(int	a_channel_num, int a_sample_rate, int a_sample_bits, int a_frame_ms,bool b3D = false) override;
		virtual void		DestorySound(wx_plugin::wxISound*& a_sound) override;
		virtual void		Set3DListener(float x, float y, float z) override;

		//直接播放声音接口
		virtual	bool		PlayMusic(const char* fileName, bool bLoop, float a_vol) override;
		virtual void		StopAllMusic() override;
	private:
		FMOD::System*		fmod_system;
	};
}

#if defined(__cplusplus)
extern "C"
{
#endif
	OVR_FMOD_API  wx_plugin::wxIPlugin* StartPlugin();
	OVR_FMOD_API  void StopPlugin();
#if defined(__cplusplus)
}
#endif

#endif
