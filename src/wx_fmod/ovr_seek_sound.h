#ifndef OVR_FMOD_OVR_SEEK_SOUND_H_
#define OVR_FMOD_OVR_SEEK_SOUND_H_

#define FMOD_SEEK_SOUND_COUNTER 2

namespace ovr_fmod 
{

class OvrSeekSound
{
public:
	OvrSeekSound(void);
	~OvrSeekSound(void);

	bool	Init(FMOD::System* a_system, int a_channel_num, int a_sample_rate, int a_sample_bits, int a_frame_ms);
	void	Uninit();

	bool	FeedSoundData(void* pBuff,int dataSize);

private:
	FMOD::System*	mpSystem;
	FMOD::Sound*	mpFmodSound[FMOD_SEEK_SOUND_COUNTER];
	int				mCurSoundID;
	FMOD_MODE		mSoundMode;

	int							mBuffSize;
	char*						mpAudioBuff[FMOD_SEEK_SOUND_COUNTER];
};

}
#endif
