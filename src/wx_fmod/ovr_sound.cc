﻿#include "headers.h"
#include "ovr_sound.h"
#include "ovr_seek_sound.h"

namespace ovr_fmod 
{
	bool OvrSound::Init(FMOD::System* a_system, int a_channel_num, int a_sample_rate, int a_sample_bits, int a_frame_ms, bool b3D)
	{
		Uninit();

		fmod_system = a_system;

		FMOD_CREATESOUNDEXINFO soundInfo;
		memset(&soundInfo, 0, sizeof(FMOD_CREATESOUNDEXINFO));
		soundInfo.cbsize = sizeof(FMOD_CREATESOUNDEXINFO);
		soundInfo.decodebuffersize = a_sample_rate * a_frame_ms / 1000 ;	//每次回调 填充多少个采样
		soundInfo.length = soundInfo.decodebuffersize*a_channel_num* (a_sample_bits / 8) * 3; //三个buffer
		soundInfo.numchannels = a_channel_num;
		soundInfo.defaultfrequency = a_sample_rate;
		soundInfo.pcmreadcallback = &OvrSound::PcmReadCallback;
		soundInfo.pcmsetposcallback = &OvrSound::PcmSetPosCallback;
		soundInfo.userdata = this;
		b3DSound = b3D;
		switch (a_sample_bits)
		{
		case 8:
			soundInfo.format = FMOD_SOUND_FORMAT_PCM8;
			break;
		case 16:
			soundInfo.format = FMOD_SOUND_FORMAT_PCM16;
			break;
		case 24:
			soundInfo.format = FMOD_SOUND_FORMAT_PCM24;
			break;
		case 32:
			soundInfo.format = FMOD_SOUND_FORMAT_PCM32;
			break;
		default:
			soundInfo.format = FMOD_SOUND_FORMAT_NONE;
			break;
		}

		if (soundInfo.format == FMOD_SOUND_FORMAT_NONE)
		{
			OvrLogE("ovr_fmod", "OvrSound::Init sound format is error");
			return false;
		}
		FMOD_MODE mode = FMOD_OPENUSER | FMOD_LOOP_NORMAL | FMOD_CREATESTREAM | FMOD_NONBLOCKING;
		if (b3DSound)
		{
			mode |= FMOD_3D;
			if (FMOD_OK != fmod_system->set3DSettings(1.0, 1.0, 1.0f))
				return false;
		}
		if (FMOD_OK != a_system->createSound(NULL, mode, &soundInfo, &fmod_sound)) 
		{
			OvrLogE("ovr_fmod", "OvrSound::Init createSound failed");
			return false;
		}
		if (b3DSound)
		{
			fmod_sound->set3DMinMaxDistance(0.1f, 1000.0f);
		}
		seek_sound = new OvrSeekSound;
		if (nullptr == seek_sound)
		{
			return false;
		}
		if (!seek_sound->Init(a_system, a_channel_num, a_sample_rate, a_sample_bits, a_frame_ms))
		{
			OvrLogE("ovr_fmod", "OvrSound::Init seek_sound::init failed");
			return false;
		}
		
		return true;
	}

	void OvrSound::Uninit() 
	{
		if (nullptr != fmod_sound)
		{
			fmod_sound->release();
			fmod_sound = nullptr;
		}
		fmod_channel = nullptr;
		fmod_system = nullptr;

		if (nullptr != seek_sound)
		{
			seek_sound->Uninit();
			delete seek_sound;
			seek_sound = nullptr;
		}
	}

	void OvrSound::SetFillAudioDataFunc(void* a_param, wx_plugin::FillAudioData a_function)
	{
		function_param = a_param;
		fill_audio_data_func = a_function;
	}

	bool OvrSound::Play() 
	{
		while (nullptr == fmod_channel)
		{
			FMOD_RESULT result = fmod_system->playSound(fmod_sound, 0, 0, &fmod_channel);
			if (result == FMOD_OK)
			{
				break;
			}
#ifdef _WIN32
			Sleep(20);
#else
            usleep(20*1000);
#endif
		} 
		if (nullptr == fmod_channel) 
		{
			return false;
		}

		bool is_paused = false;
		fmod_channel->getPaused(&is_paused);
		if (is_paused)
		{
			if (is_seek)
			{
				fmod_channel->setPosition(0, FMOD_TIMEUNIT_PCMBYTES);
			}
			fmod_channel->setPaused(!is_paused);
		}

		is_seek = false;
		return true;
	}

	void OvrSound::Pause() 
	{
		bool is_paused = false;
		fmod_channel->getPaused(&is_paused);
		if (!is_paused)
		{
			fmod_channel->setPaused(!is_paused);
		}
	}

	bool OvrSound::FeedSoundData(void* a_data, int a_data_size) 
	{
		Pause();
		seek_sound->FeedSoundData(a_data, a_data_size);
		is_seek = true;
		return true;
	}

	FMOD_RESULT OvrSound::PcmReadCallback(FMOD_SOUND* a_sound, void *a_data, unsigned int a_data_len)
	{
		OvrSound* this_sound = nullptr;
		((FMOD::Sound*)a_sound)->getUserData((void**)&this_sound);
		
		if (nullptr != this_sound && nullptr != this_sound->fill_audio_data_func)
		{
			this_sound->fill_audio_data_func(this_sound->function_param, a_data, a_data_len);
		}
		else 
		{
			memset(a_data, 0, a_data_len);
		}
	
		return FMOD_OK;
	}
	FMOD_RESULT OvrSound::PcmSetPosCallback(FMOD_SOUND *a_sound, int a_sub_sound, unsigned int a_position, FMOD_TIMEUNIT a_postype)
	{
		return FMOD_OK;
	}
	void OvrSound::SetSoundPos(float x, float y, float z)
	{
		if (b3DSound)
		{
			FMOD_VECTOR pos = { x,y,z };
			FMOD_VECTOR vec = { 0,0,0 };
			if (fmod_channel != nullptr)
			{
				fmod_channel->set3DAttributes(&pos, &vec);
			}
		}
	}
	bool OvrSound::IsPlaying()
	{
		if (nullptr == fmod_channel)
		{
			return false;
		}

		bool is_playing = false;
		fmod_channel->isPlaying(&is_playing);
		return is_playing;
	}
	void OvrSound::SetVolume(float a_vol)
	{
		if (NULL == fmod_channel)
			return;
		fmod_channel->setVolume(a_vol);
	}
}
