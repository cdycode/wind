﻿#pragma once
//Windows 定义
#include <map> 
#include <vector>
#include <list>
#include <deque>
#include <algorithm>
#include <string.h>

#ifdef _WIN32
    #include <windows.h>
#else
    #include <unistd.h>
#endif

//Fmod
#include <fmod.hpp>

#include <wx_base/wx_log.h>

