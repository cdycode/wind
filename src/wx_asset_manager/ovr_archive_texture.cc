﻿#include "headers.h"
#include <stdlib.h>
#include "wx_asset_manager/ovr_archive_texture.h"
#include "wx_asset_manager/ovr_archive_asset_manager.h"
namespace ovr_asset 
{
	OvrArchiveTexture::OvrArchiveTexture()
		: pixel_format(kPixelInvalid)
		, image_width(0)
		, image_height(0)
		, data_size(0)
		, buffer(nullptr)
		, buffer_length(0)
	{
	}
	OvrArchiveTexture::~OvrArchiveTexture()
	{
		ReleaseBuffer();
	}

	void OvrArchiveTexture::SetTextureInfo(OvrPixelFormat a_format, ovr::uint32 a_width, ovr::uint32 a_height)
	{
		pixel_format = a_format;
		image_width = a_width;
		image_height = a_height;
	}

#ifdef _MSC_VER
	bool OvrArchiveTexture::SetImagePixels(const char* a_buffer, ovr::uint32 a_image_size)
	{
		if (nullptr == a_buffer || a_image_size == 0)
		{
			return false;
		}

		ReleaseBuffer();

		KTX_image_info image_info;
		image_info.data = (GLubyte *)a_buffer;
		image_info.size = a_image_size;

		KTX_texture_info ktx_info;
		ktx_info.glType = GL_UNSIGNED_BYTE;
		ktx_info.glTypeSize = 1;
		ktx_info.pixelWidth = image_width;
		ktx_info.pixelHeight = image_height;
		ktx_info.pixelDepth = 0;
		ktx_info.numberOfFaces = 1;
		ktx_info.numberOfArrayElements = 0;
		ktx_info.numberOfMipmapLevels = 1;
		switch (pixel_format)
		{
		case kPixelInvalid:
			break;
		case kPixelAlpha:
			ktx_info.glFormat = ktx_info.glBaseInternalFormat = GL_ALPHA;
			ktx_info.glInternalFormat = GL_ALPHA;
			break;
		case kPixelLuminance:
			ktx_info.glFormat = ktx_info.glBaseInternalFormat = GL_LUMINANCE;
			ktx_info.glInternalFormat = GL_LUMINANCE;
			break;
		case kPixelRGB:
			ktx_info.glFormat = ktx_info.glBaseInternalFormat = GL_RGB;
			ktx_info.glInternalFormat = GL_RGB;
			break;
		case kPixelRGBA:
			ktx_info.glFormat = ktx_info.glBaseInternalFormat = GL_RGBA;
			ktx_info.glInternalFormat = GL_RGBA;
			break;
		default:
			break;
		}

		GLsizei out_size = 0;
		KTX_error_code code = ktxWriteKTXM(&buffer, &out_size, &ktx_info, 0, nullptr, 1, &image_info);
		if (KTX_SUCCESS != code)
		{
			const char* error_str = ktxErrorString(code);
			printf("%s", error_str);
			ReleaseBuffer();
			return false;
		}
		data_size = out_size;
		return true;
	}
	bool OvrArchiveTexture::SetImageKtxPixels(const char* a_buffer, ovr::uint32 a_image_size)
	{
		buffer = (ovr::uint8*)a_buffer;
		data_size = a_image_size;
		return true;
	}
#endif
	const ovr::uint8* OvrArchiveTexture::GetRGBABuffer()
	{
		if (rgba_buffer[0] != NULL)
			return rgba_buffer[0];
		GLuint t_tex = 0;
		GLenum t_target = 0;
		KTX_dimensions  t_dim;
		GLboolean t_bool;
		GLenum t_error;
		uint32_t t_len;
		unsigned char* t_kvd;
		
		int bytes_per_pixel = 0;

		//创建文件
		
		KTX_error_code t_ode2 = ktx2RGB(buffer, data_size, &t_target, &bytes_per_pixel, &t_dim, &t_bool, &t_error, &t_len, &t_kvd, rgba_buffer);
		
		if (pixel_format == kPixelRGB)
			bytes_per_pixel = 3;
		else if (pixel_format == kPixelRGBA)
			bytes_per_pixel = 4;
		int stride = image_width* bytes_per_pixel;

		if (KTX_SUCCESS != t_ode2)
		{
			//失败，表示buffer使用的不是压缩数据，buffer是rgb数据；
			if (rgba_buffer[0] == NULL)
			{
				rgba_buffer[0] = new unsigned char[image_width*image_height*bytes_per_pixel];
			}
			ovr::uint8* bufferrgb = buffer + 68;
			for (int i = 0; i < image_height; ++i)
			{
				int tpos_now = i*stride;
				for (int j = 0; j < image_width; ++j)
				{
					int ti = tpos_now + j*bytes_per_pixel;
					rgba_buffer[0][ti] = bufferrgb[ti];
					rgba_buffer[0][ti + 1] = bufferrgb[ti + 1];
					rgba_buffer[0][ti + 2] = bufferrgb[ti+2];

					if(bytes_per_pixel == 4)
						rgba_buffer[0][ti + 3] = bufferrgb[ti + 3];
				}
			}
		}
		else
		{
			if (rgba_buffer[0] == NULL)
				return NULL;
			//成功，表示buffer是压缩数据，rgba_buffer是bgr，仍需换成rgb
			for (int i = 0; i < image_height; ++i)
			{
				int tpos_now = i*stride;
				for (int j = 0; j < image_width; ++j)
				{
					int ti = tpos_now + j*bytes_per_pixel;
					ovr::uint8 temp = rgba_buffer[0][ti];
					rgba_buffer[0][ti] = rgba_buffer[0][ti + 2];
					rgba_buffer[0][ti + 2] = temp;
				}
			}
		}
		return rgba_buffer[0];
	}
	void OvrArchiveTexture::ReleaseBuffer()
	{
		if (nullptr != buffer)
		{
			//ktx library中使用malloc分配 因此这里也采用这种方式释放
			free(buffer);
			buffer = nullptr;

			for (int i = 0; i < 6;i++)
			{
				if (rgba_buffer[i] != NULL)
					delete rgba_buffer[i];
			}
		}
	}

	bool OvrArchiveTexture::Serialize(OvrArchive& a_serialize)
	{
		if (!a_serialize.Serialize(pixel_format))
		{
			return false;
		}
		if (!a_serialize.Serialize(image_width))
		{
			return false;
		}
		if (!a_serialize.Serialize(image_height))
		{
			return false;
		}

		if (!a_serialize.Serialize(data_size))
		{
			return false;
		}
		if (OvrArchive::kStore == a_serialize.GetType())
		{
			if (!a_serialize.SerializeBuffer(buffer, data_size, data_size))
			{
				return false;
			}
		}
		else
		{
			buffer = (ovr::uint8*)malloc(data_size);
			if (!a_serialize.SerializeBuffer(buffer, data_size, data_size))
			{
				return false;
			}
			buffer_length = data_size;
		}
		return true;
	}
	bool OvrArchiveTexture::RgbaToKtxOne(unsigned char *img, int with_alpha, int width, int height, unsigned char*&dst_buf, int* dst_len, int expandedwidth, int expandedheight)
	{
		if (!img)
		{
			return false;
		}
		int mode = MODE_COMPRESS;
		int speed = SPEED_FAST;
		int metric = METRIC_NONPERCEPTUAL;
		int codec = CODEC_ETC2;
		int format = ETC2PACKAGE_RGB_NO_MIPMAPS;
		int bytes_per_pixel = 3;
		unsigned char* newimg = img;
		unsigned char* alphaimg = NULL;
		if (with_alpha)
		{
			bytes_per_pixel = 4;
			format = ETC2PACKAGE_RGBA_NO_MIPMAPS;
		}
		int t_cur_pos = 0;
		if (with_alpha)
		{
			newimg = new unsigned char[width*height * 3];
			alphaimg = new unsigned char[width*height];
			for (int i = 0; i < height; ++i)
			{
				for (int j = 0; j < width; ++j)
				{
					int tposnow = t_cur_pos * 3 + 3 * j;
					int t0 = (t_cur_pos + j)*bytes_per_pixel;
					newimg[tposnow] = img[t0 + 0];
					newimg[tposnow + 1] = img[t0 + 1];
					newimg[tposnow + 2] = img[t0 + 2];
					alphaimg[t_cur_pos + j] = img[t0 + 3];
				}
				t_cur_pos += width;
			}
		}
		int dst_buf_len = getKtxHeaderSize();
		int tw = width;
		int th = height;
		int tmiplevel = 0;
		while (tw % 4 == 0 && th % 4 == 0)
		{
			tw = tw >> 1;
			th = th >> 1;
			tmiplevel++;
		}
		for (int i = 0; i < tmiplevel; ++i)
		{
			dst_buf_len += getDstbufSize(width >> i, height >> i, format);
		}
		dst_buf = new unsigned char[dst_buf_len];
		compressImagebuf2fileM(newimg, alphaimg, width, height, dst_buf, dst_len, expandedwidth, expandedheight, mode, speed, metric, codec, format, 1, tmiplevel);
		if (with_alpha)
		{
			delete[] newimg;
			delete[] alphaimg;
		}
		return true;
	}
}