﻿#ifndef OVR_ASSET_SERIALIZE_OVR_ASSET_HEADER_H_
#define OVR_ASSET_SERIALIZE_OVR_ASSET_HEADER_H_

#define makefourcc(ch0, ch1, ch2, ch3) \
	((ovr::uint32)(ovr::uint8)(ch0) | ((ovr::uint32)(ovr::uint8)(ch1) << 8) | \
	((ovr::uint32)(ovr::uint8)(ch2) << 16) | ((ovr::uint32)(ovr::uint8)(ch3) << 24))

namespace ovr_asset
{
	const ovr::uint32 kAssetFourcc = makefourcc('A', 'S', 'E', 'T');

	struct OvrBlockHeader
	{
		ovr::uint32	fourcc = 0;
		ovr::uint32	data_size = 0;

		bool	Serialze(OvrArchive& a_serialize)
		{
			if (!a_serialize.Serialize(fourcc))
			{
				return false;
			}
			if (!a_serialize.Serialize(data_size))
			{
				return false;
			}
			return true;
		}
	};
	struct OvrBlockTypeHeader
	{
		ovr::uint32	fourcc = 0;
		ovr::uint32	data_size = 0;
		ovr::int32	sub_type = 0;

		bool	Serialze(OvrArchive& a_serialize)
		{
			if (!a_serialize.Serialize(fourcc))
			{
				return false;
			}
			if (!a_serialize.Serialize(data_size))
			{
				return false;
			}
			if (!a_serialize.Serialize(sub_type))
			{
				return false;
			}
			return true;
		}
	};

	struct OvrAssetHeader
	{
	public:

		ovr::uint32	fourcc			= kAssetFourcc;
		ovr::uint32	data_size		= 0;
		ovr::int32	asset_type		= kAssetInvalid;
		ovr::uint16	version_major	= 0;
		ovr::uint16	version_minor	= 0;
		OvrString	display_name;

		bool	Serialze(OvrArchive& a_serialize) 
		{
			if (!a_serialize.Serialize(fourcc))
			{
				return false;
			}
			if (!a_serialize.Serialize(data_size))
			{
				return false;
			}
			if (!a_serialize.Serialize(asset_type))
			{
				return false;
			}
			if (!a_serialize.Serialize(version_major))
			{
				return false;
			}
			if (!a_serialize.Serialize(version_minor))
			{
				return false;
			}
			if (!a_serialize.Serialize(display_name))
			{
				return false;
			}
			
			return true;
		}

	};
}

#endif
