﻿#include "headers.h"
#include "wx_asset_manager/ovr_archive_material.h"
#include "serialize_data.h"
#include <wx_base/wx_small_file.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/document.h>
#include <wx_asset_manager/ovr_archive_asset_manager.h>
#include "ovr_asset_header.h"
#include "wx_base/wx_log.h"
namespace ovr_asset 
{
	std::map<int, OvrString> OvrArchiveMaterial::m_blend_mode;
	std::map<int, OvrString> OvrArchiveMaterial::m_cull_mode;
	void OvrTextureInfo::operator = (const OvrTextureInfo& a_info)
	{
		file_path_name = a_info.file_path_name;
		usage = a_info.usage;
	}

	bool OvrTextureInfo::Serialize(OvrArchive& a_serialize)
	{
		if (!a_serialize.Serialize(file_path_name))
		{
			return false;
		}

		if (OvrArchive::kStore == a_serialize.GetType())
		{
			ovr::int32 temp_usage = usage;
			if (!a_serialize.Serialize(temp_usage))
			{
				return false;
			}
		}
		else
		{
			ovr::int32 temp_usage = 0;
			if (!a_serialize.Serialize(temp_usage))
			{
				return false;
			}
			usage = (OvrTextureUsage)temp_usage;
		}
		return true;
	}

	bool OvrMaterialParam::Serialize(OvrArchive& a_serialize)
	{
		if (!ovr_utility::Serialize(a_serialize, ambient))
		{
			return false;
		}
		if (!ovr_utility::Serialize(a_serialize, diffuse))
		{
			return false;
		}
		if (!ovr_utility::Serialize(a_serialize, specular))
		{
			return false;
		}
		if (!ovr_utility::Serialize(a_serialize, emisive))
		{
			return false;
		}
		if (!ovr_utility::Serialize(a_serialize, bump))
		{
			return false;
		}

		if (!a_serialize.Serialize(opacity))
		{
			return false;
		}
		if (!a_serialize.Serialize(shininess))
		{
			return false;
		}
		if (!a_serialize.Serialize(reflectivity))
		{
			return false;
		}
		if (!a_serialize.Serialize(alpha))
		{
			return false;
		}
		return true;
	}
	OvrArchiveMaterial::OvrArchiveMaterial()
	{
		if (m_blend_mode.size() == 0)
		{
			m_blend_mode[0] = OvrString("one");
			m_blend_mode[1] = OvrString("zero");
			m_blend_mode[2] = OvrString("dest_colour");
			m_blend_mode[3] = OvrString("dest_colour");
			m_blend_mode[4] = OvrString("one_minus_dest_colour");
			m_blend_mode[5] = OvrString("one_minus_src_colour");
			m_blend_mode[6] = OvrString("dest_alpha");
			m_blend_mode[7] = OvrString("src_alpha");
			m_blend_mode[8] = OvrString("one_minus_dest_alpha");
			m_blend_mode[9] = OvrString("one_minus_src_alpha");
		}
		if (m_cull_mode.size() == 0)
		{
			m_cull_mode[0] = OvrString("None");
			m_cull_mode[1] = OvrString("Back");
			m_cull_mode[2] = OvrString("Front");
		}
	}

	//保存数据到磁盘中
	bool OvrArchiveMaterial::Save() 
	{
		//构建Json实例
		RAPIDJSON_NAMESPACE::StringBuffer s;
		RAPIDJSON_NAMESPACE::PrettyWriter<RAPIDJSON_NAMESPACE::StringBuffer> writer(s);

		writer.StartObject();

		//header
		writer.Key("header");
		writer.StartObject();
		writer.Key("fourcc");			writer.Uint(kAssetFourcc);
		writer.Key("asset type");		writer.Int(kAssetMaterial);
		writer.Key("major version");	writer.Uint(0);
		writer.Key("minor version");	writer.Uint(0);
		writer.Key("display name");		writer.String(display_name.GetStringConst());
		writer.EndObject();

		if (!IsDepthCheck())
		{
			writer.Key("DepthCheck");
			writer.Bool(false);
		}
		if (!IsDepthWrite())
		{
			writer.Key("DepthWrite");
			writer.Bool(false);
		}
		if (is_set_blend)
		{
			writer.Key("SceneBlend");
			writer.StartObject();
			writer.Key("SrcFactor");
			writer.String(GetBlendModeFromInt(src_str).GetStringConst());
			writer.Key("DestFactor");
			writer.String(GetBlendModeFromInt(dest_str).GetStringConst());
			writer.EndObject();
		}
		if (IsCullMode())
		{
			writer.Key("CullMode");
			writer.String(cull_mode.GetStringConst());
		}
		else
		{
			writer.Key("CullMode");
			writer.String(GetCullModeFromInt(1).GetStringConst());
		}
		writer.Key("Shader");
		writer.StartObject();
		writer.Key("Name");
		writer.String(shader_name.GetStringConst());
		writer.Key("Constant");
		writer.StartArray();
		for (size_t i = 0;i<constant_sample2d.size(); i++)
		{
			writer.StartObject();
			writer.Key("Name");
			writer.String(constant_sample2d[i].name.GetStringConst());
			writer.Key("Type");
			writer.String("Sample2D");
			writer.Key("Value");
			writer.String(constant_sample2d[i].texture.GetStringConst());
			writer.EndObject();
		}
		for (size_t i = 0; i < constants_float.size(); i++)
		{
			writer.StartObject();
			writer.Key("Name");
			writer.String(constants_float[i].name.GetStringConst());
			writer.Key("Type");
			writer.String("Float");
			writer.Key("Value");
			writer.Double(constants_float[i].value);
			writer.EndObject();
		}

		for (size_t i = 0; i < constants_vector4.size();i++)
		{
			writer.StartObject();
			writer.Key("Name");
			writer.String(constants_vector4[i].name.GetStringConst());
			writer.Key("Type");
			writer.String("Float4");
			writer.Key("Value");
			
			writer.StartArray();
			writer.Double(constants_vector4[i].value._m[0]);
			writer.Double(constants_vector4[i].value._m[1]);
			writer.Double(constants_vector4[i].value._m[2]);
			writer.Double(constants_vector4[i].value._m[3]);
			writer.EndArray();
			writer.EndObject();
		}
		writer.EndArray();
		writer.EndObject();
		writer.EndObject();
		//body

		//写文件
		OvrString _file_path = OvrArchiveAssetManager::GetIns()->GetRootDir() + file_path;
		return OvrSmallFile::WriteFile(_file_path, s.GetString(), s.GetSize());
	}

	void OvrArchiveMaterial::SetShaderName(const OvrString& a_name) 
	{ 
		if (shader_name != a_name)
		{
			shader_name = a_name;
			constant_sample2d.clear();
			constants_float.clear();
			constants_vector4.clear();
			is_set_blend = false;
		}
	}

	bool OvrArchiveMaterial::GetSample2DByName(const OvrString& a_name, OvrString& a_value)const 
	{
		bool is_found = false;
		for (auto it = constant_sample2d.begin(); it != constant_sample2d.end(); it++)
		{
			if (it->name == a_name)
			{
				a_value = it->texture;
				is_found = true;
				break;
			}
		}
		return is_found;
	}

	bool OvrArchiveMaterial::GetFloatByName(const OvrString& a_name, float& a_value)const 
	{
		bool is_found = false;
		for (auto it = constants_float.begin(); it != constants_float.end(); it++)
		{
			if (it->name == a_name)
			{
				a_value = it->value;
				is_found = true;
				break;
			}
		}
		return is_found;
	}
	bool OvrArchiveMaterial::GetVector4ByName(const OvrString& a_name, OvrVector4& a_value)const 
	{
		bool is_found = false;
		for (auto it = constants_vector4.begin(); it != constants_vector4.end(); it++)
		{
			if (it->name == a_name)
			{
				a_value = it->value;
				is_found = true;
				break;
			}
		}
		return is_found;
	}

	void OvrArchiveMaterial::SetConstantSample2D(const OvrString& a_name, const OvrString& a_value) 
	{
		bool is_found = false;
		for (auto it = constant_sample2d.begin(); it != constant_sample2d.end(); it++)
		{
			if (it->name == a_name)
			{
				it->texture = a_value;
				is_found = true;
				break;
			}
		}
		if (!is_found)
		{
			constant_sample2d.push_back(OvrMaterialConstants_Sample2D(a_name, a_value));
		}
	}
	void OvrArchiveMaterial::SetConstantFloat4(const OvrString& a_name, const OvrVector4& a_value)
	{
		bool is_found = false;
		for (auto it = constants_vector4.begin(); it != constants_vector4.end(); it++)
		{
			if (it->name == a_name)
			{
				it->value = a_value;
				is_found = true;
				break;
			}
		}
		if (!is_found)
		{
			constants_vector4.push_back(OvrMaterialConstants_Float4(a_name, a_value));
		}
	}
	void OvrArchiveMaterial::SetConstantFloat(const OvrString& a_name, float a_value) 
	{
		bool is_found = false;
		for (auto it = constants_float.begin(); it != constants_float.end(); it++)
		{
			if (it->name == a_name)
			{
				it->value = a_value;
				is_found = true;
				break;
			}
		}
		if (!is_found)
		{
			constants_float.push_back(OvrMaterialConstants_Float(a_name, a_value));
		}
	}

	bool OvrArchiveMaterial::Load() 
	{
		constant_sample2d.clear();
		constants_float.clear();
		constants_vector4.clear();
		//打开文件
		OvrString abs_file_path = OvrArchiveAssetManager::GetIns()->GetRootDir() + file_path;
		OvrSmallFile small_file;
		/*if (OvrArchiveAssetManager::GetIns()->GetIsOvrm())
		{
			if (!small_file.Open(OvrArchiveAssetManager::GetIns()->GetRootDir(), file_path))
				return false;
		}
		else */
		{
			if (!small_file.Open(abs_file_path))
			{
				return false;
			}
		}
		

		//解析数据
		RAPIDJSON_NAMESPACE::Document doc;
		if (doc.ParseInsitu((char*)small_file.GetFileData()).HasParseError())
		{
			return false;
		}

		/*doc.Parse<0>(small_file.GetFileData());*/
		if (!doc.IsObject())
		{
			return false;
		}

		const char* buffer = small_file.GetFileData();
		doc.Parse<0>(buffer);
		RAPIDJSON_NAMESPACE::Value &data_val = doc["Shader"];
		shader_name = data_val["Name"].GetString();

		if (doc.HasMember("SceneBlend"))
		{
			RAPIDJSON_NAMESPACE::Value &blend_value = doc["SceneBlend"];
			
			src_str = GetBlendModeFromString(blend_value["SrcFactor"].GetString());
			dest_str = GetBlendModeFromString(blend_value["DestFactor"].GetString());

			OvrLogD("ovrasset","src %d dest %d",src_str,dest_str);
			is_set_blend = true;
		}
		else
		{
			OvrLogD("ovrasset","not sceneblend");
		}
		if (doc.HasMember("DepthWrite"))
		{
			bdepth_write = doc["DepthWrite"].GetBool();
		}
		if (doc.HasMember("DepthCheck"))
		{
			bdepth_check = doc["DepthCheck"].GetBool();
		}
		if (doc.HasMember("CullMode"))
		{
			RAPIDJSON_NAMESPACE::Value &cull_value = doc["CullMode"];
			bcull_mode = true;
			cull_mode = cull_value.GetString();
		}
		else
			cull_mode = GetCullModeFromInt(1);
		if (data_val.HasMember("Constant"))
		{
			RAPIDJSON_NAMESPACE::Value &constants_list = data_val["Constant"];
			for (ovr::uint32 i = 0; i < constants_list.Size(); i++)
			{
				RAPIDJSON_NAMESPACE::Value& constant_value = constants_list[i];
				OvrString name = constant_value["Name"].GetString();
				OvrString type_str = constant_value["Type"].GetString();
				if (type_str == "Sample2D")
				{
					OvrString temp = constant_value["Value"].GetString();
					constant_sample2d.push_back(OvrMaterialConstants_Sample2D(name, temp));
				}
				else if (type_str == "Float")
				{
					constants_float.push_back(OvrMaterialConstants_Float(name, constant_value["Value"].GetFloat()));
				}
				else if (type_str == "Float4")
				{
					OvrVector4 a;
					
					auto ar = constant_value["Value"].GetArray();
					 
					//RAPIDJSON_NAMESPACE:: &constant_value_list = 
					for (ovr::uint32 j = 0; j < ar.Size(); j++)
					{
						if (j >= 0 && j < 4)
							a._m[j] = ar[j].GetFloat();
					}
				
					constants_vector4.push_back(OvrMaterialConstants_Float4(name, a));
				}
								
			}
		}

		small_file.Close();

		return true;
	}
	int	OvrArchiveMaterial::GetCullMode()
	{
		return GetCullModeFromString(cull_mode);
	}
	void	OvrArchiveMaterial::SetCullMode(int a_cm)
	{
		bcull_mode = true; 
		cull_mode = GetCullModeFromInt(a_cm); 
	}

	const std::vector<OvrMaterialConstants_Sample2D> &OvrArchiveMaterial::GetConstantSample2D()const
	{
		return constant_sample2d;
	}
	const std::vector<OvrMaterialConstants_Float>	&OvrArchiveMaterial::GetConstantFloat()const
	{
		return constants_float;
	}
	const std::vector<OvrMaterialConstants_Float4>	&OvrArchiveMaterial::GetConstantVec4()const
	{
		return constants_vector4;
	}
	
	void OvrArchiveMaterial::SetSceneBlend(int a_src, int a_dest)
	{
		src_str		= a_src; 
		dest_str	= a_dest;
		is_set_blend = true;
	}

	int  OvrArchiveMaterial::GetBlendModeFromString(const OvrString& a_mode)
	{
		std::map<int, OvrString>::iterator it = m_blend_mode.begin();
		while (it != m_blend_mode.end())
		{
			if (it->second == a_mode)
				return it->first;
			it++;
		}
		return 0;
	}

	const OvrString& OvrArchiveMaterial::GetBlendModeFromInt(int a_mode)
	{
		std::map<int, OvrString>::iterator it = m_blend_mode.find(a_mode);
		if (it != m_blend_mode.end())
			return it->second;
		return OvrString("one");
	}
	
	int  OvrArchiveMaterial::GetCullModeFromString(const OvrString& a_mode)
	{
		std::map<int, OvrString>::iterator it = m_cull_mode.begin();
		while (it != m_cull_mode.end())
		{
			if (it->second == a_mode)
				return it->first;
			it++;
		}
		return 0;
	}

	const OvrString& OvrArchiveMaterial::GetCullModeFromInt(int a_mode)
	{
		std::map<int, OvrString>::iterator it = m_cull_mode.find(a_mode);
		if (it != m_cull_mode.end())
			return it->second;
		
		return OvrString("None");
	}
}
