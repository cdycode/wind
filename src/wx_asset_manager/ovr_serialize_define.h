﻿#ifndef OVR_SERIALIZE_MANAGE_OVR_SERIALIZE_DEFINE_H 
#define OVR_SERIALIZE_MANAGE_OVR_SERIALIZE_DEFINE_H

#include <stdio.h>
#include <wx_base/wx_type.h>
#include <wx_base/wx_string.h>
#include <wx_base/wx_time.h>

#ifndef OVRMAKEFOURCC
#	define OVRMAKEFOURCC( ch0, ch1, ch2, ch3 )								\
						( ( ovr::uint32 )( ovr::uint8 )( ch0 )           |	\
						( ( ovr::uint32 )( ovr::uint8 )( ch1 ) << 8  )   |	\
						( ( ovr::uint32 )( ovr::uint8 )( ch2 ) << 16 )   |	\
						( ( ovr::uint32 )( ovr::uint8 )( ch3 ) << 24 ) )
#endif // !makefourcc

#define FOURCC_PROJECT							OVRMAKEFOURCC( 'P', 'R', 'O', 'J' )
#define FOURCC_PLOT								OVRMAKEFOURCC( 'P', 'L', 'O', 'T' )
#define FOURCC_SEQUENCE							OVRMAKEFOURCC( 'S', 'E', 'Q', 'U' )
#define FOURCC_SENCE							OVRMAKEFOURCC( 'S', 'C', 'E', 'N' )
#define FOURCC_ASSET							OVRMAKEFOURCC( 'A', 'S', 'E', 'T' )
#define FOURCC_TRACK							OVRMAKEFOURCC( 'T', 'R', 'A', 'K' )
#define FOURCC_TRACK_HEADER						OVRMAKEFOURCC( 'T', 'R', 'H', 'D' )
#define FOURCC_ACTOR							OVRMAKEFOURCC( 'A', 'C', 'T', 'R' )
#define FOURCC_ACTOR_HEADER						OVRMAKEFOURCC( 'A', 'C', 'H', 'D' )
#define FOURCC_MEDIA							OVRMAKEFOURCC( 'O', 'V', 'R', 'M' )
#define FOURCC_RENDER_FRAME						OVRMAKEFOURCC( 'F', 'D', 'F', 'R' )
#define FOURCC_FRAME							OVRMAKEFOURCC( 'F', 'R', 'A', 'M' )

#define MAJOR_VERSION							0
#define MINOR_VERSION							1

#endif
