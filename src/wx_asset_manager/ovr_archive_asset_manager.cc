﻿#include "headers.h"
#include <wx_asset_manager/ovr_archive_asset_manager.h>
#include <wx_asset_manager/ovr_archive_texture.h>
#include <wx_asset_manager/ovr_archive_mesh.h>
#include <wx_asset_manager/ovr_archive_pose.h>
#include <wx_asset_manager/ovr_archive_skeletal_animation.h>
#include <wx_asset_manager/ovr_archive_skeleton.h>
#include <wx_asset_manager/ovr_archive_material.h>

namespace ovr_asset 
{
	OvrAssetType OvrArchiveAssetManager::GetAssetType(const OvrString& a_file_path) 
	{
		OvrString file_suffix;
		OvrFileTools::GetFileSuffixFromPath(a_file_path.GetStringConst(), file_suffix);
		OvrAssetType type = kAssetInvalid;
		if (file_suffix == "tex")
		{
			type = kAssetTexture;
		}
		else if (file_suffix == "mat")
		{
			type = kAssetMaterial;
		}
		else if (file_suffix == "mesh")
		{
			type = kAssetMesh;
		}
		else if (file_suffix == "skeleton")
		{
			type = kAssetSkeleton;
		}
		else if (file_suffix == "ani")
		{
			type = kAssetSkeletalAnimation;
		}
		else if (file_suffix == "pose")
		{
			type = kAssetPose;
		}
		else if (file_suffix == "pani")
		{
			type = kAssetPoseAnimation;
		}

		return type;
	}

	void OvrArchiveAssetManager::SetRootDir(const OvrString& a_dir) 
	{
		root_dir = a_dir;

		if (!OvrFileTools::CheckPathLastIsSlash(a_dir.GetStringConst())/* && !is_ovrm*/)
		{
			root_dir.AppendString("/");
		}
	}

	OvrArchiveAsset* OvrArchiveAssetManager::CreateAsset(OvrAssetType a_type, const OvrString& a_file_path)
	{
		//从文件中进行加载
		OvrArchiveAsset* asset = CreateAssetClass(a_type);
		if (nullptr == asset)
		{
			return nullptr;
		}
		asset->SetFilePath(a_file_path);
		return asset;
	}

	OvrArchiveAsset* OvrArchiveAssetManager::LoadAsset(const OvrString& a_file_path, bool a_breload)
	{
		if (a_file_path.GetStringSize() == 0)
		{
			return nullptr;
		}
		OvrString tempString(root_dir);
		tempString.AppendString(a_file_path.GetStringConst());
		//先查找已经创建的
		auto it = asset_map.find(a_file_path);
		if (it != asset_map.end())
		{
			if (!a_breload)
				return it->second;
			else
			{
				DestoryAsset(a_file_path);
			}
		}
		
		//从文件中进行加载
		OvrArchiveAsset* asset = CreateAssetClass(OvrArchiveAssetManager::GetAssetType(tempString));
		if (nullptr == asset)
		{
			return nullptr;
		}
		asset->SetFilePath(a_file_path);
		//加载资源
		if (asset->Load())
		{
			asset_map[a_file_path] = asset;
		}
		else 
		{
			delete asset;
			asset = nullptr;
		}
		return asset;
	}

	void OvrArchiveAssetManager::DestoryAsset(OvrArchiveAsset* a_asset)
	{
		DestoryAsset(a_asset->GetFilePath());
	}
	void OvrArchiveAssetManager::DestoryAsset(const OvrString& a_file_path) 
	{
		if (a_file_path.GetStringSize() == 0)
		{
			return ;
		}

		auto it = asset_map.find(a_file_path);
		if (it != asset_map.end())
		{
			delete it->second;
			asset_map.erase(it);
		}
	}

	OvrArchiveAsset* OvrArchiveAssetManager::CreateAssetClass(OvrAssetType a_type)
	{
		OvrArchiveAsset* asset = NULL;

		switch (a_type)
		{
		case kAssetTexture:
		{
			asset = new OvrArchiveTexture();
		}
		break;
		case kAssetMaterial:
		{
			asset = new OvrArchiveMaterial();
		}
		break;
		case kAssetMesh:
		{
			asset = new OvrArchiveMesh();
		}
		break;
		case kAssetSkeleton:
		{
			asset = new OvrArchiveSkeleton();
		}
		break;
		case kAssetSkeletalAnimation:
		{
			asset = new OvrArchiveSkeletalAnimation();
		}
		break;
		case kAssetPose:
		{
			asset = new OvrArchivePoseContainer();
		}
		break;
		case kAssetPoseAnimation:
		{
			asset = new OvrArchivePoseAnimation();
		}
		break;

		default:
			break;
		}
		
		return asset;
	}
}