﻿#include "headers.h"
#include "wx_asset_manager/ovr_archive_mesh.h"
#include "serialize_data.h"

namespace ovr_asset 
{
	void OvrVertex::operator = (const OvrVertex& a_vertex)
	{
		position = a_vertex.position;
		normal = a_vertex.normal;
		tangent = a_vertex.tangent;
		binormal = a_vertex.binormal;
		color = a_vertex.color;
		uv0 = a_vertex.uv0;
		uv1 = a_vertex.uv1;
		bone_index = a_vertex.bone_index;
		bone_weights = a_vertex.bone_weights;
	}

	bool OvrVertex::operator == (const OvrVertex & a_vertex) const
	{
		return	(position == a_vertex.position) &&
			(normal == a_vertex.normal) &&
			(tangent == a_vertex.tangent) &&
			(binormal == a_vertex.binormal) &&
			(color == a_vertex.color) &&
			(uv0 == a_vertex.uv0) &&
			(uv1 == a_vertex.uv1) &&
			(bone_index == a_vertex.bone_index) &&
			(bone_weights == a_vertex.bone_weights);
	}

	bool OvrVertex::Serialize(OvrArchive& a_serialize, ovr::uint32 a_attributes)
	{
		if ((a_attributes & (kVertexAttributePosition | kVertexAttributeAll)) != 0)
		{
			if (!ovr_utility::Serialize(a_serialize, position))
			{
				return false;
			}
		}

		if ((a_attributes & (kVertexAttributeNormal | kVertexAttributeAll)) != 0)
		{
			if (!ovr_utility::Serialize(a_serialize, normal))
			{
				return false;
			}
		}

		if ((a_attributes & (kVertexAttributeTangent | kVertexAttributeAll)) != 0)
		{
			if (!ovr_utility::Serialize(a_serialize, tangent))
			{
				return false;
			}
		}

		if ((a_attributes & (kVertexAttributeBinormal | kVertexAttributeAll)) != 0)
		{
			if (!ovr_utility::Serialize(a_serialize, binormal))
			{
				return false;
			}
		}

		if ((a_attributes & (kVertexAttributeColor | kVertexAttributeAll)) != 0)
		{
			if (!ovr_utility::Serialize(a_serialize, color))
			{
				return false;
			}
		}

		if ((a_attributes & (kVertexAttributeUV0 | kVertexAttributeAll)) != 0)
		{
			if (!a_serialize.Serialize(uv0[0]) || !a_serialize.Serialize(uv0[1]))
			{
				return false;
			}
		}

		if ((a_attributes & (kVertexAttributeUV1 | kVertexAttributeAll)) != 0)
		{
			if (!a_serialize.Serialize(uv1[0]) || !a_serialize.Serialize(uv1[1]))
			{
				return false;
			}
		}


		if ((a_attributes & (kVertexAttributeIndices | kVertexAttributeAll)) != 0)
		{
			if (!a_serialize.Serialize(bone_index[0]) || !a_serialize.Serialize(bone_index[1]) || \
				!a_serialize.Serialize(bone_index[2]) || !a_serialize.Serialize(bone_index[3]))
			{
				return false;
			}
		}

		if ((a_attributes & (kVertexAttributeWeights | kVertexAttributeAll)) != 0)
		{
			if (!a_serialize.Serialize(bone_weights[0]) || !a_serialize.Serialize(bone_weights[1]) || \
				!a_serialize.Serialize(bone_weights[2]) || !a_serialize.Serialize(bone_weights[3]))
			{
				return false;
			}
		}
		return true;
	}

	void OvrPivotInfo::operator = (const OvrPivotInfo& a_info) 
	{
		rotation_pivot	= a_info.rotation_pivot;
		rotation_offset = a_info.rotation_offset;
		scale_pivot		= a_info.scale_pivot;
		scale_offset	= a_info.scale_offset;
	}

	bool OvrPivotInfo::Serialize(OvrArchive& a_serialize) 
	{
		if (!ovr_utility::Serialize(a_serialize, rotation_pivot))
		{
			return false;
		}
		if (!ovr_utility::Serialize(a_serialize, rotation_offset))
		{
			return false;
		}
		if (!ovr_utility::Serialize(a_serialize, scale_pivot))
		{
			return false;
		}
		if (!ovr_utility::Serialize(a_serialize, scale_offset))
		{
			return false;
		}
		return true;
	}

	OvrArchiveSurface::OvrArchiveSurface()
		:vertex_attributes(0)
		,vertex_num(0)
		,triangle_num(0)
		,material_num(0)
		,vertexs(nullptr)
		,triangle(nullptr)
		,material_path(nullptr)
	{
	}
	OvrArchiveSurface::~OvrArchiveSurface() 
	{
		SetVertexNum(0);
		SetTriangleNum(0);
		SetMaterialNum(0);
	}

	bool OvrArchiveSurface::SetVertexNum(ovr::uint32 a_num) 
	{
		//释放以前的内存
		if (nullptr != vertexs)
		{
			delete[]vertexs;
			vertexs = nullptr;
		}
		vertex_num = 0;
		if (a_num == 0) 
		{
			return true;
		}

		//分配新空间
		vertexs = new OvrVertex[a_num];
		if (nullptr != vertexs)
		{
			vertex_num = a_num;
		}
		return (nullptr != vertexs);
	}
	bool OvrArchiveSurface::SetTriangleNum(ovr::uint32 a_num)
	{
		if (nullptr != triangle)
		{
			delete[]triangle;
			triangle = nullptr;
		}
		triangle_num = 0;
		if (a_num == 0)
		{
			return true;
		}

		triangle = new OvrArray3i[a_num];
		if (nullptr != triangle)
		{
			triangle_num = a_num;
		}
		return (nullptr != triangle);
	}
	bool OvrArchiveSurface::SetMaterialNum(ovr::uint32 a_num) 
	{
		if (nullptr != material_path)
		{
			delete[]material_path;
			material_path = nullptr;
		}
		material_num = 0;
		if (a_num == 0)
		{
			return true;
		}

		material_path = new OvrString[a_num];
		if (nullptr != material_path)
		{
			material_num = a_num;
		}
		return (nullptr != material_path);
	}

	OvrVertex& OvrArchiveSurface::GetVertex(ovr::uint32 a_index) 
	{ 
		OVR_ASSERT(a_index < vertex_num);
		return vertexs[a_index]; 
	}
	OvrArray3i& OvrArchiveSurface::GetTriangle(ovr::uint32 a_index)
	{ 
		OVR_ASSERT(a_index < triangle_num);
		return triangle[a_index]; 
	}
	OvrString& OvrArchiveSurface::GetMaterialPath(ovr::uint32 a_index) 
	{ 
		OVR_ASSERT(a_index < material_num);
		return material_path[a_index]; 
	}

	bool OvrArchiveSurface::Serialize(OvrArchive& a_archive) 
	{
		if (!a_archive.Serialize(surface_type))
		{
			return false;
		}

		if (!a_archive.Serialize(node_name))
		{
			return false;
		}
		if (kSerializeSurfaceEmpty != surface_type)
		{
			//序列化顶点
			if (!SerializeVertex(a_archive))
			{
				return false;
			}

			//序列化三角形
			if (!SerializeTriangle(a_archive))
			{
				return false;
			}

			//序列化材质
			if (!SerializeMaterial(a_archive))
			{
				return false;
			}

			//序列化重心信息
			if (!pivot_info.Serialize(a_archive))
			{
				return false;
			}

			if (!ovr_utility::Serialize(a_archive, center_position))
			{
				return false;
			}

			return true;
		}

		//存取surface数目
		if (OvrArchive::kStore == a_archive.GetType())
		{
			ovr::uint32 surface_num = subsurface_vector.size();
			if (!a_archive.Serialize(surface_num))
			{
				return false;
			}

			for (auto surface : subsurface_vector)
			{
				if (!surface->Serialize(a_archive))
				{
					return false;
				}
			}
		}
		else
		{
			ovr::uint32 surface_num = 0;
			if (!a_archive.Serialize(surface_num))
			{
				return false;
			}

			for (ovr::uint32 i = 0; i < surface_num; i++)
			{
				OvrArchiveSurface* new_node = new OvrArchiveSurface;
				if (!new_node->Serialize(a_archive))
				{
					delete new_node;
					return false;
				}
				subsurface_vector.push_back(new_node);
			}
		}

		return true;
	}

	bool OvrArchiveSurface::SerializeVertex(OvrArchive& a_archive) 
	{
		if (!a_archive.Serialize(vertex_attributes))
		{
			return false;
		}

		//顶点
		if (!a_archive.Serialize(vertex_num))
		{
			return false;
		}
		if (OvrArchive::kLoad == a_archive.GetType())
		{
			if (!SetVertexNum(vertex_num))
			{
				return false;
			}
		}
		for (ovr::uint32 i = 0; i < vertex_num; i++)
		{
			if (!vertexs[i].Serialize(a_archive, vertex_attributes))
			{
				return false;
			}
		}
		return true;
	}
	bool OvrArchiveSurface::SerializeTriangle(OvrArchive& a_archive) 
	{
		//三角形
		if (!a_archive.Serialize(triangle_num))
		{
			return false;
		}
		if (OvrArchive::kLoad == a_archive.GetType())
		{
			if (!SetTriangleNum(triangle_num))
			{
				return false;
			}
		}
		for (ovr::uint32 i = 0; i < triangle_num; i++)
		{
			if (!a_archive.Serialize(triangle[i][0]) || !a_archive.Serialize(triangle[i][1]) || !a_archive.Serialize(triangle[i][2]))
			{
				return false;
			}
		}
		return true;
	}
	bool OvrArchiveSurface::SerializeMaterial(OvrArchive& a_archive) 
	{
		//序列化材质
		if (!a_archive.Serialize(material_num))
		{
			return false;
		}
		if (OvrArchive::kLoad == a_archive.GetType())
		{
			if (!SetMaterialNum(material_num))
			{
				return false;
			}
		}
		for (ovr::uint32 i = 0; i < material_num; i++)
		{
			if (!a_archive.Serialize(material_path[i]))
			{
				return false;
			}
		}
		return true;
	}

	
	OvrArchiveMesh::OvrArchiveMesh() 
	{
	}
	OvrArchiveMesh::~OvrArchiveMesh() 
	{
		for (ovr::uint32 i = 0; i < surface_vector.size(); i++)
		{
			delete surface_vector[i];
		}
		surface_vector.clear();
	}

	ovr::uint32	OvrArchiveMesh::AddSurface(OvrArchiveSurface* a_node)
	{	
		surface_vector.push_back(a_node);
		return surface_vector.size() - 1;
	}

	OvrArchiveSurface*	OvrArchiveMesh::AddSurface() 
	{
		OvrArchiveSurface* new_surface = new OvrArchiveSurface;
		if (nullptr != new_surface)
		{
			surface_vector.push_back(new_surface);
		}
		return new_surface;
	}

	OvrArchiveSurface*	OvrArchiveMesh::GetSurface(ovr::uint32 a_index)
	{
		return surface_vector[a_index];
	}

	bool OvrArchiveMesh::Serialize(OvrArchive& a_archive) 
	{
		if (!a_archive.Serialize(skeleton_path))
		{
			return false;
		}
		if (!a_archive.Serialize(pose_path))
		{
			return false;
		}
		//存取surface数目
		if (OvrArchive::kStore == a_archive.GetType())
		{
			ovr::uint32 surface_num = surface_vector.size();
			if (!a_archive.Serialize(surface_num))
			{
				return false;
			}

			for (auto surface : surface_vector)
			{
				if (!surface->Serialize(a_archive))
				{
					return false;
				}
			}
		}
		else 
		{
			ovr::uint32 surface_num = 0;
			if (!a_archive.Serialize(surface_num))
			{
				return false;
			}

			for (ovr::uint32 i = 0; i < surface_num; i++)
			{
				OvrArchiveSurface* new_node = new OvrArchiveSurface;
				if (!new_node->Serialize(a_archive))
				{
					delete new_node;
					return false;
				}
				surface_vector.push_back(new_node);
			}
		}
		return true;
	}
}
