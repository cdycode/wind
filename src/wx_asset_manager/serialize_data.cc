#include "headers.h"
#include "serialize_data.h"

namespace ovr_utility {
	
	bool Serialize(OvrArchive& a_serialize, OvrMatrix4& a_matrix4f)
	{
		for (int i = 0; i < 16; i++)
		{
			a_serialize.Serialize(a_matrix4f[i]);
		}
		return true;
	}


	bool Serialize(OvrArchive& a_serialize, OvrQuaternion& a_quaternion)
	{
		for (int i = 0; i < 4; i++)
		{
			a_serialize.Serialize(a_quaternion[i]);
		}
		return true;
	}

	bool Serialize(OvrArchive& a_serialize, OvrVector3& a_vector3f)
	{
		for (int i = 0; i < 3; i++)
		{
			if (!a_serialize.Serialize(a_vector3f[i]))
			{
				return false;
			}
		}
		return true;
	}

	bool Serialize(OvrArchive& a_serialize, OvrVector4& a_vector4f)
	{
		for (int i = 0; i < 4; i++)
		{
			if (!a_serialize.Serialize(a_vector4f[i]))
			{
				return false;
			}
		}
		return true;
	}


}

