﻿#ifndef OVR_ASSET_SERIALIZE_HEADERS_H_
#define OVR_ASSET_SERIALIZE_HEADERS_H_

#include <wx_base/wx_type.h>
#include <wx_base/wx_archive.h>
#include <wx_base/wx_string.h>
#include <wx_base/wx_matrix4f.h>
#include <wx_base/wx_file_tools.h>

//#ifdef _MSC_VER
#include <libktx/ktx.h>
#include <libktx/etcpack.h>
//#endif

#endif
