﻿#include "headers.h"
#include <wx_base/wx_small_file.h>
#include "wx_asset_manager/ovr_archive_asset.h"
#include "ovr_asset_header.h"
#include "wx_asset_manager/ovr_archive_asset_manager.h"

namespace ovr_asset 
{
	//保存数据到磁盘中
	bool OvrBinaryAsset::Save() 
	{
		OvrString full_path(OvrArchiveAssetManager::GetIns()->GetRootDir());
		full_path += file_path;

		//创建文件
		OvrIOFileHandle* file_handle = OvrIOFileHandle::CreateHandleFromFile(full_path.GetStringConst());
		if (nullptr == file_handle)
		{
			return false;
		}

		//填充display name
		if (display_name.GetStringSize() <= 0)
		{
			OvrFileTools::GetFileNameFromPath(file_path.GetStringConst(), display_name);
		}

		OvrArchiveStore archive_store(file_handle);

		//序列化文件头
		OvrAssetHeader asset_header;
		asset_header.asset_type = GetType();
		asset_header.version_major = version_major;
		asset_header.version_minor = version_minor;
		asset_header.display_name = display_name;

		if (!asset_header.Serialze(archive_store))
		{
			delete file_handle;
			return false;
		}

		if (!Serialize(archive_store))
		{
			delete file_handle;
			return false;
		}

		//填充datasize
		asset_header.data_size = file_handle->GetLength();
		archive_store.Seek(0);
		if (!asset_header.Serialze(archive_store))
		{
			delete file_handle;
			return false;
		}

		delete file_handle;
		return true;
	}

	//从磁盘中加载数据
	bool OvrBinaryAsset::Load() 
	{
		OvrString full_path(OvrArchiveAssetManager::GetIns()->GetRootDir());
		full_path += file_path;

		//创建文件
		OvrIOInerface* file_handle = NULL;
		/*if (OvrArchiveAssetManager::GetIns()->GetIsOvrm())
		{
			OvrSmallFile small_file;
			if (!small_file.Open(OvrArchiveAssetManager::GetIns()->GetRootDir(), file_path))
			{
				return false;
			}
			file_handle = new OvrIOVarBufferHandle(small_file.GetFileData(), small_file.GetFileSize(),true);
			file_handle->SetPosition(0);
			if (nullptr == file_handle)
			{
				return false;
			}
		}
		else*/
		file_handle = OvrIOFileHandle::LoadHandleFromFile(full_path.GetStringConst());
		

		OvrArchiveLoad archive_load(file_handle);

		//序列化文件头
		bool is_ok = false;
		OvrAssetHeader asset_header;
		if (asset_header.Serialze(archive_load) && asset_header.asset_type == GetType())
		{
			//给版本号赋值
			version_major = asset_header.version_major;
			version_minor = asset_header.version_minor;
			display_name = asset_header.display_name;

			//加载文件体
			is_ok = Serialize(archive_load);
		}

		delete file_handle;
		return is_ok;
	}
}