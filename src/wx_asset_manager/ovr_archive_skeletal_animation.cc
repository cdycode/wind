﻿#include <wx_asset_manager/ovr_motion_frame.h>
#include "headers.h"
#include "wx_asset_manager/ovr_archive_skeletal_animation.h"
#include "serialize_data.h"

namespace ovr_asset 
{
	OvrArchiveSkeletalAnimation::OvrArchiveSkeletalAnimation()
	{
	}
	OvrArchiveSkeletalAnimation::~OvrArchiveSkeletalAnimation()
	{
		if (frame_list.size() > 0)
		{
			for (auto it = frame_list.begin(); it != frame_list.end(); it++)
			{
				delete *it;
			}
			frame_list.clear();
		}
	}
	bool OvrArchiveSkeletalAnimation::AddFrame(OvrMotionFrame* a_frame)
	{
		if (nullptr == a_frame)
		{
			return false;
		}

		OvrMotionFrame* new_frame = new OvrMotionFrame;
		if (!new_frame->Init(a_frame->GetJointNum()))
		{
			delete new_frame;
			return false;
		}
		
		for (ovr::uint32 i = 0; i < a_frame->GetJointNum(); i++)
		{
			*(new_frame->GetJointAnimationFrame(i)) = *(a_frame->GetJointAnimationFrame(i));
		}
		frame_list.push_back(new_frame);
		return true;
	}

	bool OvrArchiveSkeletalAnimation::Serialize(OvrArchive& a_archive) 
	{
		if (!a_archive.Serialize(frame_rate))
		{
			return false;
		}
		if (!a_archive.Serialize(duration))
		{
			return false;
		}

		if (OvrArchive::kStore == a_archive.GetType())
		{
			//joint name
			ovr::uint32 number = joint_name_list.size();
			if (!a_archive.Serialize(number))
			{
				return false;
			}
			for (ovr::uint32 i = 0; i < number; i++)
			{
				if (!a_archive.Serialize(joint_name_list[i]))
				{
					return false;
				}
			}

			//frame number
			ovr::uint32 frame_number = frame_list.size();
			if (!a_archive.Serialize(frame_number))
			{
				return false;
			}
			
			for (ovr::uint32 i = 0; i < frame_number; i++)
			{
				if (!SerializeFrame(a_archive, frame_list[i]))
				{
					return false;
				}
			}
		}
		else 
		{
			//joint name
			ovr::uint32 number = 0;
			if (!a_archive.Serialize(number))
			{
				return false;
			}
			for (ovr::uint32 i = 0; i < number; i++)
			{
				OvrString name;
				if (!a_archive.Serialize(name))
				{
					return false;
				}
				joint_name_list.push_back(name);
			}

			//frame number
			ovr::uint32 frame_number = 0;
			if (!a_archive.Serialize(frame_number))
			{
				return false;
			}

			for (ovr::uint32 i = 0; i < frame_number; i++)
			{
				OvrMotionFrame* new_frame = new OvrMotionFrame;
				if (!SerializeFrame(a_archive, new_frame))
				{
					return false;
				}
				frame_list.push_back(new_frame);
			}
		}
		return true;
	}

	bool OvrArchiveSkeletalAnimation::SerializeFrame(OvrArchive& a_archive, OvrMotionFrame* a_frame)
	{
		if (OvrArchive::kStore == a_archive.GetType())
		{
			ovr::uint32 number = a_frame->GetJointNum();
			if (!a_archive.Serialize(number))
			{
				return false;
			}
			for (ovr::uint32 i = 0; i < number; i++)
			{
                OvrJointFrame* joint_frame = a_frame->GetJointAnimationFrame(i);
				ovr::uint8 temp = (ovr::uint8)(joint_frame->is_materix);
				if (!a_archive.Serialize(temp))
				{
					return false;
				}
				if (!a_archive.Serialize(joint_frame->joint_index))
				{
					return false;
				}
				if (!a_archive.Serialize(joint_frame->joint_name))
				{
					return false;
				}
				if (!ovr_utility::Serialize(a_archive, joint_frame->joint_materix))
				{
					return false;
				}
				if (!ovr_utility::Serialize(a_archive, joint_frame->joint_positon))
				{
					return false;
				}
				if (!ovr_utility::Serialize(a_archive, joint_frame->joint_quaternion))
				{
					return false;
				}
			}
		}
		else 
		{
			ovr::uint32 number = 0;
			if (!a_archive.Serialize(number))
			{
				return false;
			}
			a_frame->Init(number);
			for (ovr::uint32 i = 0; i < number; i++)
			{
                OvrJointFrame* joint_frame = a_frame->GetJointAnimationFrame(i);
				ovr::uint8 temp;
				if (!a_archive.Serialize(temp))
				{
					return false;
				}
				joint_frame->is_materix = temp > 0 ? true : false;

				if (!a_archive.Serialize(joint_frame->joint_index))
				{
					return false;
				}
				if (!a_archive.Serialize(joint_frame->joint_name))
				{
					return false;
				}
				if (!ovr_utility::Serialize(a_archive, joint_frame->joint_materix))
				{
					return false;
				}
				if (!ovr_utility::Serialize(a_archive, joint_frame->joint_positon))
				{
					return false;
				}
				if (!ovr_utility::Serialize(a_archive, joint_frame->joint_quaternion))
				{
					return false;
				}
			}
		}
		return true;
	}
}
