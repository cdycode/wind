﻿#include "headers.h"
#include "wx_asset_manager/ovr_archive_skeleton.h"
#include "serialize_data.h"

namespace ovr_asset
{
	bool OvrArchiveJoint::Serialize(OvrArchive& a_serialize)
	{
		if (!a_serialize.Serialize(name))
		{
			return false;
		}

		if (!a_serialize.Serialize(parent_index))
		{
			return false;
		}

		if (!ovr_utility::Serialize(a_serialize, bone_to_mesh_mat))
		{
			return false;
		}
		if (!ovr_utility::Serialize(a_serialize, parent_transform))
		{
			return false;
		}

		if (!ovr_utility::Serialize(a_serialize, default_pose))
		{
			return false;
		}
		return true;
	}

	OvrArchiveSkeleton::OvrArchiveSkeleton()
		:joint_num(0)
		,joints(nullptr)
	{

	}

	OvrArchiveSkeleton::~OvrArchiveSkeleton() 
	{
		SetJointNum(0);
	}

	bool OvrArchiveSkeleton::SetJointNum(ovr::uint32 a_num) 
	{
		if (nullptr != joints)
		{
			delete[]joints;
			joints = nullptr;
		}
		joint_num = 0;
		if (a_num == 0)
		{
			return true;
		}
		joints = new OvrArchiveJoint[a_num];
		if (nullptr != joints)
		{
			joint_num = a_num;
		}
		return nullptr != joints;
	}
	OvrArchiveJoint& OvrArchiveSkeleton::GetJoint(ovr::uint32 a_index) 
	{
		OVR_ASSERT(a_index < joint_num);
		return joints[a_index];
	}

	bool OvrArchiveSkeleton::Serialize(OvrArchive& a_archive) 
	{
		if (!a_archive.Serialize(joint_num))
		{
			return false;
		}
		if (OvrArchive::kLoad == a_archive.GetType())
		{
			if (!SetJointNum(joint_num))
			{
				return false;
			}
		}

		for (ovr::uint32 i = 0; i < joint_num; i++)
		{
			if (!joints[i].Serialize(a_archive)) 
			{
				return false;
			}
		}
		return true;
	}
}
