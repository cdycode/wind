#ifndef FBX_CONVERT_SERIALIZE_DATA_H_
#define FBX_CONVERT_SERIALIZE_DATA_H_

#include <wx_base/wx_archive.h>

namespace ovr_utility
{
	bool Serialize(OvrArchive& a_serialize, OvrMatrix4& a_matrix4f);
	bool Serialize(OvrArchive& a_serialize, OvrQuaternion& a_quaternion);
	bool Serialize(OvrArchive& a_serialize, OvrVector3& a_vector3f);
	bool Serialize(OvrArchive& a_serialize, OvrVector4& a_vector4f);
}
#endif
