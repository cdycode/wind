#include "headers.h"
#include "wx_asset_manager/ovr_archive_pose.h"
#include "serialize_data.h"

namespace ovr_asset
{
	bool OvrPose::Serialize(OvrArchive& a_serialize)
	{
		if (!a_serialize.Serialize(name))
		{
			return false;
		}

		if (!a_serialize.Serialize(surface_name))
		{
			return false;
		}

		//��ȡpose��Ŀ
		if (OvrArchive::kStore == a_serialize.GetType())
		{
			ovr::uint32 vertex_offset_num = vertex_offset_map.size();
			if (!a_serialize.Serialize(vertex_offset_num))
			{
				return false;
			}

			for (auto vertex_offset : vertex_offset_map)
			{
				ovr::uint32 key = vertex_offset.first;
				OvrVector3 value = vertex_offset.second;
				if (!a_serialize.Serialize(key))
				{
					return false;
				}
				if (!ovr_utility::Serialize(a_serialize, value))
				{
					return false;
				}
			}
		}
		else
		{
			ovr::uint32 vertex_offset_num = 0;
			if (!a_serialize.Serialize(vertex_offset_num))
			{
				return false;
			}

			for (ovr::uint32 i = 0; i < vertex_offset_num; i++)
			{
				ovr::uint32 key = 0;
				OvrVector3 value;
				if (!a_serialize.Serialize(key))
				{
					return false;
				}

				if (!ovr_utility::Serialize(a_serialize, value))
				{
					return false;
				}
				vertex_offset_map[key] = value;
			}
		}
		return true;
	}

	OvrArchivePoseContainer::OvrArchivePoseContainer()
		:pose_num(0)
		, poses(nullptr)
	{

	}

	OvrArchivePoseContainer::~OvrArchivePoseContainer()
	{
		SetPoseNum(0);
	}

	bool OvrArchivePoseContainer::Serialize(OvrArchive& a_archive)
	{
		if (!a_archive.Serialize(pose_num))
		{
			return false;
		}
		if (OvrArchive::kLoad == a_archive.GetType())
		{
			if (!SetPoseNum(pose_num))
			{
				return false;
			}
		}

		for (ovr::uint32 i = 0; i < pose_num; i++)
		{
			if (!poses[i].Serialize(a_archive))
			{
				return false;
			}
		}
		return true;
	}

	bool OvrArchivePoseContainer::SetPoseNum(ovr::uint32 a_num)
	{
		if (nullptr != poses)
		{
			delete[]poses;
			poses = nullptr;
		}
		pose_num = 0;
		if (a_num == 0)
		{
			return true;
		}
		poses = new OvrPose[a_num];
		if (nullptr != poses)
		{
			pose_num = a_num;
		}
		return nullptr != poses;
	}

	OvrPose& OvrArchivePoseContainer::GetPose(ovr::uint32 a_index)
	{
		OVR_ASSERT(a_index < pose_num);
		return poses[a_index];
	}

	OvrArchivePoseAnimation::OvrArchivePoseAnimation()
	{

	}

	OvrArchivePoseAnimation::~OvrArchivePoseAnimation()
	{
		if (frame_list.size() > 0)
		{
			for (auto it = frame_list.begin(); it != frame_list.end(); it++)
			{
				delete *it;
			}
			frame_list.clear();
		}
	}

	bool OvrArchivePoseAnimation::AddFrame(OvrMorphFrame* a_frame)
	{
		if (nullptr == a_frame)
		{
			return false;
		}

		OvrMorphFrame* new_frame = new OvrMorphFrame;
		if (!new_frame->Init(a_frame->GetPoseNum()))
		{
			delete new_frame;
			return false;
		}

		for (ovr::uint32 i = 0; i < a_frame->GetPoseNum(); i++)
		{
			*(new_frame->GetPoseFrame(i)) = *(a_frame->GetPoseFrame(i));
		}
		frame_list.push_back(new_frame);
		return true;
	}

	bool OvrArchivePoseAnimation::Serialize(OvrArchive& a_archive)
	{
		if (!a_archive.Serialize(frame_rate))
		{
			return false;
		}
		if (!a_archive.Serialize(duration))
		{
			return false;
		}

		if (OvrArchive::kStore == a_archive.GetType())
		{
			//pose name
			ovr::uint32 number = pose_name_list.size();
			if (!a_archive.Serialize(number))
			{
				return false;
			}
			for (ovr::uint32 i = 0; i < number; i++)
			{
				if (!a_archive.Serialize(pose_name_list[i]))
				{
					return false;
				}
			}

			//frame number
			ovr::uint32 frame_number = frame_list.size();
			if (!a_archive.Serialize(frame_number))
			{
				return false;
			}

			for (ovr::uint32 i = 0; i < frame_number; i++)
			{
				if (!SerializeFrame(a_archive, frame_list[i]))
				{
					return false;
				}
			}
		}
		else
		{
			//pose name
			ovr::uint32 number = 0;
			if (!a_archive.Serialize(number))
			{
				return false;
			}
			for (ovr::uint32 i = 0; i < number; i++)
			{
				OvrString name;
				if (!a_archive.Serialize(name))
				{
					return false;
				}
				pose_name_list.push_back(name);
			}

			//frame number
			ovr::uint32 frame_number = 0;
			if (!a_archive.Serialize(frame_number))
			{
				return false;
			}

			for (ovr::uint32 i = 0; i < frame_number; i++)
			{
                OvrMorphFrame* new_frame = new OvrMorphFrame;
				if (!SerializeFrame(a_archive, new_frame))
				{
					return false;
				}
				frame_list.push_back(new_frame);
			}
		}
		return true;
	}

	bool OvrArchivePoseAnimation::SerializeFrame(OvrArchive& a_archive, OvrMorphFrame* a_frame)
	{
		if (OvrArchive::kStore == a_archive.GetType())
		{
			ovr::uint32 number = a_frame->GetPoseNum();
			if (!a_archive.Serialize(number))
			{
				return false;
			}
			for (ovr::uint32 i = 0; i < number; i++)
			{
                OvrPoseFrame* pose_frame = a_frame->GetPoseFrame(i);
				if (!a_archive.Serialize(pose_frame->pose_index))
				{
					return false;
				}
				if (!a_archive.Serialize(pose_frame->pose_name))
				{
					return false;
				}
				if (!a_archive.Serialize(pose_frame->pose_weight))
				{
					return false;
				}
			}
		}
		else
		{
			ovr::uint32 number = 0;
			if (!a_archive.Serialize(number))
			{
				return false;
			}
			a_frame->Init(number);
			for (ovr::uint32 i = 0; i < number; i++)
			{
                OvrPoseFrame* pose_frame = a_frame->GetPoseFrame(i);
				if (!a_archive.Serialize(pose_frame->pose_index))
				{
					return false;
				}
				if (!a_archive.Serialize(pose_frame->pose_name))
				{
					return false;
				}
				if (!a_archive.Serialize(pose_frame->pose_weight))
				{
					return false;
				}
			}
		}
		return true;
	}
}