﻿#include "headers.h"
#include "wx_asset_manager/ovr_motion_frame.h"

namespace ovr_asset
{

	bool OvrMotionFrame::Blend(OvrMotionFrame* a_new_frame, float a_dst_weight, OvrMotionFrame* a_src_frame, float a_src_weight)
	{
		if (a_src_weight < 0)
			a_src_weight = 0;
		if (a_src_weight > 1.0f)
			a_src_weight = 1.0f;

		if (!a_src_frame || !a_new_frame)
			return false;

		if (joint_frames.size() != a_src_frame->GetJointNum())
			return false;

		ovr::uint32 joint_num = joint_frames.size();
		a_new_frame->Uinit();
		a_new_frame->Init(joint_num);
		float my_weight = a_dst_weight;
		for (ovr::uint32 i = 0; i < joint_num; i++)
		{
			OvrVector3 src_scale, src_pos, my_scale, my_pos, new_scale, new_pos;
			OvrQuaternion src_quat, my_quat, new_quat;
			OvrJointFrame* my_joint_frame = GetJointAnimationFrame(i);
			my_joint_frame->joint_materix.decompose(&my_scale, &my_quat, &my_pos);
			OvrJointFrame* src_joint_frame = a_src_frame->GetJointAnimationFrame(i);
			src_joint_frame->joint_materix.decompose(&src_scale, &src_quat, &src_pos);
			new_scale = src_scale * a_src_weight + my_scale * my_weight;
			new_pos = src_pos * a_src_weight + my_pos * my_weight;
			new_quat = OvrQuaternion::slerp(my_quat, src_quat, my_weight / a_src_weight);
			OvrMatrix4 rotation_matrix;
			rotation_matrix.from_quaternion(new_quat);
			OvrMatrix4 new_matrix = OvrMatrix4::make_scale_matrix(new_scale) * rotation_matrix * OvrMatrix4::make_translation_matrix(new_pos);
			OvrJointFrame* new_joint_frame = a_new_frame->GetJointAnimationFrame(i);
			new_joint_frame->is_materix = true;
			new_joint_frame->joint_index = my_joint_frame->joint_index;
			new_joint_frame->joint_name = my_joint_frame->joint_name;
			new_joint_frame->joint_materix = new_matrix;
			new_joint_frame->joint_positon = new_pos;
			new_joint_frame->joint_quaternion = new_quat;
		}
		return true;
	}

	bool OvrMotionFrame::Init(ovr::uint32 a_num)
	{
		Uinit();
		joint_frames.resize(a_num);
		return true;
	}

	void OvrMotionFrame::Uinit()
	{
		joint_frames.clear();
	}

	ovr::uint32 OvrMotionFrame::GetJointNum()const
	{
		return joint_frames.size();
	}

	OvrJointFrame* OvrMotionFrame::GetJointAnimationFrame(ovr::uint32 a_index)
	{
		if (a_index < joint_frames.size())
		{
			return &joint_frames[a_index];
		}
		return nullptr;
	}

	bool OvrMorphFrame::Init(ovr::uint32 a_num)
	{

		Uinit();
		pose_frames.resize(a_num);
		return true;
	}

	void OvrMorphFrame::Uinit()
	{
		pose_frames.clear();
	}

	ovr::uint32 OvrMorphFrame::GetPoseNum() const
	{
		return pose_frames.size();
	}

	OvrPoseFrame* OvrMorphFrame::GetPoseFrame(ovr::uint32 a_index)
	{
		if (a_index < pose_frames.size())
		{
			return &pose_frames[a_index];
		}
		return nullptr;
	}
}