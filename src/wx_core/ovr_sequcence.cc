﻿#include "headers.h"
#include "wx_core/ovr_sequence.h"
#include <wx_base/wx_unique_name.h>
#include <wx_base/wx_file_tools.h>
#include <wx_core/ovr_scene_context.h>
#include <wx_core/track/ovr_interactive_track.h>
#include <wx_core/track/ovr_event_track.h>
#include <wx_core/track/ovr_mark_track.h>
#include <wx_core/track/ovr_background_audio_track.h>
#include <wx_core/track/ovr_actor_track.h>
#include <wx_core/visual_node/i_ovr_node_manager.h>
#include <wx_core/ovr_background_audio.h>
#include "track/ovr_ambient_light_track.h"
#include <wx_engine/ovr_engine.h>

namespace ovr_core
{
	OvrSequence::OvrSequence()
		:OvrTrack(this)
	{
		UniqueNameCreater::CreateSequenceUniqueName(unique_name);
	}

	OvrSequence::~OvrSequence()
	{
		Uninit();
	}

	bool OvrSequence::Init() 
	{
		background_audio = new OvrBackgroundAudio;
		mark_info = new OvrMarkTrack;
		if (nullptr == background_audio || nullptr == mark_info)
		{
			return false;
		}
		if (!background_audio->Init())
		{
			OvrLogE("ovr_media", "background_audio::Init failed");
			return false;
		}

		IOvrNodeManager::Load(scene_unique_name);

		FillSupportTrackList();
		return true;
	}
	void OvrSequence::Uninit() 
	{
		OvrTrack::Uninit();

		//可视化模块
		IOvrNodeManager::Unload();

		//背景音
		if (nullptr != background_audio)
		{
			background_audio->Uninit();
			delete background_audio;
			background_audio = nullptr;
		}
		if (nullptr != mark_info)
		{
			delete mark_info;
			mark_info = nullptr;
		}
	}

	//根帧索引获取此帧索引处的list
	bool OvrSequence::Update()
	{
		if (is_loop && current_frame_index >= loop_out)
		{
			current_frame_index = loop_in;
		}

		OvrTrack::Update();

		if (current_status == kRenderPlay)
		{
			current_frame_index++;
			if (nullptr != background_audio)
			{
				background_audio->Update();
			}
		}
		return true;
	}

	void OvrSequence::Seek(ovr::uint64 a_position)
	{
		OvrTrack::Seek(a_position);

		//背景声
		if (nullptr != background_audio)
		{
			background_audio->Seek(a_position);
		}
		current_frame_index = a_position;
		current_status = kRenderSeek;
		is_record = false;
	}

	void OvrSequence::Pause()
	{
		OvrTrack::Pause();

		//背景声
		if (nullptr != background_audio)
		{
			background_audio->Pause();
		}
		current_status = kRenderPause;
		is_record = false;
	}

	void OvrSequence::Play()
	{
		OvrTrack::Play();

		//背景声
		if (nullptr != background_audio)
		{
			background_audio->Play();
		}
		current_status = kRenderPlay;
	}

	//修改播放位置 在渲染线程中调用
	void OvrSequence::ChangePlayPosition(ovr::uint64 a_new_position) 
	{ 
		if (!IsInLoopRange(a_new_position))
		{
			ClearLoopRange();
		}
		current_frame_index = a_new_position; 
	}

	//添加轨道
	OvrTrack* OvrSequence::NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) 
	{
		OvrTrack* new_track = nullptr;
		if (a_tag == "actor")
		{
			new_track = new OvrActorTrack(this);
		}
		else if (a_tag == "background_audio")
		{
			new_track = new OvrBackgrounAudioTrack(this);
		}
		else if (a_tag == "interactive")
		{
			new_track = new OvrInterActiveTrack(this);
		}
		else if (a_tag == "event")
		{
			new_track = new OvrEventTrack(this);
		}
		else if (a_tag == "ambient_light")
		{
			new_track = new OvrAmbientLightTrack(this);
		}

		return new_track;
	}

	void OvrSequence::SetLoopRange(ovr::uint64 a_loop_in, ovr::uint64 a_loop_out)
	{
		is_loop = true;
		loop_in = a_loop_in;
		loop_out = a_loop_out;
	}

	bool OvrSequence::IsInLoopRange(ovr::uint64 a_seek_frame_index)
	{
		if (!is_loop)
		{
			return false;
		}
		if (a_seek_frame_index >= loop_in && a_seek_frame_index < loop_out)
		{
			return true;
		}
		return false;
	}

	void OvrSequence::ClearLoopRange()
	{
		is_loop = false;
	}

	void OvrSequence::FillSupportTrackList()
	{
		track_support_list.clear();
		track_support_list.push_back(OvrTrackSupport(kTrackCommon, true, "Actor", "actor"));

		OvrString name = "背景音";
		track_support_list.push_back(OvrTrackSupport(kTrackCommon, true, name, "background_audio"));

		name = "交互";
		track_support_list.push_back(OvrTrackSupport(kTrackClip, true, name, "interactive"));

		name = "事件";
		track_support_list.push_back(OvrTrackSupport(kTrackPoint, true, name, "event"));

		//环境光
		track_support_list.push_back(OvrTrackSupport(kTrackCommon, true, "环境光", "ambient_light"));
	}
}

