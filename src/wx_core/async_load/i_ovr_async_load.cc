﻿#include "../headers.h"
#include <wx_core/async_load/i_ovr_async_load.h>
#include <wx_engine/ovr_texture_manager.h>

namespace ovr_core
{
	bool OvrAsyncTexture::SetFilePath(const OvrString& a_file_path) 
	{
		file_path = a_file_path;
		texture = (ovr_engine::OvrTexture*)ovr_engine::OvrTextureManager::GetIns()->GetByName(file_path);
		return (nullptr != texture);
	}

	bool OvrAsyncTexture::Loading()
	{
		if (file_path.GetStringSize() > 0)
		{
			texture = ovr_engine::OvrTextureManager::GetIns()->Load(file_path);
			return true;
		}
		return false;
	}
	ovr_engine::OvrTexture* OvrAsyncTexture::GetTexture()
	{
		if (nullptr == texture)
		{
			Loading();
		}
		return texture;
	}
}