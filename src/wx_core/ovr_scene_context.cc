﻿#include "headers.h"
#include "wx_core/ovr_scene_context.h"
//#include <wx_engine/ovr_video_screen_actor.h>
#include <wx_core/ovr_scene_camera.h>
#include "wx_engine/ovr_engine.h"
#include <wx_engine/ovr_axis_system.h>
#include <wx_engine/ovr_skinned_mesh_component.h>
#include <wx_engine/ovr_static_mesh_component.h>
#include <wx_asset_manager/ovr_archive_asset_manager.h>
#include <wx_asset_manager/ovr_archive_asset.h>
#include <wx_asset_manager/ovr_archive_mesh.h>
#include <wx_engine/ovr_render_system.h>
#include <wx_engine/ovr_mesh_component.h>
#include <wx_engine/ovr_camera_component.h>
#include <wx_engine/ovr_mesh_manager.h>

namespace ovr_core
{
	OvrSceneContext::OvrSceneContext()
	{
	}

	OvrSceneContext::~OvrSceneContext()
	{
		sequence_name_list.clear();
	}

	bool OvrSceneContext::Init(const OvrString& a_scene_unique_name)
	{
		Uninit();

		scene = ovr_engine::OvrEngine::GetIns()->CreateScene();
		if (nullptr == scene)
		{
			return false;
		}

		if (a_scene_unique_name.GetStringSize() > 0)
		{
			SetUniqueName(a_scene_unique_name);
		}

		scene->SetAmbientColor(OvrVector4(0.2f));

		scene_camera = new OvrSceneCamera;
		if (nullptr == scene_camera || !scene_camera->Init(scene))
		{
			return false;
		}
		
		return true;
	}

	void OvrSceneContext::Uninit() 
	{
		if (nullptr != scene_camera)
		{
			scene_camera->Uninit();
			delete scene_camera;
			scene_camera = nullptr;
		}

		if (nullptr != scene)
		{
			ovr_engine::OvrEngine::GetIns()->DestoryScene(scene);
			scene = nullptr;
		}
	}

	void OvrSceneContext::Update()
	{
		scene->UpdateScene();
	}

	void OvrSceneContext::Draw()
	{
		ovr_engine::OvrEngine::GetIns()->GetRenderSystem()->BeginScene(true, true, OvrColorValue(0.24f, 0.24f, 0.24f, 1.0f), \
			ovr_engine::OvrRenderSystem::Viewport(0, 0, scene_camera->GetWidth(), scene_camera->GetHeight()));

		scene->DrawScene();

		ovr_engine::OvrEngine::GetIns()->GetRenderSystem()->EndScene();
	}

	void OvrSceneContext::SetUniqueName(const OvrString& a_unique_name) 
	{ 
		scene->SetUniqueName(a_unique_name); 
	}
	const OvrString& OvrSceneContext::GetUniqueName()const 
	{ 
		return scene->GetUniqueName(); 
	}

	ovr_engine::OvrActor* OvrSceneContext::GetActorByUniqueName(const OvrString& a_unique_name)
	{
		return scene->GetActor(a_unique_name);
	}

	void OvrSceneContext::AppendSequence(const OvrString& a_sequence_unique_name)
	{
		bool is_found = false;
		auto name_it = sequence_name_list.begin();
		while (name_it != sequence_name_list.end())
		{
			if (*name_it == a_sequence_unique_name)
			{
				is_found = true;
			}
			name_it++;
		}

		if (!is_found)
		{
			sequence_name_list.push_back(a_sequence_unique_name);
		}
	}

	const std::map<OvrString, ovr_engine::OvrActor*>& OvrSceneContext::GetActorList()const 
	{ 
		return scene->GetActorMap(); 
	}
	ovr_engine::OvrActor* OvrSceneContext::GetRootActor() 
	{ 
		return scene->GetRoot(); 
	}
	ovr::uint32 OvrSceneContext::GetActorCount()const 
	{ 
		return scene->GetActorMap().size(); 
	}

	ovr_engine::OvrActor* OvrSceneContext::CreateActor(const OvrString& a_mesh_file_path)
	{
		//获取文件类型
		OvrString full_path = OvrCore::GetIns()->GetWorkDir() + a_mesh_file_path;
		ovr_asset::OvrArchiveAssetManager* serialize_manager = ovr_asset::OvrArchiveAssetManager::GetIns();
		ovr_asset::OvrAssetType asset_type = ovr_asset::OvrArchiveAssetManager::GetAssetType(full_path.GetStringConst());
		if (ovr_asset::kAssetMesh != asset_type)
		{
			return nullptr;
		}

		//创建mesh
		ovr_engine::OvrMesh* mesh = ovr_engine::OvrMeshManager::GetIns()->Load(a_mesh_file_path);
		if (nullptr == mesh)
		{
			return nullptr;
		}

		//创建Actor
		ovr_engine::OvrActor* new_actor = scene->CreateActor();
		if (nullptr == new_actor)
		{
			return nullptr;
		}

		ovr_engine::OvrMeshComponent* new_mesh_component = nullptr;
		if (mesh->HasSkeleton())
		{
			new_mesh_component = new_actor->AddComponent<ovr_engine::OvrSkinnedMeshComponent>();
		}
		else 
		{
			new_mesh_component = new_actor->AddComponent<ovr_engine::OvrStaticMeshComponent>();
		}
		
		if (nullptr == new_mesh_component)
		{
			scene->DestoryActor(new_actor);
			return nullptr;
		}
		new_mesh_component->SetMesh(mesh);

		//设置显示名
		OvrString display_name;
		OvrFileTools::GetFileNameFromPath(a_mesh_file_path.GetStringConst(), display_name);
		new_actor->SetDisplayName(display_name.GetStringConst());

		return new_actor;
	}

	ovr_engine::OvrActor* OvrSceneContext::CreateActor() 
	{
		return scene->CreateActor();
	}

	void OvrSceneContext::CopyActor(const OvrString& a_actor_id)
	{
		ovr_engine::OvrActor* src_actor = GetActorByUniqueName(a_actor_id);

		if (src_actor == nullptr)
			return;

		//if (src_actor->GetType() == ovr_engine::kObjectLight)
			return;	//灯光限制数量，暂不允许复制

		//ovr_engine::OvrObjectType type = src_actor->GetType();
		OvrTransform transform = src_actor->GetTransform();

		//ovr_engine::OvrActor* des_actor = scene->CreateActor(type);

		//if (des_actor != nullptr)
		{
			ovr_engine::OvrMesh* mesh = nullptr;
			//if (type == ovr_engine::kObjectMesh)
			{
				//ovr_engine::OvrMeshActor* Static_src_actor = (ovr_engine::OvrMeshActor*)src_actor;
				//mesh = Static_src_actor->GetMesh();

				if (mesh != nullptr)
				{
					//ovr_engine::OvrMeshActor* Static_des_actor = (ovr_engine::OvrMeshActor*)des_actor;
					//Static_des_actor->SetMesh(mesh);
				}
			}
			//else if (type == ovr_engine::kObjectVideoScreen)
			{
				//ovr_engine::OvrVideoScreenActor * video_3d_actor = (ovr_engine::OvrVideoScreenActor *)des_actor;
				//video_3d_actor->SetMovieType(ovr_engine::e_movie_type_left_right_3d_full);
			}

			//des_actor->SetTransform(transform);

			//des_actor->SetDisplayName(src_actor->GetDisplayName().GetStringConst());

			//scene_listener->OnAddActor(des_actor->GetUniqueName());

			ovr_engine::OvrActor* parent_actor = src_actor->GetParent();
			//parent_actor->AddChild(des_actor, true);

			//SetSelectedItem(des_actor->GetUniqueName().GetStringConst());
		}
	}
}
