﻿#include "headers.h"
#include <wx_core/ovr_scene_camera.h>
#include <wx_engine/ovr_actor.h>
#include <wx_engine/ovr_camera_component.h>
#include <wx_engine/ovr_engine.h>

namespace ovr_core 
{
	OvrSceneCamera::OvrSceneCamera() 
	{}
	OvrSceneCamera::~OvrSceneCamera() 
	{
	}

	//初始化
	bool OvrSceneCamera::Init(ovr_engine::OvrScene*	a_scene)
	{
		current_scene = a_scene;

		//创建漫游相机 
		roam_camera_actor = current_scene->CreateActor();
		if (nullptr == roam_camera_actor)
		{
			return false;
		}
		roam_camera_actor->SetDisplayName("Roam camera");
		roam_camera_actor->SetPosition(OvrVector3(0.0f, 1.8f, 0.0f));

		ovr_engine::OvrCameraComponent* camera_compent = roam_camera_actor->AddComponent<ovr_engine::OvrCameraComponent>();

		
		//设置漫游相机 为当前的相机
		SetCurrentCamera(roam_camera_actor);
		
		//为射线求交做准备
		ray_scene_query = current_scene->CreateRaySceneQuery();
		if (nullptr == ray_scene_query)
		{
			return false;
		}
		ray_scene_query->SetSortByDistance(true);
		
		return true;
	}

	void OvrSceneCamera::Uninit() 
	{
		if (nullptr != roam_camera_actor)
		{
			current_scene->DestoryActor(roam_camera_actor);
			roam_camera_actor = nullptr;
		}

		if (nullptr != ray_scene_query)
		{
			current_scene->DestorySceneQuery(ray_scene_query);
			ray_scene_query = nullptr;
		}
	}

	void OvrSceneCamera::Reshape(int a_new_width, int a_new_height)
	{
		ovr_engine::OvrEngine::GetIns()->Reshape(a_new_width, a_new_height);

		if (nullptr != current_camera_component)
		{
			current_camera_component->SetPerspective(90.0f*3.1415926535898f / 180.0f, float(a_new_width) / float(a_new_height), 0.01f, 2000.0f);
		}

		width = a_new_width;
		height = a_new_height;
	}

	bool OvrSceneCamera::SetCurrentCamera(ovr_engine::OvrActor* a_camera)
	{
		if (nullptr == current_scene || nullptr == a_camera)
		{
			return false;
		}

		if (current_camera == a_camera)
		{
			return true;
		}

		current_camera = a_camera;

		current_camera_component = current_camera->GetComponent<ovr_engine::OvrCameraComponent>();
		//设置当前的Camera
		current_scene->SetActiveCamera(current_camera);
		
		return true;
	}

	const ovr_engine::OvrRayIntersetResult* OvrSceneCamera::GetFocusActor() const
	{
		OvrRay3f ray;
		ray.SetDir(current_camera->GetQuaternion() * -OvrVector3::UNIT_Z);
		ray.SetOrigin(current_camera->GetPosition());

		//在整个场景中选择Mesh
		ray_scene_query->SetRay(ray);

		ovr_engine::RayIntersetResults& actor_queue = ray_scene_query->Execute();

		if (actor_queue.size() > 0)
		{
			return &(actor_queue[0]);
		}
		return nullptr;
	}

	//根据opengl屏幕坐标 获取射线
	OvrRay3f OvrSceneCamera::GeneratePickRay(float a_x_pos, float a_y_pos) 
	{
		return current_camera_component->GeneratePickRay(a_x_pos, a_y_pos);
	}

	const ovr_engine::RayIntersetResults& OvrSceneCamera::RayInterset(float a_x_pos, float a_y_pos, bool a_is_force )
	{
		OvrRay3f ray = GeneratePickRay(a_x_pos, a_y_pos);

		//在整个场景中选择Mesh
		ray_scene_query->SetRay(ray);
		return ray_scene_query->Execute(a_is_force);
	}

	void OvrSceneCamera::MoveForward()
	{
		MoveForwardBack(speed_move);
	}
	void OvrSceneCamera::MoveBackward()
	{
		MoveForwardBack(-speed_move);
	}
	void OvrSceneCamera::MoveLeft()
	{
		MoveLeftRight(speed_move);
	}
	void OvrSceneCamera::MoveRight()
	{
		MoveLeftRight(-speed_move);
	}
	void OvrSceneCamera::Yaw(float a_angle)
	{
		//支持当前相机(包含非漫游相机)
		current_camera->Yaw(-a_angle, ovr_engine::kSpaceParent);
	}

	void OvrSceneCamera::Pitch(float a_angle)
	{
		//支持当前相机(包含非漫游相机)
		current_camera->Pitch(a_angle, ovr_engine::kSpaceLocal);
	}

	bool OvrSceneCamera::MoveForwardBack(const float a_distance)
	{
		OvrVector3 right = (current_camera->GetQuaternion()*OvrVector3::UNIT_Y).cross(current_camera->GetQuaternion()*(-OvrVector3::UNIT_Z));
		OvrVector3 t_moving = (current_camera->GetQuaternion()*OvrVector3::UNIT_Y).cross(right);
		t_moving.normalize();
		current_camera->SetPosition(current_camera->GetPosition() - t_moving*a_distance);
		return true;
	}

	bool OvrSceneCamera::MoveLeftRight(const float a_distance)
	{
		OvrVector3 t_moving = (current_camera->GetQuaternion()*OvrVector3::UNIT_Y).cross(current_camera->GetQuaternion()*(-OvrVector3::UNIT_Z));

		t_moving.normalize();
		current_camera->SetPosition(current_camera->GetPosition() + t_moving*a_distance);
		return true;
	}
}