#include "headers.h"
#include <wx_core/ovr_core.h>
#include <wx_motion/ovr_motion.h>
#include <wx_plugin/wx_i_sound.h>
#include <wx_engine/ovr_engine.h>
#include <wx_engine/ovr_actor_axis.h>
#include <wx_plugin/wx_plugin_manager.h>

namespace ovr_core 
{
	static OvrCore*				sg_core = nullptr;

	bool OvrCore::Init()
	{
		if (nullptr == sg_core)
		{
			wx_plugin::wxPluginManager::GetIns()->LoadAll();

			sg_core = new OvrCore();
		}
		return (nullptr != sg_core);
	}
	void OvrCore::Uninit() 
	{
		if (nullptr != sg_core)
		{
			sg_core->Close();
			delete sg_core;
			sg_core = nullptr;

			wx_plugin::wxPluginManager::GetIns()->UnloadAll();
		}
	}
	OvrCore* OvrCore::GetIns() 
	{
		return sg_core;
	}

	bool OvrCore::Open()
	{	
		ovr_engine::OvrEngine::GetIns()->SetResourceLocation(work_dir);
		if (!ovr_engine::OvrEngine::GetIns()->Init())
		{
			return false;
		}
		
		ovr_engine::OvrActorAxis::GetIns()->Init();

		return true;
	}

	void OvrCore::Close() 
	{
		ovr_engine::OvrActorAxis::DelIns();
		ovr_engine::OvrEngine::GetIns()->Uninit();
	}

	void OvrCore::Update() 
	{
		//Sequence进行定时更新  帧率稍低
		wx_plugin::wxISoundManager* sound_manager = wx_plugin::wxISoundManager::Load();
		if (nullptr != sound_manager)
		{
			sound_manager->Update();
		}
	}
}
