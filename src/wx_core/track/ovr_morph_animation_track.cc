﻿#include "../headers.h"
#include "ovr_morph_animation_track.h"
#include <wx_core/ovr_sequence.h>
#include <wx_asset_manager/ovr_archive_pose.h>
#include <wx_motion/ovr_pose_animation_sequence.h>
#include <wx_asset_manager/ovr_archive_asset_manager.h>
#include <wx_engine/ovr_mesh_component.h>

namespace ovr_core
{
	bool OvrMorphClip::Open() 
	{
		assert(nullptr == current_animation_sequence);

		current_animation_sequence = new ovr_motion::OvrPoseAnimationSequence();
		if (nullptr == current_animation_sequence)
		{
			return false;
		}
		current_animation_sequence->SetAnimationPath(file_path.GetStringConst());
		if (!current_animation_sequence->Build())
		{
			Close();
			return false;
		}
		return true;
	}

	void OvrMorphClip::Close() 
	{
		if (nullptr != current_animation_sequence)
		{
			delete current_animation_sequence;
			current_animation_sequence = nullptr;
		}
	}

	ovr_asset::OvrMorphFrame* OvrMorphClip::GetFrame(ovr::uint64 a_current_time) 
	{
		if (nullptr != current_animation_sequence)
		{
			//计算在时间线上的相对位置  + clip自身的起始位置
			ovr::uint64 frames = a_current_time - timeline_position + start_time;
			float current_time = own_track->GetOwnSequence()->GetSeconds(frames);
			return current_animation_sequence->GetAnimation(current_time);
		}
		return nullptr;
	}

	OvrMorphAnimationTrack::OvrMorphAnimationTrack(OvrSequence* a_sequence)
		:OvrClipTrack(a_sequence) 
	{ 
		display_name = "表情动画"; 
	}
	OvrMorphAnimationTrack::~OvrMorphAnimationTrack() 
	{ 
	
	}

	ovr::uint64	OvrMorphAnimationTrack::GetAssetDuration(const OvrString& a_file_path) 
	{
		ovr_asset::OvrArchiveAsset* temp_asset = ovr_asset::OvrArchiveAssetManager::GetIns()->LoadAsset(a_file_path.GetStringConst());
		if (ovr_asset::kAssetPoseAnimation == temp_asset->GetType())
		{
			float duration = ((ovr_asset::OvrArchivePoseAnimation*) temp_asset)->GetDuration();

			return (ovr::uint64)(duration * owner_sequence->GetFPS());
		}
		return 0;
	}

	
	bool OvrMorphAnimationTrack::IsSupportDragIn(const OvrString& a_file_path)
	{
		OvrString file_suffix;
		OvrFileTools::GetFileSuffixFromPath(a_file_path, file_suffix);
		if (file_suffix == "pani")
		{
			return true;
		}
		return false;
	}

	OvrMorphAnimationGroupTrack::OvrMorphAnimationGroupTrack(OvrSequence* a_sequence, ovr_engine::OvrMeshComponent* a_actor)
		:OvrTrack(a_sequence)
		, mesh_actor(a_actor)
	{
		display_name = "表情动画组";
		track_support_list.clear();
		OvrString name = "表情动画";
		track_support_list.push_back(OvrTrackSupport(kTrackClip, true, name, "morph_animation"));
	}
	OvrMorphAnimationGroupTrack::~OvrMorphAnimationGroupTrack()
	{
	}
	OvrTrack* OvrMorphAnimationGroupTrack::NewTrack(OvrTrackType a_track_type, const OvrString& a_tag)
	{
		if (a_tag != "morph_animation")
		{
			return nullptr;
		}
		OvrMorphAnimationTrack* new_track = new OvrMorphAnimationTrack(owner_sequence);
		if (nullptr != new_track)
		{
			new_track->SetClipType(OvrClipInfo::kMorphAnimaition);
		}
		return new_track;
	}

	bool OvrMorphAnimationGroupTrack::Update()
	{
		UpdateFrame(owner_sequence->GetCurrentFrame());
		return true;
	}

	void OvrMorphAnimationGroupTrack::Seek(ovr::uint64 a_current_time)
	{
		UpdateFrame(a_current_time);
	}

	void OvrMorphAnimationGroupTrack::UpdateFrame(ovr::uint64 a_frame_time)
	{
		ovr_asset::OvrMorphFrame* motion_frame = nullptr;
		for (auto track_it : children)
		{
			OvrMorphClip* current_clip = (OvrMorphClip*)((OvrClipTrack*)track_it)->GetClipByTime(a_frame_time);
			if (nullptr == current_clip)
			{
				continue;
			}
			ovr_asset::OvrMorphFrame* temp_frame = current_clip->GetFrame(a_frame_time);
			if (nullptr == temp_frame)
			{
				continue;
			}
			if (nullptr == motion_frame)
			{
				motion_frame = temp_frame;
			}
		}

		if (nullptr != motion_frame)
		{
			mesh_actor->UpdateAnimation(motion_frame);
		}
	}
}