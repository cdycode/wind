﻿#include "../headers.h"
#include <wx_core/track/ovr_float_track.h>
#include <wx_core/ovr_sequence.h>

namespace ovr_core
{
	//需要更新当前值
	bool OvrFloatTrack::Update() 
	{
		Seek(owner_sequence->GetCurrentFrame());
		return true;
	}
	void OvrFloatTrack::Seek(ovr::uint64 a_current_time) 
	{
		//曲线数据生成
		current_value = pt_manager.GetValue(a_current_time);
		return ;

		if (frame_map.size() == 0)
		{
			current_value = default_value;
			return ;
		}

		//是否在当前的区间
		if (a_current_time >= current_begin_time && a_current_time < current_end_time)
		{
			current_value = LerpValue(a_current_time);
			return ;
		}

		//状态重置
		Reset();

		//有关键帧 或者在第一帧之前 最后一帧之后
		if (SetSpecificValue(a_current_time))
		{
			return;
		}

		//寻找前后的关键帧
		FindKeyRange(a_current_time);

		//计算当前值
		current_value = LerpValue(a_current_time);
	}

	void OvrFloatTrack::SetDefaultValue(float a_default_value)
	{ 
		default_value = a_default_value;
		current_value = a_default_value;
		pt_manager.SetDefaultValue(a_default_value);
	}

	void OvrFloatTrack::AddKeyFrame(const ovr::uint64 a_time, float a_key_value)
	{
		frame_map[a_time] = a_key_value;
		
		Reset();

		pt_manager.addPoint(a_time, a_key_value);
	}

	bool OvrFloatTrack::RmvKeyFrame(const ovr::uint64 a_time)
	{
		auto it = frame_map.find(a_time);
		if (it != frame_map.end())
		{
			frame_map.erase(it);

			Reset();

			pt_manager.RmvPoint(a_time);

			return true;
		}
		return false;
	}

	bool OvrFloatTrack::GetKeyFrame(ovr::uint64 a_key, float& a_key_value)const
	{
		auto it = frame_map.find(a_key);
		if (it != frame_map.end())
		{
			a_key_value = it->second;
			return true;
		}
		return false;
	}
	bool OvrFloatTrack::MoveKey(ovr::uint64 a_from, ovr::uint64 a_to)
	{
		if (frame_map.find(a_from) != frame_map.end() && frame_map.find(a_to) == frame_map.end())
		{
			frame_map[a_to] = frame_map[a_from];
			frame_map.erase(a_from);

			pt_manager.MovePoint(a_from, a_to);
			return true;
		}
		return false;
	}

	void OvrFloatTrack::FindKeyRange(ovr::uint64 a_current_time) 
	{
		//获取最近的
		for (auto frame_it = frame_map.begin(); frame_it != frame_map.end(); frame_it++)
		{
			if (a_current_time <= frame_it->first)
			{
				current_end_time = frame_it->first;
				current_end_value = frame_it->second;
				break;
			}
			current_begin_time = frame_it->first;
			current_begin_value = frame_it->second;
		}
	}

	bool OvrFloatTrack::SetSpecificValue(ovr::uint64 a_current_time)
	{
		//优先选择
		auto it = frame_map.find(a_current_time);
		if (it != frame_map.end())
		{
			current_value = it->second;
			return true;
		}

		//在第一个之前
		if (a_current_time <= frame_map.begin()->first )
		{
			current_value = frame_map.begin()->second;
			return true;
		}

		if (frame_map.size() < 1)
		{
			return false;
		}

		//在最后一个之后
		auto last_it = frame_map.end();
		last_it--;
		if (a_current_time >= last_it->first)
		{
			current_value = last_it->second;
			return true;
		}
		return false;
	}

	void OvrFloatTrack::Reset() 
	{
		current_begin_time = 0;
		current_end_time = 0;
		current_begin_value = default_value;
		current_end_value = default_value;
	}

	float OvrFloatTrack::LerpValue(ovr::uint64 a_frame_index)const 
	{
		//计算当前值
		float factor = (a_frame_index - current_begin_time)*1.0f / (current_end_time - current_begin_time);
		return current_begin_value * (1 - factor) + current_end_value * factor;
	}
}
