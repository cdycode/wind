﻿#include "../headers.h"
#include <wx_core/track/ovr_background_audio_track.h>
#include "ovr_audio_track.h"

namespace ovr_core
{
	OvrBackgrounAudioTrack::OvrBackgrounAudioTrack(OvrSequence* a_sequence) 
		:OvrTrack(a_sequence) 
	{ 
		display_name = "背景声"; 

		track_support_list.push_back(OvrTrackSupport(kTrackClip, true, "音频", "audio"));
	}

	OvrTrack*	OvrBackgrounAudioTrack::NewTrack(OvrTrackType a_track_type, const OvrString& a_tag)
	{
		if (a_tag == "audio_group")
		{
			//已经有audio group
			if (children.size() > 0)
			{
				return nullptr;
			}

			//先创建group
			OvrAudioGroupTrack* new_track = new OvrAudioGroupTrack(owner_sequence, nullptr);
			return new_track;
		}
		
		return nullptr;
	}
}