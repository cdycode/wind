﻿#ifndef OVR_CORE_OVR_MESH_COMPONENT_TRACK_H_
#define OVR_CORE_OVR_MESH_COMPONENT_TRACK_H_

#include <wx_core/track/ovr_track.h>

namespace ovr_engine 
{
	class OvrMeshComponent;
}

namespace ovr_core 
{
	class OvrFloatTrack;
	class OvrMeshComponentTrack : public OvrTrack 
	{
	public:
		OvrMeshComponentTrack(OvrSequence* a_sequence, ovr_engine::OvrMeshComponent* a_component) 
			: OvrTrack(a_sequence)
			,component(a_component)
		{}
		virtual ~OvrMeshComponentTrack() {}

		virtual bool	Init() override;

		virtual bool Update() override;
		virtual void Seek(ovr::uint64 a_current_time)override;

	protected:

		virtual OvrTrack*	NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) override;
		virtual void		DelTrack(OvrTrack* a_track) override;
	private:
		OvrTrack*	AddContainerTrack(const OvrString& a_sub_type);
		void		SetTransparent(float a_transparent);
	private:
		ovr_engine::OvrMeshComponent*	component = nullptr;
		OvrFloatTrack*					transparent_track = nullptr;
	};
}

#endif
