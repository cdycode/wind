﻿#ifndef OVR_CORE_OVR_TRANSFORM_GROUP_TRACK_H_
#define OVR_CORE_OVR_TRANSFORM_GROUP_TRACK_H_

#include <wx_core/track/ovr_track.h>

namespace ovr_core 
{
	class OvrVector3fTrack;
	class OvrTransfromGroupTrack : public OvrTrack 
	{
	public:
		OvrTransfromGroupTrack(OvrSequence* a_sequence, ovr_engine::OvrActor* a_actor);
		virtual ~OvrTransfromGroupTrack() {}

		virtual bool	Init() override;
		virtual void	Uninit() override;

		virtual bool Update() override;
		virtual void Seek(ovr::uint64 a_current_time) override;

		bool AddValue(ovr::uint64 a_time);
	protected:
		//创建一个track
		virtual OvrTrack*	NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) override;
		virtual void		DelTrack(OvrTrack* a_track) override;

	private:
		ovr_engine::OvrActor*	actor_ins = nullptr;

		OvrVector3fTrack*	postion_track = nullptr;
		OvrVector3fTrack*	rotation_track = nullptr;
		OvrVector3fTrack*	scale_track = nullptr;
	};
}

#endif
