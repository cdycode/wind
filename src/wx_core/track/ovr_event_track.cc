﻿#include "../headers.h"
#include <wx_core/track/ovr_event_track.h>
#include <wx_core/visual_node/i_ovr_node_cluster.h>
#include <wx_core/visual_node/i_ovr_node_manager.h>
#include <wx_core/ovr_sequence.h>
#include <wx_core/ovr_scene_context.h>

namespace ovr_core
{
	bool OvrEventTrack::Init() 
	{
		Uninit();

		return true;
	}

	void OvrEventTrack::Uninit() 
	{
	}

	bool OvrEventTrack::Update(ovr::uint64 a_current_time) 
	{
		if (owner_sequence->GetCurrentStatus() != kRenderPlay || key_frames.size() == 0)
		{
			return true;
		}

		//寻找事件
		auto frame_it = key_frames.find(a_current_time);
		if (frame_it == key_frames.end())
		{
			return false;
		}

		//找到后就执行
		IOvrNodeCluster* load_cluster = LoadCluster(frame_it->second);
		return DoEvent(frame_it->second);
	}

	void OvrEventTrack::Seek(ovr::uint64 a_current_time) 
	{
		//特殊处理 方便在某区域内都生效
		auto frame_it = key_frames.find(a_current_time);
		if (frame_it != key_frames.end())
		{
			DoEvent(frame_it->second);
			return ;
		}

		//遍历获取  找到比seek时间小 且最靠近Seek时间的事件
		OvrString will_do_event;
		for (auto event_it = key_frames.begin(); event_it != key_frames.end(); event_it++)
		{
			if (a_current_time < event_it->first)
			{
				//已经找到比Seek时间 还大的时间 所以没必要再比较下去
				break;
			}
			//保存可以执行的事件
			will_do_event = event_it->second;
		}
		if (will_do_event.GetStringSize() > 0)
		{
			DoEvent(will_do_event);
		}
		else 
		{
			//既然没有  需要清除当前的
			current_event_name.Clear();
		}
	}

	bool OvrEventTrack::AddKeyFrame(ovr::uint64 a_key, const OvrString& a_event_name)
	{ 
		//先查询是否存在 只有存在时 才添加
		if (IsHaveEvent(a_event_name))
		{
			key_frames[a_key] = a_event_name;
			return true;
		}
		return false;
	}

	void OvrEventTrack::RmvKeyFrame(ovr::uint64 a_key)
	{
		auto it = key_frames.find(a_key);
		if (it != key_frames.end())
		{
			key_frames.erase(it);
		}
	}

	bool OvrEventTrack::MoveKey(ovr::uint64 a_from, ovr::uint64 a_to)
	{
		if (key_frames.find(a_from) != key_frames.end() && key_frames.find(a_to) == key_frames.end())
		{
			key_frames[a_to] = key_frames[a_from];
			key_frames.erase(a_from);
			return true;
		}
		return false;
	}

	const OvrString& OvrEventTrack::GetKeyFrame(ovr::uint64 a_key)
	{
		auto it = key_frames.find(a_key);
		if (it != key_frames.end())
		{
			return it->second;
		}
		return OvrString::empty;
	}

	bool OvrEventTrack::CreateEvent(const OvrString& a_event_name) 
	{
		if (IsHaveEvent(a_event_name))
		{
			return false;
		}
		return IOvrNodeManager::GetIns()->CreateCluster(IOvrNodeCluster::kEvent, a_event_name);
	}

	const std::list<OvrString>& OvrEventTrack::GetEvents()
	{
		return IOvrNodeManager::GetIns()->GetClusterFileList(IOvrNodeCluster::kEvent);
	}

	bool OvrEventTrack::IsHaveEvent(const OvrString& a_event_name)
	{
		bool is_found = false;
		auto event_list = IOvrNodeManager::GetIns()->GetClusterFileList(IOvrNodeCluster::kEvent);
		for (auto it = event_list.begin(); it != event_list.end(); it++)
		{
			if (*it == a_event_name)
			{
				is_found = true;
				break;
			}
		}
		return is_found;
	}

	IOvrNodeCluster* OvrEventTrack::LoadCluster(const OvrString& a_file_name)
	{
		return IOvrNodeManager::GetIns()->LoadCluster(IOvrNodeCluster::kEvent, a_file_name, owner_sequence);
	}

	void OvrEventTrack::CloseCluster(IOvrNodeCluster* a_event_cluster)
	{
		IOvrNodeManager::GetIns()->CloseCluster(a_event_cluster);
	}

	void OvrEventTrack::SaveCluster(IOvrNodeCluster* a_event_cluster) 
	{
		IOvrNodeManager::GetIns()->SaveCluster( a_event_cluster);
	}

	bool OvrEventTrack::DoEvent(const OvrString& a_event_name)
	{
		//避免重复执行
		if (a_event_name == current_event_name)
		{
			return false;
		}

		current_event_name = a_event_name;
		//加载事件
		IOvrNodeCluster* load_cluster = LoadCluster(a_event_name);
		if (nullptr != load_cluster)
		{
			//执行并关闭
			load_cluster->BeginPlay();
			load_cluster->Update();
			load_cluster->EndPlay();

			CloseCluster(load_cluster);
		}
		return true;
	}
}
