﻿#ifndef OVR_PROJECT_OVR_SKELETAL_ANIMATION_TRACK_H_
#define OVR_PROJECT_OVR_SKELETAL_ANIMATION_TRACK_H_

#include <wx_core/track/ovr_clip.h>
#include <wx_core/track/ovr_clip_track.h>

namespace ovr_engine 
{
	class OvrSkinnedMeshComponent;
}
namespace ovr_motion 
{
	class OvrSkeletalAnimationSequence;
}
namespace ovr_asset 
{
	class OvrMotionFrame;
}

namespace ovr_core
{
	class OvrMotionClip : public OvrClipInfo 
	{
	public:
		OvrMotionClip(OvrTrack* a_track):own_track(a_track) {}
		virtual ~OvrMotionClip() {}

		//获取clip类型
		virtual Type GetType() { return kSkeletalAnimation; }

		virtual bool Open() override;
		virtual void Close() override;

		ovr_asset::OvrMotionFrame*	GetFrame(ovr::uint64 a_current_time);

	private:
		OvrTrack*			own_track = nullptr;
		ovr_motion::OvrSkeletalAnimationSequence*	current_animation_sequence = nullptr;

	};

	class OvrFloatTrack;
	class OvrSkeletalAnimationTrack : public OvrClipTrack
	{
	public:
		OvrSkeletalAnimationTrack(OvrSequence* a_sequence);
		virtual ~OvrSkeletalAnimationTrack();

		//获取文件的长度
		virtual ovr::uint64	GetAssetDuration(const OvrString& a_file_path)override;

		//是否能被拖入此轨道
		virtual bool		IsSupportDragIn(const OvrString& a_file_path);

		float			GetWeightValue()const;

	protected:
		//添加权重轨道
		virtual OvrTrack*	NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) override;
		virtual void		DelTrack(OvrTrack* a_track) override;
	private:
		OvrFloatTrack*		weight_track = nullptr;
	};

	class OvrSequence;
	class OvrSkeletalAnimationGroupTrack : public OvrTrack
	{
	public:
		OvrSkeletalAnimationGroupTrack(OvrSequence* a_sequence, ovr_engine::OvrSkinnedMeshComponent* a_component);
		virtual ~OvrSkeletalAnimationGroupTrack();

		virtual bool	Init()override;
		virtual void	Uninit() override;

		virtual bool Update() override;
		virtual void Seek(ovr::uint64 a_current_time) override;

	protected:
		virtual OvrTrack*		NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) override;
	private:
		void	UpdateFrame(ovr::uint64 a_current_frame);
	private:
		ovr_engine::OvrSkinnedMeshComponent*	skeletal_component = nullptr;
		ovr_asset::OvrMotionFrame*				curblend_frame = nullptr;
	};
}

#endif
