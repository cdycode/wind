﻿#include "../headers.h"
#include <wx_core/track/ovr_interactive_track.h>

#include <wx_core/ovr_sequence.h>
#include <wx_core/visual_node/i_ovr_node_manager.h>
#include <wx_core/visual_node/i_ovr_node_cluster.h>

#include <wx_core/ovr_scene_context.h>

namespace ovr_core
{
	OvrInterActiveTrack::OvrInterActiveTrack(OvrSequence* a_sequence) 
		:OvrClipTrack(a_sequence) 
	{ 
		display_name = "交互轨道"; 
	}

	OvrInterActiveTrack::~OvrInterActiveTrack()
	{
		CloseInteractive();
	}

	bool OvrInterActiveTrack::Update()
	{
		if (owner_sequence->GetCurrentStatus() != kRenderPlay || clip_map.size() == 0)
		{
			return true;
		}

		if (GetInteractive(owner_sequence->GetCurrentFrame(), true))
		{
			current_intercative_area->Update();
			return true;
		}

		return false;
	}

	void OvrInterActiveTrack::Play() 
	{
		if (nullptr != current_intercative_area)
		{
			current_intercative_area->BeginPlay();

			//设置循环播放
			owner_sequence->SetLoopRange(current_clip_begin_time, current_clip_end_time);
		}
	}

	void OvrInterActiveTrack::Pause() 
	{
		if (nullptr != current_intercative_area)
		{
			current_intercative_area->EndPlay();
		}
	}

	void OvrInterActiveTrack::Seek(ovr::uint64 a_current_time) 
	{
		if (clip_map.size() == 0)
		{
			return;
		}

		if (GetInteractive(a_current_time, false))
		{
			//Seek时 也停止播放
			current_intercative_area->EndPlay();
		}
	}

	//创建新的事件
	bool OvrInterActiveTrack::CreateCluster(const OvrString& a_event_name) 
	{
		return IOvrNodeManager::GetIns()->CreateCluster(IOvrNodeCluster::kArea, a_event_name);
	}

	//创建和销毁cluster
	IOvrNodeCluster* OvrInterActiveTrack::LoadCluster(const OvrString& a_file_name) 
	{
		return IOvrNodeManager::GetIns()->LoadCluster(IOvrNodeCluster::kArea, a_file_name, owner_sequence);
	}

	void OvrInterActiveTrack::CloseCluster(IOvrNodeCluster* a_event_cluster) 
	{
		IOvrNodeManager::GetIns()->CloseCluster(a_event_cluster);
	}

	void OvrInterActiveTrack::SaveCluster(IOvrNodeCluster* a_event_cluster) 
	{
		IOvrNodeManager::GetIns()->SaveCluster(a_event_cluster);
	}

	bool OvrInterActiveTrack::OpenInteractive()
	{
		current_intercative_area = IOvrNodeManager::GetIns()->LoadCluster(IOvrNodeCluster::kArea, current_file_path, owner_sequence);
		return (nullptr != current_intercative_area);
	}

	void OvrInterActiveTrack::CloseInteractive()
	{
		IOvrNodeManager::GetIns()->CloseCluster(current_intercative_area);
		current_intercative_area = nullptr;
	}

	bool	OvrInterActiveTrack::GetInteractive(ovr::uint64 a_current_time, bool a_is_play)
	{
		if (nullptr != current_intercative_area)
		{
			if (a_current_time >= current_clip_begin_time && a_current_time < current_clip_end_time)
			{
				return true;
			}
			else
			{
				current_intercative_area->EndPlay();
				CloseInteractive();
			}
		}

		if (!FindCurrentClip(a_current_time))
		{
			//未找到
			return false;
		}

		if (OpenInteractive()) 
		{
			if (a_is_play)
			{
				current_intercative_area->BeginPlay();

				//设置循环播放
				owner_sequence->SetLoopRange(current_clip_begin_time, current_clip_end_time);
			}
			return true;
		}
		return false;
	}
}
