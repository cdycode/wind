﻿#include "../headers.h"
#include "ovr_skeletal_animation_track.h"
#include <wx_core/ovr_sequence.h>
#include <wx_asset_manager/ovr_archive_skeletal_animation.h>
#include <wx_core/track/ovr_float_track.h>
#include <wx_motion/ovr_skeletal_animation_sequence.h>
#include <wx_asset_manager/ovr_archive_asset_manager.h>
#include <wx_asset_manager/ovr_motion_frame.h>
#include <wx_engine/ovr_skinned_mesh_component.h>

namespace ovr_core
{
	bool OvrMotionClip::Open() 
	{
		current_animation_sequence = new ovr_motion::OvrSkeletalAnimationSequence();
		if (nullptr == current_animation_sequence)
		{
			return false;
		}
		current_animation_sequence->SetAnimationPath(file_path.GetStringConst());
		if (!current_animation_sequence->Build())
		{
			Close();
			return false;
		}
		return true;
	}

	void OvrMotionClip::Close() 
	{
		if (nullptr != current_animation_sequence)
		{
			delete current_animation_sequence;
			current_animation_sequence = nullptr;
		}
	}

	ovr_asset::OvrMotionFrame*	OvrMotionClip::GetFrame(ovr::uint64 a_current_time)
	{
		if (nullptr != current_animation_sequence)
		{
			//计算在时间线上的相对位置  + clip自身的起始位置
			ovr::uint64 frames = a_current_time - timeline_position + start_time;
			float current_time = own_track->GetOwnSequence()->GetSeconds(frames);
			return current_animation_sequence->GetAnimation(current_time);
		}
		return nullptr;
	}

	OvrSkeletalAnimationTrack::OvrSkeletalAnimationTrack(OvrSequence* a_sequence)
		:OvrClipTrack(a_sequence) 
	{ 
		display_name = "骨骼动画";
		track_support_list.push_back(OvrTrackSupport(kTrackPoint, true, "权重", "weight"));
	}
	OvrSkeletalAnimationTrack::~OvrSkeletalAnimationTrack() 
	{ 
	}

	ovr::uint64	OvrSkeletalAnimationTrack::GetAssetDuration(const OvrString& a_file_path) 
	{
		ovr_asset::OvrArchiveAsset* temp_asset = ovr_asset::OvrArchiveAssetManager::GetIns()->LoadAsset(a_file_path.GetStringConst());
		if (ovr_asset::kAssetSkeletalAnimation == temp_asset->GetType())
		{
			float duration = ((ovr_asset::OvrArchiveSkeletalAnimation*) temp_asset)->GetDuration();

			return (ovr::uint64)(duration * owner_sequence->GetFPS());
		}
		return 0;
	}

	bool OvrSkeletalAnimationTrack::IsSupportDragIn(const OvrString& a_file_path) 
	{
		OvrString file_suffix;
		OvrFileTools::GetFileSuffixFromPath(a_file_path, file_suffix);
		if (file_suffix == "ani")
		{
			return true;
		}
		return false;
	}

	float OvrSkeletalAnimationTrack::GetWeightValue()const 
	{
		if (nullptr != weight_track)
		{
			return weight_track->GetCurrentValue();
		}
		return 1.0f;
	}

	//添加权重轨道
	OvrTrack* OvrSkeletalAnimationTrack::NewTrack(OvrTrackType a_track_type, const OvrString& a_tag)
	{
		if (a_tag == "weight")
		{
			OvrFloatTrack* weight_track = new OvrFloatTrack(owner_sequence);
			weight_track->SetDisplayName("权重");
			track_support_list.begin()->is_enable = false;

			return weight_track;
		}
		return nullptr;
	}

	void OvrSkeletalAnimationTrack::DelTrack(OvrTrack* a_track) 
	{
		if (a_track == weight_track)
		{
			weight_track = nullptr;
		}
	}

	OvrSkeletalAnimationGroupTrack::OvrSkeletalAnimationGroupTrack(OvrSequence* a_sequence, ovr_engine::OvrSkinnedMeshComponent* a_component)
		:OvrTrack(a_sequence)
		, skeletal_component(a_component)
	{
		display_name = "骨骼动画组";

		track_support_list.clear();
		OvrString name = "骨骼动画";
		track_support_list.push_back(OvrTrackSupport(kTrackClip, true, name, "skeletal_animation"));
	}
	OvrSkeletalAnimationGroupTrack::~OvrSkeletalAnimationGroupTrack()
	{
	}

	bool OvrSkeletalAnimationGroupTrack::Update()
	{
		OvrTrack::Update();

		UpdateFrame(owner_sequence->GetCurrentFrame());
		return true;
	}

	void OvrSkeletalAnimationGroupTrack::Seek(ovr::uint64 a_current_time)
	{
		OvrTrack::Seek(a_current_time);

		UpdateFrame(a_current_time);
	}

	bool OvrSkeletalAnimationGroupTrack::Init()
	{
		curblend_frame = new ovr_asset::OvrMotionFrame();
		return true;
	}

	void OvrSkeletalAnimationGroupTrack::Uninit()
	{
		if (curblend_frame != NULL)
		{
			curblend_frame->Uinit();
			delete curblend_frame;
			curblend_frame = nullptr;
		}
	}

	OvrTrack* OvrSkeletalAnimationGroupTrack::NewTrack(OvrTrackType a_track_type, const OvrString& a_tag)
	{
		if (a_tag != "skeletal_animation")
		{
			return nullptr;
		}
		OvrSkeletalAnimationTrack* new_track = new OvrSkeletalAnimationTrack(owner_sequence);
		if (nullptr != new_track)
		{
			new_track->SetClipType(OvrClipInfo::kSkeletalAnimation);
		}

		return new_track;
	}

	void OvrSkeletalAnimationGroupTrack::UpdateFrame(ovr::uint64 a_current_frame)
	{
		ovr_asset::OvrMotionFrame* motion_frame = nullptr;
		float motion_weight = 0.0f;
		for (auto track_it : children)
		{
			//获取当前的Clip
			OvrMotionClip* current_clip = (OvrMotionClip*)((OvrClipTrack*)track_it)->GetClipByTime(a_current_frame);
			if (nullptr == current_clip)
			{
				continue;
			}
			//获取Clip的当前帧
			ovr_asset::OvrMotionFrame* temp_frame = current_clip->GetFrame(a_current_frame);
			if (nullptr == temp_frame)
			{
				continue;
			}

			float weight = ((OvrSkeletalAnimationTrack*)track_it)->GetWeightValue();

			if (motion_frame == nullptr)
			{
				motion_frame = temp_frame;
				motion_weight = weight;
			}
			else
			{
				bool bresult = temp_frame->Blend(curblend_frame, weight, motion_frame, motion_weight);
				motion_frame = curblend_frame;
			}
		}

		if (nullptr != motion_frame)
		{
			skeletal_component->UpdateAnimation(motion_frame);
		}
	}
}