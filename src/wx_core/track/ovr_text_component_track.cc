﻿#include "../headers.h"
#include "ovr_text_component_track.h"
#include <wx_core/track/ovr_float_track.h>
#include <wx_core/ovr_sequence.h>
#include <wx_engine/ovr_text_component.h>

namespace ovr_core
{
	bool OvrTextClipTrack::Update()
	{
		UpdateClip(owner_sequence->GetCurrentFrame());
		return true;
	}
	void OvrTextClipTrack::Seek(ovr::uint64 a_current_time)
	{
		UpdateClip(a_current_time, true);
	}

	void OvrTextClipTrack::UpdateClip(ovr::uint64 a_current_time, bool a_is_seek /*= false*/)
	{
		OvrTextClip* text_clip = (OvrTextClip*)GetClipByTime(a_current_time);
		if (nullptr != text_clip)
		{
			if (text_clip->UpdateText(a_current_time))
			{
				text_component->SetText(text_clip->GetCurrentText());
			}
		}
		else if (a_is_seek)
		{
			//Seek状态 如果获取的textclip为空时 
			text_component->SetText("");
		}
	}

	bool OvrTextComponentTrack::Update() 
	{
		OvrTrack::Update();
		if (nullptr != transparent_track)
		{
			text_component->SetTransparentValue(transparent_track->GetCurrentValue());
		}
		return true;
	}

	void OvrTextComponentTrack::Seek(ovr::uint64 a_current_time) 
	{
		OvrTrack::Seek(a_current_time);
		if (nullptr != transparent_track)
		{
			text_component->SetTransparentValue(transparent_track->GetCurrentValue());
		}
	}

	OvrTrack* OvrTextComponentTrack::NewTrack(OvrTrackType a_track_type, const OvrString& a_tag)
	{
		OvrTrack* new_track = nullptr;
		if (a_tag == "transparent")
		{
			transparent_track = new OvrFloatTrack(owner_sequence);
			new_track = transparent_track;
			new_track->SetDisplayName("透明");
		}
		else if (a_tag == "text")
		{
			OvrTextClipTrack* text_clip_track = new OvrTextClipTrack(owner_sequence, text_component);
			text_clip_track->SetClipType(OvrClipInfo::kText);
			text_clip_track->SetDisplayName("文本");
			new_track = text_clip_track;
		}
		return new_track;
	}

	void OvrTextComponentTrack::DelTrack(OvrTrack* a_track)
	{
		if (a_track == transparent_track)
		{
			transparent_track = nullptr;
		}
	}
}