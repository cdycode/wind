﻿#include "../headers.h"
#include <wx_core/track/ovr_mark_track.h>

namespace ovr_core
{
	void OvrMarkTrack::AddKeyFrame(ovr::uint64 a_key, const OvrString& a_name)
	{
		mark_keys[a_key] = a_name;
	}

	void OvrMarkTrack::RmvKeyFrame(ovr::uint64 a_key)
	{
		if (mark_keys.find(a_key) != mark_keys.end())
		{
			mark_keys.erase(a_key);
		}
	}

	const OvrString& OvrMarkTrack::GetKeyFrame(ovr::uint64 a_key)
	{
		assert(mark_keys.find(a_key) != mark_keys.end());

		return mark_keys[a_key];
	}
}