﻿#include "../headers.h"
#include <wx_core/track/ovr_track.h>
#include <wx_core/ovr_sequence.h>
#include <wx_base/wx_time.h>

namespace ovr_core
{
	/*OvrUITrack::Type OvrUITrack::GetType(OvrTrackType a_type)
	{
		OvrUITrack::Type ui_type = kInvalid;
		switch (a_type)
		{
		case kTrackGroup:
			ui_type = kContainer;
			break;
		case kTrackSkeletalCapture:
		case kTrackMorphCapture:
		case kTrackClip:
		case kTrackInteractive:
			ui_type = kClip;
			break;
		case kTrackEvent:
		case kTrackTransform:
		case kTrackMark:
		case kTrackFloat:
			ui_type = kPoint;
			break;			
		default:
			break;
		}
		return ui_type;
	}*/

	OvrTrack::OvrTrack(OvrSequence* a_sequence) 
		:owner_sequence(a_sequence) 
	{
		display_name = "track";

		char temp[128] = { 0 };
		sprintf(temp, "track%llu", OvrTime::GetTicksNanos());
		unique_name.SetString(temp);
	}

	OvrTrack::~OvrTrack()
	{
		RmvAllTrack();
	}

	bool OvrTrack::Update()
	{
		OvrRenderStatus current_status = owner_sequence->GetCurrentStatus();
		for (auto it = children.begin(); it != children.end(); it++)
		{
			//只有live轨道和处于播放状体 才进行更新
			if ((*it)->IsLive() || current_status == kRenderPlay)
			{
				(*it)->Update();
			}
		}
		return true;
	}

	void OvrTrack::Seek(ovr::uint64 a_current_time)
	{
		for (auto track = children.begin(); track != children.end(); track++)
		{
			(*track)->Seek(a_current_time);
		}
	}

	void OvrTrack::Pause()
	{
		for (auto track = children.begin(); track != children.end(); track++)
		{
			(*track)->Pause();
		}
	}

	void OvrTrack::Play()
	{
		for (auto track = children.begin(); track != children.end(); track++)
		{
			(*track)->Play();
		}
	}

	OvrTrack* OvrTrack::AddTrack(OvrTrackType a_track_type, const OvrString& a_tag) 
	{ 
		OvrTrack* new_track = NewTrack(a_track_type, a_tag); 
		if (nullptr == new_track)
		{
			return nullptr;
		}

		new_track->SetTag(a_tag);
		if (new_track->Init())
		{
			AddChild(new_track);
			
		}
		else 
		{
			new_track->RmvAllTrack();
			delete new_track;
			new_track = nullptr;
		}
		return new_track;
	}

	bool OvrTrack::RmvTrack(OvrTrack* a_track)
	{
		if (nullptr == a_track)
		{
			return false;
		}

		DelTrack(a_track);

		if (RmvChild(a_track)) 
		{
			a_track->Uninit();
			a_track->RmvAllTrack();
			delete a_track;
			return true;
		}
		return false;
	}

	OvrTrack* OvrTrack::GetTrackByType(OvrTrackType a_track_type)
	{
		OvrTrack* found_track = nullptr;
		for (auto track_it : children)
		{
			if (a_track_type == track_it->GetTrackType())
			{
				found_track = track_it;
				break;
			}
		}
		return found_track;
	}

	void OvrTrack::GetTrackListByType(OvrTrackType a_track_type, std::list<OvrTrack*> a_track_list)
	{
		for (auto track_it : children)
		{
			if (a_track_type == track_it->GetTrackType())
			{
				a_track_list.push_back(track_it);
			}
		}
	}

	void OvrTrack::RmvAllTrack() 
	{
		for (auto it = children.begin(); it != children.end(); it++)
		{
			(*it)->Uninit();
			(*it)->RmvAllTrack();
			delete (*it);
		}
		children.clear();
	}


	//根据tag获取轨道
	OvrTrack* OvrTrack::GetTrackByTag(const OvrString& a_tag)
	{
		OvrTrack* found_track = nullptr;
		for (auto it = children.begin(); it != children.end(); it++)
		{
			if ((*it)->GetTag() == a_tag)
			{
				found_track = (*it);
				break;
			}
		}
		return found_track;
	}
}

