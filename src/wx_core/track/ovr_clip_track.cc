﻿#include "../headers.h"
#include <wx_core/track/ovr_clip_track.h>

namespace ovr_core
{
	OvrClipTrack::OvrClipTrack(OvrSequence* a_sequence)
		:OvrTrack(a_sequence)
	{}

	OvrClipTrack::~OvrClipTrack()
	{
	}

	void OvrClipTrack::Uninit() 
	{
		ResetClip();

		for (auto it = clip_map.begin(); it != clip_map.end(); it++)
		{
			delete it->second;
		}
		clip_map.clear();
	}

	bool OvrClipTrack::FindCurrentClip(ovr::uint64 a_current_time)
	{
		bool is_found = false;
		for (auto it : clip_map)
		{
			ovr::uint64 end_time = it.first + it.second->end_time - it.second->start_time;
			if ((it.first <= a_current_time) &&
				(end_time > a_current_time))
			{
				current_clip_start_time = it.second->start_time;
				current_clip_begin_time = it.first;
				current_clip_end_time = end_time;
				current_file_path = it.second->file_path;
				is_found = true;
				break;
			}
		}
		return is_found;
	}

	//重置clip
	void OvrClipTrack::ResetClip() 
	{
		current_clip_begin_time = 0;
		current_clip_end_time = 0;
		current_clip_start_time = 0;
		if (nullptr != current_clip)
		{
			current_clip->Close();
			current_clip = nullptr;
		}
	}

	bool OvrClipTrack::AddClip(const ovr::uint64 a_time, const OvrString& a_file_path, ovr::uint64 a_start_time, ovr::uint64 a_end_time)
	{
		//第一步判断 是否已经有clip存在
		auto clip_it = clip_map.find(a_time);
		if (clip_it != clip_map.end())
		{
			return false;
		}

		OvrClipInfo* new_clip = OvrClipInfo::CreateClip(clip_type, GetParent());
		new_clip->timeline_position = a_time;
		new_clip->file_path = a_file_path;
		new_clip->start_time = a_start_time;
		new_clip->end_time = a_end_time;
		clip_map[a_time] = new_clip;

		ResetClip();

		return true;
	}
	OvrClipInfo* OvrClipTrack::AddClip(const ovr::uint64 a_time)
	{
		//第一步判断 是否已经有clip存在
		auto clip_it = clip_map.find(a_time);
		if (clip_it != clip_map.end())
		{
			return nullptr;
		}

		OvrClipInfo* temp_clip = OvrClipInfo::CreateClip(clip_type, GetParent());
		temp_clip->timeline_position = a_time;
		clip_map[a_time] = temp_clip;

		ResetClip();

		return temp_clip;
	}
	bool OvrClipTrack::CloneClip(const ovr::uint64 a_time, OvrClipInfo* a_clip)
	{
		assert(0);
		if (clip_map.find(a_time) == clip_map.end())
		{
			OvrClipInfo* new_clip = OvrClipInfo::CreateClip(clip_type, GetParent());
			*new_clip = *a_clip;
			clip_map[a_time] = new_clip;
			return true;
		}
		return true;
	}

	OvrClipInfo* OvrClipTrack::Clip(ovr::uint64 a_key)
	{
		auto clip_it = clip_map.find(a_key);
		if (clip_it != clip_map.end())
		{
			return clip_it->second;
		}
		return nullptr;
	}

	bool OvrClipTrack::MoveClip(const ovr::uint64 a_old_time, const ovr::uint64 a_new_time)
	{
		if (a_old_time == a_new_time)
		{
			return true;
		}

		auto it = clip_map.find(a_old_time);
		if (it != clip_map.end() && clip_map.find(a_new_time) == clip_map.end())
		{
			OvrClipInfo* clip = it->second;
			clip_map.erase(it);
			clip->timeline_position = a_new_time;
			clip_map[a_new_time] = clip;

			ResetClip();
			return true;
		}

		return false;
	}

	void OvrClipTrack::RmvClip(const ovr::uint64 a_time)
	{
		auto it = clip_map.find(a_time);
		if (it != clip_map.end())
		{
			if (it->second == current_clip)
			{
				ResetClip();
			}

			delete it->second;
			clip_map.erase(it);
		}
	}

	OvrClipInfo* OvrClipTrack::GetClipByTime(ovr::uint64 a_frame_index)
	{
		if (nullptr != current_clip)
		{
			if (a_frame_index >= current_clip_begin_time && a_frame_index < current_clip_end_time)
			{
				return current_clip;
			}
			else
			{
				//资源释放
				ResetClip();
			}
		}

		for (auto it = clip_map.begin(); it != clip_map.end(); it++)
		{
			ovr::uint64 end_time = it->first + it->second->end_time - it->second->start_time;
			if ((it->first <= a_frame_index) && (end_time > a_frame_index))
			{
				current_clip_begin_time = it->first; it->second->start_time;
				current_clip_end_time = it->first + it->second->end_time - it->second->start_time;

				current_clip = it->second;

				current_clip->Open();

				break;
			}
		}
		return current_clip;
	}
}
