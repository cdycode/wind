﻿#ifndef OVR_PROJECT_OVR_MORPH_ANIMATION_TRACK_H_
#define OVR_PROJECT_OVR_MORPH_ANIMATION_TRACK_H_

#include <wx_core/track/ovr_clip_track.h>

namespace ovr_motion 
{
	class OvrPoseAnimationSequence;
}
namespace ovr_asset 
{
	class OvrMorphFrame;
}

namespace ovr_engine 
{
	class OvrMeshComponent;
}

namespace ovr_core
{
	class OvrMorphClip : public OvrClipInfo 
	{
	public:
		OvrMorphClip(OvrTrack* a_track):own_track(a_track) {}
		virtual ~OvrMorphClip() {}

		//获取clip类型
		virtual Type GetType() { return kMorphAnimaition; }

		virtual bool Open() override;
		virtual void Close() override;

		ovr_asset::OvrMorphFrame*	GetFrame(ovr::uint64 a_current_time);

	private:
		ovr_motion::OvrPoseAnimationSequence*	current_animation_sequence = nullptr;
		OvrTrack*	own_track = nullptr;
	};

	class OvrMorphAnimationTrack : public OvrClipTrack
	{
	public:
		OvrMorphAnimationTrack(OvrSequence* a_sequence);
		virtual ~OvrMorphAnimationTrack();

		virtual ovr::uint64	GetAssetDuration(const OvrString& a_file_path)override;

		virtual bool		IsSupportDragIn(const OvrString& a_file_path);

	};

	class  OvrMorphAnimationGroupTrack : public OvrTrack
	{
	public:
		OvrMorphAnimationGroupTrack(OvrSequence* a_sequence, ovr_engine::OvrMeshComponent* a_actor);
		virtual ~OvrMorphAnimationGroupTrack();

		virtual bool Update() override;
		virtual void Seek(ovr::uint64 a_current_time) override;

	protected:
		virtual OvrTrack*		NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) override;
	private:
		void	UpdateFrame(ovr::uint64 a_frame_time);
	private:
		ovr_engine::OvrMeshComponent*	mesh_actor = nullptr;
	};
}

#endif
