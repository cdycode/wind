#ifndef OVR_PROJECT_OVR_AMBIENT_LIGHT_TRACK_H_
#define OVR_PROJECT_OVR_AMBIENT_LIGHT_TRACK_H_

#include <wx_core/track/ovr_track.h>

namespace ovr_core
{
	class OvrFloatTrack;
	class OvrVector3fTrack;
	class OvrAmbientLightTrack : public OvrTrack 
	{
	public:
		OvrAmbientLightTrack(OvrSequence* a_sequence);
		virtual ~OvrAmbientLightTrack() {}

		virtual bool	Init() override;
		virtual void	Uninit() override;

		virtual bool Update() override;
		virtual void Seek(ovr::uint64 a_current_time) override;

	protected:
		virtual OvrTrack*	NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) override;

	private:
		void	UpdateValue();

	private:
		/*这些轨道不能动态删除*/
		OvrFloatTrack*		intensity_track = nullptr;

		OvrVector3fTrack*	rgb_track = nullptr;
	};
}

#endif