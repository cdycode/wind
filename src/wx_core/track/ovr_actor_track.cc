﻿#include "../headers.h"
#include <wx_engine/ovr_mesh_component.h>
#include <wx_engine/ovr_video_component.h>
#include <wx_engine/ovr_text_component.h>
#include <wx_core/track/ovr_actor_track.h>
#include "ovr_skeletal_animation_track.h"
#include <wx_core/component/ovr_audio_component.h>
#include "ovr_morph_animation_track.h"
#include "ovr_video_track.h"
#include "ovr_audio_track.h"
#include "ovr_transform_group_track.h"
#include "ovr_mesh_component_track.h"
#include "ovr_text_component_track.h"

#include <iostream>
#include <fstream>

namespace ovr_core
{
	OvrActorTrack::OvrActorTrack(OvrSequence* a_sequence) 
		:OvrTrack(a_sequence) 
	{ 
		display_name = "Actor"; 
	}

	OvrActorTrack::~OvrActorTrack()
	{
		owner_actor = nullptr;
	}

	const OvrString& OvrActorTrack::GetActorUniqueName() const 
	{
		if (nullptr != owner_actor)
		{
			return owner_actor->GetUniqueName();
		}
		return OvrString::empty;
	}

	const std::list<OvrTrackSupport>& OvrActorTrack::GetSupportTrackList() 
	{
		track_support_list.clear();
		if (nullptr == owner_actor)
		{
			return track_support_list;
		}

		//所有的Actor都有
		track_support_list.push_back(OvrTrackSupport(kTrackCommon, true, "变换", "transform"));

		const std::vector<ovr_engine::OvrComponent*>& all_component = owner_actor->GetAllComponents();
		for (ovr::uint32 index = 0; index < all_component.size(); index++)
		{
			switch (all_component[index]->GetType()) 
			{
			case ovr_engine::kComponentLight:
				//track_support_list.push_back(OvrTrackSupport(kTrackGroup, true, "灯光", "light"));
				break;
			case ovr_engine::kComponentCamera:
				//track_support_list.push_back(OvrTrackSupport(kTrackGroup, true, "灯光", "light"));
				break;
			case ovr_engine::kComponentBoxCollider:
				//track_support_list.push_back(OvrTrackSupport(kTrackGroup, true, "灯光", "light"));
				break;
			case ovr_engine::kComponentStaticMesh:
			case ovr_engine::kComponentSkinnedMesh:
				track_support_list.push_back(OvrTrackSupport(kTrackCommon, true, "网格", "mesh"));
				break;
			case ovr_engine::kComponentText:
				track_support_list.push_back(OvrTrackSupport(kTrackCommon, true, "文本", "text"));
				break;
			case ovr_engine::kComponentVideo:
				track_support_list.push_back(OvrTrackSupport(kTrackCommon, true, "视频", "video"));
				break;
			case kComponentAudio:
				track_support_list.push_back(OvrTrackSupport(kTrackCommon, true, "音频", "audio"));
				break;
			default:
				break;
			}
		}

		return track_support_list;
	}

	bool OvrActorTrack::IsCanAddTrack(OvrTrackType a_track_type) 
	{
		bool is_can = false;
		switch (a_track_type)
		{
		case kTrackCommon:
			//具有唯一性
			is_can = (nullptr == GetTrackByType(a_track_type));
			break;
		//case ovr_project::kTrackAudio:
		//	is_can = (owner_actor->GetType() == ovr_engine::kObjectVideoScreen || owner_actor->GetType() == ovr_engine::kObject3DAudio);
		//	break;
		//case ovr_project::kTrackVideo:
		//	is_can = (ovr_engine::kObjectVideoScreen == owner_actor->GetType() );
			break;
		default:
			break;
		}
		return is_can;
	}

	bool OvrActorTrack::IsSupportTrack(OvrTrackType a_track_type) 
	{
		bool is_support = false;
		for (auto it = track_support_list.begin(); it != track_support_list.end(); it++)
		{
			if (it->track_type == a_track_type)
			{
				is_support = it->is_enable;
				break;
			}
		}
		return is_support;
	}

	template <typename T> 
	OvrTrack* OvrActorTrack::AddGroupTrack(OvrTrackType a_group_track_type, OvrTrackType a_sub_track_type)
	{
		T* group_track = (T*)GetTrackByType(a_group_track_type);
		if (nullptr != group_track)
		{
			//返回添加的新轨道
			return group_track->AddTrack(a_sub_track_type, OvrString::empty);
		}

		//返回添加的子轨道的父轨道
		group_track = new T(owner_sequence, owner_actor);
		if (nullptr == group_track)
		{
			return nullptr;
		}
		if (nullptr == group_track->AddTrack(a_sub_track_type, OvrString::empty))
		{
			//第一个子轨道添加失败 就没必要再返回父轨道
			delete group_track;
			group_track = nullptr;
		}
		return group_track;
	}

	OvrTrack* OvrActorTrack::NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) 
	{
		OvrTrack* new_track = nullptr;
		if (a_track_type == kTrackCommon)
		{
			if (a_tag == "transform")
			{
				new_track = new OvrTransfromGroupTrack(owner_sequence, owner_actor);
				new_track->SetDisplayName("变换");
			}
			else if (a_tag == "mesh")
			{
				new_track = new OvrMeshComponentTrack(owner_sequence, owner_actor->GetComponent<ovr_engine::OvrMeshComponent>());
				new_track->SetDisplayName("网格");
			}
			else if (a_tag == "video")
			{
				new_track = new OvrVideoGroupTrack(owner_sequence, owner_actor->GetComponent<ovr_engine::OvrVideoComponent>());
				new_track->SetDisplayName("视频");
			}
			else if (a_tag == "audio")
			{
				new_track = new OvrAudioGroupTrack(owner_sequence, owner_actor);
				new_track->SetDisplayName("音频");
			}
			else if (a_tag == "text")
			{
				new_track = new OvrTextComponentTrack(owner_sequence, owner_actor->GetComponent<ovr_engine::OvrTextComponent>());
				new_track->SetDisplayName("文本");
			}
		}
		return new_track;
	}

}