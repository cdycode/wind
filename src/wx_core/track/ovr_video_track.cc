﻿#include "../headers.h"
#include "ovr_video_track.h"
#include <wx_core/ovr_sequence.h>
#include <wx_engine/ovr_video_component.h>
#include <wx_engine/ovr_media_texture.h>
#include <wx_engine/ovr_texture_manager.h>
#include <wx_plugin/wx_i_av_media.h>

namespace ovr_core
{
	bool OvrVideoClip::Open() 
	{
		assert(current_video_decoder == nullptr);

		wx_plugin::IOvrAVMedia* media_plugin = wx_plugin::IOvrAVMedia::Load();
		if (nullptr == media_plugin)
		{
			return false;
		}

		current_video_decoder = media_plugin->CreateVideoDecoder();
		if (nullptr == current_video_decoder)
		{
			return false;
		}
		OvrString abs_file_path = OvrCore::GetIns()->GetWorkDir() + file_path;
		if (current_video_decoder->Open(abs_file_path.GetStringConst()))
		{
			wx_plugin::StruVideoStreamInfo video_info;
			current_video_decoder->GetFileVideoInfo(video_info);
			current_video_decoder->SetVideoOutFormat(video_group_track->GetFrameWidth(), video_group_track->GetFrameHeight(), wx_plugin::kPixelRGB24);

			//解决从文件中裁剪出去
			float current_time = video_group_track->GetOwnSequence()->GetSeconds(start_time);
			current_video_decoder->Seek((ovr::uint64)(current_time * 1000));

			current_video_decoder->StartDecoding();
			current_video_decoder->Resume();
		}
		else
		{
			Close();
			return false;
		}

		return true;
	}

	void OvrVideoClip::Close() 
	{
		if (nullptr != current_video_decoder)
		{
			current_video_decoder->Close();
			wx_plugin::IOvrAVMedia* media_plugin = wx_plugin::IOvrAVMedia::Load();
			if (nullptr != media_plugin)
			{
				media_plugin->DestoryVideoDecoder(current_video_decoder);
				current_video_decoder = nullptr;
			}
			
		}
	}

	void OvrVideoClip::Play()
	{
		if (nullptr != current_video_decoder)
		{
			current_video_decoder->Resume();
		}
	}
	void OvrVideoClip::Seek(ovr::uint64 a_frame_index)
	{
		if (nullptr != current_video_decoder)
		{
			//计算在时间线上的相对位置  + clip自身的起始位置
			ovr::uint64 frames = a_frame_index - timeline_position + start_time;
			float current_time = video_group_track->GetOwnSequence()->GetSeconds(frames);
			current_video_decoder->Seek((ovr::uint64)(current_time * 1000));
		}
	}

	wx_plugin::wxFrameBuffer* OvrVideoClip::GetFrame()
	{
		if (nullptr != current_video_decoder)
		{
			return current_video_decoder->PopFrame();
		}
		return nullptr;
	}

	void OvrVideoClip::PushFrame(wx_plugin::wxFrameBuffer* a_frame)
	{
		if (nullptr != current_video_decoder)
		{
			current_video_decoder->PushFrame(a_frame);
		}
	}

	OvrVideoTrack::OvrVideoTrack(OvrSequence* a_sequence)
		:OvrClipTrack(a_sequence)
	{
		display_name = OvrString("视频");
	}

	OvrVideoTrack::~OvrVideoTrack()
	{
	}

	ovr::uint64	 OvrVideoTrack::GetAssetDuration(const OvrString& a_file_path) 
	{
		wx_plugin::IOvrAVMedia* media_plugin = wx_plugin::IOvrAVMedia::Load();
		if (nullptr == media_plugin)
		{
			return 0;
		}
		ovr::uint64 duration = 0;
		wx_plugin::wxIMediaInfo* media_info = media_plugin->CreateMediaInfo();
		OvrString abs_file_path = OvrCore::GetIns()->GetWorkDir() + a_file_path;
		if (media_info->Open(abs_file_path.GetStringConst()))
		{
			if (media_info->IsOwnVideo())
			{
				duration = (media_info->GetVideoInfo().length / 1000) * owner_sequence->GetFPS();
			}
		}
		media_plugin->DestoryMediaInfo(media_info);
		return duration;
	}

	bool OvrVideoTrack::IsSupportDragIn(const OvrString& a_file_path) 
	{
		OvrString file_suffix;
		OvrFileTools::GetFileSuffixFromPath(a_file_path, file_suffix);
		if (file_suffix == "avi" || file_suffix == "mp4" || file_suffix == "mpg")
		{
			return true;
		}
		return false;
	}

	OvrVideoGroupTrack::OvrVideoGroupTrack(OvrSequence* a_sequence, ovr_engine::OvrVideoComponent* a_component)
		:OvrTrack(a_sequence)
		, video_component(a_component)
	{
		display_name = "视频组";

		track_support_list.clear();
		OvrString name = "视频";
		track_support_list.push_back(OvrTrackSupport(kTrackClip, true, name, "video"));
	}
	OvrVideoGroupTrack::~OvrVideoGroupTrack()
	{}

	void OvrVideoGroupTrack::Uninit()
	{

	}

	OvrTrack* OvrVideoGroupTrack::NewTrack(OvrTrackType a_track_type, const OvrString& a_tag)
	{
		if (a_tag != "video")
		{
			return nullptr;
		}

		OvrVideoTrack* new_track = new OvrVideoTrack(owner_sequence);
		new_track->SetClipType(OvrClipInfo::kVideo);
		return new_track;
	}

	bool OvrVideoGroupTrack::Update()
	{
		if (nullptr == movie_texture)
		{
			if (nullptr != video_component)
			{
				movie_texture = ovr_engine::OvrTextureManager::GetIns()->Create(unique_name, ovr_engine::TEXTURE_TYPE_2D, frame_width, frame_height, ovr_engine::PF_R8G8B8, 0);
				video_component->GetMaterial()->SetMainTexture(movie_texture);
			}
		}

		//目前只支持一路视频
		wx_plugin::wxFrameBuffer* frame_buffer = nullptr;
		OvrVideoClip* current_clip = nullptr;
		for (auto track_it : children)
		{
			//先获取Clip
			OvrVideoClip* new_clip = (OvrVideoClip*)((OvrVideoTrack*)track_it)->GetClipByTime(owner_sequence->GetCurrentFrame());
			if (nullptr == new_clip)
			{
				continue;
			}

			//从Clip中获取数据帧
			wx_plugin::wxFrameBuffer* temp_buffer = new_clip->GetFrame();
			if (nullptr == temp_buffer)
			{
				continue;
			}
			if (nullptr == frame_buffer)
			{
				frame_buffer = temp_buffer;
				current_clip = new_clip;
			}
			else
			{
				new_clip->PushFrame(temp_buffer);
			}
		}

		if (nullptr != frame_buffer)
		{
			movie_texture->BlitFromMemory(frame_buffer->buffer, frame_buffer->data_size);
			current_clip->PushFrame(frame_buffer);
			return true;
		}
		return false;
	}

	void OvrVideoGroupTrack::Seek(ovr::uint64 a_current_time)
	{
		for (auto it = children.begin(); it != children.end(); it++)
		{
			OvrVideoClip* current_clip = (OvrVideoClip*)((OvrVideoTrack*)(*it))->GetClipByTime(a_current_time);
			if (nullptr == current_clip)
			{
				continue;
			}
			current_clip->Seek(a_current_time);
		}
	}

	void OvrVideoGroupTrack::Play()
	{
		for (auto it = children.begin(); it != children.end(); it++)
		{
			OvrVideoClip* current_clip = (OvrVideoClip*)((OvrVideoTrack*)(*it))->GetClipByTime(owner_sequence->GetCurrentFrame());
			if (nullptr == current_clip)
			{
				continue;
			}
			current_clip->Play();
		}
	}
}