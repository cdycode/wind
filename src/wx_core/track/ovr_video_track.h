﻿#ifndef OVR_PROJECT_OVR_VIDEO_TRACK_H_
#define OVR_PROJECT_OVR_VIDEO_TRACK_H_

#include <wx_core/track/ovr_clip_track.h>

namespace wx_plugin
{
	class wxIVideoDecoder;
	class IOvrAudioDecoder;
	class wxFrameBuffer;
}

namespace ovr_engine 
{
	class OvrVideoComponent;
}

namespace ovr_core
{
	class OvrVideoGroupTrack;
	class OvrVideoClip : public OvrClipInfo 
	{
	public:
		OvrVideoClip(OvrVideoGroupTrack* a_group_track)
		:video_group_track(a_group_track){}
		virtual ~OvrVideoClip() {}

		//获取clip类型
		virtual Type GetType() { return kVideo; }

		virtual bool Open() override;
		virtual void Close() override;

		void	Play();
		void	Seek(ovr::uint64 a_frame_index);

		wx_plugin::wxFrameBuffer*	GetFrame();
		void							PushFrame(wx_plugin::wxFrameBuffer* a_frame);

	private:
		wx_plugin::wxIVideoDecoder*	current_video_decoder = nullptr;
		OvrVideoGroupTrack*				video_group_track = nullptr;
	};

	class OvrVideoTrack : public OvrClipTrack
	{
	public:
		OvrVideoTrack(OvrSequence* a_sequence);
		virtual ~OvrVideoTrack();
		
		virtual ovr::uint64	GetAssetDuration(const OvrString& a_file_path)override;

		virtual bool		IsSupportDragIn(const OvrString& a_file_path);

	};

	class OvrSequence;
	class OvrVideoGroupTrack : public OvrTrack
	{
	public:
		OvrVideoGroupTrack(OvrSequence* a_sequence, ovr_engine::OvrVideoComponent* a_component);
		virtual ~OvrVideoGroupTrack();

		virtual void		Uninit()override;

		virtual bool Update()override;
		virtual void Seek(ovr::uint64 a_current_time) override;
		virtual void Play() override;

		ovr::uint32	GetFrameWidth()const { return frame_width; }
		ovr::uint32	GetFrameHeight()const { return frame_height; }

	protected:
		virtual OvrTrack*	NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) override;
	private:
		ovr_engine::OvrVideoComponent*	video_component = nullptr;

		ovr_engine::OvrTexture*	movie_texture = nullptr;
		ovr::uint32				frame_width = 1920;
		ovr::uint32				frame_height = 1080;
	};
}

#endif
