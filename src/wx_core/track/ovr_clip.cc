﻿#include "../headers.h"
#include <wx_core/track/ovr_clip.h>
#include <wx_core/ovr_sequence.h>
#include "ovr_skeletal_animation_track.h"
#include "ovr_morph_animation_track.h"
#include "ovr_video_track.h"
#include "ovr_audio_track.h"
#include <wx_plugin/wx_i_av_media.h>

namespace ovr_core
{
	OvrClipInfo* OvrClipInfo::CreateClip(Type a_clip_type, OvrTrack* a_parent_track)
	{
		OvrClipInfo* new_clip = nullptr;
		switch (a_clip_type)
		{
		case OvrClipInfo::kInvalid:
			new_clip = new OvrClipInfo;
			break;
		case OvrClipInfo::kSkeletalAnimation:
			new_clip = new OvrMotionClip(a_parent_track);
			break;
		case OvrClipInfo::kMorphAnimaition:
			new_clip = new OvrMorphClip(a_parent_track);
			break;
		case OvrClipInfo::kText:
			new_clip = new OvrTextClip(a_parent_track);
			break;
		case OvrClipInfo::kVideo:
			new_clip = new OvrVideoClip((OvrVideoGroupTrack*)a_parent_track);
			break;
		case OvrClipInfo::kAudio:
			new_clip = new OvrAudioClip((OvrAudioGroupTrack*)a_parent_track);
			break;
		default:
			break;
		}
		return new_clip;
	}

	OvrTextClip::OvrTextClip(OvrTrack* a_own_track)
		:own_track(a_own_track)
	{
		
	}

	bool OvrTextClip::Open() 
	{
		if (text.GetStringSize() == 0)
		{
			text = " ";	//至少有一个字符
		}
		OvrString::GbkToUnicode(text, unicode_text);
		current_show.Clear();
		current_show_num = -1;
		
		return true;
	}

	void OvrTextClip::Close() 
	{
		unicode_text.clear();
	}

	bool OvrTextClip::UpdateText(ovr::uint64 a_frame_index) 
	{
		if (show_frames == 0)
		{
			//不进行逐字显示
			if (current_show.GetStringSize() > 0)
			{
				//已经显示过
				return false;
			}
			OvrString::UnicodeToUtf8(unicode_text, current_show);
			return true;
		}

		//需要进行逐字显示
		ovr::uint64 diff_time = a_frame_index - timeline_position;
		ovr::uint32 world_cnt = (ovr::uint32)(diff_time / show_frames);

		//纠正world_cnt的取值
		if (world_cnt > unicode_text.size())
		{
			world_cnt = unicode_text.size();
		}
		else if (world_cnt == 0)
		{
			//第一次 至少显示一个
			world_cnt = 1;
		}

		//显示的数  发生改变
		if (world_cnt != current_show_num)
		{
			std::wstring sub_text = unicode_text.substr(0, world_cnt);
			OvrString::UnicodeToUtf8(sub_text, current_show);

			current_show_num = world_cnt;
			return true;
		}
		
		//显示的文本 没有变化
		return false;
	}

	void OvrTextClip::SetShowTime(float a_show_time)
	{
		show_time = a_show_time;
		show_frames = own_track->GetOwnSequence()->GetFrames(show_time);
	}

	bool OvrAudioClip::Open()
	{
		wx_plugin::IOvrAVMedia* media_plugin = wx_plugin::IOvrAVMedia::Load();
		if (nullptr == media_plugin)
		{
			return false;
		}

		current_audio_decoder = media_plugin->CreateAudioDecoder();
		if (nullptr == current_audio_decoder)
		{
			return false;
		}
		OvrString abs_file_path = OvrCore::GetIns()->GetWorkDir() + file_path;
		if (current_audio_decoder->Open(abs_file_path.GetStringConst()))
		{
			wx_plugin::StruAudioStreamInfo audio_info;
			current_audio_decoder->GetFileAudioInfo(audio_info);
			current_audio_decoder->SetAudioOutFormat(own_group_track->GetChannel(), \
				own_group_track->GetSampleBit(), own_group_track->GetSampleRate(), own_group_track->GetFrameTime());

			//解决从文件中裁剪出去
			float current_time = own_group_track->GetOwnSequence()->GetSeconds(start_time);
			current_audio_decoder->Seek((ovr::uint64)(current_time * 1000));
			current_audio_decoder->StartDecoding();
			current_audio_decoder->Resume();
		}
		else
		{
			current_audio_decoder->Close();
			media_plugin->DestoryAudioDecoder(current_audio_decoder);
			current_audio_decoder = nullptr;
		}

		return (nullptr != current_audio_decoder);
	}
	void OvrAudioClip::Close()
	{
		if (nullptr != current_audio_decoder)
		{
			current_audio_decoder->Close();
			wx_plugin::IOvrAVMedia* media_plugin = wx_plugin::IOvrAVMedia::Load();
			if (nullptr != media_plugin)
			{
				media_plugin->DestoryAudioDecoder(current_audio_decoder);
				current_audio_decoder = nullptr;
			}
		}
	}

	void OvrAudioClip::Play()
	{
		if (nullptr != current_audio_decoder)
		{
			current_audio_decoder->Resume();
		}
	}
	void OvrAudioClip::Seek(ovr::uint64 a_frame_index)
	{
		if (nullptr != current_audio_decoder)
		{
			//计算在时间线上的相对位置  + clip自身的起始位置
			ovr::uint64 frames = a_frame_index - timeline_position + start_time;
			float current_time = own_group_track->GetOwnSequence()->GetSeconds(frames);
			current_audio_decoder->Seek((ovr::uint64)(current_time * 1000));
		}
	}

	wx_plugin::wxFrameBuffer* OvrAudioClip::GetFrame()
	{
		if (nullptr != current_audio_decoder)
		{
			return current_audio_decoder->PopFrame();
		}
		return nullptr;
	}

	void OvrAudioClip::PushFrame(wx_plugin::wxFrameBuffer* a_frame)
	{
		if (nullptr != current_audio_decoder)
		{
			current_audio_decoder->PushFrame(a_frame);
		}
	}
}