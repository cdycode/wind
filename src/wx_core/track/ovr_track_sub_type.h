#ifndef OVR_PROJECT_OVR_TRACK_SUB_TYPE_H_
#define OVR_PROJECT_OVR_TRACK_SUB_TYPE_H_

//�����ַ���
namespace ovr_core
{
	//OvrClipTrack ��SubType
	enum OvrClipTrackSubType
	{
		kClipInvalid = 0,
		kClipSkeletalAnimation,
		kClipMorphAnimation,
		kClipText,
		kClipVideo,
		kClipAudio,
	};

	enum OvrContainerTrackSubType
	{
		kContainerInvalid = 0,
		kContainerSkeletalAnimation,
		kContainerMorphAnimation,
		kContainerVideo,
		kContainerAudio,
	};
}

#endif