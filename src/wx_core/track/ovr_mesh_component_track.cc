﻿#include "../headers.h"
#include <wx_engine/ovr_mesh_component.h>
#include <wx_engine/ovr_mesh.h>
#include <wx_engine/ovr_skinned_mesh_component.h>
#include "ovr_mesh_component_track.h"
#include <wx_core/track/ovr_float_track.h>
#include "ovr_morph_animation_track.h"
#include "ovr_skeletal_animation_track.h"

namespace ovr_core 
{
	bool OvrMeshComponentTrack::Init() 
	{
		ovr_engine::OvrMesh* mesh_ins = component->GetMesh();
		if (nullptr == mesh_ins)
		{
			return true;
		}

		track_support_list.push_back(OvrTrackSupport(kTrackPoint, true, "透明", "transparent"));

		//表情相关
		if (component->HasPose())
		{
			track_support_list.push_back(OvrTrackSupport(kTrackClip, true, "表情动画", "morph_animation"));
		}
		//骨骼相关
		if (mesh_ins->HasSkeleton())
		{
			track_support_list.push_back(OvrTrackSupport(kTrackClip, true, "骨骼动画", "skeletal_animation"));
		}

		return true;
	}

	bool OvrMeshComponentTrack::Update() 
	{
		OvrTrack::Update();
		if (nullptr != transparent_track)
		{
			SetTransparent(transparent_track->GetCurrentValue());
		}
		return true;
	}
	void OvrMeshComponentTrack::Seek(ovr::uint64 a_current_time) 
	{
		OvrTrack::Seek(a_current_time);
		if (nullptr != transparent_track)
		{
			SetTransparent(transparent_track->GetCurrentValue());
		}
	}

	OvrTrack* OvrMeshComponentTrack::NewTrack(OvrTrackType a_track_type, const OvrString& a_tag)
	{
		if (a_track_type == kTrackCommon)
		{
			return AddContainerTrack(a_tag);
		}

		OvrTrack* new_track = nullptr;
		if (a_tag == "transparent")
		{
			transparent_track = new OvrFloatTrack(owner_sequence);
			new_track = transparent_track;
		}
		else if (a_tag == "morph_animation")
		{
			OvrTrack* morph_animation_group = GetTrackByTag("morph_animation_group");
			if (nullptr != morph_animation_group)
			{
				//group已经存在 可以进行添加
				new_track = morph_animation_group->AddTrack(kTrackClip, a_tag);
			}
			else
			{
				//group不存在 先创建group 再进行添加
				new_track = AddContainerTrack("morph_animation_group");
				new_track->AddTrack(kTrackClip, a_tag);
			}
		}
		else if (a_tag == "skeletal_animation")
		{
			OvrTrack* skeletal_animation_group = GetTrackByTag("skeletal_animation_group");
			if (nullptr != skeletal_animation_group)
			{
				//group已经存在 可以进行添加
				new_track = skeletal_animation_group->AddTrack(kTrackClip, a_tag);
			}
			else
			{
				//group不存在 先创建group 再进行添加
				new_track = AddContainerTrack("skeletal_animation_group");
				new_track->AddTrack(kTrackClip, a_tag);
			}
		}

		return new_track;
	}

	void OvrMeshComponentTrack::DelTrack(OvrTrack* a_track) 
	{
		if (a_track == transparent_track)
		{
			transparent_track = nullptr;
		}
	}

	OvrTrack* OvrMeshComponentTrack::AddContainerTrack(const OvrString& a_sub_type)
	{
		OvrTrack* new_track = nullptr;
		if (a_sub_type == "skeletal_animation_group")
		{
			ovr_engine::OvrSkinnedMeshComponent* skinned_component = dynamic_cast<ovr_engine::OvrSkinnedMeshComponent*>(component);
			if (nullptr != skinned_component)
			{
				new_track = new OvrSkeletalAnimationGroupTrack(owner_sequence, skinned_component);
			}
		}
		else if (a_sub_type == "morph_animation_group")
		{
			new_track = new OvrMorphAnimationGroupTrack(owner_sequence, component);
		}
		return new_track;
	}

	void OvrMeshComponentTrack::SetTransparent(float a_transparent)
	{
		//给材质赋值
		ovr::uint32 material_num = component->GetMaterialCount();
		for (ovr::uint32 index = 0; index < material_num; index++)
		{
			ovr_engine::OvrMaterial* current_material = component->GetMaterial(index);
			if (nullptr != current_material)
			{	//"transparent"
				//OvrVector4 diffuse_color(a_transparent, a_transparent, a_transparent, a_transparent);
				OvrVector4 diffuse_color(a_transparent, a_transparent, a_transparent, 1.0f);
				current_material->GetProgramParameters()->SetNameConstant("DiffuseColor", diffuse_color);
			}
		}
	}
}