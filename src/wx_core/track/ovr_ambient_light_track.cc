#include "../headers.h"
#include "ovr_ambient_light_track.h"
#include <wx_core/track/ovr_float_track.h>
#include <wx_core/ovr_scene_context.h>
#include "ovr_vector3f_track.h"
#include <wx_core/ovr_sequence.h>

namespace ovr_core
{
	OvrAmbientLightTrack::OvrAmbientLightTrack(OvrSequence* a_sequence) 
		:OvrTrack(a_sequence) 
	{
		display_name = "环境光";
		track_support_list.clear();
	}

	bool OvrAmbientLightTrack::Init() 
	{
		AddTrack(kTrackCommon, "color");
		AddTrack(kTrackPoint, "intensity");
		return true;
	}

	void OvrAmbientLightTrack::Uninit() 
	{
		//实际删除 会在析构函数中调用
		intensity_track = nullptr;

		rgb_track = nullptr;
	}

	bool OvrAmbientLightTrack::Update() 
	{
		OvrTrack::Update();
		//if (owner_sequence->GetCurrentStatus() == kRenderPlay)
		{
			UpdateValue();
		}
		return true;
	}

	void OvrAmbientLightTrack::Seek(ovr::uint64 a_current_time) 
	{
		OvrTrack::Seek(a_current_time);

		UpdateValue();
	}

	OvrTrack* OvrAmbientLightTrack::NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) 
	{
		//设置缺省值
		ovr_engine::OvrScene* current_scene = owner_sequence->GetScene()->GetScene();
		OvrVector4 current_color = current_scene->GetAmbientColor();

		OvrTrack* new_track = nullptr;
		if (a_tag == "color")
		{
			rgb_track = new OvrVector3fTrack(owner_sequence);
			rgb_track->Init();
			rgb_track->SetDisplayName("颜色");
			rgb_track->SetSubTrackTag("red", "green", "blue");
			rgb_track->SetSubTrackDisplayName("红色", "绿色", "蓝色");
			rgb_track->SetDefaultValue(current_color[0], current_color[1], current_color[2]);
			new_track = rgb_track;
		}
		else if (a_tag == "intensity")
		{
			intensity_track = new OvrFloatTrack(owner_sequence);
			intensity_track->SetDisplayName("强度");
			intensity_track->SetDefaultValue(current_color[3]);
			new_track = intensity_track;
		}
		
		return new_track;
	}

	void OvrAmbientLightTrack::UpdateValue() 
	{
		//获取当前环境光信息
		ovr_engine::OvrScene* current_scene = owner_sequence->GetScene()->GetScene();
		OvrVector4 current_color = current_scene->GetAmbientColor();

		//rgb
		const OvrVector3& rgb_value = rgb_track->GetValue();
		current_color[0] = rgb_value[0];
		current_color[1] = rgb_value[1];
		current_color[2] = rgb_value[2];

		//设置环境光
		current_color[3] = intensity_track->GetCurrentValue();
		current_scene->SetAmbientColor(current_color);
	}
}

