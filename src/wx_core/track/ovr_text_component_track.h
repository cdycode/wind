﻿#ifndef OVR_PROJECT_OVR_TEXT_ACTOR_TRACK_H_
#define OVR_PROJECT_OVR_TEXT_ACTOR_TRACK_H_

#include <wx_core/track/ovr_actor_track.h>

namespace ovr_engine
{
	class OvrTextComponent;
}

namespace ovr_core
{
	/*text actor 的透明轨道*/
	class OvrSequence;
	
	/*文本的clip轨道*/
	class OvrTextClipTrack : public OvrClipTrack
	{
	public:
		OvrTextClipTrack(OvrSequence* a_sequence, ovr_engine::OvrTextComponent* a_text_component)
			:OvrClipTrack(a_sequence)
			, text_component(a_text_component)
		{}
		virtual ~OvrTextClipTrack() {}

		virtual bool Update() override;
		virtual void Seek(ovr::uint64 a_current_time) override;

	private:
		void UpdateClip(ovr::uint64 a_current_time, bool a_is_seek = false);

	private:
		ovr_engine::OvrTextComponent*	text_component = nullptr;
	};

	/*文本的actor轨道*/
	class OvrFloatTrack;
	class OvrTextComponentTrack : public OvrTrack 
	{
	public:
		OvrTextComponentTrack(OvrSequence* a_sequence, ovr_engine::OvrTextComponent* a_text_component)
			:OvrTrack(a_sequence)
			,text_component(a_text_component){}
		virtual ~OvrTextComponentTrack() {}

		virtual bool Update() override;
		virtual void Seek(ovr::uint64 a_current_time) override;
		
	protected:
		//创建一个track
		virtual OvrTrack*	NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) override;
		virtual void		DelTrack(OvrTrack* a_track) override;

	private:
		ovr_engine::OvrTextComponent*	text_component = nullptr;
		OvrFloatTrack*					transparent_track = nullptr;
	};
}

#endif
