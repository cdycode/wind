﻿#include "../headers.h"
#include "ovr_audio_track.h"
#include <wx_core/ovr_sequence.h>
#include <wx_core/track/ovr_float_track.h>
#include <wx_plugin/wx_i_sound.h>
#include <wx_plugin/wx_i_av_media.h>
namespace ovr_core
{
	OvrAudioTrack::OvrAudioTrack(OvrSequence* a_sequence)
		:OvrClipTrack(a_sequence)
	{
		display_name = OvrString("音频");
		track_support_list.push_back(OvrTrackSupport(kTrackPoint, true, "权重", "weight"));
	}

	OvrAudioTrack::~OvrAudioTrack()
	{
	}

	ovr::uint64	OvrAudioTrack::GetAssetDuration(const OvrString& a_file_path) 
	{
		wx_plugin::IOvrAVMedia* media_plugin = wx_plugin::IOvrAVMedia::Load();
		if (nullptr == media_plugin)
		{
			return 0;
		}

		ovr::uint64 duration = 0;
		wx_plugin::wxIMediaInfo* media_info = media_plugin->CreateMediaInfo();
		OvrString abs_file_path = OvrCore::GetIns()->GetWorkDir() + a_file_path;
		if (media_info->Open(abs_file_path.GetStringConst()))
		{
			if (media_info->IsOwnAudio())
			{
				duration = (media_info->GetAudioInfo().length / 1000) * owner_sequence->GetFPS();
			}
		}
		media_plugin->DestoryMediaInfo(media_info);
		return duration;
	}

	bool OvrAudioTrack::IsSupportDragIn(const OvrString& a_file_path) 
	{
		OvrString file_suffix;
		OvrFileTools::GetFileSuffixFromPath(a_file_path, file_suffix);
		if (file_suffix == "wav" || file_suffix == "mp3")
		{
			return true;
		}
		return false;
	}
	//添加权重轨道
	OvrTrack* OvrAudioTrack::NewTrack(OvrTrackType a_track_type, const OvrString& a_tag)
	{
		if (a_tag == "weight")
		{
			weight_track = new OvrFloatTrack(owner_sequence);
			
			weight_track->SetDisplayName("权重");
			
			track_support_list.begin()->is_enable = false;

			return weight_track;
		}
		return nullptr;
	}

	void OvrAudioTrack::DelTrack(OvrTrack* a_track) 
	{
		if (a_track == weight_track)
		{
			weight_track = nullptr;
		}
	}

	float OvrAudioTrack::GetWeightValue()const
	{
		if (nullptr != weight_track)
		{
			return weight_track->GetCurrentValue();
		}
		return 1.0f;
	}

	OvrAudioGroupTrack::OvrAudioGroupTrack(OvrSequence* a_sequence, ovr_engine::OvrActor* a_actor)
		:OvrTrack(a_sequence)
		, own_actor(a_actor)
	{
		display_name = "音频组";
		track_support_list.push_back(OvrTrackSupport(kTrackClip, true, "音频", "audio"));
	}
	OvrAudioGroupTrack::~OvrAudioGroupTrack()
	{
		Uninit();
	}

	bool OvrAudioGroupTrack::Init()
	{
		Uninit();

		wx_plugin::wxISoundManager* sound_manager = wx_plugin::wxISoundManager::Load();
		if (nullptr == sound_manager)
		{
			return false;
		}

		int tempBuffsize = default_sample_rate * default_frame_ms / 1000 * default_channel* (default_sample_bit / 8);
		current_frameBuffer = new wx_plugin::wxFrameBuffer();
		current_frameBuffer->Open(tempBuffsize * 2);
		current_frameBuffer->data_size = current_frameBuffer->buffer_size;

		current_sound = sound_manager->CreateSound(default_channel, default_sample_rate, default_sample_bit, 2 * default_frame_ms, own_actor != nullptr ? true : false);

		current_sound->SetFillAudioDataFunc(this, &OvrAudioGroupTrack::FillAudioDataCallback);
		return true;
	}

	void OvrAudioGroupTrack::Uninit()
	{
		if (nullptr != current_sound)
		{
			wx_plugin::wxISoundManager* sound_manager = wx_plugin::wxISoundManager::Load();
			if (nullptr != sound_manager)
			{
				sound_manager->DestorySound(current_sound);
				current_sound = nullptr;
			}
			
		}

		for (auto track : children)
		{
			track->Uninit();
		}
		if (current_frameBuffer != nullptr)
		{
			current_frameBuffer->Close();
			current_frameBuffer = nullptr;
		}
	}

	bool OvrAudioGroupTrack::Update()
	{
		bool is_update = false;
		OvrTrack::Update();

		if (owner_sequence->GetCurrentStatus() == kRenderSeek)
		{
			wx_plugin::wxFrameBuffer* frame_buffer = GetCurrentFrame();

			if (nullptr != frame_buffer)
			{
				current_sound->FeedSoundData(frame_buffer->buffer, frame_buffer->data_size);

				is_update = true;
			}
		}
		else if (owner_sequence->GetCurrentStatus() == kRenderPlay)
		{
			if (own_actor != nullptr)
			{
				OvrVector3 pos = own_actor->GetPosition();
				current_sound->SetSoundPos(pos._x, pos._y, pos._z);
			}
		}

		return is_update;
	}

	void OvrAudioGroupTrack::Pause()
	{
		OvrTrack::Pause();
		if (nullptr != current_sound)
		{
			current_sound->Pause();
		}
	}
	void OvrAudioGroupTrack::Play()
	{
		OvrTrack::Play();
		for (auto it = children.begin(); it != children.end(); it++)
		{
			OvrAudioClip* current_clip = (OvrAudioClip*)((OvrAudioTrack*)(*it))->GetClipByTime(owner_sequence->GetCurrentFrame());
			if (nullptr == current_clip)
			{
				continue;
			}
			current_clip->Play();
		}

		if (nullptr != current_sound)
		{
			current_sound->Play();
		}
	}

	void OvrAudioGroupTrack::Seek(ovr::uint64 a_current_time)
	{
		for (auto it = children.begin(); it != children.end(); it++)
		{
			OvrAudioClip* current_clip = (OvrAudioClip*)((OvrAudioTrack*)(*it))->GetClipByTime(a_current_time);
			if (nullptr == current_clip)
			{
				continue;
			}
			current_clip->Seek(a_current_time);
		}
	}

	OvrTrack* OvrAudioGroupTrack::NewTrack(OvrTrackType a_track_type, const OvrString& a_tag)
	{
		if (a_tag != "audio")
		{
			return nullptr;
		}
	
		OvrAudioTrack* new_track = new OvrAudioTrack(owner_sequence);
		new_track->SetClipType(OvrClipInfo::kAudio);
		return new_track;
	}

	int OvrAudioGroupTrack::FillAudioDataCallback(void* a_param, void* a_buffer, int a_buffer_size)
	{
		OvrAudioGroupTrack* this_track = (OvrAudioGroupTrack*)a_param;
		return this_track->FillAudioData(a_buffer, a_buffer_size);
	}

	int OvrAudioGroupTrack::FillAudioData(void* a_buffer, int a_buffer_size)
	{
		wx_plugin::wxFrameBuffer* frame_buffer = GetCurrentFrame();

		if (nullptr != frame_buffer)
		{
			memcpy(a_buffer, frame_buffer->buffer, frame_buffer->data_size);
		}
		else
		{
			memset(a_buffer, 0, a_buffer_size);
		}

		return a_buffer_size;
	}

	wx_plugin::wxFrameBuffer* OvrAudioGroupTrack::GetCurrentFrame()
	{
		bool bNeedMixed = false;
		int buffOffset = 0;
		memset(current_frameBuffer->buffer, 0, current_frameBuffer->buffer_size);

		//因CreateSound时使用 2*default_frame_ms创建buff，audio_decoder使用1*default_frame_ms,所以要填充两次；
		for (int i = 0; i < 2; i++)
		{
			bNeedMixed = false;
			for (auto track_it : children)
			{
				//获取clip
				OvrAudioClip* audio_clip = (OvrAudioClip*)((OvrAudioTrack*)track_it)->GetClipByTime(owner_sequence->GetCurrentFrame());
				if (nullptr == audio_clip)
				{
					continue;
				}
				float weight = ((OvrAudioTrack*)track_it)->GetWeightValue();
				//获取相应的Buffer
				wx_plugin::wxFrameBuffer* temp_buffer = audio_clip->GetFrame();
				if (nullptr == temp_buffer)
				{
					continue;
				}

				//if (bNeedMixed)
					MixedSound((char*)current_frameBuffer->buffer + buffOffset, temp_buffer->buffer, temp_buffer->data_size,weight);
				/*else
				{
					memcpy((char*)current_frameBuffer->m_pBuffer + buffOffset, temp_buffer->m_pBuffer, temp_buffer->m_iDataSize);

					bNeedMixed = true;
				}*/

				audio_clip->PushFrame(temp_buffer);
			}
			buffOffset += current_frameBuffer->data_size / 2;
		}
		return current_frameBuffer;
	}
	void OvrAudioGroupTrack::MixedSound(void* dest, void*src, int size,float a_weight)
	{

		//求平均法
		/*
		short* dest_t = (short*)dest;
		short* src_t = (short*)src;

		while (size > 0)
		{
		int t = *dest_t + *src_t;
		t /= 2;

		*dest_t = t;
		dest_t++;
		src_t++;
		size -= 2;
		}*/
		/*
		short* dest_t = (short*)dest;
		short* src_t = (short*)src;
		int const MAX = 32767;
		int const MIN = -32768;
		while (size > 0)
		{
		if (*dest_t < 0 && *src_t < 0)
		*dest_t = (*dest_t + *src_t) - ((*dest_t) * (*src_t) / (-pow(2, 16 - 1) - 1));
		else
		*dest_t = (*dest_t + *src_t) - ((*dest_t) * (*src_t) / (pow(2, 16 - 1) - 1));
		if (*dest_t > MAX)	*dest_t = MAX;
		if (*dest_t < MIN)	*dest_t = MIN;
		dest_t++;
		src_t++;
		size -=2;
		}
		*/

		int const MAX = 32767;
		int const MIN = -32768;
		double f = 1;
		int output;
		short* dest_t = (short*)dest;
		short* src_t = (short*)src;

		while (size > 0)
		{
			int temp = *dest_t + (int)(*src_t*a_weight);
			//output = temp*f;
			output = temp;

			if (output > MAX)
			{
				f = (double)MAX / (double)(output);
				output = MAX;
			}
			if (output < MIN)
			{
				f = (double)MIN / (double)(output);
				output = MIN;
			}
			if (f < 1)
			{
				f += ((double)1 - f) / (double)32;
			}
			*dest_t = (short)output;

			dest_t++;
			src_t++;
			size -= 2;
		}
	}
}