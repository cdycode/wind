﻿#include "../headers.h"
#include <wx_core/track/ovr_curve_point_manager.h>

#define kBezierNum 8

namespace ovr_core
{
	////////////////////////////////////////////////////////////
	// class OvrCtlPoints

	template<class TC, typename T>
	void lvr_bezier3(TC& ao_pt, const TC& a_begin_pt, const TC& a_ctrl_pt0, const TC& a_ctrl_pt1, const TC& a_end_pt, T t)
	{
		T ft2 = t*t;
		T ft3 = ft2*t;
		T ft22 = (T(1) - t)*(T(1) - t);
		T ft23 = ft22*(T(1) - t);
		ao_pt = a_begin_pt*ft23 + a_ctrl_pt0*T(3)*t*ft22 + a_ctrl_pt1*T(3)*ft2*(T(1) - t) + a_end_pt*ft3;
	}

	OvrCtlPoint::OvrCtlPoint(CTLMode a_mode, ovr::uint64 a_frame, float a_value)
	{
		point_mode = a_mode;

		cur_point._x = (float)a_frame;
		cur_point._y = a_value;
	}

	//return true if change to user bezier
	bool OvrCtlPoint::updateCtlPointValue(const OvrVector2& a_left_value, const OvrVector2& a_right_value, bool a_is_change_left)
	{
		bool is_user_bezier = isUserBezier();

		if (is_move_both)
		{
			setCtlPoint(a_left_value, true);
			setCtlPoint(a_right_value, false);
		}
		else
		{
			setCtlPoint(a_is_change_left ? a_left_value : a_right_value, a_is_change_left);
		}

		return is_user_bezier != isUserBezier();
	}

	void OvrCtlPoint::setCtlPoint(const OvrVector2& a_pt, bool a_is_left)
	{
		if (a_is_left)
		{
			is_left_valid = true;
			ctl_pt_left_value = a_pt;
		}
		else
		{
			is_right_valid = true;
			ctl_pt_right_value = a_pt;
		}

		//拖动右边的控制点,改变当前点为贝塞尔模式
		if (point_mode != kCMBezier && !a_is_left)
		{
			point_mode = kCMBezier;
		}
	}

	bool OvrCtlPoint::setMode(CTLMode a_mode)
	{
		if (a_mode == kCMBezier)
		{
			return resetBezier();
		}

		if (point_mode != a_mode)
		{
			point_mode = a_mode;
			return true;
		}

		return false;
	}

	bool OvrCtlPoint::resetBezier()
	{
		if (!isAutoBezier() || is_move_both == false)
		{
			point_mode = kCMBezier;
			is_left_valid = is_right_valid = false;
			ctl_pt_left_value = ctl_pt_right_value = OvrVector2(0, 0);
			is_move_both = true;
			return true;
		}
		return false;
	}

	bool OvrCtlPoint::makeDevorce()
	{
		if (is_move_both)
		{
			is_move_both = false;
			return true;
		}
		return false;
	}

	void OvrCtlPoint::moveTo(ovr::uint64 a_to)
	{
		if (is_left_valid)
		{
			ctl_pt_left_value._x += a_to - cur_point._x;
		}
		if (is_right_valid)
		{
			ctl_pt_right_value._x += a_to - cur_point._x;
		}
		cur_point._x = (float)a_to;
	}

	bool OvrCtlPoint::isUserBezier()
	{
		return (point_mode == kCMBezier) && (is_left_valid || is_right_valid);
	}

	bool OvrCtlPoint::isAutoBezier()
	{
		return (point_mode == kCMBezier) && !is_left_valid && !is_right_valid;
	}

	//////////////////////////////////////////////////////////////
	// class OvrCurvePointManager

	OvrCurvePointManager::OvrCurvePointManager()
	{

	}

	OvrCurvePointManager::~OvrCurvePointManager()
	{
		for (auto it : ctl_pt_map)
		{
			OvrCtlPoint* pt = it.second;
			delete pt;
		}
		ctl_pt_map.clear();
	}

	void OvrCurvePointManager::addPoint(ovr::uint64 a_frame, float a_value)
	{
		if (ctl_pt_map.find(a_frame) == ctl_pt_map.end())
		{
			ctl_pt_map[a_frame] = new OvrCtlPoint(OvrCtlPoint::kCMLine, a_frame, a_value);
			//ctl_pt_map[a_frame] = new OvrCtlPoint(OvrCtlPoint::kCMBezier, a_frame, a_value);
		}
		else
		{
			auto pt = ctl_pt_map[a_frame];
			if (pt->is_left_valid)
			{
				pt->ctl_pt_left_value._y += a_value - pt->cur_point._y;
			}
			if (pt->is_right_valid)
			{
				pt->ctl_pt_right_value._y += a_value - pt->cur_point._y;
			}
			pt->cur_point._y = a_value;
		}
		setChanged();
	}

	void OvrCurvePointManager::RmvPoint(ovr::uint64 a_frame)
	{
		if (ctl_pt_map.find(a_frame) != ctl_pt_map.end())
		{
			delete ctl_pt_map[a_frame];
			ctl_pt_map.erase(a_frame);

			if (a_frame == select_point_frame)
			{
				select_point_frame = -1;
			}
			setChanged();
		}
	}

	bool OvrCurvePointManager::MovePoint(ovr::uint64 a_from, ovr::uint64 a_to)
	{
		if (ctl_pt_map.find(a_from) != ctl_pt_map.end() && ctl_pt_map.find(a_to) == ctl_pt_map.end())
		{
			ctl_pt_map[a_to] = ctl_pt_map[a_from];
			ctl_pt_map[a_to]->moveTo(a_to);

			ctl_pt_map.erase(a_from);

			if (a_from == select_point_frame)
			{
				select_point_frame = (int)a_to;
			}
			setChanged();
			return true;
		}
		return false;
	}

	bool OvrCurvePointManager::makeDevorce(int a_frame)
	{
		auto pt = getPoint(a_frame);
		if (pt != nullptr)
		{
			return pt->makeDevorce();
		}
		return false;
	}

	bool OvrCurvePointManager::setMode(int a_frame, OvrCtlPoint::CTLMode a_mode)
	{
		auto pt = getPoint(a_frame);
		if (pt != nullptr)
		{
			if (pt->setMode(a_mode))
			{
				setChanged();
				return true;
			}
		}
		return false;
	}

	bool OvrCurvePointManager::updateCtlPointValue(int a_frame, const OvrVector2& a_left_value, const OvrVector2& a_right_value, bool a_is_change_left)
	{
		auto pt = getPoint(a_frame);
		if (pt != nullptr)
		{
			setChanged();
			return pt->updateCtlPointValue(a_left_value, a_right_value, a_is_change_left);
		}
		return false;
	}

	bool OvrCurvePointManager::SetSelectFrame(int a_frame)
	{
		if (select_point_frame != a_frame)
		{
			select_point_frame = a_frame;
			return true;
		}
		return false;
	}

	OvrVector2 OvrCurvePointManager::getControlPoints(int a_frame, bool a_is_left)
	{
		OvrVector2 p1, p2;
		getControlPoints(a_frame, p1, p2);
		return a_is_left ? p1 : p2;
	}

	void OvrCurvePointManager::getControlPoints(int a_frame, OvrVector2& a_pt_left, OvrVector2& a_pt_right)
	{
		OvrVector2 pt_n, pt_cur;

		getCtlNormalLine(a_frame, pt_n, pt_cur);

		float line_len = pt_n.length() / kBezierNum;

		//获取默认的控制点
		a_pt_left = pt_cur - pt_n * (line_len / pt_n.length());
		a_pt_right = pt_cur + pt_n * (line_len / pt_n.length());

		//获取改变了的控制点
		OvrCtlPoint* ctl_point = getPoint(a_frame);
		if (ctl_point->is_left_valid)
		{
			OvrVector2 pt_n_left = pt_cur - ctl_point->ctl_pt_left_value;
			a_pt_left = pt_cur - pt_n_left * (line_len / pt_n_left.length());
		}
		if (ctl_point->is_right_valid)
		{
			OvrVector2 pt_n_right = ctl_point->ctl_pt_right_value - pt_cur;
			a_pt_right = pt_cur + pt_n_right * (line_len / pt_n_right.length());
		}
	}

	void OvrCurvePointManager::getCtlNormalLine(int a_frame, OvrVector2& a_pt_n, OvrVector2& a_pt_current)
	{
		OvrVector2 pt_prev, pt_next;
		getPoints(a_frame, a_pt_current, pt_prev, pt_next);

		a_pt_n = pt_next - pt_prev;
	}

	void OvrCurvePointManager::setChanged()
	{
		is_changed = true;
		Reset();
	}

	OvrCtlPoint* OvrCurvePointManager::getPoint(ovr::uint64 a_frame)
	{
		auto it = ctl_pt_map.find(a_frame);
		if (it != ctl_pt_map.end())
		{
			return (*it).second;
		}
		return nullptr;
	}

	void OvrCurvePointManager::getPoints(int a_frame, OvrVector2& a_pt_current, OvrVector2& a_pt_prev, OvrVector2& a_pt_next)
	{
		auto it_current = ctl_pt_map.find(a_frame);
		assert(it_current != ctl_pt_map.end());
		a_pt_current = (*it_current).second->cur_point;

		auto it_prev = it_current == ctl_pt_map.begin() ? ctl_pt_map.end() : --it_current;
		a_pt_prev = it_prev != ctl_pt_map.end() ? (*it_prev).second->cur_point : OvrVector2(a_pt_current._x - 10, a_pt_current._y);
		if (a_pt_prev._x < 0)
		{
			a_pt_prev._x = 0;
		}

		auto it_next = ++ctl_pt_map.find(a_frame);
		a_pt_next = it_next != ctl_pt_map.end() ? (*it_next).second->cur_point : OvrVector2(a_pt_current._x + 10, a_pt_current._y);
	}

	bool OvrCurvePointManager::getXRange(int& a_min_frame, int& a_max_frame)
	{
		if (ctl_pt_map.size() > 0)
		{
			auto it_first = ctl_pt_map.begin();
			a_min_frame = (int)(*it_first).first;

			auto it_last = --ctl_pt_map.end();
			a_max_frame = (int)(*it_last).first;

			return true;
		}
		return false;
	}

	float OvrCurvePointManager::getYRange(float* a_min, float* a_max)
	{
		bool isFirst = true;
		float max_value = 0, min_value = 0;

		for (auto item : ctl_pt_map)
		{
			float value = item.second->cur_point._y;
			if (isFirst)
			{
				isFirst = false;
				max_value = min_value = value;
			}
			else if(value > max_value)
			{
				max_value = value;
			}
			else if (value < min_value)
			{
				min_value = value;
			}
		}

		float value_space = max_value - min_value;
		if (value_space <= 0.000001f)
		{
			value_space = max_value <= 0.000001f ? value_space = 1 : value_space = max_value;
		}

		if (a_min != nullptr) *a_min = min_value;
		if (a_max != nullptr) *a_max = max_value;

		return value_space;
	}

	//找出属于自己的点
	void OvrCurvePointManager::pickKeyPoint(const std::vector<OvrVector3>& a_lines, std::map<ovr::uint64, OvrVector3>& a_points, ovr::uint64 a_offset)
	{
		for (auto item : ctl_pt_map)
		{
			a_points[(unsigned int)item.first] = a_lines[(unsigned int)(item.first - a_offset)];
		}
	}

	float OvrCurvePointManager::GetValue(ovr::uint64 a_frame_index)
	{
		if (ctl_pt_map.size() == 0)
		{
			return default_value;
		}

		//是否在当前的区间
		if (a_frame_index >= current_begin_time && a_frame_index < current_end_time)
		{
			return CalcValue(a_frame_index);
		}

		//状态重置
		Reset();

		//有关键帧 或者在第一帧之前 最后一帧之后
		float value = 0;
		if (GetSpecificValue(a_frame_index, value))
		{
			return value;
		}

		//寻找前后的关键帧
		FindKeyRange(a_frame_index);

		//计算当前值
		return CalcValue(a_frame_index);
	}

	void OvrCurvePointManager::Reset()
	{
		current_begin_time = 0;
		current_end_time = 0;
	}

	bool OvrCurvePointManager::GetSpecificValue(ovr::uint64 a_current_time, float& a_value)
	{
		if (ctl_pt_map.size() < 1)
		{
			a_value = default_value;
			return true;
		}

		//优先选择
		auto it = ctl_pt_map.find(a_current_time);
		if (it != ctl_pt_map.end())
		{
			a_value = it->second->cur_point._y;
			return true;
		}

		//在第一个之前
		auto first_it = ctl_pt_map.begin();
		if (a_current_time <= first_it->first)
		{
			a_value = first_it->second->cur_point._y;
			return true;
		}

		//在最后一个之后
		auto last_it = ctl_pt_map.end();
		--last_it;
		if (a_current_time >= last_it->first)
		{
			a_value = last_it->second->cur_point._y;
			return true;
		}

		return false;
	}

	void OvrCurvePointManager::FindKeyRange(ovr::uint64 a_current_time)
	{
		//获取最近的
		for (auto frame_it = ctl_pt_map.begin(); frame_it != ctl_pt_map.end(); frame_it++)
		{
			if (a_current_time <= frame_it->first)
			{
				current_end_time = frame_it->first;
				current_end_pt = getPoint(frame_it->first);
				ctl_pt_2 = getControlPoints((int)frame_it->first, true);
				break;
			}
			current_begin_time = frame_it->first;
			current_begin_pt = getPoint(frame_it->first);
			ctl_pt_1 = getControlPoints((int)frame_it->first, false);
		}
	}

	float OvrCurvePointManager::CalcValue(ovr::uint64 a_frame_index)
	{
		assert(current_begin_pt != nullptr && current_end_pt != nullptr);

		if (current_begin_pt->isJump())
		{
			return current_begin_pt->cur_point._y;
		}
		else if (current_begin_pt->isLine())
		{
			float factor = (a_frame_index - current_begin_time)*1.0f / (current_end_time - current_begin_time);
			return current_begin_pt->cur_point._y * (1 - factor) + current_end_pt->cur_point._y * factor;
		}

		assert(current_begin_pt->point_mode == OvrCtlPoint::kCMBezier);

		OvrVector2 result;
		float x = (a_frame_index - current_begin_pt->cur_point._x) * 1.0f / (current_end_pt->cur_point._x - current_begin_pt->cur_point._x + 1);
		lvr_bezier3(result, current_begin_pt->cur_point, ctl_pt_1, ctl_pt_2, current_end_pt->cur_point, x);
		return result._y;
	}

	void OvrCurvePointManager::GetValues(ovr::uint64 a_frame_begin, ovr::uint64 a_frame_end, std::vector<float>& a_values)
	{
		OvrVector2 pt_head = getPoint(a_frame_begin)->cur_point;
		OvrVector2 pt_tail = getPoint(a_frame_end)->cur_point;

		ovr::uint64 count = a_frame_end - a_frame_begin + 1;
		a_values[0] = pt_head._y;
		a_values[(unsigned int)(count - 1)] = pt_tail._y;
		
		OvrVector2 pt_1 = getControlPoints((int)a_frame_begin, false);
		OvrVector2 pt_2 = getControlPoints((int)a_frame_end, true);

		OvrVector2 result;
		for (int i = 1; i < count - 1; ++i)
		{
			lvr_bezier3(result, pt_head, pt_1, pt_2, pt_tail, i * 1.0f / count);
			a_values[i] = result._y;
		}
	}

	//插值填充所有的帧数据,
	void OvrCurvePointManager::initCurveLine(std::vector<OvrVector3>& a_lines, int a_min_x, int a_max_x, int a_data_pos)
	{
		Reset();

		for (int i = a_min_x; i <= a_max_x; ++i)
		{
			a_lines[i - a_min_x]._m[a_data_pos] = GetValue(i);
		}
	}

}
