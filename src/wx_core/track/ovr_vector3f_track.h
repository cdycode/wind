﻿#ifndef OVR_CORE_OVR_VECTOR3F_TRACK_H_
#define OVR_CORE_OVR_VECTOR3F_TRACK_H_

#include <wx_core/track/ovr_track.h>

namespace ovr_core 
{
	class OvrFloatTrack;
	class OvrVector3fTrack : public OvrTrack
	{
	public:
		OvrVector3fTrack(OvrSequence* a_sequence);
		virtual ~OvrVector3fTrack() {}

		virtual bool	Init() override;
		virtual void	Uninit() override;

		void	SetSubTrackTag(const OvrString& a_tag1, const OvrString& a_tag2, const OvrString& a_tag3);
		void	SetSubTrackDisplayName(const OvrString& a_name1, const OvrString& a_name2, const OvrString& a_name3);

		//设置缺省值
		void	SetDefaultValue(float a_r_value, float a_green_value, float a_blue_value);

		//更新并返回当前值
		const OvrVector3& GetValue();

		//增加数据点
		bool	AddKey(ovr::uint64 a_time, const OvrVector3& a_value);

	protected:
		//创建一个track
		virtual OvrTrack*	NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) override;
		virtual void		DelTrack(OvrTrack* a_track) override;
	protected:
		OvrFloatTrack*	x_track = nullptr;
		OvrFloatTrack*	y_track = nullptr;
		OvrFloatTrack*	z_track = nullptr;

		OvrVector3		current_value;
	};
}

#endif
