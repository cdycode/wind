﻿#include "../headers.h"
#include "ovr_transform_group_track.h"
#include "ovr_vector3f_track.h"
#include <wx_core/ovr_sequence.h>

namespace ovr_core 
{
	OvrTransfromGroupTrack::OvrTransfromGroupTrack(OvrSequence* a_sequence, ovr_engine::OvrActor* a_actor) 
		:OvrTrack(a_sequence)
		,actor_ins(a_actor)
	{
	}
	
	bool OvrTransfromGroupTrack::Init() 
	{
		AddTrack(kTrackCommon, "position");
		AddTrack(kTrackCommon, "rotation");
		AddTrack(kTrackCommon, "scale");

		return true;
	}
	void OvrTransfromGroupTrack::Uninit() 
	{
		postion_track = nullptr;
		rotation_track = nullptr;
		scale_track = nullptr;
	}

	bool OvrTransfromGroupTrack::AddValue(ovr::uint64 a_time) 
	{
		if (nullptr == actor_ins)
		{
			return false;
		}
		const OvrVector3& position = actor_ins->GetPosition();
		const OvrVector3& scale = actor_ins->GetScale();
		OvrVector3 rotation;
		actor_ins->GetQuaternion().get_euler_angles(rotation[0], rotation[1], rotation[2]);

		postion_track->AddKey(a_time, position);
		scale_track->AddKey(a_time, scale);
		rotation_track->AddKey(a_time, rotation);

		return true;
	}

	bool OvrTransfromGroupTrack::Update() 
	{
		OvrTrack::Update();

		actor_ins->SetPosition(postion_track->GetValue());
		actor_ins->SetScale(scale_track->GetValue());
		const OvrVector3& rotation = rotation_track->GetValue();
		actor_ins->SetEulerAngle(rotation[0], rotation[1], rotation[2]);
		return true;
	}
	void OvrTransfromGroupTrack::Seek(ovr::uint64 a_current_time)
	{
		actor_ins->SetPosition(postion_track->GetValue());
		actor_ins->SetScale(scale_track->GetValue());
		const OvrVector3& rotation = rotation_track->GetValue();
		actor_ins->SetEulerAngle(rotation[0], rotation[1], rotation[2]);
	}

	OvrTrack* OvrTransfromGroupTrack::NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) 
	{
		OvrTrack* new_track = nullptr;
		if (a_tag == "position")
		{
			const OvrVector3& position = actor_ins->GetPosition();
			postion_track = new OvrVector3fTrack(owner_sequence);
			postion_track->Init();
			postion_track->SetDefaultValue(position[0], position[1], position[2]);
			postion_track->SetDisplayName("位置");
			new_track = postion_track;
		}
		else if (a_tag == "rotation")
		{
			OvrVector3 rotation;
			actor_ins->GetEulerAngle(rotation[0], rotation[1], rotation[2]);
			rotation_track = new OvrVector3fTrack(owner_sequence);
			rotation_track->Init();
			rotation_track->SetDefaultValue(rotation[0], rotation[1], rotation[2]);
			rotation_track->SetDisplayName("旋转");
			
			new_track = rotation_track;
		}
		else if (a_tag == "scale")
		{
			const OvrVector3& scale = actor_ins->GetScale();
			scale_track = new OvrVector3fTrack(owner_sequence);
			scale_track->Init();
			scale_track->SetDefaultValue(scale[0], scale[1], scale[2]);
			scale_track->SetDisplayName("缩放");

			new_track = scale_track;
		}
		return new_track;
	}
	void OvrTransfromGroupTrack::DelTrack(OvrTrack* a_track) 
	{
		if (a_track == postion_track)
		{
			postion_track = nullptr;
		}
		else if (a_track == rotation_track)
		{
			rotation_track = nullptr;
		}
		else if (a_track == scale_track)
		{
			scale_track = nullptr;
		}
	}
}