﻿#ifndef OVR_PROJECT_OVR_AUDIO_TRACK_H_
#define OVR_PROJECT_OVR_AUDIO_TRACK_H_

#include <mutex>
#include <wx_core/track/ovr_clip.h>
#include <wx_core/track/ovr_clip_track.h>

namespace wx_plugin
{
	class IOvrAudioDecoder;
	class wxFrameBuffer;
	class wxISound;
}

namespace ovr_engine 
{
	class OvrActor;
}

namespace ovr_core
{
	class OvrFloatTrack;
	class OvrAudioGroupTrack;
	class OvrAudioTrack : public OvrClipTrack
	{
	public:
		OvrAudioTrack(OvrSequence* a_sequence);
		virtual ~OvrAudioTrack();

		virtual ovr::uint64	GetAssetDuration(const OvrString& a_file_path)override;

		virtual bool		IsSupportDragIn(const OvrString& a_file_path);

		float				GetWeightValue()const;
	
	protected:
		//添加权重轨道
		virtual OvrTrack*	NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) override;
		virtual void		DelTrack(OvrTrack* a_track) override;

	private:
		OvrFloatTrack*		weight_track = nullptr;
	};

	class OvrAudioGroupTrack : public OvrTrack
	{
	public:
		OvrAudioGroupTrack(OvrSequence* a_sequence, ovr_engine::OvrActor* a_actor);
		virtual ~OvrAudioGroupTrack();

		virtual bool	Init()override;
		virtual void	Uninit()override;

		virtual bool IsLive()const { return true; }

		virtual bool Update()override;
		virtual void Pause() override;
		virtual void Play() override;
		virtual void Seek(ovr::uint64 a_current_time) override;

		ovr::uint32	GetChannel()const { return default_channel; }
		ovr::uint32	GetSampleRate()const { return default_sample_rate; }
		ovr::uint32	GetSampleBit()const { return default_sample_bit; }
		ovr::uint32	GetFrameTime() { return default_frame_ms; }

	protected:
		virtual OvrTrack*	NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) override;
	private:
		static int FillAudioDataCallback(void* a_param, void* a_buffer, int a_buffer_size);
		int		   FillAudioData(void* a_buffer, int a_buffer_size);

		wx_plugin::wxFrameBuffer*	GetCurrentFrame();

		void MixedSound(void* a_param, void* a_buffer, int a_buffer_size, float a_weight);
	private:
		ovr_engine::OvrActor*	own_actor = nullptr;	//针对3DAudio使用

		wx_plugin::wxISound*			current_sound = nullptr;

		//默认的音频格式 后期将通过UI进行配置
		ovr::uint32		default_channel = 2;
		ovr::uint32		default_sample_rate = 44100;
		ovr::uint32		default_sample_bit = 16;
		ovr::uint32		default_frame_ms = 40;	//默认每帧为40ms
		wx_plugin::wxFrameBuffer* current_frameBuffer = nullptr;
	};
}

#endif
