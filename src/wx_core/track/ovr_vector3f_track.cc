﻿#include "../headers.h"
#include "ovr_vector3f_track.h"
#include <wx_core/track/ovr_float_track.h>

namespace ovr_core 
{
	OvrVector3fTrack::OvrVector3fTrack(OvrSequence* a_sequence)
		:OvrTrack(a_sequence)
	{
		display_name = "Vector3";
	}

	bool OvrVector3fTrack::Init()
	{
		AddTrack(kTrackPoint, "X");
		AddTrack(kTrackPoint, "Y");
		AddTrack(kTrackPoint, "Z");
		return true;
	}
	void OvrVector3fTrack::Uninit()
	{
		x_track = nullptr;
		y_track = nullptr;
		z_track = nullptr;
	}

	void OvrVector3fTrack::SetSubTrackTag(const OvrString& a_tag1, const OvrString& a_tag2, const OvrString& a_tag3)
	{
		x_track->SetTag(a_tag1);
		y_track->SetTag(a_tag2);
		z_track->SetTag(a_tag3);
	}

	void OvrVector3fTrack::SetSubTrackDisplayName(const OvrString& a_name1, const OvrString& a_name2, const OvrString& a_name3)
	{
		x_track->SetDisplayName(a_name1);
		y_track->SetDisplayName(a_name2);
		z_track->SetDisplayName(a_name3);
	}

	void OvrVector3fTrack::SetDefaultValue(float a_r_value, float a_green_value, float a_blue_value)
	{
		x_track->SetDefaultValue(a_r_value);
		y_track->SetDefaultValue(a_green_value);
		z_track->SetDefaultValue(a_blue_value);
	}

	const OvrVector3& OvrVector3fTrack::GetValue()
	{
		current_value[0] = x_track->GetCurrentValue();
		current_value[1] = y_track->GetCurrentValue();
		current_value[2] = z_track->GetCurrentValue();

		return current_value;
	}

	//增加数据点
	bool OvrVector3fTrack::AddKey(ovr::uint64 a_time, const OvrVector3& a_value) 
	{
		x_track->AddKeyFrame(a_time, a_value[0]);
		y_track->AddKeyFrame(a_time, a_value[1]);
		z_track->AddKeyFrame(a_time, a_value[2]);
		return true;
	}

	//创建一个track
	OvrTrack* OvrVector3fTrack::NewTrack(OvrTrackType a_track_type, const OvrString& a_tag)
	{
		OvrTrack* new_track = nullptr;
		// new track add sub track
		if (a_tag == "X")
		{
			x_track = new OvrFloatTrack(owner_sequence);
			x_track->SetDisplayName("X");
			new_track = x_track;
		}
		else if (a_tag == "Y")
		{
			y_track = new OvrFloatTrack(owner_sequence);
			y_track->SetDisplayName("Y");
			new_track = y_track;
		}
		else if (a_tag == "Z")
		{
			z_track = new OvrFloatTrack(owner_sequence);
			z_track->SetDisplayName("Z");
			new_track = z_track;
		}
		return new_track;
	}
	void OvrVector3fTrack::DelTrack(OvrTrack* a_track) 
	{
		if (a_track == x_track)
		{
			x_track = nullptr;
		}
		else if (a_track == y_track)
		{
			y_track = nullptr;
		}
		else if (a_track == z_track)
		{
			z_track = nullptr;
		}
	}
}

