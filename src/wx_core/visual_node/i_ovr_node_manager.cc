﻿#include "../headers.h"
#include <wx_core/visual_node/i_ovr_node_manager.h>
#include <wx_core/visual_node/i_ovr_visual_node.h>
#include <wx_core/visual_node/i_ovr_node_cluster.h>

namespace ovr_core 
{
	//OvrVisualNode的初始化和反初始化函数
	typedef void* (*InitNodeModuleFunc)(const OvrString& a_scene_unique_name);
	typedef void(*UninitNodeModuleFunc)();

	//动态库的句柄
	static OvrDllLoader node_loader;

	//动态库的实例
	static IOvrNodeManager* sg_node_manager = nullptr;

	bool IOvrNodeManager::IsLoad() 
	{
		return node_loader.IsLoad();
	}

	bool IOvrNodeManager::Load(const OvrString& a_scene_unique_name)
	{
		if (node_loader.IsLoad())
		{
			OvrLogE("ovr_media", "IOvrNodeManager::Load already load");
			return false;
		}
	
		//load dll
#ifdef _WIN32
#	ifdef _DEBUG
		node_loader.Load("./OvrVisualNode_x86_d.dll");
#	else
		node_loader.Load("./OvrVisualNode_x86.dll");
#	endif
#else
		node_loader.Load("libovr_visual_node.so");
#endif
		if (!node_loader.IsLoad())
		{
			OvrLogE("ovr_media", "IOvrNodeManager::Load open dll failed");
			return false;
		}

		//获取初始函数地址
		InitNodeModuleFunc init_func = (InitNodeModuleFunc)node_loader.GetFunctionAddress("Init");
		if (nullptr == init_func)
		{
			OvrLogE("ovr_media", "IOvrNodeManager::Load get init function failed");
			Unload();
			return false;
		}

		//调用初始化函数
		sg_node_manager = (IOvrNodeManager*)(*init_func)(a_scene_unique_name);
		if (nullptr == sg_node_manager)
		{
			OvrLogE("ovr_media", "IOvrNodeManager::Load init dll failed");
			Unload();
			return false;
		}

		return true;
	}

	void IOvrNodeManager::Unload() 
	{
		if (!node_loader.IsLoad())
		{
			return;
		}

		//寻找反初始化函数 并调用
		if (nullptr != sg_node_manager)
		{
			//获取反初始化函数地址
			UninitNodeModuleFunc uninit_func = (UninitNodeModuleFunc)node_loader.GetFunctionAddress("Uninit");
			if (nullptr != uninit_func)
			{
				//调用反初始化函数
				(*uninit_func)();
			}
			sg_node_manager = nullptr;
		}
		
		//释放动态库
		node_loader.Unload();
	}

	IOvrNodeManager*	IOvrNodeManager::GetIns()
	{
		return sg_node_manager;
	}
}
