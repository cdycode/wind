﻿#include "../headers.h"
#include <wx_core/visual_node/i_ovr_node_cluster.h>

namespace ovr_core 
{
	void IOvrNodeCluster::SetFilePath(const OvrString& a_file_path) 
	{
		file_path = a_file_path;
		OvrFileTools::GetFileNameFromPath(a_file_path.GetStringConst(), display_name);
	}
}
