﻿#ifndef OVR_CORE_HEADERS_H_
#define OVR_CORE_HEADERS_H_

#include <assert.h>
#include <time.h>

#ifdef _WIN32
#include <windows.h>
#endif

#include <wx_base/wx_file_tools.h>
#include <wx_base/wx_dll_loader.h>
#include <wx_base/wx_log.h>

#include <wx_engine/ovr_actor.h>
#include <wx_engine/ovr_material.h>
#include <wx_engine/ovr_scene.h>

#endif
