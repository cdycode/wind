﻿#include "headers.h"
#include <wx_core/ovr_background_audio.h>
#include <wx_plugin/wx_i_sound.h>
#include <wx_base/wx_time.h>
#include <wx_plugin/wx_i_av_media.h>

namespace ovr_core 
{
	
	bool OvrBackgroundAudio::Init() 
	{
		wx_plugin::wxISoundManager* sound_manager = wx_plugin::wxISoundManager::Load();
		if (nullptr == sound_manager)
		{
			return false;
		}
		sound = sound_manager->CreateSound(2, 44100, 16, 80, true);
		sound->SetFillAudioDataFunc(&background_file, &FillAudioData);
		return true;
	}
	void OvrBackgroundAudio::Uninit() 
	{
		wx_plugin::wxISoundManager* sound_manager = wx_plugin::wxISoundManager::Load();
		if (nullptr != sound_manager)
		{
			sound_manager->DestorySound(sound);
		}
		
		ClearAllAudio();
	}

	void OvrBackgroundAudio::Seek(ovr::uint64 a_frame_index) 
	{
		//current_audio_decoder->Seek(a_frame_index);
		if (sound != NULL)
			sound->Pause();
		wx_plugin::wxISoundManager* sound_manager = wx_plugin::wxISoundManager::Load();
		if (nullptr != sound_manager) 
		{
			sound_manager->StopAllMusic();
		}
	}

	void OvrBackgroundAudio::Pause() 
	{
		sound->Pause();
	}

	void OvrBackgroundAudio::Play() 
	{
		sound->Play();
	}

	bool OvrBackgroundAudio::Update()
	{
		float current = (float)OvrTime::GetSeconds();
		float deltatime = (current - last_time);
		last_time = current;

		std::vector<BACKGROUND_AUDIO>::iterator it = background_file.begin();
		for (; it != background_file.end(); it++)
		{
			it->already_play_time += deltatime;
			if (it->already_play_time > it->total_time)
			{
				it->already_play_time = 0;
				it->audio_decoder->Seek(it->bloopfrom_0?0:1);
				it->bloopfrom_0 = !it->bloopfrom_0;
				it->audio_decoder->Resume();
			}
		}

		//计算混合时间
		if (background_file.size() > 1)
		{
			background_file[0].current_time += deltatime;

			if (background_file[0].current_time > background_file[0].blend_time)
			{
				background_file[0].Destroy();
				background_file.erase(background_file.begin());
			}
		}
		return false;
	}

	void OvrBackgroundAudio::PlayLoop(const OvrString& a_audio_file_path, float a_blend_time, float a_is_volume)
	{
		wx_plugin::IOvrAVMedia* media_plugin = wx_plugin::IOvrAVMedia::Load();
		if (nullptr == media_plugin)
		{
			return;
		}

		BACKGROUND_AUDIO bg;
		wx_plugin::IOvrAudioDecoder* audio_decoder = media_plugin->CreateAudioDecoder();

		if (audio_decoder->Open(a_audio_file_path.GetStringConst()) == false)
		{
			audio_decoder->Close();
			delete audio_decoder;
		}
		wx_plugin::StruAudioStreamInfo audio_indo;
		audio_decoder->GetFileAudioInfo(audio_indo);
		audio_decoder->SetAudioOutFormat(2, 16, 44100, 80);
		audio_decoder->Seek(0);
		audio_decoder->Resume();
		audio_decoder->StartDecoding();
		bg.total_time = audio_indo.length / 1000.0f;
		if (a_blend_time == 0)
		{
			ClearAllAudio();
			bg.blend_time = -1;
		}
		else
		{
			if (background_file.size() > 0)
			{
				background_file[background_file.size() - 1].current_time = 0;
				background_file[background_file.size() - 1].blend_time = a_blend_time;
			}
			bg.blend_time = a_blend_time;
		}
		bg.audio_decoder = audio_decoder;
		background_file.push_back(bg);
		sound->SetVolume(a_is_volume);
	}

	void OvrBackgroundAudio::PlayOnce(const OvrString& a_audio_file_path, float a_is_volume)
	{
		wx_plugin::wxISoundManager* sound_manager = wx_plugin::wxISoundManager::Load();
		if (nullptr != sound_manager) 
		{
			sound_manager->PlayMusic(a_audio_file_path.GetStringConst(), false, a_is_volume);
		}
	}

	void OvrBackgroundAudio::ClearAllAudio()
	{
		std::vector<BACKGROUND_AUDIO>::iterator it = background_file.begin();
		for (;it!= background_file.end();)
		{
			it->Destroy();
			it = background_file.erase(it);
		}
	}
	int OvrBackgroundAudio::FillAudioData(void* a_param, void* a_buffer, int a_buffer_size)
	{
		std::vector<BACKGROUND_AUDIO> *bg_list = (std::vector<BACKGROUND_AUDIO> *)a_param;

		if ((*bg_list).size() == 0)
			return 0;
		std::vector<BACKGROUND_AUDIO>::iterator it = (*bg_list).begin();

		if ((*bg_list).size() == 0)
			return a_buffer_size;
		
		//混合
		for (ovr::uint32 i = 0; i < bg_list->size(); i++)
		{
			wx_plugin::IOvrAudioDecoder* audio_decoder = (*bg_list)[i].audio_decoder;
			wx_plugin::wxFrameBuffer* frame_buffer = audio_decoder->PopFrame();
			int size = 0;
			if (nullptr != frame_buffer)
			{
				if (i == 0)
				{
					memcpy(a_buffer, frame_buffer->buffer, frame_buffer->data_size);
				}
				else
				{
					Mixed(a_buffer, frame_buffer->buffer, frame_buffer->data_size, (*bg_list)[i-1].current_time/ (*bg_list)[i-1].blend_time);
				}
				size = frame_buffer->data_size;
				audio_decoder->PushFrame(frame_buffer);
			}
		}

		return a_buffer_size;
	}
	void OvrBackgroundAudio::Mixed(void *a_dest,void* a_src,int a_size,float a_percent)
	{
		int const MAX = 32767;
		int const MIN = -32768;
		double f = 1;
		int output;
		short* dest_t = (short*)a_dest;
		short* src_t  = (short*)a_src;

		while (a_size > 0)
		{
			int temp = (int)(*dest_t*(1 - a_percent)) + (int)((*src_t)*a_percent);
			
			output = temp;

			if (output > MAX)
			{
				f = (double)MAX / (double)(output);
				output = MAX;
			}
			if (output < MIN)
			{
				f = (double)MIN / (double)(output);
				output = MIN;
			}
			if (f < 1)
			{
				f += ((double)1 - f) / (double)32;
			}
			*dest_t = (short)output;

			dest_t++;
			src_t++;
			a_size -= 2;
		}
	}
	void OvrBackgroundAudio::BACKGROUND_AUDIO::Destroy()
	{
		if (audio_decoder != NULL)
		{
			audio_decoder->Close();
			delete audio_decoder;
			audio_decoder = NULL;
		}
	}
}
