﻿#include "headers.h"

#include <wx_base/wx_time.h>

#include <wx_core/ovr_scene_context.h>
#include <wx_core/ovr_sequence.h>
#include <wx_engine/ovr_actor.h>
#include "ovr_visual_node.h"
#include "ovr_node_cluster.h"

namespace ovr_node 
{
	OvrVisualNode::OvrVisualNode()
	{
		display_name = "VisualNode";

		char temp[128] = { 0 };
		sprintf(temp, "node_%llu", OvrTime::GetTicksNanos());
		unique_name.SetString(temp);
	}

	ovr_core::OvrNodeParam::Type OvrVisualNode::GetParamType(bool a_is_in, const OvrString& a_param_name) const
	{
		ovr_core::OvrNodeParam::Type param_type = ovr_core::OvrNodeParam::kParamInvalid;
		if (a_is_in)
		{
			for (auto it = in_param_list.begin(); it != in_param_list.end(); it++)
			{
				if (it->unique_name == a_param_name)
				{
					param_type = it->type;
					break;
				}
			}
		}
		else 
		{
			for (auto it = out_param_list.begin(); it != out_param_list.end(); it++)
			{
				if (it->unique_name == a_param_name)
				{
					param_type = it->type;
					break;
				}
			}
		}
		return param_type;
	}

	bool OvrVisualNode::AddOutConnect(const OvrString& a_local_interface, ovr_core::IOvrVisualNode* a_other_node, const OvrString& a_other_interface)
	{
		bool is_success = false;

		//先检查是否存在现有的连接
		/*auto connect_it = out_connect_list.begin();
		while (connect_it != out_connect_list.end())
		{
			if (connect_it->local_interface == a_local_interface)
			{
				//存在现有的链接
				if (nullptr != connect_it->other_node)
				{
					//移除下端的 输入信息
					((OvrVisualNode*)connect_it->other_node)->RmvInConnect(connect_it->other_interface);
				}

				//再打断自己的链接 并指向新的链接
				connect_it->other_node = a_other_node;
				connect_it->other_interface = a_other_interface;

				is_success = true;
				break;
			}
			connect_it++;
		}*/

		if (!is_success)
		{
			//如果没现成的连接  就添加一个
			out_connect_list.push_back(ovr_core::OvrNodeConnect(a_local_interface, a_other_node, a_other_interface));
		}

		//在连接的另一端 添加输入链接信息
		((OvrVisualNode*)a_other_node)->AddInConnect(a_local_interface, this, a_other_interface);
		
		return true;
	}

	bool OvrVisualNode::RmvOutConnect(const OvrString& a_local_interface)
	{
		bool is_success = false;
		auto connect_it = out_connect_list.begin();
		while (connect_it != out_connect_list.end())
		{
			if (connect_it->local_interface == a_local_interface)
			{
				if (nullptr != connect_it->other_node)
				{
					//移除下个Node的输入信息
					((OvrVisualNode*)connect_it->other_node)->RmvInConnect(connect_it->other_interface);
				}
				connect_it = out_connect_list.erase(connect_it);
				is_success = true;
				//break;  可能会有多个
			}
			else 
			{
				connect_it++;
			}
		}
		return is_success;
	}

	void OvrVisualNode::BreakAllConnect()
	{
		for (auto connect_it = out_connect_list.begin(); connect_it != out_connect_list.end(); connect_it++)
		{
			if (nullptr != connect_it->other_node)
			{
				//移除下个Node的输入信息
				((OvrVisualNode*)connect_it->other_node)->RmvInConnect(connect_it->other_interface);
			}
		}

		//输出上端输出信息时  会移除本node的输入信息 所以使用备份进行移除
		std::list<ovr_core::OvrNodeConnect> temp_in_connect_list = in_connect_list;
		for (auto connect_it = temp_in_connect_list.begin(); connect_it != temp_in_connect_list.end(); connect_it++)
		{
			if (nullptr != connect_it->other_node)
			{
				//移除上个的Node的输入信息
				connect_it->other_node->RmvOutConnect(connect_it->other_interface);
			}
		}

		out_connect_list.clear();
		in_connect_list.clear();
	}

	ovr::uint32 OvrVisualNode::SetParamValue(bool a_is_in, const OvrString& a_name, const OvrString a_value)
	{
		ovr::uint32 is_set = false;
		ovr_core::OvrNodeParam::Type node_type = ovr_core::OvrNodeParam::kParamInvalid;
		ovr_core::OvrNodeParam* node_param = nullptr;
		//设置相关值
		if (a_is_in)
		{
			for (auto it = in_param_list.begin(); it != in_param_list.end(); it++)
			{
				if (it->unique_name == a_name)
				{
					it->value = a_value;
					is_set = 1;
					node_type = it->type;
					node_param = &(*it);
					break;
				}
			}
		}
		else 
		{
			for (auto it = out_param_list.begin(); it != out_param_list.end(); it++)
			{
				if (it->unique_name == a_name)
				{
					it->value = a_value;
					is_set = 1;
					node_type = it->type;
					node_param = &(*it);
					break;
				}
			}
		}
		if (0 == is_set)
		{
			//未设置成功
			return 0;
		}
		is_set = 0;
		switch (node_type)
		{
		case ovr_core::OvrNodeParam::kParamInt:
			is_set = SetValue(a_is_in, a_name, atoi(a_value.GetStringConst()));
			break;
		case ovr_core::OvrNodeParam::kParamFloat:
			is_set = SetValue(a_is_in, a_name, (float)atof(a_value.GetStringConst()));
			break;
		case ovr_core::OvrNodeParam::kParamBool:
			is_set = SetValue(a_is_in, a_name, atoi(a_value.GetStringConst()) > 0);
			break;
		case ovr_core::OvrNodeParam::kParamString:
			is_set = SetValue(a_is_in, a_name, a_value);
			break;
		case ovr_core::OvrNodeParam::kParamVector3:
			{
				std::list<OvrString> value_list;
				if (3 == a_value.Split(':', value_list))
				{
					OvrVector3 value_vector;
					ovr::uint32 index = 0;
					for (auto value_it = value_list.begin(); value_it != value_list.end(); value_it++)
					{
						value_vector[index++] = (float)atof(value_it->GetStringConst());
					}

					is_set = SetValue(a_is_in, a_name, value_vector);
				}
			}
			break;
		case ovr_core::OvrNodeParam::kParamVector4:
		{
			OvrVector4 value_vector;
			if (GetVector4(a_value, value_vector))
			{
				is_set = SetValue(a_is_in, a_name, value_vector);
			}
		}
			break;
		case ovr_core::OvrNodeParam::kParamResource:
			//这两个需要在渲染线程中加载
			SetValue(a_is_in, a_name, a_value);
			is_set = 1;
			break;
		case ovr_core::OvrNodeParam::kParamActor:
		{
			ovr_engine::OvrActor* actor_value = node_cluster->GetOwnSequence()->GetScene()->GetActorByUniqueName(a_value);
			if (nullptr != actor_value)
			{
				node_param->value_display = actor_value->GetDisplayName();
				is_set = SetValue(a_is_in, a_name, actor_value);
			}
		}
			break;
		case ovr_core::OvrNodeParam::kParamSequence:
		{
			ovr::int32 pos = a_value.FindLastCharPos(':');
			if (pos <= 0 )
			{
				break;
			}
			// 获取Sequence unique name
			OvrString sequence_unique_name = a_value.SubString(0, pos);

			//获取后面的索引  如果没有值  默认为0
			ovr::uint64 frame_index = 0;
			if (a_value.GetStringSize() > (pos + 1u))
			{
				OvrString mark_name = a_value.SubString(pos + 1, a_value.GetStringSize() - pos-1);
				if (mark_name.GetStringSize() > 0)
				{
					frame_index = atoll(mark_name.GetStringConst());
				}
			}
			
			is_set = SetValue(a_is_in, a_name, nullptr, frame_index);
		}
			break;
		case ovr_core::OvrNodeParam::kParamColor:
		{
			OvrVector4 value_vector;
			if (GetVector4(a_value, value_vector))
			{
				is_set = SetValue(a_is_in, a_name, value_vector);
			}
		}
			break;
		default:			
			break;
		}
		return is_set;
	}

	bool OvrVisualNode::AddInConnect(const OvrString& a_other_interface, ovr_core::IOvrVisualNode* a_other_node, const OvrString& a_local_interface)
	{
		/*auto connect_it = in_connect_list.begin();
		while (connect_it != in_connect_list.end())
		{
			if (connect_it->local_interface == a_local_interface)
			{
				connect_it->other_node = a_other_node;
				connect_it->other_interface = a_other_interface;
				break;
			}
			connect_it++;
		}

		if (connect_it != in_connect_list.end())
		{
			return true;
		}*/

		in_connect_list.push_back(ovr_core::OvrNodeConnect(a_local_interface, a_other_node, a_other_interface));
		return true;
	}

	bool OvrVisualNode::RmvInConnect(const OvrString& a_local_interface)
	{
		bool is_success = false;
		auto connect_it = in_connect_list.begin();
		while (connect_it != in_connect_list.end())
		{
			if (connect_it->local_interface == a_local_interface)
			{
				in_connect_list.erase(connect_it);
				is_success = true;
				break;
			}
			connect_it++;
		}
		return is_success;
	}

	//解析字符串 获取相应的值
	bool OvrVisualNode::GetVector4(const OvrString& a_string, OvrVector4& a_vector4) 
	{
		std::list<OvrString> value_list;
		if (4 == a_string.Split(':', value_list))
		{
			ovr::uint32 index = 0;
			for (auto value_it = value_list.begin(); value_it != value_list.end(); value_it++)
			{
				a_vector4[index++] = (float)atof(value_it->GetStringConst());
			}

			return true;
		}
		return false;
	}
}
