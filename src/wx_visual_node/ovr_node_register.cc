﻿#include "headers.h"
#include "ovr_node_id_define.h"
#include "ovr_event_node.h"
#include "ovr_focus_node.h"
#include "ovr_operator_node.h"
#include "ovr_visual_node.h"
#include "ovr_variable_node.h"
#include "ovr_node_register.h"
#include "ovr_sprite_picture_node.h"
#include "ovr_transform_node.h"
#include "ovr_feather_node.h"
#include "ovr_camera_node.h"
#include <wx_core/visual_node/i_ovr_node_manager.h>

namespace ovr_node
{
	bool OvrNodeRegister::Init() 
	{
		//Event node
		RegisterNode(OvrNodeClassID::event_begin_id, OvrRegisterNodeInfo(OvrNodeClassID::event_begin_category, CreateNodeTmpl<OvrEventNode>));
		RegisterNode(OvrNodeClassID::event_update_id, OvrRegisterNodeInfo(OvrNodeClassID::event_update_category,CreateNodeTmpl<OvrEventNode>));
		RegisterNode(OvrNodeClassID::event_end_id, OvrRegisterNodeInfo(OvrNodeClassID::event_end_category,CreateNodeTmpl<OvrEventNode>));

		//操作Node
		RegisterNode(OvrNodeClassID::operator_focus_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_focus_category, CreateNodeTmpl<OvrFocusNode>));
		RegisterNode(OvrNodeClassID::operator_multi_focus_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_multi_focus_category, CreateNodeTmpl<OvrMultiFocusNode>));
		RegisterNode(OvrNodeClassID::operator_get_focus_actor_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_get_focus_actor_category, CreateNodeTmpl<OvrGetFocusActorNode>));
		RegisterNode(OvrNodeClassID::operator_enable_actor_focus_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_enable_actor_focus_category, CreateNodeTmpl<OvrEnableActorFocusNode>));
		RegisterNode(OvrNodeClassID::operator_goto_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_goto_category, CreateNodeTmpl<OvrSequenceGotoNode>));
		RegisterNode(OvrNodeClassID::operator_change_texture_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_change_texture_category, CreateNodeTmpl<OvrChangeTextureNode>));
		RegisterNode(OvrNodeClassID::operator_change_diffuse_texture_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_change_diffuse_texture_category, CreateNodeTmpl<OvrChangeDiffuseTextureNode>));
		RegisterNode(OvrNodeClassID::operator_play_audio_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_play_audio_category, CreateNodeTmpl<OvrPlaySoundNode>));
		RegisterNode(OvrNodeClassID::operator_timer_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_timer_category ,CreateNodeTmpl<OvrTimerNode>));
		RegisterNode(OvrNodeClassID::operator_alternate_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_alternate_category,CreateNodeTmpl<OvrAlternateNode>));
		RegisterNode(OvrNodeClassID::operator_follow_camera_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_follow_camera_category,CreateNodeTmpl<OvrFollowCameraNode>));
		RegisterNode(OvrNodeClassID::operator_camera_front_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_camera_front_category, CreateNodeTmpl<OvrSetCameraFrontNode>));
		RegisterNode(OvrNodeClassID::operator_visiable_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_visiable_category ,CreateNodeTmpl<OvrSetVisiableNode>));
		RegisterNode(OvrNodeClassID::operator_sprite_picture_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_sprite_picture_category,CreateNodeTmpl<OvrSpritePicture>));
		RegisterNode(OvrNodeClassID::operator_set_main_camera_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_set_main_camera_category, CreateNodeTmpl<OvrSetMainCameraNode>));
		RegisterNode(OvrNodeClassID::operator_set_ambient_color_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_set_ambient_color_category, CreateNodeTmpl<OvrSetAmbientColorNode>));
		RegisterNode(OvrNodeClassID::operator_branch_node_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_branch_category, CreateNodeTmpl<OvrBranchNode>));
		RegisterNode(OvrNodeClassID::operator_switch_actor_node_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_switch_actor_category, CreateNodeTmpl<OvrSwitchActorNode>));
		RegisterNode(OvrNodeClassID::operator_set_text_node_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_set_text_category, CreateNodeTmpl<OvrSetTextNode>));

		RegisterNode(OvrNodeClassID::operator_movement_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_movement_category, CreateNodeTmpl<OvrMovementNode>));
		RegisterNode(OvrNodeClassID::operator_rotation_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_rotation_category, CreateNodeTmpl<OvrRotationNode>));
		RegisterNode(OvrNodeClassID::operator_move_position_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_move_position_category, CreateNodeTmpl<OvrMovePositionNode>));

		//变量Node
		RegisterNode(OvrNodeClassID::variable_actor_id, OvrRegisterNodeInfo(OvrNodeClassID::variable_actor_category, CreateNodeTmpl<OvrActorNode>));

		//特效Node
		RegisterNode(OvrNodeClassID::operator_effect_feather_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_effect_feather_category, CreateNodeTmpl<OvrFeatherNode>));

		RegisterNode(OvrNodeClassID::operator_set_status_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_set_status_category, CreateNodeTmpl<OvrSetStatusNode>));
		RegisterNode(OvrNodeClassID::operator_switch_status_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_switch_status_category, CreateNodeTmpl<OvrStatusSwitchNode>));

		RegisterNode(OvrNodeClassID::operator_set_actor_diffusecolor_id, OvrRegisterNodeInfo(OvrNodeClassID::operator_set_actor_diffusecolor_category, CreateNodeTmpl<OvrMeshActorDiffuseColorNode>));

		//不能放到事件编辑器中的Node
		std::map<OvrString, bool> event_except_node_map;
		event_except_node_map[OvrNodeClassID::event_update_id] = true;
		event_except_node_map[OvrNodeClassID::event_end_id] = true;
		event_except_node_map[OvrNodeClassID::operator_focus_id] = true;
		event_except_node_map[OvrNodeClassID::operator_timer_id] = true;

		//初始化可以放到相应编辑中的Node信息
		for (auto node_it = create_node_function_map.begin(); node_it != create_node_function_map.end(); node_it++)
		{
			if (event_except_node_map.find(node_it->first) == event_except_node_map.end())
			{
				//放到Event列表中
				event_node_id_list.push_back(ovr_core::OvrNodeInfo(node_it->first, node_it->second.category));
			}
			//放到交互区Node中
			area_node_id_list.push_back(ovr_core::OvrNodeInfo(node_it->first, node_it->second.category));
		}
		
		//设置唯一实例的Node
		unique_node_map.insert(std::make_pair(OvrNodeClassID::event_begin_id, true));
		unique_node_map.insert(std::make_pair(OvrNodeClassID::event_update_id, true));
		unique_node_map.insert(std::make_pair(OvrNodeClassID::event_end_id, true));

		//保存Actor ID
		actor_node_id = OvrNodeClassID::variable_actor_id;
		return true;
	}

	void OvrNodeRegister::Uninit() 
	{
		event_node_id_list.clear();
		area_node_id_list.clear();
		create_node_function_map.clear();
	}

	//根据classID 创建Node
	OvrVisualNode*	 OvrNodeRegister::CreateNode(const OvrString& a_class_id, OvrNodeCluster* a_cluster)
	{
		auto it = create_node_function_map.find(a_class_id);
		if (it != create_node_function_map.end())
		{
			OvrVisualNode* new_node = it->second.create_function();
			new_node->SetCluster(a_cluster);
			new_node->SetNodeID(a_class_id);
			new_node->SetClassCategory(it->second.category);
			new_node->SetDisplayName(it->second.display_name);

			return new_node;
		}
		return nullptr;
	}

	void OvrNodeRegister::DestoryNode(OvrVisualNode* a_node)
	{
		if (nullptr != a_node)
		{
			delete a_node;
			a_node = nullptr;
		}
	}

	//是不是唯一实例
	bool OvrNodeRegister::IsUniqueNode(const OvrString& a_node_id) 
	{
		return unique_node_map.find(a_node_id) != unique_node_map.end();
	}

	bool OvrNodeRegister::RegisterNode(const OvrString& a_class_id, const OvrRegisterNodeInfo& a_node_info)
	{
		auto it = create_node_function_map.find(a_class_id);
		if (it == create_node_function_map.end())
		{
			create_node_function_map[a_class_id] = a_node_info;
			return true;
		}
		return false;
	}

	template<typename T>
	OvrVisualNode * OvrNodeRegister::CreateNodeTmpl()
	{
		return new T;
	}
}
