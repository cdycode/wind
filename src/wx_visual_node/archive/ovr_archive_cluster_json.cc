﻿#include "../headers.h"
#include <wx_core/visual_node/i_ovr_visual_node.h>
#include <wx_base/wx_small_file.h>
#include "../ovr_node_cluster.h"
#include "../ovr_visual_node.h"
#include "ovr_archive_cluster_json.h"

namespace ovr_node 
{
	bool OvrArchiveClusterJson::Load(OvrNodeCluster* a_area)
	{
		//打开文件
		OvrString file_path = ovr_core::OvrCore::GetIns()->GetWorkDir() + a_area->GetFilePath();
		OvrSmallFile small_file;
		if (!small_file.Open(file_path))
		{
			return false;
		}

		//解析数据
		RAPIDJSON_NAMESPACE::Document doc;
		if (doc.ParseInsitu((char*)small_file.GetFileData()).HasParseError()) 
		{
			return false;
		}
			
		/*doc.Parse<0>(small_file.GetFileData());*/
		if (!doc.IsObject())
		{
			return false;
		}

		//设置显示名
		OvrString display_name;
		OvrFileTools::GetFileAllNameFromPath(a_area->GetFilePath().GetStringConst(), display_name);
		a_area->SetDisplayName(display_name);

		//加载内容
		if (!LoadHeader(a_area, doc) || !LoadAllNode(a_area, doc) || !LoadConnect(a_area, doc))
		{
			return false;
		}
		return true;
	}

	bool OvrArchiveClusterJson::Save(OvrNodeCluster* a_area)
	{
		if (nullptr == a_area)
		{
			return false;
		}
		//构建Json实例
		RAPIDJSON_NAMESPACE::StringBuffer s;
		RAPIDJSON_NAMESPACE::PrettyWriter<RAPIDJSON_NAMESPACE::StringBuffer> writer(s);

		//开始写json文件
		writer.StartObject();

		//写文件头
		SaveHeader(a_area, writer);

		//保存Node
		SaveAllNode(a_area, writer);

		//保存连接
		SaveConnect(a_area, writer);

		//结束写json文件
		writer.EndObject();
		
		//写文件
		OvrString file_path = ovr_core::OvrCore::GetIns()->GetWorkDir() + a_area->GetFilePath();
		return OvrSmallFile::WriteFile(file_path, s.GetString(), s.GetSize());
	}

	void OvrArchiveClusterJson::SaveHeader(OvrNodeCluster* a_area, RAPIDJSON_NAMESPACE::PrettyWriter<RAPIDJSON_NAMESPACE::StringBuffer> & a_writer)
	{
		//header
		a_writer.Key("header");
		a_writer.StartObject();
		if (ovr_core::IOvrNodeCluster::kEvent == a_area->GetType())
		{
			a_writer.Key("fourcc");			a_writer.String("CTET");
		}
		else
		{
			a_writer.Key("fourcc");			a_writer.String("CTAR");
		}
		
		a_writer.Key("major version");	a_writer.Uint(0);
		a_writer.Key("minor version");	a_writer.Uint(0);
		a_writer.Key("unique name");	a_writer.String(a_area->GetUniqueName().GetStringConst());
		a_writer.Key("display name");	a_writer.String(a_area->GetDisplayName().GetStringConst());
		a_writer.Key("view scale");		a_writer.Double(a_area->GetViewScale());
		a_writer.Key("view x");			a_writer.Double(a_area->GetViewX());
		a_writer.Key("view y");			a_writer.Double(a_area->GetViewY());
		a_writer.EndObject();
	}

	void OvrArchiveClusterJson::SaveAllNode(OvrNodeCluster* a_area, RAPIDJSON_NAMESPACE::PrettyWriter<RAPIDJSON_NAMESPACE::StringBuffer> & a_writer)
	{
		//以数组方式 写所有的Node
		a_writer.Key("node");
		a_writer.StartArray();
		
		auto all_note_map = a_area->GetAllNodeMap();
		for (auto it = all_note_map.begin(); it != all_note_map.end(); it++)
		{
			//写Node
			SaveNode((OvrVisualNode*)it->second, a_writer);
		}

		a_writer.EndArray();
	}

	void OvrArchiveClusterJson::SaveNode(ovr_core::IOvrVisualNode* a_node, RAPIDJSON_NAMESPACE::PrettyWriter<RAPIDJSON_NAMESPACE::StringBuffer> & a_writer)
	{
		a_writer.StartObject();

		//基本信息
		a_writer.Key("node id");		a_writer.String(a_node->GetNodeID().GetStringConst());
		a_writer.Key("unique name");	a_writer.String(a_node->GetUniqueName().GetStringConst());
		a_writer.Key("position x");		a_writer.Int(a_node->GetPosistionX());
		a_writer.Key("position y");		a_writer.Int(a_node->GetPosistionY());

		//获取所有的参数
		auto in_param_list = a_node->GetInParamList();
		//输入参数
		a_writer.Key("in param");
		a_writer.StartArray();
		for (auto it = in_param_list.begin(); it != in_param_list.end(); it++)
		{
			//写参数信息
			a_writer.StartObject();
			a_writer.Key("name");	a_writer.String(it->unique_name.GetStringConst());
			a_writer.Key("value");	a_writer.String(it->value.GetStringConst());
			a_writer.EndObject();
		}
		a_writer.EndArray();

		//输出参数
		auto out_param_list = a_node->GetOutParamList();
		a_writer.Key("out param");
		a_writer.StartArray();
		for (auto it = out_param_list.begin(); it != out_param_list.end(); it++)
		{
			//写参数信息
			a_writer.StartObject();
			a_writer.Key("name");	a_writer.String(it->unique_name.GetStringConst());
			a_writer.Key("value");	a_writer.String(it->value.GetStringConst());
			a_writer.EndObject();
		}
		a_writer.EndArray();

		a_writer.EndObject();
	}

	void OvrArchiveClusterJson::SaveConnect(OvrNodeCluster* a_area, RAPIDJSON_NAMESPACE::PrettyWriter<RAPIDJSON_NAMESPACE::StringBuffer> & a_writer)
	{
		//以数组方式写所有的连接
		a_writer.Key("connect");
		a_writer.StartArray();

		auto all_node = a_area->GetAllNodeMap();

		for (auto it = all_node.begin(); it != all_node.end(); it++)
		{
			auto connect_list = it->second->GetOutConnects();
			for (auto connect_it = connect_list.begin(); connect_it != connect_list.end(); connect_it++)
			{
				a_writer.StartObject();
				a_writer.Key("from node");		a_writer.String(it->second->GetUniqueName().GetStringConst());
				a_writer.Key("from interface");	a_writer.String(connect_it->local_interface.GetStringConst());
				a_writer.Key("to node");		a_writer.String(connect_it->other_node->GetUniqueName().GetStringConst());
				a_writer.Key("to interface");	a_writer.String(connect_it->other_interface.GetStringConst());
				a_writer.EndObject();
			}
		}

		a_writer.EndArray();
	}

	bool OvrArchiveClusterJson::LoadHeader(OvrNodeCluster* a_area, RAPIDJSON_NAMESPACE::Document& a_document)
	{
		if (!a_document.HasMember("header") || !a_document["header"].IsObject())
		{
			return false;
		}

		RAPIDJSON_NAMESPACE::Value &header = a_document["header"];
		
		//unique name
		if (!header.HasMember("unique name") || !header["unique name"].IsString())
		{
			return false;
		}
		a_area->SetUniqueName(header["unique name"].GetString());

		//display name
		if (!header.HasMember("display name") || !header["display name"].IsString())
		{
			return false;
		}
		a_area->SetUniqueName(header["display name"].GetString());

		//view scale
		if (!header.HasMember("view scale") || !header["view scale"].IsDouble())
		{
			return false;
		}
		a_area->SetViewScale((float)header["view scale"].GetDouble());

		//view x y
		if (!header.HasMember("view x") || !header["view x"].IsDouble() || \
			!header.HasMember("view y") || !header["view y"].IsDouble())
		{
			return false;
		}
		a_area->SetViewOffset((float)header["view x"].GetDouble(), (float)header["view y"].GetDouble());

		return true;
	}

	bool OvrArchiveClusterJson::LoadAllNode(OvrNodeCluster* a_area, RAPIDJSON_NAMESPACE::Document& a_document)
	{
		if (!a_document.HasMember("node") || !a_document["node"].IsArray())
		{
			return false;
		}
		RAPIDJSON_NAMESPACE::Value &node_array = a_document["node"];

		for (ovr::uint32 i = 0; i < node_array.Size(); i++)
		{
			if (!LoadNode(a_area, node_array[i])) 
			{
				return false;
			}
		}

		return true;
	}

	bool OvrArchiveClusterJson::LoadNode(OvrNodeCluster* a_area, RAPIDJSON_NAMESPACE::Value & a_json_node)
	{
		if (!a_json_node.IsObject())
		{
			return false;
		}

		//class id
		if (!a_json_node.HasMember("node id") || !a_json_node["node id"].IsString()) 
		{
			return false;
		}
		//unique name
		if (!a_json_node.HasMember("unique name") || !a_json_node["unique name"].IsString())
		{
			return false;
		}

		ovr_core::IOvrVisualNode* new_node = a_area->CreateNode(a_json_node["node id"].GetString(), a_json_node["unique name"].GetString());
		if (nullptr == new_node)
		{
			return false;
		}

		//position
		if (!a_json_node.HasMember("position x") || !a_json_node["position x"].IsInt() ||\
			!a_json_node.HasMember("position y") || !a_json_node["position y"].IsInt())
		{
			return false;
		}
		new_node->SetPosition(a_json_node["position x"].GetInt(), a_json_node["position y"].GetInt());

		//in param 
		if (!a_json_node.HasMember("in param") || !a_json_node["in param"].IsArray())
		{
			return false;
		}
		RAPIDJSON_NAMESPACE::Value & in_param_array = a_json_node["in param"];
		for (ovr::uint32 i = 0; i < in_param_array.Size(); i++)
		{
			if (!in_param_array[i].IsObject()) 
			{
				return false;
			}
			RAPIDJSON_NAMESPACE::Value& param_json = in_param_array[i];
			//param
			new_node->SetParamValue(true, param_json["name"].GetString(), param_json["value"].GetString());
		}

		//out param 
		if (!a_json_node.HasMember("out param") || !a_json_node["out param"].IsArray())
		{
			return false;
		}
		RAPIDJSON_NAMESPACE::Value & out_param_array = a_json_node["out param"];
		for (ovr::uint32 i = 0; i < out_param_array.Size(); i++)
		{
			if (!out_param_array[i].IsObject())
			{
				return false;
			}
			RAPIDJSON_NAMESPACE::Value& param_json = out_param_array[i];
			//param
			new_node->SetParamValue(false, param_json["name"].GetString(), param_json["value"].GetString());
		}

		return true;
	}

	bool OvrArchiveClusterJson::LoadConnect(OvrNodeCluster* a_area, RAPIDJSON_NAMESPACE::Document& a_document)
	{
		if (!a_document.HasMember("connect") || !a_document["connect"].IsArray())
		{
			return false;
		}

		RAPIDJSON_NAMESPACE::Value &connect_array = a_document["connect"];
		for (ovr::uint32 i = 0; i < connect_array.Size(); i++)
		{
			if (!connect_array[i].IsObject()) 
			{
				return false;
			}
			RAPIDJSON_NAMESPACE::Value &connect = connect_array[i];

			//判断json中的数据是否有
			if (!connect.HasMember("from node") || !connect["from node"].IsString() ||\
				!connect.HasMember("to node") || !connect["to node"].IsString() || \
				!connect.HasMember("from interface") || !connect["from interface"].IsString() || \
				!connect.HasMember("to interface") || !connect["to interface"].IsString() )
			{
				return false;
			}

			//获取Node
			ovr_core::IOvrVisualNode* from_node = a_area->GetNode(connect["from node"].GetString());
			ovr_core::IOvrVisualNode* to_node = a_area->GetNode(connect["to node"].GetString());
			if (nullptr == from_node || nullptr == to_node)
			{
				return false;
			}
			//添加连接
			if (!from_node->AddOutConnect(connect["from interface"].GetString(), to_node, connect["to interface"].GetString())) 
			{
				return false;
			}
		}
		return true;
	}
}
