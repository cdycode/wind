﻿#ifndef OVR_VISUAL_NODE_OVR_ARCHIVE_CLUSTER_JSON_H_
#define OVR_VISUAL_NODE_OVR_ARCHIVE_CLUSTER_JSON_H_

#include <rapidjson/prettywriter.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/document.h>

namespace ovr_core
{
	class IOvrVisualNode;
}

namespace ovr_node 
{
	class OvrVisualNode;
	class OvrNodeCluster;
	class OvrArchiveClusterJson 
	{
	public:
		OvrArchiveClusterJson() {}
		~OvrArchiveClusterJson() {}

		bool	Load(OvrNodeCluster* a_area);
		bool	Save(OvrNodeCluster* a_area);
		
	private:
		void	SaveHeader(OvrNodeCluster* a_area, RAPIDJSON_NAMESPACE::PrettyWriter<RAPIDJSON_NAMESPACE::StringBuffer> & a_writer);
		void	SaveAllNode(OvrNodeCluster* a_area, RAPIDJSON_NAMESPACE::PrettyWriter<RAPIDJSON_NAMESPACE::StringBuffer> & a_writer);
		void	SaveNode(ovr_core::IOvrVisualNode* a_node, RAPIDJSON_NAMESPACE::PrettyWriter<RAPIDJSON_NAMESPACE::StringBuffer> & a_writer);
		void	SaveConnect(OvrNodeCluster* a_area, RAPIDJSON_NAMESPACE::PrettyWriter<RAPIDJSON_NAMESPACE::StringBuffer> & a_writer);

		bool	LoadHeader(OvrNodeCluster* a_area, RAPIDJSON_NAMESPACE::Document& a_document);
		bool	LoadAllNode(OvrNodeCluster* a_area, RAPIDJSON_NAMESPACE::Document& a_document);
		bool	LoadNode(OvrNodeCluster* a_area, RAPIDJSON_NAMESPACE::Value & a_json_node);
		bool	LoadConnect(OvrNodeCluster* a_area, RAPIDJSON_NAMESPACE::Document& a_document);
	};
}

#endif
