﻿#include "headers.h"
#include "ovr_sprite_picture_node.h"
#include <wx_engine/ovr_texture_manager.h>
#include <wx_engine/ovr_actor.h>
//#include <ovr_engine/ovr_mesh_actor.h>
#include "ovr_variable_node.h"
#include "ovr_node_id_define.h"
#include <wx_core/ovr_scene_context.h>
#include <wx_core/ovr_sequence.h>
#include "ovr_node_cluster.h"

namespace ovr_node 
{
	OvrSpritePicture::OvrSpritePicture() 
	{
		//接口
		interface_list.push_back(ovr_core::OvrNodeInterface(true, kUpdateName, kUpdateID));
		interface_list.push_back(ovr_core::OvrNodeInterface(true, kResetName, kResetID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, "", kOutputID));

		//输入参数
		in_param_list.push_back(ovr_core::OvrNodeParam(kObjectName, kObjectID,ovr_core::OvrNodeParam::kParamActor));
		in_param_list.push_back(ovr_core::OvrNodeParam(kRowNumName, kRowNumID,ovr_core::OvrNodeParam::kParamInt, "1"));
		in_param_list.push_back(ovr_core::OvrNodeParam(kColumnNumName, kColumnNumID,ovr_core::OvrNodeParam::kParamInt, "1"));
		in_param_list.push_back(ovr_core::OvrNodeParam(kDefaultIndexName, kDefaultIndexID,ovr_core::OvrNodeParam::kParamInt, "0"));
		in_param_list.push_back(ovr_core::OvrNodeParam(kLoopTimeName, kLoopTimeID,ovr_core::OvrNodeParam::kParamFloat, "1.0"));
	}

	void OvrSpritePicture::BeginPlay() 
	{
		actor_node = nullptr;
		texture_layer = nullptr;
		actor = nullptr;

		unit_x = 1.0f / column_num;
		unit_y = 1.0f / row_num;
		total_unit = column_num*row_num;
		unit_time = loop_time / total_unit;
		current_index = default_index;

		//填充变量输入
		for (auto it = in_connect_list.begin(); it != in_connect_list.end(); it++)
		{
			if (it->local_interface == kObjectID)
			{
				actor_node = (OvrActorNode*)it->other_node;

				actor = actor_node->GetActor();
			}
		}

		//if (nullptr == actor || actor->GetType() != ovr_engine::kObjectMesh)
		{
			return;
		}

		//将挂在相机的前面  所以不需要进行相交测试
		//actor->SetIntersetTestEnable(false);

		/*ovr_engine::OvrMaterial* material = ((ovr_engine::OvrMeshActor*)actor)->GetMaterial(0);
		if (nullptr != material)
		{
			texture_layer = material->GetShaderParameters()->GetTextureLayer("Texture0");
			if (nullptr != texture_layer)
			{
				texture_layer->SetTextureTilingAndOffset(unit_x, unit_y, current_index * unit_x, current_index * unit_y);
			}
		}*/

		//填充输出接口
		for (auto it = out_connect_list.begin(); it != out_connect_list.end(); it++)
		{
			if (it->local_interface == kOutputID)
			{
				out_node = (OvrOperatorNode*)it->other_node;
				out_interface = it->other_interface;
			}
		}

		ResetPicture();
	}

	void OvrSpritePicture::EndPlay() 
	{

	}

	bool OvrSpritePicture::Update(float a_delta_time, const OvrString& a_interface_name) 
	{
		if (nullptr == texture_layer)
		{
			return false;
		}
		
		if (a_interface_name == kUpdateID)
		{
			//更新
			accum_gaze_time += a_delta_time;
			ovr::uint32 new_index = (ovr::uint32)(accum_gaze_time / unit_time);
			if (new_index != current_index)
			{
				//避免最大值的出现
				if (new_index >= total_unit)
				{
					new_index = total_unit - 1;
					accum_gaze_time -= loop_time;
				}

				//计算偏移
				ovr::uint32 x_offset = new_index % column_num;
				ovr::uint32 y_offset = new_index / row_num;

				//修改UV
				texture_layer->SetTextureTilingAndOffset(unit_x, unit_y, x_offset * unit_x, y_offset * unit_y);

				current_index = new_index;
			}
			
		}
		else 
		{
			ResetPicture();
		}

		if (nullptr != out_node)
		{
			out_node->Update(a_delta_time, out_interface);
		}
		return false;
	}

	ovr::uint32 OvrSpritePicture::SetValue(bool a_is_in, const OvrString& a_name, int a_value)
	{
		if (a_name == kRowNumID)
		{
			row_num = a_value;
		}
		else if (a_name == kColumnNumID)
		{
			column_num = a_value;
		}
		else if (a_name == kDefaultIndexID)
		{
			default_index = a_value;
		}
		else 
		{
			return 0;
		}
		return 1;
	}

	ovr::uint32 OvrSpritePicture::SetValue(bool a_is_in, const OvrString& a_name, float a_value)
	{
		if (a_name == kLoopTimeID)
		{
			loop_time = a_value;
			return 1;
		}
		return 0;
	}

	ovr::uint32 OvrSpritePicture::SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value)
	{
		if (a_name == kObjectID)
		{
			actor = a_value;
			return 1;
		}
		return 0;
	}

	void OvrSpritePicture::ResetPicture() 
	{
		//重置
		current_index = 0;
		accum_gaze_time = 0.0f;
		texture_layer->SetTextureTilingAndOffset(unit_x, unit_y, current_index * unit_x, current_index * unit_y);
	}
}
