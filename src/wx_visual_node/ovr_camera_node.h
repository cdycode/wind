﻿#ifndef OVR_VISUAL_NODE_OVR_CAMERA_NODE_H_
#define OVR_VISUAL_NODE_OVR_CAMERA_NODE_H_

#include "ovr_operator_node.h"

namespace ovr_node 
{
	class OvrSetCameraFrontNode : public OvrNormalOperatorNode
	{
	public:
		OvrSetCameraFrontNode();
		virtual ~OvrSetCameraFrontNode() {}

		virtual void BeginPlay() override;

		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, float a_value) override;
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, bool a_value) override;

	protected:
		//执行体 需要由子类实现
		virtual bool	Doing(float a_delta_time) override;

	private:
		ovr_engine::OvrActor*	actor		= nullptr;
		float					distance	= 0.0f;
		bool					is_vertical = false;
	};

}

#endif