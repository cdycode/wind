﻿#ifndef OVR_VISUAL_NODE_OVR_EVENT_NODE_H_
#define OVR_VISUAL_NODE_OVR_EVENT_NODE_H_

#include "ovr_visual_node.h"

namespace ovr_node 
{
	class OvrEventNode : public OvrVisualNode
	{
	public:
		OvrEventNode();
		virtual ~OvrEventNode() {}

		//时间单位为秒
		virtual bool Update(float a_delta_time, const OvrString& a_interface_name) override;

	};
}

#endif
