﻿#include "headers.h"
#include "ovr_camera_node.h"
#include "ovr_variable_node.h"
#include <wx_core/ovr_scene_context.h>
#include <wx_core/ovr_scene_camera.h>
#include "ovr_node_cluster.h"
#include <wx_core/ovr_sequence.h>

namespace ovr_node 
{
	OvrSetCameraFrontNode::OvrSetCameraFrontNode() 
	{
		display_name = kFrontCameraName;

		in_param_list.push_back(ovr_core::OvrNodeParam(kObjectName, kObjectID,ovr_core::OvrNodeParam::kParamActor));
		in_param_list.push_back(ovr_core::OvrNodeParam(kDistanceName, kDistanceID,ovr_core::OvrNodeParam::kParamFloat, "1.0f"));
		in_param_list.push_back(ovr_core::OvrNodeParam(kVerticalName, kVerticalID,ovr_core::OvrNodeParam::kParamBool, "1"));
	}
	
	void OvrSetCameraFrontNode::BeginPlay() 
	{
		actor = nullptr;

		//填充变量输入
		for (auto it = in_connect_list.begin(); it != in_connect_list.end(); it++)
		{
			if (it->local_interface == kObjectID)
			{
				//直接赋值
				actor = ((OvrActorNode*)it->other_node)->GetActor();
			}
		}
	}

	ovr::uint32	OvrSetCameraFrontNode::SetValue(bool a_is_in, const OvrString& a_name, float a_value) 
	{
		if (a_name == kDistanceID)
		{
			distance = a_value;
			return 1;
		}
		return 0;
	}

	ovr::uint32	OvrSetCameraFrontNode::SetValue(bool a_is_in, const OvrString& a_name, bool a_value) 
	{
		if (a_name == kVerticalID)
		{
			is_vertical = a_value;
			return 1;
		}
		return 0;
	}

	//执行体 需要由子类实现
	bool OvrSetCameraFrontNode::Doing(float a_delta_time) 
	{
		if (nullptr != actor)
		{
			/*ovr_engine::OvrCameraActor* camera_actor = node_cluster->GetOwnSequence()->GetScene()->GetSceneCamera()->GetCurrentCamera();
			OvrQuaternion quaternion = camera_actor->GetQuaternion();

			OvrVector3 up = quaternion*OvrVector3::UNIT_Y;
			OvrVector3 dir = (quaternion*(-OvrVector3::UNIT_Z)) ;

			OvrQuaternion actor_quaternion = OvrQuaternion::make_quat_from_up_dir(up, dir);

			OvrRay3f ray = camera_actor->GeneratePickRay(0.5200f, 0.5200f);
			OvrVector3 new_position = ray.GetOrigin() + ray.GetDir() * distance;
			actor->SetPosition(new_position);
			actor->SetQuaternion(actor_quaternion);*/
		}
		return true;
	}
}
