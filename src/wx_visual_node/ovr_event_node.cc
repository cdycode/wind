﻿#include "headers.h"
#include "ovr_event_node.h"
#include "ovr_operator_node.h"

namespace ovr_node 
{
	OvrEventNode::OvrEventNode()
	{
		display_name = "EventNode";

		interface_list.push_back(ovr_core::OvrNodeInterface(false, "", kOutputID));
	}

	bool OvrEventNode::Update(float a_delta_time, const OvrString& a_interface_name)
	{
		for (auto it = out_connect_list.begin(); it != out_connect_list.end(); it++)
		{
			OvrOperatorNode* operator_node = (OvrOperatorNode*)it->other_node;
			operator_node->Update(a_delta_time, it->other_interface);
		}
		return true;
	}
}