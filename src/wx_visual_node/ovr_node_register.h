﻿#ifndef OVR_VISUAL_NODE_OVR_NODE_REGISTER_H_
#define OVR_VISUAL_NODE_OVR_NODE_REGISTER_H_

namespace ovr_core
{
	class OvrNodeInfo;
}

namespace ovr_node 
{
	class OvrVisualNode;
	typedef OvrVisualNode* (*CreateNodeFunction)();

	//注册信息
	struct OvrRegisterNodeInfo
	{
	public:
		OvrRegisterNodeInfo() {}
		OvrRegisterNodeInfo(const OvrRegisterNodeInfo& a_info) 
		{
			create_function = a_info.create_function;
		}

		OvrRegisterNodeInfo(const OvrString& a_category,CreateNodeFunction a_function)
			:category(a_category)
			,create_function(a_function)
		{
			ovr::int32 index = category.FindLastCharPos('/');
			display_name = category.SubString(index + 1, category.GetBufferLength() - index - 1);
		}

		void operator = (const OvrRegisterNodeInfo& a_info)
		{
			category		= a_info.category;
			create_function = a_info.create_function;
		}

		OvrString			category;
		OvrString			display_name;
		CreateNodeFunction	create_function;
	};

	class OvrNodeCluster;
	class OvrNodeRegister 
	{
	public:
		OvrNodeRegister() {}
		~OvrNodeRegister() {}

		bool	Init();
		void	Uninit();

		//获取ActorNode的Class ID
		const OvrString& GetActorNodeID()const { return actor_node_id;}

		//获取所有的Event
		const std::list<ovr_core::OvrNodeInfo>&	GetEventNodeIDList()const { return event_node_id_list; }
		//获取所有的交互区
		const std::list<ovr_core::OvrNodeInfo>&	GetAreaNodeIDList()const { return area_node_id_list; }

		//根据classID 创建Node
		OvrVisualNode*		CreateNode(const OvrString& a_class_id, OvrNodeCluster* a_cluster);
		void				DestoryNode(OvrVisualNode* a_node);

		//是不是唯一实例
		bool		IsUniqueNode(const OvrString& a_node_id);

	private:
		//注册Node的创建函数
		bool			RegisterNode(const OvrString& a_class_id, const OvrRegisterNodeInfo& a_node_info);

		template<typename T>
		static OvrVisualNode * CreateNodeTmpl();

	private:
		OvrString				actor_node_id;

		//支持Event轨道使用的Node
		std::list<ovr_core::OvrNodeInfo>	event_node_id_list;

		//支持交互轨道使用的Node
		std::list<ovr_core::OvrNodeInfo>	area_node_id_list;

		//Node的注册信息
		std::map<OvrString, OvrRegisterNodeInfo> create_node_function_map;

		//唯一实例
		std::map<OvrString, bool>		unique_node_map;
	};
}

#endif
