﻿#include "headers.h"
#include "ovr_variable_node.h"

namespace ovr_node 
{
	OvrActorNode::OvrActorNode() 
	{
		//变量Node 不用显示名字
		display_name.Reset();

		in_param_list.push_back(ovr_core::OvrNodeParam(kObjectName, kObjectID,ovr_core::OvrNodeParam::kParamActor));

		out_param_list.push_back(ovr_core::OvrNodeParam(kReturnName,kReturnID, ovr_core::OvrNodeParam::kParamActor));
	}

	ovr::uint32 OvrActorNode::SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value)
	{
		if (a_is_in && a_name == kObjectID)
		{
			own_actor = a_value;
			return 1;
		}
		return 0;
	}

}
