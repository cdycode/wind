﻿#ifndef OVR_VISUAL_NODE_OVR_OPERATOR_NODE_H_
#define OVR_VISUAL_NODE_OVR_OPERATOR_NODE_H_

#include "ovr_visual_node.h"
#include <wx_core/async_load/i_ovr_async_load.h>

namespace ovr_engine
{
	class OvrActor;
	class OvrMaterial;
	class IOvrTexture;
	class OvrTextActor;
	class OvrMeshActor;
}

namespace ovr_node 
{
	//封装执行流输出执行接口
	class OvrOperatorNode;
	class OvrOutFlowInfo 
	{
	public:
		OvrOutFlowInfo() {}
		OvrOutFlowInfo(OvrOperatorNode* a_node, const OvrString& a_interface)
			:next_node(a_node)
			,next_interface(a_interface)
		{}
		OvrOutFlowInfo(const OvrOutFlowInfo& a_other) 
		{
			next_node		= a_other.next_node;
			next_interface	= a_other.next_interface;
		}
		~OvrOutFlowInfo() {}

		void operator = (const OvrOutFlowInfo& a_other) 
		{
			next_node		= a_other.next_node;
			next_interface	= a_other.next_interface;
		}

		OvrOperatorNode*	next_node = nullptr;
		OvrString			next_interface;
	};

	class OvrActorNode;
	//操作Node
	class OvrOperatorNode : public OvrVisualNode
	{
	public:
		OvrOperatorNode() {}
		virtual ~OvrOperatorNode() {}

		//a_delta_time 距离上帧的时间
		//a_interface_name 指定连接本Node的哪个接口
		virtual bool	Update(float a_delta_time, const OvrString& a_interface_name) = 0;

	private:
	};

	//只有一个输入  一个输出的执行流
	class OvrNormalOperatorNode : public OvrOperatorNode 
	{
	public:
		OvrNormalOperatorNode();
		virtual ~OvrNormalOperatorNode() {}

		//播放前后的准备
		virtual void BeginPlay() override;
		virtual void EndPlay() override;

		virtual bool	Update(float a_delta_time, const OvrString& a_interface_name) override;

	protected:
		//执行体 需要由子类实现
		virtual bool	Doing(float a_delta_time) = 0;

	private:
		OvrOperatorNode*	out_node = nullptr;
		OvrString			out_node_interface;
	};

	//实现在Sequence上跳转的逻辑
	class OvrSequenceGotoNode : public OvrNormalOperatorNode
	{
	public:
		OvrSequenceGotoNode();
		virtual ~OvrSequenceGotoNode() {}

		
	protected:
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, ovr_core::OvrSequence* a_sequence, ovr::uint64 a_frame_index)override;

		virtual bool Doing(float a_delta_time) override;
	private:
		ovr_core::OvrSequence*	sequence = nullptr;
		ovr::uint64					goto_position = 0;	//以帧为单位
	};

	//实现纹理更换的逻辑
	class OvrChangeTextureNode : public OvrNormalOperatorNode
	{
	public:
		OvrChangeTextureNode();
		virtual ~OvrChangeTextureNode() {}

		virtual void BeginPlay()override;
		virtual void EndPlay()override;

		virtual void AppendAssetFile(std::map<OvrString, bool>& a_file_map) override;
	protected:
		virtual bool	Doing(float a_delta_time) override;

	private:
		ovr_engine::OvrMaterial*		material = nullptr;			//需要修改的材质
		ovr_core::OvrAsyncTexture	diffuse_texture;	//漫反射贴图
		ovr_core::OvrAsyncTexture	emissive_texture;	//自发光贴图
		ovr_core::OvrAsyncTexture	normal_texture;	//法线贴图
		ovr_core::OvrAsyncTexture	specular_texture;	//高光贴图

	};

	//实现简单的更换贴图
	class OvrChangeDiffuseTextureNode : public OvrNormalOperatorNode
	{
	public:
		OvrChangeDiffuseTextureNode();
		virtual ~OvrChangeDiffuseTextureNode() {}

		virtual void BeginPlay()override;

		virtual void AppendAssetFile(std::map<OvrString, bool>& a_file_map) override;

	protected:
		virtual bool	Doing(float a_delta_time) override;

	private:
		ovr_engine::OvrMaterial*		material = nullptr;				//需要修改的材质
		ovr_core::OvrAsyncTexture	diffuse_texture_1;	//漫反射贴图1
		ovr_core::OvrAsyncTexture	diffuse_texture_2;	//漫反射贴图2

		bool	is_first = true;
	};

	class OvrPlaySoundNode : public OvrNormalOperatorNode
	{
	public:
		OvrPlaySoundNode();
		virtual ~OvrPlaySoundNode() {}

		virtual void BeginPlay()override;
		virtual void EndPlay()override;

		virtual void AppendAssetFile(std::map<OvrString, bool>& a_file_map) override;

	private:
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, bool a_value) override;
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, float a_value) override;

	protected:
		virtual bool Doing(float a_delta_time) override;
	private:
		OvrString		audio_file_path;
		bool			is_loop = false;
		float			blend_time = 1.0f;
		float			volume = 1.0f;

	};

	//定时Node
	class OvrTimerNode : public OvrOperatorNode
	{
	public:
		OvrTimerNode();
		virtual ~OvrTimerNode() {}

		virtual void BeginPlay()override;

		virtual bool	Update(float a_delta_time, const OvrString& a_interface_name) override;

	protected:
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, float a_value) override;

	private:
		float	duration = 1.0f;			//定时的时间

		float	accumulated_time = 0.0f;	//累计时间
	};
	//交替执行的Node
	class OvrAlternateNode : public OvrOperatorNode
	{
	public:
		OvrAlternateNode();
		virtual ~OvrAlternateNode() {}

		virtual void	BeginPlay()override;

		virtual bool	Update(float a_delta_time, const OvrString& a_interface_name) override;

	private:
		bool	is_first = true;	//是否是第一个

		OvrOperatorNode*	first_out_node = nullptr;
		OvrString			first_out_node_interface;

		OvrOperatorNode*	second_out_node = nullptr;
		OvrString			second_out_node_interface;
	};

	//跟谁相机设置 功能暂时不可用
	class OvrFollowCameraNode : public OvrNormalOperatorNode
	{
	public:
		OvrFollowCameraNode();
		virtual ~OvrFollowCameraNode() {}

		virtual void	BeginPlay() override;

	protected:
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, bool a_value) override;
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value) override;

		virtual bool	Doing(float a_delta_time) override;
	private:
		bool					is_follow = true;
		ovr_engine::OvrActor*	own_actor = nullptr;

		OvrActorNode*			actor_node = nullptr;				//输入的actorNode
	};

	//设置是否可见
	class OvrSetVisiableNode : public OvrNormalOperatorNode
	{
	public:
		OvrSetVisiableNode();
		virtual ~OvrSetVisiableNode() {}

		virtual void	BeginPlay() override;

	protected:
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, bool a_value) override;
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value) override;

		virtual bool	Doing(float a_delta_time) override;

	private:
		bool					is_visiable = true;
		ovr_engine::OvrActor*	own_actor = nullptr;

		OvrActorNode*			actor_node = nullptr;				//输入的actorNode
	};

	//设置主相机
	class OvrSetMainCameraNode : public OvrNormalOperatorNode 
	{
	public:
		OvrSetMainCameraNode();
		virtual ~OvrSetMainCameraNode() {}

		virtual void	BeginPlay() override;

	protected:
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value) override;

		virtual bool	Doing(float a_delta_time) override;
	private:
		ovr_engine::OvrActor*	own_actor = nullptr;

		OvrActorNode*			actor_node = nullptr;				//输入的actorNode
	};

	//设置环境光
	class OvrSetAmbientColorNode : public OvrNormalOperatorNode
	{
	public:
		OvrSetAmbientColorNode();
		virtual ~OvrSetAmbientColorNode() {}

	protected:
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, const OvrVector4& a_value) override;

		virtual bool	Doing(float a_delta_time) override;
	private:
		OvrVector4	ambient_color;
	};

	class OvrBranchNode : public OvrOperatorNode 
	{
	public:
		OvrBranchNode();
		virtual ~OvrBranchNode(){}

		virtual bool	Update(float a_delta_time, const OvrString& a_interface_name) override;

		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, int a_value) override;

	private:
		bool	ResizeOut(int a_new_size);
	private:
		ovr::uint32		out_number = 2;

	};

	//actor比较
	class OvrSwitchActorNode : public OvrOperatorNode 
	{
		//封装Actor参数输出执行接口
		class OvrActorOutNode
		{
		public:
			OvrActorOutNode() {}
			~OvrActorOutNode() {}

			void operator = (const OvrActorOutNode& a_other)
			{
				actor = a_other.actor;
				out_node = a_other.out_node;
				other_interface = a_other.other_interface;
			}

			void Clear() 
			{
				actor = nullptr;
				out_node = nullptr;
				other_interface.Clear();
			}

			ovr_engine::OvrActor*			actor = nullptr;
			ovr_core::IOvrVisualNode*	out_node = nullptr;
			OvrString						other_interface;
		};
	public:
		OvrSwitchActorNode();
		virtual ~OvrSwitchActorNode() {}

		virtual void	BeginPlay() override;

		virtual bool	Update(float a_delta_time, const OvrString& a_interface_name) override;

		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, int a_value) override;
		virtual ovr::uint32 SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value) override;

	private:
		ovr::uint32	Resize(ovr::uint32 a_new_size);
		void		RmvInputParamConnect(const OvrString& a_input_param_name);
	private:
		ovr::uint32		actor_number = 2;

		OvrActorOutNode	actor_array[64];

		//用于对比的对象
		ovr_engine::OvrActor*	input_actor = nullptr;

		//没有相同时调用
		IOvrVisualNode*			none_out_node = nullptr;
		OvrString				none_other_interface;
	};

	//设置文本
	class OvrSetTextNode : public OvrNormalOperatorNode 
	{
	public:
		OvrSetTextNode();
		virtual ~OvrSetTextNode() {}

		virtual void	BeginPlay() override;

	protected:
		//执行体 需要由子类实现
		virtual bool	Doing(float a_delta_time) override;
		
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, const OvrString& a_value) override;
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value) override;

	private:
		OvrString					text;
		ovr_engine::OvrTextActor*	text_actor = nullptr;
	};

	//设置状态 临时添加
	class OvrSetStatusNode : public OvrNormalOperatorNode 
	{
	public:
		OvrSetStatusNode();
		virtual ~OvrSetStatusNode() {}

	protected:
		//执行体 需要由子类实现
		virtual bool	Doing(float a_delta_time) override;

		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, int a_value) override;

	private:
		int		new_status;
	};

	class OvrStatusSwitchNode : public OvrOperatorNode
	{
	public:
		OvrStatusSwitchNode();
		virtual ~OvrStatusSwitchNode() {}

		virtual void	BeginPlay() override;

		virtual bool	Update(float a_delta_time, const OvrString& a_interface_name) override;

	private:
		OvrOperatorNode*	out_node_none = nullptr;
		OvrString			out_node_none_interface;

		OvrOperatorNode*	out_node1 = nullptr;
		OvrString			out_node1_interface;

		OvrOperatorNode*	out_node2 = nullptr;
		OvrString			out_node2_interface;
	};

	class OvrMeshActorDiffuseColorNode : public OvrNormalOperatorNode 
	{
	public:
		OvrMeshActorDiffuseColorNode();
		virtual ~OvrMeshActorDiffuseColorNode() {}

		virtual void	BeginPlay() override;

	protected:
		//执行体 需要由子类实现
		virtual bool	Doing(float a_delta_time) override;

		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, const OvrVector4& a_value) override;
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value) override;

	private:
		OvrVector4					diffuse_color;
		ovr_engine::OvrMeshActor*	mesh_actor = nullptr;
	};
}

#endif
