#include "headers.h"
#include "ovr_visual_node.h"

#include "ovr_node_id_define.h"
#include "ovr_node_manager.h"
#include "ovr_event_node.h"
#include "ovr_node_register.h"

#include "ovr_node_cluster.h"
#include <wx_core/async_load/i_ovr_async_load.h>

namespace ovr_node 
{
	ovr_core::IOvrVisualNode* OvrNodeCluster::CreateNode(const OvrString& a_node_id, const OvrString& a_unique_name)
	{
		bool is_unique_node = sg_node_manager->GetNodeRegister()->IsUniqueNode(a_node_id);
		
		if (is_unique_node)
		{
			//如果创建是唯一的 且已经创建过  就直接返回
			auto it = all_node_map.find(a_node_id);
			if (it != all_node_map.end())
			{
				return nullptr;
			}
		}
	
		//创建Node
		OvrVisualNode* new_node = sg_node_manager->GetNodeRegister()->CreateNode(a_node_id, this);
		if (nullptr == new_node)
		{
			return nullptr;
		}

		//设置Unique Name
		if (a_unique_name.GetStringSize() > 0)
		{
			new_node->SetUniqueName(a_unique_name);
		}
		else if (is_unique_node)
		{
			//这三个事件  只能有一个 为了方便查找，就使用classid作为Uniquename
			new_node->SetUniqueName(a_node_id);
		}

		all_node_map[new_node->GetUniqueName()] = new_node;

		return new_node;
	}

	ovr_core::IOvrVisualNode*	 OvrNodeCluster::GetNode(const OvrString& a_node_unique_name)
	{
		auto node_it = all_node_map.find(a_node_unique_name);
		if (node_it != all_node_map.end())
		{
			return node_it->second;
		}
		return nullptr;
	}

	bool OvrNodeCluster::DestoryNode(ovr_core::IOvrVisualNode* a_node)
	{
		if (nullptr == a_node)
		{
			return false;
		}

		auto it = all_node_map.find(a_node->GetUniqueName());
		if (it != all_node_map.end())
		{
			all_node_map.erase(it);

			//断开所有的连接
			a_node->BreakAllConnect();
			delete a_node;
			a_node = nullptr;
		}

		return (nullptr == a_node);
	}

	void OvrNodeCluster::EndPlay() 
	{
		async_load_list.clear();
	}

	void OvrNodeCluster::Update() 
	{
		//加载异步资源 每次加载一个
		if (async_load_list.size() <= 0)
		{
			return;
		}

		//加载并删除
		auto it = async_load_list.begin();
		(*it)->Loading();
		async_load_list.erase(it);
	}

	void	OvrNodeCluster::AppendAssetFile(std::map<OvrString, bool>& a_file_map)
	{
		for (auto it = all_node_map.begin(); it != all_node_map.end(); it++)
		{
			it->second->AppendAssetFile(a_file_map);
		}
	}

	OvrNodeClusterEvent::OvrNodeClusterEvent(ovr_core::OvrSequence* a_own_sequence)
		:OvrNodeCluster(a_own_sequence)
	{
		char temp[128] = { 0 };
		sprintf(temp, "NodeClusterEvent_%llu", OvrTime::GetTicksNanos());
		unique_name.SetString(temp);
	}

	void OvrNodeClusterEvent::BeginPlay() 
	{
		//播放前的处理
		for (auto it = all_node_map.begin(); it != all_node_map.end(); it++)
		{
			it->second->BeginPlay();
		}
	}
	void OvrNodeClusterEvent::EndPlay() 
	{
		//播放后的处理
		for (auto it = all_node_map.begin(); it != all_node_map.end(); it++)
		{
			it->second->EndPlay();
		}

		OvrNodeCluster::EndPlay();
	}
	void OvrNodeClusterEvent::Update() 
	{
		OvrNodeCluster::Update();

		//寻找tick
		OvrEventNode* tick_update_event = nullptr;
		auto update_it = all_node_map.find(OvrNodeClassID::event_begin_id);
		if (update_it != all_node_map.end())
		{
			tick_update_event = (OvrEventNode*)update_it->second;
			tick_update_event->Update(0.0f, OvrString::empty);
		}
	}

	OvrNodeClusterArea::OvrNodeClusterArea(ovr_core::OvrSequence* a_own_sequence)
		:OvrNodeCluster(a_own_sequence)
	{
		char temp[128] = { 0 };
		sprintf(temp, "NodeClusterArea_%llu", OvrTime::GetTicksNanos());
		unique_name.SetString(temp);
	}

	void OvrNodeClusterArea::BeginPlay()
	{
		//播放前的处理
		for (auto it = all_node_map.begin(); it != all_node_map.end(); it++)
		{
			it->second->BeginPlay();
		}

		//寻找开始播放Event  然后进行更新
		last_update_time = (float)OvrTime::GetSeconds();
		auto it = all_node_map.find(OvrNodeClassID::event_begin_id);
		if (it != all_node_map.end())
		{
			((OvrEventNode*)it->second)->Update(0.0f, OvrString::empty);
		}

		//寻找tick
		tick_update_event = nullptr;
		auto update_it = all_node_map.find(OvrNodeClassID::event_update_id);
		if (update_it != all_node_map.end())
		{
			tick_update_event = (OvrEventNode*)update_it->second;
		}
	}
	void OvrNodeClusterArea::EndPlay()
	{
		auto it = all_node_map.find(OvrNodeClassID::event_end_id);
		if (it != all_node_map.end())
		{
			float current_time = (float)OvrTime::GetSeconds();
			((OvrEventNode*)it->second)->Update(current_time - last_update_time, OvrString::empty);
		}

		//播放后的处理
		for (auto it = all_node_map.begin(); it != all_node_map.end(); it++)
		{
			it->second->EndPlay();
		}

		tick_update_event = nullptr;

		OvrNodeCluster::EndPlay();
	}
	void OvrNodeClusterArea::Update()
	{
		OvrNodeCluster::Update();

		if (nullptr != tick_update_event)
		{
			float current_time = (float)OvrTime::GetSeconds();
			tick_update_event->Update(current_time - last_update_time, OvrString::empty);
			last_update_time = current_time;
		}
	}
}
