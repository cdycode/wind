﻿#ifndef OVR_VISUAL_NODE_OVR_FEATHER_NODE_H_
#define OVR_VISUAL_NODE_OVR_FEATHER_NODE_H_

#include "ovr_operator_node.h"

namespace ovr_engine 
{
	//class OvrParticleActor;
}

namespace ovr_node 
{
	class OvrFeatherNode : public OvrOperatorNode
	{
	public:
		OvrFeatherNode();
		virtual ~OvrFeatherNode(){}

		virtual void BeginPlay() override;
		virtual void EndPlay() override;

		virtual bool	Update(float a_delta_time, const OvrString& a_interface_name) override;

	protected:
	
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, int a_value) override;
	private:
		//ovr_engine::OvrParticleActor*	particle_actor = nullptr;

		ovr_engine::OvrTexture*	texture = nullptr;	//漫反射贴图

		ovr::int32				max_size = 20;

		//输出
		OvrOperatorNode*	out_node = nullptr;
		OvrString			out_node_interface;
	};
}

#endif
