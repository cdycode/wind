﻿#ifndef OVR_VISUAL_NODE_OVR_NODE_MANAGER_H_
#define OVR_VISUAL_NODE_OVR_NODE_MANAGER_H_

//OVR_NODE_API
#ifdef _WIN32
#	ifdef WXVISUALNODE_EXPORTS
#		define OVR_VISUAL_NODE_API __declspec(dllexport)
#	else
#		define OVR_VISUAL_NODE_API __declspec(dllimport)
#	endif // OVRVISUALNODE_EXPORTS
#else
#define OVR_VISUAL_NODE_API 
#endif // _WIN32


#include <wx_core/visual_node/i_ovr_node_manager.h>

namespace ovr_node 
{
	class OvrNodeClusterEvent;
	class OvrNodeClusterArea;
	class OvrNodeRegister;
	class OvrNodeManager : public ovr_core::IOvrNodeManager
	{
	public:
		OvrNodeManager() {}
		virtual ~OvrNodeManager() {}

		//初始化
		bool	Init(const OvrString& a_scene_unique_name);
		void	Uninit();

		//获取ActorNode的Class ID
		virtual const OvrString&			GetActorNodeID()const override;

		virtual const OvrString&			GetEventDir()const override { return event_dir; }
		virtual const OvrString&			GetAreaDir()const override { return area_dir; }

		//获取所有的node
		virtual const std::list<ovr_core::OvrNodeInfo>&	GetNodeIDList(ovr_core::IOvrNodeCluster::Type a_type)const override;

		virtual const std::list<OvrString>&	GetClusterFileList(ovr_core::IOvrNodeCluster::Type a_type)const override;

		OvrNodeRegister*	GetNodeRegister() { return node_register; }

	protected:

		//创建事件
		virtual bool							CreateCluster(ovr_core::IOvrNodeCluster::Type a_type, const OvrString& a_file_name) override;
		//加载和关闭事件
		virtual ovr_core::IOvrNodeCluster*	LoadCluster(ovr_core::IOvrNodeCluster::Type a_type, const OvrString& a_file_name, ovr_core::OvrSequence* a_own_sequence) override;
		virtual void							CloseCluster(ovr_core::IOvrNodeCluster* a_node_cluster) override;
		virtual void							SaveCluster(ovr_core::IOvrNodeCluster* a_node_cluster) override;

	private:
		//创建交互区
		bool							CreateArea(const OvrString& a_file_name) ;
		//加载和关闭交互区
		ovr_core::IOvrNodeCluster*	LoadArea(const OvrString& a_file_name, ovr_core::OvrSequence* a_own_sequence);
		void							CloseArea(ovr_core::IOvrNodeCluster* a_node_cluster) ;

		//创建事件
		bool							CreateEvent(const OvrString& a_file_name);
		//加载和关闭事件
		ovr_core::IOvrNodeCluster*	LoadEvent(const OvrString& a_file_name, ovr_core::OvrSequence* a_own_sequence);
		void							CloseEvent(ovr_core::IOvrNodeCluster* a_node_cluster);

	private:
		OvrNodeRegister*		node_register = nullptr;

		OvrString									area_dir;
		std::list<OvrString>						area_file_list;
		std::map<OvrString, OvrNodeClusterArea*>	area_map;

		OvrString									event_dir;
		std::list<OvrString>						event_file_list;
		std::map<OvrString, OvrNodeClusterEvent*>	event_map;
	};

	extern  OvrNodeManager* sg_node_manager;
}

#if defined(__cplusplus)
extern "C"
{
#endif
	OVR_VISUAL_NODE_API  void* Init(const OvrString& a_scene_unique_name);
	OVR_VISUAL_NODE_API  void  Uninit();
#if defined(__cplusplus)
}
#endif

#endif