﻿#ifndef OVR_VISUAL_NODE_OVR_SPRITE_PICTURE_H_
#define OVR_VISUAL_NODE_OVR_SPRITE_PICTURE_H_

#include "ovr_operator_node.h"

namespace ovr_node 
{
	class OvrSpritePicture : public OvrOperatorNode
	{
	public:
		OvrSpritePicture();
		virtual ~OvrSpritePicture() {}

		virtual void BeginPlay() override;
		virtual void EndPlay() override;
		virtual bool Update(float a_delta_time, const OvrString& a_interface_name) override;

	protected:
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, int a_value)override;
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, float a_value)override;
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value)override;

	private:
		void	ResetPicture();
	private:
		ovr_engine::OvrActor*	actor = nullptr;
		ovr::uint32				row_num = 1;
		ovr::uint32				column_num = 1;
		ovr::uint32				default_index = 0;
		float					loop_time = 1.0f;	//单位为秒

		OvrActorNode*			actor_node = nullptr;				//输入的actorNode

		//从actor中获取
		ovr_engine::OvrTextureUnit* texture_layer = nullptr;

		ovr::uint32	current_index = 0;	//当前的索引
		ovr::uint32	total_unit = 0;		//总的Icon数目
		
		float		unit_x = 0.0f;		//x方向的单位尺寸
		float		unit_y = 0.0f;		//y方向的单位尺寸
		float		unit_time = 0.0f;	//每个Icon停留的时间
		float		accum_gaze_time = 0.0f;	//累计时间

		OvrOperatorNode*	out_node = nullptr;
		OvrString			out_interface;
	};

}

#endif
