﻿#include "headers.h"
#include "ovr_operator_node.h"
#include <wx_core/ovr_sequence.h>
#include "ovr_node_id_define.h"
#include "ovr_variable_node.h"
#include <wx_core/ovr_scene_context.h>
#include <wx_core/ovr_background_audio.h>
#include <wx_engine/ovr_texture_manager.h>
#include <wx_engine/ovr_material_manager.h>
#include <wx_core/ovr_scene_camera.h>
#include <wx_core/ovr_scene_context.h>
#include <wx_engine/ovr_scene.h>
//#include <ovr_engine/ovr_text_actor.h>
//#include <ovr_engine/ovr_mesh_actor.h>
#include "ovr_node_cluster.h"

namespace ovr_node 
{
	OvrNormalOperatorNode::OvrNormalOperatorNode() 
	{
		//接口
		interface_list.push_back(ovr_core::OvrNodeInterface(true, "", kInputID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, "", kOutputID));
	}

	void OvrNormalOperatorNode::BeginPlay() 
	{
		out_node = nullptr;
		//填充输出接口
		for (auto it = out_connect_list.begin(); it != out_connect_list.end(); it++)
		{
			if (it->local_interface == kOutputID)
			{
				out_node = (OvrOperatorNode*)it->other_node;
				out_node_interface = it->other_interface;
			}
		}
	}
	void OvrNormalOperatorNode::EndPlay() 
	{
		out_node = nullptr;
	}

	bool OvrNormalOperatorNode::Update(float a_delta_time, const OvrString& a_interface_name) 
	{
		//调用执行体
		Doing(a_delta_time);

		//执行下一个
		if (nullptr != out_node)
		{
			out_node->Update(a_delta_time, out_node_interface);
		}
		return true;
	}

	OvrSequenceGotoNode::OvrSequenceGotoNode()
	{
		//参数
		in_param_list.push_back(ovr_core::OvrNodeParam(kClipName, kClipID,ovr_core::OvrNodeParam::kParamSequence));
	}

	ovr::uint32 OvrSequenceGotoNode::SetValue(bool a_is_in, const OvrString& a_name, ovr_core::OvrSequence* a_sequence, ovr::uint64 a_frame_index)
	{
		if (a_is_in && a_name == kClipID)
		{
			if (nullptr == a_sequence)
			{
				//目前只支持同Sequence内的跳转
				sequence = node_cluster->GetOwnSequence();
			}
			else 
			{
				sequence = a_sequence;
			}
			
			goto_position = a_frame_index;
			return 1;
		}
		return 0;
	}

	bool OvrSequenceGotoNode::Doing(float a_delta_time) 
	{
		if (ovr_core::kRenderPlay == node_cluster->GetOwnSequence()->GetCurrentStatus())
		{
			if (nullptr != sequence)
			{
				sequence->ChangePlayPosition(goto_position);
			}
		}
		return true;
	}

	OvrChangeTextureNode::OvrChangeTextureNode() 
	{
		//参数
		in_param_list.push_back(ovr_core::OvrNodeParam(kOwnMaterialName, kOwnMaterialID,ovr_core::OvrNodeParam::kParamResource, OvrNodeClassID::material_type_option, ""));
		in_param_list.push_back(ovr_core::OvrNodeParam(kTextureDiffuseName, kTextureDiffuseID,ovr_core::OvrNodeParam::kParamResource, OvrNodeClassID::texture_type_option, ""));
		in_param_list.push_back(ovr_core::OvrNodeParam(kTextureEmissiveName, kTextureEmissiveID,ovr_core::OvrNodeParam::kParamResource, OvrNodeClassID::texture_type_option, ""));
		in_param_list.push_back(ovr_core::OvrNodeParam(kTextureNormalName, kTextureNormalID,ovr_core::OvrNodeParam::kParamResource, OvrNodeClassID::texture_type_option, ""));
		in_param_list.push_back(ovr_core::OvrNodeParam(kTextureSpecularName, kTextureSpecularID,ovr_core::OvrNodeParam::kParamResource, OvrNodeClassID::texture_type_option, ""));
	}

	void OvrChangeTextureNode::BeginPlay() 
	{
		OvrNormalOperatorNode::BeginPlay();

		diffuse_texture.Clear();
		emissive_texture.Clear();
		normal_texture.Clear();
		specular_texture.Clear();

		for (auto it = in_param_list.begin(); it != in_param_list.end(); it++)
		{
			if (it->value.GetStringSize() == 0)
			{
				continue;
			}
			if (it->unique_name == kOwnMaterialID)
			{
				material = ovr_engine::OvrMaterialManager::GetIns()->Load(it->value);
			}
			else if (it->unique_name == kTextureDiffuseID)
			{
				if (!diffuse_texture.SetFilePath(it->value)) 
				{
					node_cluster->AddAsyncLoad(&diffuse_texture);
				}
			}
			else if (it->unique_name == kTextureEmissiveID)
			{
				if (!emissive_texture.SetFilePath(it->value))
				{
					node_cluster->AddAsyncLoad(&emissive_texture);
				}

			}
			else if (it->unique_name == kTextureNormalID)
			{
				if (!normal_texture.SetFilePath(it->value))
				{
					node_cluster->AddAsyncLoad(&normal_texture);
				}
			}
			else if (it->unique_name == kTextureSpecularID)
			{
				if (!specular_texture.SetFilePath(it->value))
				{
					node_cluster->AddAsyncLoad(&specular_texture);
				}
			}
		}
	}

	void OvrChangeTextureNode::EndPlay() 
	{
		diffuse_texture.Clear();
		emissive_texture.Clear();
		normal_texture.Clear();
		specular_texture.Clear();
		OvrNormalOperatorNode::EndPlay();
	}
	void OvrChangeTextureNode::AppendAssetFile(std::map<OvrString, bool>& a_file_map)
	{
		for (auto it = in_param_list.begin(); it != in_param_list.end(); it++)
		{
			if (it->value.GetStringSize() == 0)
			{
				continue;
			}
			if (it->unique_name == kOwnMaterialID)
			{
				a_file_map[it->value] = true;
			}
			else if (it->unique_name == kTextureDiffuseID)
			{
				a_file_map[it->value] = true;
			}
			else if (it->unique_name == kTextureEmissiveID)
			{
				a_file_map[it->value] = true;
			}
			else if (it->unique_name == kTextureNormalID)
			{
				a_file_map[it->value] = true;
			}
			else if (it->unique_name == kTextureSpecularID)
			{
				a_file_map[it->value] = true;
			}
		}
	}

	bool OvrChangeTextureNode::Doing(float a_delta_time)
	{
		if (nullptr != material)
		{
			ovr_engine::OvrGpuProgramParameters* shader_params = material->GetProgramParameters();
			if (nullptr != diffuse_texture.GetTexture())
			{
				shader_params->SetNameConstant("Texture0", diffuse_texture.GetTexture());
			}
			if (nullptr != emissive_texture.GetTexture())
			{
				shader_params->SetNameConstant("Texture1", emissive_texture.GetTexture());
			}
			if (nullptr != normal_texture.GetTexture())
			{
				shader_params->SetNameConstant("Texture2", normal_texture.GetTexture());
			}
			if (nullptr != specular_texture.GetTexture())
			{
				shader_params->SetNameConstant("Texture3", specular_texture.GetTexture());
			}
		}
		
		return true;
	}

	OvrChangeDiffuseTextureNode::OvrChangeDiffuseTextureNode()
	{
		display_name = kTextureModifyName;

		//参数
		in_param_list.push_back(ovr_core::OvrNodeParam(kOwnMaterialName, kOwnMaterialID,ovr_core::OvrNodeParam::kParamResource, OvrNodeClassID::material_type_option, ""));
		in_param_list.push_back(ovr_core::OvrNodeParam(kTexture1Name, kTexture1ID,ovr_core::OvrNodeParam::kParamResource, OvrNodeClassID::texture_type_option, ""));
		in_param_list.push_back(ovr_core::OvrNodeParam(kTexture2Name, kTexture2ID,ovr_core::OvrNodeParam::kParamResource, OvrNodeClassID::texture_type_option, ""));
	}

	void OvrChangeDiffuseTextureNode::BeginPlay()
	{
		OvrNormalOperatorNode::BeginPlay();

		material = nullptr;
		
		diffuse_texture_1.Clear();
		diffuse_texture_2.Clear();

		for (auto it = in_param_list.begin(); it != in_param_list.end(); it++)
		{
			if (it->value.GetStringSize() == 0)
			{
				continue;
			}
			if (it->unique_name == kOwnMaterialID)
			{
				material = ovr_engine::OvrMaterialManager::GetIns()->Load(it->value);
			}
			else if (it->unique_name == kTexture1ID)
			{
				if (!diffuse_texture_1.SetFilePath(it->value))
				{
					node_cluster->AddAsyncLoad(&diffuse_texture_1);
				}
			}
			else if (it->unique_name == kTexture2ID)
			{
				if (!diffuse_texture_2.SetFilePath(it->value))
				{
					node_cluster->AddAsyncLoad(&diffuse_texture_2);
				}
			}
		}
	}
	void OvrChangeDiffuseTextureNode::AppendAssetFile(std::map<OvrString, bool>& a_file_map)
	{
		for (auto it = in_param_list.begin(); it != in_param_list.end(); it++)
		{
			if (it->value.GetStringSize() == 0)
			{
				continue;
			}
			if (it->unique_name == kOwnMaterialID)
			{
				a_file_map[it->value] = true;
			}
			else if (it->unique_name == kTexture1ID)
			{
				a_file_map[it->value] = true;
			}
			else if (it->unique_name == kTexture2ID)
			{
				a_file_map[it->value] = true;
			}
		}
	}

	bool OvrChangeDiffuseTextureNode::Doing(float a_delta_time)
	{
		if (nullptr != material)
		{
			ovr_engine::OvrGpuProgramParameters* shader_params = material->GetProgramParameters();
			if (is_first)
			{
				shader_params->SetNameConstant("Texture0", diffuse_texture_1.GetTexture());
			}
			else 
			{
				shader_params->SetNameConstant("Texture0", diffuse_texture_2.GetTexture());
			}
			is_first = !is_first;
		}
		return true;
	}

	OvrPlaySoundNode::OvrPlaySoundNode() 
	{
		//参数
		in_param_list.push_back(ovr_core::OvrNodeParam(kAudioFileName, kAudioFileID,ovr_core::OvrNodeParam::kParamResource, OvrNodeClassID::audio_type_option, ""));
		in_param_list.push_back(ovr_core::OvrNodeParam(kIsLoopName, kIsLoopID,ovr_core::OvrNodeParam::kParamBool, "0"));
		in_param_list.push_back(ovr_core::OvrNodeParam(kBlendTimeName, kBlendTimeID,ovr_core::OvrNodeParam::kParamFloat, "1.0"));
		in_param_list.push_back(ovr_core::OvrNodeParam(kVolumeName, kVolumeID,ovr_core::OvrNodeParam::kParamFloat, "1.0"));
	}

	void OvrPlaySoundNode::BeginPlay() 
	{
		OvrNormalOperatorNode::BeginPlay();
		for (auto it = in_param_list.begin(); it != in_param_list.end(); it++)
		{
			if (it->value.GetStringSize() == 0)
			{
				continue;
			}
			if (it->unique_name == kAudioFileID)
			{
				audio_file_path = ovr_core::OvrCore::GetIns()->GetWorkDir() + it->value;
			}
		}
	}

	void OvrPlaySoundNode::EndPlay() 
	{
		audio_file_path.Clear();
		OvrNormalOperatorNode::EndPlay();
	}

	void OvrPlaySoundNode::AppendAssetFile(std::map<OvrString, bool>& a_file_map)
	{
		for (auto it = in_param_list.begin(); it != in_param_list.end(); it++)
		{
			if (it->value.GetStringSize() == 0)
			{
				continue;
			}
			if (it->unique_name == kAudioFileID)
			{
				a_file_map[it->value] = true;
			}
		}
	}

	ovr::uint32 OvrPlaySoundNode::SetValue(bool a_is_in, const OvrString& a_name, bool a_value)
	{
		if (a_name == kIsLoopID)
		{
			is_loop = a_value;
			return 1;
		}
		return 0;
	}

	ovr::uint32 OvrPlaySoundNode::SetValue(bool a_is_in, const OvrString& a_name, float a_value)
	{
		if (a_name == kBlendTimeID)
		{
			blend_time = a_value;
			return 1;
		}
		else if (a_name == kVolumeID)
		{
			volume = a_value;
		}
		return 0;
	}

	bool OvrPlaySoundNode::Doing(float a_delta_time) 
	{
		if (ovr_core::kRenderPlay == node_cluster->GetOwnSequence()->GetCurrentStatus())
		{
			if (audio_file_path.GetStringSize() > 0)
			{
				ovr_core::OvrBackgroundAudio* background_audio = node_cluster->GetOwnSequence()->GetBackgroundAudio();
				if (is_loop)
				{
					//循环播放
					background_audio->PlayLoop(audio_file_path, blend_time, volume);
				}
				else
				{
					//只播放一次
					background_audio->PlayOnce(audio_file_path, volume);
				}
			}
		}
		return true;
	}

	OvrTimerNode::OvrTimerNode() 
	{
		interface_list.push_back(ovr_core::OvrNodeInterface(true, kUpdateName, kUpdateID));
		interface_list.push_back(ovr_core::OvrNodeInterface(true, kResetName, kResetID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, "", kOutputID));

		//参数
		in_param_list.push_back(ovr_core::OvrNodeParam(kTimeName, kTimeID,ovr_core::OvrNodeParam::kParamFloat));
	}

	void OvrTimerNode::BeginPlay() 
	{
		accumulated_time = 0.0f;
	}

	bool OvrTimerNode::Update(float a_delta_time, const OvrString& a_interface_name) 
	{
		if (a_interface_name == kUpdateID)
		{
			accumulated_time += a_delta_time;
			if (accumulated_time > duration)
			{
				//填充输出接口
				for (auto it = out_connect_list.begin(); it != out_connect_list.end(); it++)
				{
					((OvrOperatorNode*)it->other_node)->Update(a_delta_time, it->other_interface);
				}

				accumulated_time -= duration;
				return true;
			}
		}
		else 
		{
			accumulated_time = 0.0f;
		}
		
		return false;
	}

	ovr::uint32 OvrTimerNode::SetValue(bool a_is_in, const OvrString& a_name, float a_value)
	{
		if (a_name == kTimeID)
		{
			duration = a_value;
			return 1;
		}
		return 0;
	}

	OvrAlternateNode::OvrAlternateNode() 
	{
		//接口
		interface_list.push_back(ovr_core::OvrNodeInterface(true, "", kInputID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, kOutput1Name, kOutput1ID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, kOutput2Name, kOutput2ID));
	}
	void OvrAlternateNode::BeginPlay() 
	{
		first_out_node = nullptr;
		second_out_node = nullptr;

		//填充输出接口
		for (auto it = out_connect_list.begin(); it != out_connect_list.end(); it++)
		{
			if (it->local_interface == kOutput1ID)
			{
				first_out_node = (OvrOperatorNode*)it->other_node;
				first_out_node_interface = it->other_interface;
			}
			else if (it->local_interface == kOutput2ID)
			{
				second_out_node = (OvrOperatorNode*)it->other_node;
				second_out_node_interface = it->other_interface;
			}
		}
		is_first = false;
	}

	bool OvrAlternateNode::Update(float a_delta_time, const OvrString& a_interface_name) 
	{
		if (is_first)
		{
			if (nullptr != first_out_node)
			{
				first_out_node->Update(a_delta_time, first_out_node_interface);
			}
		}
		else 
		{
			if (nullptr != second_out_node)
			{
				second_out_node->Update(a_delta_time, second_out_node_interface);
			}
		}
		is_first = !is_first;
		return true;
	}

	OvrFollowCameraNode::OvrFollowCameraNode() 
	{
		//参数
		in_param_list.push_back(ovr_core::OvrNodeParam(kFollowName, kFollowID,ovr_core::OvrNodeParam::kParamBool, "1"));
		in_param_list.push_back(ovr_core::OvrNodeParam(kObjectName, kObjectID,ovr_core::OvrNodeParam::kParamActor));
	}

	void OvrFollowCameraNode::BeginPlay() 
	{
		OvrNormalOperatorNode::BeginPlay();

		own_actor = nullptr;
		actor_node = nullptr;

		//填充变量输入
		for (auto it = in_connect_list.begin(); it != in_connect_list.end(); it++)
		{
			if (it->local_interface == kObjectID)
			{
				actor_node = (OvrActorNode*)it->other_node;
			}
		}
	}

	ovr::uint32 OvrFollowCameraNode::SetValue(bool a_is_in, const OvrString& a_name, bool a_value)
	{
		if (a_name == kFollowID)
		{
			is_follow = a_value;
			return 1;
		}
		return 0;
	}

	ovr::uint32 OvrFollowCameraNode::SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value)
	{
		if (a_name == kObjectID)
		{
			own_actor = a_value;
			return 1;
		}
		return 0;
	}
	bool OvrFollowCameraNode::Doing(float a_delta_time)
	{
		if (nullptr != actor_node)
		{
			own_actor = actor_node->GetActor();
		}
		if (nullptr != own_actor)
		{
			//ovr_project::OvrProject::GetIns()->GetCurrentScene()->GetCazeController()->FollowCamera(own_actor, is_follow);
		}
		return true;
	}

	OvrSetVisiableNode::OvrSetVisiableNode()
	{
		//参数
		in_param_list.push_back(ovr_core::OvrNodeParam(kIsVisualName, kIsVisualID, ovr_core::OvrNodeParam::kParamBool, "1"));
		in_param_list.push_back(ovr_core::OvrNodeParam(kObjectName, kObjectID,ovr_core::OvrNodeParam::kParamActor));
	}

	void OvrSetVisiableNode::BeginPlay()
	{
		OvrNormalOperatorNode::BeginPlay();

		actor_node = nullptr;

		//填充变量输入
		for (auto it = in_connect_list.begin(); it != in_connect_list.end(); it++)
		{
			if (it->local_interface == kObjectID)
			{
				actor_node = (OvrActorNode*)it->other_node;
			}
		}
	}

	ovr::uint32 OvrSetVisiableNode::SetValue(bool a_is_in, const OvrString& a_name, bool a_value)
	{
		if (a_name == kIsVisualID)
		{
			is_visiable = a_value;
			return 1;
		}
		return 0;
	}

	ovr::uint32 OvrSetVisiableNode::SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value)
	{
		if (a_name == kObjectID)
		{
			own_actor = a_value;
			return 1;
		}
		return 0;
	}
	bool OvrSetVisiableNode::Doing(float a_delta_time)
	{
		if (nullptr != actor_node)
		{
			own_actor = actor_node->GetActor();
		}
		if (nullptr != own_actor)
		{
			//own_actor->SetVisible(is_visiable);
		}

		return true;
	}

	OvrSetMainCameraNode::OvrSetMainCameraNode()
	{
		//参数
		in_param_list.push_back(ovr_core::OvrNodeParam(kCameraName, kCameraID,ovr_core::OvrNodeParam::kParamActor));
	}

	void OvrSetMainCameraNode::BeginPlay()
	{
		OvrNormalOperatorNode::BeginPlay();

		actor_node = nullptr;

		//填充变量输入
		for (auto it = in_connect_list.begin(); it != in_connect_list.end(); it++)
		{
			if (it->local_interface == kCameraID)
			{
				actor_node = (OvrActorNode*)it->other_node;

				//直接赋值
				own_actor = actor_node->GetActor();
			}
		}
	}

	ovr::uint32 OvrSetMainCameraNode::SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value)
	{
		if (a_name == kCameraID)
		{
			own_actor = a_value;
			return 1;
		}
		return 0;
	}

	bool OvrSetMainCameraNode::Doing(float a_delta_time)
	{
		node_cluster->GetOwnSequence()->GetScene()->GetSceneCamera()->SetCurrentCamera(own_actor);
		return true;
	}

	OvrSetAmbientColorNode::OvrSetAmbientColorNode() 
		:ambient_color(1.0f)
	{
		//参数
		in_param_list.push_back(ovr_core::OvrNodeParam(kColorName, kColorID,ovr_core::OvrNodeParam::kParamColor, "1.0:1.0:1.0:1.0"));
	}

	ovr::uint32 OvrSetAmbientColorNode::SetValue(bool a_is_in, const OvrString& a_name, const OvrVector4& a_value)
	{
		if (a_name == kColorID)
		{
			ambient_color = a_value;
			return 1;
		}
		return 0;
	}

	bool OvrSetAmbientColorNode::Doing(float a_delta_time)
	{
		node_cluster->GetOwnSequence()->GetScene()->GetScene()->SetAmbientColor(OvrVector4(ambient_color[0], ambient_color[1], ambient_color[2], 1.0f));
		return true;
	}

	OvrBranchNode::OvrBranchNode() 
	{
		interface_list.push_back(ovr_core::OvrNodeInterface(true, "", kInputID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, kOutput1Name, kOutput1ID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, kOutput2Name, kOutput2ID));

		//参数
		in_param_list.push_back(ovr_core::OvrNodeParam(kOutputNumName, kOutputNumID, ovr_core::OvrNodeParam::kParamInt, "2"));

		out_number = 2;
	}
	
	bool OvrBranchNode::Update(float a_delta_time, const OvrString& a_interface_name) 
	{
		//填充输出接口
		for (auto it = out_connect_list.begin(); it != out_connect_list.end(); it++)
		{
			((OvrOperatorNode*)it->other_node)->Update(a_delta_time, it->other_interface);
		}
		return true;
	}

	ovr::uint32	OvrBranchNode::SetValue(bool a_is_in, const OvrString& a_name, int a_value) 
	{
		if (a_name == kOutputNumID)
		{
			if (ResizeOut(a_value))
			{
				return 2;
			}
		}
		return 0;
	}

	bool OvrBranchNode::ResizeOut(int a_new_size) 
	{
		if (a_new_size == out_number || a_new_size <= 0 || a_new_size > 64)
		{
			return false;
		}

		//第一个是输入接口 所以需要减去
		if (a_new_size > (ovr::int32)(interface_list.size() - 1))
		{
			//需要增加新的输出接口
			ovr::int32 diff_num = a_new_size - (interface_list.size() - 1);
			for (ovr::int32 i = 0; i < diff_num; i++)
			{
				char temp[50];
				sprintf(temp, kOutputFormatName, out_number + 1 + i);
				char temp2[50];
				sprintf(temp2, kOutputFormatID, out_number + 1 + i);
				interface_list.push_back(ovr_core::OvrNodeInterface(false, temp, temp2));
			}
		}
		else 
		{
			//需要删除输出接口
			ovr::int32 index = 0;
			auto it = interface_list.begin();
			while (it != interface_list.end())
			{
				if (index <= a_new_size)
				{
					//跳过第一个 和需要保留的输出接口
					index++;
					it++;
				}
				else 
				{
					RmvOutConnect(it->unique_name);
					it = interface_list.erase(it);
				}
			}
		}

		out_number = a_new_size;
		return true;
	}

	OvrSwitchActorNode::OvrSwitchActorNode() 
	{
		interface_list.push_back(ovr_core::OvrNodeInterface(true, "", kInputID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, kNoneName, kNoneID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, kOutputObject1Name, kOutputObject1ID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, kOutputObject2Name, kOutputObject2ID));

		//参数
		in_param_list.push_back(ovr_core::OvrNodeParam(kInputObjectName, kInputObjectID,ovr_core::OvrNodeParam::kParamActor));
		in_param_list.push_back(ovr_core::OvrNodeParam(kNumName, kNumID,ovr_core::OvrNodeParam::kParamInt, "2"));
		in_param_list.push_back(ovr_core::OvrNodeParam(kObject1Name, kObject1ID,ovr_core::OvrNodeParam::kParamActor));
		in_param_list.push_back(ovr_core::OvrNodeParam(kObject2Name, kObject2ID,ovr_core::OvrNodeParam::kParamActor));
		
		actor_number = 2;
	}

	void OvrSwitchActorNode::BeginPlay()
	{
		input_actor = nullptr;

		for (auto it = out_connect_list.begin(); it != out_connect_list.end(); it++) 
		{
			if (it->local_interface == kOutputObject1ID)
			{
				actor_array[0].out_node = it->other_node;
				actor_array[0].other_interface = it->other_interface;
			}
			else if (it->local_interface == kOutputObject2ID)
			{
				actor_array[1].out_node = it->other_node;
				actor_array[1].other_interface = it->other_interface;
			}
			else if (it->local_interface == kNoneID)
			{
				none_out_node = it->other_node;
				none_other_interface = it->other_interface;
			}
		}

		for (auto it = in_connect_list.begin(); it != in_connect_list.end(); it++)
		{
			if (it->local_interface == kInputID)
			{
				OvrActorNode* actor_node = (OvrActorNode*)it->other_node;
				input_actor = actor_node->GetActor();
			}
			else if (it->local_interface == kNumID)
			{
				//不做处理
			}
			else 
			{
				ovr::int32 position = it->local_interface.FindLastCharPos('_');
				if (position <= 0)
				{
					continue;
				}
				OvrString id = it->local_interface.SubString(position + 1, it->local_interface.GetStringSize() - position - 1);
				int index = atoi(id.GetStringConst());
				if (index > 0)
				{
					actor_array[index - 1].actor = ((OvrActorNode*)it->other_node)->GetActor();
				}
			}
		}
	}

	bool OvrSwitchActorNode::Update(float a_delta_time, const OvrString& a_interface_name)
	{
		if (nullptr == input_actor)
		{
			//输入为空 直接执行 都不是的接口
			return none_out_node->Update(a_delta_time, none_other_interface);
		}

		//逐个比较
		for (ovr::uint32 index = 0; index < actor_number; index++)
		{
			if (actor_array[index].actor == input_actor)
			{
				if (nullptr != actor_array[index].out_node)
				{
					actor_array[index].out_node->Update(a_delta_time, actor_array[index].other_interface);
				}
				return true;
			}
		}

		//都不是的接口
		if (nullptr != none_out_node)
		{
			return none_out_node->Update(a_delta_time, none_other_interface);
		}
		return true;
	}

	ovr::uint32	OvrSwitchActorNode::SetValue(bool a_is_in, const OvrString& a_name, int a_value) 
	{
		if (a_name == kNumID)
		{
			return Resize(a_value);
		}
		return 0;
	}

	ovr::uint32 OvrSwitchActorNode::SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value)
	{
		if (a_name == kObject1ID)
		{
			actor_array[0].actor = a_value;
			return 1;
		}
		else if (a_name == kObject2ID)
		{
			actor_array[1].actor = a_value;
		}
		else if (a_name == kInputObjectID)
		{
			input_actor = a_value;
		}
		else 
		{
			return 0;
		}
		return 1;
	}

	ovr::uint32	OvrSwitchActorNode::Resize(ovr::uint32 a_new_size) 
	{
		//取值范围限制在 1~64
		if (a_new_size < 1 || a_new_size > 64)
		{
			return 0;
		}

		if (actor_number == a_new_size)
		{
			return 1;
		}

		if (actor_number < a_new_size)
		{
			//需要增加接口
			ovr::uint32 add_num = a_new_size - actor_number;
			for (ovr::uint32 i = 1; i <= add_num; i++)
			{
				//增加输出接口
				char temp[50];
				sprintf(temp,  kObjectFormatName, actor_number + i);
				char temp2[50];
				sprintf(temp2,  kObjectFormatID, actor_number + i);
				
				interface_list.push_back(ovr_core::OvrNodeInterface(false, temp, temp2));

				//增加输入参数
				char id[50];
				sprintf(id, kObjectFormatID, actor_number + i);
				in_param_list.push_back(ovr_core::OvrNodeParam(temp, id,ovr_core::OvrNodeParam::kParamActor));
			}
		}
		else 
		{
			//需要删除接口
			int diff = actor_number - a_new_size;
			auto out_it = interface_list.end();
			auto in_it = in_param_list.end();
			for (int i = 0; i < diff; i++)
			{
				actor_array[actor_number + i].Clear();

				//移除输出链接
				--out_it;
				RmvOutConnect(out_it->unique_name);
				out_it = interface_list.erase(out_it);

				//移除输入链接
				--in_it;
				RmvInputParamConnect(in_it->unique_name);
				in_it = in_param_list.erase(in_it);
			}
		}

		actor_number = a_new_size;
		return 2;
	}

	void OvrSwitchActorNode::RmvInputParamConnect(const OvrString& a_input_param_name)
	{
		IOvrVisualNode* other_node = nullptr;
		OvrString		other_interface;

		//寻找链接
		auto connect_it = in_connect_list.begin();
		while (connect_it != in_connect_list.end())
		{
			if (connect_it->local_interface == a_input_param_name)
			{
				other_node = connect_it->other_node;
				other_interface = connect_it->other_interface;
				break;
			}
			connect_it++;
		}

		if (nullptr != other_node)
		{
			other_node->RmvOutConnect(other_interface);
		}
	}

	OvrSetTextNode::OvrSetTextNode() 
	{
		in_param_list.push_back(ovr_core::OvrNodeParam(kObjectName, kObjectID, ovr_core::OvrNodeParam::kParamActor));
		in_param_list.push_back(ovr_core::OvrNodeParam(kTextName, kTextID, ovr_core::OvrNodeParam::kParamString));
	}

	void OvrSetTextNode::BeginPlay() 
	{
		OvrNormalOperatorNode::BeginPlay();

		for (auto it = in_connect_list.begin(); it != in_connect_list.end(); it++)
		{
			if (it->local_interface == kObjectID)
			{
				OvrActorNode* actor_node = (OvrActorNode*)it->other_node;
				//text_actor = dynamic_cast<ovr_engine::OvrTextActor*> (actor_node->GetActor());
			}
		
		}
	}

	//执行体 需要由子类实现
	bool OvrSetTextNode::Doing(float a_delta_time) 
	{
		if (nullptr != text_actor)
		{
			OvrString utf8_string;
			text.ToUtf8(utf8_string);
			//text_actor->SetText(utf8_string);
		}
		return true;
	}

	ovr::uint32	OvrSetTextNode::SetValue(bool a_is_in, const OvrString& a_name, const OvrString& a_value) 
	{
		if (a_name == kTextID)
		{
			text = a_value;
			return 1;
		}
		return 0;
	}
	ovr::uint32	OvrSetTextNode::SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value) 
	{
		if (a_name == kObjectID)
		{
			//text_actor = dynamic_cast<ovr_engine::OvrTextActor*> (a_value);
			return 1;
		}
		return 0;
	}

	static ovr::int32 sg_current_value = 0;
	OvrSetStatusNode::OvrSetStatusNode() 
	{
		in_param_list.push_back(ovr_core::OvrNodeParam(kValueName, kValueID, ovr_core::OvrNodeParam::kParamInt, "0"));
	}

	//执行体 需要由子类实现
	bool OvrSetStatusNode::Doing(float a_delta_time) 
	{
		sg_current_value = new_status;
		return true;
	}

	ovr::uint32	OvrSetStatusNode::SetValue(bool a_is_in, const OvrString& a_name, int a_value) 
	{
		if (a_name == kValueID)
		{
			new_status = a_value;
		}
		return 1;
	}

	OvrStatusSwitchNode::OvrStatusSwitchNode() 
	{
		interface_list.push_back(ovr_core::OvrNodeInterface(true, "", kInputID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, kNoneName, kNoneID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, kOutput1Name, kOutput1ID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, kOutput2Name, kOutput2ID));
	}

	void OvrStatusSwitchNode::BeginPlay()
	{
		for (auto it = out_connect_list.begin(); it != out_connect_list.end(); it++)
		{
			if (it->local_interface == kNoneID)
			{
				out_node_none = (OvrOperatorNode*)it->other_node;
				out_node_none_interface = it->other_interface;
			}
			else if (it->local_interface == kOutput1ID)
			{
				out_node1 = (OvrOperatorNode*)it->other_node;
				out_node1_interface = it->other_interface;
			}
			else if (it->local_interface == kOutput2ID)
			{
				out_node2 = (OvrOperatorNode*)it->other_node;
				out_node2_interface = it->other_interface;
			}
		}
	}

	bool OvrStatusSwitchNode::Update(float a_delta_time, const OvrString& a_interface_name) 
	{
		if (sg_current_value == 1)
		{
			//打开灯光失败
			if (nullptr != out_node1)
			{
				out_node1->Update(a_delta_time, out_node1_interface);
			}
		}
		else if (sg_current_value == 2)
		{
			//没有点中小球
			if (nullptr != out_node2)
			{
				out_node2->Update(a_delta_time, out_node2_interface);
			}
		}
		else
		{
			//按原路径执行
			if (nullptr != out_node_none)
			{
				out_node_none->Update(a_delta_time, out_node_none_interface);
			}
		}
		return true;
	}

	OvrMeshActorDiffuseColorNode::OvrMeshActorDiffuseColorNode() 
	{
		in_param_list.push_back(ovr_core::OvrNodeParam(kObjectName, kObjectID, ovr_core::OvrNodeParam::kParamActor));
		in_param_list.push_back(ovr_core::OvrNodeParam(kDiffuseColorName, kDiffuseColorID, ovr_core::OvrNodeParam::kParamColor));
	}
	
	void OvrMeshActorDiffuseColorNode::BeginPlay() 
	{
		OvrNormalOperatorNode::BeginPlay();

		for (auto it = in_connect_list.begin(); it != in_connect_list.end(); it++)
		{
			if (it->local_interface == kObjectID)
			{
				OvrActorNode* actor_node = (OvrActorNode*)it->other_node;
				//mesh_actor = dynamic_cast<ovr_engine::OvrMeshActor*> (actor_node->GetActor());
			}

		}
	}

	//执行体 需要由子类实现
	bool OvrMeshActorDiffuseColorNode::Doing(float a_delta_time)
	{
		if (nullptr != mesh_actor)
		{
			//逐一设置所有的材质
			/*ovr::uint32 material_cnt = mesh_actor->GetMaterialCount();
			for (ovr::uint32 index = 0; index < material_cnt; index++)
			{
				ovr_engine::OvrMaterial* material = mesh_actor->GetMaterial(index);
				if (nullptr == material)
				{
					continue;
				}
				ovr_engine::OvrShaderParameters* shader_params = material->GetShaderParameters();
				if (nullptr == shader_params)
				{
					continue;
				}
				shader_params->SetNameConstant("DiffuseColor", diffuse_color);
			}*/
		}
		return true;
	}

	ovr::uint32	OvrMeshActorDiffuseColorNode::SetValue(bool a_is_in, const OvrString& a_name, const OvrVector4& a_value)
	{
		if (a_name == kDiffuseColorID)
		{
			diffuse_color = a_value;
			return 1;
		}
		return 0;
	}

	ovr::uint32	OvrMeshActorDiffuseColorNode::SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value) 
	{
		if (a_name == kObjectID)
		{
			//mesh_actor = dynamic_cast<ovr_engine::OvrMeshActor*>(a_value);
			return 1;
		}
		return 0;
	}
}
