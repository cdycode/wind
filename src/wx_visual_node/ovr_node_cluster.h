#ifndef OVR_VISUAL_NODE_OVR_NODE_CLUSTER_H_
#define OVR_VISUAL_NODE_OVR_NODE_CLUSTER_H_

#include <map>

#include <wx_core/visual_node/i_ovr_node_cluster.h>

namespace ovr_core
{
	class IOvrAsyncLoad;
}

namespace ovr_node 
{
	class OvrVisualNode;
	class OvrNodeCluster : public ovr_core::IOvrNodeCluster
	{
	public:
		OvrNodeCluster(ovr_core::OvrSequence* a_own_sequence):ovr_core::IOvrNodeCluster(a_own_sequence){}
		virtual ~OvrNodeCluster() {}

		virtual ovr_core::IOvrVisualNode* CreateNode(const OvrString& a_node_id, const OvrString& a_unique_name = "")override;
		virtual ovr_core::IOvrVisualNode* GetNode(const OvrString& a_node_unique_name)override;

		//销毁node  并断开与此Node相关的所有连接
		virtual bool DestoryNode(ovr_core::IOvrVisualNode* a_node) override;

		//获取所有的Node
		virtual const std::map<OvrString, ovr_core::IOvrVisualNode*>&	GetAllNodeMap()const override { return all_node_map; }
		virtual void	AppendAssetFile(std::map<OvrString, bool>& a_file_map) override;

		virtual void	EndPlay() override;
		virtual void	Update() override;

		void	AddAsyncLoad(ovr_core::IOvrAsyncLoad* a_load) { async_load_list.push_back(a_load); }

	protected:

		std::map<OvrString, ovr_core::IOvrVisualNode*>	all_node_map;

		std::list<ovr_core::IOvrAsyncLoad*> async_load_list;
	};

	//事件
	class OvrNodeClusterEvent : public OvrNodeCluster 
	{
	public:
		OvrNodeClusterEvent(ovr_core::OvrSequence* a_own_sequence);
		virtual ~OvrNodeClusterEvent() {}

		virtual Type	GetType() override { return ovr_core::IOvrNodeCluster::kEvent; }

		virtual void	BeginPlay() override;
		virtual void	EndPlay() override;
		virtual void	Update() override;
	};

	//交互区
	class OvrEventNode;
	class OvrNodeClusterArea : public OvrNodeCluster 
	{
	public:
		OvrNodeClusterArea(ovr_core::OvrSequence* a_own_sequence);
		virtual ~OvrNodeClusterArea() {}

		virtual Type	GetType() override { return ovr_core::IOvrNodeCluster::kArea; }

		virtual void	BeginPlay() override;
		virtual void	EndPlay() override;
		virtual void	Update() override;

	private:

		OvrEventNode*		begin_play_event = nullptr;
		OvrEventNode*		end_play_event = nullptr;
		OvrEventNode*		tick_update_event = nullptr;

		float				last_update_time = 0.0f;
	};
}

#endif