﻿#include "headers.h"
#include "ovr_focus_node.h"
#include <wx_core/ovr_scene_context.h>
#include <wx_core/ovr_scene_camera.h>
#include <wx_core/ovr_sequence.h>
#include "ovr_node_id_define.h"
#include "ovr_variable_node.h"
#include "ovr_node_cluster.h"

namespace ovr_node 
{
	OvrFocusNode::OvrFocusNode() 
	{
		display_name = kFocusName;

		//接口
		interface_list.push_back(ovr_core::OvrNodeInterface(true, "", kInputID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, kGetFocusName, kGetFocusID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, kLostFocusName, kLostFocusID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, kSelectedName, kSelectedID));

		//参数
		in_param_list.push_back(ovr_core::OvrNodeParam(kObjectName, kObjectID,ovr_core::OvrNodeParam::kParamActor, ""));
		in_param_list.push_back(ovr_core::OvrNodeParam(kSelectedTimeName, kSelectedTimeID,ovr_core::OvrNodeParam::kParamFloat, "1.0"));
	}

	void OvrFocusNode::BeginPlay() 
	{
		focus_actor = nullptr;
		get_focus_node = nullptr;
		lost_focus_node = nullptr;
		focused_time = 0.0f;

		//填充输出接口
		for (auto it = out_connect_list.begin(); it != out_connect_list.end(); it++)
		{
			if (it->local_interface == kGetFocusID)
			{
				get_focus_node = (OvrOperatorNode*)it->other_node;
				get_focus_node_interface = it->other_interface;
			}
			else if (it->local_interface == kLostFocusID)
			{
				lost_focus_node = (OvrOperatorNode*)it->other_node;
				lost_focus_node_interface = it->other_interface;
			}
			else if (it->local_interface == kSelectedID)
			{
				select_node = (OvrOperatorNode*)it->other_node;
				select_node_interface = it->other_interface;
			}
		}

		//填充变量输入
		for (auto it = in_connect_list.begin(); it != in_connect_list.end(); it++)
		{
			if (it->local_interface == kObjectID)
			{
				OvrActorNode* actor_node = (OvrActorNode*)it->other_node;
				if (nullptr != actor_node)
				{
					focus_actor = actor_node->GetActor();
				}
			}
		}
	}

	void OvrFocusNode::EndPlay() 
	{
		
	}

	bool OvrFocusNode::Update(float a_delta_time, const OvrString& a_interface_name)
	{ 
		if (nullptr == focus_actor)
		{
			//未设置Actor
			return false;
		}

		//获取射线
		ovr_engine::OvrActor* selected_actor = nullptr;
		ovr_core::OvrSceneContext* scene_context = node_cluster->GetOwnSequence()->GetScene();
		//const ovr_engine::OvrRayIntersetActor* interset_actor = scene_context->GetSceneCamera()->GetFocusActor();
		//if (nullptr != interset_actor)
		{
			//selected_actor = interset_actor->actor;
		}
		if (focus_actor == selected_actor)
		{
			//执行获得焦点操作
			if (nullptr != get_focus_node)
			{
				get_focus_node->Update(a_delta_time, get_focus_node_interface);
			}

			//判断是否达到选中
			focused_time += a_delta_time;
			if (focused_time > selected_time)
			{
				if (nullptr != select_node)
				{
					select_node->Update(a_delta_time, select_node_interface);
				}
				focused_time -= selected_time;
			}
		}
		else 
		{
			//执行丢失焦点操作
			if (nullptr != lost_focus_node)
			{
				lost_focus_node->Update(a_delta_time, lost_focus_node_interface);
			}

			//计时清零
			focused_time = 0.0f;
		}

		return true; 
	}

	ovr::uint32 OvrFocusNode::SetValue(bool a_is_in, const OvrString& a_name, float a_value)
	{
		if (a_name == kSelectedTimeID)
		{
			selected_time = a_value;
			return 1;
		}
		return 0;
	}

	OvrGetFocusActorNode::OvrGetFocusActorNode()
	{
		display_name = kFocusObjectName;

		//参数
		out_param_list.push_back(ovr_core::OvrNodeParam(kObjectName, kObjectID, ovr_core::OvrNodeParam::kParamActor, ""));
	}

	bool OvrGetFocusActorNode::Doing(float a_delta_time)
	{
		OvrString next_interface_name;
		OvrVisualNode* next_node = GetNextNode(kObjectName, next_interface_name);
		if (nullptr != next_node)
		{
			//获取取值
			ovr_core::OvrSceneContext* scene_context = node_cluster->GetOwnSequence()->GetScene();
			/*const ovr_engine::OvrRayIntersetActor* interset_actor = scene_context->GetSceneCamera()->GetFocusActor();
			if (nullptr != interset_actor)
			{
				//直接设置
				next_node->SetValue(true, next_interface_name, interset_actor->actor);
			}*/
		}
		return true;
	}

	//获得与本地接口相连接的Node
	OvrVisualNode*	OvrGetFocusActorNode::GetNextNode(const OvrString& a_local_interface_name, OvrString& a_next_interface_name)
	{
		OvrVisualNode* found_node = nullptr;
		for (auto it = out_connect_list.begin(); it != out_connect_list.end(); it++)
		{
			if (it->local_interface == a_local_interface_name)
			{
				found_node = (OvrVisualNode*)it->other_node;
				a_next_interface_name = it->other_interface;
				break;
			}
		}
		return found_node;
	}

	OvrEnableActorFocusNode::OvrEnableActorFocusNode() 
	{
		display_name = kEnableFocusName;

		//参数
		in_param_list.push_back(ovr_core::OvrNodeParam(kIsFocusName, kIsFocusID,ovr_core::OvrNodeParam::kParamBool, "1"));
		in_param_list.push_back(ovr_core::OvrNodeParam(kObjectName, kObjectID,ovr_core::OvrNodeParam::kParamActor));
	}

	void OvrEnableActorFocusNode::BeginPlay()
	{
		OvrNormalOperatorNode::BeginPlay();

		own_actor = nullptr;

		//填充变量输入
		for (auto it = in_connect_list.begin(); it != in_connect_list.end(); it++)
		{
			if (it->local_interface == kObjectID)
			{
				OvrActorNode* actor_node = (OvrActorNode*)it->other_node;

				if (nullptr != actor_node)
				{
					own_actor = actor_node->GetActor();
				}
				
			}
		}
	}

	bool OvrEnableActorFocusNode::Doing(float a_delta_time)
	{
		if (nullptr != own_actor)
		{
			//own_actor->SetIntersetTestEnable(is_focus);
		}

		return true;
	}

	ovr::uint32 OvrEnableActorFocusNode::SetValue(bool a_is_in, const OvrString& a_name, bool a_value)
	{
		if (a_name == kIsFocusID)
		{
			is_focus = a_value;
			return 1;
		}
		return 0;
	}

	ovr::uint32 OvrEnableActorFocusNode::SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value)
	{
		if (a_name == kObjectID)
		{
			own_actor = a_value;
			return 1;
		}
		return 0;
	}

	OvrMultiFocusNode::OvrMultiFocusNode() 
	{
		display_name = kMultiObjectFocusName;

		//接口
		interface_list.push_back(ovr_core::OvrNodeInterface(true, "", kInputID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, kGetFocusName, kGetFocusID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, kLostFocusName, kLostFocusID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, kSelectedName, kSelectedID));

		//参数
		in_param_list.push_back(ovr_core::OvrNodeParam(kSelectedTimeName, kSelectedTimeID, ovr_core::OvrNodeParam::kParamFloat, "1.0"));
		in_param_list.push_back(ovr_core::OvrNodeParam(kObjectNumName, kObjectNumID, ovr_core::OvrNodeParam::kParamInt, "2"));
		in_param_list.push_back(ovr_core::OvrNodeParam(kObject1Name, kObject1ID,ovr_core::OvrNodeParam::kParamActor));
		in_param_list.push_back(ovr_core::OvrNodeParam(kObject2Name, kObject2ID,ovr_core::OvrNodeParam::kParamActor));

		out_param_list.push_back(ovr_core::OvrNodeParam(kFocusObjectName, kFocusObjectID,ovr_core::OvrNodeParam::kParamActor));
		
		actor_number = 2;
		for (int i = 0; i < 64; i++)
		{
			input_actor[i] = nullptr;
		}
	}

	void OvrMultiFocusNode::BeginPlay()
	{
		current_focus_actor = nullptr;
		//填充输出接口
		for (auto it = out_connect_list.begin(); it != out_connect_list.end(); it++)
		{
			if (it->local_interface == kGetFocusID)
			{
				get_foucus_info.next_node = (OvrOperatorNode*)it->other_node;
				get_foucus_info.next_interface = it->other_interface;
			}
			else if (it->local_interface == kLostFocusID)
			{
				lost_foucus_info.next_node = (OvrOperatorNode*)it->other_node;
				lost_foucus_info.next_interface = it->other_interface;
			}
			else if (it->local_interface == kSelectedID)
			{
				selected_info.next_node = (OvrOperatorNode*)it->other_node;
				selected_info.next_interface = it->other_interface;
			}
			else if (it->local_interface == kFocusObjectID)
			{
				current_focus_actor_out.push_back(OvrOutFlowInfo((OvrOperatorNode*)it->other_node, it->other_interface));
			}
		}

		//填充变量输入
		for (auto it = in_connect_list.begin(); it != in_connect_list.end(); it++)
		{
			ovr::int32 position = it->local_interface.FindLastCharPos('_');
			if (position <= 0)
			{
				continue;
			}
			OvrString id = it->local_interface.SubString(position + 1, it->local_interface.GetStringSize() - position -1);
			int index = atoi(id.GetStringConst());
			if (index > 0)
			{
				input_actor[index - 1] = ((OvrActorNode*)it->other_node)->GetActor();
			}
		}
	}

	bool OvrMultiFocusNode::Update(float a_delta_time, const OvrString& a_interface_name)
	{
		if (nullptr == input_actor[0] && nullptr == input_actor[1])
		{
			//未设置Actor
			return false;
		}

		//获取聚焦的Actor
		ovr_engine::OvrActor* selected_actor = GetFocusActor();

		/*过滤以下两种情况 1、a_actor为空 2、不再备选Actor中  这两种情况 不需要进一步处理*/
		if (nullptr == selected_actor)
		{
			//先处理选中的Actor为空的情况 上次选中的为非空 需要通知失去焦点
			if (nullptr != current_focus_actor)
			{
				//通知失去焦点
				if (nullptr != lost_foucus_info.next_node)
				{
					lost_foucus_info.next_node->Update(a_delta_time, lost_foucus_info.next_interface);
				}
				SetCurrentFocusActor(nullptr);
			}
			return true;
		}
		else 
		{
			//选中的actor必须在输入Actor中
			if (!IsInputActor(selected_actor))
			{
				return false;
			}
		}

		/*选中的Actor非空*/
		if (nullptr == current_focus_actor)
		{
			//当前选中的Actor为空  直接更新 并通知获得焦点
			SetCurrentFocusActor(selected_actor);

			//通知获得焦点
			if (nullptr != get_foucus_info.next_node)
			{
				get_foucus_info.next_node->Update(a_delta_time, get_foucus_info.next_interface);
			}
		}
		else 
		{
			if (current_focus_actor == selected_actor)
			{
				//跟上次选中的Actor一致 进行时间值累加
				focused_time += a_delta_time;
				if (focused_time > selected_time)
				{
					//通知进行选中
					if (nullptr != selected_info.next_node)
					{
						selected_info.next_node->Update(a_delta_time, selected_info.next_interface);
					}
					focused_time -= selected_time;
				}

				//通知获得焦点
				if (nullptr != get_foucus_info.next_node)
				{
					get_foucus_info.next_node->Update(a_delta_time, get_foucus_info.next_interface);
				}
			}
			else 
			{
				//跟上次选中的不一样
				//1、通知失去焦点
				if (nullptr != lost_foucus_info.next_node)
				{
					lost_foucus_info.next_node->Update(a_delta_time, lost_foucus_info.next_interface);
				}
				//2、设置新的Actor
				SetCurrentFocusActor(selected_actor);
				//3、通知获得焦点
				if (nullptr != get_foucus_info.next_node)
				{
					get_foucus_info.next_node->Update(a_delta_time, get_foucus_info.next_interface);
				}
			}

		}
		return true;
	}

	ovr::uint32	OvrMultiFocusNode::SetValue(bool a_is_in, const OvrString& a_name, int a_value) 
	{
		if (a_name == kObjectNumID)
		{
			return ResizeInputActor(a_value);
		}
		return 0;
	}

	ovr::uint32 OvrMultiFocusNode::SetValue(bool a_is_in, const OvrString& a_name, float a_value)
	{
		if (a_name == kSelectedTimeID)
		{
			selected_time = a_value;
			return 1;
		}
		return 0;
	}

	ovr::uint32 OvrMultiFocusNode::SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value)
	{
		if (a_name == kObject1ID)
		{
			input_actor[0] = a_value;
		}
		else if (a_name == kObject2ID)
		{
			input_actor[1] = a_value;
		}
		else
		{
			return 0;
		}
		return 1;
	}

	//获得当前选中的Actor
	ovr_engine::OvrActor* OvrMultiFocusNode::GetFocusActor() 
	{
		ovr_engine::OvrActor* selected_actor = nullptr;
		ovr_core::OvrSceneContext* scene_context = node_cluster->GetOwnSequence()->GetScene();
		/*const ovr_engine::OvrRayIntersetActor* interset_actor = scene_context->GetSceneCamera()->GetFocusActor();
		if (nullptr != interset_actor)
		{
			selected_actor = interset_actor->actor;
		}

		return selected_actor;*/
		return nullptr;
	}

	void OvrMultiFocusNode::SetCurrentFocusActor(ovr_engine::OvrActor* a_actor) 
	{
		current_focus_actor = a_actor;
		for (auto it = current_focus_actor_out.begin(); it != current_focus_actor_out.end(); it++)
		{
			it->next_node->SetValue(true, it->next_interface, a_actor);
		}
		focused_time = 0;	//无论是设置为空 或者非空 都将计时清空
	}

	bool OvrMultiFocusNode::IsInputActor(ovr_engine::OvrActor* a_actor) 
	{
		bool is_in = false;
		for (ovr::uint32 i  = 0; i < actor_number; i++)
		{
			if (input_actor[i] == a_actor)
			{
				is_in = true;
				break;
			}
		}
		return is_in;
	}

	//重新设置对象数目
	ovr::uint32	OvrMultiFocusNode::ResizeInputActor(ovr::uint32 a_new_size) 
	{
		//设置的值非法
		if (a_new_size <= 0 || a_new_size > 64)
		{
			return 0;
		}

		//值相同
		if (a_new_size == actor_number)
		{
			return 1;
		}

		//需要减少对象
		if ((int)actor_number > a_new_size)
		{
			int diff = actor_number - a_new_size;
			auto it = in_param_list.end();
			for (int i = 0; i < diff; i++)
			{
				input_actor[actor_number + i] = nullptr;

				//减少列表中的对象
				--it;
				RmvInputParamConnect(it->unique_name);
				it = in_param_list.erase(it);
			}
		}
		else 
		{
			int add = a_new_size - actor_number;
			for (int i = 1; i <= add; i++)
			{
				char temp[50];
				sprintf(temp, kObjectFormatName, actor_number + i);
				char id[50];
				sprintf(id, kObjectFormatID, actor_number + i);
				in_param_list.push_back(ovr_core::OvrNodeParam(temp, id,ovr_core::OvrNodeParam::kParamActor));
			}
		}
		actor_number = a_new_size;
		return 2;
	}

	void OvrMultiFocusNode::RmvInputParamConnect(const OvrString& a_input_param_name) 
	{
		IOvrVisualNode* other_node = nullptr;
		OvrString		other_interface;

		//寻找链接
		auto connect_it = in_connect_list.begin();
		while (connect_it != in_connect_list.end())
		{
			if (connect_it->local_interface == a_input_param_name)
			{
				other_node = connect_it->other_node;
				other_interface = connect_it->other_interface;
				break;
			}
			connect_it++;
		}
		
		if (nullptr != other_node)
		{
			other_node->RmvOutConnect(other_interface);
		}
	}
}

