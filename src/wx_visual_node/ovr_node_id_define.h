﻿#ifndef OVR_PROJECT_OVR_INTERACTIVE_DEFINE_H_
#define OVR_PROJECT_OVR_INTERACTIVE_DEFINE_H_

#include <wx_base/wx_string.h>

namespace ovr_node 
{
	class OvrNodeClassID 
	{
	public:
		/*事件Node*/
		static	const  char*		event_begin_category;
		static  const  char*		event_update_category;
		static  const  char*		event_end_category;

		/*操作Node*/
		static	const char*			operator_focus_category;
		static	const char*			operator_multi_focus_category;
		static	const char*			operator_get_focus_actor_category;
		static	const char*			operator_enable_actor_focus_category;
		static	const char*			operator_goto_category;
		static	const char*			operator_change_texture_category;
		static	const char*			operator_change_diffuse_texture_category;
		static	const char*			operator_play_audio_category;
		static	const char*			operator_timer_category;
		static	const char*			operator_alternate_category;
		static	const char*			operator_follow_camera_category;
		static	const char*			operator_camera_front_category;
		static	const char*			operator_visiable_category;
		static	const char*			operator_sprite_picture_category;
		static	const char*			operator_cursor_temp_category;
		static	const char*			operator_cursor_visual_category;
		static	const char*			operator_set_main_camera_category;
		static	const char*			operator_set_ambient_color_category;
		static	const char*			operator_branch_category;
		static	const char*			operator_switch_actor_category;
		static	const char*			operator_set_text_category;

		/*移动*/
		static	const char*			operator_movement_category;
		static	const char*			operator_rotation_category;
		static	const char*			operator_move_position_category;
		static	const char*			operator_movement_id;
		static	const char*			operator_rotation_id;
		static	const char*			operator_move_position_id;

		/*变量*/
		static	const char*			variable_actor_category;

		//id
		static	const  char*		event_begin_id;
		static  const  char*		event_update_id;
		static  const  char*		event_end_id;

		static	const char*			operator_focus_id;
		static	const char*			operator_multi_focus_id;
		static	const char*			operator_get_focus_actor_id;
		static	const char*			operator_enable_actor_focus_id;
		static	const char*			operator_goto_id;
		static	const char*			operator_change_texture_id;
		static	const char*			operator_change_diffuse_texture_id;
		static	const char*			operator_play_audio_id;
		static	const char*			operator_timer_id;
		static	const char*			operator_alternate_id;
		static	const char*			operator_follow_camera_id;
		static	const char*			operator_camera_front_id;
		static	const char*			operator_visiable_id;
		static	const char*			operator_sprite_picture_id;
		
		static	const char*			operator_set_main_camera_id;
		static	const char*			operator_set_ambient_color_id;
		static	const char*			operator_branch_node_id;
		static	const char*			operator_switch_actor_node_id;
		static	const char*			operator_set_text_node_id;

		static	const char*			variable_actor_id;

		//特效
		static	const char*			operator_effect_feather_category;
		static	const char*			operator_effect_feather_id;

		//临时Node  全局状态
		static const char*			operator_set_status_category;
		static const char*			operator_set_status_id;
		static const char*			operator_switch_status_category;
		static const char*			operator_switch_status_id;

		static const char*			operator_set_actor_diffusecolor_category;
		static const char*			operator_set_actor_diffusecolor_id;

		/*操作类型*/
		static const char*			material_type_option;	//材质
		static const char*			texture_type_option;	//纹理
		static const char*			audio_type_option;		//声音
	};
}

#endif
