﻿#ifndef OVR_VISUAL_NODE_OVR_NODE_CONSTANT_DEFINE_H_
#define OVR_VISUAL_NODE_OVR_NODE_CONSTANT_DEFINE_H_

namespace ovr_node 
{	
	extern const char* kAudioFileID;
	extern const char* kAudioFileName;

	extern const char* kBackwordID;
	extern const char* kBackwordName;

	extern const char* kBeginPositionID;
	extern const char* kBeginPositionName;

	extern const char* kBeginAngleID;
	extern const char* kBeginAngleName;

	extern const char* kBlendTimeID;
	extern const char* kBlendTimeName;

	extern const char* kCameraID;
	extern const char* kCameraName;

	extern const char* kClipID;
	extern const char* kClipName;

	extern const char* kColorID;
	extern const char* kColorName;

	extern const char* kColumnNumID;
	extern const char* kColumnNumName;

	extern const char* kDefaultIndexID;
	extern const char* kDefaultIndexName;

	extern const char* kDiffuseColorID;
	extern const char* kDiffuseColorName;

	extern const char* kDistanceID;
	extern const char* kDistanceName;

	extern const char* kEnableFocusID;
	extern const char* kEnableFocusName;

	extern const char* kEndAngleID;
	extern const char* kEndAngleName;

	extern const char* kEndPositionID;
	extern const char* kEndPositionName;

	extern const char* kFeatherNodeID;
	extern const char* kFeatherNodeName;

	extern const char* kFocusID;
	extern const char* kFocusName;

	extern const char* kFocusObjectID;
	extern const char* kFocusObjectName;

	extern const char* kFollowID;
	extern const char* kFollowName;

	extern const char* kForwardID;
	extern const char* kForwardName;

	extern const char* kFrontCameraID;
	extern const char* kFrontCameraName;

	extern const char* kGetFocusID;
	extern const char* kGetFocusName;

	extern const char* kInputID;
	extern const char* kInputName;

	extern const char* kInput1ID;
	extern const char* kInput1Name;

	extern const char* kInput2ID;
	extern const char* kInput2Name;

	extern const char* kInputObjectID;
	extern const char* kInputObjectName;

	extern const char* kIsFocusID;
	extern const char* kIsFocusName;

	extern const char* kIsLoopID;
	extern const char* kIsLoopName;

	extern const char* kIsVisualID;
	extern const char* kIsVisualName;

	extern const char* kLoopTimeID;
	extern const char* kLoopTimeName;

	extern const char* kLostFocusID;
	extern const char* kLostFocusName;

	extern const char* kMultiObjectFocusID;
	extern const char* kMultiObjectFocusName;

	extern const char* kNoneID;
	extern const char* kNoneName;

	extern const char* kNumID;
	extern const char* kNumName;

	extern const char* kObjectID;
	extern const char* kObjectName;

	extern const char* kObjectFormatID;
	extern const char* kObjectFormatName;

	extern const char* kObject1ID;
	extern const char* kObject1Name;

	extern const char* kObject2ID;
	extern const char* kObject2Name;

	extern const char* kObjectNumID;
	extern const char* kObjectNumName;

	extern const char* kOutputID;
	extern const char* kOutputName;

	extern const char* kOutput1ID;
	extern const char* kOutput1Name;

	extern const char* kOutput2ID;
	extern const char* kOutput2Name;

	extern const char* kOutputNumID;
	extern const char* kOutputNumName;

	extern const char* kOutputObject1ID;
	extern const char* kOutputObject1Name;
	extern const char* kOutputObject2ID;
	extern const char* kOutputObject2Name;

	extern const char* kOutputFormatID;
	extern const char* kOutputFormatName;

	extern const char* kOwnMaterialID;
	extern const char* kOwnMaterialName;

	extern const char* kPlayID;
	extern const char* kPlayName;

	extern const char* kParticleObjectID;
	extern const char* kParticleObjectName;

	extern const char* kParticleMaxNumID;
	extern const char* kParticleMaxNumName;

	extern const char* kResetID;
	extern const char* kResetName;

	extern const char* kReturnID;
	extern const char* kReturnName;

	extern const char* kRowNumID;
	extern const char* kRowNumName;

	extern const char* kSelectedID;
	extern const char* kSelectedName;

	extern const char* kSelectedTimeID;
	extern const char* kSelectedTimeName;

	extern const char* kStopID;
	extern const char* kStopName;

	extern const char* kTextID;
	extern const char* kTextName;

	extern const char* kTexture1ID;
	extern const char* kTexture1Name;

	extern const char* kTexture2ID;
	extern const char* kTexture2Name;

	extern const char* kTextureID;
	extern const char* kTextureName;

	extern const char* kTextureDiffuseID;
	extern const char* kTextureDiffuseName;

	extern const char* kTextureEmissiveID;
	extern const char* kTextureEmissiveName;
	
	extern const char* kTextureModifyID;
	extern const char* kTextureModifyName;

	extern const char* kTextureNormalID;
	extern const char* kTextureNormalName;

	extern const char* kTextureSpecularID;
	extern const char* kTextureSpecularName;

	extern const char* kTimeID;
	extern const char* kTimeName;

	extern const char* kUpdateID;
	extern const char* kUpdateName;

	extern const char* kValueID;
	extern const char* kValueName;

	extern const char* kVerticalID;
	extern const char* kVerticalName;

	extern const char* kVolumeID;
	extern const char* kVolumeName;

}

#endif
