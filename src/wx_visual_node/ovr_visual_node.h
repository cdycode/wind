﻿#ifndef OVR_VISUAL_NODE_OVR_VISUAL_NODE_H_
#define OVR_VISUAL_NODE_OVR_VISUAL_NODE_H_

#include <list>
#include <wx_base/wx_string.h>
#include <wx_base/wx_vector3.h>
#include <wx_base/wx_vector4.h>

#include <wx_core/visual_node/i_ovr_visual_node.h>

namespace ovr_engine 
{
	class OvrActor;
}

namespace ovr_core
{
	class OvrSequence;
}

namespace ovr_node 
{
	class OvrNodeCluster;
	class OvrVisualNode : public ovr_core::IOvrVisualNode
	{
	public:
		OvrVisualNode();
		virtual ~OvrVisualNode() {}

		void	SetCluster(OvrNodeCluster* a_cluster) { node_cluster = a_cluster; }

		//播放前后的准备
		virtual void BeginPlay() override {}
		virtual void EndPlay() override {}
		virtual bool Update(float a_delta_time, const OvrString& a_interface_name) override { return false; }

		//获取参数信息
		virtual const std::list<ovr_core::OvrNodeInterface>&	GetInterface() const override  { return interface_list; }
		virtual const std::list<ovr_core::OvrNodeParam>&		GetInParamList()const override { return in_param_list; }
		virtual const std::list<ovr_core::OvrNodeParam>&		GetOutParamList()const override { return out_param_list; }
		virtual const std::list<ovr_core::OvrNodeConnect>&	GetOutConnects()const override { return out_connect_list; }
		virtual const std::list<ovr_core::OvrNodeConnect>&	GetInConnects()const override { return in_connect_list; }

		virtual ovr_core::OvrNodeParam::Type	GetParamType(bool a_is_in, const OvrString& a_param_name)const override;

		//输出链接
		virtual bool	AddOutConnect(const OvrString& a_local_interface, \
			ovr_core::IOvrVisualNode* a_other_node, \
										const OvrString& a_other_interface) override;
		virtual bool	RmvOutConnect(const OvrString& a_local_interface) override;

		//断开所有连接
		virtual void	BreakAllConnect() override;

		// a_is_in 是输入参数 还是出参数
		// a_name 参数名
		// a_value 取值 内容根据参数的类型
		virtual ovr::uint32	SetParamValue(bool a_is_in, const OvrString& a_name, const OvrString a_value) override;

		//获取Node中 使用到的资源
		virtual void AppendAssetFile(std::map<OvrString, bool>& a_file_map) override { }
	public:
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, int a_value) { return false; }
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, float a_value) { return false; }
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, bool a_value) { return false; }
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, const OvrVector3& a_value) { return false; }
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, const OvrVector4& a_value) { return false; }
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, const OvrString& a_value) { return false; }
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value) { return false; }
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, ovr_core::OvrSequence* a_sequence, ovr::uint64 a_frame_index) { return false; }

	protected:
		//输入链接 在添加输出链接时 会自动调用此接口 添加另一端的输入信息 故此接口私有 外边不必调用
		bool	AddInConnect(const OvrString& a_other_interface, \
			ovr_core::IOvrVisualNode* a_other_node, \
								const OvrString& a_local_interface);
		bool	RmvInConnect(const OvrString& a_local_interface);

		//解析字符串 获取相应的值
		bool	GetVector4(const OvrString& a_string, OvrVector4& a_vector4);

	protected:

		std::list<ovr_core::OvrNodeInterface>		interface_list;		//接口列表
		std::list<ovr_core::OvrNodeParam>			in_param_list;		//输入参数列表
		std::list<ovr_core::OvrNodeParam>			out_param_list;		//输出参数列表

		std::list<ovr_core::OvrNodeConnect>		out_connect_list;	//输出接口连接信息
		std::list<ovr_core::OvrNodeConnect>		in_connect_list;	//输入接口连接信息

		OvrNodeCluster* node_cluster = nullptr;
	};
}

#endif