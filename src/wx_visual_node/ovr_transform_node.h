﻿#ifndef OVR_VISUAL_NODE_OVR_TRANSFORM_NODE_H_
#define OVR_VISUAL_NODE_OVR_TRANSFORM_NODE_H_

#include "ovr_operator_node.h"

namespace ovr_node 
{
	//实现Actor的移动逻辑
	class OvrActorNode;
	class OvrMovementNode : public OvrOperatorNode
	{
	public:
		OvrMovementNode();
		virtual ~OvrMovementNode() {}

		virtual void BeginPlay() override;

		virtual bool	Update(float a_delta_time, const OvrString& a_interface_name) override;

	protected:
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, float a_value)override;
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, const OvrVector3& a_value) override;
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value) override;
	private:
		OvrVector3		begin_position;	//开始位置
		OvrQuaternion	begin_angle;	//开始角度
		OvrVector3		end_posistion;	//结束位置
		OvrQuaternion	end_angle;		//结束角度	

		float			move_time = 1.0f;

		ovr_engine::OvrActor*	move_actor = nullptr;
		OvrActorNode*			actor_node = nullptr;				//输入的actorNode

		float			current_move_time = 0.0f;	//当前运动的时间 根据时间 确定当前的位置  0为begin_angle move_time为end_posistion
		OvrVector3		current_position;			//当前位置
		OvrQuaternion	current_angle;				//当前角度
	};

	//直接跳转
	class OvrMovePositionNode : public OvrNormalOperatorNode
	{
	public:
		OvrMovePositionNode();
		virtual ~OvrMovePositionNode() {}

		virtual void	BeginPlay() override;

	protected:
		virtual bool	Doing(float a_delta_time) override;

		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, const OvrVector3& a_value) override;
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value) override;
	private:

		OvrVector3		end_posistion;	//结束位置

		ovr_engine::OvrActor*	move_actor = nullptr;
		OvrActorNode*			actor_node = nullptr;				//输入的actorNode
	};

	class OvrRotationNode : public OvrNormalOperatorNode
	{
	public:
		OvrRotationNode();
		virtual ~OvrRotationNode() {}

		virtual void	BeginPlay() override;

	protected:

		//执行体 需要由子类实现
		virtual bool		Doing(float a_delta_time) override;

		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, const OvrVector3& a_value) override;
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value) override;

	private:
		ovr_engine::OvrActor*	actor = nullptr;
		OvrVector3				rotation;			
	};
}

#endif
