﻿#ifndef OVR_VISUAL_NODE_OVR_FOCUS_NODE_H_
#define OVR_VISUAL_NODE_OVR_FOCUS_NODE_H_

#include "ovr_operator_node.h"

namespace ovr_engine 
{
	class OvrActor;
}
namespace ovr_node
{
	class OvrActorNode;
	class OvrFocusNode : public OvrOperatorNode
	{
	public:
		OvrFocusNode();
		virtual ~OvrFocusNode() {}

		virtual void BeginPlay() override;
		virtual void EndPlay()override;

		virtual bool	Update(float a_delta_time, const OvrString& a_interface_name) override;

	protected:
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, float a_value) override;
	private:
		
		ovr_engine::OvrActor*	focus_actor = nullptr;


		OvrOperatorNode*			get_focus_node = nullptr;		//输出的OvrOperatorNode
		OvrOperatorNode*			lost_focus_node = nullptr;		//输出的OvrOperatorNode
		OvrString					get_focus_node_interface;		//连接到get_focus_node的接口名
		OvrString					lost_focus_node_interface;		//连接到lost_focus_node_interface的接口名


		float		selected_time = 1.0f;	//超过此时间  可认为是选中
		float		focused_time = 0.0f;	//当前已聚焦的累计时间

		OvrOperatorNode*			select_node = nullptr;		//选中输出的OvrOperatorNode
		OvrString					select_node_interface;		//连接到select_node_interface的接口名
	};

	//获得焦点Actor
	class OvrGetFocusActorNode : public OvrNormalOperatorNode
	{
	public:
		OvrGetFocusActorNode();
		virtual ~OvrGetFocusActorNode() {}

	protected:
		virtual bool	Doing(float a_delta_time) override;

	private:

		//获得与本地接口相连接的Node
		OvrVisualNode*	GetNextNode(const OvrString& a_local_interface_name, OvrString& a_next_interface_name);
	};

	//设置Focus
	class OvrEnableActorFocusNode : public OvrNormalOperatorNode
	{
	public:
		OvrEnableActorFocusNode();
		virtual ~OvrEnableActorFocusNode() {}

		virtual void	BeginPlay() override;

	protected:
		virtual bool	Doing(float a_delta_time) override;

		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, bool a_value) override;
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value) override;

	private:
		bool					is_focus = true;
		ovr_engine::OvrActor*	own_actor = nullptr;
	};

	//多个Actor的选择
	class OvrMultiFocusNode : public OvrOperatorNode
	{
	public:
		OvrMultiFocusNode();
		virtual ~OvrMultiFocusNode() {}

		virtual void	BeginPlay() override;

		virtual bool	Update(float a_delta_time, const OvrString& a_interface_name) override;

	protected:
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, int a_value) override;
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, float a_value) override;
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value) override;

	private:
		//获得当前选中的Actor
		ovr_engine::OvrActor*	GetFocusActor();
		void					SetCurrentFocusActor(ovr_engine::OvrActor* a_actor);
		bool					IsInputActor(ovr_engine::OvrActor* a_actor);

		//重新设置对象数目
		ovr::uint32		ResizeInputActor(ovr::uint32 a_new_size);
		void			RmvInputParamConnect(const OvrString& a_input_param_name);

	private:
		ovr::uint32				actor_number = 2;
		ovr_engine::OvrActor*	input_actor[64];		//输入Actor

		OvrOutFlowInfo	get_foucus_info;
		OvrOutFlowInfo	lost_foucus_info;
		OvrOutFlowInfo	selected_info;

		float		selected_time = 1.0f;	//超过此时间  可认为是选中
		float		focused_time = 0.0f;	//当前已聚焦的累计时间

		//当前聚焦的Actor
		ovr_engine::OvrActor*		current_focus_actor = nullptr;
		std::list<OvrOutFlowInfo>	current_focus_actor_out;		//可能会有多个	
	};
}

#endif
