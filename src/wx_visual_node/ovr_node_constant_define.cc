﻿#include "headers.h"
#include "ovr_node_constant_define.h"

namespace ovr_node 
{
	const char* kAudioFileID = "audio_file";
	const char* kAudioFileName = "声音";

	const char* kBackwordID = "backward";
	const char* kBackwordName = "回退";

	const char* kBeginPositionID = "begin_position";
	const char* kBeginPositionName = "开始位置";

	const char* kBeginAngleID = "begin_angle";
	const char* kBeginAngleName = "开始角度";

	const char* kBlendTimeID = "blend_time";
	const char* kBlendTimeName = "过渡时间";

	const char* kCameraID = "camera";
	const char* kCameraName = "相机";

	const char* kClipID = "clip";
	const char* kClipName = "片段";

	const char* kColorID = "color";
	const char* kColorName = "颜色";

	const char* kColumnNumID = "column_num";
	const char* kColumnNumName = "列数";

	const char* kDefaultIndexID = "default_index";
	const char* kDefaultIndexName = "默认索引";

	const char* kDiffuseColorID = "diffuse_color";
	const char* kDiffuseColorName = "DiffuseColor";

	const char* kDistanceID = "distance";
	const char* kDistanceName = "距离";

	const char* kEnableFocusID = "enable_foucs";
	const char* kEnableFocusName = "启用焦点";

	const char* kEndAngleID = "end_angle";
	const char* kEndAngleName = "目的角度";

	const char* kEndPositionID = "end_position";
	const char* kEndPositionName = "结束位置";

	const char* kFeatherNodeID = "feather_effect";
	const char* kFeatherNodeName = "羽毛特效";

	const char* kFocusID = "focus";
	const char* kFocusName = "焦点";

	const char* kFocusObjectID = "focus_object";
	const char* kFocusObjectName = "焦点对象";

	const char* kFollowID = "follow";
	const char* kFollowName = "跟随";

	const char* kForwardID = "forward";
	const char* kForwardName = "前进";

	const char* kFrontCameraID = "front_camera";
	const char* kFrontCameraName = "相机前方";

	const char* kGetFocusID = "get_focus";
	const char* kGetFocusName = "获得焦点";

	const char* kInputID = "input";
	const char* kInputName = "输入";

	const char* kInput1ID = "input_1";
	const char* kInput1Name = "输入_1";

	const char* kInput2ID = "input_2";
	const char* kInput2Name = "输入 2";

	const char* kInputObjectID = "input_object";
	const char* kInputObjectName = "输入对象";

	const char* kIsFocusID = "is_focus";
	const char* kIsFocusName = "聚焦";

	const char* kIsLoopID = "is_loop";
	const char* kIsLoopName = "循环";

	const char* kIsVisualID = "visual";
	const char* kIsVisualName = "可见";

	const char* kLoopTimeID = "loop_time";
	const char* kLoopTimeName = "循环时间";

	const char* kLostFocusID = "lost_focus";
	const char* kLostFocusName = "失去焦点";

	const char* kMultiObjectFocusID = "multi_object_foucs";
	const char* kMultiObjectFocusName = "多对象焦点";

	const char* kNoneID = "none";
	const char* kNoneName = "都不是";

	const char* kNumID = "num";
	const char* kNumName = "数目";

	const char* kObjectID = "object";
	const char* kObjectName = "对象";

	const char* kObjectFormatID = "object_%d";
	const char* kObjectFormatName = "对象 %d";

	const char* kObject1ID = "object_1";
	const char* kObject1Name = "对象 1";

	const char* kObject2ID = "object_2";
	const char* kObject2Name = "对象 2";

	const char* kObjectNumID = "object_num";
	const char* kObjectNumName = "对象数目";

	const char* kOutputID = "output";
	const char* kOutputName = "输出";

	const char* kOutput1ID = "output_1";
	const char* kOutput1Name = "输出 1";

	const char* kOutput2ID = "output_2";
	const char* kOutput2Name = "输出 2";

	const char* kOutputNumID = "output_num";
	const char* kOutputNumName = "输出数目";

	const char* kOutputObject1ID = "output_object_1";
	const char* kOutputObject1Name = "对象 1";
	const char* kOutputObject2ID = "output_object_2";
	const char* kOutputObject2Name = "对象 2";

	const char* kOutputFormatID = "out_%d";
	const char* kOutputFormatName = "输出 %d";

	const char* kOwnMaterialID = "own_material";
	const char* kOwnMaterialName = "所属材质";

	const char* kPlayID = "play";
	const char* kPlayName = "播放";

	const char* kParticleObjectID = "particle_object";
	const char* kParticleObjectName = "粒子对象";

	const char* kParticleMaxNumID = "particle_max_num";
	const char* kParticleMaxNumName = "最大粒子数";

	const char* kResetID = "reset";
	const char* kResetName = "重置";

	const char* kReturnID = "return";
	const char* kReturnName = "返回值";

	const char* kRowNumID = "row_num";
	const char* kRowNumName = "行数";

	const char* kSelectedID = "selected";
	const char* kSelectedName = "选中";

	const char* kSelectedTimeID = "selected_time";
	const char* kSelectedTimeName = "选中时间";

	const char* kStopID = "stop";
	const char* kStopName = "停止";

	const char* kTextID = "text";
	const char* kTextName = "文本";

	const char* kTexture1ID = "texture_1";
	const char* kTexture1Name = "纹理 1";

	const char* kTexture2ID = "texture_2";
	const char* kTexture2Name = "纹理 2";

	const char* kTextureID = "texture";
	const char* kTextureName = "纹理";

	const char* kTextureDiffuseID = "diffuse_texture";
	const char* kTextureDiffuseName = "漫反射贴图";

	const char* kTextureEmissiveID = "emissive_texture";
	const char* kTextureEmissiveName = "自发光贴图";

	const char* kTextureModifyID = "modify_texture";
	const char* kTextureModifyName = "修改纹理";

	const char* kTextureNormalID = "normal_texture";
	const char* kTextureNormalName = "法线贴图";

	const char* kTextureSpecularID = "specular_texture";
	const char* kTextureSpecularName = "高光贴图";

	const char* kTimeID = "time";
	const char* kTimeName = "时间";

	const char* kUpdateID = "update";
	const char* kUpdateName = "更新";

	const char* kValueID = "value";
	const char* kValueName = "值";

	const char* kVerticalID = "vertical";
	const char* kVerticalName = "垂直";

	const char* kVolumeID = "volume";
	const char* kVolumeName = "音量";
}
