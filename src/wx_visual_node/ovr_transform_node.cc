﻿#include "headers.h"
#include "ovr_transform_node.h"
#include "ovr_variable_node.h"
#include <wx_engine/ovr_actor.h>

namespace ovr_node 
{
	OvrMovementNode::OvrMovementNode()
	{
		//接口
		interface_list.push_back(ovr_core::OvrNodeInterface(true, kForwardName, kForwardID));
		interface_list.push_back(ovr_core::OvrNodeInterface(true, kBackwordName, kBackwordID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, "", kOutputID));

		//参数
		in_param_list.push_back(ovr_core::OvrNodeParam(kBeginPositionName, kBeginPositionID, ovr_core::OvrNodeParam::kParamVector3, "0.0:0.0:0.0"));
		in_param_list.push_back(ovr_core::OvrNodeParam(kBeginAngleName, kBeginAngleID,ovr_core::OvrNodeParam::kParamVector3, "0.0:0.0:0.0"));
		in_param_list.push_back(ovr_core::OvrNodeParam(kEndPositionName, kEndPositionID,ovr_core::OvrNodeParam::kParamVector3, "0.0:0.0:0.0"));
		in_param_list.push_back(ovr_core::OvrNodeParam(kEndAngleName, kEndAngleID,ovr_core::OvrNodeParam::kParamVector3, "0.0:0.0:0.0"));
		in_param_list.push_back(ovr_core::OvrNodeParam(kTimeName, kTimeID,ovr_core::OvrNodeParam::kParamFloat, "1.0"));
		in_param_list.push_back(ovr_core::OvrNodeParam(kObjectName, kObjectID,ovr_core::OvrNodeParam::kParamActor, ""));
	}

	void OvrMovementNode::BeginPlay()
	{
		current_position = begin_position;
		current_angle = begin_angle;
		current_move_time = 0.0f;

		actor_node = nullptr;

		//填充变量输入
		for (auto it = in_connect_list.begin(); it != in_connect_list.end(); it++)
		{
			if (it->local_interface == kObjectID)
			{
				actor_node = (OvrActorNode*)it->other_node;
			}
		}
	}

	bool OvrMovementNode::Update(float a_delta_time, const OvrString& a_interface_name)
	{
		if (nullptr != actor_node)
		{
			move_actor = actor_node->GetActor();
		}

		if (nullptr == move_actor)
		{
			return false;
		}
		bool is_success = false;
		if (a_interface_name == kForwardID)
		{
			//向终点运动
			//计算时间
			current_move_time += a_delta_time;
			if (current_move_time > move_time)
			{
				current_move_time = move_time;
			}
			is_success = true;
		}
		else if (a_interface_name == kBackwordID)
		{
			//向起点运动
			current_move_time -= a_delta_time;
			if (current_move_time < 0)
			{
				current_move_time = 0;
			}
			is_success = true;
		}
		if (is_success)
		{
			float progress = current_move_time / move_time;
			current_position = end_posistion.lerp(begin_position, progress);
			current_angle = OvrQuaternion::lerp(begin_angle, end_angle, progress);
			move_actor->SetPosition(current_position);
			move_actor->SetQuaternion(current_angle);
		}
		return is_success;
	}

	ovr::uint32 OvrMovementNode::SetValue(bool a_is_in, const OvrString& a_name, float a_value)
	{
		if (a_name == kTimeID)
		{
			move_time = a_value;
			return 1;
		}
		return 0;
	}

	ovr::uint32 OvrMovementNode::SetValue(bool a_is_in, const OvrString& a_name, const OvrVector3& a_value)
	{
		bool is_success = true;
		if (a_name == kEndPositionID)
		{
			end_posistion = a_value;
		}
		else if (a_name == kEndAngleID)
		{
			end_angle = OvrQuaternion::make_euler_quat(OVR_DEG_TO_RAD(a_value[0]), OVR_DEG_TO_RAD(a_value[1]), OVR_DEG_TO_RAD(a_value[2]));
		}
		else if (a_name == kBeginPositionID)
		{
			begin_position = a_value;
		}
		else if (a_name == kBeginAngleID)
		{
			begin_angle = OvrQuaternion::make_euler_quat(OVR_DEG_TO_RAD(a_value[0]), OVR_DEG_TO_RAD(a_value[1]), OVR_DEG_TO_RAD(a_value[2]));
		}
		else
		{
			return 0;
		}
		return 1;
	}
	ovr::uint32 OvrMovementNode::SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value)
	{
		if (nullptr == a_value)
		{
			return 0;
		}
		if (a_name == kObjectID)
		{
			move_actor = a_value;
			return 1;
		}
		return 0;
	}

	OvrMovePositionNode::OvrMovePositionNode()
	{
		in_param_list.push_back(ovr_core::OvrNodeParam(kObjectName, kObjectID,ovr_core::OvrNodeParam::kParamActor, ""));
		in_param_list.push_back(ovr_core::OvrNodeParam(kEndPositionName, kEndPositionID,ovr_core::OvrNodeParam::kParamVector3, "0.0:0.0:0.0"));
	}

	void OvrMovePositionNode::BeginPlay()
	{
		actor_node = nullptr;

		//填充变量输入
		for (auto it = in_connect_list.begin(); it != in_connect_list.end(); it++)
		{
			if (it->local_interface == kObjectID)
			{
				actor_node = (OvrActorNode*)it->other_node;
			}
		}

		OvrNormalOperatorNode::BeginPlay();
	}

	bool OvrMovePositionNode::Doing(float a_delta_time)
	{
		if (nullptr != actor_node)
		{
			move_actor = actor_node->GetActor();
		}

		if (nullptr != move_actor)
		{
			move_actor->SetPosition(end_posistion);
		}
		return true;
	}

	ovr::uint32 OvrMovePositionNode::SetValue(bool a_is_in, const OvrString& a_name, const OvrVector3& a_value)
	{
		if (a_name == kEndPositionID)
		{
			end_posistion = a_value;
			return 1;
		}
		return 0;
	}

	ovr::uint32 OvrMovePositionNode::SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value)
	{
		if (a_name == kObjectID)
		{
			move_actor = a_value;
			return 1;
		}
		return 0;
	}

	void OvrRotationNode::BeginPlay()
	{
		OvrNormalOperatorNode::BeginPlay();

		actor = nullptr;

		//填充变量输入
		for (auto it = in_connect_list.begin(); it != in_connect_list.end(); it++)
		{
			if (it->local_interface == kObjectID)
			{
				actor = ((OvrActorNode*)it->other_node)->GetActor();
			}
		}
	}

	OvrRotationNode::OvrRotationNode()
	{
		in_param_list.push_back(ovr_core::OvrNodeParam(kObjectName, kObjectID,ovr_core::OvrNodeParam::kParamActor, ""));
		in_param_list.push_back(ovr_core::OvrNodeParam(kEndAngleName, kEndAngleID,ovr_core::OvrNodeParam::kParamVector3, "0.0:0.0:0.0"));
	}

	//执行体 需要由子类实现
	bool OvrRotationNode::Doing(float a_delta_time) 
	{
		if (nullptr != actor)
		{
			actor->SetEulerAngle(rotation[0], rotation[1], rotation[2]);
		}
		return true;
	}

	ovr::uint32	OvrRotationNode::SetValue(bool a_is_in, const OvrString& a_name, const OvrVector3& a_value) 
	{
		if (a_name == kEndAngleID)
		{
			rotation = a_value;
			return 1;
		}
		return 0;
	}

	ovr::uint32	OvrRotationNode::SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value) 
	{
		if (a_name == kObjectID)
		{
			actor = a_value;
			return 1;
		}
		return 0;
	}
}

