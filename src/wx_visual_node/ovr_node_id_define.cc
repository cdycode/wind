﻿#include "headers.h"
#include "ovr_node_id_define.h"

namespace ovr_node 
{
	const  char* OvrNodeClassID::event_begin_category = "事件/开始";
	const  char* OvrNodeClassID::event_update_category = "事件/更新";
	const  char* OvrNodeClassID::event_end_category = "事件/结束";
	
	const char*	OvrNodeClassID::operator_focus_category = "焦点/聚焦";
	const char*	OvrNodeClassID::operator_multi_focus_category = "焦点/多对象聚焦";
	const char*	OvrNodeClassID::operator_get_focus_actor_category = "焦点/获得焦点对象";
	const char*	OvrNodeClassID::operator_enable_actor_focus_category = "焦点/启用聚焦";
	const char*	OvrNodeClassID::operator_goto_category = "操作/跳转";
	const char*	OvrNodeClassID::operator_change_texture_category = "操作/修改纹理";
	const char*	OvrNodeClassID::operator_change_diffuse_texture_category = "操作/修改漫反射纹理";
	const char*	OvrNodeClassID::operator_play_audio_category = "操作/播放声音";
	const char*	OvrNodeClassID::operator_timer_category = "操作/定时";
	const char*	OvrNodeClassID::operator_alternate_category = "操作/交替";
	const char*	OvrNodeClassID::operator_follow_camera_category = "操作/跟随相机";
	const char*	OvrNodeClassID::operator_camera_front_category = "操作/相机前面";
	const char*	OvrNodeClassID::operator_visiable_category = "操作/可见";
	const char*	OvrNodeClassID::operator_sprite_picture_category = "操作/精灵纹理";
	const char*	OvrNodeClassID::operator_cursor_temp_category = "操作/游标";
	const char*	OvrNodeClassID::operator_cursor_visual_category = "操作/游标可见";
	const char*	OvrNodeClassID::operator_set_main_camera_category = "操作/设置主相机";
	const char*	OvrNodeClassID::operator_set_ambient_color_category = "操作/设置环境光";
	const char*	OvrNodeClassID::operator_branch_category = "操作/分支";
	const char*	OvrNodeClassID::operator_switch_actor_category = "操作/对象比较";
	const char*	OvrNodeClassID::operator_set_text_category = "操作/设置文本";

	const char*	OvrNodeClassID::operator_movement_category = "变换/移动";
	const char*	OvrNodeClassID::operator_rotation_category = "变换/旋转";
	const char*	OvrNodeClassID::operator_move_position_category = "变换/位置";
	const char*	OvrNodeClassID::operator_movement_id = "operator_movement";
	const char*	OvrNodeClassID::operator_rotation_id = "operator_rotation";
	const char*	OvrNodeClassID::operator_move_position_id = "move_pisition";

	const char*	OvrNodeClassID::variable_actor_category = "变量/对象";

	const  char* OvrNodeClassID::event_begin_id = "event_begin";
	const  char* OvrNodeClassID::event_update_id = "event_update";
	const  char* OvrNodeClassID::event_end_id = "event_dnd";

	
	const char* OvrNodeClassID::operator_focus_id = "operator_foucus";
	const char* OvrNodeClassID::operator_multi_focus_id = "operator_multi_foucus";
	const char* OvrNodeClassID::operator_get_focus_actor_id = "get_focus_actor";
	const char* OvrNodeClassID::operator_enable_actor_focus_id = "enable_focus_actor";
	const char* OvrNodeClassID::operator_goto_id = "operator_goto";
	const char* OvrNodeClassID::operator_change_texture_id = "operator_change_texture";
	const char* OvrNodeClassID::operator_change_diffuse_texture_id = "operator_change_diffuse_texture";
	const char*	OvrNodeClassID::operator_play_audio_id = "operator_play_audio";
	const char* OvrNodeClassID::operator_timer_id = "operator_timer";
	const char*	OvrNodeClassID::operator_alternate_id = "operator_alternate";
	const char*	OvrNodeClassID::operator_follow_camera_id = "operator_follow_camera";
	const char*	OvrNodeClassID::operator_camera_front_id = "operator_camera_front";
	const char*	OvrNodeClassID::operator_visiable_id = "operator_visiable";
	const char*	OvrNodeClassID::operator_sprite_picture_id = "operator_sprite_picture";
	const char*	OvrNodeClassID::operator_set_main_camera_id = "set_main_camera";
	const char*	OvrNodeClassID::operator_set_ambient_color_id = "set_ambient_color";
	const char*	OvrNodeClassID::operator_branch_node_id = "branch";
	const char*	OvrNodeClassID::operator_switch_actor_node_id = "switch_actor";
	const char*	OvrNodeClassID::operator_set_text_node_id = "set_text";

	const char*	OvrNodeClassID::variable_actor_id = "variable_actor";

	const char* OvrNodeClassID::operator_effect_feather_category = "特效/羽毛";
	const char*	OvrNodeClassID::operator_effect_feather_id = "operator_effect_feather";

	const char*	OvrNodeClassID::operator_set_status_category = "操作/设置状态";
	const char* OvrNodeClassID::operator_set_status_id = "operator_set_status";
	const char*	OvrNodeClassID::operator_switch_status_category = "操作/判断状态";
	const char* OvrNodeClassID::operator_switch_status_id = "operator_switch_status";

	const char* OvrNodeClassID::operator_set_actor_diffusecolor_category = "操作/设置DiffuseColor";
	const char* OvrNodeClassID::operator_set_actor_diffusecolor_id = "set_actor_diffuse_color";

	const char* OvrNodeClassID::material_type_option = "选择材质:材质(*.mat)";
	const char*	OvrNodeClassID::texture_type_option = "选择纹理:纹理(*.tex)";
	const char*	OvrNodeClassID::audio_type_option = "选择声音:声音(*.mp3 *.wav)";
}
