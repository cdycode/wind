﻿#ifndef OVR_VISUAL_NODE_OVR_VARIABLE_NODE_H_
#define OVR_VISUAL_NODE_OVR_VARIABLE_NODE_H_

#include "ovr_visual_node.h"

namespace ovr_engine
{
	class OvrActor;
}

namespace ovr_node 
{
	//Actor node
	class OvrActorNode : public OvrVisualNode
	{
	public:
		OvrActorNode();
		virtual ~OvrActorNode() {}

		ovr_engine::OvrActor*	GetActor() { return own_actor; }

	private:
		virtual ovr::uint32	SetValue(bool a_is_in, const OvrString& a_name, ovr_engine::OvrActor* a_value)override;
	private:
		ovr_engine::OvrActor*	own_actor = nullptr;	//所属的Actor
	};
}

#endif
