﻿#include "headers.h"
#include "ovr_feather_node.h"
#include <wx_engine/ovr_particle_system_manager.h>
#include <wx_engine/ovr_particle_system.h>
#include <wx_engine/ovr_texture_manager.h>
#include "ovr_variable_node.h"
#include "ovr_node_id_define.h"

namespace ovr_node 
{
	OvrFeatherNode::OvrFeatherNode() 
	{
		display_name = kFeatherNodeName;

		//接口
		interface_list.push_back(ovr_core::OvrNodeInterface(true, kPlayName, kPlayID));
		interface_list.push_back(ovr_core::OvrNodeInterface(true, kStopName, kStopID));
		interface_list.push_back(ovr_core::OvrNodeInterface(false, "", kOutputID));

		//参数
		in_param_list.push_back(ovr_core::OvrNodeParam(kParticleObjectName, kParticleObjectID, ovr_core::OvrNodeParam::kParamActor, ""));
		in_param_list.push_back(ovr_core::OvrNodeParam(kTextureName, kTextureID,ovr_core::OvrNodeParam::kParamResource, OvrNodeClassID::texture_type_option, ""));
		in_param_list.push_back(ovr_core::OvrNodeParam(kParticleMaxNumName, kParticleMaxNumID,ovr_core::OvrNodeParam::kParamInt, "20"));
	}

	void OvrFeatherNode::BeginPlay() 
	{
		//填充变量输入
		/*for (auto it = in_connect_list.begin(); it != in_connect_list.end(); it++)
		{
			if (it->local_interface == kParticleObjectID)
			{
				OvrActorNode* actor_node = (OvrActorNode*)it->other_node;
				particle_actor = (ovr_engine::OvrParticleActor*)(actor_node->GetActor());
			}
		}

		for (auto it = in_param_list.begin(); it != in_param_list.end(); it++)
		{
			if (it->value.GetStringSize() == 0)
			{
				continue;
			}
			else if (it->unique_name == kTextureID)
			{
				texture = ovr_engine::OvrTextureManager::GetIns()->Load(it->value);
			}
		}

		out_node = nullptr;
		//填充输出接口
		for (auto it = out_connect_list.begin(); it != out_connect_list.end(); it++)
		{
			if (it->local_interface == kOutputID)
			{
				out_node = (OvrOperatorNode*)it->other_node;
				out_node_interface = it->other_interface;
			}
		}

		if (nullptr == texture)
		{
			particle_actor = nullptr;
		}

		if (nullptr == particle_actor)
		{
			return;
		}
		//初始化粒子特效
		ovr_engine::OvrParticleSystem* particle_system = ovr_engine::OvrParticleSystemManager::GetIns()->GetFeatherparticleSystem();
		particle_actor->SetParticleSystem(particle_system);
		//particle_actor->SetTexture(texture);
		particle_actor->SetTransparent(1);
		particle_system->SetMaxParticleNum(max_size);
		particle_system->GetEmitter()->SetPos(OvrVector3(0));
		*/
	}

	void OvrFeatherNode::EndPlay() 
	{
		/*if (nullptr != particle_actor)
		{
			particle_actor->SetParticlePause(true);
		}
		particle_actor = nullptr;*/
	}

	bool OvrFeatherNode::Update(float a_delta_time, const OvrString& a_interface_name) 
	{
		/*if (nullptr != particle_actor)
		{
			if (a_interface_name == kPlayID)
			{
				particle_actor->SetParticleRestart();
			}
			else if (a_interface_name == kStopID)
			{
				particle_actor->SetParticlePause(true);
			}
		}
		
		//执行下一个
		if (nullptr != out_node)
		{
			out_node->Update(a_delta_time, out_node_interface);
		}*/
		return true;
	}

	ovr::uint32	OvrFeatherNode::SetValue(bool a_is_in, const OvrString& a_name, int a_value) 
	{
		if (a_name == kParticleMaxNumID)
		{
			max_size = a_value;
			return 1;
		}
		return 0;
	}
}