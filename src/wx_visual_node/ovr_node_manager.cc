﻿#include "headers.h"

#include "ovr_node_register.h"
#include "ovr_node_cluster.h"
#include "ovr_visual_node.h"

#include "ovr_node_manager.h"
#include "archive/ovr_archive_cluster_json.h"

namespace ovr_node 
{
	OvrNodeManager* sg_node_manager = nullptr;
}


//导出函数
//OvrVisualNode的初始化和反初始化函数

OVR_VISUAL_NODE_API void Uninit()
{
	if (nullptr != ovr_node::sg_node_manager)
	{
		ovr_node::sg_node_manager->Uninit();
		delete ovr_node::sg_node_manager;
		ovr_node::sg_node_manager = nullptr;
	}
}

OVR_VISUAL_NODE_API void* Init(const OvrString& a_scene_unique_name) 
{
	if (nullptr != ovr_node::sg_node_manager)
	{
		return ovr_node::sg_node_manager;
	}
	ovr_node::sg_node_manager = new ovr_node::OvrNodeManager;
	if (nullptr == ovr_node::sg_node_manager)
	{
		return nullptr;
	}
	if (!ovr_node::sg_node_manager->Init(a_scene_unique_name))
	{
		Uninit();
	}
	return ovr_node::sg_node_manager;
}

namespace ovr_node 
{

	//初始化
	bool OvrNodeManager::Init(const OvrString& a_scene_unique_name) 
	{
		Uninit();

		node_register = new OvrNodeRegister;
		if (nullptr == node_register)
		{
			return false;
		}
		if (!node_register->Init())
		{
			return false;
		}

		if (a_scene_unique_name.GetStringSize() > 0)
		{
			area_dir = OvrString("project/scene_asset/") + a_scene_unique_name + "/node_cluster_area/";
			event_dir = OvrString("project/scene_asset/") + a_scene_unique_name + "/node_cluster_event/";
		}
		else 
		{
			//这种是错误方式  作为临时解决方案 兼容以前的错误方式
			area_dir = OvrString("project/scene_asset/") +"node_cluster_area/";
			event_dir = OvrString("project/scene_asset/") + "node_cluster_event/";
		}

		//获取目录下的所有文件名
		area_file_list.clear();
		event_file_list.clear();
		OvrFileTools::GetFilesInDir(ovr_core::OvrCore::GetIns()->GetWorkDir() + area_dir, area_file_list);
		OvrFileTools::GetFilesInDir(ovr_core::OvrCore::GetIns()->GetWorkDir() + event_dir, event_file_list);
		
		return true;
	}
	void OvrNodeManager::Uninit() 
	{
		if (nullptr != node_register)
		{
			node_register->Uninit();
			delete node_register;
			node_register = nullptr;
		}
	}

	//获取ActorNode的Class ID
	const OvrString& OvrNodeManager::GetActorNodeID()const 
	{
		return node_register->GetActorNodeID();
	}

	//获取所有的node
	const std::list<ovr_core::OvrNodeInfo>&	OvrNodeManager::GetNodeIDList(ovr_core::IOvrNodeCluster::Type a_type)const
	{
		if (ovr_core::IOvrNodeCluster::kEvent == a_type)
		{
			return node_register->GetEventNodeIDList();
		}
		return node_register->GetAreaNodeIDList();
	}

	const std::list<OvrString>&	OvrNodeManager::GetClusterFileList(ovr_core::IOvrNodeCluster::Type a_type)const
	{
		if (ovr_core::IOvrNodeCluster::kEvent == a_type)
		{
			return event_file_list;
		}
		return area_file_list;
	}

	//创建事件
	bool OvrNodeManager::CreateCluster(ovr_core::IOvrNodeCluster::Type a_type, const OvrString& a_file_name)
	{
		if (ovr_core::IOvrNodeCluster::kEvent == a_type)
		{
			return CreateEvent(a_file_name);
		}
		return CreateArea(a_file_name);
	}
	//加载和关闭事件
	ovr_core::IOvrNodeCluster* OvrNodeManager::LoadCluster(ovr_core::IOvrNodeCluster::Type a_type, const OvrString& a_file_name, ovr_core::OvrSequence* a_own_sequence)
	{
		if (ovr_core::IOvrNodeCluster::kEvent == a_type)
		{
			return LoadEvent(a_file_name, a_own_sequence);
		}
		return LoadArea(a_file_name, a_own_sequence);
	}
	void OvrNodeManager::CloseCluster(ovr_core::IOvrNodeCluster* a_node_cluster)
	{
		if (nullptr == a_node_cluster)
		{
			return;
		}

		if (ovr_core::IOvrNodeCluster::kEvent == a_node_cluster->GetType())
		{
			CloseEvent(a_node_cluster);
		}
		else 
		{
			CloseArea(a_node_cluster);
		}
	}
	void OvrNodeManager::SaveCluster(ovr_core::IOvrNodeCluster* a_node_cluster)
	{
		OvrArchiveClusterJson archive_cluster;
		archive_cluster.Save((OvrNodeCluster*)a_node_cluster);
	}

	//创建交互区
	bool	OvrNodeManager::CreateArea(const OvrString& a_file_name) 
	{
		OvrString file_path = area_dir + a_file_name;
		OvrArchiveClusterJson cluster_json;
		OvrNodeClusterArea cluster_area(nullptr);
		cluster_area.SetFilePath(file_path);
		if (cluster_json.Save(&cluster_area)) 
		{
			area_file_list.push_back(a_file_name);
			return true;
		}
		return false;
	}

	//加载和关闭交互区
	ovr_core::IOvrNodeCluster*	OvrNodeManager::LoadArea(const OvrString& a_file_name, ovr_core::OvrSequence* a_own_sequence)
	{
		//查看已有的
		OvrString file_path = area_dir + a_file_name;
		auto it = area_map.find(file_path);
		if (it != area_map.end())
		{
			return it->second;
		}

		//从磁盘中加载
		OvrNodeClusterArea* new_area = new OvrNodeClusterArea(a_own_sequence);
		new_area->SetFilePath(file_path);
		OvrArchiveClusterJson cluster_json;
		if (cluster_json.Load(new_area))
		{
			area_map[file_path] = new_area;
		}
		else 
		{
			delete new_area;
			new_area = nullptr;
		}
		return new_area;
	}

	void OvrNodeManager::CloseArea(ovr_core::IOvrNodeCluster* a_node_cluster)
	{
		if (nullptr == a_node_cluster)
		{
			return;
		}

		auto it = area_map.find(a_node_cluster->GetFilePath());
		if (it != area_map.end())
		{
			//外部已经不在使用
			area_map.erase(it);
		}
	}

	//创建事件
	bool OvrNodeManager::CreateEvent(const OvrString& a_file_name)
	{
		OvrString file_path = event_dir + a_file_name;
		OvrArchiveClusterJson cluster_json;
		OvrNodeClusterEvent cluster_area(nullptr);
		cluster_area.SetFilePath(file_path);
		if (cluster_json.Save(&cluster_area)) 
		{
			event_file_list.push_back(a_file_name);
			return true;
		}
		return false;
	}
	//加载和关闭事件
	ovr_core::IOvrNodeCluster* OvrNodeManager::LoadEvent(const OvrString& a_file_name, ovr_core::OvrSequence* a_own_sequence)
	{
		OvrString file_path = event_dir + a_file_name;
		auto it = event_map.find(file_path);
		if (it != event_map.end())
		{
			return it->second;
		}

		//从磁盘中加载
		OvrNodeClusterEvent* cluster_event = new OvrNodeClusterEvent(a_own_sequence);
		cluster_event->SetFilePath(file_path);
		OvrArchiveClusterJson cluster_json;
		if (cluster_json.Load(cluster_event))
		{
			event_map[file_path] = cluster_event;
		}
		else
		{
			delete cluster_event;
			cluster_event = nullptr;
		}
		return cluster_event;
	}

	void OvrNodeManager::CloseEvent(ovr_core::IOvrNodeCluster* a_node_cluster)
	{
		if (nullptr == a_node_cluster)
		{
			return;
		}

		auto it = event_map.find(a_node_cluster->GetFilePath());
		if (it != event_map.end())
		{
			//外部已经不在使用
			event_map.erase(it);
		}
	}
}
