// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: sequence.proto

#ifndef PROTOBUF_sequence_2eproto__INCLUDED
#define PROTOBUF_sequence_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3005000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3005001 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_table_driven.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/metadata_lite.h>
#include <google/protobuf/message_lite.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include "common.pb.h"
#include "track.pb.h"
// @@protoc_insertion_point(includes)

namespace protobuf_sequence_2eproto {
// Internal implementation detail -- do not use these members.
struct TableStruct {
  static const ::google::protobuf::internal::ParseTableField entries[];
  static const ::google::protobuf::internal::AuxillaryParseTableField aux[];
  static const ::google::protobuf::internal::ParseTable schema[1];
  static const ::google::protobuf::internal::FieldMetadata field_metadata[];
  static const ::google::protobuf::internal::SerializationTable serialization_table[];
  static const ::google::protobuf::uint32 offsets[];
};
void InitDefaultsSequenceImpl();
void InitDefaultsSequence();
inline void InitDefaults() {
  InitDefaultsSequence();
}
}  // namespace protobuf_sequence_2eproto
namespace pb {
class Sequence;
class SequenceDefaultTypeInternal;
extern SequenceDefaultTypeInternal _Sequence_default_instance_;
}  // namespace pb
namespace pb {

// ===================================================================

class Sequence : public ::google::protobuf::MessageLite /* @@protoc_insertion_point(class_definition:pb.Sequence) */ {
 public:
  Sequence();
  virtual ~Sequence();

  Sequence(const Sequence& from);

  inline Sequence& operator=(const Sequence& from) {
    CopyFrom(from);
    return *this;
  }
  #if LANG_CXX11
  Sequence(Sequence&& from) noexcept
    : Sequence() {
    *this = ::std::move(from);
  }

  inline Sequence& operator=(Sequence&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }
  #endif
  static const Sequence& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const Sequence* internal_default_instance() {
    return reinterpret_cast<const Sequence*>(
               &_Sequence_default_instance_);
  }
  static PROTOBUF_CONSTEXPR int const kIndexInFileMessages =
    0;

  void Swap(Sequence* other);
  friend void swap(Sequence& a, Sequence& b) {
    a.Swap(&b);
  }

  // implements Message ----------------------------------------------

  inline Sequence* New() const PROTOBUF_FINAL { return New(NULL); }

  Sequence* New(::google::protobuf::Arena* arena) const PROTOBUF_FINAL;
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from)
    PROTOBUF_FINAL;
  void CopyFrom(const Sequence& from);
  void MergeFrom(const Sequence& from);
  void Clear() PROTOBUF_FINAL;
  bool IsInitialized() const PROTOBUF_FINAL;

  size_t ByteSizeLong() const PROTOBUF_FINAL;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input) PROTOBUF_FINAL;
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const PROTOBUF_FINAL;
  void DiscardUnknownFields();
  int GetCachedSize() const PROTOBUF_FINAL { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(Sequence* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return NULL;
  }
  inline void* MaybeArenaPtr() const {
    return NULL;
  }
  public:

  ::std::string GetTypeName() const PROTOBUF_FINAL;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // repeated string asset_file_path_list = 7;
  int asset_file_path_list_size() const;
  void clear_asset_file_path_list();
  static const int kAssetFilePathListFieldNumber = 7;
  const ::std::string& asset_file_path_list(int index) const;
  ::std::string* mutable_asset_file_path_list(int index);
  void set_asset_file_path_list(int index, const ::std::string& value);
  #if LANG_CXX11
  void set_asset_file_path_list(int index, ::std::string&& value);
  #endif
  void set_asset_file_path_list(int index, const char* value);
  void set_asset_file_path_list(int index, const char* value, size_t size);
  ::std::string* add_asset_file_path_list();
  void add_asset_file_path_list(const ::std::string& value);
  #if LANG_CXX11
  void add_asset_file_path_list(::std::string&& value);
  #endif
  void add_asset_file_path_list(const char* value);
  void add_asset_file_path_list(const char* value, size_t size);
  const ::google::protobuf::RepeatedPtrField< ::std::string>& asset_file_path_list() const;
  ::google::protobuf::RepeatedPtrField< ::std::string>* mutable_asset_file_path_list();

  // repeated .pb.TrackInfo track_info = 8;
  int track_info_size() const;
  void clear_track_info();
  static const int kTrackInfoFieldNumber = 8;
  const ::pb::TrackInfo& track_info(int index) const;
  ::pb::TrackInfo* mutable_track_info(int index);
  ::pb::TrackInfo* add_track_info();
  ::google::protobuf::RepeatedPtrField< ::pb::TrackInfo >*
      mutable_track_info();
  const ::google::protobuf::RepeatedPtrField< ::pb::TrackInfo >&
      track_info() const;

  // string unique_name = 2;
  void clear_unique_name();
  static const int kUniqueNameFieldNumber = 2;
  const ::std::string& unique_name() const;
  void set_unique_name(const ::std::string& value);
  #if LANG_CXX11
  void set_unique_name(::std::string&& value);
  #endif
  void set_unique_name(const char* value);
  void set_unique_name(const char* value, size_t size);
  ::std::string* mutable_unique_name();
  ::std::string* release_unique_name();
  void set_allocated_unique_name(::std::string* unique_name);

  // string scene_unique_name = 3;
  void clear_scene_unique_name();
  static const int kSceneUniqueNameFieldNumber = 3;
  const ::std::string& scene_unique_name() const;
  void set_scene_unique_name(const ::std::string& value);
  #if LANG_CXX11
  void set_scene_unique_name(::std::string&& value);
  #endif
  void set_scene_unique_name(const char* value);
  void set_scene_unique_name(const char* value, size_t size);
  ::std::string* mutable_scene_unique_name();
  ::std::string* release_scene_unique_name();
  void set_allocated_scene_unique_name(::std::string* scene_unique_name);

  // string description = 4;
  void clear_description();
  static const int kDescriptionFieldNumber = 4;
  const ::std::string& description() const;
  void set_description(const ::std::string& value);
  #if LANG_CXX11
  void set_description(::std::string&& value);
  #endif
  void set_description(const char* value);
  void set_description(const char* value, size_t size);
  ::std::string* mutable_description();
  ::std::string* release_description();
  void set_allocated_description(::std::string* description);

  // .pb.ProjectHeader header = 1;
  bool has_header() const;
  void clear_header();
  static const int kHeaderFieldNumber = 1;
  const ::pb::ProjectHeader& header() const;
  ::pb::ProjectHeader* release_header();
  ::pb::ProjectHeader* mutable_header();
  void set_allocated_header(::pb::ProjectHeader* header);

  // .pb.Tree root_track = 9;
  bool has_root_track() const;
  void clear_root_track();
  static const int kRootTrackFieldNumber = 9;
  const ::pb::Tree& root_track() const;
  ::pb::Tree* release_root_track();
  ::pb::Tree* mutable_root_track();
  void set_allocated_root_track(::pb::Tree* root_track);

  // uint64 duration = 6;
  void clear_duration();
  static const int kDurationFieldNumber = 6;
  ::google::protobuf::uint64 duration() const;
  void set_duration(::google::protobuf::uint64 value);

  // uint32 fps = 5;
  void clear_fps();
  static const int kFpsFieldNumber = 5;
  ::google::protobuf::uint32 fps() const;
  void set_fps(::google::protobuf::uint32 value);

  // @@protoc_insertion_point(class_scope:pb.Sequence)
 private:

  ::google::protobuf::internal::InternalMetadataWithArenaLite _internal_metadata_;
  ::google::protobuf::RepeatedPtrField< ::std::string> asset_file_path_list_;
  ::google::protobuf::RepeatedPtrField< ::pb::TrackInfo > track_info_;
  ::google::protobuf::internal::ArenaStringPtr unique_name_;
  ::google::protobuf::internal::ArenaStringPtr scene_unique_name_;
  ::google::protobuf::internal::ArenaStringPtr description_;
  ::pb::ProjectHeader* header_;
  ::pb::Tree* root_track_;
  ::google::protobuf::uint64 duration_;
  ::google::protobuf::uint32 fps_;
  mutable int _cached_size_;
  friend struct ::protobuf_sequence_2eproto::TableStruct;
  friend void ::protobuf_sequence_2eproto::InitDefaultsSequenceImpl();
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// Sequence

// .pb.ProjectHeader header = 1;
inline bool Sequence::has_header() const {
  return this != internal_default_instance() && header_ != NULL;
}
inline const ::pb::ProjectHeader& Sequence::header() const {
  const ::pb::ProjectHeader* p = header_;
  // @@protoc_insertion_point(field_get:pb.Sequence.header)
  return p != NULL ? *p : *reinterpret_cast<const ::pb::ProjectHeader*>(
      &::pb::_ProjectHeader_default_instance_);
}
inline ::pb::ProjectHeader* Sequence::release_header() {
  // @@protoc_insertion_point(field_release:pb.Sequence.header)
  
  ::pb::ProjectHeader* temp = header_;
  header_ = NULL;
  return temp;
}
inline ::pb::ProjectHeader* Sequence::mutable_header() {
  
  if (header_ == NULL) {
    header_ = new ::pb::ProjectHeader;
  }
  // @@protoc_insertion_point(field_mutable:pb.Sequence.header)
  return header_;
}
inline void Sequence::set_allocated_header(::pb::ProjectHeader* header) {
  ::google::protobuf::Arena* message_arena = GetArenaNoVirtual();
  if (message_arena == NULL) {
    delete reinterpret_cast< ::google::protobuf::MessageLite*>(header_);
  }
  if (header) {
    ::google::protobuf::Arena* submessage_arena = NULL;
    if (message_arena != submessage_arena) {
      header = ::google::protobuf::internal::GetOwnedMessage(
          message_arena, header, submessage_arena);
    }
    
  } else {
    
  }
  header_ = header;
  // @@protoc_insertion_point(field_set_allocated:pb.Sequence.header)
}

// string unique_name = 2;
inline void Sequence::clear_unique_name() {
  unique_name_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline const ::std::string& Sequence::unique_name() const {
  // @@protoc_insertion_point(field_get:pb.Sequence.unique_name)
  return unique_name_.GetNoArena();
}
inline void Sequence::set_unique_name(const ::std::string& value) {
  
  unique_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:pb.Sequence.unique_name)
}
#if LANG_CXX11
inline void Sequence::set_unique_name(::std::string&& value) {
  
  unique_name_.SetNoArena(
    &::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:pb.Sequence.unique_name)
}
#endif
inline void Sequence::set_unique_name(const char* value) {
  GOOGLE_DCHECK(value != NULL);
  
  unique_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:pb.Sequence.unique_name)
}
inline void Sequence::set_unique_name(const char* value, size_t size) {
  
  unique_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:pb.Sequence.unique_name)
}
inline ::std::string* Sequence::mutable_unique_name() {
  
  // @@protoc_insertion_point(field_mutable:pb.Sequence.unique_name)
  return unique_name_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* Sequence::release_unique_name() {
  // @@protoc_insertion_point(field_release:pb.Sequence.unique_name)
  
  return unique_name_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void Sequence::set_allocated_unique_name(::std::string* unique_name) {
  if (unique_name != NULL) {
    
  } else {
    
  }
  unique_name_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), unique_name);
  // @@protoc_insertion_point(field_set_allocated:pb.Sequence.unique_name)
}

// string scene_unique_name = 3;
inline void Sequence::clear_scene_unique_name() {
  scene_unique_name_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline const ::std::string& Sequence::scene_unique_name() const {
  // @@protoc_insertion_point(field_get:pb.Sequence.scene_unique_name)
  return scene_unique_name_.GetNoArena();
}
inline void Sequence::set_scene_unique_name(const ::std::string& value) {
  
  scene_unique_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:pb.Sequence.scene_unique_name)
}
#if LANG_CXX11
inline void Sequence::set_scene_unique_name(::std::string&& value) {
  
  scene_unique_name_.SetNoArena(
    &::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:pb.Sequence.scene_unique_name)
}
#endif
inline void Sequence::set_scene_unique_name(const char* value) {
  GOOGLE_DCHECK(value != NULL);
  
  scene_unique_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:pb.Sequence.scene_unique_name)
}
inline void Sequence::set_scene_unique_name(const char* value, size_t size) {
  
  scene_unique_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:pb.Sequence.scene_unique_name)
}
inline ::std::string* Sequence::mutable_scene_unique_name() {
  
  // @@protoc_insertion_point(field_mutable:pb.Sequence.scene_unique_name)
  return scene_unique_name_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* Sequence::release_scene_unique_name() {
  // @@protoc_insertion_point(field_release:pb.Sequence.scene_unique_name)
  
  return scene_unique_name_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void Sequence::set_allocated_scene_unique_name(::std::string* scene_unique_name) {
  if (scene_unique_name != NULL) {
    
  } else {
    
  }
  scene_unique_name_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), scene_unique_name);
  // @@protoc_insertion_point(field_set_allocated:pb.Sequence.scene_unique_name)
}

// string description = 4;
inline void Sequence::clear_description() {
  description_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline const ::std::string& Sequence::description() const {
  // @@protoc_insertion_point(field_get:pb.Sequence.description)
  return description_.GetNoArena();
}
inline void Sequence::set_description(const ::std::string& value) {
  
  description_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:pb.Sequence.description)
}
#if LANG_CXX11
inline void Sequence::set_description(::std::string&& value) {
  
  description_.SetNoArena(
    &::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:pb.Sequence.description)
}
#endif
inline void Sequence::set_description(const char* value) {
  GOOGLE_DCHECK(value != NULL);
  
  description_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:pb.Sequence.description)
}
inline void Sequence::set_description(const char* value, size_t size) {
  
  description_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:pb.Sequence.description)
}
inline ::std::string* Sequence::mutable_description() {
  
  // @@protoc_insertion_point(field_mutable:pb.Sequence.description)
  return description_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* Sequence::release_description() {
  // @@protoc_insertion_point(field_release:pb.Sequence.description)
  
  return description_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void Sequence::set_allocated_description(::std::string* description) {
  if (description != NULL) {
    
  } else {
    
  }
  description_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), description);
  // @@protoc_insertion_point(field_set_allocated:pb.Sequence.description)
}

// uint32 fps = 5;
inline void Sequence::clear_fps() {
  fps_ = 0u;
}
inline ::google::protobuf::uint32 Sequence::fps() const {
  // @@protoc_insertion_point(field_get:pb.Sequence.fps)
  return fps_;
}
inline void Sequence::set_fps(::google::protobuf::uint32 value) {
  
  fps_ = value;
  // @@protoc_insertion_point(field_set:pb.Sequence.fps)
}

// uint64 duration = 6;
inline void Sequence::clear_duration() {
  duration_ = GOOGLE_ULONGLONG(0);
}
inline ::google::protobuf::uint64 Sequence::duration() const {
  // @@protoc_insertion_point(field_get:pb.Sequence.duration)
  return duration_;
}
inline void Sequence::set_duration(::google::protobuf::uint64 value) {
  
  duration_ = value;
  // @@protoc_insertion_point(field_set:pb.Sequence.duration)
}

// repeated string asset_file_path_list = 7;
inline int Sequence::asset_file_path_list_size() const {
  return asset_file_path_list_.size();
}
inline void Sequence::clear_asset_file_path_list() {
  asset_file_path_list_.Clear();
}
inline const ::std::string& Sequence::asset_file_path_list(int index) const {
  // @@protoc_insertion_point(field_get:pb.Sequence.asset_file_path_list)
  return asset_file_path_list_.Get(index);
}
inline ::std::string* Sequence::mutable_asset_file_path_list(int index) {
  // @@protoc_insertion_point(field_mutable:pb.Sequence.asset_file_path_list)
  return asset_file_path_list_.Mutable(index);
}
inline void Sequence::set_asset_file_path_list(int index, const ::std::string& value) {
  // @@protoc_insertion_point(field_set:pb.Sequence.asset_file_path_list)
  asset_file_path_list_.Mutable(index)->assign(value);
}
#if LANG_CXX11
inline void Sequence::set_asset_file_path_list(int index, ::std::string&& value) {
  // @@protoc_insertion_point(field_set:pb.Sequence.asset_file_path_list)
  asset_file_path_list_.Mutable(index)->assign(std::move(value));
}
#endif
inline void Sequence::set_asset_file_path_list(int index, const char* value) {
  GOOGLE_DCHECK(value != NULL);
  asset_file_path_list_.Mutable(index)->assign(value);
  // @@protoc_insertion_point(field_set_char:pb.Sequence.asset_file_path_list)
}
inline void Sequence::set_asset_file_path_list(int index, const char* value, size_t size) {
  asset_file_path_list_.Mutable(index)->assign(
    reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_set_pointer:pb.Sequence.asset_file_path_list)
}
inline ::std::string* Sequence::add_asset_file_path_list() {
  // @@protoc_insertion_point(field_add_mutable:pb.Sequence.asset_file_path_list)
  return asset_file_path_list_.Add();
}
inline void Sequence::add_asset_file_path_list(const ::std::string& value) {
  asset_file_path_list_.Add()->assign(value);
  // @@protoc_insertion_point(field_add:pb.Sequence.asset_file_path_list)
}
#if LANG_CXX11
inline void Sequence::add_asset_file_path_list(::std::string&& value) {
  asset_file_path_list_.Add(std::move(value));
  // @@protoc_insertion_point(field_add:pb.Sequence.asset_file_path_list)
}
#endif
inline void Sequence::add_asset_file_path_list(const char* value) {
  GOOGLE_DCHECK(value != NULL);
  asset_file_path_list_.Add()->assign(value);
  // @@protoc_insertion_point(field_add_char:pb.Sequence.asset_file_path_list)
}
inline void Sequence::add_asset_file_path_list(const char* value, size_t size) {
  asset_file_path_list_.Add()->assign(reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_add_pointer:pb.Sequence.asset_file_path_list)
}
inline const ::google::protobuf::RepeatedPtrField< ::std::string>&
Sequence::asset_file_path_list() const {
  // @@protoc_insertion_point(field_list:pb.Sequence.asset_file_path_list)
  return asset_file_path_list_;
}
inline ::google::protobuf::RepeatedPtrField< ::std::string>*
Sequence::mutable_asset_file_path_list() {
  // @@protoc_insertion_point(field_mutable_list:pb.Sequence.asset_file_path_list)
  return &asset_file_path_list_;
}

// repeated .pb.TrackInfo track_info = 8;
inline int Sequence::track_info_size() const {
  return track_info_.size();
}
inline const ::pb::TrackInfo& Sequence::track_info(int index) const {
  // @@protoc_insertion_point(field_get:pb.Sequence.track_info)
  return track_info_.Get(index);
}
inline ::pb::TrackInfo* Sequence::mutable_track_info(int index) {
  // @@protoc_insertion_point(field_mutable:pb.Sequence.track_info)
  return track_info_.Mutable(index);
}
inline ::pb::TrackInfo* Sequence::add_track_info() {
  // @@protoc_insertion_point(field_add:pb.Sequence.track_info)
  return track_info_.Add();
}
inline ::google::protobuf::RepeatedPtrField< ::pb::TrackInfo >*
Sequence::mutable_track_info() {
  // @@protoc_insertion_point(field_mutable_list:pb.Sequence.track_info)
  return &track_info_;
}
inline const ::google::protobuf::RepeatedPtrField< ::pb::TrackInfo >&
Sequence::track_info() const {
  // @@protoc_insertion_point(field_list:pb.Sequence.track_info)
  return track_info_;
}

// .pb.Tree root_track = 9;
inline bool Sequence::has_root_track() const {
  return this != internal_default_instance() && root_track_ != NULL;
}
inline const ::pb::Tree& Sequence::root_track() const {
  const ::pb::Tree* p = root_track_;
  // @@protoc_insertion_point(field_get:pb.Sequence.root_track)
  return p != NULL ? *p : *reinterpret_cast<const ::pb::Tree*>(
      &::pb::_Tree_default_instance_);
}
inline ::pb::Tree* Sequence::release_root_track() {
  // @@protoc_insertion_point(field_release:pb.Sequence.root_track)
  
  ::pb::Tree* temp = root_track_;
  root_track_ = NULL;
  return temp;
}
inline ::pb::Tree* Sequence::mutable_root_track() {
  
  if (root_track_ == NULL) {
    root_track_ = new ::pb::Tree;
  }
  // @@protoc_insertion_point(field_mutable:pb.Sequence.root_track)
  return root_track_;
}
inline void Sequence::set_allocated_root_track(::pb::Tree* root_track) {
  ::google::protobuf::Arena* message_arena = GetArenaNoVirtual();
  if (message_arena == NULL) {
    delete reinterpret_cast< ::google::protobuf::MessageLite*>(root_track_);
  }
  if (root_track) {
    ::google::protobuf::Arena* submessage_arena = NULL;
    if (message_arena != submessage_arena) {
      root_track = ::google::protobuf::internal::GetOwnedMessage(
          message_arena, root_track, submessage_arena);
    }
    
  } else {
    
  }
  root_track_ = root_track;
  // @@protoc_insertion_point(field_set_allocated:pb.Sequence.root_track)
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__

// @@protoc_insertion_point(namespace_scope)

}  // namespace pb

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_sequence_2eproto__INCLUDED
