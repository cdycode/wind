// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: scene.proto

#ifndef PROTOBUF_scene_2eproto__INCLUDED
#define PROTOBUF_scene_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3005000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3005001 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_table_driven.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/metadata_lite.h>
#include <google/protobuf/message_lite.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include "common.pb.h"
#include "actor.pb.h"
// @@protoc_insertion_point(includes)

namespace protobuf_scene_2eproto {
// Internal implementation detail -- do not use these members.
struct TableStruct {
  static const ::google::protobuf::internal::ParseTableField entries[];
  static const ::google::protobuf::internal::AuxillaryParseTableField aux[];
  static const ::google::protobuf::internal::ParseTable schema[1];
  static const ::google::protobuf::internal::FieldMetadata field_metadata[];
  static const ::google::protobuf::internal::SerializationTable serialization_table[];
  static const ::google::protobuf::uint32 offsets[];
};
void InitDefaultsSceneImpl();
void InitDefaultsScene();
inline void InitDefaults() {
  InitDefaultsScene();
}
}  // namespace protobuf_scene_2eproto
namespace pb {
class Scene;
class SceneDefaultTypeInternal;
extern SceneDefaultTypeInternal _Scene_default_instance_;
}  // namespace pb
namespace pb {

// ===================================================================

class Scene : public ::google::protobuf::MessageLite /* @@protoc_insertion_point(class_definition:pb.Scene) */ {
 public:
  Scene();
  virtual ~Scene();

  Scene(const Scene& from);

  inline Scene& operator=(const Scene& from) {
    CopyFrom(from);
    return *this;
  }
  #if LANG_CXX11
  Scene(Scene&& from) noexcept
    : Scene() {
    *this = ::std::move(from);
  }

  inline Scene& operator=(Scene&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }
  #endif
  static const Scene& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const Scene* internal_default_instance() {
    return reinterpret_cast<const Scene*>(
               &_Scene_default_instance_);
  }
  static PROTOBUF_CONSTEXPR int const kIndexInFileMessages =
    0;

  void Swap(Scene* other);
  friend void swap(Scene& a, Scene& b) {
    a.Swap(&b);
  }

  // implements Message ----------------------------------------------

  inline Scene* New() const PROTOBUF_FINAL { return New(NULL); }

  Scene* New(::google::protobuf::Arena* arena) const PROTOBUF_FINAL;
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from)
    PROTOBUF_FINAL;
  void CopyFrom(const Scene& from);
  void MergeFrom(const Scene& from);
  void Clear() PROTOBUF_FINAL;
  bool IsInitialized() const PROTOBUF_FINAL;

  size_t ByteSizeLong() const PROTOBUF_FINAL;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input) PROTOBUF_FINAL;
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const PROTOBUF_FINAL;
  void DiscardUnknownFields();
  int GetCachedSize() const PROTOBUF_FINAL { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(Scene* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return NULL;
  }
  inline void* MaybeArenaPtr() const {
    return NULL;
  }
  public:

  ::std::string GetTypeName() const PROTOBUF_FINAL;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // repeated string asset_file_path_list = 5;
  int asset_file_path_list_size() const;
  void clear_asset_file_path_list();
  static const int kAssetFilePathListFieldNumber = 5;
  const ::std::string& asset_file_path_list(int index) const;
  ::std::string* mutable_asset_file_path_list(int index);
  void set_asset_file_path_list(int index, const ::std::string& value);
  #if LANG_CXX11
  void set_asset_file_path_list(int index, ::std::string&& value);
  #endif
  void set_asset_file_path_list(int index, const char* value);
  void set_asset_file_path_list(int index, const char* value, size_t size);
  ::std::string* add_asset_file_path_list();
  void add_asset_file_path_list(const ::std::string& value);
  #if LANG_CXX11
  void add_asset_file_path_list(::std::string&& value);
  #endif
  void add_asset_file_path_list(const char* value);
  void add_asset_file_path_list(const char* value, size_t size);
  const ::google::protobuf::RepeatedPtrField< ::std::string>& asset_file_path_list() const;
  ::google::protobuf::RepeatedPtrField< ::std::string>* mutable_asset_file_path_list();

  // repeated string sequence_unique_name_list = 6;
  int sequence_unique_name_list_size() const;
  void clear_sequence_unique_name_list();
  static const int kSequenceUniqueNameListFieldNumber = 6;
  const ::std::string& sequence_unique_name_list(int index) const;
  ::std::string* mutable_sequence_unique_name_list(int index);
  void set_sequence_unique_name_list(int index, const ::std::string& value);
  #if LANG_CXX11
  void set_sequence_unique_name_list(int index, ::std::string&& value);
  #endif
  void set_sequence_unique_name_list(int index, const char* value);
  void set_sequence_unique_name_list(int index, const char* value, size_t size);
  ::std::string* add_sequence_unique_name_list();
  void add_sequence_unique_name_list(const ::std::string& value);
  #if LANG_CXX11
  void add_sequence_unique_name_list(::std::string&& value);
  #endif
  void add_sequence_unique_name_list(const char* value);
  void add_sequence_unique_name_list(const char* value, size_t size);
  const ::google::protobuf::RepeatedPtrField< ::std::string>& sequence_unique_name_list() const;
  ::google::protobuf::RepeatedPtrField< ::std::string>* mutable_sequence_unique_name_list();

  // repeated .pb.ActorInfo actor_info = 7;
  int actor_info_size() const;
  void clear_actor_info();
  static const int kActorInfoFieldNumber = 7;
  const ::pb::ActorInfo& actor_info(int index) const;
  ::pb::ActorInfo* mutable_actor_info(int index);
  ::pb::ActorInfo* add_actor_info();
  ::google::protobuf::RepeatedPtrField< ::pb::ActorInfo >*
      mutable_actor_info();
  const ::google::protobuf::RepeatedPtrField< ::pb::ActorInfo >&
      actor_info() const;

  // string unique_name = 2;
  void clear_unique_name();
  static const int kUniqueNameFieldNumber = 2;
  const ::std::string& unique_name() const;
  void set_unique_name(const ::std::string& value);
  #if LANG_CXX11
  void set_unique_name(::std::string&& value);
  #endif
  void set_unique_name(const char* value);
  void set_unique_name(const char* value, size_t size);
  ::std::string* mutable_unique_name();
  ::std::string* release_unique_name();
  void set_allocated_unique_name(::std::string* unique_name);

  // string description = 3;
  void clear_description();
  static const int kDescriptionFieldNumber = 3;
  const ::std::string& description() const;
  void set_description(const ::std::string& value);
  #if LANG_CXX11
  void set_description(::std::string&& value);
  #endif
  void set_description(const char* value);
  void set_description(const char* value, size_t size);
  ::std::string* mutable_description();
  ::std::string* release_description();
  void set_allocated_description(::std::string* description);

  // .pb.ProjectHeader header = 1;
  bool has_header() const;
  void clear_header();
  static const int kHeaderFieldNumber = 1;
  const ::pb::ProjectHeader& header() const;
  ::pb::ProjectHeader* release_header();
  ::pb::ProjectHeader* mutable_header();
  void set_allocated_header(::pb::ProjectHeader* header);

  // .pb.Vector3 ambient_light = 4;
  bool has_ambient_light() const;
  void clear_ambient_light();
  static const int kAmbientLightFieldNumber = 4;
  const ::pb::Vector3& ambient_light() const;
  ::pb::Vector3* release_ambient_light();
  ::pb::Vector3* mutable_ambient_light();
  void set_allocated_ambient_light(::pb::Vector3* ambient_light);

  // .pb.Tree root_actor = 8;
  bool has_root_actor() const;
  void clear_root_actor();
  static const int kRootActorFieldNumber = 8;
  const ::pb::Tree& root_actor() const;
  ::pb::Tree* release_root_actor();
  ::pb::Tree* mutable_root_actor();
  void set_allocated_root_actor(::pb::Tree* root_actor);

  // @@protoc_insertion_point(class_scope:pb.Scene)
 private:

  ::google::protobuf::internal::InternalMetadataWithArenaLite _internal_metadata_;
  ::google::protobuf::RepeatedPtrField< ::std::string> asset_file_path_list_;
  ::google::protobuf::RepeatedPtrField< ::std::string> sequence_unique_name_list_;
  ::google::protobuf::RepeatedPtrField< ::pb::ActorInfo > actor_info_;
  ::google::protobuf::internal::ArenaStringPtr unique_name_;
  ::google::protobuf::internal::ArenaStringPtr description_;
  ::pb::ProjectHeader* header_;
  ::pb::Vector3* ambient_light_;
  ::pb::Tree* root_actor_;
  mutable int _cached_size_;
  friend struct ::protobuf_scene_2eproto::TableStruct;
  friend void ::protobuf_scene_2eproto::InitDefaultsSceneImpl();
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// Scene

// .pb.ProjectHeader header = 1;
inline bool Scene::has_header() const {
  return this != internal_default_instance() && header_ != NULL;
}
inline const ::pb::ProjectHeader& Scene::header() const {
  const ::pb::ProjectHeader* p = header_;
  // @@protoc_insertion_point(field_get:pb.Scene.header)
  return p != NULL ? *p : *reinterpret_cast<const ::pb::ProjectHeader*>(
      &::pb::_ProjectHeader_default_instance_);
}
inline ::pb::ProjectHeader* Scene::release_header() {
  // @@protoc_insertion_point(field_release:pb.Scene.header)
  
  ::pb::ProjectHeader* temp = header_;
  header_ = NULL;
  return temp;
}
inline ::pb::ProjectHeader* Scene::mutable_header() {
  
  if (header_ == NULL) {
    header_ = new ::pb::ProjectHeader;
  }
  // @@protoc_insertion_point(field_mutable:pb.Scene.header)
  return header_;
}
inline void Scene::set_allocated_header(::pb::ProjectHeader* header) {
  ::google::protobuf::Arena* message_arena = GetArenaNoVirtual();
  if (message_arena == NULL) {
    delete reinterpret_cast< ::google::protobuf::MessageLite*>(header_);
  }
  if (header) {
    ::google::protobuf::Arena* submessage_arena = NULL;
    if (message_arena != submessage_arena) {
      header = ::google::protobuf::internal::GetOwnedMessage(
          message_arena, header, submessage_arena);
    }
    
  } else {
    
  }
  header_ = header;
  // @@protoc_insertion_point(field_set_allocated:pb.Scene.header)
}

// string unique_name = 2;
inline void Scene::clear_unique_name() {
  unique_name_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline const ::std::string& Scene::unique_name() const {
  // @@protoc_insertion_point(field_get:pb.Scene.unique_name)
  return unique_name_.GetNoArena();
}
inline void Scene::set_unique_name(const ::std::string& value) {
  
  unique_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:pb.Scene.unique_name)
}
#if LANG_CXX11
inline void Scene::set_unique_name(::std::string&& value) {
  
  unique_name_.SetNoArena(
    &::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:pb.Scene.unique_name)
}
#endif
inline void Scene::set_unique_name(const char* value) {
  GOOGLE_DCHECK(value != NULL);
  
  unique_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:pb.Scene.unique_name)
}
inline void Scene::set_unique_name(const char* value, size_t size) {
  
  unique_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:pb.Scene.unique_name)
}
inline ::std::string* Scene::mutable_unique_name() {
  
  // @@protoc_insertion_point(field_mutable:pb.Scene.unique_name)
  return unique_name_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* Scene::release_unique_name() {
  // @@protoc_insertion_point(field_release:pb.Scene.unique_name)
  
  return unique_name_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void Scene::set_allocated_unique_name(::std::string* unique_name) {
  if (unique_name != NULL) {
    
  } else {
    
  }
  unique_name_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), unique_name);
  // @@protoc_insertion_point(field_set_allocated:pb.Scene.unique_name)
}

// string description = 3;
inline void Scene::clear_description() {
  description_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline const ::std::string& Scene::description() const {
  // @@protoc_insertion_point(field_get:pb.Scene.description)
  return description_.GetNoArena();
}
inline void Scene::set_description(const ::std::string& value) {
  
  description_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:pb.Scene.description)
}
#if LANG_CXX11
inline void Scene::set_description(::std::string&& value) {
  
  description_.SetNoArena(
    &::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:pb.Scene.description)
}
#endif
inline void Scene::set_description(const char* value) {
  GOOGLE_DCHECK(value != NULL);
  
  description_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:pb.Scene.description)
}
inline void Scene::set_description(const char* value, size_t size) {
  
  description_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:pb.Scene.description)
}
inline ::std::string* Scene::mutable_description() {
  
  // @@protoc_insertion_point(field_mutable:pb.Scene.description)
  return description_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* Scene::release_description() {
  // @@protoc_insertion_point(field_release:pb.Scene.description)
  
  return description_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void Scene::set_allocated_description(::std::string* description) {
  if (description != NULL) {
    
  } else {
    
  }
  description_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), description);
  // @@protoc_insertion_point(field_set_allocated:pb.Scene.description)
}

// .pb.Vector3 ambient_light = 4;
inline bool Scene::has_ambient_light() const {
  return this != internal_default_instance() && ambient_light_ != NULL;
}
inline const ::pb::Vector3& Scene::ambient_light() const {
  const ::pb::Vector3* p = ambient_light_;
  // @@protoc_insertion_point(field_get:pb.Scene.ambient_light)
  return p != NULL ? *p : *reinterpret_cast<const ::pb::Vector3*>(
      &::pb::_Vector3_default_instance_);
}
inline ::pb::Vector3* Scene::release_ambient_light() {
  // @@protoc_insertion_point(field_release:pb.Scene.ambient_light)
  
  ::pb::Vector3* temp = ambient_light_;
  ambient_light_ = NULL;
  return temp;
}
inline ::pb::Vector3* Scene::mutable_ambient_light() {
  
  if (ambient_light_ == NULL) {
    ambient_light_ = new ::pb::Vector3;
  }
  // @@protoc_insertion_point(field_mutable:pb.Scene.ambient_light)
  return ambient_light_;
}
inline void Scene::set_allocated_ambient_light(::pb::Vector3* ambient_light) {
  ::google::protobuf::Arena* message_arena = GetArenaNoVirtual();
  if (message_arena == NULL) {
    delete reinterpret_cast< ::google::protobuf::MessageLite*>(ambient_light_);
  }
  if (ambient_light) {
    ::google::protobuf::Arena* submessage_arena = NULL;
    if (message_arena != submessage_arena) {
      ambient_light = ::google::protobuf::internal::GetOwnedMessage(
          message_arena, ambient_light, submessage_arena);
    }
    
  } else {
    
  }
  ambient_light_ = ambient_light;
  // @@protoc_insertion_point(field_set_allocated:pb.Scene.ambient_light)
}

// repeated string asset_file_path_list = 5;
inline int Scene::asset_file_path_list_size() const {
  return asset_file_path_list_.size();
}
inline void Scene::clear_asset_file_path_list() {
  asset_file_path_list_.Clear();
}
inline const ::std::string& Scene::asset_file_path_list(int index) const {
  // @@protoc_insertion_point(field_get:pb.Scene.asset_file_path_list)
  return asset_file_path_list_.Get(index);
}
inline ::std::string* Scene::mutable_asset_file_path_list(int index) {
  // @@protoc_insertion_point(field_mutable:pb.Scene.asset_file_path_list)
  return asset_file_path_list_.Mutable(index);
}
inline void Scene::set_asset_file_path_list(int index, const ::std::string& value) {
  // @@protoc_insertion_point(field_set:pb.Scene.asset_file_path_list)
  asset_file_path_list_.Mutable(index)->assign(value);
}
#if LANG_CXX11
inline void Scene::set_asset_file_path_list(int index, ::std::string&& value) {
  // @@protoc_insertion_point(field_set:pb.Scene.asset_file_path_list)
  asset_file_path_list_.Mutable(index)->assign(std::move(value));
}
#endif
inline void Scene::set_asset_file_path_list(int index, const char* value) {
  GOOGLE_DCHECK(value != NULL);
  asset_file_path_list_.Mutable(index)->assign(value);
  // @@protoc_insertion_point(field_set_char:pb.Scene.asset_file_path_list)
}
inline void Scene::set_asset_file_path_list(int index, const char* value, size_t size) {
  asset_file_path_list_.Mutable(index)->assign(
    reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_set_pointer:pb.Scene.asset_file_path_list)
}
inline ::std::string* Scene::add_asset_file_path_list() {
  // @@protoc_insertion_point(field_add_mutable:pb.Scene.asset_file_path_list)
  return asset_file_path_list_.Add();
}
inline void Scene::add_asset_file_path_list(const ::std::string& value) {
  asset_file_path_list_.Add()->assign(value);
  // @@protoc_insertion_point(field_add:pb.Scene.asset_file_path_list)
}
#if LANG_CXX11
inline void Scene::add_asset_file_path_list(::std::string&& value) {
  asset_file_path_list_.Add(std::move(value));
  // @@protoc_insertion_point(field_add:pb.Scene.asset_file_path_list)
}
#endif
inline void Scene::add_asset_file_path_list(const char* value) {
  GOOGLE_DCHECK(value != NULL);
  asset_file_path_list_.Add()->assign(value);
  // @@protoc_insertion_point(field_add_char:pb.Scene.asset_file_path_list)
}
inline void Scene::add_asset_file_path_list(const char* value, size_t size) {
  asset_file_path_list_.Add()->assign(reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_add_pointer:pb.Scene.asset_file_path_list)
}
inline const ::google::protobuf::RepeatedPtrField< ::std::string>&
Scene::asset_file_path_list() const {
  // @@protoc_insertion_point(field_list:pb.Scene.asset_file_path_list)
  return asset_file_path_list_;
}
inline ::google::protobuf::RepeatedPtrField< ::std::string>*
Scene::mutable_asset_file_path_list() {
  // @@protoc_insertion_point(field_mutable_list:pb.Scene.asset_file_path_list)
  return &asset_file_path_list_;
}

// repeated string sequence_unique_name_list = 6;
inline int Scene::sequence_unique_name_list_size() const {
  return sequence_unique_name_list_.size();
}
inline void Scene::clear_sequence_unique_name_list() {
  sequence_unique_name_list_.Clear();
}
inline const ::std::string& Scene::sequence_unique_name_list(int index) const {
  // @@protoc_insertion_point(field_get:pb.Scene.sequence_unique_name_list)
  return sequence_unique_name_list_.Get(index);
}
inline ::std::string* Scene::mutable_sequence_unique_name_list(int index) {
  // @@protoc_insertion_point(field_mutable:pb.Scene.sequence_unique_name_list)
  return sequence_unique_name_list_.Mutable(index);
}
inline void Scene::set_sequence_unique_name_list(int index, const ::std::string& value) {
  // @@protoc_insertion_point(field_set:pb.Scene.sequence_unique_name_list)
  sequence_unique_name_list_.Mutable(index)->assign(value);
}
#if LANG_CXX11
inline void Scene::set_sequence_unique_name_list(int index, ::std::string&& value) {
  // @@protoc_insertion_point(field_set:pb.Scene.sequence_unique_name_list)
  sequence_unique_name_list_.Mutable(index)->assign(std::move(value));
}
#endif
inline void Scene::set_sequence_unique_name_list(int index, const char* value) {
  GOOGLE_DCHECK(value != NULL);
  sequence_unique_name_list_.Mutable(index)->assign(value);
  // @@protoc_insertion_point(field_set_char:pb.Scene.sequence_unique_name_list)
}
inline void Scene::set_sequence_unique_name_list(int index, const char* value, size_t size) {
  sequence_unique_name_list_.Mutable(index)->assign(
    reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_set_pointer:pb.Scene.sequence_unique_name_list)
}
inline ::std::string* Scene::add_sequence_unique_name_list() {
  // @@protoc_insertion_point(field_add_mutable:pb.Scene.sequence_unique_name_list)
  return sequence_unique_name_list_.Add();
}
inline void Scene::add_sequence_unique_name_list(const ::std::string& value) {
  sequence_unique_name_list_.Add()->assign(value);
  // @@protoc_insertion_point(field_add:pb.Scene.sequence_unique_name_list)
}
#if LANG_CXX11
inline void Scene::add_sequence_unique_name_list(::std::string&& value) {
  sequence_unique_name_list_.Add(std::move(value));
  // @@protoc_insertion_point(field_add:pb.Scene.sequence_unique_name_list)
}
#endif
inline void Scene::add_sequence_unique_name_list(const char* value) {
  GOOGLE_DCHECK(value != NULL);
  sequence_unique_name_list_.Add()->assign(value);
  // @@protoc_insertion_point(field_add_char:pb.Scene.sequence_unique_name_list)
}
inline void Scene::add_sequence_unique_name_list(const char* value, size_t size) {
  sequence_unique_name_list_.Add()->assign(reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_add_pointer:pb.Scene.sequence_unique_name_list)
}
inline const ::google::protobuf::RepeatedPtrField< ::std::string>&
Scene::sequence_unique_name_list() const {
  // @@protoc_insertion_point(field_list:pb.Scene.sequence_unique_name_list)
  return sequence_unique_name_list_;
}
inline ::google::protobuf::RepeatedPtrField< ::std::string>*
Scene::mutable_sequence_unique_name_list() {
  // @@protoc_insertion_point(field_mutable_list:pb.Scene.sequence_unique_name_list)
  return &sequence_unique_name_list_;
}

// repeated .pb.ActorInfo actor_info = 7;
inline int Scene::actor_info_size() const {
  return actor_info_.size();
}
inline const ::pb::ActorInfo& Scene::actor_info(int index) const {
  // @@protoc_insertion_point(field_get:pb.Scene.actor_info)
  return actor_info_.Get(index);
}
inline ::pb::ActorInfo* Scene::mutable_actor_info(int index) {
  // @@protoc_insertion_point(field_mutable:pb.Scene.actor_info)
  return actor_info_.Mutable(index);
}
inline ::pb::ActorInfo* Scene::add_actor_info() {
  // @@protoc_insertion_point(field_add:pb.Scene.actor_info)
  return actor_info_.Add();
}
inline ::google::protobuf::RepeatedPtrField< ::pb::ActorInfo >*
Scene::mutable_actor_info() {
  // @@protoc_insertion_point(field_mutable_list:pb.Scene.actor_info)
  return &actor_info_;
}
inline const ::google::protobuf::RepeatedPtrField< ::pb::ActorInfo >&
Scene::actor_info() const {
  // @@protoc_insertion_point(field_list:pb.Scene.actor_info)
  return actor_info_;
}

// .pb.Tree root_actor = 8;
inline bool Scene::has_root_actor() const {
  return this != internal_default_instance() && root_actor_ != NULL;
}
inline const ::pb::Tree& Scene::root_actor() const {
  const ::pb::Tree* p = root_actor_;
  // @@protoc_insertion_point(field_get:pb.Scene.root_actor)
  return p != NULL ? *p : *reinterpret_cast<const ::pb::Tree*>(
      &::pb::_Tree_default_instance_);
}
inline ::pb::Tree* Scene::release_root_actor() {
  // @@protoc_insertion_point(field_release:pb.Scene.root_actor)
  
  ::pb::Tree* temp = root_actor_;
  root_actor_ = NULL;
  return temp;
}
inline ::pb::Tree* Scene::mutable_root_actor() {
  
  if (root_actor_ == NULL) {
    root_actor_ = new ::pb::Tree;
  }
  // @@protoc_insertion_point(field_mutable:pb.Scene.root_actor)
  return root_actor_;
}
inline void Scene::set_allocated_root_actor(::pb::Tree* root_actor) {
  ::google::protobuf::Arena* message_arena = GetArenaNoVirtual();
  if (message_arena == NULL) {
    delete reinterpret_cast< ::google::protobuf::MessageLite*>(root_actor_);
  }
  if (root_actor) {
    ::google::protobuf::Arena* submessage_arena = NULL;
    if (message_arena != submessage_arena) {
      root_actor = ::google::protobuf::internal::GetOwnedMessage(
          message_arena, root_actor, submessage_arena);
    }
    
  } else {
    
  }
  root_actor_ = root_actor;
  // @@protoc_insertion_point(field_set_allocated:pb.Scene.root_actor)
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__

// @@protoc_insertion_point(namespace_scope)

}  // namespace pb

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_scene_2eproto__INCLUDED
