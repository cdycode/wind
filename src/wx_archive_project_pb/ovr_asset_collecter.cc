﻿#include "headers.h"
#include "ovr_asset_collecter.h"
#include <wx_core/ovr_scene_context.h>
#include <wx_core/ovr_sequence.h>
#include <wx_core/track/ovr_track.h>
#include <wx_core/track/ovr_actor_track.h>
#include <wx_core/track/ovr_clip_track.h>
#include <wx_core/track/ovr_event_track.h>
//#include <ovr_engine/ovr_mesh_actor.h>
#include <wx_engine/ovr_mesh.h>
#include <wx_engine/ovr_material.h>
#include <wx_engine/ovr_skeleton_instance.h>
#include <wx_core/visual_node/i_ovr_node_manager.h>
#include <wx_engine/ovr_texture.h>

namespace ovr_archive_project 
{
	//收集场景中使用到的资源
	void OvrAssetCollecter::CollectSequenceAsset(ovr_core::OvrSequence* a_sequence)
	{
		asset_file_map.clear();

		const std::list<ovr_core::OvrTrack*>& track_list = a_sequence->GetChildren();
		for (auto track_it = track_list.begin(); track_it != track_list.end(); track_it++)
		{
			CollectTrack(*track_it);
		}
	}

	void OvrAssetCollecter::CollectTrack(ovr_core::OvrTrack* a_track)
	{
		ovr_core::OvrTrackType track_type = a_track->GetTrackType();
		switch (track_type)
		{
		case ovr_core::kTrackCommon:
		{
			auto children = a_track->GetChildren();
			for (auto it = children.begin(); it != children.end(); it++)
			{
				CollectTrack(*it);
			}
		}
		break;
		/*case ovr_core::kTrackInteractive:
		{
			const OvrString& area_dir = ovr_core::IOvrNodeManager::GetIns()->GetAreaDir();
			ovr_core::OvrClipTrack* clip_track = (ovr_core::OvrClipTrack*)a_track;
			const std::map<ovr::uint64, ovr_core::OvrClipInfo*>& clip_map = clip_track->GetClipMap();
			for (auto it = clip_map.begin(); it != clip_map.end(); it++)
			{
				AddAssetFile(area_dir + it->second->file_path);

				//从Node中读取资源
				ovr_core::IOvrNodeCluster* cluster = ovr_core::IOvrNodeManager::GetIns()->LoadCluster(ovr_core::IOvrNodeCluster::kArea, it->second->file_path, a_track->GetOwnSequence());
				if (nullptr != cluster)
				{
					cluster->AppendAssetFile(asset_file_map);
					ovr_core::IOvrNodeManager::GetIns()->CloseCluster(cluster);
			}
		}
		}
		break;
		case ovr_core::kTrackEvent:
		{
			const OvrString& event_dir = ovr_core::IOvrNodeManager::GetIns()->GetEventDir();
			ovr_core::OvrEventTrack* event_track = (ovr_core::OvrEventTrack*)a_track;
			const std::map<ovr::uint64, OvrString>& key_map = event_track->GetAllKeyFrames();
			for (auto it = key_map.begin(); it != key_map.end(); it++)
			{
				AddAssetFile(event_dir+ it->second);

				//从Node中读取资源
				ovr_core::IOvrNodeCluster* cluster = ovr_core::IOvrNodeManager::GetIns()->LoadCluster(ovr_core::IOvrNodeCluster::kEvent, it->second, a_track->GetOwnSequence());
				if (nullptr != cluster)
				{
					cluster->AppendAssetFile(asset_file_map);
					ovr_core::IOvrNodeManager::GetIns()->CloseCluster(cluster);
				}
			}
		}
		break;*/
		case ovr_core::kTrackClip:
		{
			ovr_core::OvrClipTrack* clip_track = (ovr_core::OvrClipTrack*)a_track;
			const std::map<ovr::uint64, ovr_core::OvrClipInfo*>& clip_map = clip_track->GetClipMap();
			ovr_core::OvrClipInfo::Type clip_type = clip_track->GetClipType();
			switch (clip_type)
			{
			case ovr_core::OvrClipInfo::kInvalid:
			case ovr_core::OvrClipInfo::kSkeletalAnimation:
			case ovr_core::OvrClipInfo::kMorphAnimaition:
			case ovr_core::OvrClipInfo::kVideo:
			case ovr_core::OvrClipInfo::kAudio:
			{
				for (auto it = clip_map.begin(); it != clip_map.end(); it++)
				{
					AddAssetFile(it->second->file_path);
				}
			}
			break;
			case ovr_core::OvrClipInfo::kText:
				break;
			default:
				break;
			}

		}
		break;

		/*case ovr_core::kTrackMark:
		case ovr_core::kTrackFloat:
		case ovr_core::kTrackTransform:
		case ovr_core::kTrackSkeletalCapture:
		case ovr_core::kTrackMorphCapture:
			//没有资源可以进行收集
			break;*/
		default:
			break;
		}
	}

	//收集使用到的资源
	void OvrAssetCollecter::CollectSceneAsset(ovr_core::OvrSceneContext* a_scene)
	{
		asset_file_map.clear();

		const std::map<OvrString, ovr_engine::OvrActor*>& actor_map = a_scene->GetActorList();
		for (auto it = actor_map.begin(); it != actor_map.end(); it++)
		{
			//场景中 目前只有MeshActor 才使用到资源
			/*if (ovr_engine::kObjectMesh == it->second->GetType())
			{
				ovr_engine::OvrMeshActor* mesh_actor = (ovr_engine::OvrMeshActor*)(it->second);
				assert(nullptr != mesh_actor);

				//mesh
				ovr_engine::OvrMesh* mesh = mesh_actor->GetMesh();
				if (nullptr != mesh)
				{
					AddAssetFile(mesh->GetName());
				}

				//材质
				for (ovr::uint32 i = 0; i < mesh_actor->GetMaterialCount(); i++)
				{
					//材质
					ovr_engine::OvrMaterial* current_material = mesh_actor->GetMaterial(i);
					AddAssetFile(current_material->GetName());

					CollectMaterialAsset(current_material);
					
				}

				//骨骼
				if (mesh_actor->HasSkeleton())
				{
					AddAssetFile(mesh_actor->GetSkeletonInstance()->GetName());
				}

				//表情 待提供新的接口
				if (mesh_actor->HasPose())
				{
					AddAssetFile(mesh->GetPoseName());
				}
			}*/
		}
	}
	void OvrAssetCollecter::AddAssetFile(const OvrString& a_asset_file_path)
	{
		if (a_asset_file_path.GetStringSize() > 0 )
		{
			asset_file_map[a_asset_file_path] = false;
		}
	}

	void OvrAssetCollecter::CollectMaterialAsset(ovr_engine::OvrMaterial* a_material) 
	{
		//获取材质参数列表
		ovr_engine::OvrGpuProgramParameters* shader_params = a_material->GetProgramParameters();
		const ovr_engine::ConstantList* constant_list = shader_params->GetCustomConstantList();

		//遍历参数列表
		for (ovr::uint32 index = 0; index < constant_list->size(); index++)
		{
			const ovr_engine::OvrConstant& constant = (*constant_list)[index];

			if (!constant.IsSampler())
			{
				//目前只支持纹理
				continue;
			}

			//获取纹理路径
			ovr_engine::OvrTextureUnit* texture_layer = shader_params->GetTextureUnit(constant.physicalIndex);
			if (nullptr != texture_layer && nullptr != texture_layer->GetTexture())
			{
				OvrString texture_path = texture_layer->GetTexture()->GetName();
				if (texture_path != "null")
				{
					AddAssetFile(texture_path);
				}
			}
		}
	}
}