﻿#ifndef OVR_PROJECT_OVR_ARCHIVE_PROJECT_BUILDER_H_
#define OVR_PROJECT_OVR_ARCHIVE_PROJECT_BUILDER_H_

namespace ovr_project 
{
	class OvrProject;
}

namespace ovr_archive_project
{
	class OvrArchiveProjectBuilder
	{
	public:
		OvrArchiveProjectBuilder() {}
		~OvrArchiveProjectBuilder() {}

		bool	Save(ovr_project::OvrProject* a_project, const OvrString& a_file_path);
		bool	Load(ovr_project::OvrProject* a_project, const OvrString& a_file_path);

		bool	SaveEmpty(const OvrString& a_full_file_path);
	};
}

#endif
