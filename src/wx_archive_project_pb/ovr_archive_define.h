﻿#ifndef OVR_PROJECT_OVR_ARCHIVE_DEFINE_H_
#define OVR_PROJECT_OVR_ARCHIVE_DEFINE_H_

#define OVR_MAKE_FOURCC(ch0, ch1, ch2, ch3) \
	((ovr::uint32)(ovr::uint8)(ch0) | ((ovr::uint32)(ovr::uint8)(ch1) << 8) | \
	((ovr::uint32)(ovr::uint8)(ch2) << 16) | ((ovr::uint32)(ovr::uint8)(ch3) << 24))

#define OVR_FOURCC_PROJECT				OVR_MAKE_FOURCC( 'P', 'R', 'O', 'J' )
#define OVR_FOURCC_SEQUENCE				OVR_MAKE_FOURCC( 'S', 'E', 'Q', 'U' )
#define OVR_FOURCC_PLOT					OVR_MAKE_FOURCC( 'P', 'L', 'O', 'T' )
#define OVR_FOURCC_SENCE				OVR_MAKE_FOURCC( 'S', 'C', 'E', 'N' )
#define OVR_FOURCC_ASSET				OVR_MAKE_FOURCC( 'A', 'S', 'E', 'T' )
#define OVR_FOURCC_BLOCK_ACTOR			OVR_MAKE_FOURCC( 'A', 'T', 'H', 'D' )
#define OVR_FOURCC_ACTOR				OVR_MAKE_FOURCC( 'A', 'C', 'T', 'R' )
#define OVR_FOURCC_TRACK				OVR_MAKE_FOURCC( 'T', 'R', 'A', 'K' )
#define OVR_FOURCC_MEDIA				OVR_MAKE_FOURCC( 'O', 'V', 'R', 'M' )
#define OVR_FOURCC_CONFIG				OVR_MAKE_FOURCC( 'C', 'O', 'N', 'F' )
//#define OVR_FOURCC_ACTIVE_CONFIG		OVR_MAKE_FOURCC( 'A', 'C', 'N', 'F' )
//#define OVR_FOURCC_FACE_CONFIG			OVR_MAKE_FOURCC( 'F', 'C', 'N', 'F' )

namespace pb 
{
	class Transform;
	class Vector3;
	class Vector4;
}

namespace ovr_archive_project 
{
	struct OvrProjectHeader
	{
	public:

		ovr::uint32	fourcc = OVR_FOURCC_SEQUENCE;
		ovr::uint32	header_size = 0;
		ovr::uint16	version_major = 0;
		ovr::uint16	version_minor = 0;

		bool	Serialze(OvrArchive& a_serialize)
		{
			if (!a_serialize.Serialize(fourcc))
			{
				return false;
			}
			if (!a_serialize.Serialize(header_size))
			{
				return false;
			}
	
			if (!a_serialize.Serialize(version_major))
			{
				return false;
			}
			if (!a_serialize.Serialize(version_minor))
			{
				return false;
			}

			return true;
		}
	};

	class OvrArchiveBlock 
	{
	public:

		OvrArchiveBlock() {}
		~OvrArchiveBlock() {}

		ovr::uint32 fourcc = 0;
		ovr::uint32 size = 0;

		bool	Serialze(OvrArchive& a_serialize) 
		{
			if (!a_serialize.Serialize(fourcc))
			{
				return false;
			}
			if (!a_serialize.Serialize(size))
			{
				return false;
			}
			return true;
		}
	};
	class OvrArchiveActorHeader 
	{
	public:
		OvrArchiveActorHeader() {}
		~OvrArchiveActorHeader() {}

		ovr::uint32		fourcc	= 0;
		ovr::uint32		size	= 0;
		ovr::uint32		actor_type = 0;
		OvrString		display_name;
		OvrString		unique_name;
		OvrTransform	transform;

		bool	Serialze(OvrArchive& a_serialize)
		{
			if (!a_serialize.Serialize(fourcc))
			{
				return false;
			}
			if (!a_serialize.Serialize(size))
			{
				return false;
			}
			if (!a_serialize.Serialize(actor_type))
			{
				return false;
			}
			if (!a_serialize.Serialize(display_name))
			{
				return false;
			}
			if (!a_serialize.Serialize(unique_name))
			{
				return false;
			}
			if (!a_serialize.Serialize(transform))
			{
				return false;
			}
			return true;
		}
	};

	class OvrTrackBlock 
	{
	public:
		OvrTrackBlock() {}
		~OvrTrackBlock() {}

		ovr::uint32 fourcc = OVR_FOURCC_TRACK;
		ovr::uint32 size = 0;
		ovr::uint32 track_type = 0;

		bool	Serialze(OvrArchive& a_serialize)
		{
			if (!a_serialize.Serialize(fourcc))
			{
				return false;
			}
			if (!a_serialize.Serialize(size))
			{
				return false;
			}
			if (!a_serialize.Serialize(track_type))
			{
				return false;
			}
			return true;
		}
	};

	class OvrArchiveClipInfo
	{
	public:
		OvrArchiveClipInfo() {}
		OvrArchiveClipInfo(const OvrString& a_file_path, ovr::uint64 a_start_time, ovr::uint64 a_end_time) 
			:asset_file_path(a_file_path)
			, start_time(a_start_time)
			,end_time(a_end_time)
		{}
		~OvrArchiveClipInfo() {}

		void operator = (const OvrArchiveClipInfo& a_info)
		{
			asset_file_path = a_info.asset_file_path;
			start_time = a_info.start_time;
			end_time = a_info.end_time;
		}

		bool	Serialize(OvrArchive& a_archive) 
		{
			if (!a_archive.Serialize(asset_file_path))
			{
				return false;
			}
			if (!a_archive.Serialize(start_time))
			{
				return false;
			}
			if (!a_archive.Serialize(end_time))
			{
				return false;
			}
			return true;
		}

		OvrString	asset_file_path;
		ovr::uint64 start_time, end_time;
	};

	/*
	class OvrActorTrackBlock
	{
	public:

		OvrActorTrackBlock() {}
		~OvrActorTrackBlock() {}

		ovr::uint32 fourcc = OVR_FOURCC_TRACK;
		ovr::uint32 size = 0;
		ovr::uint32 track_type = 0;
		OvrString	actor_unique_name;
		ovr::uint32	subtrack_num = 0;

		bool	Serialze(OvrArchive& a_serialize)
		{
			if (!a_serialize.Serialize(fourcc))
			{
				return false;
			}
			if (!a_serialize.Serialize(size))
			{
				return false;
			}
			if (!a_serialize.Serialize(track_type))
			{
				return false;
			}
			if (!a_serialize.Serialize(actor_unique_name))
			{
				return false;
			}
			if (!a_serialize.Serialize(subtrack_num))
			{
				return false;
			}
			return true;
		}
	};*/

	bool	SetPBVector3(pb::Vector3* a_pb_vector3, const OvrVector3& a_vector3);
	void	SetOvrVector3(OvrVector3& a_vector3, const pb::Vector3& a_pb_vector3);

	bool	SetPBVector4(pb::Vector4* a_pb_vector4, const OvrVector4& a_vector4);
	void	SetOvrVector4(OvrVector4& a_vector4, const pb::Vector4& a_pb_vector4);

	bool	SetPBTransform(pb::Transform* a_pb_transform, const OvrTransform& a_transform);
	void	SetOvrTransform(OvrTransform& a_transform, const pb::Transform& a_pb_transform);
}

#endif
