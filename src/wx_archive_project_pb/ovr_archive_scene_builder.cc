﻿#include "headers.h"
#include <wx_core/ovr_scene_context.h>
#include "ovr_archive_scene_builder.h"


#include <wx_engine/ovr_texture_manager.h>
#include <wx_engine/ovr_actor.h>
#include <wx_engine/ovr_scene.h>
#include <wx_engine/ovr_material.h>
#include <wx_engine/ovr_mesh_manager.h>
#include <wx_engine/ovr_material_manager.h>
#include <wx_engine/ovr_mesh_component.h>
#include <wx_engine/ovr_text_component.h>
#include <wx_engine/ovr_camera_component.h>
#include <wx_engine/ovr_video_component.h>
#include <wx_engine/ovr_light_component.h>
#include <wx_engine/ovr_box_component.h>
#include <wx_engine/ovr_skinned_mesh_component.h>
#include <wx_engine/ovr_static_mesh_component.h>
#include <wx_core/component/ovr_audio_component.h>
#include <wx_core/ovr_scene_context.h>
#include <wx_project/ovr_scene_editor.h>
#include "wx_engine/ovr_skeleton_instance.h"
#include "pb/scene.pb.h"
#include "ovr_archive_define.h"
#include "ovr_asset_collecter.h"

namespace ovr_archive_project 
{
	bool OvrArchiveSceneBuilder::Save(ovr_project::OvrSceneEditor* a_scene)
	{
		if (nullptr == a_scene)
		{
			return false;
		}
		ovr_core::OvrSceneContext* scene_context = a_scene->GetSceneContext();
	
		pb::Scene archive_scene;
		//setheader
		archive_scene.mutable_header()->set_fourcc(OVR_FOURCC_SENCE);
		archive_scene.mutable_header()->set_major_version(0);
		archive_scene.mutable_header()->set_minor_version(0);

		OvrString unique_name_utf8;
		OvrString description_utf8;
		scene_context->GetUniqueName().ToUtf8(unique_name_utf8);
		scene_context->GetDescription().ToUtf8(description_utf8);

		//设置基本信息
		archive_scene.set_unique_name(unique_name_utf8.GetStringConst());
		archive_scene.set_description(description_utf8.GetStringConst());
		pb::Vector3* ambient_color = archive_scene.mutable_ambient_light();
		ambient_color->set_x(scene_context->GetScene()->GetAmbientColor()[0]);
		ambient_color->set_y(scene_context->GetScene()->GetAmbientColor()[1]);
		ambient_color->set_z(scene_context->GetScene()->GetAmbientColor()[2]);

		//sequence unique name list
		const std::list<OvrString>& sequence_name_list = scene_context->GetSequenceNameList();
		for (auto it = sequence_name_list.begin(); it != sequence_name_list.end(); it++)
		{
			OvrString string_utf8;
			it->ToUtf8(string_utf8);
			archive_scene.add_sequence_unique_name_list(string_utf8.GetStringConst());
		}
		
		//添加资源文件列表
		OvrAssetCollecter collecter;
		collecter.CollectSceneAsset(scene_context);
		for (auto it = collecter.asset_file_map.begin(); it != collecter.asset_file_map.end(); it++)
		{
			OvrString file_path_utf8;
			it->first.ToUtf8(file_path_utf8);
			archive_scene.add_asset_file_path_list(file_path_utf8.GetStringConst());
		}

		//actor对象
		ovr_engine::OvrActor* root_actor = scene_context->GetScene()->GetRoot();
		if (nullptr != root_actor)
		{
			pb::Tree* actor_tree = archive_scene.mutable_root_actor();
			actor_tree->set_unique_name("root_actor");
			SaveChildActor(a_scene, archive_scene, root_actor, actor_tree);
		}

		//序列化
		std::string out_stream;
		archive_scene.SerializeToString(&out_stream);

		//获取存盘路径
		OvrString full_path = ovr_core::OvrCore::GetIns()->GetWorkDir() + a_scene->GetFilePath();

		//写文件
		return OvrSmallFile::WriteFile(full_path, out_stream.c_str(), out_stream.size());
	}

	ovr_core::OvrSceneContext* OvrArchiveSceneBuilder::Load(const OvrString& a_scene_file_path)
	{
		OvrString full_path = ovr_core::OvrCore::GetIns()->GetWorkDir() + a_scene_file_path;
		OvrSmallFile small_file;
		if (!small_file.Open(full_path))
		{
			return nullptr;
		}
		pb::Scene archive_scene;
		std::string serialize_stream(small_file.GetFileData(), small_file.GetFileSize());
		if (!archive_scene.ParseFromString(serialize_stream))
		{
			return nullptr;
		}

		ovr_core::OvrSceneContext* new_scene = new ovr_core::OvrSceneContext;
		if (nullptr == new_scene)
		{
			return new_scene;
		}
		OvrString unique_name_gbk;
		unique_name_gbk.FromUtf8(archive_scene.unique_name().c_str());
		if (!new_scene->Init(unique_name_gbk))
		{
			new_scene->Uninit();
			delete new_scene;
			return nullptr;
		}
		
		OvrString description_gbk;
		description_gbk.FromUtf8(archive_scene.description().c_str());

		new_scene->SetFilePath(a_scene_file_path);
		new_scene->SetDescription(description_gbk.GetStringConst());
		auto ambient_color = archive_scene.ambient_light();
		new_scene->GetScene()->SetAmbientColor(OvrVector4(ambient_color.x(), ambient_color.y(), ambient_color.z(), 1.0f));

		//sequence list
		int sequence_file_list_size = archive_scene.sequence_unique_name_list_size();
		for (int i = 0; i < sequence_file_list_size; i++)
		{
			OvrString unique_name_gbk;
			unique_name_gbk.FromUtf8(archive_scene.sequence_unique_name_list(i).c_str());
			new_scene->AppendSequence(unique_name_gbk);
		}

		//actor list
		if (archive_scene.has_root_actor())
		{
			//形成Actormap
			LoadChildActor(new_scene, archive_scene, new_scene->GetScene()->GetRoot(),archive_scene.root_actor());
		}

		return new_scene;
	}

	bool OvrArchiveSceneBuilder::Create(const OvrString& a_full_path, const OvrString& a_unique_name) 
	{
		pb::Scene archive_scene;
		archive_scene.mutable_header()->set_fourcc(OVR_FOURCC_SENCE);
		archive_scene.mutable_header()->set_major_version(0);
		archive_scene.mutable_header()->set_minor_version(0);

		OvrString unique_name_utf8;
		a_unique_name.ToUtf8(unique_name_utf8);
		archive_scene.set_unique_name(unique_name_utf8.GetStringConst());
		pb::Vector3* amblight = archive_scene.mutable_ambient_light();
		amblight->set_x(1.0f);
		amblight->set_y(1.0f);
		amblight->set_z(1.0f);

		//序列化
		std::string out_stream;
		archive_scene.SerializeToString(&out_stream);

		//写文件
		return OvrSmallFile::WriteFile(a_full_path, out_stream.c_str(), out_stream.size());
	}

	bool OvrArchiveSceneBuilder::AddSequenceToScene(const OvrString& a_scene_full_path, const OvrString& a_sequence_unique_name) 
	{
		/*打开文件*/
		OvrSmallFile small_file;
		if (!small_file.Open(a_scene_full_path))
		{
			return false;
		}
		pb::Scene archive_scene;
		if (!archive_scene.ParseFromArray(small_file.GetFileData(), small_file.GetFileSize()))
		{
			return false;
		}

		/*修改*/
		OvrString unique_name_utf8;
		a_sequence_unique_name.ToUtf8(unique_name_utf8);
		archive_scene.add_sequence_unique_name_list(unique_name_utf8.GetStringConst());

		//保存
		//序列化
		std::string out_stream;
		archive_scene.SerializeToString(&out_stream);

		//写文件
		return OvrSmallFile::WriteFile(a_scene_full_path, out_stream.c_str(), out_stream.size());
	}

	void OvrArchiveSceneBuilder::SaveChildActor(ovr_project::OvrSceneEditor* a_scene, pb::Scene& a_archive_scene, \
												ovr_engine::OvrActor* a_parent_actor, pb::Tree* a_parent_tree)
	{
		ovr::uint32 child_num = a_parent_actor->GetChildCount();
		for (ovr::uint32 i = 0; i < child_num; i++)
		{
			ovr_engine::OvrActor* child_actor = a_parent_actor->GetChild(i);
			if (!a_scene->IsShowInScene(child_actor))
			{
				continue;
			}
			//将自身进行序列化
			if (!SaveActor(a_archive_scene, child_actor))
			{
				//自身序列化失败 不再对其子进行序列化
				continue;
			}
		
			OvrString unique_name_utf8;
			child_actor->GetUniqueName().ToUtf8(unique_name_utf8);

			pb::Tree* child_tree = a_parent_tree->add_child_tree();
			child_tree->set_index(a_archive_scene.actor_info_size() - 1);				//设置Actor在数组中的索引
			child_tree->set_unique_name(unique_name_utf8.GetStringConst());

			//将子进行序列化
			SaveChildActor(a_scene, a_archive_scene, child_actor, child_tree);
		}
	}

	void OvrArchiveSceneBuilder::LoadChildActor(ovr_core::OvrSceneContext* a_scene, pb::Scene& a_archive_scene, \
		ovr_engine::OvrActor* a_parent_actor, const pb::Tree& a_parent_tree)
	{
		int children_size = a_parent_tree.child_tree_size();
		for (int index = 0; index < children_size; index++)
		{
			//获取索引信息
			const pb::Tree& child_tree = a_parent_tree.child_tree(index);

			//创建Actor
			ovr_engine::OvrActor* new_actor = LoadActor(a_scene, a_archive_scene.actor_info(child_tree.index()));

			//此处还未构造正确的矩阵 所以最后一个参数  使用默认的false
			a_parent_actor->AddChild(new_actor);

			LoadChildActor(a_scene, a_archive_scene, new_actor, child_tree);
		}

	}

	bool OvrArchiveSceneBuilder::SaveActor(pb::Scene& a_archive_scene, ovr_engine::OvrActor* a_actor)
	{
		if (nullptr == a_actor)
		{
			return false;
		}

		//存放Actor的基本信息
		pb::ActorInfo* actor_info = a_archive_scene.add_actor_info();

		//设置bool值
		actor_info->set_is_visual(a_actor->IsVisible());
		actor_info->set_is_static(a_actor->IsStatic());

		//设置名字
		OvrString display_name_utf8;
		OvrString unique_name_utf8;
		a_actor->GetDisplayName().ToUtf8(display_name_utf8);
		a_actor->GetUniqueName().ToUtf8(unique_name_utf8);
		actor_info->set_display_name(display_name_utf8.GetStringConst());
		actor_info->set_unique_name(unique_name_utf8.GetStringConst());

		//设置transform
		pb::Transform* archive_transform = actor_info->mutable_transform();
		SetPBTransform(archive_transform, a_actor->GetTransform());
		
		const std::vector<ovr_engine::OvrComponent*>& components = a_actor->GetAllComponents();
		for (auto it = components.begin(); it < components.end(); it++)
		{
			//添加一个组件实例
			pb::ComponentInfo* component_info = actor_info->add_component_body();
			component_info->set_type((*it)->GetType());
			component_info->set_is_active((*it)->IsActive());

			//序列化组件体
			std::string serialize_stream;
			switch ((*it)->GetType())
			{
			case ovr_engine::kComponentParticleSystem:
				break;
			case ovr_engine::kComponentBoxCollider:
				SaveColliderBoxComponent(*it, serialize_stream);
				break;
			case ovr_engine::kComponentStaticMesh:
			case ovr_engine::kComponentSkinnedMesh:
				SaveMeshComponent(*it, serialize_stream);
				break;
			case ovr_engine::kComponentVideo:
				SaveVideoComponent(*it, serialize_stream);
				break;
			case ovr_core::kComponentAudio:
				SaveAudioComponent(*it, serialize_stream);
				break;
			case ovr_engine::kComponentLight:
				SaveLightComponent(*it, serialize_stream);
				break;
			case ovr_engine::kComponentText:
				SaveTextComponent(*it, serialize_stream);
				break;

			case ovr_engine::kComponentCamera:
				SaveCameraComponent(*it, serialize_stream);
				break;
			default:
				break;
			}

			component_info->set_body(serialize_stream);
		}

		return true;
	}

	void OvrArchiveSceneBuilder::SaveColliderBoxComponent(ovr_engine::OvrComponent* a_component, std::string& a_out_stream) 
	{
		ovr_engine::OvrBoxColliderComponent* collider_component = dynamic_cast<ovr_engine::OvrBoxColliderComponent*>(a_component);
		if (nullptr == collider_component)
		{
			return;
		}

		pb::ColliderBoxComponentBody collider_body;

		SetPBVector3(collider_body.mutable_center(), collider_component->GetCenter());
		SetPBVector3(collider_body.mutable_size(), collider_component->GetSize());

		collider_body.SerializeToString(&a_out_stream);
	}

	void OvrArchiveSceneBuilder::SaveMeshComponent(ovr_engine::OvrComponent* a_component, std::string& a_out_stream)
	{
		ovr_engine::OvrMeshComponent* skeletal_actor = (ovr_engine::OvrMeshComponent*)a_component;

		pb::MeshComponentBody mesh_body;

		mesh_body.set_is_cast_shadow(skeletal_actor->GetCastShadow());

		//mesh file path
		mesh_body.set_mesh_file_path(skeletal_actor->GetMesh()->GetName().GetStringConst());

		//材质
		for (ovr::uint32 i = 0; i < skeletal_actor->GetMaterialCount(); i++)
		{
			ovr_engine::OvrMaterial* material = skeletal_actor->GetMaterial(i);
			OvrString file_paht_utf8;
			material->GetName().ToUtf8(file_paht_utf8);
			mesh_body.add_material_file_path(file_paht_utf8.GetStringConst());
		}
		
		//序列化
		mesh_body.SerializeToString(&a_out_stream);
	}

	void OvrArchiveSceneBuilder::SaveTextComponent(ovr_engine::OvrComponent* a_component, std::string& a_out_stream)
	{
		pb::TextComponentBody text_body;

		ovr_engine::OvrTextComponent* text_actor = (ovr_engine::OvrTextComponent*)a_component;
		
		text_body.set_text(text_actor->GetText().GetStringConst());
		pb::Vector4* text_color = text_body.mutable_color();
		text_color->set_x(text_actor->GetTextColor()[0]);
		text_color->set_y(text_actor->GetTextColor()[1]);
		text_color->set_z(text_actor->GetTextColor()[2]);
		text_color->set_w(text_actor->GetTextColor()[3]);

		text_body.set_text_scale(text_actor->GetTextScale());

		float margin_x = 0.0f, margin_y = 0.0f;
		text_actor->GetTextMargin(margin_x, margin_y);
		text_body.set_margin_x(margin_x);
		text_body.set_margin_y(margin_y);

		text_body.set_width_wrap(text_actor->GetWarpWidth());
		text_body.set_line_wrap(text_actor->GetLineWarp());
		
		text_body.set_align_row(text_actor->GetAlignHorizontalMethod());
		text_body.set_align_column(text_actor->GetAlignVerticleMethod());
		text_body.set_transparent(text_actor->GetTransparentValue());
		
		text_body.SerializeToString(&a_out_stream);
	}

	void OvrArchiveSceneBuilder::SaveCameraComponent(ovr_engine::OvrComponent* a_component, std::string& a_out_stream)
	{
		ovr_engine::OvrCameraComponent* camera_actor = (ovr_engine::OvrCameraComponent*) a_component;

		pb::CameraComponentBody camera_body;
		camera_body.set_field_view(camera_actor->GetFieldView());
		camera_body.set_aspect_ration(camera_actor->GetAspectRatio());
		camera_body.set_near_clip(camera_actor->GetNearClip());
		camera_body.set_far_clip(camera_actor->GetFarClip());
		
		camera_body.SerializeToString(&a_out_stream);
	}
	

	void OvrArchiveSceneBuilder::SaveAudioComponent(ovr_engine::OvrComponent* a_component, std::string& a_out_stream)
	{
		//暂时无内容进行序列化
		ovr_core::OvrAudioComponent* audio_component = dynamic_cast<ovr_core::OvrAudioComponent*> (a_component);
		if (nullptr == audio_component)
		{
			return;
		}
	}

	void OvrArchiveSceneBuilder::SaveVideoComponent(ovr_engine::OvrComponent* a_component, std::string& a_out_stream)
	{
		ovr_engine::OvrVideoComponent* video_actor = (ovr_engine::OvrVideoComponent*)a_component;
		pb::VideoComponentBody actor_body;
		actor_body.set_mesh_type(video_actor->GetVideoType());
		actor_body.set_movie_type(video_actor->GetMovieType());

		actor_body.SerializeToString(&a_out_stream);
	}

	void OvrArchiveSceneBuilder::SaveLightComponent(ovr_engine::OvrComponent* a_component, std::string& a_out_stream)
	{
		pb::LightComponentBody light_body;

		ovr_engine::OvrLightComponent* light_actor = (ovr_engine::OvrLightComponent*)a_component;
		
		light_body.set_light_type(light_actor->GetType());
		light_body.set_is_cast_shadow(light_actor->IsCastShadow());
		SetPBVector4(light_body.mutable_light_color(), light_actor->GetDiffuseColor());

		if (ovr_engine::ePointLight == light_actor->GetType())
		{
			//点光
			light_body.set_intensity(light_actor->GetIntensity());
			light_body.set_range(light_actor->GetRange());
		}
		else if (ovr_engine::eDirectionLight == light_actor->GetType())
		{
			//平行光
			light_body.set_intensity(light_actor->GetIntensity());
		}
		else if (ovr_engine::eSpotLight == light_actor->GetType())
		{
			//锥形光
			light_body.set_intensity(light_actor->GetIntensity());
		}

		light_body.SerializeToString(&a_out_stream);
	}

	ovr_engine::OvrActor* OvrArchiveSceneBuilder::LoadActor(ovr_core::OvrSceneContext* a_scene_context, const pb::ActorInfo& a_actor_info)
	{
		ovr_engine::OvrActor* new_actor = a_scene_context->GetScene()->CreateActor();
		if (nullptr == new_actor)
		{
			return nullptr;
		}

		//设置bool值
		new_actor->SetVisible(a_actor_info.is_visual());
		new_actor->SetStatic(a_actor_info.is_static());

		//设置名字
		OvrString display_name_gbk;
		OvrString unique_name_gbk;
		display_name_gbk.FromUtf8(a_actor_info.display_name().c_str());
		unique_name_gbk.FromUtf8(a_actor_info.unique_name().c_str());
		new_actor->SetDisplayName(display_name_gbk.GetStringConst());
		new_actor->SetUniqueName(unique_name_gbk.GetStringConst());

		//设置transform
		OvrTransform transform;
		SetOvrTransform(transform, a_actor_info.transform());
		new_actor->SetTransform(transform);

		for (ovr::int32 i = 0; i < a_actor_info.component_body_size(); i++)
		{
			const ::pb::ComponentInfo& component_info = a_actor_info.component_body(i);
			ovr_engine::OvrComponent* new_component = nullptr;
			switch (component_info.type())
			{
			case ovr_engine::kComponentParticleSystem:
				break;
			case ovr_engine::kComponentBoxCollider:
				new_component = new_actor->AddComponent<ovr_engine::OvrBoxColliderComponent>();
				LoadColliderBoxComponent(new_component, component_info.body());
				break;
			case ovr_engine::kComponentStaticMesh:
				new_component = new_actor->AddComponent<ovr_engine::OvrStaticMeshComponent>();
				LoadMeshComponent(new_component, component_info.body());
				break;
			case ovr_engine::kComponentSkinnedMesh:
				new_component = new_actor->AddComponent<ovr_engine::OvrSkinnedMeshComponent>();
				LoadMeshComponent(new_component, component_info.body());
				break;
			case ovr_engine::kComponentVideo:
				new_component = new_actor->AddComponent<ovr_engine::OvrVideoComponent>();
				LoadVideoComponent(new_component, component_info.body());
				break;
			case ovr_core::kComponentAudio:
				new_component = new_actor->AddComponent<ovr_core::OvrAudioComponent>();
				LoadAudioComponent(new_component, component_info.body());
				break;
			case ovr_engine::kComponentLight:
				new_component = new_actor->AddComponent<ovr_engine::OvrLightComponent>();
				LoadLightComponent(new_component, component_info.body());
				break;

			case  ovr_engine::kComponentText:
				new_component = new_actor->AddComponent<ovr_engine::OvrTextComponent>();
				LoadTextComponent(new_component, component_info.body());
				break;
			case ovr_engine::kComponentCamera:
				new_component = new_actor->AddComponent<ovr_engine::OvrCameraComponent>();
				LoadCameraComponent(new_component, component_info.body());
				break;
			default:
				break;
			}

			if (nullptr != new_component)
			{
				new_component->SetActive(component_info.is_active());
			}
		}
		return new_actor;
	}

	bool OvrArchiveSceneBuilder::LoadColliderBoxComponent(ovr_engine::OvrComponent* a_component, const std::string& a_serialize_stream) 
	{
		ovr_engine::OvrBoxColliderComponent* collider_component = dynamic_cast<ovr_engine::OvrBoxColliderComponent*>(a_component);
		if (nullptr == collider_component)
		{
			return false;
		}

		pb::ColliderBoxComponentBody collider_body;
		if (!collider_body.ParseFromString(a_serialize_stream))
		{
			return false;
		}

		OvrVector3 box_center;
		OvrVector3 box_size;
		SetOvrVector3(box_center, collider_body.center());
		SetOvrVector3(box_size, collider_body.size());

		return true;
	}

	bool OvrArchiveSceneBuilder::LoadMeshComponent(ovr_engine::OvrComponent* a_component, const std::string& a_serialize_stream)
	{
		ovr_engine::OvrMeshComponent* mesh_component = dynamic_cast<ovr_engine::OvrMeshComponent*>(a_component);
		if (nullptr == mesh_component)
		{
			return false;
		}

		pb::MeshComponentBody mesh_body;
		if (!mesh_body.ParseFromString(a_serialize_stream)) 
		{
			return false;
		}

		mesh_component->SetCastShadow(mesh_body.is_cast_shadow());

		//mesh
		OvrString mesh_file_gbk;
		mesh_file_gbk.FromUtf8(mesh_body.mesh_file_path().c_str());
		ovr_engine::OvrMesh* ovr_mesh = ovr_engine::OvrMeshManager::GetIns()->Load(mesh_file_gbk);
		if (nullptr == ovr_mesh)
		{
			return false;
		}
		mesh_component->SetMesh(ovr_mesh);

		//材质
		for (int index = 0; index < mesh_body.material_file_path_size(); index++)
		{
			OvrString file_path_gbk;
			file_path_gbk.FromUtf8(mesh_body.material_file_path(index).c_str());
			ovr_engine::OvrMaterial* temp_material = ovr_engine::OvrMaterialManager::GetIns()->Load(file_path_gbk);
			mesh_component->SetMaterial(index, temp_material);
		}
		return true;
	}

	bool OvrArchiveSceneBuilder::LoadTextComponent(ovr_engine::OvrComponent* a_component, const std::string& a_serialize_stream)
	{
		ovr_engine::OvrTextComponent* text_component = dynamic_cast<ovr_engine::OvrTextComponent*>(a_component);
		if (nullptr == text_component)
		{
			return false;
		}

		pb::TextComponentBody text_body;
		if (!text_body.ParseFromString(a_serialize_stream))
		{
			return false;
		}

		text_component->SetText(text_body.text().c_str());
		//a_actor->SetFont(a_archive_actor->font);

		OvrVector4 color;
		color[0] = text_body.color().x();
		color[1] = text_body.color().y();
		color[2] = text_body.color().z();
		color[3] = text_body.color().w();

		text_component->SetTextColor(color);
		text_component->SetTextScale(text_body.text_scale());
		text_component->SetTextMargin(text_body.margin_x(), text_body.margin_y());
		text_component->SetWarpWidth(text_body.width_wrap());
		text_component->SetLineWarp(text_body.line_wrap());
		text_component->SetAlignMethod((ovr_engine::OvrAlignMethod)(text_body.align_row()), (ovr_engine::OvrAlignMethod)text_body.align_column());
		text_component->SetTransparentValue(text_body.transparent());
		
		/*if (text_body.background_texture().size() > 0)
		{
			OvrString file_path_gbk;
			file_path_gbk.FromUtf8(text_body.background_texture().c_str());
			ovr_engine::OvrTexture* texture = ovr_engine::OvrTextureManager::GetIns()->Load(file_path_gbk);
			text_component->SetBackgroundTexture(texture);
		}*/
		
		return true;
	}

	bool OvrArchiveSceneBuilder::LoadCameraComponent(ovr_engine::OvrComponent* a_component, const std::string& a_serialize_stream)
	{
		ovr_engine::OvrCameraComponent* camera_component = dynamic_cast<ovr_engine::OvrCameraComponent*>(a_component);
		if (nullptr == camera_component)
		{
			return false;
		}

		pb::CameraComponentBody camera_body;
		if (!camera_body.ParseFromString(a_serialize_stream))
		{
			return false;
		}

		camera_component->SetFieldView(camera_body.field_view());
		camera_component->SetAspectRatio(camera_body.aspect_ration());
		camera_component->SetNearClip(camera_body.near_clip());
		camera_component->SetFarClip(camera_body.far_clip());
		return true;
	}


	bool OvrArchiveSceneBuilder::LoadAudioComponent(ovr_engine::OvrComponent* a_component, const std::string& a_serialize_stream)
	{
		return true;
	}

	bool OvrArchiveSceneBuilder::LoadVideoComponent(ovr_engine::OvrComponent* a_component, const std::string& a_serialize_stream)
	{
		ovr_engine::OvrVideoComponent* video_component = dynamic_cast<ovr_engine::OvrVideoComponent*>(a_component);
		if (nullptr == video_component)
		{
			return false;
		}

		pb::VideoComponentBody video_body;
		if (!video_body.ParseFromString(a_serialize_stream))
		{
			return false;
		}

		video_component->SetVideoType((ovr_engine::VideoType) video_body.mesh_type());
		video_component->SetMovieType(ovr_engine::e_movie_type_left_right_3d_full);//临时代码
		video_component->Build();
		
		return true;
	}

	bool OvrArchiveSceneBuilder::LoadLightComponent(ovr_engine::OvrComponent* a_component, const std::string& a_serialize_stream)
	{
		ovr_engine::OvrLightComponent* light_component = dynamic_cast<ovr_engine::OvrLightComponent*>(a_component);
		if (nullptr == light_component)
		{
			return false;
		}

		pb::LightComponentBody light_body;
		if (!light_body.ParseFromString(a_serialize_stream))
		{
			return false;
		}

		light_component->SetLightType(ovr_engine::OvrLightType(light_body.light_type()));
		light_component->EnableCastShadow(light_body.is_cast_shadow());

		OvrVector4 light_color;
		SetOvrVector4(light_color, light_body.light_color());
		light_component->SetDiffuseColor(light_color);

		if (ovr_engine::ePointLight == light_body.light_type())
		{
			light_component->SetIntensity(light_body.intensity());
			light_component->SetRange(light_body.range());
		}
		else if (ovr_engine::eDirectionLight == light_body.light_type())
		{
			light_component->SetIntensity(light_body.intensity());
		}
		else if (ovr_engine::eSpotLight == light_body.light_type())
		{
			light_component->SetIntensity(light_body.intensity());
		}
		
		return true;
	}
} // namespace