﻿#include "headers.h"
#include <wx_engine/ovr_scene.h>
#include <wx_engine/ovr_actor.h>
#include <wx_core/ovr_sequence.h>
#include <wx_core/track/ovr_track.h>
#include <wx_core/track/ovr_actor_track.h>
#include <wx_core/track/ovr_background_audio_track.h>
#include <wx_core/track/ovr_interactive_track.h>
#include <wx_core/track/ovr_mark_track.h>
#include <wx_core/track/ovr_event_track.h>
#include <wx_core/track/ovr_clip.h>
#include <wx_core/track/ovr_float_track.h>
#include <wx_core/ovr_scene_context.h>
#include <wx_project/ovr_timeline_editor.h>

#include "pb/sequence.pb.h"
#include "pb/track.pb.h"
#include "pb/track_clip.pb.h"
#include "ovr_asset_collecter.h"

#include "ovr_archive_sequence_builder.h"

//#define OVR_BACKGROUND_3DAUDIO_UNIQUE_NAME		"THIS_IS_THE_BACKGROUND_3DAUDIO"
namespace ovr_archive_project
{
	bool OvrArchiveSequenceBuilder::Save(ovr_project::OvrTimelineEditor* a_sequence)
	{
		if (nullptr == a_sequence)
		{
			return false;
		}

		ovr_core::OvrSequence* sequence_core = a_sequence->GetSequence();

		pb::Sequence archive_sequence;
		//setheader
		archive_sequence.mutable_header()->set_fourcc(OVR_FOURCC_SENCE);
		archive_sequence.mutable_header()->set_major_version(0);
		archive_sequence.mutable_header()->set_minor_version(0);

		//基本信息
		OvrString unique_name_utf8;
		OvrString scene_unique_name_utf8;
		OvrString description_utf8;
		sequence_core->GetUniqueName().ToUtf8(unique_name_utf8);
		sequence_core->GetSenceUniqueName().ToUtf8(scene_unique_name_utf8);
		sequence_core->GetDescription().ToUtf8(description_utf8);

		archive_sequence.set_unique_name(unique_name_utf8.GetStringConst());
		archive_sequence.set_scene_unique_name(scene_unique_name_utf8.GetStringConst());
		archive_sequence.set_fps(sequence_core->GetFPS());
		archive_sequence.set_duration(sequence_core->GetDurtion());
		archive_sequence.set_description(description_utf8.GetStringConst());

		//收集asset file
		OvrAssetCollecter collecter;
		collecter.CollectSequenceAsset(sequence_core);

		for (auto it = collecter.asset_file_map.begin(); it != collecter.asset_file_map.end(); it++)
		{
			OvrString file_path_utf8;
			it->first.ToUtf8(file_path_utf8);
			archive_sequence.add_asset_file_path_list(file_path_utf8.GetStringConst());
		}

		//根轨道
		pb::Tree* root_tree = archive_sequence.mutable_root_track();
		root_tree->set_unique_name("root_track");

		
		const std::list<ovr_core::OvrTrack*>& track_list = sequence_core->GetChildren();
		for (auto track_it = track_list.begin(); track_it!= track_list.end(); track_it++)
		{
			SaveTrack(archive_sequence, root_tree, *track_it);
		}

		//序列化
		std::string out_stream;
		archive_sequence.SerializeToString(&out_stream);

		OvrString full_path = ovr_core::OvrCore::GetIns()->GetWorkDir() + a_sequence->GetFilePath();

		//写文件
		return OvrSmallFile::WriteFile(full_path, out_stream.c_str(), out_stream.size());
	}

	bool OvrArchiveSequenceBuilder::Load(ovr_project::OvrTimelineEditor* a_sequence)
	{
		ovr_core::OvrSequence* sequence_core = a_sequence->GetSequence();

		OvrString full_path = ovr_core::OvrCore::GetIns()->GetWorkDir() + a_sequence->GetFilePath();
		OvrSmallFile small_file;
		if (!small_file.Open(full_path))
		{
			return false;
		}
		pb::Sequence archive_sequence;
		std::string serialize_stream(small_file.GetFileData(), small_file.GetFileSize());
		if (!archive_sequence.ParseFromString(serialize_stream))
		{
			return false;
		}

		OvrString unique_name_gbk;
		OvrString scene_unique_name_gbk;
		OvrString description_gbk;
		unique_name_gbk.FromUtf8(archive_sequence.unique_name().c_str());
		scene_unique_name_gbk.FromUtf8(archive_sequence.scene_unique_name().c_str());
		description_gbk.FromUtf8(archive_sequence.description().c_str());

		sequence_core->SetUniqueName(unique_name_gbk);
		sequence_core->SetSceneUniqueName(scene_unique_name_gbk);
		sequence_core->SetFPS(archive_sequence.fps());
		sequence_core->SetDurtion(archive_sequence.duration());
		sequence_core->SetDescription(description_gbk);

		if (archive_sequence.track_info_size() > 0 && archive_sequence.has_root_track())
		{
			LoadTrack(sequence_core, archive_sequence, archive_sequence.root_track());
		}
		
		return true;
	}

	bool OvrArchiveSequenceBuilder::Create(const OvrString& a_full_path, const OvrString& a_scene_unique_name, const OvrString& a_unique_name) 
	{
		pb::Sequence archive_sequence;
		archive_sequence.mutable_header()->set_fourcc(OVR_FOURCC_SENCE);
		archive_sequence.mutable_header()->set_major_version(0);
		archive_sequence.mutable_header()->set_minor_version(0);
		archive_sequence.set_unique_name(a_unique_name.GetStringConst());
		archive_sequence.set_scene_unique_name(a_scene_unique_name.GetStringConst());
		archive_sequence.set_fps(30);
		archive_sequence.set_duration(10 * 60 * 30);

		//序列化
		std::string out_stream;
		archive_sequence.SerializeToString(&out_stream);

		//写文件
		return OvrSmallFile::WriteFile(a_full_path, out_stream.c_str(), out_stream.size());
	}

	bool OvrArchiveSequenceBuilder::SaveTrack(pb::Sequence& a_archive_sequence, pb::Tree* a_parent_tree, ovr_core::OvrTrack* a_track)
	{
		bool is_success = true;
		ovr_core::OvrTrackType track_type = a_track->GetTrackType();
		std::string body_stream;
		/*switch (a_track->GetTrackType())
		{
		case ovr_core::kTrackInteractive:
			SaveClipTrack(a_track, body_stream);
			break;
		case ovr_core::kTrackEvent:
			SaveEventTrack(a_track, body_stream);
			break;
		case ovr_core::kTrackGroup:
			if (a_track->GetTag() == "actor")
			{
				SaveActorTrack(a_track, body_stream);
			}
			break;
		case ovr_core::kTrackMark:
			SaveMarkTrack(a_track, body_stream);
			break;
		default:
			is_success = false;
			break;
		}*/
		if (is_success)
		{
			//填充轨道信息
			OvrString tag_utf8;
			OvrString unique_name_utf8;
			a_track->GetTag().ToUtf8(tag_utf8);
			a_track->GetUniqueName().ToUtf8(unique_name_utf8);
			pb::TrackInfo* track_info = a_archive_sequence.add_track_info();
			track_info->set_track_type(a_track->GetTrackType());
			track_info->set_tag(tag_utf8.GetStringConst());
			track_info->set_unique_name(unique_name_utf8.GetStringConst());
			track_info->set_body(body_stream);

			//增加一个子
			pb::Tree* current_tree = a_parent_tree->add_child_tree();
			current_tree->set_unique_name(unique_name_utf8.GetStringConst());
			current_tree->set_index(a_archive_sequence.track_info_size() - 1);

			//增加子轨道
			SaveSubTrack(a_archive_sequence, current_tree, a_track);
		}
		return is_success;
	}

	void OvrArchiveSequenceBuilder::SaveActorTrack(ovr_core::OvrTrack* a_track, std::string& a_out_stream)
	{
		ovr_core::OvrActorTrack* actor_track = (ovr_core::OvrActorTrack*)a_track;

		//设置ActorTrackBody的信息
		pb::ActorTrackBody actor_body;
		OvrString name_utf8;
		actor_track->GetActor()->GetUniqueName().ToUtf8(name_utf8);
		actor_body.set_actor_unique_name(name_utf8.GetStringConst());
		actor_body.SerializeToString(&a_out_stream);
	}

	void OvrArchiveSequenceBuilder::SaveSubTrack(pb::Sequence& a_archive_sequence, pb::Tree* a_parent_tree, ovr_core::OvrTrack* a_parent_track)
	{
		auto sub_track_list = a_parent_track->GetChildren();
		for (auto track_it = sub_track_list.begin(); track_it != sub_track_list.end(); track_it++)
		{
			ovr_core::OvrTrack* current_track = (*track_it);
			bool is_success = true;
			std::string body_stream;
			/*switch (current_track->GetTrackType())
			{
			case ovr_core::kTrackClip:
				SaveClipTrack(current_track, body_stream);
				break;

			case ovr_core::kTrackGroup:
				//除子轨道外 没有其他数据 
				break;

			case ovr_core::kTrackSkeletalCapture:
			case ovr_core::kTrackMorphCapture:
				SaveCaptureTrack((ovr_core::OvrActorSubCaptureTrack*)current_track, body_stream);
				break;
			case ovr_core::kTrackFloat:
				SaveTransparentTrack(current_track, body_stream);
				break;
			default:
				is_success = false;
				break;
			}*/
			if (is_success)
			{
				//填充轨道信息
				OvrString tag_utf8;
				OvrString unique_name_utf8;
				current_track->GetTag().ToUtf8(tag_utf8);
				current_track->GetUniqueName().ToUtf8(unique_name_utf8);
				pb::TrackInfo* track_info = a_archive_sequence.add_track_info();
				track_info->set_track_type(current_track->GetTrackType());
				track_info->set_tag(tag_utf8.GetStringConst());
				track_info->set_unique_name(unique_name_utf8.GetStringConst());
				track_info->set_body(body_stream);

				//添加关联信息
				pb::Tree* current_tree = a_parent_tree->add_child_tree();
				current_tree->set_unique_name(unique_name_utf8.GetStringConst());
				current_tree->set_index(a_archive_sequence.track_info_size() - 1);

				SaveSubTrack(a_archive_sequence, current_tree, current_track);
			}
		}
	}

	void OvrArchiveSequenceBuilder::SaveClipTrack(ovr_core::OvrTrack* a_track, std::string& a_out_stream)
	{
		ovr_core::OvrClipTrack* clip_track = (ovr_core::OvrClipTrack*)a_track;

		pb::ClipTrackBody clip_body;
		clip_body.set_clip_type(clip_track->GetClipType());

		if (clip_track->GetClipType() != ovr_core::OvrClipInfo::kText)
		{
			auto clip_map = clip_track->GetClipMap();
			for (auto &clip_it : clip_map)
			{
				//非文本clip
				pb::FilePathClip path_clip;
				pb::TrackClipHeader* clip_info = path_clip.mutable_clip_header();
				clip_info->set_timeline_position(clip_it.second->timeline_position);
				clip_info->set_start_time(clip_it.second->start_time);
				clip_info->set_duration((ovr::uint32)(clip_it.second->end_time - clip_it.second->start_time));
				OvrString path_utf8;
				clip_it.second->file_path.ToUtf8(path_utf8);
				path_clip.set_file_path(path_utf8.GetStringConst());

				std::string out_stream;
				path_clip.SerializeToString(&out_stream);

				clip_body.add_clip_info(out_stream);
			}
		}
		else 
		{
			auto clip_map = clip_track->GetClipMap();
			for (auto &clip_it : clip_map)
			{
				ovr_core::OvrTextClip* temp_info = (ovr_core::OvrTextClip*)clip_it.second;

				//非文本clip
				pb::TextClip text_clip;
				pb::TrackClipHeader* clip_info = text_clip.mutable_clip_header();
				clip_info->set_timeline_position(clip_it.second->timeline_position);
				clip_info->set_start_time(clip_it.second->start_time);
				clip_info->set_duration((ovr::uint32)(clip_it.second->end_time - clip_it.second->start_time));
				OvrString text_utf8;
				temp_info->GetText().ToUtf8(text_utf8);
				text_clip.set_text(text_utf8.GetStringConst());
				text_clip.set_show_time(temp_info->GetShowTime());

				std::string out_stream;
				text_clip.SerializeToString(&out_stream);

				clip_body.add_clip_info(out_stream);
			}
		}

		//body -> 字符串
		clip_body.SerializeToString(&a_out_stream);
	}

	void OvrArchiveSequenceBuilder::SaveMarkTrack(ovr_core::OvrTrack* a_track, std::string& a_out_stream)
	{
		ovr_core::OvrMarkTrack* mark_track = (ovr_core::OvrMarkTrack*)a_track;

		const std::map<ovr::uint64, OvrString>& mark_frames = mark_track->GetKeyFrames();
		pb::StringTrackBody track_body;
		auto pb_mark_frames = track_body.mutable_value_map();
		for (auto it = mark_frames.begin(); it != mark_frames.end(); it++)
		{
			OvrString value_utf8;
			it->second.ToUtf8(value_utf8);
			(*pb_mark_frames)[it->first] = value_utf8.GetStringConst();
		}

		track_body.SerializeToString(&a_out_stream);
	}

	void OvrArchiveSequenceBuilder::SaveEventTrack(ovr_core::OvrTrack* a_track, std::string& a_out_stream)
	{
		ovr_core::OvrEventTrack* mark_track = (ovr_core::OvrEventTrack*)a_track;

		const std::map<ovr::uint64, OvrString>& mark_frames = mark_track->GetAllKeyFrames();
		pb::StringTrackBody track_body;
		auto pb_mark_frames = track_body.mutable_value_map();
		for (auto it = mark_frames.begin(); it != mark_frames.end(); it++)
		{
			OvrString value_utf8;
			it->second.ToUtf8(value_utf8);
			(*pb_mark_frames)[it->first] = value_utf8.GetStringConst();
		}

		track_body.SerializeToString(&a_out_stream);
	}

	void OvrArchiveSequenceBuilder::SaveTransparentTrack(ovr_core::OvrTrack* a_track, std::string& a_out_stream)
	{
		ovr_core::OvrFloatTrack* transparent_track = (ovr_core::OvrFloatTrack*)a_track;

		const std::map<ovr::uint64, float>& frame_map = transparent_track->GetKeyFrameMap();

		pb::FloatTrackBody float_body;
		auto pb_frame_map = float_body.mutable_value_map();
		for (auto it = frame_map.begin(); it != frame_map.end(); it++)
		{
			(*pb_frame_map)[it->first] = it->second;
		}

		float_body.SerializeToString(&a_out_stream);
	}

	//--------------------load--------------------------------------------------------------------------

	bool OvrArchiveSequenceBuilder::LoadTrack(ovr_core::OvrSequence* a_sequence, const pb::Sequence& a_archive_sequence, const pb::Tree& a_parent_tree)
	{
		int child_size = a_parent_tree.child_tree_size();
		for (int i = 0; i < child_size; i++)
		{
			const ::pb::Tree& child = a_parent_tree.child_tree(i);
			const ::pb::TrackInfo& track_info = a_archive_sequence.track_info(child.index());
			
			OvrString tag_gbk;
			OvrString unique_name_gbk;
			tag_gbk.FromUtf8(track_info.tag().c_str());
			unique_name_gbk.FromUtf8(track_info.unique_name().c_str());

			ovr_core::OvrTrack* new_track = nullptr;
			/*switch (track_info.track_type())
			{
			case ovr_core::kTrackInteractive:
				new_track = LoadInteractiveTrack(a_sequence, track_info);
				break;
			case ovr_core::kTrackEvent:
				new_track = LoadEventTrack(a_sequence, track_info);
				break;
			case ovr_core::kTrackMark:
				new_track = LoadMarkTrack(a_sequence, track_info);
				break;
			case ovr_core::kTrackGroup:
				if (tag_gbk == "actor")
				{
					new_track = LoadActorTrack(a_sequence, track_info);
				}
				else 
				{
					new_track = a_sequence->AddTrack(ovr_core::kTrackGroup, tag_gbk);
				}
				
			default:
				break;
			}*/
			if (nullptr != new_track)
			{
				new_track->SetUniqueName(unique_name_gbk);
				LoadSubTrack(new_track, a_archive_sequence, child);
			}
		}
		return true;
	}
	ovr_core::OvrTrack* OvrArchiveSequenceBuilder::LoadActorTrack(ovr_project::OvrTimelineEditor* a_sequence, const ::pb::TrackInfo& a_track_info)
	{
		pb::ActorTrackBody track_body;
		if (!track_body.ParseFromString(a_track_info.body())) 
		{
			return nullptr;
		}

		ovr_core::OvrSequence* sequence_core = a_sequence->GetSequence();
		ovr_core::OvrActorTrack* actor_track = nullptr;
		ovr_engine::OvrScene* current_scene = sequence_core->GetScene()->GetScene();
		if (nullptr != current_scene)
		{
			OvrString actor_unique_name_gbk;
			OvrString track_unique_name_gbk;
			actor_unique_name_gbk.FromUtf8(track_body.actor_unique_name().c_str());
			track_unique_name_gbk.FromUtf8(a_track_info.unique_name().c_str());
			actor_track = a_sequence->AddActorTrack(actor_unique_name_gbk);
			actor_track->SetUniqueName(track_unique_name_gbk);
		}
		return actor_track;
	}

	ovr_core::OvrTrack* OvrArchiveSequenceBuilder::LoadMarkTrack(ovr_core::OvrSequence* a_sequence, const ::pb::TrackInfo& a_track_info)
	{
		pb::StringTrackBody track_body;
		if (!track_body.ParseFromString(a_track_info.body()))
		{
			return nullptr;
		}

		/*ovr_core::OvrMarkTrack* new_track = (ovr_core::OvrMarkTrack*)a_sequence->AddTrack(ovr_core::kTrackMark, "mark");
		if (nullptr == new_track)
		{
			return nullptr;
		}
		auto pb_value_map = track_body.value_map();
		for (auto it = pb_value_map.begin(); it != pb_value_map.end(); it++)
		{
			OvrString value_gbk;
			value_gbk.FromUtf8(it->second.c_str());
			new_track->AddKeyFrame(it->first, value_gbk);
		}

		return new_track;*/
		return nullptr;
	}

	ovr_core::OvrTrack* OvrArchiveSequenceBuilder::LoadEventTrack(ovr_core::OvrSequence* a_sequence, const ::pb::TrackInfo& a_track_info)
	{
		pb::StringTrackBody track_body;
		if (!track_body.ParseFromString(a_track_info.body()))
		{
			return nullptr;
		}

		ovr_core::OvrEventTrack* new_track;// = (ovr_core::OvrEventTrack*)a_sequence->AddTrack(ovr_core::kTrackEvent, "event");
		//if (nullptr == new_track)
		{
			return nullptr;
		}

		auto pb_value_map = track_body.value_map();
		for (auto it = pb_value_map.begin(); it != pb_value_map.end(); it++)
		{
			OvrString value_gbk;
			value_gbk.FromUtf8(it->second.c_str());
			new_track->AddKeyFrame(it->first, value_gbk);
		}

		return new_track;
	}

	ovr_core::OvrTrack* OvrArchiveSequenceBuilder::LoadInteractiveTrack(ovr_core::OvrSequence* a_sequence, const ::pb::TrackInfo& a_track_info)
	{
		ovr_core::OvrTrack* new_track = nullptr;// = a_sequence->AddTrack(ovr_core::kTrackInteractive, "interactive");
		
		LoadClipTrack(new_track, a_track_info);

		return new_track;
	}

	bool OvrArchiveSequenceBuilder::LoadSubTrack(ovr_core::OvrTrack* a_parent_track, const pb::Sequence& a_archive_sequence, const pb::Tree& a_parent_tree)
	{
		int child_size = a_parent_tree.child_tree_size();
		for (int i = 0; i < child_size; i++)
		{
			const ::pb::Tree& child = a_parent_tree.child_tree(i);
			const ::pb::TrackInfo& track_info = a_archive_sequence.track_info(child.index());

			OvrString tag_gbk;
			OvrString unique_name_gbk;
			tag_gbk.FromUtf8(track_info.tag().c_str());
			unique_name_gbk.FromUtf8(track_info.unique_name().c_str());

			ovr_core::OvrTrack* new_track = a_parent_track->AddTrack((ovr_core::OvrTrackType)track_info.track_type(), tag_gbk);
			assert(nullptr != new_track);
			new_track->SetUniqueName(unique_name_gbk);
			
			/*switch (track_info.track_type())
			{
			case ovr_core::kTrackClip:
				LoadClipTrack(new_track, track_info);
				break;
			case ovr_core::kTrackGroup:
				//没有需要加载的内容
				break;

			case ovr_core::kTrackSkeletalCapture:
			case ovr_core::kTrackMorphCapture:
				LoadCaptureTrack(new_track, track_info);
				break;
			case ovr_core::kTrackFloat:
				LoadTransparentTrack(new_track, track_info);
				break;
			default:
				break;
			}*/
			if (nullptr != new_track)
			{
				LoadSubTrack(new_track, a_archive_sequence, child);
			}
		}
		return true;
	}

	bool OvrArchiveSequenceBuilder::LoadClipTrack(ovr_core::OvrTrack* a_track, const pb::TrackInfo& a_track_info)
	{
		pb::ClipTrackBody track_body;
		if (!track_body.ParseFromString(a_track_info.body()))
		{
			assert(false);
			return false;
		}

		ovr_core::OvrClipTrack* clip_track = (ovr_core::OvrClipTrack*)a_track;
		clip_track->SetClipType((ovr_core::OvrClipInfo::Type)track_body.clip_type());
		if (clip_track->GetClipType() == ovr_core::OvrClipInfo::kText)
		{
			int clip_size = track_body.clip_info_size();
			for (int i = 0; i < clip_size; i++)
			{
				pb::TextClip pb_text_clip;
				if (!pb_text_clip.ParseFromString(track_body.clip_info(i)))
				{
					continue;
				}

				ovr_core::OvrTextClip* new_clip = (ovr_core::OvrTextClip*)clip_track->AddClip(pb_text_clip.clip_header().timeline_position());
				new_clip->start_time = pb_text_clip.clip_header().start_time();
				new_clip->end_time = pb_text_clip.clip_header().start_time() + pb_text_clip.clip_header().duration();
				OvrString text_gbk;
				text_gbk.FromUtf8(pb_text_clip.text().c_str());
				new_clip->SetText(text_gbk);
				new_clip->SetShowTime(pb_text_clip.show_time());
			}
			
		}
		else 
		{
			int clip_size = track_body.clip_info_size();
			for (int i = 0; i < clip_size; i++)
			{
				pb::FilePathClip pb_file_path_clip;
				if (!pb_file_path_clip.ParseFromString(track_body.clip_info(i)))
				{
					continue;
				}
				ovr_core::OvrClipInfo* new_clip = clip_track->AddClip(pb_file_path_clip.clip_header().timeline_position());
				new_clip->start_time = pb_file_path_clip.clip_header().start_time();
				new_clip->end_time = pb_file_path_clip.clip_header().start_time() + pb_file_path_clip.clip_header().duration();
				OvrString path_gbk;
				path_gbk.FromUtf8(pb_file_path_clip.file_path().c_str());
				new_clip->file_path = path_gbk;
			}
		}
		return true;
	}

	bool OvrArchiveSequenceBuilder::LoadTransparentTrack(ovr_core::OvrTrack* a_track, const pb::TrackInfo& a_track_info)
	{
		pb::FloatTrackBody track_body;
		if (!track_body.ParseFromString(a_track_info.body()))
		{
			assert(false);
			return false;
		}

		ovr_core::OvrFloatTrack* new_track = (ovr_core::OvrFloatTrack*)a_track;

		auto value_map = track_body.value_map();
		for (auto it = value_map.begin(); it != value_map.end();it++)
		{
			new_track->AddKeyFrame(it->first, it->second);
		}
		return true;
	}

} // namespace ovr_project

