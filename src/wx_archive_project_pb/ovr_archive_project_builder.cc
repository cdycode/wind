﻿

#include "headers.h"
#include "ovr_archive_project_builder.h"
#include <wx_project/ovr_project.h>
#include <wx_core/ovr_scene_context.h>
#include <wx_core/ovr_sequence.h>
#include <wx_project/ovr_asset_manager.h>
#include "pb/project.pb.h"

namespace ovr_archive_project 
{
	bool OvrArchiveProjectBuilder::Save(ovr_project::OvrProject* a_project, const OvrString& a_file_path)
	{
		pb::Project archive_project;
		archive_project.mutable_header()->set_fourcc(OVR_FOURCC_PROJECT);
		archive_project.mutable_header()->set_major_version(0);
		archive_project.mutable_header()->set_minor_version(0);
		
		ovr_core::OvrSceneContext* current_scene = a_project->GetCurrentScene();
		ovr_core::OvrSequence* current_sequence = a_project->GetCurrentSequence();
		if (nullptr != current_scene)
		{
			OvrString file_path_utf8;
			current_scene->GetFilePath().ToUtf8(file_path_utf8);
			archive_project.set_current_scene_file(file_path_utf8.GetStringConst());
		}
		if (nullptr != current_sequence)
		{
			OvrString file_path_utf8;
			current_sequence->GetFilePath().ToUtf8(file_path_utf8);
			archive_project.set_current_sequence_file(file_path_utf8.GetStringConst());
		}

		//Plot file list
		const std::list<ovr_project::OvrProjectFileInfo>& plot_file_list = a_project->GetAssetManager()->GetPlotFileList();
		for (auto it = plot_file_list.begin(); it != plot_file_list.end(); it++)
		{
			OvrString file_path_utf8;
			OvrString unique_name_utf8;
			it->file_path.ToUtf8(file_path_utf8);
			it->unique_name.ToUtf8(unique_name_utf8);

			auto plot_file = archive_project.add_plot_file_list();
			plot_file->set_file_path(file_path_utf8.GetStringConst());
			plot_file->set_unique_name(unique_name_utf8.GetStringConst());
		}

		//Sequence file list
		const std::list<ovr_project::OvrProjectFileInfo>& sequence_file_list = a_project->GetAssetManager()->GetSequenceFileList();
		for (auto it = sequence_file_list.begin(); it != sequence_file_list.end(); it++)
		{
			OvrString file_path_utf8;
			OvrString unique_name_utf8;
			it->file_path.ToUtf8(file_path_utf8);
			it->unique_name.ToUtf8(unique_name_utf8);

			auto sequence_file = archive_project.add_sequence_file_list();
			sequence_file->set_file_path(file_path_utf8.GetStringConst());
			sequence_file->set_unique_name(unique_name_utf8.GetStringConst());
		}

		//scene file list
		const std::list<ovr_project::OvrProjectFileInfo>& scene_file_list = a_project->GetAssetManager()->GetSceneFileList();
		for (auto it = scene_file_list.begin(); it != scene_file_list.end(); it++)
		{
			OvrString file_path_utf8;
			OvrString unique_name_utf8;
			it->file_path.ToUtf8(file_path_utf8);
			it->unique_name.ToUtf8(unique_name_utf8);

			auto scene_file = archive_project.add_scene_file_list();
			scene_file->set_file_path(file_path_utf8.GetStringConst());
			scene_file->set_unique_name(unique_name_utf8.GetStringConst());
		}

		//asset file list
		const std::list<ovr_project::OvrAssetInfo>& asset_file_list = a_project->GetAssetManager()->GetAssetFileList();
		for (auto it = asset_file_list.begin(); it != asset_file_list.end(); it++)
		{
			OvrString file_path_utf8;
			it->file_path.ToUtf8(file_path_utf8);

			auto asset_file = archive_project.add_asset_file_list();
			asset_file->set_file_path(file_path_utf8.GetStringConst());
			asset_file->set_type((pb::AssetFileInfo_AssetType)it->asset_type);
		}
		
		//序列化
		std::string out_stream;
		archive_project.SerializeToString(&out_stream);

		//获取存盘路径
		OvrString full_path = ovr_project::OvrProject::GetIns()->GetWorkDir() + a_file_path;

		//写文件
		return OvrSmallFile::WriteFile(full_path, out_stream.c_str(), out_stream.size());
	}

	bool OvrArchiveProjectBuilder::Load(ovr_project::OvrProject* a_project, const OvrString& a_file_path)
	{
		OvrString full_path = ovr_project::OvrProject::GetIns()->GetWorkDir() + a_file_path;
		OvrSmallFile small_file;
		if (!small_file.Open(full_path))
		{
			return false;
		}
		pb::Project archive_project;
		std::string serialize_stream(small_file.GetFileData(), small_file.GetFileSize());
		if (!archive_project.ParseFromString(serialize_stream))
		{
			return false;
		}

		if (archive_project.current_scene_file().size() > 0)
		{
			OvrString file_path_gbk;
			file_path_gbk.FromUtf8(archive_project.current_scene_file().c_str());
			a_project->OpenSceneFromFile(file_path_gbk.GetStringConst());
		}
		if (archive_project.current_sequence_file().size() > 0)
		{
			OvrString file_path_gbk;
			file_path_gbk.FromUtf8(archive_project.current_sequence_file().c_str());
			a_project->OpenSequenceFromFile(file_path_gbk.GetStringConst());
		}

		//plot file
		for (int index = 0; index < archive_project.plot_file_list_size(); index++)
		{
			ovr_project::OvrProjectFileInfo plot_info;
			plot_info.file_path.FromUtf8(archive_project.plot_file_list(index).file_path().c_str());
			plot_info.unique_name.FromUtf8(archive_project.plot_file_list(index).unique_name().c_str());
			a_project->GetAssetManager()->AddPlotFile(plot_info);
		}

		//sequence file
		for (int index = 0; index < archive_project.sequence_file_list_size(); index++)
		{
			ovr_project::OvrProjectFileInfo sequence_info;
			sequence_info.file_path.FromUtf8(archive_project.sequence_file_list(index).file_path().c_str());
			sequence_info.unique_name.FromUtf8(archive_project.sequence_file_list(index).unique_name().c_str());
			a_project->GetAssetManager()->AddSequenceFile(sequence_info);
		}
		
		//scene file
		for (int index = 0; index < archive_project.scene_file_list_size(); index++)
		{
			ovr_project::OvrProjectFileInfo scene_info;
			scene_info.file_path.FromUtf8(archive_project.scene_file_list(index).file_path().c_str());
			scene_info.unique_name.FromUtf8(archive_project.scene_file_list(index).unique_name().c_str());
			a_project->GetAssetManager()->AddSceneFile(scene_info);
		}

		//asset file
		for (int index = 0; index < archive_project.asset_file_list_size(); index++)
		{
			ovr_project::OvrAssetInfo asset_info;
			asset_info.file_path.FromUtf8(archive_project.asset_file_list(index).file_path().c_str());
			asset_info.asset_type = archive_project.asset_file_list(index).type();
			a_project->GetAssetManager()->AddAssetFile(asset_info);
		}

		return true;
	}

	bool OvrArchiveProjectBuilder::SaveEmpty(const OvrString& a_full_file_path) 
	{
		pb::Project archive_project;
		archive_project.mutable_header()->set_fourcc(OVR_FOURCC_PROJECT);
		archive_project.mutable_header()->set_major_version(0);
		archive_project.mutable_header()->set_minor_version(0);

		//序列化
		std::string out_stream;
		archive_project.SerializeToString(&out_stream);

		//写文件
		return OvrSmallFile::WriteFile(a_full_file_path, out_stream.c_str(), out_stream.size());
	}
}

