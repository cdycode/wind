﻿#include "headers.h"
#include "ovr_archive_project_manager.h"
#include "ovr_archive_project_builder.h"
#include "ovr_archive_scene_builder.h"
#include "ovr_archive_sequence_builder.h"

namespace ovr_archive_project
{
	OvrArchiveProjectManager* sg_archive_project_manager = nullptr;
}


//导出函数
//OvrVisualNode的初始化和反初始化函数

OVR_ARCHIVE_PROJECT_API void StopPlugin()
{
	if (nullptr != ovr_archive_project::sg_archive_project_manager)
	{
		delete ovr_archive_project::sg_archive_project_manager;
		ovr_archive_project::sg_archive_project_manager = nullptr;

		google::protobuf::ShutdownProtobufLibrary();
	}
}

OVR_ARCHIVE_PROJECT_API wx_plugin::wxIPlugin* StartPlugin()
{
	GOOGLE_PROTOBUF_VERIFY_VERSION;

	if (nullptr != ovr_archive_project::sg_archive_project_manager)
	{
		return ovr_archive_project::sg_archive_project_manager;
	}
	ovr_archive_project::sg_archive_project_manager = new ovr_archive_project::OvrArchiveProjectManager;
	if (nullptr == ovr_archive_project::sg_archive_project_manager)
	{
		return nullptr;
	}
	return ovr_archive_project::sg_archive_project_manager;
}


namespace ovr_archive_project 
{
	bool OvrArchiveProjectManager::CreateProject(const OvrString& a_file_full_path) 
	{
		OvrArchiveProjectBuilder builder;
		return builder.SaveEmpty(a_file_full_path);
	}

	bool OvrArchiveProjectManager::SaveProject(ovr_project::OvrProject* a_project, const OvrString& a_file_path)
	{
		OvrArchiveProjectBuilder builder;
		return builder.Save(a_project, a_file_path);
	}
	bool OvrArchiveProjectManager::LoadProject(ovr_project::OvrProject* a_project, const OvrString& a_file_path)
	{
		OvrArchiveProjectBuilder builder;
		return builder.Load(a_project, a_file_path);
	}

	bool OvrArchiveProjectManager::CreateScene(const OvrString& a_full_path, const OvrString& a_unique_name) 
	{
		OvrArchiveSceneBuilder builder;
		return builder.Create(a_full_path, a_unique_name);
	}

	bool OvrArchiveProjectManager::SaveScene(ovr_project::OvrSceneEditor* a_scene)
	{
		OvrArchiveSceneBuilder builder;
		return builder.Save(a_scene);
	}

	ovr_core::OvrSceneContext* OvrArchiveProjectManager::LoadScene(const OvrString& a_scene_file_path)
	{
		OvrArchiveSceneBuilder builder;
		return builder.Load(a_scene_file_path);
	}

	bool OvrArchiveProjectManager::AddSequenceToScene(const OvrString& a_scene_full_path, const OvrString& a_sequence_unique_name) 
	{
		OvrArchiveSceneBuilder builder;
		return builder.AddSequenceToScene(a_scene_full_path, a_sequence_unique_name);
	}

	bool OvrArchiveProjectManager::CreateSequence(const OvrString& a_full_path, const OvrString& a_scene_unique_name, const OvrString& a_unique_name)
	{
		OvrArchiveSequenceBuilder builder;
		return builder.Create(a_full_path, a_scene_unique_name, a_unique_name);
	}

	bool OvrArchiveProjectManager::SaveSequence(ovr_project::OvrTimelineEditor* a_sequence)
	{
		OvrArchiveSequenceBuilder builder;
		return builder.Save(a_sequence);
	}
	bool OvrArchiveProjectManager::LoadSequence(ovr_project::OvrTimelineEditor* a_sequence)
	{
		OvrArchiveSequenceBuilder builder;
		return builder.Load(a_sequence);
	}
}
