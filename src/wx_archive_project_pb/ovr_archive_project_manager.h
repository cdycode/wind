#ifndef OVR_ARCHIVE_PROJECT_BP_OVR_ARCHIVE_PROJECT_MANAGER_H_
#define OVR_ARCHIVE_PROJECT_BP_OVR_ARCHIVE_PROJECT_MANAGER_H_

//OVR_ARCHIVE_PROJECT_API
#ifdef _WIN32
#   ifdef WXARCHIVEPROJECTPB_EXPORTS
#	    define OVR_ARCHIVE_PROJECT_API __declspec(dllexport)
#   else
#	    define OVR_ARCHIVE_PROJECT_API __declspec(dllimport)
#   endif // OVRARCHIVEPROJECT_EXPORTS
#else
#	define OVR_ARCHIVE_PROJECT_API
#endif

#include <wx_plugin/wx_i_archive_project.h>

namespace ovr_archive_project 
{
	class OvrArchiveProjectManager : public wx_plugin::wxIArchiveProject 
	{
	public:
		OvrArchiveProjectManager() { display_name = "archive_probtobuff"; }
		virtual ~OvrArchiveProjectManager() {}

		virtual bool		CreateProject(const OvrString& a_file_full_path) override;
		virtual bool		SaveProject(ovr_project::OvrProject* a_project, const OvrString& a_file_path) override;
		virtual bool		LoadProject(ovr_project::OvrProject* a_project, const OvrString& a_file_path) override;

		virtual bool							CreateScene(const OvrString& a_full_path, const OvrString& a_unique_name) override;
		virtual bool							SaveScene(ovr_project::OvrSceneEditor* a_scene) override;
		virtual ovr_core::OvrSceneContext*	LoadScene(const OvrString& a_scene_file_path) override;

		virtual bool	AddSequenceToScene(const OvrString& a_scene_full_path, const OvrString& a_sequence_unique_name) override;

		virtual bool	CreateSequence(const OvrString& a_full_path, const OvrString& a_scene_unique_name, const OvrString& a_unique_name) override;
		virtual bool	SaveSequence(ovr_project::OvrTimelineEditor* a_sequence) override;
		virtual bool	LoadSequence(ovr_project::OvrTimelineEditor* a_sequence) override;
	};
}

#if defined(__cplusplus)
extern "C"
{
#endif
	OVR_ARCHIVE_PROJECT_API  wx_plugin::wxIPlugin* StartPlugin();
	OVR_ARCHIVE_PROJECT_API  void  StopPlugin();
#if defined(__cplusplus)
}
#endif

#endif