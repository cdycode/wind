﻿#ifndef OVR_PROJECT_OVR_ARCHIVE_SEQUENCE_BUILDER_H_
#define OVR_PROJECT_OVR_ARCHIVE_SEQUENCE_BUILDER_H_


namespace ovr_core
{
	class OvrSequence;
	class OvrTrack;
	class OvrTrack;
	class OvrTransformTrack;
	class OvrSkeletalAnimationTrack;
	class OvrMorphAnimationTrack;
	class OvrVideoTrack;
}

namespace ovr_project
{
	class OvrTimelineEditor;
};

namespace pb 
{
	class Sequence;
	class Tree;
	class TrackInfo;
}

namespace ovr_archive_project
{
	class OvrArchiveTrack;
	class OvrArchiveActorTrack;
	class OvrArchiveTransformTrack;
	class OvrArchiveSkeletalAnimationTrack;
	class OvrArchiveFaceAnimationTrack;
	class OvrArchiveCaptureTrack;
	class OvrArchiveAudioTrack;
	class OvrArchiveVideoTrack;
	
	class OvrArchiveClipTrack;
	class OvrArchiveParentTack;
	class OvrArchivePointTrack;
	class OvrArchiveTransparentTrack;

	class OvrArchiveSequenceBuilder 
	{
	public:
		OvrArchiveSequenceBuilder() {}
		~OvrArchiveSequenceBuilder() {}

		bool	Save(ovr_project::OvrTimelineEditor* a_sequence);
		bool	Load(ovr_project::OvrTimelineEditor* a_sequence);
		bool	Create(const OvrString& a_full_path, const OvrString& a_scene_unique_name, const OvrString& a_unique_name);

	private:
		
		bool	SaveTrack(pb::Sequence& a_archive_sequence, pb::Tree* a_parent_tree, ovr_core::OvrTrack* a_track);
		void	SaveActorTrack(ovr_core::OvrTrack* a_track, std::string& a_out_stream);
		void	SaveMarkTrack(ovr_core::OvrTrack* a_track, std::string& a_out_stream);
		void	SaveEventTrack(ovr_core::OvrTrack* a_track, std::string& a_out_stream);

		void	SaveSubTrack(pb::Sequence& a_archive_sequence, pb::Tree* a_parent_tree, ovr_core::OvrTrack* a_parent_track);
		void	SaveClipTrack(ovr_core::OvrTrack* a_track, std::string& a_out_stream);
		void	SaveTransparentTrack(ovr_core::OvrTrack* a_track, std::string& a_out_stream);

		bool	LoadTrack(ovr_core::OvrSequence* a_sequence, const pb::Sequence& a_archive_sequence, const pb::Tree& a_parent_tree);
		ovr_core::OvrTrack*	LoadActorTrack(ovr_project::OvrTimelineEditor* a_sequence, const ::pb::TrackInfo& a_track_info);
		ovr_core::OvrTrack*	LoadMarkTrack(ovr_core::OvrSequence* a_sequence, const ::pb::TrackInfo& a_track_info);
		ovr_core::OvrTrack*	LoadEventTrack(ovr_core::OvrSequence* a_sequence, const ::pb::TrackInfo& a_track_info);
		ovr_core::OvrTrack*	LoadInteractiveTrack(ovr_core::OvrSequence* a_sequence, const ::pb::TrackInfo& a_track_info);

		bool	LoadSubTrack(ovr_core::OvrTrack* a_parent_track, const pb::Sequence& a_archive_sequence, const pb::Tree& a_parent_tree);
		bool	LoadClipTrack(ovr_core::OvrTrack* a_track, const pb::TrackInfo& a_track_info);
		bool	LoadTransparentTrack(ovr_core::OvrTrack* a_track, const pb::TrackInfo& a_track_info);

	};

}

#endif
