@ECHO OFF

echo compile common.proto
start "" protoc -I=%~dp0 --cpp_out=%~dp0\..\pb %~dp0\common.proto

echo compile project.proto
start "" protoc -I=%~dp0 --cpp_out=%~dp0\..\pb %~dp0\project.proto


echo compile actor.proto
start "" protoc -I=%~dp0 --cpp_out=%~dp0\..\pb %~dp0\actor.proto
echo compile scene.proto
start "" protoc -I=%~dp0 --cpp_out=%~dp0\..\pb %~dp0\scene.proto

echo compile track_clip.proto
start "" protoc -I=%~dp0 --cpp_out=%~dp0\..\pb %~dp0\track_clip.proto
echo compile track.proto
start "" protoc -I=%~dp0 --cpp_out=%~dp0\..\pb %~dp0\track.proto
echo compile sequence.proto
start "" protoc -I=%~dp0 --cpp_out=%~dp0\..\pb %~dp0\sequence.proto
echo compile finished
pause
