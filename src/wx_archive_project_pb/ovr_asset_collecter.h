﻿#ifndef OVR_ARCHIVE_PROJECT_PB_OVR_ASSET_COLLECTER_H_
#define OVR_ARCHIVE_PROJECT_PB_OVR_ASSET_COLLECTER_H_

namespace ovr_core
{
	class OvrSequence;
	class OvrTrack;
	class OvrSceneContext;
}

namespace ovr_engine 
{
	class OvrMaterial;
}

namespace ovr_archive_project 
{
	class OvrAssetCollecter 
	{
	public:
		OvrAssetCollecter() {}
		~OvrAssetCollecter() {}

		//收集sequence中使用到的资源
		void	CollectSequenceAsset(ovr_core::OvrSequence* a_sequence);
		void	CollectTrack(ovr_core::OvrTrack* a_track);

		//收集使用到的资源
		void	CollectSceneAsset(ovr_core::OvrSceneContext* a_scene);

		//保存资源  用于添加本场景中引用的所有资源
		void	AddAssetFile(const OvrString& a_asset_file_path);

	private:
		void	CollectMaterialAsset(ovr_engine::OvrMaterial* a_material);
	public:

		std::map<OvrString, bool>	asset_file_map;		//保存场景中使用的资源
	};
}

#endif
