#ifndef OVR_ARCHIVE_PROJECT_HEADERS_H_
#define OVR_ARCHIVE_PROJECT_HEADERS_H_

#pragma warning(disable:4146)
#pragma warning(disable:4800)

#include <vector>
#include <map>
#include <memory>
#include <functional>
#include <fstream>
#include <assert.h>

#include <wx_base/wx_archive.h>
#include <wx_base/wx_small_file.h>
#include <wx_base/wx_log.h>

//#include <ovr_engine/ovr_light.h>

#include <google/protobuf/stubs/common.h>

#include "ovr_archive_define.h"



#endif