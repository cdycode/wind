﻿#include "headers.h"
#include "ovr_archive_define.h"
#include "pb/common.pb.h"

namespace ovr_archive_project 
{
	bool	SetPBVector3(pb::Vector3* a_pb_vector3, const OvrVector3& a_vector3) 
	{
		a_pb_vector3->set_x(a_vector3[0]);
		a_pb_vector3->set_y(a_vector3[1]);
		a_pb_vector3->set_z(a_vector3[2]);
		return true;
	}

	void	SetOvrVector3(OvrVector3& a_vector3, const pb::Vector3& a_pb_vector3) 
	{
		a_vector3[0] = a_pb_vector3.x();
		a_vector3[1] = a_pb_vector3.y();
		a_vector3[2] = a_pb_vector3.z();
	}

	bool	SetPBVector4(pb::Vector4* a_pb_vector4, const OvrVector4& a_vector4) 
	{
		a_pb_vector4->set_x(a_vector4[0]);
		a_pb_vector4->set_y(a_vector4[1]);
		a_pb_vector4->set_z(a_vector4[2]);
		a_pb_vector4->set_w(a_vector4[3]);
		return true;
	}
	void	SetOvrVector4(OvrVector4& a_vector4, const pb::Vector4& a_pb_vector4) 
	{
		a_vector4[0] = a_pb_vector4.x();
		a_vector4[1] = a_pb_vector4.y();
		a_vector4[2] = a_pb_vector4.z();
		a_vector4[3] = a_pb_vector4.w();
	}

	bool	SetPBTransform(pb::Transform* a_pb_transform, const OvrTransform& a_transform) 
	{
		pb::Vector3* position = a_pb_transform->mutable_position();
		pb::Vector3* scale = a_pb_transform->mutable_scale();
		pb::Vector4* quaternion = a_pb_transform->mutable_quaternion();

		SetPBVector3(position, a_transform.position);
		SetPBVector3(scale, a_transform.scale);

		quaternion->set_x(a_transform.quaternion[0]);
		quaternion->set_y(a_transform.quaternion[1]);
		quaternion->set_z(a_transform.quaternion[2]);
		quaternion->set_w(a_transform.quaternion[3]);

		return true;
	}

	void	SetOvrTransform(OvrTransform& a_transform, const pb::Transform& a_pb_transform) 
	{

		SetOvrVector3(a_transform.position, a_pb_transform.position());
		SetOvrVector3(a_transform.scale, a_pb_transform.scale());

		a_transform.quaternion[0] = a_pb_transform.quaternion().x();
		a_transform.quaternion[1] = a_pb_transform.quaternion().y();
		a_transform.quaternion[2] = a_pb_transform.quaternion().z();
		a_transform.quaternion[3] = a_pb_transform.quaternion().w();
	}
}