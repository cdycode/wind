﻿#ifndef OVR_PROJECT_OVR_ARCHIVE_SCENE_BUILDER_H_
#define OVR_PROJECT_OVR_ARCHIVE_SCENE_BUILDER_H_

#include <map>

namespace ovr_engine 
{
	class OvrActor;
	class OvrComponent;
}

namespace ovr_core
{
	class OvrSceneContext;
}

namespace ovr_project 
{
	class OvrSceneEditor;
}

namespace pb 
{
	class Scene;
	class Tree;
	class ActorInfo;
}

namespace ovr_archive_project 
{
	class OvrArchiveSceneBuilder
	{
	public:
		OvrArchiveSceneBuilder() {}
		~OvrArchiveSceneBuilder() {}

		bool							Save(ovr_project::OvrSceneEditor* a_scene);
		ovr_core::OvrSceneContext*		Load(const OvrString& a_scene_file_path);
		bool							Create(const OvrString& a_full_path, const OvrString& a_unique_name);

		//添加Sequence到场景文件中
		bool				AddSequenceToScene(const OvrString& a_scene_full_path, const OvrString& a_sequence_unique_name);

	private:
		void	SaveChildActor(ovr_project::OvrSceneEditor* a_scene, pb::Scene& a_archive_scene, \
								ovr_engine::OvrActor* a_parent_actor, pb::Tree* a_parent_tree);
		void	LoadChildActor(ovr_core::OvrSceneContext* a_scene, pb::Scene& a_archive_scene, \
								ovr_engine::OvrActor* a_parent_actor, const pb::Tree& a_parent_tree);

		bool	SaveActor(pb::Scene& a_archive_scene, ovr_engine::OvrActor* a_actor);
		void	SaveColliderBoxComponent(ovr_engine::OvrComponent* a_component, std::string& a_out_stream);
		void	SaveMeshComponent(ovr_engine::OvrComponent* a_component, std::string& a_out_stream);
		void	SaveTextComponent(ovr_engine::OvrComponent* a_component, std::string& a_out_stream);
		void	SaveCameraComponent(ovr_engine::OvrComponent* a_component, std::string& a_out_stream);
		void	SaveAudioComponent(ovr_engine::OvrComponent* a_component, std::string& a_out_stream);
		void	SaveVideoComponent(ovr_engine::OvrComponent* a_component, std::string& a_out_stream);
		void	SaveLightComponent(ovr_engine::OvrComponent* a_component, std::string& a_out_stream);

		ovr_engine::OvrActor*	LoadActor(ovr_core::OvrSceneContext* a_scene_context, const pb::ActorInfo& a_actor_info);
		bool	LoadColliderBoxComponent(ovr_engine::OvrComponent* a_component, const std::string& a_serialize_stream);
		bool	LoadMeshComponent(ovr_engine::OvrComponent* a_component, const std::string& a_serialize_stream);
		bool	LoadTextComponent(ovr_engine::OvrComponent* a_component, const std::string& a_serialize_stream);
		bool	LoadCameraComponent(ovr_engine::OvrComponent* a_component, const std::string& a_serialize_stream);

		bool	LoadAudioComponent(ovr_engine::OvrComponent* a_component, const std::string& a_serialize_stream);
		bool	LoadVideoComponent(ovr_engine::OvrComponent* a_component, const std::string& a_serialize_stream);
		bool	LoadLightComponent(ovr_engine::OvrComponent* a_component, const std::string& a_serialize_stream);
	};
}

#endif
