﻿#include "rapidjson/stringbuffer.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/document.h"
#include "headers.h"
#include "ovr_raw_model.h"
#include "image/lvr_image_manager.h"

ovr::int32	OvrSurfaceVertex::AddVertex(const ovr_asset::OvrVertex& a_vertex)
{
	int index = -1;
	auto vertex_it = vertex_hash_map.find(a_vertex);
	if (vertex_it != vertex_hash_map.end())
	{
		return vertex_it->second;
	}

	vertex_hash_map[a_vertex] = vertices.size();
	vertices.push_back(a_vertex);
	return vertices.size() - 1;
}

void OvrSurfaceVertex::FillSurface(ovr_asset::OvrArchiveSurface* a_surface)
{
	CalcCenterPosition();

	a_surface->SetVertexNum(vertices.size());
	for (ovr::uint32 j = 0; j < vertices.size(); j++)
	{
		a_surface->GetVertex(j) = vertices[j];
	}

	ovr::uint32 _triangles_count = triangles.size();
	a_surface->SetTriangleNum(_triangles_count);
	for (ovr::uint32 j = 0; j < _triangles_count; j++)
	{
		a_surface->GetTriangle(j) = triangles[j];
	}
	
	a_surface->SetCenterPosition(center_position);
}

void OvrSurfaceVertex::CalcCenterPosition()
{
	//计算包围盒
	OvrBox3f box_mesh;
	for (auto vertex : vertices)
	{
		box_mesh.expand(vertex.position);
	}
	//获得中心点
	center_position = box_mesh.get_center();
}


OvrRawModel::OvrRawModel() 
{
}

OvrRawModel::~OvrRawModel() 
{
}

void OvrRawModel::AddTextureFilePath(const char* a_file_path)
{
	auto file_it = texture_file_path.find(a_file_path);
	if (file_it == texture_file_path.end())
	{
		texture_file_path[a_file_path] = OvrString();
	}
}

void OvrRawModel::Reset()
{
	material_vector.clear();
	texture_file_path.clear();
	joint_list.clear();
	fbx_node_list.clear();
	if (motion_frame_list.size() > 0)
	{
		std::list<ovr_asset::OvrMotionFrame*>::iterator it = motion_frame_list.begin();
		for (; it != motion_frame_list.end(); it++)
		{
			delete *it;
		}
		motion_frame_list.clear();
	}

	if (morph_frame_list.size() > 0)
	{
		std::list<ovr_asset::OvrMorphFrame*>::iterator it = morph_frame_list.begin();
		for (; it != morph_frame_list.end(); it++)
		{
			delete *it;
		}
		morph_frame_list.clear();
	}

	if (nullptr != mesh)
	{
		delete mesh;
		mesh = nullptr;
	}

	pose_list.clear();
	fbx_mesh_list.clear();
}

bool OvrRawModel::InitMesh(const char* a_fbx_file, const char* a_out_dir)
{
	OvrString output_dir(a_out_dir);
	OvrString fbx_file_name;
	OvrFileTools::GetFileNameFromPath(a_fbx_file, fbx_file_name);

	ovr_asset::OvrArchiveAssetManager* asset_manager = ovr_asset::OvrArchiveAssetManager::GetIns();

	OvrString mesh_path = output_dir + fbx_file_name + OvrString(".mesh");
	mesh = (ovr_asset::OvrArchiveMesh*)asset_manager->CreateAsset(ovr_asset::kAssetMesh, mesh_path.GetStringConst());

	return nullptr != mesh;
}

ovr::uint32	OvrRawModel::AddMaterial(const OvrMaterial& a_material)
{
	ovr::uint32 index = 0;
	for (auto material : material_vector)
	{
		if (material == a_material)
		{
			return index;
		}
		index++;
	}
	material_vector.push_back(a_material);
	return material_vector.size() - 1;
}

void OvrRawModel::AddJoint(const ovr_asset::OvrArchiveJoint& a_joint, FbxNode* a_node)
{
	joint_list.push_back(a_joint);
	fbx_node_list.push_back(a_node);
}

void OvrRawModel::SetJointDefaultPose(const OvrString& a_name, const OvrMatrix4& a_pose)
{
	ovr_asset::OvrArchiveJoint* joint = GetJoint(a_name.GetStringConst());
	if (nullptr != joint)
	{
		joint->default_pose = a_pose;
	}
}

ovr::int32 OvrRawModel::FindJointIndexUsingName(const char* a_name)
{
	ovr::int32 found_index = -1;
	int index = 0;
	for (auto joint : joint_list)
	{
		if (joint.name == a_name)
		{
			found_index = index;
			break;
		}
		index++;
	}
	return found_index;
}

ovr_asset::OvrArchiveJoint*	OvrRawModel::GetJoint(ovr::uint32 a_index)
{
	auto joint_it = joint_list.begin();
	while (joint_it != joint_list.end())
	{
		if (a_index == 0)
		{
			return &(*joint_it);
		}
		a_index--;
		joint_it++;
	}
	return nullptr;
}

ovr_asset::OvrArchiveJoint* OvrRawModel::GetJoint(const char* a_name) 
{
	auto joint_it = joint_list.begin();
	while (joint_it != joint_list.end())
	{
		if (joint_it->name == a_name)
		{
			return &(*joint_it);
		}
		joint_it++;
	}
	return nullptr;
}

FbxNode* OvrRawModel::GetSkeletonNode(ovr::uint32 a_index)
{
	auto node_it = fbx_node_list.begin();
	while (node_it != fbx_node_list.end())
	{
		if (a_index == 0)
		{
			return *node_it;
		}
		a_index--;
		node_it++;
	}
	return nullptr;
}

void OvrRawModel::Save(const char* a_fbx_file, const char* a_out_dir, std::list<OvrString>&	a_out_file_list)
{
	OvrString output_dir(a_out_dir);
	OvrString fbx_file_name;
	OvrFileTools::GetFileNameFromPath(a_fbx_file, fbx_file_name);

	ovr_asset::OvrArchiveAssetManager* asset_manager = ovr_asset::OvrArchiveAssetManager::GetIns();

	//保存图片
	auto texture_it = texture_file_path.begin();
	while (texture_it != texture_file_path.end())
	{
		OvrString file_name;
		OvrFileTools::GetFileNameFromPath(texture_it->first.c_str(), file_name);
		OvrString file_path = output_dir + file_name + OvrString(".tex");
		texture_it->second = file_path;

		a_out_file_list.push_back(file_path);
		SaveTexture(texture_it->first.c_str(), file_path.GetStringConst(), file_name.GetStringConst());

		texture_it++;
	}

	//保存材质
	auto material_it = material_vector.begin(); 
	while (material_it != material_vector.end())
	{
		material_it->save_path = output_dir + material_it->name + OvrString(".mat");
		a_out_file_list.push_back(material_it->save_path);
		SaveMaterial(*material_it);
		material_it++;
	}

	OvrString skeleton_path;
	//save骨骼
	if (joint_list.size() > 0)
	{
		skeleton_path = output_dir + fbx_file_name + OvrString(".skeleton");
		ovr_asset::OvrArchiveSkeleton* new_skeleton = (ovr_asset::OvrArchiveSkeleton*)asset_manager->CreateAsset(ovr_asset::kAssetSkeleton, skeleton_path.GetStringConst());
		a_out_file_list.push_back(skeleton_path);
		new_skeleton->SetJointNum(joint_list.size());
		int index = 0;
		for (auto joint : joint_list)
		{
			new_skeleton->GetJoint(index++) = joint;
		}
		new_skeleton->Save();
	}
	OvrString pose_path;
	//face
	if (pose_list.size() > 0)
	{
		pose_path = output_dir + fbx_file_name + OvrString(".pose");
		ovr_asset::OvrArchivePoseContainer* new_pose_container = (ovr_asset::OvrArchivePoseContainer*)asset_manager->CreateAsset(ovr_asset::kAssetPose, pose_path.GetStringConst());
		a_out_file_list.push_back(pose_path);
		new_pose_container->SetPoseNum(pose_list.size());
		int index = 0;
		for (auto pose : pose_list)
		{
			new_pose_container->GetPose(index++) = pose;
		}
		new_pose_container->Save();
	}

	//face
	//保存Mesh
	/*OvrString mesh_path = output_dir + fbx_file_name + OvrString(".mesh");
	ovr_asset::OvrArchiveMesh* new_mesh = (ovr_asset::OvrArchiveMesh*)asset_manager->CreateAsset(ovr_asset::kAssetMesh, mesh_path.GetStringConst());
	for (auto surface : surface_vector)
	{
		surface->FillSurface(new_mesh, material_vector);
	}*/
	a_out_file_list.push_back(mesh->GetFilePath());
	mesh->SetSkeletonPath(skeleton_path.GetStringConst());
	mesh->SetPosePath(pose_path.GetStringConst());
	mesh->Save();

	//动画
	if (motion_frame_list.size() > 0)
	{
		OvrString animation_path = output_dir + fbx_file_name + OvrString(".ani");
		ovr_asset::OvrArchiveSkeletalAnimation* new_animation = (ovr_asset::OvrArchiveSkeletalAnimation*)asset_manager->CreateAsset(ovr_asset::kAssetSkeletalAnimation, animation_path.GetStringConst());
		a_out_file_list.push_back(animation_path);
		new_animation->SetDuration(duration);
		for (auto joint : joint_list)
		{
			new_animation->AddJointName(joint.name);
		}
		auto frame = motion_frame_list.begin();
		while (frame != motion_frame_list.end())
		{
			new_animation->AddFrame(*frame);
			delete *frame;
			frame++;
		}
		motion_frame_list.clear();
		new_animation->Save();
	}

	if (morph_frame_list.size() > 0)
	{
		OvrString animation_path = output_dir + fbx_file_name + OvrString(".pani");
		ovr_asset::OvrArchivePoseAnimation* new_animation = (ovr_asset::OvrArchivePoseAnimation*)asset_manager->CreateAsset(ovr_asset::kAssetPoseAnimation, animation_path.GetStringConst());
		a_out_file_list.push_back(animation_path);
		new_animation->SetDuration(duration);
		auto frame = morph_frame_list.begin();
		while (frame != morph_frame_list.end())
		{
			new_animation->AddFrame(*frame);
			delete *frame;
			frame++;
		}
		morph_frame_list.clear();
		new_animation->Save();
	}

}

void OvrRawModel::SaveMaterial(const OvrMaterial& a_mat)
{
	OvrString full_file_path = OvrString(ovr_asset::OvrArchiveAssetManager::GetIns()->GetRootDir()) + OvrString(a_mat.save_path);
	FILE *fp = nullptr;
	fp = fopen(full_file_path.GetStringConst(), "w");
	if (!fp)
	{
		return;
	}
	RAPIDJSON_NAMESPACE::StringBuffer str;
	RAPIDJSON_NAMESPACE::PrettyWriter<RAPIDJSON_NAMESPACE::StringBuffer> writer(str);
	writer.StartObject();

	if (a_mat.type == kMaterialTransparent)
	{
		writer.Key("SceneBlend");
		writer.StartObject();
		writer.Key("SrcFactor");
		writer.String("src_alpha");
		writer.Key("DestFactor");
		writer.String("one_minus_src_alpha");
		writer.EndObject();
	}

	writer.Key("Shader");
	writer.StartObject();
	writer.Key("Name");
	writer.String(GetShaderName(a_mat).GetStringConst());
	writer.Key("Constant");
	writer.StartArray();
	writer.StartObject();
	writer.Key("Name");
	writer.String("Transparent");
	writer.Key("Type");
	writer.String("Float");
	writer.Key("Value");
	writer.Double(1.0);
	writer.EndObject();
	for (ovr::uint32 i = 0; i < a_mat.raw_texture.size(); i++)
	{
		OvrString uniform_name;
		if (GetTextureUniformName(a_mat.raw_texture[i].usage, uniform_name))
		{
			writer.StartObject();
			writer.Key("Name");
			writer.String(uniform_name.GetStringConst());
			writer.Key("Type");
			writer.String("Sample2D");
			writer.Key("Value");
			auto file_it = texture_file_path.find(a_mat.raw_texture[i].name.GetStringConst());
			if (file_it != texture_file_path.end())
			{
				writer.String(file_it->second.GetStringConst());
			}
			else
			{
				writer.String("null");
			}
			writer.EndObject();
		}
	}
	writer.EndArray();
	writer.EndObject();
	writer.EndObject();
	fwrite(str.GetString(), str.GetSize(), 1, fp);
	fclose(fp);
	return;
}

OvrString OvrRawModel::GetShaderName(const OvrMaterial& a_mat)
{
	OvrString shader_name;
	if (a_mat.is_use_skeleton)
	{
		if (a_mat.is_normal_map)
			shader_name = "StandardSkinnedWithNM";
		else
			shader_name = "StandardSkinned";
	}
	else
	{
		if (a_mat.is_normal_map)
			shader_name = "StandardWithNM";
		else
			shader_name = "Standard";
		
	}
	return shader_name;
}

bool OvrRawModel::GetTextureUniformName(ovr_asset::OvrTextureUsage a_usage, OvrString& a_name)
{
	bool ret = true;
	switch (a_usage)
	{
	case ovr_asset::kTextureUsageDiffuse:
		a_name = "Texture0";
		break;
	case ovr_asset::kTextureUsageEmissive:
		a_name = "Texture1";
		break;
	case ovr_asset::kTextureUsageNormal:
		a_name = "Texture2";
		break;
	case ovr_asset::kTextureUsageSpecular:
		a_name = "Texture3";
		break;
	default:
		ret = false;
		break;
	}
	return ret;
}

void OvrRawModel::SaveTexture(const char* a_file_path, const char* a_file_name, const char* a_name)
{
	lvr_image_info image_info;
	lvr_image_manager image_manager;
	if (lvr_image_manager::e_no_error != image_manager.load_pic_from_path((const int8_t*)a_file_path, (const int8_t*)a_name, image_info))
	{
		return;
	}
	lvr_pixels pixels = image_manager.get_pixel_data((const int8_t*)a_name);

	ovr_asset::OvrPixelFormat pixel_format = ovr_asset::kPixelInvalid;
	switch (pixels.pixel_type)
	{
	case lvr_e_pixel_rgb:
		pixel_format = ovr_asset::kPixelRGB;
		break;
	case lvr_e_pixel_rgba:
		pixel_format = ovr_asset::kPixelRGBA;
		break;
	case lvr_e_pixel_a8:
		break;
	case lvr_e_pixel_r32:
		break;
	default:
		break;
	}
	if (pixel_format == ovr_asset::kPixelInvalid)
	{
		return;
	}
	ovr_asset::OvrArchiveTexture* new_texture = (ovr_asset::OvrArchiveTexture*)ovr_asset::OvrArchiveAssetManager::GetIns()->CreateAsset(ovr_asset::kAssetTexture, a_file_name);

	new_texture->SetDisplayName(a_name);
	new_texture->SetTextureInfo(pixel_format, pixels.width, pixels.height);
	new_texture->SetImagePixels((const char*)pixels.pixels, pixels.buf_size);
	new_texture->Save();
	delete new_texture;
	//if (image_info.gl_format)
	{
	}

	//new_texture->SetTextureInfo()
}
