#ifndef FBX_CONVERT_HEADERS_H_
#define FBX_CONVERT_HEADERS_H_

#include <fbxsdk.h>

#include <unordered_map>

#include <wx_base/wx_string.h>
#include <wx_base/wx_array.h>
#include <wx_base/wx_file_tools.h>
#include <wx_base/wx_box3f.h>

#include <wx_asset_manager/ovr_archive_asset_manager.h>
#include <wx_asset_manager/ovr_archive_mesh.h>
#include <wx_asset_manager/ovr_archive_skeleton.h>
#include <wx_asset_manager/ovr_archive_skeletal_animation.h>
#include <wx_asset_manager/ovr_archive_material.h>
#include <wx_asset_manager/ovr_archive_texture.h>
#include <wx_asset_manager/ovr_archive_pose.h>
#include <wx_asset_manager/ovr_motion_frame.h>

#endif
