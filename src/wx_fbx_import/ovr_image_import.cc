﻿#include "headers.h"
#include <wx_fbx_import/ovr_image_import.h>
#include "image/lvr_image_manager.h"

bool OvrImageImporter::Import(const char* a_file, const char* a_out_dir, OvrString& a_out_file)
{
	OvrString output_dir(a_out_dir);
	OvrString file_name;
	OvrFileTools::GetFileNameFromPath(a_file, file_name);
	a_out_file = output_dir + file_name + OvrString(".tex");

	lvr_image_info image_info;
	lvr_image_manager image_manager;
	if (lvr_image_manager::e_no_error != image_manager.load_pic_from_path((const int8_t*)a_file, (const int8_t*)file_name.GetStringConst(), image_info))
	{
		return false;
	}
	lvr_pixels pixels = image_manager.get_pixel_data((const int8_t*)file_name.GetStringConst());

	ovr_asset::OvrPixelFormat pixel_format = ovr_asset::kPixelInvalid;
	switch (pixels.pixel_type)
	{
	case lvr_e_pixel_rgb:
		pixel_format = ovr_asset::kPixelRGB;
		break;
	case lvr_e_pixel_rgba:
		pixel_format = ovr_asset::kPixelRGBA;
		break;
	case lvr_e_pixel_a8:
		break;
	case lvr_e_pixel_r32:
		break;
	default:
		break;
	}
	if (pixel_format == ovr_asset::kPixelInvalid)
	{
		return false;
	}
	ovr_asset::OvrArchiveTexture* new_texture = (ovr_asset::OvrArchiveTexture*)ovr_asset::OvrArchiveAssetManager::GetIns()->CreateAsset(ovr_asset::kAssetTexture, a_out_file.GetStringConst());

	new_texture->SetDisplayName(file_name.GetStringConst());
	new_texture->SetTextureInfo(pixel_format, pixels.width, pixels.height);
	new_texture->SetImagePixels((const char*)pixels.pixels, pixels.buf_size);
	new_texture->Save();
	delete new_texture;
	return true;
}
void OvrImageImporter::SaveTextureFromLoad(const char* a_loadfilepath, const char*a_loadfilename,const char* a_savefile, bool a_balpha, int a_width, int a_height)
{
	FILE* f = fopen(a_loadfilepath, "rb");
	if (f == NULL)
		return;
	fseek(f, 0, SEEK_END); //定位到文件末 
	long nFileLen = ftell(f); //文件长度
	char* data = new char[nFileLen];
	fseek(f,0,SEEK_SET);
	long nRead = 1;
	long nReadCount = 0;
	while (nRead > 0)
	{
		nRead = fread(data + nReadCount, 1, 1024, f);
		nReadCount += nRead;
	}
	fclose(f);
	ovr_asset::OvrArchiveTexture* new_texture = (ovr_asset::OvrArchiveTexture*)ovr_asset::OvrArchiveAssetManager::GetIns()->CreateAsset(ovr_asset::kAssetTexture, a_savefile);

	new_texture->SetDisplayName(a_loadfilename);
	new_texture->SetTextureInfo(a_balpha? ovr_asset::OvrPixelFormat::kPixelRGBA: ovr_asset::OvrPixelFormat::kPixelRGB, a_width, a_height);
	new_texture->SetImageKtxPixels(data, nFileLen);
	new_texture->Save();
	new_texture->ReleaseBuffer();
	
}
