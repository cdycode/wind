﻿#include "headers.h"

#include <wx_fbx_import/ovr_fbx_import.h>
#include "ovr_fbx_parser.h"
//static ovr::uint32 import_fbx_percent = 0;
//#ifdef __cplusplus
//extern "C"
//{
//#endif
//	void ProcessCallback(ovr::uint32 percent)
//	{
//		import_fbx_percent = percent;
//	}
//
//	bool OvrImportFbx(const char* a_fbx_file, const char* a_out_dir, std::list<OvrString>&	a_out_file_list)
//	{
//		OvrFbxParser temp;
//		if (temp.Init(a_fbx_file, a_out_dir, ProcessCallback))
//		{
//			temp.Save(a_out_dir, a_out_file_list);
//			return true;
//		}
//		return false;
//	}
//
//	ovr::uint32 GetPercent()
//	{
//		return import_fbx_percent;
//	}
//#ifdef __cplusplus
//};
//#endif

OvrFbxImporter::OvrFbxImporter()
{
	parser = new OvrFbxParser;
	is_import = false;
}

OvrFbxImporter::~OvrFbxImporter()
{
	delete parser;
}

bool OvrFbxImporter::Import(const char* a_fbx_file, const char* a_out_dir, std::list<OvrString>& a_out_file_list)
{
	bool result = false;

	if (is_import)
	{
		return result;
	}

	is_import = true;
	if (parser->Init(a_fbx_file, a_out_dir))
	{
		parser->Save(a_out_dir, a_out_file_list);
		result = true;
	}

	parser->Unint();
	is_import = false;
	return result;
}

ovr::uint32 OvrFbxImporter::GetPercent()
{	
	return parser->GetPercent();
}

void OvrFbxImporter::CancelImport()
{
	if(is_import)
		parser->Stop();
}
