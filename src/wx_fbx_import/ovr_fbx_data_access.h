﻿#ifndef OVR_FBX_IMPORT_OVR_FBX_DATA_ACCESS_H_
#define OVR_FBX_IMPORT_OVR_FBX_DATA_ACCESS_H_

template< typename _type_ >
class FbxLayerElementAccess
{
public:

	FbxLayerElementAccess(const FbxLayerElementTemplate< _type_ > * layer, int count) :
		mappingMode(FbxGeometryElement::eNone),
		elements(NULL),
		indices(NULL)
	{
		if (count <= 0 || layer == NULL)
		{
			return;
		}
		const FbxGeometryElement::EMappingMode newMappingMode = layer->GetMappingMode();
		if (newMappingMode == FbxGeometryElement::eByControlPoint ||
			newMappingMode == FbxGeometryElement::eByPolygonVertex)
		{
			mappingMode = newMappingMode;
			elements = &layer->GetDirectArray();
			indices = (layer->GetReferenceMode() == FbxGeometryElement::eIndexToDirect ||
				layer->GetReferenceMode() == FbxGeometryElement::eIndex) ? &layer->GetIndexArray() : NULL;
		}
	}

	bool LayerPresent() const
	{
		return (mappingMode != FbxGeometryElement::eNone);
	}

	_type_ GetElement(const int polygonIndex, const int polygonVertexIndex, const int controlPointIndex, const _type_ defaultValue) const
	{
		if (mappingMode != FbxGeometryElement::eNone)
		{
			int index = (mappingMode == FbxGeometryElement::eByControlPoint) ? controlPointIndex :
				((mappingMode == FbxGeometryElement::eByPolygonVertex) ? polygonVertexIndex : polygonIndex);
			index = (indices != NULL) ? (*indices).GetAt(index) : index;
			_type_ element = elements->GetAt(index);
			return element;
		}
		return defaultValue;
	}

	_type_ GetElement(const int polygonIndex, const int polygonVertexIndex, const int controlPointIndex, const _type_ defaultValue, const FbxMatrix & transform, const bool normalize) const
	{
		if (mappingMode != FbxGeometryElement::eNone)
		{
			_type_ element = transform.MultNormalize(GetElement(polygonIndex, polygonVertexIndex, controlPointIndex, defaultValue));
			if (normalize)
			{
				element.Normalize();
			}
			return element;
		}
		return defaultValue;
	}

private:
	FbxGeometryElement::EMappingMode				mappingMode;
	const FbxLayerElementArrayTemplate< _type_ > *	elements;
	const FbxLayerElementArrayTemplate< int > *		indices;
};
class FbxMaterialAccess
{
public:

	FbxMaterialAccess(const FbxMesh * pMesh) :
		mappingMode(FbxGeometryElement::eNone),
		mesh(NULL),
		indices(NULL)
	{
		int material_count = pMesh->GetElementMaterialCount();
		if (material_count <= 0)
		{
			return;
		}

		const FbxGeometryElement::EMappingMode materialMappingMode = pMesh->GetElementMaterial()->GetMappingMode();
		if (materialMappingMode != FbxGeometryElement::eByPolygon && materialMappingMode != FbxGeometryElement::eAllSame)
		{
			return;
		}

		const FbxGeometryElement::EReferenceMode materialReferenceMode = pMesh->GetElementMaterial()->GetReferenceMode();
		if (materialReferenceMode != FbxGeometryElement::eIndexToDirect)
		{
			return;
		}

		mappingMode = materialMappingMode;
		mesh = pMesh;
		indices = &pMesh->GetElementMaterial()->GetIndexArray();
	}
	const FbxSurfaceMaterial * GetMaterial(const int polygonIndex) const
	{
		if (mappingMode != FbxGeometryElement::eNone)
		{
			const int materialIndex = (mappingMode == FbxGeometryElement::eByPolygon) ? polygonIndex : 0;
			const FbxSurfaceMaterial * pMaterial = mesh->GetNode()->GetSrcObject<FbxSurfaceMaterial>(indices->GetAt(materialIndex));
			return pMaterial;
		}
		return NULL;
	}

	FbxGeometryElement::EMappingMode	GetMapMode()const { return mappingMode; }
private:
	FbxGeometryElement::EMappingMode			mappingMode;
	const FbxMesh *								mesh;
	const FbxLayerElementArrayTemplate< int > * indices;
};

class FbxSkinningAccess
{
public:

	static const int MAX_WEIGHTS = 4;

	FbxSkinningAccess(const FbxMesh * pMesh, OvrRawModel& a_model)
	{
		const FbxSkin * skin = (pMesh->GetDeformerCount(FbxDeformer::eSkin) > 0) ? static_cast<const FbxSkin *>(pMesh->GetDeformer(0, FbxDeformer::eSkin)) : NULL;
		if (skin != NULL)
		{
			const int controlPointCount = pMesh->GetControlPointsCount();

			vertexJointIndices.resize(controlPointCount);
			vertexJointWeights.resize(controlPointCount);

			const int clusterCount = skin->GetClusterCount();
			for (int clusterIndex = 0; clusterIndex < clusterCount; clusterIndex++)
			{
				const FbxCluster * cluster = skin->GetCluster(clusterIndex);
				const int indexCount = cluster->GetControlPointIndicesCount();
				const int * clusterIndices = cluster->GetControlPointIndices();
				const double * clusterWeights = cluster->GetControlPointWeights();
				for (int i = 0; i < indexCount; i++)
				{
					if (clusterIndices[i] < 0 || clusterIndices[i] >= controlPointCount)
					{
						continue;
					}
					if (clusterWeights[i] <= vertexJointWeights[clusterIndices[i]][MAX_WEIGHTS - 1])
					{
						continue;
					}
					vertexJointIndices[clusterIndices[i]][MAX_WEIGHTS - 1] = clusterIndex;
					vertexJointWeights[clusterIndices[i]][MAX_WEIGHTS - 1] = (float)clusterWeights[i];
					for (int j = MAX_WEIGHTS - 1; j > 0; j--)
					{
						if (vertexJointWeights[clusterIndices[i]][j - 1] >= vertexJointWeights[clusterIndices[i]][j])
						{
							break;
						}
						std::swap(vertexJointIndices[clusterIndices[i]][j - 1], vertexJointIndices[clusterIndices[i]][j]);
						std::swap(vertexJointWeights[clusterIndices[i]][j - 1], vertexJointWeights[clusterIndices[i]][j]);
					}
				}

				const char* name2 = cluster->GetLink()->GetName();
				jointNames.push_back(OvrString(name2));

				ovr_asset::OvrArchiveJoint* joint = a_model.GetJoint(name2);
				FbxAMatrix transformLinkMatrix;
				cluster->GetTransformLinkMatrix(transformLinkMatrix);
				for (int i = 0; i < 4; i++)
				{
					for (int j = 0; j < 4; j++)
					{
						joint->bone_to_mesh_mat(i, j) = (float)(transformLinkMatrix.Inverse()).Get(i, j);
					}
				}
			}

			for (int i = 0; i < controlPointCount; i++)
			{
				vertexJointWeights[i].normalize();
			}
		}
	}

	bool IsSkinned() const
	{
		return vertexJointWeights.size() > 0;
	}

	const char * GetJointName(const int jointIndex) const
	{
		return jointNames[jointIndex].GetStringConst();
	}

	const OvrArray4i GetVertexIndices(const int controlPointIndex) const
	{
		return (vertexJointIndices.size() > 0) ? vertexJointIndices[controlPointIndex] : OvrArray4i();
	}

	const OvrVector4 GetVertexWeights(const int controlPointIndex) const
	{
		return (vertexJointWeights.size() > 0) ? vertexJointWeights[controlPointIndex] : OvrVector4(0.0f);
	}

private:
	std::vector< OvrString >	jointNames;
	//std::vector<OvrMatrix4>		joint_matrix;
	std::vector< OvrArray4i >	vertexJointIndices;
	std::vector< OvrVector4 >	vertexJointWeights;
};

//face
class FbxBlendShapeAccess
{
public:
	FbxBlendShapeAccess(FbxMesh * pMesh, OvrRawModel& a_model)
	{
		int lBlendShapeChannelCount, lTargetShapeCount;
		OvrString a = pMesh->GetNode()->GetName();
		const FbxBlendShape * blendShape = (pMesh->GetDeformerCount(FbxDeformer::eBlendShape) > 0) ? static_cast<const FbxBlendShape *>(pMesh->GetDeformer(0, FbxDeformer::eBlendShape)) : NULL;
		if (blendShape != NULL)
		{
			a_model.AddPoseMesh(pMesh);
			lBlendShapeChannelCount = blendShape->GetBlendShapeChannelCount();
			for (int lBlendShapeChannelIndex = 0; lBlendShapeChannelIndex < lBlendShapeChannelCount; ++lBlendShapeChannelIndex)
			{				
				const FbxBlendShapeChannel* lBlendShapeChannel = blendShape->GetBlendShapeChannel(lBlendShapeChannelIndex);
				lTargetShapeCount = lBlendShapeChannel->GetTargetShapeCount();
				for (int lTargetShapeIndex = 0; lTargetShapeIndex < lTargetShapeCount; ++lTargetShapeIndex)
				{
					const FbxShape* lShape = lBlendShapeChannel->GetTargetShape(lTargetShapeIndex);
					ovr_asset::OvrPose pose;
					pose.name = lShape->GetName();
					pose.surface_name = pMesh->GetName();
					int indexCount = lShape->GetControlPointIndicesCount();
					const int* pointIndexs = lShape->GetControlPointIndices();
					for (int i = 0; i < indexCount; i++)
					{
						if (pointIndexs[i] < 0 || pointIndexs[i] >= lShape->GetControlPointsCount())
							continue;
						FbxVector4 targetPos = lShape->GetControlPointAt(pointIndexs[i]);
						FbxVector4 orginPos = pMesh->GetControlPointAt(pointIndexs[i]);
						OvrVector3 offsetPos(float(targetPos[0] - orginPos[0]), float(targetPos[1] - orginPos[1]), float(targetPos[2] - orginPos[2]));
						pose.vertex_offset_map[pointIndexs[i]] = offsetPos;
					}
					pose_vector.push_back(pose);
				}	
			}
		}
	}

	bool IsBlendShaped() const
	{
		return pose_vector.size() > 0;
	}

	const ovr_asset::OvrPose* GetPose(const int index) const
	{
		return (pose_vector.size() > 0) ? &pose_vector[index] : nullptr;
	}

	int GetPoseCount() const
	{
		return pose_vector.size();
	}
private:
	std::vector< ovr_asset::OvrPose >	pose_vector;
};
//face
#endif