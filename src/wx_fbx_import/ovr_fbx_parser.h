﻿#ifndef OVR_FBX_IMPORT_OVR_READ_MESH_H_
#define OVR_FBX_IMPORT_OVR_READ_MESH_H_

#include "ovr_raw_model.h"

class fbx_export_tool;
class OvrFbxParser 
{
public:
	OvrFbxParser();
	~OvrFbxParser();

	bool		Init(const char* a_fbx_file, const char* a_out_dir);
	void		Unint();

	void		Save(const char* a_out_dir, std::list<OvrString>&	a_out_file_list);

	ovr::uint32	GetPercent();
	void		Stop();
private:
	void	ReadSkeleton(FbxNode* a_root_node);

	void	ReadSkeletonAnimation(FbxScene* a_scene);

	bool	ReadTexture(FbxScene* a_scene);

	ovr_asset::OvrArchiveSurface*	ReadNode(FbxNode* a_node);
	bool	ReadMesh(FbxManager* a_manager, FbxScene * a_scene, FbxNode* a_node, ovr_asset::OvrArchiveSurface* a_new_surface);

	void	ReadPose();

	void    ReadPoseAnimation(FbxScene* a_scene);
private:

	bool InitFbxSdk(const char* a_fbx_file);
	void UninitFbxSdk();

	const char*	GetTexturePathName(const FbxSurfaceMaterial * a_material, const char * a_texture_type);
	bool		FillMaterial(OvrMaterial& a_material , const FbxSurfaceMaterial* a_fbx_material);

	OvrMaterialType GetMaterialType(const FbxSurfaceMaterial * a_fbx_material, OvrMaterial& a_material);

	void ProcessSkeletonHierarchyRecursively(FbxNode* inNode, int myIndex, int inParentIndex);
	bool GoNextStep(int a_progress);
	//void FillDefaultJointAnimation(FbxScene* a_scene, FbxNode* a_skeleton_node, ovr_asset::JointAnimation* a_animation);
private:
	OvrString		fbx_file;
	OvrString		output_dir;

	OvrRawModel		raw_model;

	FbxScene*		fbx_scene;
	FbxManager*		fbx_manager;
	ovr::uint32     progress_percent;
	bool			is_stop_parse;
};

#endif
