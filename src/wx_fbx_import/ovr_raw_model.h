﻿#ifndef OVR_FBX_IMPORT_OVR_RAW_MODEL_H_
#define OVR_FBX_IMPORT_OVR_RAW_MODEL_H_

struct VertexHaskFunc
{
	std::size_t operator()(const ovr_asset::OvrVertex& key)const
	{
		using std::size_t;
		using std::hash;
		size_t h1 = std::hash<float>{}(key.position[0]);
		size_t h2 = std::hash<float>{}(key.position[1]);
		size_t h3 = std::hash<float>{}(key.position[2]);
		return h1 ^ (h2 << 1) ^ (h3 << 2);
	}
};

struct RawTexture
{
	RawTexture(const char* a_name, ovr_asset::OvrTextureUsage a_usage)
		:name(a_name)
		, usage(a_usage)
	{
	}
	void operator = (const RawTexture& a_texture)
	{
		name = a_texture.name;
		usage = a_texture.usage;
	}
	bool operator == (const RawTexture& a_texture)const
	{
		return name == a_texture.name && usage == a_texture.usage;
	}
	OvrString				name;
	ovr_asset::OvrTextureUsage			usage;
};


enum OvrMaterialType
{
	kMaterialOpaque,
	kMaterialPerforated,
	kMaterialTransparent,
	kMaterialAdditive
};


struct OvrMaterial
{
	OvrMaterialType			type;
	OvrString				name;
	std::vector<RawTexture>	raw_texture;
	OvrString				save_path;
	bool					is_use_skeleton = false;
	bool					is_use_morpth = false;
	bool					is_normal_map = false;
	void operator = (const OvrMaterial& a_material)
	{
		name = a_material.name;
		type = a_material.type;
		raw_texture = a_material.raw_texture;
		save_path = a_material.save_path;
	}

	bool operator == (const OvrMaterial& a_material)
	{
		if (type != a_material.type || !(name == a_material.name))
		{
			return false;
		}
		if (raw_texture.size() != a_material.raw_texture.size())
		{
			return false;
		}
		if (!(save_path == a_material.save_path))
		{
			return false;
		}
		for (ovr::uint32 i = 0; i < a_material.raw_texture.size(); i++)
		{
			if (raw_texture[i] == a_material.raw_texture[i])
			{
				continue;
			}
			return false;
		}
		return true;
	}
};


class OvrSurfaceVertex 
{
public:
	OvrSurfaceVertex() {}
	~OvrSurfaceVertex() {}

	ovr::int32	AddVertex(const ovr_asset::OvrVertex& a_vertex);
	void		AddTriangle(const OvrArray3i& a_triangle) { triangles.push_back(a_triangle); }

	void		FillSurface(ovr_asset::OvrArchiveSurface* a_surface);

private:
	//计算mesh的中心 并重置顶点位置
	void	CalcCenterPosition();

private:

	std::unordered_map<ovr_asset::OvrVertex, ovr::int32, VertexHaskFunc>	vertex_hash_map;
	std::vector<ovr_asset::OvrVertex>			vertices;
	std::vector<OvrArray3i>							triangles;

	OvrVector3										center_position;
};


class OvrRawModel 
{
public:
	OvrRawModel();
	~OvrRawModel();

	void		AddTextureFilePath(const char* a_file_path);
	void		Reset();
	bool					InitMesh(const char* a_fbx_file, const char* a_out_dir);
	ovr_asset::OvrArchiveMesh*	GetMesh() { return mesh; }

	ovr::uint32	AddMaterial(const OvrMaterial& a_material);
	void		AddJoint(const ovr_asset::OvrArchiveJoint& a_joint, FbxNode* a_node);
	void		AddPoseMesh(FbxMesh* a_mesh) { fbx_mesh_list.push_back(a_mesh); }
	void		AddPose(const ovr_asset::OvrPose& a_pose) { pose_list.push_back(a_pose); }
	void		SetJointDefaultPose(const OvrString& a_name, const OvrMatrix4& a_pose);
	ovr::int32	GetJointNum()const { return joint_list.size(); }
	ovr::int32	FindJointIndexUsingName(const char* a_name);
	ovr_asset::OvrArchiveJoint*	GetJoint(ovr::uint32 a_index);
	FbxNode*	GetSkeletonNode(ovr::uint32 a_index);
	FbxMesh*	GetPoseMesh(ovr::uint32 a_index) { return fbx_mesh_list[a_index]; }
	ovr::uint32	GetPoseMeshNum() { return fbx_mesh_list.size(); }
	ovr::uint32	GetPoseNum() { return pose_list.size(); }

	ovr_asset::OvrArchiveJoint*	GetJoint(const char* a_name);
	//void		AddAnimation(ovr_asset::JointAnimation* a_animation) { joint_animation_list.push_back(a_animation); }
	void		AddMotionFrame(ovr_asset::OvrMotionFrame* a_frame) { motion_frame_list.push_back(a_frame); }
	void		AddMorphFrame(ovr_asset::OvrMorphFrame* a_frame) { morph_frame_list.push_back(a_frame); }

	float&      GetDuration() { return duration; }
	void Save(const char* a_fbx_file, const char* a_out_dir, std::list<OvrString>&	a_out_file_list);
	
private:
	void	SaveTexture(const char* a_file_path, const char* a_file_name, const char* a_name);
	void	SaveMaterial(const OvrMaterial& a_mat);
	bool	GetTextureUniformName(ovr_asset::OvrTextureUsage a_usage, OvrString& a_name);
	OvrString GetShaderName(const OvrMaterial& a_mat);
private:

	std::vector<OvrMaterial>		material_vector;
	std::map<std::string,OvrString>	texture_file_path;	//key 为原文件路径  value为新文件路径

	std::list<ovr_asset::OvrArchiveJoint>			joint_list;
	std::list<FbxNode*>							fbx_node_list;
	//std::list<wx_asset::JointAnimation*>	joint_animation_list;
	std::list<ovr_asset::OvrMotionFrame*>	motion_frame_list;
	std::list<ovr_asset::OvrMorphFrame*>	morph_frame_list;

	float duration = 0.0f;
	ovr_asset::OvrArchiveMesh*				mesh;
	std::list<ovr_asset::OvrPose>			pose_list;
	std::vector<FbxMesh*>					fbx_mesh_list;
};

#endif

