#include <algorithm>
#include "lvr_pvr.h"

bool lvr_pvr::load_from_mem(const ovr::int8* a_file_name,const ovr::uint8* a_buffer,const ovr::int32 a_buf_len, bool use_srgb,bool nomipmaps,lvr_image_info& ao_ii)
{
	ao_ii.width = 0;
	ao_ii.height = 0;
	if ( a_buf_len < ( int )( sizeof( lvr_pvr_header ) ) )
	{
		//LVR_LOG( "%s: Invalid PVR file", a_file_name );
		return false;
	}

	const lvr_pvr_header & header = *( lvr_pvr_header * )a_buffer;
	if ( header.Version != 0x03525650 )
	{
		//LVR_LOG( "%s: Invalid PVR file version", a_file_name );
		return false;
	}

	int format = 0;
	switch ( header.PixelFormat )
	{
	case 2:						format = 0x01400/*Texture_PVR4bRGB*/;	break;
	case 3:						format = 0x01500/*Texture_PVR4bRGBA*/;	break;
	case 6:						format = 0x01800/*Texture_ETC1*/;		break;
	case 22:					format = 0x01900/*Texture_ETC2_RGB*/;	break;
	case 23:					format = 0x01A00/*Texture_ETC2_RGBA*/;	break;
#ifdef LVR_OS_WIN32
	case 578721384203708274ull:	format = 0x00300/*Texture_RGBA*/;		break;
#else
	case 578721384203708274llu:	format = 0x00300/*Texture_RGBA*/;		break;
#endif
	default:
		//LVR_LOG( "%s: Unknown PVR texture format %llu, size %ix%i", a_file_name, header.PixelFormat, ao_ii.width, ao_ii.height );
		return false;
	}

	// skip the metadata
	const ovr::uint32 startTex = sizeof( lvr_pvr_header ) + header.MetaDataSize;
	if ( ( startTex < sizeof( lvr_pvr_header ) ) || ( startTex >= static_cast< size_t >( a_buf_len ) ) )
	{
		//LVR_LOG( "%s: Invalid PVR header sizes", a_file_name );
		return false;
	}

	const ovr::uint32 mipCount = ( nomipmaps ) ? 1 : std::max( 1u, header.MipMapCount );

	ao_ii.width = header.Width;
	ao_ii.height = header.Height;

	if (a_file_name)
	{
		strcpy(ao_ii.name, (const char*)a_file_name);
	}
	else
	{
		ao_ii.name[0] = '\0';
	}
	ao_ii.format = format;
	ao_ii.data = a_buffer + startTex;
	ao_ii.data_size = a_buf_len - startTex;
	ao_ii.mipcount = mipCount;
	ao_ii.use_srgb = use_srgb;
	ao_ii.image_size_stored = false;
	ao_ii.data_type = header.ChannelType;
	if ( header.NumFaces == 1 )
	{
		ao_ii.is_cube_tex = false;
		return true;
	}
	else if ( header.NumFaces == 6 )
	{
		ao_ii.is_cube_tex = true;
		return true;
	}
	else
	{
		//LVR_LOG( "%s: PVR file has unsupported number of faces %d", a_file_name, header.NumFaces );
	}
	ao_ii.width = 0;
	ao_ii.height = 0;
	return false;
}

