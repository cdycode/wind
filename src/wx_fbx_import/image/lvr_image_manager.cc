#include "lvr_image_manager.h"
#include <string>
#include <map>

#include "lvr_ktx.h"
#include "lvr_pvr.h"
#include "lvr_jpg.h"
#include "lvr_png.h"
#include "lvr_tga.h"


class lvr_image_manager::impl
{
public:
	impl(){}
	~impl(){
		std::map<std::string,lvr_image*>::iterator it = _pixels_map.begin();
		for (;it != _pixels_map.end();it++)
		{
			delete it->second;
		}
		_pixels_map.clear();
	}
public:
	std::map<std::string,lvr_image*>	_pixels_map;
};

lvr_image_manager::lvr_image_manager()
{
	_this = new impl;
}

lvr_image_manager::~lvr_image_manager()
{
	delete _this;
}

lvr_image_manager::load_error lvr_image_manager::load_pic_from_mem(const ovr::uint8* a_file_mem,const ovr::int8* a_pic_name,const char* extension,ovr::uint32 a_buf_len,lvr_image_info& ao_ii)
{
	if (strcmp(extension,"ktx") == 0)
	{
		lvr_ktx::load_from_mem(a_pic_name,a_file_mem,a_buf_len,false,false,ao_ii);
		return e_no_error;
	}
	else if (strcmp(extension,"pvr") == 0)
	{
		lvr_ktx::load_from_mem(a_pic_name,a_file_mem,a_buf_len,false,false,ao_ii);
		return e_no_error;
	}
	else if (strcmp(extension,"jpg") == 0)
	{
		lvr_jpg* jpg = new lvr_jpg;
		if(jpg->load_from_mem(a_pic_name,a_file_mem,a_buf_len,ao_ii))
		{
			_this->_pixels_map[std::string((const char*)a_pic_name)] = jpg;
		}
		else
		{
			return e_unknown_error;
		}
		return e_no_error;
	}
	else if (strcmp(extension,"png") == 0)
	{
		lvr_png* png = new lvr_png;
		if(png->load_from_mem(a_pic_name,a_file_mem,a_buf_len,ao_ii))
		{
			_this->_pixels_map[std::string((const char*)a_pic_name)] = png;
		}
		else
		{
			return e_unknown_error;
		}
		return e_no_error;
	}
	else if (strcmp(extension,"tga") == 0)
	{
		lvr_tga* tga = new lvr_tga;
		tga->load_from_mem(a_pic_name,a_file_mem,a_buf_len,ao_ii);
		_this->_pixels_map[std::string((const char*)a_pic_name)] = tga;
		return e_no_error;
	}
	else if (strcmp(extension,"dds") == 0)
	{
		lvr_tga* tga = new lvr_tga;
		tga->load_from_mem(a_pic_name,a_file_mem,a_buf_len,ao_ii);
		_this->_pixels_map[std::string((const char*)a_pic_name)] = tga;
		return e_no_error;
	}
	else if (strcmp(extension,"bmp") == 0)
	{
		lvr_tga* tga = new lvr_tga;
		tga->load_from_mem(a_pic_name,a_file_mem,a_buf_len,ao_ii);
		_this->_pixels_map[std::string((const char*)a_pic_name)] = tga;
		return e_no_error;
	}
	return e_unknown_error;
}

lvr_image_manager::load_error lvr_image_manager::load_pic_from_path(const ovr::int8* a_file_path,const ovr::int8* a_pic_name,lvr_image_info& ao_ii)
{
	load_error t_er = e_unknown_error;
	const char* extension = strrchr((const char*)a_file_path, '.');
	if (!extension)
	{
		return t_er;
	}
	FILE* pfile = fopen((const char*)a_file_path, "rb");
	if (pfile)
	{
		fseek(pfile, 0,SEEK_END);
		int len = ftell(pfile);
		rewind(pfile);
		ovr::uint8* buf = new ovr::uint8[len];
		fread(buf, sizeof(char), len, pfile);
		fclose(pfile);
		t_er = load_pic_from_mem(buf, a_pic_name, extension + 1 , len, ao_ii);
		delete[] buf;
	}
	return t_er;
}

lvr_pixels lvr_image_manager::get_pixel_data(const ovr::int8* a_pic_name)
{
	lvr_pixels lp;
	lp.buf_size = 0;
	lp.pixels = 0;
	lp.width = 0;
	lp.height = 0;
	lp.pixel_type = lvr_e_pixel_rgb;
	std::map<std::string,lvr_image*>::iterator it = _this->_pixels_map.find(std::string((const char*)a_pic_name));
	if(it != _this->_pixels_map.end())
	{
		lp = it->second->get_pixels();
	}
	return lp;
}

bool lvr_image_manager::release_image(const ovr::int8* a_pic_name)
{
	std::map<std::string,lvr_image*>::iterator it = _this->_pixels_map.find(std::string((const char*)a_pic_name));
	if(it != _this->_pixels_map.end())
	{
		delete it->second;
		_this->_pixels_map.erase(it);
		return true;
	}
	return false;
}
