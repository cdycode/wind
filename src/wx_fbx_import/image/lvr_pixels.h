#ifndef __lvr_pixels_h__
#define __lvr_pixels_h__

#include "lvr_image_configure.h"
#include <wx_base/wx_type.h>

class lvr_pixels
{
public:
	lvr_pixels()
		:pixels(0),width(0),height(0),buf_size(0),pixel_type(lvr_e_pixel_unknown)
	{

	}
public:
	ovr::uint8*			pixels;
	ovr::int32				width;
	ovr::int32				height;
	ovr::int32				buf_size;
	lvr_enum_pixel_type	pixel_type;
public:
	void clear()
	{
		if (pixels)
		{
			delete[] pixels;
			pixels = 0;
		}
		width = 0;
		height = 0;
		buf_size = 0;
		pixel_type = lvr_e_pixel_unknown;
	}
};

#endif
