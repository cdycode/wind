
#include "lvr_png.h"
#include "stb_image.h"


lvr_png::lvr_png()
{
}

lvr_png::~lvr_png()
{
	_pixel_info.clear();
}

bool lvr_png::load_from_mem(const ovr::int8* a_file_name,const ovr::uint8* a_buf,ovr::uint32 a_buf_len,lvr_image_info& ao_ii)
{
	int comp = 0;
	_pixel_info.clear();
	_pixel_info.pixels = stbi_load_from_memory(a_buf,a_buf_len,&(_pixel_info.width),&(_pixel_info.height),&comp,4);
	if (!_pixel_info.pixels)
	{
		//LVR_LOG("%s", stbi_failure_reason());
		return false;
	}
	_pixel_info.pixel_type = lvr_e_pixel_rgba;
	_pixel_info.buf_size = _pixel_info.width * _pixel_info.height * 4;
	ao_ii.width = _pixel_info.width;
	ao_ii.height = _pixel_info.height;
	ao_ii.data_type = 0x1401;
	ao_ii.is_cube_tex = false;
	if (_pixel_info.pixels != NULL)
	{
		ao_ii.data = _pixel_info.pixels;
		ao_ii.data_size = _pixel_info.buf_size;
		ao_ii.format = 0x00300;
		ao_ii.gl_format = 0;
		ao_ii.gl_internal_format = 0;
		ao_ii.mipcount = 1;
		ao_ii.use_srgb = false;
		if (a_file_name)
		{
			strcpy(ao_ii.name, (const char*)a_file_name);
		}
		else
		{
			ao_ii.name[0] = '\0';
		}
		ao_ii.image_size_stored = false;
		return true;
	}
	return false;
}

void lvr_png::release()
{
	_pixel_info.clear();
}

const lvr_pixels& lvr_png::get_pixels()
{
	return _pixel_info;
}


