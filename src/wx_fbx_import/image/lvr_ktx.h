#ifndef __lvr_ktx_h__
#define __lvr_ktx_h__

#include "lvr_image_configure.h"
/*

KTX Container Format

KTX is a format for storing textures for OpenGL and OpenGL ES applications.
It is distinguished by the simplicity of the loader required to instantiate
a GL texture object from the file contents.

Byte[12] identifier
ovr::uint32 endianness
ovr::uint32 glType
ovr::uint32 glTypeSize
ovr::uint32 glFormat
ovr::uint32 glInternalFormat
ovr::uint32 glBaseInternalFormat
ovr::uint32 pixelWidth
ovr::uint32 pixelHeight
ovr::uint32 pixelDepth
ovr::uint32 numberOfArrayElements
ovr::uint32 numberOfFaces
ovr::uint32 numberOfMipmapLevels
ovr::uint32 bytesOfKeyValueData
  
for each keyValuePair that fits in bytesOfKeyValueData
    ovr::uint32   keyAndValueByteSize
    Byte     keyAndValue[keyAndValueByteSize]
    Byte     valuePadding[3 - ((keyAndValueByteSize + 3) % 4)]
end
  
for each mipmap_level in numberOfMipmapLevels*
    ovr::uint32 imageSize;
    for each array_element in numberOfArrayElements*
       for each face in numberOfFaces
           for each z_slice in pixelDepth*
               for each row or row_of_blocks in pixelHeight*
                   for each pixel or block_of_pixels in pixelWidth
                       Byte data[format-specific-number-of-bytes]**
                   end
               end
           end
           Byte cubePadding[0-3]
       end
    end
    Byte mipPadding[3 - ((imageSize + 3) % 4)]
end

*/

#pragma pack(1)
struct lvr_ktx_header
{
	ovr::uint8	identifier[12];
	ovr::uint32	endianness;
	ovr::uint32	glType;
	ovr::uint32	glTypeSize;
	ovr::uint32	glFormat;
	ovr::uint32	glInternalFormat;
	ovr::uint32	glBaseInternalFormat;
	ovr::uint32	pixelWidth;
	ovr::uint32	pixelHeight;
	ovr::uint32	pixelDepth;
	ovr::uint32	numberOfArrayElements;
	ovr::uint32	numberOfFaces;
	ovr::uint32	numberOfMipmapLevels;
	ovr::uint32	bytesOfKeyValueData;
};
#pragma pack()

class lvr_ktx
{
public:
	lvr_ktx();
	~lvr_ktx();
public:
	static bool load_from_mem(const ovr::int8* a_file_name,const ovr::uint8* a_buffer,const ovr::int32 a_buf_len,
		bool use_srgb,bool nomipmaps,lvr_image_info& ao_ii);
};

#endif
