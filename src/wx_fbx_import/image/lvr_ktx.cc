#include <algorithm>
#include "lvr_ktx.h"

bool lvr_ktx::load_from_mem(const ovr::int8* a_file_name,const ovr::uint8* a_buffer,const ovr::int32 a_buf_len, bool use_srgb,bool nomipmaps,lvr_image_info& ao_ii)
{
	ao_ii.width = 0;
	ao_ii.height = 0;

	if ( a_buf_len < (int)( sizeof( lvr_ktx_header ) ) )
	{
		//OvrLogD( "%s: Invalid KTX file", a_file_name );
		return false;
	}

	//Byte[12] FileIdentifier = {
	//	0xAB, 0x4B, 0x54, 0x58, 0x20, 0x31, 0x31, 0xBB, 0x0D, 0x0A, 0x1A, 0x0A
	//}
	const ovr::uint8 fileIdentifier[12] =
	{
		static_cast<ovr::uint8>('\xAB'), 'K', 'T', 'X', ' ', '1', '1', static_cast<ovr::uint8>('\xBB'), '\r', '\n', '\x1A', '\n'
	};

	const lvr_ktx_header & header = *(lvr_ktx_header *)a_buffer;
	if ( memcmp( header.identifier, fileIdentifier, sizeof( fileIdentifier ) ) != 0 )
	{
		//LVR_LOG( "%s: Invalid KTX file", a_file_name );
		return false;
	}
	// only support little endian
	if ( header.endianness != 0x04030201 )
	{
		//LVR_LOG( "%s: KTX file has wrong endianess", a_file_name );
		return false;
	}
	// only support compressed or unsigned byte
	if (header.glType != 0 && header.glType != 0x1401/*GL_UNSIGNED_BYTE*/ && header.glType != 0x1403/*GL_UNSIGNED_SHORT*/)
	{
		//LVR_LOG( "%s: KTX file has unsupported glType %d", a_file_name, header.glType );
		return false;
	}
	// no support for texture arrays
	if ( header.numberOfArrayElements != 0 )
	{
		//LVR_LOG( "%s: KTX file has unsupported number of array elements %d", a_file_name, header.numberOfArrayElements );
		return false;
	}
 	// derive the texture format from the GL format
	ao_ii.format = 0;
	ao_ii.gl_format = header.glFormat;
	ao_ii.gl_internal_format = header.glInternalFormat;
//  	int format = 0;
//  	if ( !GlFormatToTextureFormat( format, header.glFormat, header.glInternalFormat ) )
//  	{
//  		LVR_LOG( "%s: KTX file has unsupported glFormat %d, glInternalFormat %d", a_file_name, header.glFormat, header.glInternalFormat );
//  		return false;
//  	}
	// skip the key value data
	const uintptr_t startTex = sizeof( lvr_ktx_header ) + header.bytesOfKeyValueData;
	if ( ( startTex < sizeof( lvr_ktx_header ) ) || ( startTex >= static_cast< size_t >( a_buf_len ) ) )
	{
		//LVR_LOG( "%s: Invalid KTX header sizes", a_file_name );
		return false;
	}

	ao_ii.width = header.pixelWidth;
	ao_ii.height = header.pixelHeight;
	const ovr::uint32 mipCount = ( nomipmaps ) ? 1 : std::max( 1u, header.numberOfMipmapLevels );

	if (a_file_name)
	{
		strcpy(ao_ii.name, (const char*)a_file_name);
	}
	else
	{
		ao_ii.name[0] = '\0';
	}
	ao_ii.data = a_buffer + startTex;
	ao_ii.data_size = a_buf_len - startTex;
	ao_ii.mipcount = mipCount;
	ao_ii.use_srgb = use_srgb;
	ao_ii.image_size_stored = true;
	ao_ii.data_type = header.glType;
	if (header.glType == 0x1403/*GL_UNSIGNED_SHORT*/)
	{
		ao_ii.image_size_stored = false;
	}
	if (header.numberOfFaces == 1 || header.numberOfFaces == 0 )
	{
		ao_ii.is_cube_tex = false;
		return true;
	}
	else if ( header.numberOfFaces == 6 )
	{
		ao_ii.is_cube_tex = true;
		return true;
	}
	else
	{
		//LVR_LOG( "%s: KTX file has unsupported number of faces %d", a_file_name, header.numberOfFaces );
	}

	ao_ii.width = 0;
	ao_ii.height = 0;
	return false;
}

