#ifndef __lvr_image_configure_h__
#define __lvr_image_configure_h__

#include <wx_base/wx_type.h>
#include <string.h>

enum lvr_enum_pixel_type
{
	lvr_e_pixel_unknown = 0,
	lvr_e_pixel_rgb = 1,
	lvr_e_pixel_rgba = 2,
	lvr_e_pixel_a8 = 3,
	lvr_e_pixel_r32 = 4
};

struct lvr_image_info
{
	char	name[256];
	int		format;
	ovr::uint32	gl_format;
	ovr::uint32	gl_internal_format;
	int		width;
	int		height;
	const ovr::uint8* data;
	ovr::int32 data_size;
	int		mipcount;
	ovr::uint32  data_type;
	bool	use_srgb;
	bool	image_size_stored;
	bool	is_cube_tex;
};


#endif
