#include "lvr_image_util.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

ovr::int32	lvr_get_pixel_size(lvr_enum_pixel_type a_pixel_type)
{
	switch(a_pixel_type)
	{
	case lvr_e_pixel_rgb:
		return 3;
	case lvr_e_pixel_rgba:
		return 4;
	case lvr_e_pixel_r32:
		return 4;
	case lvr_e_pixel_a8:
		return 1;
	default:
		return 0;
	}
}

