﻿#include "headers.h"
#include "ovr_fbx_parser.h"
#include "ovr_fbx_data_access.h"
#include "wx_engine/ovr_material.h"
#include <assert.h>
typedef std::map<ovr::uint32, std::vector<ovr::uint32>> FBX2OVRIndexMap;
typedef std::vector<ovr::uint32> OVRIndexVector;

OvrFbxParser::OvrFbxParser() 
	:is_stop_parse(false)
	,progress_percent(0)
	,fbx_manager(nullptr)
	,fbx_scene(nullptr)
{
}

OvrFbxParser::~OvrFbxParser()
{
	Unint();
}

bool OvrFbxParser::Init(const char* a_fbx_file, const char* a_out_dir)
{
	progress_percent = 0;
	is_stop_parse = false;

	fbx_file.SetString(a_fbx_file);
	output_dir = a_out_dir;

	raw_model.InitMesh(a_fbx_file, a_out_dir);

	if (!InitFbxSdk(a_fbx_file)) 
	{
		return false;
	}
	GoNextStep(10);


	ReadTexture(fbx_scene);
	GoNextStep(10);


	ReadSkeleton(fbx_scene->GetRootNode());
	GoNextStep(20);
	
	ReadSkeletonAnimation(fbx_scene);
	GoNextStep(10);

	//读取mesh信息
	//ParserMesh();
	FbxNode* root_node = fbx_scene->GetRootNode();
	int node_num = root_node->GetChildCount();
	for (int child = 0; child < node_num; child++)
	{
		ReadNode(root_node->GetChild(child));		
		GoNextStep(50 / node_num);
	}

	ReadPoseAnimation(fbx_scene);
	progress_percent = 100;
	//ReadPose();

	return true;
}

void OvrFbxParser::Unint()
{
	UninitFbxSdk();
	raw_model.Reset();
	//释放空间
}

void OvrFbxParser::ReadSkeleton(FbxNode* a_root_node)
{
	int rootNodeChildCount = a_root_node->GetChildCount();
	for (int childIndex = 0; childIndex < rootNodeChildCount; childIndex++)
	{
		FbxNode* currNode = a_root_node->GetChild(childIndex);
		ProcessSkeletonHierarchyRecursively(currNode, 0, -1);
	}
}

void OvrFbxParser::ReadSkeletonAnimation(FbxScene* a_scene)
{
	FbxAnimStack* pAnimStack = a_scene->GetSrcObject<FbxAnimStack>(0);
	if (!pAnimStack)
	{
		return;
	}
	FbxAnimLayer* lAnimLayer = pAnimStack->GetMember<FbxAnimLayer>(0);
	if (!lAnimLayer)
	{
		return;
	}

	ovr::uint32 joint_num = raw_model.GetJointNum();
	if (joint_num <= 0)
		return;

	FbxNode* pNode = raw_model.GetSkeletonNode(0);
	FbxAnimCurve* pCurve = pNode->LclTranslation.GetCurve(lAnimLayer, FBXSDK_CURVENODE_COMPONENT_X);
	const int nKeyCount = pCurve->KeyGetCount();
	FbxTime kTime;
	for (int frame_index = 0; frame_index < nKeyCount; ++frame_index)
	{
		ovr_asset::OvrMotionFrame* frame = new ovr_asset::OvrMotionFrame();
		kTime = pCurve->KeyGetTime(frame_index);
		frame->Init(joint_num);
		for (ovr::uint32 joint_index = 0; joint_index < joint_num; joint_index++)
		{
			FbxNode* pNode = raw_model.GetSkeletonNode(joint_index);
			const FbxAMatrix& kMatLocal = pNode->EvaluateLocalTransform(kTime);
			frame->GetJointAnimationFrame(joint_index)->is_materix = true;
			frame->GetJointAnimationFrame(joint_index)->joint_index = joint_index;
			frame->GetJointAnimationFrame(joint_index)->joint_name = raw_model.GetJoint(joint_index)->name;
			for (int m = 0; m < 4; m++)
			{
				for (int n = 0; n < 4; n++)
				{
					frame->GetJointAnimationFrame(joint_index)->joint_materix(m, n) = (float)kMatLocal.Get(m, n);
				}
			}
		}
		raw_model.AddMotionFrame(frame);
	}
	raw_model.GetDuration() = (float)kTime.GetSecondDouble();
}

bool OvrFbxParser::ReadTexture(FbxScene* a_scene)
{
	// Try to match the FBX texture names with the actual files on disk.
	int num = a_scene->GetTextureCount();
	for (int i = 0; i < a_scene->GetTextureCount(); i++)
	{
		const FbxTexture * pTexture = a_scene->GetTexture(i);
		const FbxFileTexture * pFileTexture = FbxCast<FbxFileTexture>(pTexture);
		if (pFileTexture == NULL)
		{
			continue;
		}
		const char* name = pTexture->GetName();
		const char* path_name = pFileTexture->GetFileName();
		raw_model.AddTextureFilePath(path_name);
	}
	return true;
}

ovr_asset::OvrArchiveSurface* OvrFbxParser::ReadNode(FbxNode* a_node)
{
	const char * node_name = a_node->GetName();

	if (!a_node->GetVisibility())
	{
		return nullptr;
	}

	FbxNodeAttribute * pNodeAttribute = a_node->GetNodeAttribute();
	if (pNodeAttribute == NULL)
	{
		return nullptr;
	}
	const FbxNodeAttribute::EType attributeType = pNodeAttribute->GetAttributeType();
	switch (attributeType)
	{
	case FbxNodeAttribute::eMesh:
	case FbxNodeAttribute::eNurbs:
	case FbxNodeAttribute::eNurbsSurface:
	case FbxNodeAttribute::ePatch:
	{
		ovr_asset::OvrArchiveSurface* new_surface = new ovr_asset::OvrArchiveSurface;
		new_surface->SetType(ovr_asset::kSerializeSurface);
		ReadMesh(fbx_manager, fbx_scene, a_node, new_surface);
		raw_model.GetMesh()->AddSurface(new_surface);
		return new_surface;
	}
	case FbxNodeAttribute::eNull:
	{
		int childnum = a_node->GetChildCount();
		for (int child = 0; child < a_node->GetChildCount(); child++)
		{
			ReadNode(a_node->GetChild(child));
		}
		return nullptr;
	}
	break;
	case FbxNodeAttribute::eMarker:
	case FbxNodeAttribute::eSkeleton:
	case FbxNodeAttribute::eCamera:
	case FbxNodeAttribute::eLight:
	{
		break;
	}
	}

	return nullptr;
}

bool OvrFbxParser::ReadMesh(FbxManager* a_manager, FbxScene * a_scene, FbxNode* a_node, ovr_asset::OvrArchiveSurface* a_new_surface)
{
	FbxGeometryConverter meshConverter(a_manager);
	meshConverter.Triangulate(a_node->GetNodeAttribute(), true);
	FbxMesh * pMesh = a_node->GetMesh();
	int controlPointCount = pMesh->GetControlPointsCount();
	const FbxVector4 meshTranslation = a_node->GetGeometricTranslation(FbxNode::eSourcePivot);
	const FbxVector4 meshRotation = a_node->GetGeometricRotation(FbxNode::eSourcePivot);
	const FbxVector4 meshScaling = a_node->GetGeometricScaling(FbxNode::eSourcePivot);
	const FbxAMatrix meshTransform(meshTranslation, meshRotation, meshScaling);
	const FbxAMatrix & globalTransform = a_scene->GetAnimationEvaluator()->GetNodeGlobalTransform(a_node);
	const FbxMatrix transform = globalTransform * meshTransform;
	const FbxMatrix inverseTransposeTransform = transform.Inverse().Transpose();
	const OvrMatrix4 surfaceTransform(
		(float)globalTransform[0][0], (float)globalTransform[1][0], (float)globalTransform[2][0], (float)globalTransform[3][0],
		(float)globalTransform[0][1], (float)globalTransform[1][1], (float)globalTransform[2][1], (float)globalTransform[3][1],
		(float)globalTransform[0][2], (float)globalTransform[1][2], (float)globalTransform[2][2], (float)globalTransform[3][2],
		(float)globalTransform[0][3], (float)globalTransform[1][3], (float)globalTransform[2][3], (float)globalTransform[3][3]);

	const char * meshName = (a_node->GetName()[0] != '\0') ? a_node->GetName() : pMesh->GetName();
	a_new_surface->SetName(meshName);
	//const ovr::uint32 surface_index = raw_model.AddSurface(meshName, surfaceTransform);
	//OvrSurface*	current_surface = raw_model.Geturface(surface_index);

	const FbxVector4 * controlPoints = pMesh->GetControlPoints();
	const FbxLayerElementAccess< FbxVector4 > normalLayer(pMesh->GetElementNormal(), pMesh->GetElementNormalCount());
	const FbxLayerElementAccess< FbxVector4 > binormalLayer(pMesh->GetElementBinormal(), pMesh->GetElementBinormalCount());
	const FbxLayerElementAccess< FbxVector4 > tangentLayer(pMesh->GetElementTangent(), pMesh->GetElementTangentCount());
	const FbxLayerElementAccess< FbxColor >   colorLayer(pMesh->GetElementVertexColor(), pMesh->GetElementVertexColorCount());
	const FbxLayerElementAccess< FbxVector2 > uvLayer0(pMesh->GetElementUV(0), pMesh->GetElementUVCount());
	const FbxLayerElementAccess< FbxVector2 > uvLayer1(pMesh->GetElementUV(1), pMesh->GetElementUVCount());
	const FbxSkinningAccess skinning(pMesh, raw_model);
	const FbxMaterialAccess materials(pMesh);
	const FbxBlendShapeAccess blendShape(pMesh, raw_model);
	//目前只支持allsame  非allsame准备采用oculus中FBXConvert中的办法
	if (FbxGeometryElement::eAllSame == materials.GetMapMode())
	{
		const FbxSurfaceMaterial * pMaterial = materials.GetMaterial(0);
		if (nullptr != pMaterial)
		{
			OvrMaterial	ovr_material;	
			if (FillMaterial(ovr_material, pMaterial))
			{
				OvrString temp = pMaterial->GetName();
				int index = temp.FindLastCharPos(':');
				if (index >= 0)
				{
					temp[index] = '_';
				}	
				ovr_material.name = temp;
				ovr_material.save_path = output_dir + ovr_material.name + OvrString(".mat");
				ovr_material.type = GetMaterialType(pMaterial, ovr_material);
				ovr_material.is_use_skeleton = skinning.IsSkinned();
				ovr_material.is_use_morpth = blendShape.IsBlendShaped();
				ovr::uint32 material_index = raw_model.AddMaterial(ovr_material);
				//current_surface->AddMaterialIndex(material_index);
				a_new_surface->SetMaterialNum(1);
				a_new_surface->GetMaterialPath(0) = ovr_material.save_path;
			}
		}
	}

	ovr::uint32 vertex_attribute = ovr_asset::kVertexAttributePosition;
	if (normalLayer.LayerPresent()) { vertex_attribute |= ovr_asset::kVertexAttributeNormal; }
	if (tangentLayer.LayerPresent()) { vertex_attribute |= ovr_asset::kVertexAttributeTangent; }
	if (binormalLayer.LayerPresent()) { vertex_attribute |= ovr_asset::kVertexAttributeBinormal; }
	if (colorLayer.LayerPresent()) { vertex_attribute |= ovr_asset::kVertexAttributeColor; }
	if (uvLayer0.LayerPresent()) { vertex_attribute |= ovr_asset::kVertexAttributeUV0; }
	if (uvLayer1.LayerPresent()) { vertex_attribute |= ovr_asset::kVertexAttributeUV1; }
	if (skinning.IsSkinned()) { vertex_attribute |= ovr_asset::kVertexAttributeWeights; }
	if (skinning.IsSkinned()) { vertex_attribute |= ovr_asset::kVertexAttributeIndices; }
	if (blendShape.IsBlendShaped()){ vertex_attribute |= ovr_asset::kVertexAttributeMorph; }
	a_new_surface->SetVertexAttributes(vertex_attribute);

	int polygonVertexIndex = 0;

	OvrSurfaceVertex surface_vertex;
	FBX2OVRIndexMap index_map;
	for (int polygonIndex = 0; polygonIndex < pMesh->GetPolygonCount(); polygonIndex++)
	{
		FBX_ASSERT(pMesh->GetPolygonSize(polygonIndex) == 3);
		ovr::uint32 fbxControlPointIndex[3];
		ovr_asset::OvrVertex rawVertices[3];
		for (int vertexIndex = 0; vertexIndex < 3; vertexIndex++, polygonVertexIndex++)
		{
			const int controlPointIndex = pMesh->GetPolygonVertex(polygonIndex, vertexIndex);
			
			// Note that the default values here must be the same as the RawVertex default values!
			FbxVector4 src = controlPoints[controlPointIndex];
			const FbxVector4 fbxPosition = transform.MultNormalize(controlPoints[controlPointIndex]);
			const FbxVector4 fbxNormal = normalLayer.GetElement(polygonIndex, polygonVertexIndex, controlPointIndex, FbxVector4(0.0f, 0.0f, 0.0f, 0.0f), inverseTransposeTransform, true);
			const FbxVector4 fbxTangent = tangentLayer.GetElement(polygonIndex, polygonVertexIndex, controlPointIndex, FbxVector4(0.0f, 0.0f, 0.0f, 0.0f), inverseTransposeTransform, true);
			const FbxVector4 fbxBinormal = binormalLayer.GetElement(polygonIndex, polygonVertexIndex, controlPointIndex, FbxVector4(0.0f, 0.0f, 0.0f, 0.0f), inverseTransposeTransform, true);
			const FbxColor   fbxColor = colorLayer.GetElement(polygonIndex, polygonVertexIndex, controlPointIndex, FbxColor(0.0f, 0.0f, 0.0f, 0.0f));
			const FbxVector2 fbxUV0 = uvLayer0.GetElement(polygonIndex, polygonVertexIndex, controlPointIndex, FbxVector2(0.0f, 0.0f));
			const FbxVector2 fbxUV1 = uvLayer1.GetElement(polygonIndex, polygonVertexIndex, controlPointIndex, FbxVector2(0.0f, 0.0f));

			ovr_asset::OvrVertex & vertex = rawVertices[vertexIndex];
			fbxControlPointIndex[vertexIndex] = controlPointIndex;
			vertex.position[0] = (float)fbxPosition[0];
			vertex.position[1] = (float)fbxPosition[1];
			vertex.position[2] = (float)fbxPosition[2];
			vertex.normal[0] = (float)fbxNormal[0];
			vertex.normal[1] = (float)fbxNormal[1];
			vertex.normal[2] = (float)fbxNormal[2];
			vertex.tangent[0] = (float)fbxTangent[0];
			vertex.tangent[1] = (float)fbxTangent[1];
			vertex.tangent[2] = (float)fbxTangent[2];
			vertex.binormal[0] = (float)fbxBinormal[0];
			vertex.binormal[1] = (float)fbxBinormal[1];
			vertex.binormal[2] = (float)fbxBinormal[2];
			vertex.color[0] = (float)fbxColor.mRed;
			vertex.color[1] = (float)fbxColor.mGreen;
			vertex.color[2] = (float)fbxColor.mBlue;
			vertex.color[3] = (float)fbxColor.mAlpha;
			vertex.uv0[0] = (float)fbxUV0[0];
			vertex.uv0[1] = (float)fbxUV0[1];
			vertex.uv1[0] = (float)fbxUV1[0];
			vertex.uv1[1] = (float)fbxUV1[1];
			
			if (skinning.IsSkinned())
			{
				OvrArray4i jointIndices = skinning.GetVertexIndices(controlPointIndex);
				OvrVector4 jointWeights = skinning.GetVertexWeights(controlPointIndex);
				vertex.bone_index[0] = jointIndices[0];
				vertex.bone_index[1] = jointIndices[1];
				vertex.bone_index[2] = jointIndices[2];
				vertex.bone_index[3] = jointIndices[3];
				vertex.bone_weights[0] = jointWeights[0];
				vertex.bone_weights[1] = jointWeights[1];
				vertex.bone_weights[2] = jointWeights[2];
				vertex.bone_weights[3] = jointWeights[3];
				// remap the mesh joint indices to raw model joint indices
				for (int i = 0; i < 4; i++)
				{
					vertex.bone_index[i] = raw_model.FindJointIndexUsingName(skinning.GetJointName(vertex.bone_index[i]));
				}
			}
		}


		OvrArray3i rawVertexIndices;
		for (int vertexIndex = 0; vertexIndex < 3; vertexIndex++)
		{
			int fbxVertexIndex = fbxControlPointIndex[vertexIndex];
			int ovrVertexIndex = rawVertexIndices[vertexIndex] = surface_vertex.AddVertex(rawVertices[vertexIndex]);
			FBX2OVRIndexMap::iterator iter = index_map.find(fbxVertexIndex);
			OVRIndexVector indexVector;
			if (iter != index_map.end())
			{
				indexVector = iter->second;
				ovr::uint32 i = 0;
				for (; i < indexVector.size(); i++)
				{
					if (indexVector.at(i) == ovrVertexIndex)
						break;
				}
				if (i == indexVector.size())
					iter->second.push_back(ovrVertexIndex);
			}
			else
			{
				indexVector.push_back(ovrVertexIndex);
				index_map[fbxVertexIndex] = indexVector;
			}
		}
		surface_vertex.AddTriangle(rawVertexIndices);
	}

	surface_vertex.FillSurface(a_new_surface);

	if (blendShape.IsBlendShaped())
	{
		for (int i = 0; i < blendShape.GetPoseCount(); i++)
		{
			const ovr_asset::OvrPose* fbxPose = blendShape.GetPose(i);
			ovr_asset::OvrPose ovrPose;
			std::map<ovr::uint32, OvrVector3>::const_iterator vertex_offset_iter = fbxPose->vertex_offset_map.begin();
			for (; vertex_offset_iter != fbxPose->vertex_offset_map.end(); vertex_offset_iter++)
			{
				ovr::uint32 vertexIndex = vertex_offset_iter->first;
				OvrVector3 vertexOffset = vertex_offset_iter->second;
				OVRIndexVector ovrIndexs = index_map[vertexIndex];
				for (ovr::uint32 j = 0; j < ovrIndexs.size(); j++)
				{
					ovrPose.vertex_offset_map[ovrIndexs.at(j)] = vertexOffset * 0.01f;
				}
			}
			ovrPose.name = fbxPose->name;
			ovrPose.surface_name = a_new_surface->GetName();
			raw_model.AddPose(ovrPose);
		}
	}

	//获取mesh的重心
	ovr_asset::OvrPivotInfo pivot_info;
	FbxVector4 temp_vector;

	FbxNode::EPivotState lPivotState;
	a_node->GetPivotState(FbxNode::eSourcePivot, lPivotState);

	temp_vector = a_node->GetRotationPivot(FbxNode::eSourcePivot);
	pivot_info.rotation_pivot = OvrVector3((float)temp_vector[0], (float)temp_vector[1], (float)temp_vector[2]);
	temp_vector = a_node->GetRotationOffset(FbxNode::eSourcePivot);
	pivot_info.rotation_offset = OvrVector3((float)temp_vector[0], (float)temp_vector[1], (float)temp_vector[2]);
	temp_vector = a_node->GetScalingPivot(FbxNode::eSourcePivot);
	pivot_info.scale_pivot = OvrVector3((float)temp_vector[0], (float)temp_vector[1], (float)temp_vector[2]);
	temp_vector = a_node->GetScalingOffset(FbxNode::eSourcePivot);
	pivot_info.scale_offset = OvrVector3((float)temp_vector[0], (float)temp_vector[1], (float)temp_vector[2]);
	a_new_surface->SetPiovtInfo(pivot_info);
	return true;
}

void OvrFbxParser::ReadPose()
{
	int pose_num = fbx_scene->GetPoseCount();
	for (int i = 0; i < pose_num; i++)
	{
		FbxPose* fbx_pose = fbx_scene->GetPose(i);
		FbxString pose_name = fbx_pose->GetName();
		int pose_node_num = fbx_pose->GetCount();
		for (int j = 0; j < fbx_pose->GetCount(); j++)
		{
			FbxString node_name = fbx_pose->GetNodeName(j).GetCurrentName();
			ovr_asset::OvrArchiveJoint* joint = raw_model.GetJoint(node_name.Buffer());
			if (nullptr == joint)
			{
				continue;
			}
			bool is_bind = fbx_pose->IsBindPose();
			bool is_local = fbx_pose->IsLocalMatrix(j);
			FbxMatrix  lMatrix = fbx_pose->GetMatrix(j);
			for (int m = 0; m < 4; m++)
			{
				for (int n = 0; n < 4; n++)
				{
					joint->default_pose(m, n) = (float)lMatrix.Get(i, j);
				}
			}
		}
	}
}

void OvrFbxParser::ReadPoseAnimation(FbxScene* a_scene)
{
	if (raw_model.GetPoseMeshNum() <= 0 || raw_model.GetPoseNum() <= 0)
		return;

	FbxAnimStack* lAnimStack = a_scene->GetSrcObject<FbxAnimStack>(0);
	if (!lAnimStack)
		return;

	FbxAnimLayer* lAnimLayer = lAnimStack->GetMember<FbxAnimLayer>(0);
	FbxGeometry*  lGeometry = (FbxGeometry*)(raw_model.GetPoseMesh(0)->GetNode()->GetNodeAttribute());
	FbxAnimCurve* lAnimCurve = lGeometry->GetShapeChannel(0, 0, lAnimLayer, true);
	const int nKeyCount = lAnimCurve->KeyGetCount();
	for (int frame_index = 0; frame_index < nKeyCount; ++frame_index)
	{
		ovr_asset::OvrMorphFrame* morph_frame = new ovr_asset::OvrMorphFrame();
		morph_frame->Init(raw_model.GetPoseNum());
		ovr::uint32 index = 0;
		for (int i = 0; i < raw_model.GetPoseMeshNum(); ++i)
		{
			FbxMesh* pMesh = raw_model.GetPoseMesh(i);
			const FbxBlendShape * blendShape = static_cast<const FbxBlendShape *>(pMesh->GetDeformer(0, FbxDeformer::eBlendShape));
			ovr::uint32 lBlendShapeChannelCount = blendShape->GetBlendShapeChannelCount();
			lGeometry = (FbxGeometry*)(pMesh->GetNode()->GetNodeAttribute());
			for (int lBlendShapeChannelIndex = 0; lBlendShapeChannelIndex < lBlendShapeChannelCount; ++lBlendShapeChannelIndex)
			{
				FbxAnimCurve* lAnimCurve = lGeometry->GetShapeChannel(0, lBlendShapeChannelIndex, lAnimLayer, true);
				const FbxBlendShapeChannel* lBlendShapeChannel = blendShape->GetBlendShapeChannel(lBlendShapeChannelIndex);
				const FbxShape* lShape = lBlendShapeChannel->GetTargetShape(0);
				FbxTime kTime = lAnimCurve->KeyGetTime(frame_index);
				double lWeight = lAnimCurve->Evaluate(kTime) * 0.01;
				ovr_asset::OvrPoseFrame* pose_frame = morph_frame->GetPoseFrame(index);
				pose_frame->pose_index = index;
				pose_frame->pose_name = lShape->GetName();
				pose_frame->pose_weight = lWeight;
				index++;
			}
		}
		raw_model.AddMorphFrame(morph_frame);
	}
}

ovr::uint32 OvrFbxParser::GetPercent()
{
	if (progress_percent > 100)
	{
		return 100;
	}

	return progress_percent;
}

void OvrFbxParser::Stop()
{
	is_stop_parse = true;
}

void OvrFbxParser::Save(const char* a_out_dir, std::list<OvrString>&	a_out_file_list)
{
	if (is_stop_parse)
		return;
	raw_model.Save(fbx_file.GetStringConst(), a_out_dir, a_out_file_list);
}

bool OvrFbxParser::InitFbxSdk(const char* a_fbx_file)
{
	fbx_manager = FbxManager::Create();
	FbxIOSettings* lvr_ios = FbxIOSettings::Create(fbx_manager, IOSROOT);
	fbx_manager->SetIOSettings(lvr_ios);
	FbxImporter* importer = FbxImporter::Create(fbx_manager, "");
	if (!importer->Initialize(a_fbx_file, -1, fbx_manager->GetIOSettings()))
	{
		printf("Call to FbxImporter failed.\n");
		printf("Error returned: %s\n\n", importer->GetStatus().GetErrorString());
		importer->Destroy();
		UninitFbxSdk();
		return false;
	}
	fbx_scene = FbxScene::Create(fbx_manager, "myscene");
	if (fbx_scene == NULL)
	{
		importer->Destroy();
		UninitFbxSdk();
		return false;
	}
	importer->Import(fbx_scene);
	importer->Destroy();

	//scene axis system is Y axis up, parity odd,and right handed system.
	//also this is OpenGL's case.this info get from the FBX exported from max2015 with Y up option.
	FbxAxisSystem SceneAxisSystem = fbx_scene->GetGlobalSettings().GetAxisSystem();
	//FbxAxisSystem OurAxisSystem(FbxAxisSystem::eYAxis, FbxAxisSystem::eParityOdd, FbxAxisSystem::eRightHanded);
	FbxAxisSystem OurAxisSystem(FbxAxisSystem::eOpenGL);
	if (SceneAxisSystem != OurAxisSystem)
	{
		OurAxisSystem.ConvertScene(fbx_scene);
	}
	FbxSystemUnit SceneSystemUnit = fbx_scene->GetGlobalSettings().GetSystemUnit();
	if (SceneSystemUnit.GetScaleFactor() != 100.0)
	{
		FbxSystemUnit::m.ConvertScene(fbx_scene);
	}
	return true;
}

void OvrFbxParser::UninitFbxSdk()
{
	if (nullptr != fbx_scene)
	{
		fbx_scene->Destroy();
		fbx_scene = nullptr;
	}
	if (nullptr != fbx_manager)
	{
		fbx_manager->Destroy();
		fbx_manager = nullptr;
	}
}

const char*	OvrFbxParser::GetTexturePathName(const FbxSurfaceMaterial * a_material, const char * a_texture_type)
{
	if (nullptr == a_material)
	{
		return nullptr;
	}

	const FbxProperty prop = a_material->FindProperty(a_texture_type);
	if (!prop.IsValid())
	{
		return nullptr;
	}
	const FbxTexture * pTexture = prop.GetSrcObject<FbxTexture>();
	if (nullptr == pTexture)
	{
		return nullptr;
	}
	const FbxFileTexture * pFileTexture = FbxCast<FbxFileTexture>(pTexture);
	if (nullptr == pFileTexture)
	{
		return nullptr;
	}
	return pFileTexture->GetFileName();
}

bool OvrFbxParser::FillMaterial(OvrMaterial& a_material, const FbxSurfaceMaterial * a_fbx_material)
{
	const char* file_path_name = GetTexturePathName(a_fbx_material, FbxSurfaceMaterial::sDiffuse);
	if (nullptr != file_path_name)
	{
		 a_material.raw_texture.push_back(RawTexture(file_path_name, ovr_asset::kTextureUsageDiffuse));
	}
	file_path_name = GetTexturePathName(a_fbx_material, FbxSurfaceMaterial::sNormalMap);
	if (nullptr != file_path_name)
	{
		a_material.raw_texture.push_back(RawTexture(file_path_name, ovr_asset::kTextureUsageNormal));
		a_material.is_normal_map = true;
	}
	file_path_name = GetTexturePathName(a_fbx_material, FbxSurfaceMaterial::sSpecular);
	if (nullptr != file_path_name)
	{
		a_material.raw_texture.push_back(RawTexture(file_path_name, ovr_asset::kTextureUsageSpecular));
	}
	file_path_name = GetTexturePathName(a_fbx_material, FbxSurfaceMaterial::sEmissive);
	if (nullptr != file_path_name)
	{
		a_material.raw_texture.push_back(RawTexture(file_path_name, ovr_asset::kTextureUsageEmissive));
	}
	file_path_name = GetTexturePathName(a_fbx_material, FbxSurfaceMaterial::sReflection);
	if (nullptr != file_path_name)
	{
		a_material.raw_texture.push_back(RawTexture(file_path_name, ovr_asset::kTextureUsageReflection));
	}
	return a_material.raw_texture.size() > 0;
}

OvrMaterialType OvrFbxParser::GetMaterialType(const FbxSurfaceMaterial * a_fbx_material, OvrMaterial& a_material)
{
	if (a_fbx_material == NULL)
	{
		return kMaterialOpaque;
	}

	// Look up the shading model, typically "Phong" or "Lambert".
	const std::string shadingModel = (const char*)a_fbx_material->ShadingModel.Get();

	// Check the material name for a special extension.
	const std::string materialName = a_fbx_material->GetName();
	/*if (materialName.Right(9).CompareNoCase("_additive") == 0)
	{
		return RAW_MATERIAL_TYPE_ADDITIVE;
	}*/

	// Determine material type based on texture occlusion.
	/*if (textures[RAW_TEXTURE_USAGE_DIFFUSE] >= 0)
	{
		switch (raw.GetTexture(textures[RAW_TEXTURE_USAGE_DIFFUSE]).occlusion)
		{
		case RAW_TEXTURE_OCCLUSION_OPAQUE:		return RAW_MATERIAL_TYPE_OPAQUE;
		case RAW_TEXTURE_OCCLUSION_PERFORATED:	return RAW_MATERIAL_TYPE_PERFORATED;
		case RAW_TEXTURE_OCCLUSION_TRANSPARENT:	return RAW_MATERIAL_TYPE_TRANSPARENT;
		}
	}*/

	// Default to simply opaque.
	return kMaterialOpaque;
}

void OvrFbxParser::ProcessSkeletonHierarchyRecursively(FbxNode* inNode, int myIndex, int inParentIndex)
{
	if (is_stop_parse)
	{
		return;
	}

	if (inNode->GetNodeAttribute() && inNode->GetNodeAttribute()->GetAttributeType() && inNode->GetNodeAttribute()->GetAttributeType() == FbxNodeAttribute::eSkeleton)
	{
		ovr_asset::OvrArchiveJoint new_joint;
		new_joint.parent_index = inParentIndex;
		new_joint.name = inNode->GetName();
		raw_model.AddJoint(new_joint, inNode);
		//ovr_asset::JointAnimation* new_animation = new ovr_asset::JointAnimation;
		//new_animation->joint_name = new_joint.name;67
		//raw_model.AddAnimation(new_animation);

		FbxAMatrix localMatrix = inNode->EvaluateLocalTransform();
		for (int m = 0; m < 4; m++)
		{
			for (int n = 0; n < 4; n++)
			{
				raw_model.GetJoint(inNode->GetName())->parent_transform(m, n) = (float)localMatrix.Get(m, n);
			}
		}

		//FillDefaultJointAnimation(a_scene, inNode, new_animation);
	}
	int currNodeChildCount = inNode->GetChildCount();
	for (int i = 0; i < currNodeChildCount; i++)
	{
		ProcessSkeletonHierarchyRecursively(inNode->GetChild(i), raw_model.GetJointNum(), myIndex);
	}
}

bool OvrFbxParser::GoNextStep(int a_progress)
{
	progress_percent += a_progress;
	if (is_stop_parse)
		return false;
	return true;
}

//void OvrFbxParser::FillDefaultJointAnimation(FbxScene* a_scene, FbxNode* a_skeleton_node, ovr_asset::JointAnimation* a_animation)
//{
//	FbxAnimStack* currAnimStack = a_scene->GetSrcObject<FbxAnimStack>(0);
//	FbxString animStackName = currAnimStack->GetName();
//	const char* mAnimationName = animStackName.Buffer();
//	FbxTakeInfo* takeInfo = a_scene->GetTakeInfo(animStackName);
//	FbxTime start = takeInfo->mLocalTimeSpan.GetStart();
//	FbxTime end = takeInfo->mLocalTimeSpan.GetStop();
//	FbxTime mFrameTime;
//	mFrameTime.SetTime(0, 0, 0, 1, 0, a_scene->GetGlobalSettings().GetTimeMode());
//	ovr::uint32 animation_num = (ovr::uint32)(end.GetFrameCount(a_scene->GetGlobalSettings().GetTimeMode()) - start.GetFrameCount(a_scene->GetGlobalSettings().GetTimeMode()));
//
//	a_animation->duration = (float)end.GetSecondDouble();
//	a_animation->SetFrameNum(animation_num);
//
//	//准备使用骨骼动画第一帧作为默认姿态
//	ovr_asset::OvrArchiveJoint* current_joint = raw_model.GetJoint(a_animation->joint_name.GetStringConst());
//	bool is_first_frame = true;
//
//	FbxTime currTime;
//	for (ovr::uint32 i = 0; i < animation_num; i++)
//	{
//		OvrMatrix4& local_tranform = a_animation->GetFrame(i);
//		FbxAMatrix localmat = (a_skeleton_node->EvaluateLocalTransform(currTime));
//		for (int m = 0; m < 4; m++)
//		{
//			for (int n = 0; n < 4; n++)
//			{
//				local_tranform(m, n) = (float)localmat.Get(m, n);
//			}
//		}
//
//		//使用骨骼动画的第一帧作为默认姿态
//		if (is_first_frame && nullptr != current_joint)
//		{
//			current_joint->default_pose = local_tranform;
//			is_first_frame = false;
//		}
//
//		FbxQuaternion localQ_ = localmat.GetQ();
//		OvrQuaternion  my_q = OvrQuaternion((float)localQ_.GetAt(0), (float)localQ_.GetAt(1), (float)localQ_.GetAt(2), (float)localQ_.GetAt(3));
//
//		currTime += mFrameTime;
//	}
//}