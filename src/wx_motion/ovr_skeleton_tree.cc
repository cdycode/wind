﻿//#include "headers.h"
//#include "ovr_skeleton_tree.h"
//
//namespace ovr_motion
//{
//	OvrSkeletonTree::OvrSkeletonTree()
//	{
//	}
//	OvrSkeletonTree::~OvrSkeletonTree()
//	{
//		Uinit();
//	}
//
//	bool OvrSkeletonTree::Init(ovr_serialize::OvrSkeleton* a_skeleton)
//	{
//		if (nullptr == a_skeleton)
//		{
//			return false;
//		}
//
//		//初始化
//		bone_num = a_skeleton->GetJointNum();
//		skeleton_animation_array = new OvrSingleSkeletonAnimation[bone_num];
//		animation_frame.Init(bone_num);
//
//		//进行骨骼和动画的匹配
//		for (ovr::uint32 i = 0; i < bone_num; i++)
//		{
//			//skeleton_animation_array 与a_skeleton中数组一一对应
//			ovr_serialize::OvrJoint& current_bont = a_skeleton->GetJoint(i);
//			skeleton_animation_array[i].vertex_to_local_matrix = current_bont.bone_to_mesh_mat;
//			skeleton_animation_array[i].bind_pose = current_bont.default_pose;
//			skeleton_animation_array[i].local_to_parent_matrix = current_bont.parent_transform;
//			//构建父子关系
//			if (current_bont.parent_index >= 0)
//			{
//				skeleton_animation_array[i].parent = &(skeleton_animation_array[current_bont.parent_index]);
//				skeleton_animation_array[i].parent->child_list.push_back(&(skeleton_animation_array[i]));
//			}
//
//			//获取输出最终矩阵的地址
//			skeleton_animation_array[i].final_matrix = animation_frame.GetSkeletonMatrix(i);
//		}
//
//		return true;
//	}
//
//	void OvrSkeletonTree::Uinit()
//	{
//		if (nullptr != skeleton_animation_array)
//		{
//			delete[]skeleton_animation_array;
//			skeleton_animation_array = nullptr;
//		}
//		bone_num = 0;
//		animation_frame.Uinit();
//	}
//
//	void OvrSkeletonTree::SetLocalMatrix(int a_index, const OvrMatrix4& a_local)
//	{
//		skeleton_animation_array[a_index].SetLocalMatrix(a_local);
//	}
//
//	void OvrSkeletonTree::SetQuaternion(int a_index, const OvrQuaternion& a_local)
//	{
//		skeleton_animation_array[a_index].SetQuaternion(a_local);
//	}
//
//	void OvrSkeletonTree::SetPosition(int a_index, const OvrVector3& a_local)
//	{
//		skeleton_animation_array[a_index].SetPosition(a_local);
//	}
//
//	void OvrSkeletonTree::SetAutoCalculateLocalMatrix(int a_index, bool a_local)
//	{
//		skeleton_animation_array[a_index].SetAutoCalculateLocalMatrix(a_local);
//	}
//
//	void OvrSkeletonTree::UpdateMatrix()
//	{
//		skeleton_animation_array[0].UpdateWorldMatrix();
//		skeleton_animation_array[0].UpdateFinalMatrix();
//	}
//
//	void OvrSkeletonTree::GetDefaultPose(OvrMotionFrame& a_frame)
//	{
//		for (ovr::uint32 i = 0; i < bone_num; i++)
//		{
//			skeleton_animation_array[i].local_matrix = skeleton_animation_array[i].bind_pose;
//		}
//
//		UpdateMatrix();
//
//		a_frame.Init(animation_frame.GetSkeletonNum());
//		for (ovr::uint32 i = 0; i < animation_frame.GetSkeletonNum(); i++)
//		{
//			*(a_frame.GetSkeletonMatrix(i)) = *(animation_frame.GetSkeletonMatrix(i));
//		}
//	}
//}
