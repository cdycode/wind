﻿#ifndef OVR_MOTION_HEADERS_H_
#define OVR_MOTION_HEADERS_H_

#include <list>
#include <map>
#include <vector>
#include <math.h>

#include <wx_base/wx_base.h>
#include <wx_base/wx_matrix4f.h>
#include <wx_base/wx_vector3.h>
#include <wx_base/wx_quaternion.h>

#include <wx_asset_manager/ovr_archive_asset_manager.h>
#include <wx_asset_manager/ovr_archive_skeleton.h>
#include <wx_asset_manager/ovr_archive_skeletal_animation.h>
#include <wx_asset_manager/ovr_motion_frame.h>

#endif