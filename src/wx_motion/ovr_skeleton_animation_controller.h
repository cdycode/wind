#ifndef OVR_MOTION_OVR_SKELETON_ANIMATION_CONTROLLER_H_
#define OVR_MOTION_OVR_SKELETON_ANIMATION_CONTROLLER_H_

#include "ovr_skeletal_animation.h"
#include "ovr_skeleton_tree.h"

namespace ovr_motion 
{

class OvrSkeletonAnimationController 
{
public:
	OvrSkeletonAnimationController() {}
	~OvrSkeletonAnimationController() { Uinit(); }

	bool	Init(ovr_asset::OvrArchiveSkeletalAnimation* a_animation);
	void	Uinit();

	ovr_asset::OvrMotionFrame*	GetCurrentFrame(float a_current_time);

	float	GetDuration()const { return duration; }

private:
	void				InitAnimation(ovr_asset::OvrArchiveSkeletalAnimation* a_animation);

private:

	float							duration = 0.0f;
	std::vector<ovr_asset::OvrMotionFrame*>	motion_frame_list;
	ovr::uint32						frame_num = 0;

};

}
#endif
