﻿#include "headers.h"
#include "ovr_motion_type_define.h"

float GetFloatVale(const char* a_value) 
{
	return *(float*)a_value;
}

bool OvrMotionDataHeader::Load(const char * a_pack_data, int a_pack_len)
{
	for (int i = 0; i < 6; i++)
	{
		id_string[i] = (ovr::int8)a_pack_data[i];
	}
	sample_counter		= *((ovr::uint32*)(a_pack_data + 6));
	datagram_counter	= a_pack_data[10];
	items_number		= a_pack_data[11];
	timecode			= *((ovr::uint32*)(a_pack_data + 12));
	avatar_id			= a_pack_data[16];

	return true;
}

bool OvrMotionSkeletonData::Load(const char * a_pack_data, int a_pack_len)
{
	//标识判断
	if ((a_pack_data[0] != 'M') || (a_pack_data[1] != 'I') || (a_pack_data[2] != 'T') || (a_pack_data[3] != 'P'))
	{
		return false;
	}

	//有效性检测
	if (0 != (a_pack_len - OvrMotionDataHeader::GetSize()) % OvrMotionBoneData::GetSize()) 
	{
		return false;
	}

	if (!header.Load(a_pack_data, a_pack_len))
	{
		return false;
	}
	
	const char* data_buffer = a_pack_data + OvrMotionDataHeader::GetSize();
	for (int i = 0; i < header.items_number; i++)
	{
		ovr::uint32 segment_id = *((ovr::uint32*)data_buffer) - 1;
		data[segment_id].segment_id = segment_id;

		//todo 下面是按照unity的坐标系，如果不是unity的坐标系需要做相应的修改
		//位移
		data[segment_id].position._z = GetFloatVale(data_buffer + 4);
		data[segment_id].position._x = GetFloatVale(data_buffer + 8);
		data[segment_id].position._y = GetFloatVale(data_buffer + 12);

		//姿态四元数
		data[segment_id].quaternion.w = -GetFloatVale(data_buffer + 16);
		data[segment_id].quaternion.z = GetFloatVale(data_buffer + 20);
		data[segment_id].quaternion.x = GetFloatVale(data_buffer + 24);
		data[segment_id].quaternion.y = GetFloatVale(data_buffer + 28);

		data_buffer += OvrMotionBoneData::GetSize();
	}
	return true;
}

bool OvrFaceMorphData::Load(const char * a_pack_data, int a_pack_len)
{
	if (a_pack_len != GetSize() || nullptr == a_pack_data)
	{
		return false;
	}
	memcpy(weight, a_pack_data, 39 * sizeof(float));
	return true;
}

