﻿//#ifndef OVR_MOTION_OVR_SKELETON_TREE_H_
//#define OVR_MOTION_OVR_SKELETON_TREE_H_
//
//#include "ovr_serialize_manage/ovr_motion_frame.h"
//namespace ovr_motion
//{
//	//单个骨骼和单个动画数据的对应关系
//	class OvrSingleSkeletonAnimation
//	{
//	public:
//		OvrSingleSkeletonAnimation() {}
//		~OvrSingleSkeletonAnimation() {}
//
//		void	SetLocalMatrix(const OvrMatrix4& a_matrix)
//		{
//			local_matrix = a_matrix;
//		}
//		void	SetQuaternion(const OvrQuaternion& a_quaternion)
//		{
//			local_quaternion = a_quaternion;
//		}
//		void	SetPosition(const OvrVector3& a_position)
//		{
//			local_position = a_position;
//		}
//
//		void SetAutoCalculateLocalMatrix(bool a_auto_calculate_local_matrix)
//		{
//			is_calculate_local_matrix = a_auto_calculate_local_matrix;
//		}
//
//		void	UpdateWorldMatrix()
//		{
//			OvrMatrix4 localMatrix;
//			if(is_calculate_local_matrix)
//			{
//				OvrMatrix4 current_position;
//				OvrMatrix4 current_pose;
//				current_pose.from_quaternion(local_quaternion);
//				current_position = OvrMatrix4::make_translation_matrix(local_position);
//				localMatrix = current_pose * current_position * local_to_parent_matrix;
//			}
//			else
//			{
//				localMatrix = local_matrix;
//			}
//
//			if (nullptr == parent)
//			{
//				*final_matrix = localMatrix;
//			}
//			else
//			{
//				*final_matrix = localMatrix * (*parent->final_matrix);
//			}
//
//			//递归处理子骨骼
//			for (auto child : child_list)
//			{
//				child->UpdateWorldMatrix();
//			}
//		}
//
//		void UpdateFinalMatrix()
//		{
//			*final_matrix = vertex_to_local_matrix * (*final_matrix);
//
//			//递归处理子骨骼
//			for (auto child : child_list)
//			{
//				child->UpdateFinalMatrix();
//			}
//		}
//
//		OvrMatrix4									vertex_to_local_matrix;	//将mesh中的vertex变换到骨骼空间
//		OvrMatrix4									local_to_parent_matrix;	//将mesh中的vertex变换到骨骼空间
//		OvrMatrix4									bind_pose;				//绑定的姿态
//
//		OvrSingleSkeletonAnimation*					parent = nullptr;
//		std::list<OvrSingleSkeletonAnimation*>		child_list;
//
//		OvrMatrix4		local_matrix;								//骨骼动画的当前矩阵
//		OvrQuaternion	local_quaternion;							//骨骼动画的当前旋转四元数
//		OvrVector3		local_position;								//骨骼动画的当前位置
//		bool			is_calculate_local_matrix = false;			//是否自动计算当前矩阵
//		OvrMatrix4*		final_matrix = nullptr;						//最终的矩阵 local_matrix与parent的localmatrix相乘积 指向OvrrAnimationFrame中的矩阵
//	};
//
//	class OvrSkeletonTree
//	{
//	public:
//		OvrSkeletonTree();
//		~OvrSkeletonTree();
//
//		bool	Init(ovr_serialize::OvrSkeleton* a_skeleton);
//		void	Uinit();
//
//		void	SetLocalMatrix(int a_index, const OvrMatrix4& a_local);
//		void	SetQuaternion(int a_index, const OvrQuaternion& a_local);
//		void	SetPosition(int a_index, const OvrVector3& a_local);
//		void	SetAutoCalculateLocalMatrix(int a_index, bool a_local);
//		void	UpdateMatrix();
//
//		ovr_serialize::OvrMotionFrame*	GetFrame() { return &animation_frame; }
//
//		void	GetDefaultPose(OvrMotionFrame& a_frame);
//	private:
//
//		ovr::uint32						bone_num = 0;
//		OvrSingleSkeletonAnimation*		skeleton_animation_array = nullptr;
//
//		ovr_serialize::OvrMotionFrame					animation_frame;
//	};
//}
//
//#endif
