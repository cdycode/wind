﻿#ifndef OVR_MOTION_TRACK_OVR_MOTION_TYPE_DEFINE_H_
#define OVR_MOTION_TRACK_OVR_MOTION_TYPE_DEFINE_H_

/************************************************************************/
/* 来自动补设备的数据结构                                                 */
/************************************************************************/
// size = 6 + 4 + 1 +1 + 4 + 1+ 7 = 24
struct OvrMotionDataHeader
{
	ovr::int8	id_string[6];//数据头标识
	ovr::uint32 sample_counter;//帧序号
	ovr::uint8	datagram_counter;//数据包序号，目前没用上，可以不用管
	ovr::uint8	items_number;//骨骼数量
	ovr::uint32 timecode;//帧时间戳
	ovr::uint8	avatar_id;//角色ID
	ovr::uint8	reserved[7];//保留数据

	static int GetSize() { return 24; }

	bool	Load(const char * a_pack_data, int a_pack_len);
};

//骨骼 size = 4 + 12 + 16 = 32
struct OvrMotionBoneData
{
	ovr::uint32		segment_id;//骨骼ID
	OvrVector3		position;
	OvrQuaternion	quaternion;

	static int GetSize() { return 32; }
};

// size = 24 + 55 * 32 = 1784
struct OvrMotionSkeletonData
{
	OvrMotionDataHeader	header;
	OvrMotionBoneData	data[55];

	static int GetSize() {return OvrMotionDataHeader::GetSize() + 55 * OvrMotionBoneData::GetSize();}

	bool	Load(const char * a_pack_data, int a_pack_len);
};

struct OvrFaceMorphData
{
	float weight[39];
	static int GetSize() { return sizeof(float) * 39; }

	bool	Load(const char * a_pack_data, int a_pack_len);
};
//face

#endif

