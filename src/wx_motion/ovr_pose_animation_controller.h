#ifndef OVR_MOTION_OVR_POSE_ANIMATION_CONTROLLER_H_
#define OVR_MOTION_OVR_POSE_ANIMATION_CONTROLLER_H_

#include "wx_asset_manager/ovr_archive_pose.h"
#include "ovr_skeleton_tree.h"

namespace ovr_motion 
{

class OvrPoseAnimationController 
{
public:
	OvrPoseAnimationController() {}
	~OvrPoseAnimationController() { Uinit(); }

	bool	Init(ovr_asset::OvrArchivePoseAnimation* a_animation);
	void	Uinit();

	ovr_asset::OvrMorphFrame*	GetCurrentFrame(float a_current_time);
	float	GetDuration()const { return duration; }

private:
	void	InitAnimation(ovr_asset::OvrArchivePoseAnimation* a_animation);

private:

	float							duration = 0.0f;
	std::vector<ovr_asset::OvrMorphFrame*>	pose_frame_list;
	ovr::uint32						frame_num = 0;

};

}
#endif
