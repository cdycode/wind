#include "headers.h"
#include "ovr_skeletal_animation.h"

namespace ovr_motion 
{
OvrBoneAnimation::OvrBoneAnimation() 
{
}
OvrBoneAnimation::~OvrBoneAnimation() 
{
	if (nullptr != local_matrixs)
	{
		delete []local_matrixs;
		local_matrixs = nullptr;
	}
}

bool OvrBoneAnimation::SetFrameNum(ovr::uint32 a_frame_num) 
{
	if (nullptr != local_matrixs)
	{
		delete[]local_matrixs;
		local_matrixs = nullptr;
	}
	frame_num = 0;
	local_matrixs = new OvrMatrix4[a_frame_num];
	if (nullptr != local_matrixs)
	{
		frame_num = a_frame_num;
		return true;
	}
	return false;
}

OvrMatrix4*	OvrBoneAnimation::GetFrame(ovr::uint32 a_frame_index) 
{
	if (a_frame_index < frame_num)
	{
		return &(local_matrixs[a_frame_index]);
	}
	return nullptr;
}

const OvrMatrix4&	 OvrBoneAnimation::GetMatrix(float a_time)
{
	if (frame_num <= 0)
	{
		return uint_matrix;
	}

	float current_time = (float)fmod(a_time, duration);
	int id = (int)(current_time* frame_num / duration);
	if (id == frame_num)
	{
		id -= 1;
	}
	return local_matrixs[id];
}
}