#include "headers.h"
#include "ovr_skeleton_animation_controller.h"
#include <cassert>

namespace ovr_motion 
{

bool OvrSkeletonAnimationController::Init(ovr_asset::OvrArchiveSkeletalAnimation* a_animation)
{

	Uinit();

	if (nullptr == a_animation)
	{
		return false;
		
	}

	InitAnimation(a_animation);

	return true;
}

void OvrSkeletonAnimationController::Uinit() 
{
	auto frame_it = motion_frame_list.begin();
	while (frame_it != motion_frame_list.end())
	{
		(*frame_it)->Uinit();
		delete (*frame_it);
		frame_it++;
	}
	motion_frame_list.clear();
}

ovr_asset::OvrMotionFrame* OvrSkeletonAnimationController::GetCurrentFrame(float a_current_time)
{
	if (frame_num <= 0)
	{
		return nullptr;
	}

	float current_time = a_current_time; // (float)fmod(a_current_time, duration);
	if (current_time < 0)
	{
		current_time = 0;
	}
	else if (current_time > duration)
	{
		current_time = duration;
	}

	ovr::uint32 id = (ovr::uint32)(current_time* frame_num / duration);

	if (id >= frame_num)
	{
		id = frame_num - 1;
	}
	return motion_frame_list[id];
}

void OvrSkeletonAnimationController::InitAnimation(ovr_asset::OvrArchiveSkeletalAnimation* a_animation)
{
	ovr::uint32 frame = a_animation->GetFrameNum();
	if (frame <= 0)
	{
		return;
	}

	duration = a_animation->GetDuration();
	frame_num = a_animation->GetFrameNum();
	for (ovr::uint32 i = 0; i < frame; ++i)
	{
		//��ȡ����ָ��
		ovr_asset::OvrMotionFrame* frame = a_animation->GetFrame(i);
		ovr_asset::OvrMotionFrame* new_frame = new ovr_asset::OvrMotionFrame;
		new_frame->Init(frame->GetJointNum());
		*new_frame = *frame;
		motion_frame_list.push_back(new_frame);
	}
}

}
