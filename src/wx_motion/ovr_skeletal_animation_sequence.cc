#include "headers.h"
#include "wx_motion/ovr_skeletal_animation_sequence.h"
#include "ovr_skeleton_animation_controller.h"

namespace ovr_motion 
{
	OvrSkeletalAnimationSequence::OvrSkeletalAnimationSequence() 
		:animation_controller(nullptr)
	{
	}
	OvrSkeletalAnimationSequence::~OvrSkeletalAnimationSequence() 
	{
		UnBuild();
	}

	bool OvrSkeletalAnimationSequence::Build()
	{
		UnBuild();

		ovr_asset::OvrArchiveAssetManager* manager = ovr_asset::OvrArchiveAssetManager::GetIns();
		ovr_asset::OvrArchiveSkeletalAnimation* animation_asset = (ovr_asset::OvrArchiveSkeletalAnimation*)manager->LoadAsset(animation_file.GetStringConst(), true);
		if (nullptr == animation_asset)
		{
			return false;
		}

		//��ʼ����
		animation_controller = new OvrSkeletonAnimationController;
		if (nullptr == animation_controller)
		{
			return false;
		}

		if (!animation_controller->Init(animation_asset))
		{
			delete animation_controller;
			animation_controller = nullptr;
			return false;
		}
		return (nullptr != animation_controller);
	}

	void OvrSkeletalAnimationSequence::UnBuild()
	{
		if (nullptr != animation_controller)
		{
			delete animation_controller;
			animation_controller = nullptr;
		}
	}

	float OvrSkeletalAnimationSequence::GetDuration()const
	{
		return animation_controller->GetDuration();
	}

	ovr_asset::OvrMotionFrame* OvrSkeletalAnimationSequence::GetAnimation(float a_second)
	{
		if (nullptr != animation_controller)
		{
			return animation_controller->GetCurrentFrame(a_second);
		}
		return nullptr;
	}
}
