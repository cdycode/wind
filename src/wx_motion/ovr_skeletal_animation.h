#ifndef OVR_MOTION_OVR_SKELETAL_ANIMATION_H_
#define OVR_MOTION_OVR_SKELETAL_ANIMATION_H_

#include "wx_base/wx_matrix4f.h"
#include "wx_base/wx_string.h"

namespace ovr_motion 
{

class OvrBoneAnimation 
{
public:
	OvrBoneAnimation();
	~OvrBoneAnimation();

	void				SetName(const OvrString& a_name) { bone_name = a_name; }
	const OvrString&	GetName()const { return bone_name; }

	ovr::uint32	GetFrameNum()const { return frame_num; }
	bool		SetFrameNum(ovr::uint32 a_frame_num);
	OvrMatrix4*	GetFrame(ovr::uint32 a_frame_index);

	void	SetDuration(float a_duration) { duration = a_duration; }
	float	GetDuration()const { return duration; }

	//do not interpolation,just choose the nearest frame value.
	const OvrMatrix4&	 GetMatrix(float a_time);
private:
	OvrString		bone_name;
	ovr::uint32		frame_num = 0;
	OvrMatrix4*		local_matrixs	= nullptr;// root T,other only R
	float			duration		= 0.0f;
	OvrMatrix4      uint_matrix;
};

}

#endif
