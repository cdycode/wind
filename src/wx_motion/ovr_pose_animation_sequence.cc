#include "headers.h"
#include "wx_motion/ovr_pose_animation_sequence.h"
#include "ovr_pose_animation_controller.h"

namespace ovr_motion 
{
	OvrPoseAnimationSequence::OvrPoseAnimationSequence()
		:animation_controller(nullptr)
	{
	}
	OvrPoseAnimationSequence::~OvrPoseAnimationSequence()
	{
		UnBuild();
	}

	bool OvrPoseAnimationSequence::Build()
	{
		UnBuild();
		ovr_asset::OvrArchiveAssetManager* manager = ovr_asset::OvrArchiveAssetManager::GetIns();
		ovr_asset::OvrArchivePoseAnimation* animation_asset = (ovr_asset::OvrArchivePoseAnimation*)manager->LoadAsset(animation_file.GetStringConst(), true);
		if (nullptr == animation_asset)
		{
			return false;
		}

		//��ʼ����
		animation_controller = new OvrPoseAnimationController;
		if (nullptr == animation_controller)
		{
			return false;
		}

		if (!animation_controller->Init(animation_asset))
		{
			delete animation_controller;
			animation_controller = nullptr;
			return false;
		}
		return (nullptr != animation_controller);
	}


	void OvrPoseAnimationSequence::UnBuild()
	{
		if (nullptr != animation_controller)
		{
			delete animation_controller;
			animation_controller = nullptr;
		}
	}

	float OvrPoseAnimationSequence::GetDuration()const
	{
		return animation_controller->GetDuration();
	}

	ovr_asset::OvrMorphFrame* OvrPoseAnimationSequence::GetAnimation(float a_second)
	{
		if (nullptr != animation_controller)
		{
			return animation_controller->GetCurrentFrame(a_second);
		}
		return nullptr;
	}
}
