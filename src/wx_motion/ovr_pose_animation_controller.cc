#include "headers.h"
#include "ovr_pose_animation_controller.h"

namespace ovr_motion 
{

bool OvrPoseAnimationController::Init(ovr_asset::OvrArchivePoseAnimation* a_animation)
{

	Uinit();

	if (nullptr == a_animation)
	{
		return false;
		
	}

	InitAnimation(a_animation);

	return true;
}

void OvrPoseAnimationController::Uinit()
{
	auto frame_it = pose_frame_list.begin();
	while (frame_it != pose_frame_list.end())
	{
		(*frame_it)->Uinit();
		delete (*frame_it);
		frame_it++;
	}
	pose_frame_list.clear();
}

ovr_asset::OvrMorphFrame* OvrPoseAnimationController::GetCurrentFrame(float a_current_time)
{
	if (frame_num <= 0)
	{
		return nullptr;
	}

	float current_time = (float)fmod(a_current_time, duration);
	int id = (int)(current_time* frame_num / duration);
	if (id == frame_num)
	{
		id -= 1;
	}
	return pose_frame_list[id];
}

void OvrPoseAnimationController::InitAnimation(ovr_asset::OvrArchivePoseAnimation* a_animation)
{
	ovr::uint32 frame = a_animation->GetFrameNum();
	if (frame <= 0)
	{
		return;
	}

	duration = a_animation->GetDuration();
	frame_num = a_animation->GetFrameNum();
	for (ovr::uint32 i = 0; i < frame; ++i)
	{
		//��ȡ����ָ��
		ovr_asset::OvrMorphFrame* frame = a_animation->GetFrame(i);
		ovr_asset::OvrMorphFrame* new_frame = new ovr_asset::OvrMorphFrame;
		new_frame->Init(frame->GetPoseNum());
		*new_frame = *frame;
		pose_frame_list.push_back(new_frame);
	}
}

}
