﻿#include "headers.h"
#include "wx_project/ovr_preview_scene.h"

#include "wx_core/ovr_scene_context.h"
#include "wx_project/ovr_scene_event_manager.h"
#include <wx_core/ovr_scene_camera.h>

namespace ovr_project 
{
	static OvrPreviewScene* sg_render_scene = nullptr;

	OvrPreviewScene* OvrPreviewScene::GetIns()
	{
		if (nullptr != sg_render_scene)
		{
			return sg_render_scene;
		}
		sg_render_scene = new OvrPreviewScene;
		return sg_render_scene;
	}

	void OvrPreviewScene::DelIns()
	{
		if (nullptr != sg_render_scene)
		{
			sg_render_scene->Uninit();
			delete sg_render_scene;
			sg_render_scene = nullptr;
		}
	}

	bool OvrPreviewScene::Init(IOvrRenderContext* a_context)
	{
		assert(!is_working && a_context != nullptr);

		render_context = a_context;
		current_scene = new ovr_core::OvrSceneContext;

		is_working = true;
		work_thread = new std::thread(&OvrPreviewScene::WorkThread, this);
		if (work_thread != nullptr)
		{
			std::unique_lock<std::mutex> init_lock(init_mutex);
			init_condition.wait(init_lock);
			return true;
		}
		
		is_working = false;
		return false;
	}

	void OvrPreviewScene::Uninit() 
	{
		if (nullptr != work_thread)
		{
			is_working = false;
			work_thread->join();
			delete work_thread;
			work_thread = nullptr;
		}
		render_context = nullptr;

		delete current_scene;
		current_scene = nullptr;

		delete capture_monitor;
		capture_monitor = nullptr;
	}

	void OvrPreviewScene::Reshape(ovr::int32 a_new_width, ovr::int32 a_new_height) 
	{
		render_width = a_new_width;
		render_height = a_new_height;
		is_size_change = true;
	}

	void OvrPreviewScene::setMonitor(OvrCaptureMonitor* a_capture_monitor)
	{
		capture_monitor = a_capture_monitor;
	}

	void OvrPreviewScene::CreateActor(const OvrString& a_mesh_file_path)
	{
		ovr_engine::OvrActor* new_actor = current_scene->CreateActor(a_mesh_file_path);
		setActor(new_actor);
	}

	void OvrPreviewScene::setActor(ovr_engine::OvrActor* a_actor)
	{
		if (a_actor != actor)
		{
			actor = a_actor;
			is_actor_changed = true;
		}
	}

	void OvrPreviewScene::ChangeCaptureDevice(const OvrString& a_device_name)
	{
		if (device_name == a_device_name)
		{
			return;
		}
		device_name = a_device_name;
		is_device_changed = true;
	}

	void OvrPreviewScene::WorkThread() 
	{
		assert(current_scene != nullptr);

		render_context->OnInit();
		Reshape(render_context->GetWidth(), render_context->GetHeight());

		current_scene->Init();

		//通知初始化函数  初始化完毕
		init_condition.notify_one();

		while (is_working)
		{
			//同步帧速率
			SyncFrameTime(last_frame_time, current_time);

			//同步窗口大小
			if (is_size_change)
			{
				current_scene->GetSceneCamera()->Reshape(render_width, render_height);
				is_size_change = false;
			}

			//更新鼠标事件响应
			//OvrSceneEventManager::GetIns()->Update(current_scene);

			//数据的OpenGL渲染
			DoRenderScene();

			//OpenGL 数据提交
			render_context->OnRender();

			last_frame_time = current_time;
		}

		current_scene->Uninit();
		render_context->OnUninit();
	}

	void OvrPreviewScene::DoRenderScene()
	{
		if (capture_monitor != nullptr)
		{
			if (is_actor_changed)
			{
				capture_monitor->setActor(actor);
				is_actor_changed = false;
			}
			if (is_device_changed)
			{
				capture_monitor->ChangeCaptureDevice(device_name);
				is_device_changed = false;
			}
			capture_monitor->UpdateCaptureDeviceData();
		}
		current_scene->Update();
		current_scene->Draw();
	}

	void OvrPreviewScene::SyncFrameTime(double a_last_frame_time, double& a_current_time)
	{
		a_current_time = OvrTime::GetSeconds();

		//本帧渲染时间 与 当前时间的差
		double diff_time = (a_last_frame_time + 1.0 / current_fps) - a_current_time;
		if (diff_time > 0)
		{
			//临时使用此函数  后面将换掉
			Sleep((ovr::uint32)(diff_time * 1000));

			//更新当前时间
			a_current_time = OvrTime::GetSeconds();
		}
	}

	bool OvrCaptureMonitor::UpdateCaptureDeviceData()
	{
		if (device_name == "" || actor == nullptr)
		{
			return false;
		}
		return true;
	}
}

