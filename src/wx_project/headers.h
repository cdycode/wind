﻿#ifndef OVR_PROJECT_HEADERS_H_
#define OVR_PROJECT_HEADERS_H_

#include <io.h>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <memory>
#include <windows.h>
#include <cassert>
#include <algorithm>

#include <iostream>
#include <fstream>

#include <wx_base/wx_type.h>
#include <wx_base/wx_time.h>
#include <wx_base/wx_unique_name.h>
#include <wx_base/wx_file_tools.h>
#include <wx_base/wx_plane3f.h>
#include <wx_base/wx_archive.h>
#include <wx_base/wx_singleton.h>

//#include <wx_engine/ovr_mesh_actor.h>

#include <wx_engine/ovr_axis_system.h>
#include <wx_engine/ovr_actor_axis.h>
//#include <wx_engine/ovr_ground_plane.h>
//#include <wx_engine/ovr_empty_actor.h>
#include <wx_engine/ovr_mesh_manager.h>
//#include <wx_engine/ovr_camera_actor.h>
#include <wx_engine/ovr_scene.h>
#include <wx_engine/ovr_ray_scene_query.h>
#include <wx_engine/ovr_engine.h>
//#include <wx_engine/ovr_light_actor.h>
#include <wx_engine/ovr_media_texture.h>

#include <wx_motion/ovr_skeletal_animation_sequence.h>
#include <wx_motion/ovr_pose_animation_sequence.h>

#include <wx_core/ovr_core.h>

//#include <wx_serialize_manage/ovr_serialize_macro.h>

#include <wx_asset_manager/ovr_archive_asset_manager.h>
#include <wx_asset_manager/ovr_archive_mesh.h>
#include <wx_asset_manager/ovr_archive_skeleton.h>
#include <wx_asset_manager/ovr_archive_material.h>
#include <wx_asset_manager/ovr_motion_frame.h>

#include <wx_fbx_import/ovr_fbx_import.h>

#include <wx_project/ovr_project.h>

#endif

