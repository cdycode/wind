﻿#include "headers.h"
#include <wx_project/ovr_asset_manager.h>
#include <wx_project/ovr_scene_editor.h>
#include <wx_core/ovr_scene_context.h>
#include <wx_core/ovr_sequence.h>
#include <wx_plugin/wx_i_archive_project.h>

#include <wx_engine/ovr_material.h>
#include <wx_engine/ovr_serializer_impl.h>
#include <wx_engine/ovr_material_manager.h>

#include "import/ovr_importer.h"
#include "import/ovr_import_manager.h"

#include <io.h>
#include<stdio.h>
#include <direct.h>

#include <fstream>

#define OVR_STR_TO_LOWER(STR)									\
{																\
	auto& f = std::use_facet<std::ctype<char>>(std::locale());	\
	f.tolower(&str_ext[0], &str_ext[0]+str_ext.size());			\
}

namespace ovr_project 
{
	const static std::vector<OvrString> s_res_sub_dirs{
		"content",
		"project",
		"project/plot",
		"project/scene",
		"project/sequence",
		"project/config",
		//"system/content/base_obj/",
		//"system/content/texture/"
	};

	OvrAssetManager::OvrAssetManager() 
		//:importer(nullptr)
	{
		//fbx_importer = new OvrFbxImporter;
	}
	OvrAssetManager::~OvrAssetManager()
	{
	}

	bool OvrAssetManager::Open(const char* a_project_file_path) 
	{
		if (!OvrFileTools::GetPathFromFullPath(a_project_file_path, work_dir))
		{
			return false;
		}
		return true;
	}

	void OvrAssetManager::Close() 
	{
		work_dir = "";
	}

	bool OvrAssetManager::IsAssetExist(const char* a_asset_file) 
	{
		return false;
	}

	static std::mutex g_mgr_mutex;
	bool OvrAssetManager::ImportFiles(
		const std::vector<std::string>& a_asset_files,
		const char * a_out_dir,
		std::function<bool(const char*, bool)> &a_notify_callback)
	{
		//// for test 
		//bool resuto = a_notify_callback("d:/test/a.jpg", true);
		//// test end

		OvrImportManager local_import_mgr;
		import_mgr = &local_import_mgr;

		std::list<OvrString> asset_list;
		bool b_result = import_mgr->Import(a_asset_files, a_out_dir, asset_list,a_notify_callback);
		if (b_result) {
			for (auto file : asset_list)
			{
				ovr_asset::OvrAssetType asset_type = ovr_asset::OvrArchiveAssetManager::GetAssetType(file.GetStringConst());
				//assert(asset_type != ovr_asset::kAssetInvalid);
				AppendAsset(asset_type, file.GetStringConst());
			}
		}

		std::lock_guard<std::mutex> guard(g_mgr_mutex);
		import_mgr = nullptr;
		return b_result;
	}

	int OvrAssetManager::GetImportProgress()
	{
		std::lock_guard<std::mutex> guard(g_mgr_mutex);
		return import_mgr != nullptr ? import_mgr->GetPercent() : 0;
	}

	void OvrAssetManager::CancelImport() 
	{
		std::lock_guard<std::mutex> guard(g_mgr_mutex);
		if (import_mgr != nullptr) {
			import_mgr->CancelImport();
		}
	}

	void OvrAssetManager::AppendAsset(const ovr::uint32 a_asset_type, const OvrString& a_file_path)
	{
		// 过滤重复资源, 只真对 <资源> 有效
		for (auto & res_info : asset_list) 
		{
			if (res_info.asset_type == a_asset_type && res_info.file_path == a_file_path) 
			{
				// 说明是完全重复的, 不加增加
				return;
			}
		}

		asset_list.push_back(OvrAssetInfo(a_asset_type, a_file_path));
	}

	void OvrAssetManager::CopyShareFiles(const char* aDestDir, const char * aSrc)
	{
		// dest: dir ,if not exist , create; src : file or dir, if dir copy recursive;
		std::function<void(const std::string &, const std::string &)> copy_files_function =
			[&copy_files_function](const std::string &dest, const std::string &src) -> void {
			if (_access(src.c_str(), 0) != 0) {
				return;
			}

			const std::string src_path = src[src.length() - 1] != '/' ? src : src.substr(0, src.length() - 1);
			const std::string dest_dir = dest[dest.length() - 1] != '/' ? dest + "/" : dest;

			const std::string src_last_name = src_path.substr(src_path.rfind('/') + 1);
			const std::string dest_path = dest_dir + src_last_name;

			long h_file = 0;
			struct _stat buf = { 0 };
			_stat(src_path.c_str(), &buf);
			if (buf.st_mode & _S_IFDIR) {  // dir
				_mkdir(dest_path.c_str());

				struct _finddata_t fileinfo;
				if ((h_file = _findfirst((src_path + "/*").c_str(), &fileinfo)) != -1) {
					do {
						if (strcmp(fileinfo.name, ".") != 0 && strcmp(fileinfo.name, "..") != 0) {
							std::string cur_src_file_path = src_path + "/" + fileinfo.name;
							copy_files_function(dest_path, cur_src_file_path);
						}
					} while (_findnext(h_file, &fileinfo) == 0);  //寻找下一个，成功返回0，否则-1
					_findclose(h_file);
				}
			}
			else { // file
				std::ifstream src_file(src, std::ios::binary);
				std::ofstream dest_file(dest_path, std::ios::binary);
				if (!src_file.is_open() || !dest_file.is_open()) {
					return;
				}

				const uint32_t cbs = 1024;
				char buff[cbs] = { 0 };
				while (!src_file.eof()) {
					src_file.read(buff, cbs);
					dest_file.write(buff, src_file.gcount());
				}
			} // else
		};

		copy_files_function(aDestDir, aSrc);
	}

	bool OvrAssetManager::DeleteAsset(const char* a_file) 
	{
		return true;
	}

	bool OvrAssetManager::CreateProject(const char* a_abs_file_path)
	{
		OvrString temp_work_dir;
		//获取工作路径
		if (!OvrFileTools::GetPathFromFullPath(a_abs_file_path, temp_work_dir))
		{
			return false;
		}

		//创建Project文件 *.ovrp *.oproject
		if (!OvrFileTools::MakeFile(a_abs_file_path))
		{
			return false;
		}

		//创建其他目录
		for (auto &sub_dir : s_res_sub_dirs) {
			OvrString sub_abs_dir = temp_work_dir + sub_dir;
			if (!OvrFileTools::CreateFolder(sub_abs_dir.GetStringConst())) {
				return false;
			}
		}

#ifdef _WIN32
		TCHAR exeFullPath[MAX_PATH] = { 0 };
		GetModuleFileName(NULL, exeFullPath, MAX_PATH);
#ifdef UNICODE
		char mb_full_path[MAX_PATH * 2] = { 0 };
		std::wcstombs(mb_full_path, exeFullPath, MAX_PATH);
		std::string str_exe_path = mb_full_path;
#else
		std::string str_exe_path = exeFullPath;
#endif // UNICODE
		std::replace_if(str_exe_path.begin(), str_exe_path.end(),
			std::bind(std::equal_to<int>(), std::placeholders::_1, '\\'), '/');
		int n_last_sprit_index = str_exe_path.rfind('/');
		std::string src_exe_dir = str_exe_path.substr(0, n_last_sprit_index + 1);
#else
#error "please realize the code!"
		char mb_full_path[PATH_MAX] = { 0 };
		int n = readlink("/proc/self/exe", mb_full_path, PATH_MAX);
		std::string src_exe_dir = mb_full_path;
#endif // _WIN32

		std::string const_pro_sys_dir = src_exe_dir + "system";
		CopyShareFiles(temp_work_dir.GetStringConst(), const_pro_sys_dir.c_str());

		return wx_plugin::wxIArchiveProject::Load()->CreateProject(a_abs_file_path);
	}

	ovr_engine::OvrMaterial* OvrAssetManager::LoadMaterial(const char * a_file_path)
	{
		auto mat_mgr = ovr_engine::OvrMaterialManager::GetIns();
		auto get_mat = mat_mgr->Load(a_file_path);
		return get_mat;
	}

	bool OvrAssetManager::CreateMat(const char * a_file_path)
	{
		auto mat_mgr = ovr_engine::OvrMaterialManager::GetIns();
		auto default_mat = mat_mgr->GetDefaultMaterial();

		if (!default_mat)
			return false;

		auto serialer_imp = ovr_engine::OvrSerializerImpl::GetIns();

		bool result = serialer_imp->ExportMaterial(a_file_path, default_mat);
		return result;
	}

	bool OvrAssetManager::CreateSence(const char* a_file_path) 
	{
		bool is_success = false;

		OvrString full_path = work_dir + a_file_path;
	
		OvrString unique_name;
		UniqueNameCreater::CreateSenceUniqueName(unique_name);

		if (wx_plugin::wxIArchiveProject::Load()->CreateScene(full_path, unique_name))
		{
			//保存信息
			scene_list.push_back(OvrProjectFileInfo(a_file_path, unique_name));

			is_success = true;
		}

		return is_success;
	}
	
	bool OvrAssetManager::CreateSequcence(const char* a_scene_unique_name, const char* a_sequence_file_path)
	{
		OvrString full_path = work_dir + a_sequence_file_path;

		//创建sequence 并保存
		OvrString unique_name;
		UniqueNameCreater::CreateSequenceUniqueName(unique_name);
		if (!wx_plugin::wxIArchiveProject::Load()->CreateSequence(full_path, a_scene_unique_name, unique_name))
		{
			return false;
		}
		
		//在Scene中保存Sequence信息
		ovr_core::OvrSceneContext* current_scene = OvrProject::GetIns()->GetCurrentScene();
		if (nullptr != current_scene && current_scene->GetUniqueName() == a_scene_unique_name)
		{
			//场景已经打开 直接进行保存
			current_scene->AppendSequence(unique_name);
		}
		else
		{
			//场景未打开 需要进行打开 修改 然后再保存
			OvrString file_path;
			if (!GetSceneFilePath(a_scene_unique_name, file_path))
			{
				return false;
			}
			OvrString full_path = work_dir + file_path;
			if (!wx_plugin::wxIArchiveProject::Load()->AddSequenceToScene(full_path, unique_name))
			{
				return false;
			}
		}

		//保存信息
		sequence_list.push_back(OvrProjectFileInfo(a_sequence_file_path, unique_name));
		return true;
	}

	bool OvrAssetManager::GetPlotFilePath(const OvrString& a_unique_name, OvrString& a_file_path) 
	{
		bool is_found = false;
		auto plot_it = plot_list.begin();
		while (plot_it != plot_list.end())
		{
			if (a_unique_name == plot_it->unique_name)
			{
				a_file_path = plot_it->file_path;
				is_found = true;
				break;
			}
			plot_it++;
		}
		return is_found;
	}

	bool OvrAssetManager::GetSequenceFilePath(const OvrString& a_unique_name, OvrString& a_file_path) 
	{
		bool is_found = false;
		auto sequence_it = sequence_list.begin();
		while (sequence_it != sequence_list.end())
		{
			if (a_unique_name == sequence_it->unique_name)
			{
				a_file_path = sequence_it->file_path;
				is_found = true;
				break;
			}
			sequence_it++;
		}
		return is_found;
	}

	bool OvrAssetManager::GetSceneFilePath(const OvrString& a_unique_name, OvrString& a_file_path) 
	{
		bool is_found = false;
		auto scene_it = scene_list.begin();
		while (scene_it != scene_list.end())
		{
			if (a_unique_name == scene_it->unique_name)
			{
				a_file_path = scene_it->file_path;
				is_found = true;
				break;
			}
			scene_it++;
		}
		return is_found;
	}

	bool OvrAssetManager::GetAssetDisplayName(const OvrString& a_file_path, OvrString& a_display_name) 
	{
		return OvrFileTools::GetFileNameFromPath(a_file_path.GetStringConst(), a_display_name);
	}

	bool OvrAssetManager::RenameAssetFile(const OvrString & a_src_path, const OvrString & a_new_path)
	{
		for (auto & ass_file : asset_list)
		{
			if (ass_file.file_path == a_src_path) 
			{
				ass_file.file_path = a_new_path;
				return true;
			}
		}

		return false;
	}

	bool OvrAssetManager::RenamePlotFilePath(const OvrString & a_unique_name, const OvrString & a_file_path)
	{
		bool is_found = false;
		auto plot_it = plot_list.begin();
		while (plot_it != plot_list.end())
		{
			if (a_unique_name == plot_it->unique_name)
			{
				plot_it->file_path = a_file_path;
				is_found = true;
				break;
			}
			plot_it++;
		}
		return is_found;
	}

	bool OvrAssetManager::RenameSequenceFilePath(const OvrString & a_unique_name, const OvrString & a_file_path)
	{
		bool is_found = false;
		auto sequence_it = sequence_list.begin();
		while (sequence_it != sequence_list.end())
		{
			if (a_unique_name == sequence_it->unique_name)
			{
				sequence_it->file_path = a_file_path;
				is_found = true;
				break;
			}
			sequence_it++;
		}
		return is_found;
	}

	bool OvrAssetManager::RenameSceneFilePath(const OvrString & a_unique_name, const OvrString & a_file_path)
	{
		bool is_found = false;
		auto scene_it = scene_list.begin();
		while (scene_it != scene_list.end())
		{
			if (a_unique_name == scene_it->unique_name)
			{
				scene_it->file_path = a_file_path;
				is_found = true;
				break;
			}
			scene_it++;
		}
		return is_found;
	}

	const OvrProjectFileInfo* OvrAssetManager::GetSceneInfo(const OvrString& a_file_path)const
	{
		auto it = scene_list.begin();
		while (it != scene_list.end())
		{
			if (it->file_path == a_file_path)
			{
				return &(*it);
			}
			it++;
		}
		return nullptr;
	}

	bool OvrAssetManager::GetAssetFileList(const char* a_dir, std::list<OvrAssetInfo>& a_asset_list)
	{
		return false;
	}
}
