#include "headers.h"
#include "wx_project/ovr_scene_event_manager.h"
#include "wx_project/ovr_project.h"
#include "wx_core/ovr_scene_context.h"
#include "wx_core/ovr_sequence.h"
#include "wx_core/ovr_scene_camera.h"
#include <wx_engine/ovr_actor.h>
#include <wx_project/ovr_scene_editor.h>

namespace ovr_project
{
	OvrSceneEventManager::OvrSceneEventManager()
	{
	}
	OvrSceneEventManager::~OvrSceneEventManager()
	{
	}

	void OvrSceneEventManager::Reshape(ovr::int32 a_new_width, ovr::int32 a_new_height) 
	{
		render_width = a_new_width;
		render_height = a_new_height;
	}

	void OvrSceneEventManager::MouseMove(int a_x_pos, int a_y_pos)
	{
		if (!IsResponse())
		{
			return;
		}

		//QT的窗口原点在左上方  引擎的原点在左下方
		mouse_x_pos = a_x_pos*1.0f / render_width;
		mouse_y_pos = 1.0f - a_y_pos *1.0f / render_height;
	}

	void OvrSceneEventManager::BeginDragAsset(const char* a_asset_path)
	{
		if (!IsResponse())
		{
			return;
		}

		drag_asset_file_path = a_asset_path;
		is_dragging = true;
	}
	void OvrSceneEventManager::EndDragAsset(bool a_is_add)
	{
		is_add_drag_asset = a_is_add;
		is_dragging = false;
	}

	void OvrSceneEventManager::MouseLeftButtonPressed(bool a_is_pressed)
	{
		if (!IsResponse())
		{
			return;
		}

		if (a_is_pressed == left_btn_pressed_new)
		{
			return;
		}
		if (a_is_pressed)
		{
			pressed_begin_time = (float)OvrTime::GetSeconds();
		}
	
		left_btn_pressed_new = a_is_pressed;
		is_left_btn_changed = true;
	}

	void OvrSceneEventManager::Update(OvrSceneEditor* a_scene)
	{
		if (nullptr == a_scene)
		{
			return;
		}

		//优先处理拖拽事件
		if (DealDragEvent(a_scene))
		{
			return;
		}

		if (is_left_btn_changed)
		{
			//按键有变化 只处理单击事件 
			if (!left_btn_pressed_new)
			{
				//左键抬起
				float diff_time = (float)OvrTime::GetSeconds() - pressed_begin_time;
				if (diff_time < 0.15f)
				{
					//单击事件
					a_scene->SelectedActor(mouse_x_pos, mouse_y_pos);
				}
			}

			left_btn_pressed_current = left_btn_pressed_new;
		}
	}

	bool OvrSceneEventManager::IsResponse() 
	{
		if (nullptr == OvrProject::GetIns()->GetCurrentSequence())
		{
			return false;
		}

		ovr_core::OvrRenderStatus current_status = OvrProject::GetIns()->GetCurrentSequence()->GetCurrentStatus();
		if (current_status == ovr_core::kRenderPlay)
		{
			return false;
		}
		return true;
	}

	bool OvrSceneEventManager::DealDragEvent(OvrSceneEditor* a_scene)
	{
		bool is_dragging_temp = is_dragging;
		if (!is_dragging_temp)
		{
			//拖拽结束
			if (is_drag_asset_file_current)
			{
				DealEndDrag(a_scene);
				is_drag_asset_file_current = false;
			}

			return false;
		}

		if (is_dragging_temp != is_drag_asset_file_current)
		{
			//开始拖拽
			current_drag_actor = a_scene->GetSceneContext()->CreateActor(drag_asset_file_path);
			is_drag_asset_file_current = is_dragging_temp;
		}
		else 
		{
			//拖拽过程中
			//获取鼠标射线
			OvrRay3f ray = a_scene->GetSceneContext()->GetSceneCamera()->GeneratePickRay(mouse_x_pos, mouse_y_pos);
			OvrVector3 new_position = ray.GetOrigin() + ray.GetDir() * 2;

			current_drag_actor->SetPosition(new_position);
		}
		return true;
	}

	void OvrSceneEventManager::DealEndDrag(OvrSceneEditor* a_scene)
	{
		if (is_add_drag_asset)
		{
			//通知UI
			OvrSceneListener* scene_listener = OvrProject::GetIns()->GetListener();
			if (nullptr != scene_listener)
			{
				scene_listener->OnAddActor(current_drag_actor->GetUniqueName());
			}
		}
		else
		{
			a_scene->RemoveActor(current_drag_actor);
		}
	}
}
