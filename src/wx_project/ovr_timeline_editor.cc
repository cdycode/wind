﻿#include "headers.h"
#include <wx_engine/ovr_actor.h>
#include <wx_core/ovr_sequence.h>
#include <wx_core/ovr_scene_context.h>
#include <wx_core/track/ovr_actor_track.h>
#include <wx_project/ovr_timeline_editor.h>
#include <wx_project/ovr_scene_editor.h>
#include <wx_plugin/wx_i_archive_project.h>

namespace ovr_project 
{
	bool OvrTimelineEditor::Open(const OvrString& a_file_path) 
	{
		Close();

		sequence = new ovr_core::OvrSequence;
		if (nullptr == sequence)
		{
			return false;
		}
		sequence->SetFilePath(a_file_path);
		if (!sequence->Init())
		{
			Close();
			return false;
		}
		
		if (!wx_plugin::wxIArchiveProject::Load()->LoadSequence(this))
		{
			Close();
			return false;
		}
		return true;
	}

	void OvrTimelineEditor::Close() 
	{
		if (nullptr != sequence)
		{
			sequence->Uninit();
			delete sequence;
			sequence = nullptr;
		}
	}

	void OvrTimelineEditor::Update() 
	{
		if (nullptr != sequence)
		{
			sequence->Update();
		}
	}

	const OvrString& OvrTimelineEditor::GetFilePath()const 
	{
		if (nullptr != sequence)
		{
			return sequence->GetFilePath();
		}
		return OvrString::empty;
	}

	void OvrTimelineEditor::SetRecord(bool a_is_record) 
	{
		if (nullptr != sequence)
		{
			sequence->SetRecord(a_is_record);
		}
	}
	bool OvrTimelineEditor::IsRecord()const 
	{
		if (nullptr != sequence)
		{
			return sequence->IsRecord();
		}
		return false;
	}

	ovr::uint64	OvrTimelineEditor::GetCurrentFrame()const 
	{
		if (nullptr != sequence)
		{
			return sequence->GetCurrentFrame();
		}
		return 0;
	}

	void OvrTimelineEditor::SetScene(OvrSceneEditor* a_scene)
	{
		current_scene = a_scene;
		if (nullptr != sequence)
		{
			sequence->SetScene(a_scene->GetSceneContext());
		}
	}

	ovr::uint32 OvrTimelineEditor::GetAddActorList(std::list<OvrActorName>& a_actor_name_list)
	{
		if (!IsOpen())
		{
			return 0;
		}

		//获取可以添加到轨道上的Actor
		const std::map<OvrString, ovr_engine::OvrActor*>& actor_map = sequence->GetScene()->GetActorList();
		for (auto it = actor_map.begin(); it != actor_map.end(); it++)
		{
			if (!current_scene->IsShowInScene(it->second))
			{
				continue;
			}
			a_actor_name_list.push_back(OvrActorName(it->second->GetDisplayName(), it->second->GetUniqueName()));
		}
		return a_actor_name_list.size();
	}

	ovr_core::OvrActorTrack* OvrTimelineEditor::AddActorTrack(const OvrString& a_actor_unique_name)
	{
		if (!IsOpen())
		{
			return nullptr;
		}

		ovr_engine::OvrActor* actor = current_scene->GetActorByUniqueName(a_actor_unique_name);
		if (nullptr == actor)
		{
			return nullptr;
		}

		ovr_core::OvrActorTrack* new_track = dynamic_cast<ovr_core::OvrActorTrack*>(sequence->AddTrack(ovr_core::kTrackCommon, "actor"));
		if (nullptr != new_track)
		{
			new_track->SetActor(actor);
		}
		return new_track;
	}
}