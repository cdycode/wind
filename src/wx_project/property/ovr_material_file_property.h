﻿#ifndef OVR_PROJECT_OVR_MATERIAL_FILE_PROPERTY_H_
#define OVR_PROJECT_OVR_MATERIAL_FILE_PROPERTY_H_

#include <wx_project/property/i_ovr_material_property.h>

namespace ovr_asset 
{
	class OvrArchiveMaterial;
}

namespace ovr_project 
{
	class OvrMaterialFileProperty : public IOvrMaterialProperty 
	{
	public:
		OvrMaterialFileProperty() {}
		virtual ~OvrMaterialFileProperty();

		//设置材质路径
		bool			Init(const OvrString& a_file_path);

		//设置材质类型
		virtual bool	SetType(const OvrString& a_name) override;

		//设置参数值
		virtual bool	SetParamValue(const OvrString& a_display_name, const OvrString& a_value) override;

		virtual bool	Save() override;

	private:

		//填充参数列表
		bool	FillParamList();

		//设置参数的值
		void	FillParamValueFromFile();

		//获取融合值
		void	GetBlendValue(OvrString& a_value);

		//物理名和显示名之间相互查询
		void	GetTextureDisplayName(const OvrString& a_gpu_name, OvrString& a_display_name);

		//根据字符串 获取Vector4
		bool	GetVector4(const OvrString& a_string, OvrVector4& a_value);

		//根据名字获取值
		bool	GetBlendValueFromName(const OvrString& a_display_name, ovr::int32& a_src_value, ovr::int32& a_dst_value);

	private:
		ovr_asset::OvrArchiveMaterial* material_file = nullptr;	//序列化实例

		ovr_engine::OvrMaterial*	material_ins = nullptr;			//引擎实例
	};
}

#endif
