﻿#pragma once

#include "../headers.h"
#include "wx_project/property/ovr_actor_property.h"
#include <cassert>

#define kFloatZero 0.000001
#define Float_Equal(f1,f2) (fabs(f1 - f2) <= kFloatZero)

namespace ovr_project 
{
	IOvrActorProperty* IOvrActorProperty::Create(OvrActorPropertyType a_type, const OvrString& a_id, const OvrString& a_name, void* a_value, IOvrActorPropertyGroup* a_parent)
	{
		assert(a_id.GetStringSize() > 0 && a_name.GetStringSize() > 0);

		IOvrActorProperty* obj = nullptr;

		switch (a_type)
		{
		case ovr_project::kOPTBool:			obj = new OvrActorPropertyBool(a_value, a_parent);		break;
		case ovr_project::kOPTFloat:		obj = new OvrActorPropertyFloat(a_value, a_parent);		break;
		case ovr_project::kOPTVector2f:		obj = new OvrActorPropertyVector2f(a_value, a_parent);	break;
		case ovr_project::kOPTVector3f:		obj = new OvrActorPropertyVector3f(a_value, a_parent);	break;
		case ovr_project::kOPTVector4f:		obj = new OvrActorPropertyVector4f(a_value, a_parent);	break;
		case ovr_project::kOPTString:		obj = new OvrActorPropertyString(a_value, a_parent);	break;
		case ovr_project::kOPTComboBox:		obj = new OvrActorPropertyComboBox(a_value, a_parent);	break;
		case ovr_project::kOPTRes:			obj = new OvrActorPropertyRes(a_value, a_parent);		break;
		default:
			assert(false);
			break;
		}

		if (obj != nullptr)
		{
			obj->property_id = a_id;
			obj->property_name = a_name;
		}
		return obj;
	}

	///////////////////////////////////////////////
	// class OvrActorPropertyBool

	OvrActorPropertyBool::OvrActorPropertyBool(void* a_value, IOvrActorPropertyGroup* a_parent) :IOvrActorProperty(a_parent, kOPTBool)
	{
		if (a_value != nullptr)
		{
			value = *(bool*)a_value;
		}
		property_type_name = "kOPTBool";
	}

	void OvrActorPropertyBool::InitValue(void* a_value)
	{
		value = *(bool*)a_value;
	}

	bool OvrActorPropertyBool::SetValue(bool a_value)
	{
		if (a_value != value)
		{
			value = a_value;
			OnValueChanged();
			return true;
		}
		return false;
	}

	///////////////////////////////////////////////
	// class OvrActorPropertyFloat

	OvrActorPropertyFloat::OvrActorPropertyFloat(void* a_value, IOvrActorPropertyGroup* a_parent) :IOvrActorProperty(a_parent, kOPTFloat)
	{
		if (a_value != nullptr)
		{
			value = *(float*)a_value;
		}
		property_type_name = "kOPTFloat";
	}

	void OvrActorPropertyFloat::InitValue(void* a_value)
	{
		value = *(float*)a_value;
	}

	bool OvrActorPropertyFloat::SetValue(float a_value)
	{
		if (!Float_Equal(a_value, value))
		{
			if (is_limit_range)
			{
				if (a_value < min_value || a_value > max_value)
				{
					return false;
				}
			}
			value = a_value;
			OnValueChanged();
			return true;
		}
		return false;
	}

	void OvrActorPropertyFloat::SetRange(float a_min_value, float a_max_value)
	{
		is_limit_range = true;
		min_value = a_min_value;
		max_value = a_max_value;
	}

	void OvrActorPropertyFloat::GetRange(float& a_min_value, float& a_max_value)
	{
		a_min_value = min_value;
		a_max_value = max_value;
	}

	///////////////////////////////////////////////
	// class OvrActorPropertyVector2f

	OvrActorPropertyVector2f::OvrActorPropertyVector2f(void* a_value, IOvrActorPropertyGroup* a_parent) :IOvrActorProperty(a_parent, kOPTVector2f)
	{
		if (a_value != nullptr)
		{
			value = *(OvrVector2*)a_value;
		}
		property_type_name = "kOPTVector2f";
	}

	void OvrActorPropertyVector2f::InitValue(void* a_value)
	{
		value = *(OvrVector2*)a_value;
	}

	bool OvrActorPropertyVector2f::SetValue(const OvrVector2& a_value)
	{
		if (a_value == value)
		{
			return false;
		}

		value = a_value;
		OnValueChanged();
		return true;
	}

	///////////////////////////////////////////////
	// class OvrActorPropertyVector3f

	OvrActorPropertyVector3f::OvrActorPropertyVector3f(void* a_value, IOvrActorPropertyGroup* a_parent) :IOvrActorProperty(a_parent, kOPTVector3f)
	{
		if (a_value != nullptr)
		{
			value = *(OvrVector3*)a_value;
		}
		property_type_name = "kOPTVector3f";
	}

	void OvrActorPropertyVector3f::InitValue(void* a_value)
	{
		value = *(OvrVector3*)a_value;
	}

	bool OvrActorPropertyVector3f::SetValue(const OvrVector3& a_value)
	{
		if (a_value == value)
		{
			return false;
		}

		value = a_value;
		OnValueChanged();
		return true;
	}

	///////////////////////////////////////////////
	// class OvrActorPropertyVector4f

	OvrActorPropertyVector4f::OvrActorPropertyVector4f(void* a_value, IOvrActorPropertyGroup* a_parent) :IOvrActorProperty(a_parent, kOPTVector4f)
	{
		if (a_value != nullptr)
		{
			value = *(OvrVector4*)a_value;
		}
		property_type_name = "kOPTVector4f";
	}

	void OvrActorPropertyVector4f::InitValue(void* a_value)
	{
		value = *(OvrVector4*)a_value;
	}

	bool OvrActorPropertyVector4f::SetValue(const OvrVector4& a_value)
	{
		if (a_value == value)
		{
			return false;
		}

		value = a_value;
		OnValueChanged();
		return true;
	}

	///////////////////////////////////////////////
	// class OvrActorPropertyString

	OvrActorPropertyString::OvrActorPropertyString(void* a_value, IOvrActorPropertyGroup* a_parent) :IOvrActorProperty(a_parent, kOPTString)
	{
		if (a_value != nullptr)
		{
			value = *(OvrString*)a_value;
		}
		property_type_name = "kOPTString";
	}

	void OvrActorPropertyString::InitValue(void* a_value)
	{
		value = *(OvrString*)a_value;
	}

	bool OvrActorPropertyString::SetValue(const OvrString& a_value)
	{
		if (a_value == value)
		{
			return false;
		}

		value = a_value;
		OnValueChanged();
		return true;
	}

	///////////////////////////////////////////////
	// class OvrActorPropertyRes

	OvrActorPropertyRes::OvrActorPropertyRes(void* a_value, IOvrActorPropertyGroup* a_parent) :IOvrActorProperty(a_parent, kOPTRes)
	{
		if (a_value != nullptr)
		{
			value = *(OvrString*)a_value;
		}
		property_type_name = "kOPTRes";
	}

	void OvrActorPropertyRes::InitValue(void* a_value)
	{
		value = *(OvrString*)a_value;
	}

	bool OvrActorPropertyRes::SetValue(const OvrString& a_value)
	{
		if (a_value == value)
		{
			return false;
		}

		value = a_value;
		OnValueChanged();
		return true;
	}

	///////////////////////////////////////////////
	// class OvrActorPropertyComboBox

	OvrActorPropertyComboBox::OvrActorPropertyComboBox(void* a_value, IOvrActorPropertyGroup* a_parent) :IOvrActorProperty(a_parent, kOPTComboBox)
	{
		if (a_value != nullptr)
		{
			item_id = *(int*)a_value;
		}
		property_type_name = "kOPTBox";
	}

	void OvrActorPropertyComboBox::InitValue(void* a_value)
	{
		item_id = *(int*)a_value;
	}

	bool OvrActorPropertyComboBox::SetValue(int a_value)
	{
		if (a_value == item_id)
		{
			return false;
		}

		item_id = a_value;
		OnValueChanged();
		return true;
	}
}
