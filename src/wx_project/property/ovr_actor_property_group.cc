﻿
#include "wx_project/property/ovr_actor_property_group.h"
#include "wx_project/property/ovr_actor_property.h"

#include <wx_engine/ovr_actor.h>
#include <wx_engine/ovr_mesh.h>
#include <wx_engine/ovr_light_component.h>
#include <wx_engine/ovr_camera_component.h>
//#include <wx_engine/ovr_mesh_component.h>
#include <wx_engine/ovr_static_mesh_component.h>
#include <wx_engine/ovr_skinned_mesh_component.h>
#include <wx_engine/ovr_video_component.h>
#include <wx_engine/ovr_text_component.h>
#include <wx_engine/ovr_box_component.h>
#include <wx_engine/ovr_texture.h>
#include <wx_engine/ovr_material.h>

#include <wx_project/command/ovr_material_changed.h>
#include <wx_project/command/ovr_command_component.h>
#include <wx_project/command/ovr_command_manager.h>

#include <wx_core/ovr_scene_context.h>

#include <cassert>
#include <set>
#include <algorithm>

#define Init_Value(a_target, a_value) {auto v = a_value; if (a_target != nullptr) a_target->InitValue(&v);}

static OvrVector3 Quaternion2Degree(const OvrQuaternion& a_value)
{
	OvrVector3 v;
	a_value.get_euler_angles(v._x, v._y, v._z);

	for (int i = 0; i < 3; ++i)
	{
		v[i] = OVR_RAD_TO_DEG(v[i]);
	}

	return v;
}

static OvrQuaternion Degree2Quaternion(const OvrVector3& a_value)
{
	OvrVector3 v;
	for (int i = 0; i < 3; ++i)
	{
		v[i] = OVR_DEG_TO_RAD(a_value[i]);
	}

	return OvrQuaternion::make_euler_quat(v[0], v[1], v[2]);
}

namespace ovr_project 
{
	IOvrActorPropertyGroup* IOvrActorPropertyGroup::CreateGroup(int a_group_type, ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_compnent)
	{
		IOvrActorPropertyGroup* group = nullptr;

		switch (a_group_type)
		{
		case ovr_project::kOPTGBase:
			group = new OvrActorPropertyGroupBase;
			break;
		case ovr_project::kOPTGTrans:
			group = new OvrActorPropertyGroupTrans;
			break;
		case ovr_project::kOPTGStaticMesh:
			group = new OvrActorPropertyGroupStaticMesh;
			break;
		case ovr_project::kOPTGSkinMesh:
			group = new OvrActorPropertyGroupSkinMesh;
			break;
		case ovr_project::kOPTGMat:
			group = new OvrActorPropertyGroupMat;
			break;
		case ovr_project::kOPTGLight:
			group = new OvrActorPropertyGroupLight;
			break;
		case ovr_project::kOPTGVideo:
			group = new OvrActorPropertyGroupVideo;
			break;
		case ovr_project::kOPTGText:
			group = new OvrActorPropertyGroupText;
			break;
		case ovr_project::kOPTGTextLayout:
			group = new OvrActorPropertyGroupTextLayout;
			break;
		case ovr_project::kOPTGCollideBox:
			group = new OvrActorPropertyGroupCollideBox;
			break;
		case ovr_project::kOPTGCamera:
			group = new OvrActorPropertyGroupCamera;
			break;
		default:
			assert(false);
			break;
		}

		if (group != nullptr)
		{
			group->init(a_actor, a_compnent);
		}

		return group;
	}

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupBase
	const OvrActorPropertyGroupRoot* OvrActorPropertyGroupRoot::GetSupportPropertyGroups()
	{
		static OvrActorPropertyGroupRoot* surpport_groups = nullptr;

		if (surpport_groups == nullptr)
		{
			surpport_groups = new OvrActorPropertyGroupRoot;

			int types[] = { kOPTGStaticMesh, kOPTGSkinMesh , kOPTGVideo ,kOPTGLight,kOPTGCamera ,kOPTGCollideBox };//kOPTGText
			int count = sizeof(types) / sizeof(int);

			for (int i = 0; i < count; ++i)
			{
				surpport_groups->AddGroup(types[i]);
			}
		}

		return surpport_groups;
	}

	OvrActorPropertyGroupRoot* OvrActorPropertyGroupRoot::CreatePropertyGroups(ovr_engine::OvrActor* a_actor)
	{
		assert(a_actor != nullptr);

		//根group
		auto root_group = new OvrActorPropertyGroupRoot;
		root_group->init(a_actor, nullptr);

		//基本属性
		root_group->AddGroup(kOPTGBase);

		//变换
		root_group->AddGroup(kOPTGTrans);

		//组件
		auto& items = a_actor->GetAllComponents();
		for (auto component : a_actor->GetAllComponents())
		{
			root_group->addGroupForComponent(component);
		}

		return root_group;
	}

	OvrActorPropertyGroupRoot::OvrActorPropertyGroupRoot(IOvrActorPropertyGroup* a_parent) :IOvrActorPropertyGroup(a_parent, kOPTGRoot)
	{
		property_id = "group_root";
		property_name = "Root Group";
	}

	OvrActorPropertyGroupRoot::~OvrActorPropertyGroupRoot()
	{
		if (group_list.size() > 0)
		{
			for (auto item : group_list)
			{
				delete item;
			}
			group_list.clear();
			group_map.clear();
		}
	}

	void OvrActorPropertyGroupRoot::AddGroup(IOvrActorPropertyGroup* a_group)
	{
		assert(a_group != nullptr && a_group->property_id.GetStringSize() > 0);
		assert(group_map.find(a_group->GetGroupType()) == group_map.end());

		group_list.push_back(a_group);
		group_map[a_group->GetGroupType()] = a_group;
	}

	void OvrActorPropertyGroupRoot::AddGroup(int a_group_type)
	{
		auto group = CreateGroup(a_group_type, actor);
		if (group != nullptr)
		{
			AddGroup(group);
		}
	}

	bool OvrActorPropertyGroupRoot::RemoveGroup(int a_group_type)
	{
		if (Contains(a_group_type))
		{
			auto target = group_map[a_group_type];

			group_map.erase(a_group_type);
			group_list.remove(target);

			delete target;

			return true;
		}

		return false;
	}

	bool OvrActorPropertyGroupRoot::RemoveGroupIncludeComponent(int a_com_type)
	{
		return RemoveGroup(groupTypeIncludeComponent(a_com_type));
	}

	void OvrActorPropertyGroupRoot::RemoveComponent(int a_group_type)
	{
		auto group = GetGroup(a_group_type);
		if (group != nullptr)
		{
			group->RemoveComponent();
		}
	}

	bool OvrActorPropertyGroupRoot::Contains(int a_group_type)
	{
		return group_map.find(a_group_type) != group_map.end();
	}

	int OvrActorPropertyGroupRoot::Index(int a_group_type)
	{
		int pos = 0;

		for (auto item : group_list)
		{
			if (a_group_type == item->GetGroupType())
			{
				return pos;
			}
			++pos;
		}

		return -1;
	}

	IOvrActorPropertyGroup* OvrActorPropertyGroupRoot::GetGroup(int a_group_type)
	{
		if (Contains(a_group_type))
		{
			return group_map[a_group_type];
		}
		return nullptr;
	}

	void OvrActorPropertyGroupRoot::createComponent(int a_group_type, const OvrString& a_param)
	{
		assert(a_group_type > kOPTGComponent);

		if (Contains(a_group_type))
		{
			return;
		}
		if (a_group_type == kOPTGSkinMesh || a_group_type == kOPTGStaticMesh)
		{
			if (Contains(kOPTGSkinMesh) || Contains(kOPTGStaticMesh))
			{
				return;
			}
		}

		int com_type = componentTypeInGroup(a_group_type);
		if (com_type >= 0)
		{
			ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new OvrComponentCmdAdd(actor, com_type, a_param));
		}
	}

	IOvrActorPropertyGroup* OvrActorPropertyGroupRoot::addGroupForComponent(ovr_engine::OvrComponent* a_component)
	{
		int target_group_type = groupTypeIncludeComponent(a_component->GetType());

		if (target_group_type > kOPTGComponent)
		{
			auto new_group = IOvrActorPropertyGroup::CreateGroup(target_group_type, actor, a_component);
			if (new_group != nullptr)
			{
				AddGroup(new_group);
				return new_group;
			}
		}

		return nullptr;
	}

	int	OvrActorPropertyGroupRoot::componentTypeInGroup(int a_group_type)
	{
		switch (a_group_type)
		{
		case ovr_project::kOPTGStaticMesh:	return ovr_engine::kComponentStaticMesh;
		case ovr_project::kOPTGSkinMesh:	return ovr_engine::kComponentSkinnedMesh;
		case ovr_project::kOPTGLight:		return ovr_engine::kComponentLight;
		case ovr_project::kOPTGVideo:		return ovr_engine::kComponentVideo;
		case ovr_project::kOPTGText:		return ovr_engine::kComponentText;
		case ovr_project::kOPTGCollideBox:	return ovr_engine::kComponentBoxCollider;
		case ovr_project::kOPTGCamera:		return ovr_engine::kComponentCamera;
		}

		return -1;
	}

	int	OvrActorPropertyGroupRoot::groupTypeIncludeComponent(int a_com_type)
	{
		switch (a_com_type)
		{
		case ovr_engine::kComponentStaticMesh:	return ovr_project::kOPTGStaticMesh;
		case ovr_engine::kComponentSkinnedMesh:	return ovr_project::kOPTGSkinMesh;
		case ovr_engine::kComponentLight:		return ovr_project::kOPTGLight;
		case ovr_engine::kComponentVideo:		return ovr_project::kOPTGVideo;
		case ovr_engine::kComponentText:		return ovr_project::kOPTGText;
		case ovr_engine::kComponentBoxCollider:	return ovr_project::kOPTGCollideBox;
		case ovr_engine::kComponentCamera:		return ovr_project::kOPTGCamera;
		}

		return -1;
	}


	///////////////////////////////////////////////
	// class OvrActorPropertyGroupFun

	IOvrActorProperty* OvrActorPropertyGroupFun::AddPropertyComboBox(const OvrString& a_id, const OvrString& a_name, const std::vector<OvrDataItemInt>& a_items, void* a_value, bool a_update_flag)
	{
		OvrString first_item;

		auto obj = AddProperty(IOvrActorProperty::Create(kOPTComboBox, a_id, a_name, a_value));
		auto combox_obj = static_cast<OvrActorPropertyComboBox*>(obj);

		combox_obj->SetItems(a_items);
		combox_obj->SetNeedUpdateGroup(a_update_flag);

		return obj;
	}

	void OvrActorPropertyGroupFun::RemoveComponent()
	{
		if (actor != nullptr && component != nullptr)
		{
			ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new OvrComponentCmdDel(actor, component));
			component = nullptr;
		}
	}

	bool OvrActorPropertyGroupFun::CanBeRemoved()
	{
		return group_type > kOPTGComponent;
	}

	int OvrActorPropertyGroupFun::getComboxValue(IOvrActorProperty* a_property)
	{
		assert(a_property->property_type == kOPTComboBox);

		return static_cast<OvrActorPropertyComboBox*>(a_property)->GetValue();
	}

	OvrString OvrActorPropertyGroupFun::getComboxValueTitle(IOvrActorProperty* a_property, int a_data)
	{
		assert(a_property->property_type == kOPTComboBox);

		for (auto& item : static_cast<OvrActorPropertyComboBox*>(a_property)->GetItems())
		{
			if (item.item_id == a_data)
			{
				return item.item_name;
			}
		}
		return "";
	}

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupBase

	OvrActorPropertyGroupBase::OvrActorPropertyGroupBase(IOvrActorPropertyGroup* a_parent) :OvrActorPropertyGroupFun(a_parent, kOPTGBase)
	{
		property_id = "group_base";
		property_name = "基本属性";
	}

	void OvrActorPropertyGroupBase::OnInitWithNoComponent()
	{
		AddProperty(IOvrActorProperty::Create(kOPTBool, "hide", "隐藏"));
		Init_Value(property_map["hide"], !actor->IsVisible());

		AddProperty(IOvrActorProperty::Create(kOPTBool, "static", "静止"));
		Init_Value(property_map["static"], actor->IsStatic());

		AddProperty(IOvrActorProperty::Create(kOPTString, "name", "显示名"));
		Init_Value(property_map["name"], actor->GetDisplayName());
	}

	void OvrActorPropertyGroupBase::DoPropertyChange(IOvrActorProperty* a_property)
	{
		if (a_property->property_id == "hide")
		{
			auto value = static_cast<OvrActorPropertyBool*>(a_property)->GetValue();
			actor->SetVisible(!value);
		}
		else if (a_property->property_id == "static")
		{
			auto value = static_cast<OvrActorPropertyBool*>(a_property)->GetValue();
			actor->SetStatic(value);
		}
		else if (a_property->property_id == "name")
		{
			auto value = static_cast<OvrActorPropertyString*>(a_property)->GetValue();
			actor->SetDisplayName(value.GetStringConst());
		}
	}
	///////////////////////////////////////////////
	// class OvrActorPropertyGroupTrans

	OvrActorPropertyGroupTrans::OvrActorPropertyGroupTrans(IOvrActorPropertyGroup* a_parent) :OvrActorPropertyGroupFun(a_parent, kOPTGTrans)
	{
		property_id = "group_trans";
		property_name = "变换";
	}
	
	void OvrActorPropertyGroupTrans::OnInitWithNoComponent()
	{
		AddProperty(IOvrActorProperty::Create(kOPTVector3f, "pos", "位置"));
		AddProperty(IOvrActorProperty::Create(kOPTVector3f, "rotate", "旋转"));
		AddProperty(IOvrActorProperty::Create(kOPTVector3f, "scale", "缩放"));

		InitValue();
	}

	void OvrActorPropertyGroupTrans::DoReload()
	{
		InitValue();
	}

	void OvrActorPropertyGroupTrans::InitValue()
	{
		auto& trans = actor->GetTransform();

		Init_Value(property_map["pos"], trans.position);
		Init_Value(property_map["rotate"], Quaternion2Degree(trans.quaternion));
		Init_Value(property_map["scale"], trans.scale);
	}

	void OvrActorPropertyGroupTrans::DoPropertyChange(IOvrActorProperty* a_property)
	{
		auto value = static_cast<OvrActorPropertyVector3f*>(a_property)->GetValue();
		auto trans = actor->GetTransform();

		if (a_property->property_id == "pos")
		{
			for (int i = 0; i < 3; ++i) trans.position[i] = value[i];
			actor->SetTransform(trans);
		}
		else if (a_property->property_id == "rotate")
		{
			trans.quaternion = Degree2Quaternion(value);
			actor->SetTransform(trans);
		}
		else if (a_property->property_id == "scale")
		{
			for (int i = 0; i < 3; ++i) trans.scale[i] = value[i];
			actor->SetTransform(trans);
		}
	}

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupMesh

	OvrActorPropertyGroupStaticMesh::OvrActorPropertyGroupStaticMesh(IOvrActorPropertyGroup* a_parent, OvrActorPropertyGroupType a_group_type) :OvrActorPropertyGroupFun(a_parent, a_group_type)
	{
		property_id = "group_static_mesh";
		property_name = "静态网格";		 
	}

	void OvrActorPropertyGroupStaticMesh::OnInit()
	{
		auto com = static_cast<ovr_engine::OvrMeshComponent*>(component);

		AddProperty(IOvrActorProperty::Create(kOPTBool, "shadow", "产生阴影"));
		Init_Value(property_map["shadow"], com->GetCastShadow());

		AddProperty(IOvrActorProperty::Create(kOPTRes, "mesh", "网格"))->SetAttribute("res_type", "mesh");
		Init_Value(property_map["mesh"], com->GetMesh()->GetName());

		AddProperty(IOvrActorPropertyGroup::CreateGroup(kOPTGMat, actor, component));
	}

	void OvrActorPropertyGroupStaticMesh::DoPropertyChange(IOvrActorProperty* a_property)
	{
		auto com = static_cast<ovr_engine::OvrMeshComponent*>(component);

		if (a_property->property_id == "shadow")
		{
			auto value = static_cast<OvrActorPropertyBool*>(a_property)->GetValue();
			com->SetCastShadow(value);
		}
		else if (a_property->property_id == "mesh")
		{
			auto value = static_cast<OvrActorPropertyRes*>(a_property)->GetValue();

			auto cmd = new ovr_project::OvrChangeMesh(actor);
			cmd->SetNewMesh(value);
			ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(cmd);
		}
	}

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupSkinMesh

	OvrActorPropertyGroupSkinMesh::OvrActorPropertyGroupSkinMesh(IOvrActorPropertyGroup* a_parent) :OvrActorPropertyGroupStaticMesh(a_parent, kOPTGSkinMesh)
	{
		property_id = "group_skin_mesh";
		property_name = "皮肤网格";
	}

	void OvrActorPropertyGroupSkinMesh::OnInit()
	{
		OvrActorPropertyGroupStaticMesh::OnInit();

		auto com = static_cast<ovr_engine::OvrSkinnedMeshComponent*>(component);

		AddProperty(IOvrActorProperty::Create(kOPTRes, "skeleton", "骨骼"))->SetAttribute("res_type", "skeleton");
		Init_Value(property_map["skeleton"], com->GetMesh()->GetSkeleton()->GetName());
	}

	void OvrActorPropertyGroupSkinMesh::DoPropertyChange(IOvrActorProperty* a_property)
	{
		if (a_property->property_id == "skeleton")
		{
			auto com = static_cast<ovr_engine::OvrSkinnedMeshComponent*>(component);
			auto value = static_cast<OvrActorPropertyRes*>(a_property)->GetValue();
			//dothing
		}
		else
		{
			OvrActorPropertyGroupStaticMesh::DoPropertyChange(a_property);
		}
	}

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupMat

	OvrActorPropertyGroupMat::OvrActorPropertyGroupMat(IOvrActorPropertyGroup* a_parent) :OvrActorPropertyGroupFun(a_parent, kOPTGMat)
	{
		property_id = "group_mat";
		property_name = "材质";
	}

	void OvrActorPropertyGroupMat::OnInit()
	{
		auto com = static_cast<ovr_engine::OvrMeshComponent*>(component);

		char str_id[10] = { 0 }, str_name[10] = { 0 };
		for (unsigned i = 0; i < com->GetMaterialCount(); ++i)
		{
			sprintf(str_id, "%d", i);
			sprintf(str_name, "%d", i + 1);
			AddProperty(IOvrActorProperty::Create(kOPTRes, str_id, str_name))->SetAttribute("res_type", "mat");
			auto mat = com->GetMaterial(i);
			if (mat != nullptr)
			{
				Init_Value(property_map[str_id], mat->GetName());
			}
		}
	}

	void OvrActorPropertyGroupMat::DoPropertyChange(IOvrActorProperty* a_property)
	{
		auto com = static_cast<ovr_engine::OvrMeshComponent*>(component);

		int index = atoi(a_property->property_id.GetStringConst());
		auto value = static_cast<OvrActorPropertyRes*>(a_property)->GetValue();

		auto cmd = new ovr_project::OvrChangeMaterial(actor);
		cmd->SetNewMaterial(index, value);
		ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(cmd);
	}
	///////////////////////////////////////////////
	// class OvrActorPropertyGroupLight

	OvrActorPropertyGroupLight::OvrActorPropertyGroupLight(IOvrActorPropertyGroup* a_parent) :OvrActorPropertyGroupFun(a_parent, kOPTGLight)
	{
		property_id = "group_light";
		property_name = "灯光";
	}

	void OvrActorPropertyGroupLight::InitContent(int a_light_type_id)
	{
		static std::vector<OvrDataItemInt> items = { { ovr_engine::ePointLight, "点光" },{ ovr_engine::eSpotLight,"锥形光" },{ ovr_engine::eDirectionLight,"平行光" } };
		AddPropertyComboBox("light_type", "类型", items, nullptr, true);

		AddProperty(IOvrActorProperty::Create(kOPTBool, "shadow", "产生阴影"));
		AddProperty(IOvrActorProperty::Create(kOPTVector4f, "color", "颜色"))->SetAttribute("format", "color");
		AddProperty(IOvrActorProperty::Create(kOPTFloat, "strength", "强度"));

		if (a_light_type_id == ovr_engine::ePointLight || a_light_type_id == ovr_engine::eSpotLight)
		{
			AddProperty(IOvrActorProperty::Create(kOPTFloat, "range", "范围"));
		}

		if (a_light_type_id == ovr_engine::eSpotLight)
		{
			AddProperty(IOvrActorProperty::Create(kOPTFloat, "degree", "角度"));
			AddProperty(IOvrActorProperty::Create(kOPTFloat, "damping", "衰减速度"));
		}
	}

	void OvrActorPropertyGroupLight::InitValue(int a_light_type_id)
	{
		auto com = static_cast<ovr_engine::OvrLightComponent*>(component);

		Init_Value(property_map["light_type"], a_light_type_id);
		Init_Value(property_map["shadow"], com->IsCastShadow());
		Init_Value(property_map["color"], com->GetDiffuseColor());
		Init_Value(property_map["strength"], com->GetIntensity());

		if (a_light_type_id == ovr_engine::ePointLight || a_light_type_id == ovr_engine::eSpotLight)
		{
			Init_Value(property_map["range"], com->GetRange());
		}

		if (a_light_type_id == ovr_engine::eSpotLight)
		{
			Init_Value(property_map["degree"], com->GetFalloff());
			Init_Value(property_map["damping"], com->GetExp());
		}
	}

	void OvrActorPropertyGroupLight::OnInit()
	{
		auto com = static_cast<ovr_engine::OvrLightComponent*>(component);
		auto light_type = com->GetLightType();

		InitContent(light_type);
		InitValue(light_type);
	}

	void OvrActorPropertyGroupLight::DoPropertyChange(IOvrActorProperty* a_property)
	{
		auto com = static_cast<ovr_engine::OvrLightComponent*>(component);

		if (a_property->property_id == "light_type")
		{
			auto combox_obj = static_cast<OvrActorPropertyComboBox*>(a_property);
			auto type_id = combox_obj->GetValue();
			com->SetLightType(ovr_engine::OvrLightType(type_id));
		}
		else if (a_property->property_id == "shadow")
		{
			auto value = static_cast<OvrActorPropertyBool*>(a_property)->GetValue();
			com->EnableCastShadow(value);
		}
		else if (a_property->property_id == "color")
		{
			auto value = static_cast<OvrActorPropertyVector4f*>(a_property)->GetValue();
			com->SetDiffuseColor(value);
		}
		else if (a_property->property_id == "strength")
		{
			auto value = static_cast<OvrActorPropertyFloat*>(a_property)->GetValue();
			com->SetIntensity(value);
		}
		else if (a_property->property_id == "range")
		{
			auto value = static_cast<OvrActorPropertyFloat*>(a_property)->GetValue();
			com->SetRange(value);
		}
		else if (a_property->property_id == "degree")
		{
			auto value = static_cast<OvrActorPropertyFloat*>(a_property)->GetValue();
			com->SetFalloff(value);
		}
		else if (a_property->property_id == "damping")
		{
			auto value = static_cast<OvrActorPropertyFloat*>(a_property)->GetValue();
			com->SetExp(value);
		}
		else
		{
			assert(false);
		}
	}

	void OvrActorPropertyGroupLight::DoReload()
	{
		auto light_type = static_cast<ovr_engine::OvrLightComponent*>(component)->GetLightType();
		if (IsLayoutChanged())
		{
			clear();
			InitContent(light_type);
		}
		InitValue(light_type);
	}
	///////////////////////////////////////////////
	// class OvrActorPropertyGroupVideo

	OvrActorPropertyGroupVideo::OvrActorPropertyGroupVideo(IOvrActorPropertyGroup* a_parent) :OvrActorPropertyGroupFun(a_parent, kOPTGVideo)
	{
		property_id = "group_video";
		property_name = "视频";
	}

	void OvrActorPropertyGroupVideo::DoPropertyChange(IOvrActorProperty* a_property)
	{
		auto com = static_cast<ovr_engine::OvrVideoComponent*>(component);

		if (a_property->property_id == "video_type")
		{
			auto value = getComboxValue(a_property);
			auto title = getComboxValueTitle(a_property, value);
			ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new OvrComponentCmdVideoChangeVideoType(actor, com, value));
			return;
		}

		int result = 0;
		std::vector<int> movie_types;
		switch (com->GetVideoType())
		{
		case ovr_engine::Video2D:  result = GetMovieTypeFor2D(movie_types); break;
		case ovr_engine::Video3D:  result = GetMovieTypeFor3D(movie_types); break;
		case ovr_engine::Video180: result = GetMovieTypeFor180(movie_types); break;
		case ovr_engine::Video360: result = GetMovieTypeFor360(movie_types); break;
		}

		if (result > 0)
		{
			if (movie_types.size() > 1)
			{
				SetNeedReload(true);
				SetAsyncLoad(true);
			}
			ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new OvrComponentCmdVideoChangeMovieType(actor, com, result));
		}
	}

	void OvrActorPropertyGroupVideo::DoReload()
	{
		auto video_type = static_cast<ovr_engine::OvrVideoComponent*>(component)->GetVideoType();
		if (IsLayoutChanged())
		{
			clear();
			InitContent(video_type);
		}
		InitValue(video_type);
	}

	void OvrActorPropertyGroupVideo::OnInit()
	{
		auto video_type = static_cast<ovr_engine::OvrVideoComponent*>(component)->GetVideoType();

		InitContent(video_type);
		InitValue(video_type);
	}

	void OvrActorPropertyGroupVideo::InitContent(int a_video_type_id)
	{
		if (a_video_type_id == 0 || a_video_type_id > ovr_engine::Video2D)
		{
			return;
		}

		static std::vector<OvrDataItemInt> items = { 
			{ ovr_engine::Video2D,"2D" },{ ovr_engine::Video3D,"3D" },{ ovr_engine::Video180,"180" },
			{ ovr_engine::Video360,"全景" }};// ,{ ovr_engine::Video360Box,"全景盒子" } 
		AddPropertyComboBox("video_type", "视频类型", items, nullptr, true)->SetAsyncLoad(true);

		switch (a_video_type_id)
		{
		case ovr_engine::Video2D:  InitContent2D(); break;
		case ovr_engine::Video3D:  InitContent3D(); break;
		case ovr_engine::Video180: InitContent180(); break;
		case ovr_engine::Video360: InitContent360(); break;
		}
	}
	void OvrActorPropertyGroupVideo::InitContent2D()
	{
		static std::vector<OvrDataItemInt> items = { { kDMFull,"全屏" },{ kDMRatio,"等比例缩放" } };
		AddPropertyComboBox("video_display_mode", "显示模式", items);
	}

	void OvrActorPropertyGroupVideo::InitContent3D()
	{
		static std::vector<OvrDataItemInt> items = { { kSFTD,"上下" }, { KSFLR,"左右" } };

		AddPropertyComboBox("video_save_format", "存储格式", items);
		AddProperty(IOvrActorProperty::Create(kOPTBool, "full_resolution", "全分辨率"));
	}

	void OvrActorPropertyGroupVideo::InitContent180()
	{
		static std::vector<OvrDataItemInt> items = { { kSFTD,"上下" },{ KSFLR,"左右" } };
		AddPropertyComboBox("video_save_format", "存储格式", items);
	}

	void OvrActorPropertyGroupVideo::InitContent360() 
	{
		{
			static std::vector<OvrDataItemInt> items = { { kDMPano,"球" },{ kDMCube, "立方体" } };
			AddPropertyComboBox("video_display_mode", "类型", items);
		}

		{
			static std::vector<OvrDataItemInt> items = { { kSF2D,"2d" },{ kSFTD, "上下" },{ KSFLR,"左右" } };
			AddPropertyComboBox("video_save_format", "存储格式", items);
		}
	}

	void OvrActorPropertyGroupVideo::InitValue(int a_video_type_id)
	{
		auto com = static_cast<ovr_engine::OvrVideoComponent*>(component);

		if (a_video_type_id == 0 || a_video_type_id > ovr_engine::Video2D)
		{
			return;
		}

		Init_Value(property_map["video_type"], a_video_type_id);

		switch (a_video_type_id)
		{
		case ovr_engine::Video2D:  InitValue2D(); break;
		case ovr_engine::Video3D:  InitValue3D(); break;
		case ovr_engine::Video180: InitValue180(); break;
		case ovr_engine::Video360: InitValue360(); break;
		}
	}

	void OvrActorPropertyGroupVideo::InitValue2D()
	{
		auto com = static_cast<ovr_engine::OvrVideoComponent*>(component);
		int movie_type = com->GetMovieType();

		Init_Value(property_map["video_display_mode"], GetDisplayMode(ovr_engine::Video2D, movie_type));
	}

	void OvrActorPropertyGroupVideo::InitValue3D()
	{
		auto com = static_cast<ovr_engine::OvrVideoComponent*>(component);
		int movie_type = com->GetMovieType();

		Init_Value(property_map["video_save_format"], GetSaveFormat(ovr_engine::Video3D, movie_type));
		Init_Value(property_map["full_resolution"], IsFullResolution(ovr_engine::Video3D, movie_type));

	}

	void OvrActorPropertyGroupVideo::InitValue180()
	{
		auto com = static_cast<ovr_engine::OvrVideoComponent*>(component);
		int movie_type = com->GetMovieType();

		Init_Value(property_map["video_save_format"], GetSaveFormat(ovr_engine::Video180, movie_type));
	}

	void OvrActorPropertyGroupVideo::InitValue360()
	{
		auto com = static_cast<ovr_engine::OvrVideoComponent*>(component);
		int movie_type = com->GetMovieType();

		Init_Value(property_map["video_display_mode"], GetDisplayMode(ovr_engine::Video360, movie_type));
		Init_Value(property_map["video_save_format"], GetSaveFormat(ovr_engine::Video360, movie_type));
	}

	int OvrActorPropertyGroupVideo::GetDisplayMode(int a_video_mode, int a_movie_type)
	{
		int result = -1;

		switch (a_video_mode)
		{
		case ovr_engine::Video2D:
			switch (a_movie_type)
			{
			case ovr_engine::e_movie_type_common_2d:
				result = kDMRatio;
				break;
			case ovr_engine::e_movie_type_common_2d_full:
				result = kDMFull;
				break;
			}
			break;
		case ovr_engine::Video360:
			switch (a_movie_type)
			{
			case ovr_engine::e_movie_type_pano_360:
			case ovr_engine::e_movie_type_pano_360_LR:
			case ovr_engine::e_movie_type_pano_360_TD:
				result = kDMPano;
				break;
			case ovr_engine::e_movie_type_pano_cb:
			case ovr_engine::e_movie_type_pano_cb_LR:
			case ovr_engine::e_movie_type_pano_cb_TD:
				result = kDMCube;
				break;
			}
			break;
		}

		return result;
	}

	int OvrActorPropertyGroupVideo::GetSaveFormat(int a_video_mode, int a_movie_type)
	{
		int result = -1;

		switch (a_video_mode)
		{
		case ovr_engine::Video3D:
			switch (a_movie_type)
			{
			case ovr_engine::e_movie_type_top_bottom_3d:
			case ovr_engine::e_movie_type_top_bottom_3d_full:
				result = kSFTD;
				break;
			case ovr_engine::e_movie_type_left_right_3d:
			case ovr_engine::e_movie_type_left_right_3d_full:
				result = KSFLR;
				break;
			}
			break;
		case ovr_engine::Video180:
			switch (a_movie_type)
			{
			case ovr_engine::e_movie_type_pano_180_TD:
				result = kSFTD;
				break;
			case ovr_engine::e_movie_type_pano_180_LR:
				result = KSFLR;
				break;
			}
			break;
		case ovr_engine::Video360:
			switch (a_movie_type)
			{
			case ovr_engine::e_movie_type_pano_cb:
			case ovr_engine::e_movie_type_pano_360:
				result = kSF2D;
				break;
			case ovr_engine::e_movie_type_pano_cb_TD:
			case ovr_engine::e_movie_type_pano_360_TD:
				result = kSFTD;
				break;
			case ovr_engine::e_movie_type_pano_cb_LR:
			case ovr_engine::e_movie_type_pano_360_LR:
				result = KSFLR;
				break;
			}
			break;
		}

		return result;
	}

	bool OvrActorPropertyGroupVideo::IsFullResolution(int a_video_mode, int a_movie_type)
	{
		bool result = false;

		switch (a_video_mode)
		{
		case ovr_engine::Video3D:
			switch (a_movie_type)
			{
			case ovr_engine::e_movie_type_top_bottom_3d_full:
			case ovr_engine::e_movie_type_left_right_3d_full:
				result = true;
				break;
			}
			break;
		}

		return result;
	}

	int OvrActorPropertyGroupVideo::GetMovieTypeFor2D(std::vector<int>& a_movie_types)
	{
		auto display_mode = getComboxValue(property_map["video_display_mode"]);
		auto display_mode_title = getComboxValueTitle(property_map["video_display_mode"], display_mode);

		switch (display_mode)
		{
		case kDMFull:  
			a_movie_types = { ovr_engine::e_movie_type_common_2d_full };
			break;
		case kDMRatio: 
			a_movie_types = { ovr_engine::e_movie_type_common_2d };
			break;
		default:
			a_movie_types = { ovr_engine::e_movie_type_common_2d , ovr_engine::e_movie_type_common_2d_full };
			break;
		}

		return a_movie_types.size() > 0 ? a_movie_types[0] : 0;
	}

	int OvrActorPropertyGroupVideo::GetMovieTypeFor3D(std::vector<int>& a_movie_types)
	{
		auto video_save_format = getComboxValue(property_map["video_save_format"]);
		auto video_save_format_title = getComboxValueTitle(property_map["video_save_format"], video_save_format);

		auto is_full = static_cast<OvrActorPropertyBool*>(property_map["full_resolution"])->GetValue();

		std::set<int> types_for_save_format;
		{
			static std::set<int> types_for_video_format_TD = { ovr_engine::e_movie_type_top_bottom_3d , ovr_engine::e_movie_type_top_bottom_3d_full };;
			static std::set<int> types_for_video_format_LR = { ovr_engine::e_movie_type_left_right_3d, ovr_engine::e_movie_type_left_right_3d_full };;
			switch (video_save_format)
			{
			case kSFTD:
				types_for_save_format = types_for_video_format_TD;
				break;
			case KSFLR:
				types_for_save_format = types_for_video_format_LR;
				break;
			default:
				types_for_save_format.insert(types_for_video_format_TD.begin(), types_for_video_format_TD.end());
				types_for_save_format.insert(types_for_video_format_LR.begin(), types_for_video_format_LR.end());
				break;
			}
		}

		std::set<int> types_for_Full;
		{
			static std::set<int> types_for_is_full = { ovr_engine::e_movie_type_top_bottom_3d_full, ovr_engine::e_movie_type_left_right_3d_full };
			static std::set<int> types_for_not_full = { ovr_engine::e_movie_type_top_bottom_3d,ovr_engine::e_movie_type_left_right_3d };
			types_for_Full = is_full ? types_for_is_full : types_for_not_full;
		}

		int insetcion_size = std::min(types_for_save_format.size(), types_for_Full.size());
		a_movie_types.resize(insetcion_size);

		auto it = set_intersection(types_for_save_format.begin(), types_for_save_format.end(), types_for_Full.begin(), types_for_Full.end(), a_movie_types.begin());
		a_movie_types.resize(it - a_movie_types.begin());

		return a_movie_types.size() > 0 ? a_movie_types[0] : 0;
	}

	int OvrActorPropertyGroupVideo::GetMovieTypeFor180(std::vector<int>& a_movie_types)
	{
		auto video_save_format = getComboxValue(property_map["video_save_format"]);
		auto video_save_format_title = getComboxValueTitle(property_map["video_save_format"], video_save_format);

		switch (video_save_format)
		{
		case kSFTD:
			a_movie_types = { ovr_engine::e_movie_type_pano_180_TD };
			break;
		case KSFLR:
			a_movie_types = { ovr_engine::e_movie_type_pano_180_LR };
		default:
			a_movie_types = { ovr_engine::e_movie_type_pano_180_TD, ovr_engine::e_movie_type_pano_180_LR };
			break;
		}

		return a_movie_types.size() > 0 ? a_movie_types[0] : 0;
	}

	int OvrActorPropertyGroupVideo::GetMovieTypeFor360(std::vector<int>& a_movie_types)
	{
		auto display_mode = getComboxValue(property_map["video_display_mode"]);
		auto display_mode_title = getComboxValueTitle(property_map["video_display_mode"], display_mode);

		auto video_save_format = getComboxValue(property_map["video_save_format"]);
		auto video_save_format_title = getComboxValueTitle(property_map["video_save_format"], video_save_format);

		////当前显示模式对应的movie type
		std::set<int> types_for_display_mode;
		{
			static std::set<int> types_for_pano = { ovr_engine::e_movie_type_pano_360, ovr_engine::e_movie_type_pano_360_LR , ovr_engine::e_movie_type_pano_360_TD };
			static std::set<int> types_for_cube = { ovr_engine::e_movie_type_pano_cb , ovr_engine::e_movie_type_pano_cb_LR , ovr_engine::e_movie_type_pano_cb_TD };
			switch (display_mode)
			{
			case kDMPano: types_for_display_mode = types_for_pano; break;
			case kDMCube: types_for_display_mode = types_for_cube; break;
			default:
				types_for_display_mode.insert(types_for_pano.begin(), types_for_pano.end());
				types_for_display_mode.insert(types_for_cube.begin(), types_for_cube.end());
				break;
			}
		}

		//当前保存格式对应的movie type
		std::set<int> types_for_save_format;
		{
			static std::set<int> types_for_save_2D = { ovr_engine::e_movie_type_pano_cb , ovr_engine::e_movie_type_pano_360 };
			static std::set<int> types_for_save_TD = { ovr_engine::e_movie_type_pano_cb_TD , ovr_engine::e_movie_type_pano_360_TD };
			static std::set<int> types_for_save_LR = { ovr_engine::e_movie_type_pano_cb_LR , ovr_engine::e_movie_type_pano_360_LR };
			switch (video_save_format)
			{
			case kSF2D: types_for_save_format = types_for_save_2D; break;
			case kSFTD: types_for_save_format = types_for_save_TD; break;
			case KSFLR: types_for_save_format = types_for_save_LR; break;
			default:
				types_for_save_format.insert(types_for_save_2D.begin(), types_for_save_2D.end());
				types_for_save_format.insert(types_for_save_TD.begin(), types_for_save_TD.end());
				types_for_save_format.insert(types_for_save_LR.begin(), types_for_save_LR.end());
				break;
			}
		}

		int insetcion_size = std::min(types_for_save_format.size(), types_for_display_mode.size());
		a_movie_types.resize(insetcion_size);

		auto it = set_intersection(types_for_save_format.begin(), types_for_save_format.end(), types_for_display_mode.begin(), types_for_display_mode.end(), a_movie_types.begin());
		a_movie_types.resize(it - a_movie_types.begin());

		return a_movie_types.size() > 0 ? a_movie_types[0] : 0;
	}

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupText

	OvrActorPropertyGroupText::OvrActorPropertyGroupText(IOvrActorPropertyGroup* a_parent) :OvrActorPropertyGroupFun(a_parent, kOPTGText)
	{
		property_id = "group_text";
		property_name = "文本";	
	}

	void OvrActorPropertyGroupText::OnInit()
	{
		auto com = static_cast<ovr_engine::OvrTextComponent*>(component);

		AddProperty(IOvrActorProperty::Create(kOPTString, "text", "文本"))->SetAttribute("is_text", "true");

		static std::vector<OvrDataItemInt> items = { { 1,"宋体" },{ 2,"楷体" },{ 3,"行书" } };
		AddPropertyComboBox("font_list", "字体", items, nullptr, true)->SetAttribute("is_font", "true");
		
		
		AddProperty(IOvrActorProperty::Create(kOPTFloat, "scale", "缩放"));
		AddProperty(IOvrActorProperty::Create(kOPTVector4f, "color", "颜色"))->SetAttribute("format", "color");

		AddProperty(IOvrActorPropertyGroup::CreateGroup(kOPTGTextLayout, actor, component));

		AddProperty(IOvrActorProperty::Create(kOPTRes, "bgn", "背景"))->SetAttribute("res_type", "img");
		AddProperty(IOvrActorProperty::Create(kOPTFloat, "opacity", "透明"));

		Init_Value(property_map["text"], com->GetText());
		Init_Value(property_map["font_list"], "-1");
		Init_Value(property_map["scale"], com->GetTextScale());
		Init_Value(property_map["color"], com->GetTextColor());

		/*auto tex = com->GetBackgroundTexture();
		if (tex != nullptr)
		{
			Init_Value(property_map["bgn"], tex->GetName());
		}*/
		Init_Value(property_map["opacity"], com->GetTransparentValue());
	}

	void OvrActorPropertyGroupText::DoPropertyChange(IOvrActorProperty* a_property)
	{
		auto com = static_cast<ovr_engine::OvrTextComponent*>(component);
		if (a_property->property_id == "text")
		{
			auto& v = static_cast<OvrActorPropertyString*>(a_property)->GetValue();

			const char* d = v.GetStringConst();
			com->SetText(v);
		}
		else if (a_property->property_id == "color")
		{
			 auto& v = static_cast<OvrActorPropertyVector4f*>(a_property)->GetValue();
			 com->SetTextColor(v);
		}
		else if (a_property->property_id == "scale")
		{
			auto v = static_cast<OvrActorPropertyFloat*>(a_property)->GetValue();
			com->SetTextScale(v);
		}
		else if (a_property->property_id == "bgn")
		{

		}
		else if (a_property->property_id == "opacity")
		{
			auto v = static_cast<OvrActorPropertyFloat*>(a_property)->GetValue();
			com->SetTransparentValue(v);
		}
	}


	///////////////////////////////////////////////
	// class OvrActorPropertyGroupTextLayout

	OvrActorPropertyGroupTextLayout::OvrActorPropertyGroupTextLayout(IOvrActorPropertyGroup* a_parent) :OvrActorPropertyGroupFun(a_parent, kOPTGTextLayout)
	{
		property_id = "group_text_layout";
		property_name = "排版";
	}

	void OvrActorPropertyGroupTextLayout::OnInit()
	{
		auto com = static_cast<ovr_engine::OvrTextComponent*>(component);

		AddProperty(IOvrActorProperty::Create(kOPTVector2f, "align", "对齐"))->SetAttribute("is_align", "true");
		OvrVector2 value((float)com->GetAlignHorizontalMethod(), (float)com->GetAlignVerticleMethod());
		Init_Value(property_map["align"], value);
		
		AddProperty(IOvrActorProperty::Create(kOPTVector2f, "border_space", "边缘空白"));
		com->GetTextMargin(value._x, value._y);
		Init_Value(property_map["border_space"], value);

		AddProperty(IOvrActorProperty::Create(kOPTFloat, "word_space", "字间距"));
		Init_Value(property_map["word_space"], com->GetWarpWidth());

		AddProperty(IOvrActorProperty::Create(kOPTFloat, "row_space", "行间距"));
		Init_Value(property_map["row_space"], com->GetLineWarp());
	}

	void OvrActorPropertyGroupTextLayout::DoPropertyChange(IOvrActorProperty* a_property)
	{
		auto com = static_cast<ovr_engine::OvrTextComponent*>(component);
		if (a_property->property_id == "align")
		{
			auto v = static_cast<OvrActorPropertyVector2f*>(a_property)->GetValue();
			ovr_engine::OvrAlignMethod left;
			ovr_engine::OvrAlignMethod right;
			if (v._x == 0)
				left = ovr_engine::eTextAlignLeft;
			else if (v._x == 1)
				left = ovr_engine::eTextAlignMiddle;
			else if (v._x == 2)
				left = ovr_engine::eTextAlignRight;
			if (v._y == 0)
				right = ovr_engine::eTextAlignUp;
			else if (v._y == 1)
				right = ovr_engine::eTextAlignMiddle;
			else if (v._y == 2)
				right = ovr_engine::eTextAlignDown;
			com->SetAlignMethod(left,right);
		}
		else if (a_property->property_id == "border_space")
		{
			auto v = static_cast<OvrActorPropertyVector2f*>(a_property)->GetValue();
			com->SetTextMargin(v._x,v._y);
		}
		else if (a_property->property_id == "word_space")
		{
			auto v = static_cast<OvrActorPropertyFloat*>(a_property)->GetValue();
			com->SetWarpWidth(v);
		}
		else if (a_property->property_id == "row_space")
		{
			auto v = static_cast<OvrActorPropertyFloat*>(a_property)->GetValue();
			com->SetLineWarp(v);
		}
	}

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupCollideBox

	OvrActorPropertyGroupCollideBox::OvrActorPropertyGroupCollideBox(IOvrActorPropertyGroup* a_parent) :OvrActorPropertyGroupFun(a_parent, kOPTGCollideBox)
	{
		property_id = "group_collide_box";
		property_name = "碰撞盒";
	}

	void OvrActorPropertyGroupCollideBox::OnInit()
	{
		auto com = static_cast<ovr_engine::OvrBoxColliderComponent*>(component);

		AddProperty(IOvrActorProperty::Create(kOPTVector3f, "center_point", "中心点"));
		Init_Value(property_map["center_point"], com->GetCenter());

		AddProperty(IOvrActorProperty::Create(kOPTVector3f, "size", "大小"));
		Init_Value(property_map["size"], com->GetSize());
	}

	void OvrActorPropertyGroupCollideBox::DoPropertyChange(IOvrActorProperty* a_property)
	{
		auto com = static_cast<ovr_engine::OvrBoxColliderComponent*>(component);

		if (a_property->property_id == "center_point")
		{
			auto& v = static_cast<OvrActorPropertyVector3f*>(a_property)->GetValue();
			com->SetCenter(v);
		}
		else if (a_property->property_id == "size")
		{
			auto& v = static_cast<OvrActorPropertyVector3f*>(a_property)->GetValue();
			com->SetSize(v);
		}
	}

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupCamera

	OvrActorPropertyGroupCamera::OvrActorPropertyGroupCamera(IOvrActorPropertyGroup* a_parent) :OvrActorPropertyGroupFun(a_parent, kOPTGCamera)
	{
		property_id = "group_camera";
		property_name = "相机";
	}

	void OvrActorPropertyGroupCamera::OnInit()
	{
		auto com = static_cast<ovr_engine::OvrCameraComponent*>(component);

		auto field = AddProperty(IOvrActorProperty::Create(kOPTFloat, "field_view", "视场"));
		auto float_field = static_cast<OvrActorPropertyFloat*>(field);
		float_field->SetAttribute("is_slide", "true");
		float_field->SetRange(0, 359);

		Init_Value(property_map["field_view"], com->GetFieldView());

		AddProperty(IOvrActorProperty::Create(kOPTFloat, "w_h_ratio", "宽高比"));
		Init_Value(property_map["w_h_ratio"], com->GetAspectRatio());

		AddProperty(IOvrActorProperty::Create(kOPTFloat, "near", "近裁剪面"));
		Init_Value(property_map["near"], com->GetNearClip());

		AddProperty(IOvrActorProperty::Create(kOPTFloat, "far", "远裁剪面"));
		Init_Value(property_map["far"], com->GetFarClip());
	}

	void OvrActorPropertyGroupCamera::DoPropertyChange(IOvrActorProperty* a_property)
	{
		auto com = static_cast<ovr_engine::OvrCameraComponent*>(component);

		if (a_property->property_id == "field_view")
		{
			auto v = static_cast<OvrActorPropertyFloat*>(a_property)->GetValue();
			com->SetFieldView(v);
		}
		else if (a_property->property_id == "w_h_ratio")
		{
			auto v = static_cast<OvrActorPropertyFloat*>(a_property)->GetValue();
			com->SetAspectRatio(v);
		}
		else if (a_property->property_id == "near")
		{
			auto v = static_cast<OvrActorPropertyFloat*>(a_property)->GetValue();
			com->SetNearClip(v);
		}
		else if (a_property->property_id == "far")
		{
			auto v = static_cast<OvrActorPropertyFloat*>(a_property)->GetValue();
			com->SetFarClip(v);
		}
	}
}
