﻿#include "../headers.h"
#include "ovr_material_file_property.h"
#include <wx_engine/ovr_material_manager.h>
#include <wx_project/command/ovr_material_changed.h>
#include <wx_engine/ovr_texture_manager.h>
#include <wx_engine/ovr_gpu_program.h>
#include <wx_engine/ovr_gpu_program_manager.h>
#include <wx_engine/ovr_material.h>

namespace ovr_project 
{
	OvrMaterialFileProperty::~OvrMaterialFileProperty() 
	{
		if (nullptr != material_file)
		{
			ovr_asset::OvrArchiveAssetManager::GetIns()->DestoryAsset(material_file);
			material_file = nullptr;
		}
	}

	bool OvrMaterialFileProperty::Init(const OvrString& a_file_path) 
	{
		material_file_path = a_file_path;
		material_ins = (ovr_engine::OvrMaterial*)ovr_engine::OvrMaterialManager::GetIns()->GetByName(a_file_path);

		//磁盘加载
		material_file = (ovr_asset::OvrArchiveMaterial*)ovr_asset::OvrArchiveAssetManager::GetIns()->LoadAsset(a_file_path);
		if (nullptr == material_file)
		{
			return false;
		}

		//确认是不是材质
		if (ovr_asset::kAssetMaterial != material_file->GetType())
		{
			ovr_asset::OvrArchiveAssetManager::GetIns()->DestoryAsset(material_file);
			material_file = nullptr;
			return false;
		}

		current_type = material_file->GetShaderName();

		FillParamList();
		FillParamValueFromFile();

		return true;
	}

	//设置材质类型
	bool OvrMaterialFileProperty::SetType(const OvrString& a_name) 
	{
		if (nullptr == material_file)
		{
			return false;
		}
		//查询是否存在此shader
		ovr_engine::OvrGpuProgram* new_shader = ovr_engine::OvrGpuProgramManager::GetIns()->GetProgram(a_name);
		if (nullptr == new_shader)
		{
			return false;
		}

		material_file->SetShaderName(a_name);

		if (nullptr != material_ins)
		{
			material_ins->SetProgram(new_shader);
		}

		current_type = a_name;

		FillParamList();
		FillParamValueFromFile();

		return true;
	}

	//设置参数值
	bool OvrMaterialFileProperty::SetParamValue(const OvrString& a_display_name, const OvrString& a_value)
	{
		bool is_success = false;

		if (a_value.GetStringSize() <= 0 || a_value == "null")
		{
			return false;
		}

		//获取参数信息
		OvrMaterialParam* param = GetParam(a_display_name);
		if (nullptr == param)
		{
			return false;
		}
		param->value = a_value;
		if (nullptr == material_ins)
		{
			return true;
		}

		//目前只支持纹理
		if (param->type == OvrMaterialParam::kTexture)
		{
			//更新实例里面的值
			ovr_engine::OvrTexture* texture = (ovr_engine::OvrTexture*)ovr_engine::OvrTextureManager::GetIns()->GetByName(a_value);
			if (nullptr != texture)
			{
				//已经加载到内存中 可以直接设置 
				ovr_engine::OvrGpuProgramParameters* shader_params = material_ins->GetProgramParameters();
				shader_params->SetNameConstant(param->gpu_name, texture);
			}
			else
			{
				//需要在渲染线程中加载 然后设置
				OvrMaterialChangeTexture* new_command = new OvrMaterialChangeTexture(material_ins);
				new_command->SetTexture(param->gpu_name, a_value);
				OvrProject::GetIns()->GetCommandManager()->PushCommand(new_command);
			}
			
			is_success = true;
		}
		else if (param->type == OvrMaterialParam::kEnum)
		{
			if (param->display_name == "透明")
			{
				int blend_src = 0; int blend_dst = 0;
				if (GetBlendValueFromName(a_value, blend_src, blend_dst))
				{
					material_ins->SetSceneBlend((ovr_engine::SceneBlendFactor)blend_src, (ovr_engine::SceneBlendFactor)blend_dst);
				}
				is_success = true;
			}
		}
		else if (param->type == OvrMaterialParam::kColor)
		{
			if (param->display_name == "DiffuseColor")
			{
				OvrVector4 values;
				if (GetVector4(a_value, values)) 
				{
					//已经加载到内存中 可以直接设置 
					ovr_engine::OvrGpuProgramParameters* shader_params = material_ins->GetProgramParameters();
					shader_params->SetNameConstant(param->gpu_name, values);
				}
				is_success = true;
			}
		}
		return is_success;
	}

	bool OvrMaterialFileProperty::Save() 
	{
		material_file->SetShaderName(current_type);

		for (auto it = param_list.begin(); it != param_list.end(); it++)
		{
			if (it->type == OvrMaterialParam::kTexture)
			{
				if (it->value.GetStringSize() > 0 && it->value != "null")
				{
					material_file->SetConstantSample2D(it->gpu_name, it->value);
				}
			}
			else if (it->type == OvrMaterialParam::kEnum)
			{
				int blend_src = 0; int blend_dst = 0;
				if (GetBlendValueFromName(it->value, blend_src, blend_dst)) 
				{
					material_file->SetSceneBlend(blend_src, blend_dst);
				}
			}
			else if (it->type == OvrMaterialParam::kColor)
			{
				OvrVector4 values;
				if (GetVector4(it->value, values))
				{
					material_file->SetConstantFloat4(it->gpu_name, values);
				}
			}
		}
		material_file->Save();
		return true;
	}

	//填充参数列表
	bool OvrMaterialFileProperty::FillParamList() 
	{
		//清空参数列表
		param_list.clear();

		ovr_engine::OvrGpuProgram* shader =  ovr_engine::OvrGpuProgramManager::GetIns()->GetProgram(current_type);
		if (nullptr == shader)
		{
			return false;
		}
		const ovr_engine::OvrGpuProgramConstants* shader_constants = shader->GetShaderConstants();
		if (nullptr == shader_constants)
		{
			return false;
		}
		const ovr_engine::ConstantList* constant_list = shader_constants->GetCustomConstantList();

		//遍历参数列表
		for (ovr::uint32 index = 0; index < constant_list->size(); index++)
		{
			const ovr_engine::OvrConstant& constant = (*constant_list)[index];

			if (constant.gpu_name == "DiffuseColor")
			{
				OvrMaterialParam diffuse_color(constant.gpu_name, OvrMaterialParam::kColor, "1.0:1.0:1.0:1.0");
				diffuse_color.display_name = constant.gpu_name;
				param_list.push_back(diffuse_color);
				continue;
			}

			if (!constant.IsSampler())
			{
				//目前只支持纹理
				continue;
			}

			//构建材质参数
			OvrMaterialParam temp_param(constant.gpu_name, OvrMaterialParam::kTexture);
			GetTextureDisplayName(constant.gpu_name, temp_param.display_name);
			param_list.push_back(temp_param);
		}

		//添加融合参数
		OvrMaterialParam param("blend", OvrMaterialParam::kEnum);
		param.display_name = "透明";
		param.reserve = "关闭:透明:背景融合";
		param_list.push_back(param);
		return true;
	}

	//设置参数的值
	void OvrMaterialFileProperty::FillParamValueFromFile()
	{
		for (auto it = param_list.begin(); it != param_list.end(); it++)
		{
			if (it->type == OvrMaterialParam::kTexture)
			{
				if (!material_file->GetSample2DByName(it->gpu_name, it->value)) 
				{
					it->value = "null";
				}
			}
			else if (it->type == OvrMaterialParam::kColor)
			{
				OvrVector4 value;
				if (material_file->GetVector4ByName(it->gpu_name, value))
				{
					char temp[100];
					sprintf(temp, "%f:%f:%f:%f", value[0], value[1], value[2], value[3]);
					it->value = temp;
				}
				else 
				{
					material_file->SetConstantFloat4(it->gpu_name, OvrVector4(1.0f));

					if (nullptr != material_ins)
					{
						//已经加载到内存中 可以直接设置 
						ovr_engine::OvrGpuProgramParameters* shader_params = material_ins->GetProgramParameters();
						shader_params->SetNameConstant(it->gpu_name, OvrVector4(1.0f));
					}
				}
			}
			else if (it->type == OvrMaterialParam::kEnum)
			{
				if (it->gpu_name == "blend")
				{
					GetBlendValue(it->value);
				}
			}
		}
	}

	void OvrMaterialFileProperty::GetBlendValue(OvrString& a_value)
	{
		//将修改成从文件获取 material_file
		if (nullptr == material_file)
		{
			a_value = "关闭";
			return;
		}

		//实例存在
		ovr_engine::SceneBlendFactor src_factor = (ovr_engine::SceneBlendFactor)material_file->GetSceneBlendSrc();
		ovr_engine::SceneBlendFactor dst_factor = (ovr_engine::SceneBlendFactor)material_file->GetSceneBlendDest();
		if (ovr_engine::SBF_ONE == src_factor && ovr_engine::SBF_ZERO == dst_factor)
		{
			a_value = "关闭";
		}
		else if (ovr_engine::SBF_SOURCE_ALPHA == src_factor && ovr_engine::SBF_ONE_MINUS_SOURCE_ALPHA == dst_factor)
		{
			a_value = "透明";
		}
		else if (ovr_engine::SBF_SOURCE_ALPHA == src_factor && ovr_engine::SBF_ONE == dst_factor)
		{
			a_value = "背景融合";
		}
		else
		{
			a_value = "未知";
		}
	}

	void OvrMaterialFileProperty::GetTextureDisplayName(const OvrString& a_gpu_name, OvrString& a_display_name) 
	{
		if (a_gpu_name == "Texture0")
		{
			a_display_name = "漫反射";
		}
		else if (a_gpu_name == "Texture1")
		{
			a_display_name = "自发光";
		}
		else if (a_gpu_name == "Texture2")
		{
			a_display_name = "法线";
		}
		else if (a_gpu_name == "Texture3")
		{
			a_display_name = "高光";
		}
		else
		{
			a_display_name = a_gpu_name;
		}
	}

	bool OvrMaterialFileProperty::GetVector4(const OvrString& a_string, OvrVector4& a_value) 
	{
		std::list<OvrString> string_list;
		ovr::uint32 count = a_string.Split(':', string_list);
		if (count != 4)
		{
			return false;
		}
		
		auto it = string_list.begin();
		a_value[0] = (float)atof(it->GetStringConst());
		it++;
		a_value[1] = (float)atof(it->GetStringConst());
		it++;
		a_value[2] = (float)atof(it->GetStringConst());
		it++;
		a_value[3] = (float)atof(it->GetStringConst());
		return true;
	}

	//根据名字获取值
	bool OvrMaterialFileProperty::GetBlendValueFromName(const OvrString& a_display_name, ovr::int32& a_src_value, ovr::int32& a_dst_value)
	{
		if (a_display_name == "关闭")
		{
			a_src_value = ovr_engine::SBF_ONE;
			a_dst_value = ovr_engine::SBF_ZERO;
		}
		else if (a_display_name == "透明")
		{
			a_src_value = ovr_engine::SBF_SOURCE_ALPHA;
			a_dst_value = ovr_engine::SBF_ONE_MINUS_SOURCE_ALPHA;
		}
		else if (a_display_name == "背景融合")
		{
			a_src_value = ovr_engine::SBF_SOURCE_ALPHA;
			a_dst_value = ovr_engine::SBF_ONE;
		}
		else
		{
			return false;
		}
		return true;
	}
}