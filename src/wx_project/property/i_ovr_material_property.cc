﻿#include "../headers.h"
#include <wx_project/property/i_ovr_material_property.h>
#include <wx_engine/ovr_material_manager.h>
#include <wx_engine/ovr_gpu_program_manager.h>
#include "ovr_material_file_property.h"

namespace ovr_project 
{
	IOvrMaterialProperty* IOvrMaterialProperty::Create(const OvrString& a_material_file_path) 
	{
		//直接使用文件
		OvrMaterialFileProperty* new_ins = new OvrMaterialFileProperty;
		if (nullptr != new_ins)
		{
			if (!new_ins->Init(a_material_file_path))
			{
				delete new_ins;
				new_ins = nullptr;
			}
		}
		return new_ins;
	}

	void IOvrMaterialProperty::Destory(IOvrMaterialProperty* a_ins) 
	{
		if (nullptr != a_ins)
		{
			delete a_ins;
			a_ins = nullptr;
		}
	}

	//获取材质种类
	void IOvrMaterialProperty::GetTypeList(std::list<OvrString>& a_name_list)
	{
		ovr_engine::OvrGpuProgramManager::GpuProgramNameList name_list = ovr_engine::OvrGpuProgramManager::GetIns()->GetAllProgramNames();
		for (ovr::uint32 i = 0; i < name_list.size(); i++)
		{
			a_name_list.push_back(name_list[i]);
		}
	}

	//获取参数信息
	OvrMaterialParam* IOvrMaterialProperty::GetParam(const OvrString& a_display_name)
	{
		for (auto it = param_list.begin(); it != param_list.end(); it++)
		{
			if (it->display_name == a_display_name)
			{
				return &(*it);
			}
		}
		return nullptr;
	}

}