﻿#pragma once

#include "../headers.h"
#include "wx_project/property/i_ovr_actor_property_group.h"

namespace ovr_project 
{
	IOvrActorPropertyGroup::IOvrActorPropertyGroup(IOvrActorPropertyGroup* a_parent, OvrActorPropertyGroupType a_group_type)
		:IOvrActorProperty(a_parent, kOPTGroup)
	{ 
		group_type = a_group_type;
	}

	IOvrActorPropertyGroup::~IOvrActorPropertyGroup()
	{
		if (property_list.size() > 0)
		{
			for (auto item : property_list)
			{
				delete item;
			}
			property_list.clear();
			property_map.clear();
		}
	}

	void IOvrActorPropertyGroup::init(ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_compnent)
	{
		actor = a_actor;
		component = a_compnent;

		if (a_actor != nullptr)
		{
			component != nullptr ? OnInit() : OnInitWithNoComponent();
		}
	}

	void IOvrActorPropertyGroup::Reload()
	{
		DoReload();

		is_need_reload = false;
		is_layout_changed = false;
		is_async_load = false;
	}

	IOvrActorProperty* IOvrActorPropertyGroup::AddProperty(IOvrActorProperty* a_property)
	{
		assert(a_property != nullptr && a_property->property_id.GetStringSize() > 0);

		a_property->parent = this;

		property_list.push_back(a_property);

		property_map[a_property->property_id] = a_property;

		return a_property;
	}

	void IOvrActorPropertyGroup::clear()
	{
		for (auto item : property_list)
		{
			delete item;
		}

		property_map.clear();
		property_list.clear();
	}

	IOvrActorProperty* IOvrActorPropertyGroup::GetProperty(const OvrString& a_id)
	{
		if (property_map.find(a_id) == property_map.end())
		{
			assert(false);
			return nullptr;
		}
		return property_map[a_id];
	}

	void IOvrActorPropertyGroup::OnPropertyChange(IOvrActorProperty* a_property)
	{
		if (actor != nullptr)
		{
			if (component != nullptr || group_type < kOPTGComponent)
			{
				if (a_property->IsNeedUpdateGroup())
				{
					is_layout_changed = true;
					SetAsyncLoad(a_property->IsAsyncLoad());
				}
				DoPropertyChange(a_property);
			}
		}
	}
}