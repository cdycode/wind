﻿#pragma once

#include "../headers.h"

#include "wx_project/property/i_ovr_actor_property.h"
#include "wx_project/property/i_ovr_actor_property_group.h"

namespace ovr_project 
{

	IOvrActorProperty::~IOvrActorProperty()
	{
		printf("");
	}

	void IOvrActorProperty::OnValueChanged()
	{
		assert(parent != nullptr);

		parent->OnPropertyChange(this);
	}


	OvrString IOvrActorProperty::GetAttribute(const OvrString& a_key)
	{
		if (atts.find(a_key) != atts.end())
		{
			return atts[a_key];
		}
		return "";
	}

	void IOvrActorProperty::SetAttribute(const OvrString& a_key, const OvrString& a_value)
	{
		atts[a_key] = a_value;
	}
}

