﻿#include "headers.h"
#include <wx_engine/ovr_camera_component.h>
#include <wx_project/ovr_scene_editor.h>
#include <wx_project/ovr_scene_event_manager.h>
#include <wx_core/ovr_scene_context.h>
#include <wx_core/ovr_scene_camera.h>
#include <wx_plugin/wx_i_archive_project.h>
#include "ovr_indicator.h"
#include "ovr_world_axis_actor.h"

namespace ovr_project
{
	bool OvrSceneEditor::Open(const OvrString& a_file_path)
	{
		Close();

		scene_context = wx_plugin::wxIArchiveProject::Load()->LoadScene(a_file_path);

		//添加坐标
		indicator = scene_context->GetScene()->CreateActorT<OvrIndicator>();
		indicator->Init(this);

		world_axis = scene_context->GetScene()->CreateActorT<OvrWorldAxisActor>();
		world_axis->Init();

		event_manager = new OvrSceneEventManager;

		return true;
	}

	void OvrSceneEditor::Close()
	{
		if (nullptr != indicator)
		{
			indicator->Uninit();
			scene_context->GetScene()->DestoryActor(indicator);
			indicator = nullptr;
		}

		if (nullptr != world_axis)
		{
			world_axis->Uninit();
			scene_context->GetScene()->DestoryActor(world_axis);
			world_axis = nullptr;
		}

		if (nullptr != event_manager)
		{
			delete event_manager;
			event_manager = nullptr;
		}

		if (nullptr != scene_context)
		{
			scene_context->Uninit();
			delete scene_context;
			scene_context = nullptr;
		}
	}

	void OvrSceneEditor::SetMode(Mode a_mode)
	{
		current_mode = a_mode;
		if (current_mode == kPreview)
		{
			//处理场景编辑时的相关事件
			SetSelectedActor(nullptr);
		}
		else
		{
			ovr_core::OvrSceneCamera* scene_camera = scene_context->GetSceneCamera();
			scene_camera->SetCurrentCamera(scene_camera->GetRoamCamera());
		}
	}

	const OvrString& OvrSceneEditor::GetFilePath()const
	{
		if (nullptr != scene_context)
		{
			return scene_context->GetFilePath();
		}
		return OvrString::empty;
	}

	void OvrSceneEditor::Update()
	{
		event_manager->Update(this);

		indicator->Update();

		scene_context->Update();
	}

	void OvrSceneEditor::Draw()
	{
		scene_context->Draw();

		if (kEditor == current_mode)
		{
			actor_track_line.draw();
		}
	}

	void OvrSceneEditor::SetActorAixsType(OvrAxisType a_type)
	{
		indicator->SetAxisType(a_type);
	}

	void OvrSceneEditor::SelectedActor(float a_x_pos, float a_y_pos)
	{
		SetMode(kEditor);

		const ovr_engine::RayIntersetResults& actor_queue = scene_context->GetSceneCamera()->RayInterset(a_x_pos, a_y_pos, true);

		ovr_engine::OvrActor* new_actor = nullptr;
		if (actor_queue.size() > 0)
		{
			for (ovr::uint32 index = 0; index < actor_queue.size(); index++)
			{
				new_actor = actor_queue[index].component->GetParentActor();
				break;
			}
		}

		if (!SetSelectedActor(new_actor))
		{
			//设置当前选中的Actor失败
			return;
		}

		OvrSceneListener* listener = OvrProject::GetIns()->GetListener();
		if (nullptr != listener)
		{
			if (nullptr != new_actor)
			{
				listener->OnSetSelectedItem(new_actor->GetUniqueName().GetStringConst());
			}
			else
			{
				listener->OnSetSelectedItem("");
			}
		}
	}

	bool OvrSceneEditor::SetSelectedActor(ovr_engine::OvrActor* a_new_selected_actor)
	{
		if (selected_actor == a_new_selected_actor)
		{
			//已经相同 不再进行变化
			return true;
		}

		if (nullptr == a_new_selected_actor)
		{
			if (nullptr != selected_actor)
			{
				selected_actor->ShowBoundingBox(false);
			}
			indicator->SetVisible(false);
			selected_actor = a_new_selected_actor;

			return true;
		}

		//属于编辑状态的Actor 是不可以被选中的
		if (!IsShowInScene(a_new_selected_actor))
		{
			return false;
		}

		if (nullptr != selected_actor)
		{
			selected_actor->ShowBoundingBox(false);
		}

		selected_actor = a_new_selected_actor;

		indicator->SetVisible(true);
		selected_actor->ShowBoundingBox(true);

		const OvrMatrix4& worldmat4 = selected_actor->GetWorldTransform();
		OvrQuaternion quat;
		OvrVector3  position;
		worldmat4.decompose(nullptr, &quat, &position);

		indicator->SetPosition(position);
		indicator->SetQuaternion(quat);
		return true;
	}

	void OvrSceneEditor::RemoveActor(const OvrString& a_actor_unique_name)
	{
		ovr_engine::OvrActor* actor = scene_context->GetActorByUniqueName(a_actor_unique_name);
		RemoveActor(actor);
	}

	void OvrSceneEditor::RemoveActor(ovr_engine::OvrActor* a_actor)
	{
		if (nullptr == a_actor)
		{
			return;
		}

		OvrSceneListener* listener = OvrProject::GetIns()->GetListener();
		if (nullptr != listener)
		{
			listener->OnRmvActor(a_actor->GetUniqueName());
		}

		scene_context->GetScene()->DestoryActor(a_actor);
	}

	ovr_engine::OvrActor*	OvrSceneEditor::GetActorByUniqueName(const OvrString& a_unique_name)
	{
		return scene_context->GetActorByUniqueName(a_unique_name);
	}

	bool OvrSceneEditor::GetCurrentPickRay(OvrRay3f& a_ray)
	{
		return GeneratePickRay(event_manager->GetMousePosX(), event_manager->GetMousePosY(), a_ray);
	}

	bool OvrSceneEditor::IsShowInScene(ovr_engine::OvrActor* a_actor) 
	{
		if (indicator->IsIncludeActor(a_actor) || world_axis->IsIncludeActor(a_actor))
		{
			return false;
		}
		if (a_actor == scene_context->GetSceneCamera()->GetRoamCamera())
		{
			return false;
		}
		return  true;
	}

	bool OvrSceneEditor::GeneratePickRay(float a_x_pos, float a_y_pos, OvrRay3f& a_ray) 
	{
		ovr_core::OvrSceneCamera* scene_camera = scene_context->GetSceneCamera();
		ovr_engine::OvrCameraComponent* current_camera = scene_camera->GetCurrentCameraComponent();
		a_ray = current_camera->GeneratePickRay(a_x_pos, a_y_pos);
		return true;
	}

	void OvrSceneEditor::CheckSelectedActor(ovr_engine::OvrActor* a_actor)
	{
		int count = a_actor->GetChildCount();

		for (int i = 0; i < count; i++)
		{
			ovr_engine::OvrActor* child_actor = a_actor->GetChild(0);
			CheckSelectedActor(child_actor);
		}

		//判断是否将选中的Actor移除
		if (a_actor == selected_actor)
		{
			SetSelectedActor(nullptr);
		}
	}
}
