﻿#ifndef OVR_PROJECT_OVR_INDICATOR_H_
#define OVR_PROJECT_OVR_INDICATOR_H_

#include <wx_engine/ovr_actor.h>

namespace ovr_engine 
{
	class OvrStaticMeshComponent;
	class OvrMaterial;
}

namespace ovr_project 
{
	class OvrSceneEditor;
	class OvrIndicator : public ovr_engine::OvrActor
	{
	public:
		enum AxisName
		{
			kAxisNameNone = 0,
			kAxisNameY,
			kAxisNameX,
			kAxisNameZ
		};

	public:
		OvrIndicator(ovr_engine::OvrScene* a_scene)
			:OvrActor(a_scene) {}
		~OvrIndicator() {}

		bool	Init(OvrSceneEditor* a_scene_editor);
		void	Uninit();

		void	SetAxisType(OvrAxisType a_new_type);

		void	Update();

		//是否包含当前的Actor
		bool	IsIncludeActor(ovr_engine::OvrActor* a_actor);

	private:
		void	BeginPressed(ovr_engine::OvrActor* a_selected_actor);
		void	GeneratePane();
		void	UpdateSelectedActor(float a_mouse_x, float a_mouse_y, ovr_engine::OvrActor* a_selected_actor);

		AxisName	FindPickAxisName(const OvrRay3f& a_ray);
		void		SetAxisName(AxisName a_axis_name);

		void	SetAxisFixedSize();

		//是否是子actor
		bool	IsChild(ovr_engine::OvrActor* a_host_actor, ovr_engine::OvrActor* a_actor);
	private:
		OvrSceneEditor*	scene_editor = nullptr;

		OvrAxisType		current_axis_type = kAxisTransform;
		AxisName		current_axis_name = kAxisNameNone;

		ovr_engine::OvrActor*	scale_actor = nullptr;
		ovr_engine::OvrActor*	transform_actor = nullptr;
		ovr_engine::OvrActor*	rotate_actor = nullptr;

		ovr_engine::OvrMaterial* material[5];
		ovr_engine::OvrStaticMeshComponent* mesh_component[9];

		//坐标轴的交互
		bool	is_pressed = false;
		float	pressed_x = 0.0f;
		float	pressed_y = 0.0f;
		OvrVector3		start_pos;			//鼠标单击按下时 焦点
		OvrVector3		indicator_pos;
		OvrQuaternion   indicator_quat;
		OvrPlane3f		help_plane; //(look, point);
		OvrTransform	default_transform;
		OvrTransform    world_transform;
		OvrMatrix4      world_transform_matrix;			//被选中Actor的父空间到世界空间的变化矩阵
		OvrMatrix4      world_transform_matrix_inverse;	//从世界空间到被选中actor的父空间变化
	};
}

#endif
