#ifndef OVR_PROJECT_OVR_WORLD_AXIS_ACTOR_H_
#define OVR_PROJECT_OVR_WORLD_AXIS_ACTOR_H_

#include <wx_engine/ovr_actor.h>

namespace ovr_engine
{
	class OvrScene;
	class OvrMaterial;
	class OvrStaticMeshComponent;
}

namespace ovr_project 
{
	class OvrWorldAxisActor : public ovr_engine::OvrActor
	{
	public:
		OvrWorldAxisActor(ovr_engine::OvrScene* a_scene)
			:OvrActor(a_scene){}
		virtual ~OvrWorldAxisActor() {}

		bool	Init();
		void	Uninit();

		//是否包含当前的Actor
		bool	IsIncludeActor(ovr_engine::OvrActor* a_actor);

	private:
		//是否是子actor
		bool	IsChild(ovr_engine::OvrActor* a_host_actor, ovr_engine::OvrActor* a_actor);

	private:
		ovr_engine::OvrMaterial*			material[3];
		ovr_engine::OvrActor*				transform_actor = nullptr;
		ovr_engine::OvrStaticMeshComponent* mesh_component[3];
	};
}

#endif
