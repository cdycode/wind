﻿#include "headers.h"
#include "wx_engine/ovr_engine.h"
#include "wx_core/ovr_scene_context.h"
#include <wx_project/ovr_actor_track_line.h>
#include <wx_project/ovr_scene_editor.h>
#include "wx_core/ovr_scene_context.h"
#include <wx_core/ovr_scene_camera.h>

namespace ovr_project
{
	OvrActorTrackLine::~OvrActorTrackLine()
	{
		clear();
	}

	void OvrActorTrackLine::setTrackLine(OvrString a_actor_id, std::vector<OvrVector3>* a_lines, std::vector<OvrVector3>* a_points, int a_pt_selected, const OvrColorValue& a_color)
	{
		clear();

		actor_id = a_actor_id;
		lines = a_lines;
		points = a_points;
		pt_select = a_pt_selected;
		pt_select_color = a_color;
		is_show = true;
	}

	void OvrActorTrackLine::setSelectPointColor(int a_pt_selected, const OvrColorValue& a_color)
	{
		pt_select = a_pt_selected;
		pt_select_color = a_color;
	}

	void OvrActorTrackLine::clear()
	{
		actor_id = "";
		

		if (lines != nullptr)
		{
			delete lines;
			lines = nullptr;
		}
		if (points != nullptr)
		{
			delete points;
			points = nullptr;
		}
	}

	bool OvrActorTrackLine::checkValid()
	{
		if (actor_id.GetStringSize() > 0)
		{
			if (OvrProject::GetIns()->GetCurrentScene()->GetActorByUniqueName(actor_id) != nullptr)
			{
				return lines != nullptr && lines->size() > 0;
			}
		}

		return false;
	}

	void OvrActorTrackLine::draw()
	{	
		if (is_show && checkValid())
		{
			//auto& view = context->GetSceneCamera()->GetCurrentCamera()->GetViewMatrix();
			//auto& proj = context->GetSceneCamera()->GetCurrentCamera()->GetProjectMatrix();
			if (lines != nullptr && lines->size() > 1)
			{
				//ovr_engine::OvrEngine::GetIns()->GetDriver()->Draw3DLine(*lines, view, proj, line_size, OvrColorValue(1.0f, 1.0f, 0.0f, 0.8f));
			}
			if (points != nullptr && points->size() > 0)
			{
				//ovr_engine::OvrEngine::GetIns()->GetDriver()->Draw3DPoint(*points, view, proj, pt_size, OvrColorValue(0.0f, 1.0f, 1.0f, 0.8f));
				if (pt_select != -1 && pt_select < (int)points->size())
				{
					std::vector<OvrVector3> pt;
					pt.push_back(points->at(pt_select));
					//ovr_engine::OvrEngine::GetIns()->GetDriver()->Draw3DPoint(pt, view, proj, sel_pt_size, pt_select_color);
				}
			}
		}
	}
}

