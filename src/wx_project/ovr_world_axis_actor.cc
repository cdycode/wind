#include "headers.h"
#include "ovr_world_axis_actor.h"
#include "wx_engine/ovr_gpu_program_manager.h"
#include "wx_engine/ovr_material_manager.h"
#include "wx_engine/ovr_material.h"
#include "wx_engine/ovr_mesh_component.h"
#include "wx_engine/ovr_static_mesh_component.h"

namespace ovr_project 
{
	bool OvrWorldAxisActor::Init()
	{
		ovr_engine::OvrGpuProgram* program = ovr_engine::OvrGpuProgramManager::GetIns()->GetProgram("AxisWorldRender");

		material[0] = ovr_engine::OvrMaterialManager::GetIns()->Create("<world_indicator_green>");
		material[0]->SetProgram(program);
		material[0]->GetProgramParameters()->SetNameConstant("UniformColor", OvrVector4(0.0, 1.0, 0.0, 1.0));
		material[0]->SetDepthCheckEnable(false);

		material[1] = ovr_engine::OvrMaterialManager::GetIns()->Create("<world_indicator_red>");
		material[1]->SetProgram(program);
		material[1]->GetProgramParameters()->SetNameConstant("UniformColor", OvrVector4(1.0, 0.0, 0.0, 1.0));
		material[1]->SetDepthCheckEnable(false);

		material[2] = ovr_engine::OvrMaterialManager::GetIns()->Create("<world_indicator_blue>");
		material[2]->SetProgram(program);
		material[2]->GetProgramParameters()->SetNameConstant("UniformColor", OvrVector4(0.0, 0.0, 1.0, 1.0));
		material[2]->SetDepthCheckEnable(false);

		ovr_engine::OvrMesh* mesh = ovr_engine::OvrMeshManager::GetIns()->Load("system/content/indicator/move.mesh");
		SetScale(OvrVector3(0.001f, 0.001f, 0.001f));

		//Y
		transform_actor = CreateChildActor();
		mesh_component[0] = transform_actor->AddComponent < ovr_engine::OvrStaticMeshComponent > ();
		mesh_component[0]->SetMesh(mesh);
		mesh_component[0]->SetMaterial(0, material[0]);

		//X
		ovr_engine::OvrActor* actor = transform_actor->CreateChildActor();
		actor->Roll(-3.1415926f / 2);
		mesh_component[1] = actor->AddComponent<ovr_engine::OvrStaticMeshComponent>();
		mesh_component[1]->SetMesh(mesh);
		mesh_component[1]->SetMaterial(0, material[1]);

		//Z
		actor = transform_actor->CreateChildActor();
		actor->Pitch(3.1415926f / 2);
		mesh_component[2] = actor->AddComponent<ovr_engine::OvrStaticMeshComponent>();
		mesh_component[2]->SetMesh(mesh);
		mesh_component[2]->SetMaterial(0, material[2]);

		for (int index = 0; index < 3; index++)
		{
			mesh_component[index]->SetCastShadow(false);
			mesh_component[index]->SetCollisionEnabled(false);
		}

		return true;
	}

	void OvrWorldAxisActor::Uninit()
	{

	}

	bool OvrWorldAxisActor::IsIncludeActor(ovr_engine::OvrActor* a_actor)
	{
		return this == a_actor || IsChild(this, a_actor);
	}

	bool OvrWorldAxisActor::IsChild(ovr_engine::OvrActor* a_host_actor, ovr_engine::OvrActor* a_actor)
	{
		ovr::uint32 child_count = a_host_actor->GetChildCount();
		for (ovr::uint32 i = 0; i < child_count; i++)
		{
			if (a_host_actor->GetChild(i) == a_actor)
			{
				//跟自己相等
				return true;
			}
			if (IsChild(a_host_actor->GetChild(i), a_actor))
			{
				//是它的子
				return true;
			}
		}
		return false;
	}
}
