#ifndef __OVR_PROJECT_OVR_IMPORTER_MANAGER_H__
#define __OVR_PROJECT_OVR_IMPORTER_MANAGER_H__

#include <atomic>
#include <list>
#include <vector>
#include <functional>
#include <wx_base/wx_string.h>
#include "ovr_importer.h"

namespace ovr_project
{
	class OvrImporter;

	class OvrImportManager
	{
	public:
		OvrImportManager();
		~OvrImportManager();


		bool		Import(const std::vector<std::string>& a_asset_files, const char* a_out_dir, std::list<OvrString>& a_out_files, std::function<bool(const char*, bool)> &a_notify_callback);
		bool		IsHasFileExist(OvrImporter::FILE_IMPORT_TYPE a_efit,const char* a_out_dir, const char* a_file,OvrString& a_file_in_folder);
		ovr::uint32 GetPercent();
		void		CancelImport();

		//static OvrImportManager* GetImportMgr();
	private:
		void		ProcessHotUpdate(std::list<OvrString> & a_file);
	protected:
		std::atomic_bool			b_canceled;
		std::atomic_uint32_t		u_percent;  // [0 - 100]

		std::atomic_uint32_t n_total_files;				// 总文件数
		std::atomic_uint32_t n_dealed_file;				// 当前已处理几个文件

		OvrImporter * cur_importer = nullptr;
	};

} // namespace ovr_project

#endif // __OVR_PROJECT_OVR_IMPORTER_MANAGER_H__