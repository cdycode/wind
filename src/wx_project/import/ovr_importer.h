#ifndef __OVR_PROJECT_OVR_IMPORTER_H__
#define __OVR_PROJECT_OVR_IMPORTER_H__


#include <atomic>
#include <list>
#include <wx_base/wx_string.h>

namespace ovr_project
{
	class OvrImporter
	{

	public:
		enum FILE_IMPORT_TYPE { IFT_MODEL, IFT_COPY, IFT_TEXTURE };

		OvrImporter()
			: b_canceled(false)
			, u_percent(0)
		{}
		virtual ~OvrImporter() {};

		virtual bool		Import(const char* a_file, const char* a_out_dir, std::list<OvrString>& a_out_file_list) = 0;
		virtual ovr::uint32 GetPercent() = 0;
		virtual void		CancelImport() = 0;

		// get all the improter with FILE_IMPORT_TYPE
	static OvrImporter*		GetOvrImporter(OvrImporter::FILE_IMPORT_TYPE a_file_type);
	static FILE_IMPORT_TYPE GetFileImportType(const char * a_file_name);
	protected:
		template <typename T> static T * GetImporter();

		void InitImporter();

	protected:
		std::atomic_bool			b_canceled;
		std::atomic_uint32_t		u_percent;  // [0 - 100]

	};
}


#endif  // __OVR_PROJECT_OVR_IMPORTER_H__