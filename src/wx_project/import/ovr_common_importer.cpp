#include <algorithm>
#include <string>
#include <functional>
#include <memory>
#include <fstream>
#include "ovr_common_importer.h"
#include <wx_project/ovr_project.h>

namespace ovr_project
{

	OvrCommonImporter::OvrCommonImporter()
		:OvrImporter()
	{
	}


	OvrCommonImporter::~OvrCommonImporter()
	{
	}

	bool OvrCommonImporter::Import(const char * a_file, const char * a_out_dir, std::list<OvrString>& a_out_file_list)
	{
		OvrString workDir = OvrProject::GetIns()->GetWorkDir();
		std::string strsrcfile(a_file);
#ifdef _WIN32
		//std::replace_if(strsrcfile.begin(), strsrcfile.end(), std::bind(std::equal_to<int>(), std::placeholders::_1, '/'), '\\');
		//int nfile_index = strsrcfile.rfind('\\');
		int nfile_index = strsrcfile.rfind('/');
#else 
		int nfile_index = strsrcfile.rfind('/');
#endif // !_WIN32
		std::string str_file_name = strsrcfile.substr(nfile_index + 1);
		std::string str_xd_path = +a_out_dir + str_file_name;
		std::string str_dest_file = workDir.GetStringConst() + str_xd_path;

#ifdef _WIN32
		//std::replace_if(str_dest_file.begin(), str_dest_file.end(), std::bind(std::equal_to<int>(), std::placeholders::_1, '/'), '\\');
#endif // _WIN32

		std::ifstream src_file(a_file, std::ios::binary);
		std::ofstream dest_file(str_dest_file.c_str(), std::ios::binary);

		if (!src_file.is_open() || !dest_file.is_open()) {
			return false;
		}

		src_file.seekg(0, std::ios::end);
		//std::streampos sp = ;
		uint64_t i_file_size = src_file.tellg().seekpos();
		src_file.seekg(0, std::ios::beg);
		const uint32_t cbs = 1024;
		char buff[cbs] = { 0 };
		while (!src_file.eof() && !b_canceled) {
			//uint64_t i_file_cur_pos = src_file.gcount();
			std::streampos sp = src_file.tellg();
			uint64_t i_file_cur_pos = sp.seekpos();
			u_percent = static_cast<uint32_t>(100.*i_file_cur_pos / i_file_size);
			src_file.read(buff, cbs);
			dest_file.write(buff, src_file.gcount());
		}

		if (b_canceled) {  // 如果取消, 删除正在复制的文件
			b_canceled = false;  // 还原
			dest_file.close();
			std::remove(str_dest_file.c_str());
			return false;
		}

		a_out_file_list.push_back(OvrString(str_xd_path.c_str()));
		return true;
	}

	ovr::uint32 OvrCommonImporter::GetPercent()
	{
		return u_percent;
	}

	void OvrCommonImporter::CancelImport()
	{
		b_canceled = true;
	}

}