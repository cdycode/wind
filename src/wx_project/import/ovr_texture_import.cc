﻿#include "../headers.h"
#include "ovr_texture_import.h"
#include "wx_fbx_import/ovr_image_import.h"

namespace ovr_project 
{
	OvrTextureImport::OvrTextureImport() 
	{
	}
	OvrTextureImport::~OvrTextureImport() 
	{
	}

	bool OvrTextureImport::Import(const char* a_file, const char* a_out_dir, std::list<OvrString>& a_out_file_list) 
	{
		u_percent = 0;

		OvrString out_file;
		OvrImageImporter image_importer;
		bool bresult = image_importer.Import(a_file, a_out_dir, out_file);
		if (!bresult)
			return false;

		a_out_file_list.push_back(out_file);

		u_percent = 100;

		return true;
	}
	ovr::uint32 OvrTextureImport::GetPercent()
	{
		return u_percent;
	}
	void OvrTextureImport::CancelImport()
	{

	}
}