﻿#ifndef OVR_PROJECT_OVR_TEXTURE_IMPORT_H_
#define OVR_PROJECT_OVR_TEXTURE_IMPORT_H_

#include "ovr_importer.h"

namespace ovr_project 
{
	class OvrTextureImport : public OvrImporter 
	{
	public:
		OvrTextureImport();
		virtual ~OvrTextureImport();

	public:
		virtual bool		Import(const char* a_file, const char* a_out_dir, std::list<OvrString>& a_out_file_list) override;
		virtual ovr::uint32 GetPercent() override;
		virtual void		CancelImport() override;
	};
}

#endif
