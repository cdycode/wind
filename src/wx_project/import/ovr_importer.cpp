#include "../headers.h"
#include "ovr_importer.h"
#include "ovr_common_importer.h"
#include "ovr_model_importer.h"
#include "ovr_texture_import.h"

#include <memory>

#define OVR_STR_TO_LOWER(STR)									\
{																\
	auto& f = std::use_facet<std::ctype<char>>(std::locale());	\
	f.tolower(&str_ext[0], &str_ext[0]+str_ext.size());			\
}

namespace ovr_project
{

	template<typename T>
	inline T * OvrImporter::GetImporter()
	{
		static std::unique_ptr<T> imp;
		if (imp == nullptr)
			imp.reset(new T());

		imp->InitImporter();  // need init the data every time call
		return imp.get();
	}

	void OvrImporter::InitImporter()
	{
		u_percent = 0;
		b_canceled = false;

		//is_import = false;
	}

	OvrImporter * OvrImporter::GetOvrImporter(OvrImporter::FILE_IMPORT_TYPE a_file_type)
	{
		switch (a_file_type)
		{
		case OvrImporter::IFT_MODEL: return OvrImporter::GetImporter<OvrModelImporter>();
		case OvrImporter::IFT_COPY: return  OvrImporter::GetImporter<OvrCommonImporter>();
		case OvrImporter::IFT_TEXTURE: return  OvrImporter::GetImporter<OvrTextureImport>();
		default:
			return nullptr;
		}
	}


	OvrImporter::FILE_IMPORT_TYPE OvrImporter::GetFileImportType(const char * a_file_name)
	{
		const char* tail = strrchr(a_file_name, '.');
		tail += 1;

		std::string str_ext = tail;
		OVR_STR_TO_LOWER(str_ext);

		if (str_ext == "fbx") 
		{
			return OvrImporter::IFT_MODEL;
		}
		else if ("png" == str_ext || "jpeg" == str_ext ||
			"jpg" == str_ext || "tif" == str_ext ||
			"tiff" == str_ext || "bmp" == str_ext ||
			"ico" == str_ext || "gif" == str_ext ||
			"tga" == str_ext)
		{
			return OvrImporter::IFT_TEXTURE;
		}
		else 
		{
			return OvrImporter::IFT_COPY;  // 不名文件全部使用copy
		}

		return OvrImporter::FILE_IMPORT_TYPE::IFT_COPY;
	}

}