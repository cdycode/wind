#ifndef __OVR_PROJECT_OVR_MODEL_IMPORTER_H__
#define __OVR_PROJECT_OVR_MODEL_IMPORTER_H__

#include "ovr_importer.h"
#include <wx_fbx_import/ovr_fbx_import.h>

namespace ovr_project
{
	class OvrModelImporter : public OvrImporter, OvrFbxImporter
	{
	public:
		OvrModelImporter();
		~OvrModelImporter();

		virtual bool		Import(const char* a_file, const char* a_out_dir, std::list<OvrString>& a_out_file_list);
		virtual ovr::uint32 GetPercent();
		virtual void		CancelImport();

	};

}
#endif // __OVR_PROJECT_OVR_MODEL_IMPORTER_H__