#include "../headers.h"
#include "ovr_model_importer.h"
#include <direct.h>
namespace ovr_project
{
	OvrModelImporter::OvrModelImporter()
		:OvrFbxImporter()
		,OvrImporter()
	{
	}


	OvrModelImporter::~OvrModelImporter()
	{
	}

	bool OvrModelImporter::Import(const char * a_file, const char * a_out_dir, std::list<OvrString>& a_out_file_list)
	{
		char buf[80];
		_getcwd(buf, sizeof(buf));
		OvrString s;
		OvrFileTools::GetFileSuffixFromPath("", s);
		return OvrFbxImporter::Import(a_file, a_out_dir, a_out_file_list);
	}

	ovr::uint32 OvrModelImporter::GetPercent()
	{
		u_percent = OvrFbxImporter::GetPercent();
		return u_percent;
	}

	void OvrModelImporter::CancelImport()
	{
		OvrFbxImporter::CancelImport();
	}

}