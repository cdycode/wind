#ifndef __OVR_PROJECT_OVR_COMMON_IMPORTER_H__
#define __OVR_PROJECT_OVR_COMMON_IMPORTER_H__

#include "ovr_importer.h"

namespace ovr_project
{
	class OvrCommonImporter : public OvrImporter
	{
	public:
		OvrCommonImporter();
		~OvrCommonImporter();

		virtual bool		Import(const char* a_file, const char* a_out_dir, std::list<OvrString>& a_out_file_list);
		virtual ovr::uint32 GetPercent();
		virtual void		CancelImport();

	};

}

#endif // __OVR_PROJECT_OVR_COMMON_IMPORTER_H__