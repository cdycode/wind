#include "ovr_import_manager.h"

#include <mutex>
#include <wx_asset_manager/ovr_archive_asset_manager.h>

#include <wx_base/wx_file_tools.h>
#include <wx_project/command/ovr_command_actor.h>
namespace ovr_project
{
	static std::mutex g_importer_mutex;

	OvrImportManager::OvrImportManager()
		: b_canceled(false)
		, u_percent(0)
		, n_total_files(0)
		, n_dealed_file(0)
	{
		//import_mgr = this;
	}


	OvrImportManager::~OvrImportManager()
	{
		//delete import_mgr;
	}
	bool OvrImportManager::IsHasFileExist(OvrImporter::FILE_IMPORT_TYPE a_efit, const char* a_out_dir, const char* a_file, OvrString& a_file_in_folder)
	{
		OvrString output_dir(a_out_dir);
		OvrString file_name;
		OvrFileTools::GetFileNameFromPath(a_file, file_name);
		OvrString extName;
		
		switch (a_efit)
		{
		case ovr_project::OvrImporter::IFT_MODEL:
			extName = ".mesh";
			break;
		case ovr_project::OvrImporter::IFT_COPY:
			break;
		case ovr_project::OvrImporter::IFT_TEXTURE:
			extName = ".tex";
			break;
		default:
			break;
		}
		a_file_in_folder = ovr_asset::OvrArchiveAssetManager::GetIns()->GetRootDir() + output_dir + file_name + extName;
				
		return OvrFileTools::IsFileExist(a_file_in_folder);
	}
	bool OvrImportManager::Import(const std::vector<std::string>& a_asset_files, const char * a_out_dir, std::list<OvrString>& a_out_files, std::function<bool(const char*, bool)> &a_notify_callback)
	{
		n_total_files = a_asset_files.size();
		n_dealed_file = 0;
		for (auto & file : a_asset_files) {
			const char * asset_file = file.c_str();
			OvrImporter::FILE_IMPORT_TYPE efit = OvrImporter::GetFileImportType(asset_file);
			OvrString file_in_folder;
			if (IsHasFileExist(efit, a_out_dir, asset_file, file_in_folder))
			{
				bool bcover = a_notify_callback(file_in_folder.GetStringConst(), true);
				if(!bcover)
					continue;
			}

			//std::lock_guard<std::mutex> guard(g_importer_mutex);
			g_importer_mutex.lock();
			cur_importer = OvrImporter::GetOvrImporter(efit);
			g_importer_mutex.unlock();
			if (!cur_importer) {
				return false;
			}

			std::list<OvrString> asset_list;
			bool bresult = cur_importer->Import(asset_file, a_out_dir, asset_list);
			if (!bresult)
				return false;

			if (b_canceled)
				return false;
			auto it = a_out_files.end();
			a_out_files.splice(it, asset_list);

			n_dealed_file++;
			g_importer_mutex.lock();
			cur_importer = nullptr;		// ��������
			g_importer_mutex.unlock();
		}
		ProcessHotUpdate(a_out_files);
		return true;
	}

	ovr::uint32 OvrImportManager::GetPercent()
	{
		g_importer_mutex.lock();
		int n_cur_importer_percent = cur_importer != nullptr ? cur_importer->GetPercent() : 0;
		g_importer_mutex.unlock();
		double d_percent = 100.*n_dealed_file / n_total_files + 1. / n_total_files * n_cur_importer_percent;

		return (int)d_percent;
	}

	void OvrImportManager::CancelImport()
	{
		b_canceled = true;

		g_importer_mutex.lock();

		if (cur_importer != nullptr)
			cur_importer->CancelImport();

		g_importer_mutex.unlock();
	}
	void OvrImportManager::ProcessHotUpdate(std::list<OvrString> & a_file)
	{
		auto new_command = new OvrHotUpdateImportFile();
		new_command->SetFileList(a_file);
		OvrProject::GetIns()->GetCommandManager()->PushCommand(new_command);
	}

} // namespace ovr_project