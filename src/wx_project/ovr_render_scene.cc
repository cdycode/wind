﻿#include "headers.h"
#include <wx_project/ovr_render_scene.h>
#include "wx_core/ovr_scene_context.h"
#include "wx_project/ovr_scene_event_manager.h"
#include <wx_core/ovr_sequence.h>
#include <wx_project/command/ovr_command_manager.h>
#include <wx_core/ovr_scene_camera.h>
#include <wx_project/ovr_scene_editor.h>
#include <wx_project/ovr_timeline_editor.h>

namespace ovr_project 
{
	OvrRenderScene::OvrRenderScene()
		:render_status(ovr_core::kRenderPause)
	{}
	OvrRenderScene::~OvrRenderScene() 
	{
		Uninit();
	}

	bool OvrRenderScene::Init(IOvrRenderContext* a_context, IPlayerNotify* a_notify)
	{
		Uninit();
		if (nullptr == a_context)
		{
			return false;
		}

		command_manager = new OvrCommandManager;
		if (nullptr == command_manager)
		{
			return false;
		}

		current_scene = new OvrSceneEditor;
		current_sequence = new OvrTimelineEditor;

		render_context = a_context;
		player_notify = a_notify;
		is_working = true;
		work_thread = new std::thread(&OvrRenderScene::WorkThread, this);

		if (nullptr == work_thread)
		{
			return false;
		}

		return true;
	}

	void OvrRenderScene::Uninit() 
	{
		is_working = false;
		if (nullptr != work_thread)
		{
			work_thread->join();
			delete work_thread;
			work_thread = nullptr;
		}
		render_context = nullptr;

		if (nullptr != command_manager)
		{
			delete command_manager;
			command_manager = nullptr;
		}
		if (nullptr != current_scene)
		{
			delete current_scene;
			current_scene = nullptr;
		}
		if (nullptr != current_sequence)
		{
			delete current_sequence;
			current_sequence = nullptr;
		}
	}

	void OvrRenderScene::OpenScene(const OvrString& a_scene_file)
	{
		if (nullptr != current_scene && a_scene_file == current_scene->GetFilePath())
		{
			return;
		}

		new_scene_file_path = a_scene_file;
		is_open_new_scene = true;
	}

	void OvrRenderScene::CloseScene() 
	{
		new_scene_file_path = "";

		is_open_new_scene = true;
	}

	void OvrRenderScene::OpenSequence(const OvrString& a_sequence_file)
	{
		if (nullptr != current_sequence && a_sequence_file == current_sequence->GetFilePath())
		{
			return ;
		}
		new_sequence_file_path = a_sequence_file;
		is_open_new_sequence = true;
	}

	void OvrRenderScene::CloseSequence() 
	{
		new_sequence_file_path = "";
		is_open_new_sequence = true;
	}

	void OvrRenderScene::Reshape(ovr::int32 a_new_width, ovr::int32 a_new_height) 
	{
		render_width = a_new_width;
		render_height = a_new_height;
		is_size_change = true;
	}

	void OvrRenderScene::Play()
	{
		render_status.SetStatus(ovr_core::kRenderPlay);
	}

	void OvrRenderScene::Pause()
	{
		render_status.SetStatus(ovr_core::kRenderPause);
	}

	void OvrRenderScene::Seek(ovr::uint64 a_seek_frame_index)
	{
		seek_frame_index = a_seek_frame_index;
		render_status.SetStatus(ovr_core::kRenderSeek);
	}

	void OvrRenderScene::SetRecord(bool a_is_record) 
	{ 
		if (nullptr != current_sequence)
		{
			current_sequence->SetRecord(a_is_record);
		}
	}
	bool OvrRenderScene::IsRecord()const 
	{ 
		if (nullptr != current_sequence)
		{
			return current_sequence->IsRecord();
		}
		return false;
	}

	ovr::uint64	OvrRenderScene::GetCurrentFrame()const 
	{ 
		if (nullptr != current_sequence)
		{
			current_sequence->GetCurrentFrame();
		}
		return 0; 
	}

	void OvrRenderScene::WorkThread() 
	{
		render_context->OnInit();
		Reshape(render_context->GetWidth(), render_context->GetHeight());

		ovr_core::OvrCore::GetIns()->Open();

		next_frame_time = OvrTime::GetSeconds();

		while (is_working)
		{
			
			SyncFrameTime(last_frame_time, current_time);

			command_manager->Update();

			DoStatusChanged();

			if (nullptr != current_scene)
			{
				//Sequence进行定时更新  帧率稍低
				if (IsUpdateSequence())
				{
					ovr_core::OvrCore::GetIns()->Update();

					if (nullptr != current_sequence)
					{
						//播放位置通知
						if (nullptr != player_notify)
						{
							player_notify->OnPlayPosition(current_sequence->GetCurrentFrame());
						}

						//update后  会递加1
						current_sequence->Update();						
					}
				}
				
				//场景帧率要高
				current_scene->Update();
				current_scene->Draw();
			}
			else 
			{
				DoModeEmpty();
			}

			render_context->OnRender();

			last_frame_time = current_time;
		}

		DoCloseSequence();
		DoCloseScene();
		
		ovr_core::OvrCore::Uninit();
		
		render_context->OnUninit();
	}
	void OvrRenderScene::DoStatusChanged() 
	{
		if (is_open_new_scene)
		{
			DoOpenScene();

			is_open_new_scene = false;
		}
		if (is_open_new_sequence)
		{
			DoOpenSequence();
			is_open_new_sequence = false;
		}

		//需要生效的操作 这些操作必须在OpenGL线程中调用
		if (is_size_change && nullptr != current_scene)
		{
			current_scene->GetSceneContext()->GetSceneCamera()->Reshape(render_width, render_height);
			current_scene->GetEventManager()->Reshape(render_width, render_height);
			is_size_change = false;
		}

		if (!render_status.IsChanged() || nullptr == current_sequence)
		{
			return;
		}

		ovr_core::OvrSequence* sequence_core = current_sequence->GetSequence();

		render_status.UpdateStatus();

		if (render_status == ovr_core::kRenderPause)
		{
			sequence_core->Pause();
		}
		else
		{
			if (render_status == ovr_core::kRenderPlay)
			{
				current_scene->SetMode(OvrSceneEditor::kPreview);
				sequence_core->Play();
			}
			else
			{
				sequence_core->Seek(seek_frame_index);
				//进行Seek状态通知
				if (nullptr != player_notify)
				{
					player_notify->OnPlayPosition(seek_frame_index);
				}
			}
		}

		if (nullptr != player_notify)
		{
			player_notify->OnPlayStatus(render_status.GetStatus());
		}
	}
	void OvrRenderScene::DoModeEmpty() 
	{
		//先空着
	}

	void OvrRenderScene::SyncFrameTime(double a_last_frame_time, double& a_current_time)
	{
		a_current_time = OvrTime::GetSeconds();

		//本帧渲染时间 与 当前时间的差
		double diff_time = (a_last_frame_time + 1.0 / current_fps) - a_current_time;
		if (diff_time > 0)
		{
			//临时使用此函数  后面将换掉
			OvrTime::SleepMs((ovr::uint32)(diff_time * 1000));

			//更新当前时间
			a_current_time = OvrTime::GetSeconds();
		}
	}

	void OvrRenderScene::DoOpenScene() 
	{
		DoCloseScene();

		//从文件中加载场景内容
		current_scene->Open(new_scene_file_path);
		if (nullptr != current_sequence)
		{
			//设置当前的场景
			current_sequence->SetScene(current_scene);
		}
		player_notify->OnOpenScene(current_scene->GetSceneContext());
	}
	void OvrRenderScene::DoCloseScene() 
	{
		if (current_scene->IsOpen())
		{
			current_scene->Close();
		}
	}

	void OvrRenderScene::DoOpenSequence() 
	{
		DoCloseSequence();

		if (current_sequence->Open(new_sequence_file_path)) 
		{
			current_sequence->SetScene(current_scene);
			sequence_fps = current_sequence->GetSequence()->GetFPS();
			player_notify->OnOpenSequence(current_sequence->GetSequence());
		}		
	}
	void OvrRenderScene::DoCloseSequence() 
	{
		if (nullptr != current_sequence)
		{
			current_sequence->Close();
		}
		sequence_fps = editor_fps;
	}
	bool OvrRenderScene::IsUpdateSequence() 
	{
		double new_time = OvrTime::GetSeconds();

		if (new_time >= next_frame_time)
		{
			next_frame_time += 1.0 / sequence_fps;
			return true;
		}
		return false;
	}
}

