﻿#include "headers.h"
#include "ovr_indicator.h"
#include <wx_engine/ovr_gpu_program.h>
#include <wx_engine/ovr_gpu_program_manager.h>
#include <wx_engine/ovr_material.h>
#include <wx_engine/ovr_material_manager.h>
#include <wx_engine/ovr_actor.h>
#include <wx_engine/ovr_static_mesh_component.h>
#include <wx_engine/ovr_camera_component.h>
#include <wx_project/ovr_project.h>
#include <wx_project/ovr_scene_editor.h>
#include <wx_project/ovr_scene_event_manager.h>
#include <wx_core/ovr_scene_context.h>
#include <wx_core/ovr_scene_camera.h>

namespace ovr_project 
{
	bool OvrIndicator::Init(OvrSceneEditor* a_scene_editor)
	{
		scene_editor = a_scene_editor;

		ovr_engine::OvrActor* temp_actor = nullptr;
		ovr_engine::OvrMesh* mesh = nullptr;
		ovr_engine::OvrGpuProgram* program = ovr_engine::OvrGpuProgramManager::GetIns()->GetProgram("SimpleRender");

		material[0] = ovr_engine::OvrMaterialManager::GetIns()->Create("<indicator_green>");
		material[0]->SetProgram(program);
		material[0]->GetProgramParameters()->SetNameConstant("UniformColor", OvrVector4(0.0, 1.0, 0.0, 1.0));
		material[0]->SetDepthCheckEnable(false);

		material[1] = ovr_engine::OvrMaterialManager::GetIns()->Create("<indicator_red>");
		material[1]->SetProgram(program);
		material[1]->GetProgramParameters()->SetNameConstant("UniformColor", OvrVector4(1.0, 0.0, 0.0, 1.0));
		material[1]->SetDepthCheckEnable(false);

		material[2] = ovr_engine::OvrMaterialManager::GetIns()->Create("<indicator_blue>");
		material[2]->SetProgram(program);
		material[2]->GetProgramParameters()->SetNameConstant("UniformColor", OvrVector4(0.0, 0.0, 1.0, 1.0));
		material[2]->SetDepthCheckEnable(false);

		material[3] = ovr_engine::OvrMaterialManager::GetIns()->Create("<indicator_yellow>");
		material[3]->SetProgram(program);
		material[3]->GetProgramParameters()->SetNameConstant("UniformColor", OvrVector4(1.0, 1.0, 0.0, 1.0));
		material[3]->SetDepthCheckEnable(false);

		material[4] = ovr_engine::OvrMaterialManager::GetIns()->Create("<indicator_white>");
		material[4]->SetProgram(program);
		material[4]->GetProgramParameters()->SetNameConstant("UniformColor", OvrVector4(1.0, 1.0, 1.0, 1.0));
		material[4]->SetDepthCheckEnable(false);

		mesh = ovr_engine::OvrMeshManager::GetIns()->Load("system/content/indicator/rotate.mesh");
		//Y
		rotate_actor = CreateChildActor();
		rotate_actor->SetDisplayName("rotation axis");
		temp_actor = rotate_actor->CreateChildActor();
		temp_actor->SetDisplayName("rotation roll");
		temp_actor->Roll(-3.1415926f / 2);
		mesh_component[0] = temp_actor->AddComponent<ovr_engine::OvrStaticMeshComponent>();
		mesh_component[0]->SetMesh(mesh);
		mesh_component[0]->SetMaterial(0, material[0]);

		//X
		temp_actor = rotate_actor->CreateChildActor();
		temp_actor->SetDisplayName("rotation picth");
		mesh_component[1] = temp_actor->AddComponent<ovr_engine::OvrStaticMeshComponent>();
		mesh_component[1]->SetMesh(mesh);
		mesh_component[1]->SetMaterial(0, material[1]);

		//Z
		temp_actor = rotate_actor->CreateChildActor();
		temp_actor->SetDisplayName("rotation yaw");
		temp_actor->Yaw(3.1415926f / 2);
		mesh_component[2] = temp_actor->AddComponent<ovr_engine::OvrStaticMeshComponent>();
		mesh_component[2]->SetMesh(mesh);
		mesh_component[2]->SetMaterial(0, material[2]);

		//////////////////////////////////////////////////////////
		mesh = ovr_engine::OvrMeshManager::GetIns()->Load("system/content/indicator/move.mesh");
		//Y
		transform_actor = CreateChildActor();
		transform_actor->SetDisplayName("transform axis");
		mesh_component[3] = transform_actor->AddComponent<ovr_engine::OvrStaticMeshComponent>();
		mesh_component[3]->SetMesh(mesh);
		mesh_component[3]->SetMaterial(0, material[0]);

		//X
		temp_actor = transform_actor->CreateChildActor();
		temp_actor->SetDisplayName("transform x");
		temp_actor->Roll(-3.1415926f / 2);
		mesh_component[4] = temp_actor->AddComponent<ovr_engine::OvrStaticMeshComponent>();
		mesh_component[4]->SetMesh(mesh);
		mesh_component[4]->SetMaterial(0, material[1]);

		//Z
		temp_actor = transform_actor->CreateChildActor();
		temp_actor->SetDisplayName("transform z");
		temp_actor->Pitch(3.1415926f / 2);
		mesh_component[5] = temp_actor->AddComponent<ovr_engine::OvrStaticMeshComponent>();
		mesh_component[5]->SetMesh(mesh);
		mesh_component[5]->SetMaterial(0, material[2]);

		////////////////////////////////////////////////////////
		mesh = ovr_engine::OvrMeshManager::GetIns()->Load("system/content/indicator/scale.mesh");
		//Y
		scale_actor = CreateChildActor();
		scale_actor->SetDisplayName("scale axis");
		mesh_component[6] = scale_actor->AddComponent<ovr_engine::OvrStaticMeshComponent>();
		mesh_component[6]->SetMesh(mesh);
		mesh_component[6]->SetMaterial(0, material[0]);

		//X
		temp_actor = scale_actor->CreateChildActor();
		temp_actor->SetDisplayName("scale x");
		temp_actor->Roll(-3.1415926f / 2);
		mesh_component[7] = temp_actor->AddComponent<ovr_engine::OvrStaticMeshComponent>();
		mesh_component[7]->SetMesh(mesh);
		mesh_component[7]->SetMaterial(0, material[1]);

		//Z
		temp_actor = scale_actor->CreateChildActor();
		temp_actor->SetDisplayName("scale z");
		temp_actor->Pitch(3.1415926f / 2);
		mesh_component[8] = temp_actor->AddComponent<ovr_engine::OvrStaticMeshComponent>();
		mesh_component[8]->SetMesh(mesh);
		mesh_component[8]->SetMaterial(0, material[2]);
		

		for (int index = 0; index < 9 ; index++)
		{
			mesh_component[index]->SetCastShadow(false);
			mesh_component[index]->SetCollisionEnabled(false);
			mesh_component[index]->SetRenderQueueGroupPriority(OVR_RENDERABLE_DEFAULT_PRIORITY + 100);
		}

		//////////////////////////////////////////////
		SetVisible(true);
		rotate_actor->SetVisible(false);
		scale_actor->SetVisible(false);
		transform_actor->SetVisible(true);
		current_axis_type = kAxisTransform;
		return true;
	}

	void OvrIndicator::Uninit() 
	{
	}

	void OvrIndicator::SetAxisType(OvrAxisType a_new_type)
	{
		if (current_axis_type == a_new_type)
		{
			return;
		}
		current_axis_type = a_new_type;
		switch (a_new_type)
		{
		case kAxisTransform:
		{
			rotate_actor->SetVisible(false);
			scale_actor->SetVisible(false);
			transform_actor->SetVisible(true);
		}
		break;
		case kAxisRotation:
		{
			rotate_actor->SetVisible(true);
			scale_actor->SetVisible(false);
			transform_actor->SetVisible(false);
		}
		break;
		case kAxisScale:
		{
			rotate_actor->SetVisible(false);
			scale_actor->SetVisible(true);
			transform_actor->SetVisible(false);
		}
		break;
		}

		SetAxisFixedSize();
	}

	void OvrIndicator::Update()
	{
		//只有处于编辑状态、当前有选中的actor
		if (scene_editor->GetMode() != OvrSceneEditor::kEditor || nullptr == scene_editor->GetSelector())
		{
			return;
		}

		//状态统一
		OvrSceneEventManager* event_manager = scene_editor->GetEventManager();
		if (is_pressed != event_manager->IsPressed())
		{
			if (event_manager->IsPressed())
			{
				pressed_x = event_manager->GetMousePosX();
				pressed_y = event_manager->GetMousePosY();

				BeginPressed(scene_editor->GetSelector());
			}
			is_pressed = event_manager->IsPressed();
		}

		if (is_pressed)
		{
			//根据用户操作进行坐标变换
			UpdateSelectedActor(event_manager->GetMousePosX(), event_manager->GetMousePosY(), scene_editor->GetSelector());
		}
		else 
		{
			//选择坐标轴
			OvrRay3f pick_ray;
			scene_editor->GetCurrentPickRay(pick_ray);
			SetAxisName(FindPickAxisName(pick_ray));
		}
		
		SetAxisFixedSize();
	}

	bool OvrIndicator::IsIncludeActor(ovr_engine::OvrActor* a_actor)
	{
		return this == a_actor || IsChild(this, a_actor);
	}

	void OvrIndicator::BeginPressed(ovr_engine::OvrActor* a_selected_actor)
	{
		OvrRay3f pick_ray;
		scene_editor->GetCurrentPickRay(pick_ray);

		world_transform = OvrTransform();
		world_transform_matrix = OvrMatrix4();
		OvrActor* parent = a_selected_actor->GetParent();
		if (parent)
		{
			world_transform_matrix = parent->GetWorldTransform();
			world_transform_matrix.decompose(&world_transform.scale, &world_transform.quaternion, &world_transform.position);
		}

		default_transform = a_selected_actor->GetTransform();
		indicator_pos = GetPosition();
		indicator_quat = GetQuaternion();

		GeneratePane();

		float distance = 0;
		if (help_plane.Intersect(distance, pick_ray.GetOrigin(), pick_ray.GetDir()))
		{
			start_pos = pick_ray.GetOrigin() + pick_ray.GetDir()*distance;
		}

		//计算从世界空间到被选中Actor的父空间变化
		world_transform_matrix.get_inverse(world_transform_matrix_inverse);
	}

	void OvrIndicator::GeneratePane() 
	{
		ovr_engine::OvrActor* camera_actor = scene_editor->GetSceneContext()->GetSceneCamera()->GetCurrentCamera();

		const OvrVector3& look_dir = camera_actor->GetWorldQuaternion()*(-OvrVector3::UNIT_Z);
		if (kAxisNameX == current_axis_name)
		{
			//help_plane
			float value1 = look_dir * OvrVector3::UNIT_Y;
			float value2 = look_dir * OvrVector3::UNIT_Z;
			if (fabs(value1) > fabs(value2))
			{
				help_plane.SetNormal(OvrVector3::UNIT_Y);
			}
			else
			{
				help_plane.SetNormal(OvrVector3::UNIT_Z);
			}
		}
		else if (kAxisNameY == current_axis_name)
		{
			float value1 = look_dir * OvrVector3::UNIT_X;
			float value2 = look_dir * OvrVector3::UNIT_Z;
			if (fabs(value1) > fabs(value2))
			{
				help_plane.SetNormal(OvrVector3::UNIT_X);
			}
			else
			{
				help_plane.SetNormal(OvrVector3::UNIT_Z);
			}
		}
		else if (kAxisNameZ == current_axis_name)
		{
			float value1 = look_dir * OvrVector3::UNIT_X;
			float value2 = look_dir * OvrVector3::UNIT_Y;
			if (fabs(value1) > fabs(value2))
			{
				help_plane.SetNormal(OvrVector3::UNIT_X);
			}
			else
			{
				help_plane.SetNormal(OvrVector3::UNIT_Y);
			}
		}
		float length = help_plane.GetNormal() * world_transform_matrix.transform_point(default_transform.position);
		help_plane.SetDistanceToOrigin(length);
	}

	void OvrIndicator::UpdateSelectedActor(float a_mouse_x, float a_mouse_y, ovr_engine::OvrActor* a_selected_actor)
	{
		//需要判断是否有轴被选中
		if (current_axis_name < kAxisNameY || current_axis_name > kAxisNameZ)
		{
			return;
		}

		OvrRay3f pick_ray;
		scene_editor->GetCurrentPickRay(pick_ray);

		float distance = 0;
		if (!help_plane.Intersect(distance, pick_ray.GetOrigin(), pick_ray.GetDir()))
		{
			return;
		}

		OvrVector3 current_pos = (pick_ray.GetOrigin() + pick_ray.GetDir()*distance) - start_pos;

		if (kAxisTransform == current_axis_type)
		{
			OvrVector3 move_pos;
			if (kAxisNameX == current_axis_name)
			{
				move_pos = OvrVector3(current_pos[0], 0, 0);
			}
			else if (kAxisNameY == current_axis_name)
			{
				move_pos = OvrVector3(0, current_pos[1], 0);
			}
			else if (kAxisNameZ == current_axis_name)
			{
				move_pos = OvrVector3(0, 0, current_pos[2]);
			}
			//设置坐标系本身的位置
			SetPosition(indicator_pos + move_pos);

			//设置actor位置
			OvrVector3 pos = world_transform_matrix_inverse.transform_vector(move_pos);
			a_selected_actor->SetPosition(default_transform.position + pos);
		}
		else if (kAxisScale == current_axis_type)
		{
			if (kAxisNameX == current_axis_name)
			{
				a_selected_actor->SetScale(default_transform.scale + OvrVector3(current_pos[0], 0.0f, 0.0f));
			}
			else if (kAxisNameY == current_axis_name)
			{
				a_selected_actor->SetScale(default_transform.scale + OvrVector3(0.0f, current_pos[1], 0.0f));
			}
			else if (kAxisNameZ == current_axis_name)
			{
				a_selected_actor->SetScale(default_transform.scale + OvrVector3(0.0f, 0.0f, current_pos[2]));
			}
		}
		else
		{
			OvrQuaternion new_quaternion_local;
			if (kAxisNameX == current_axis_name)
			{
				float move_y = a_mouse_y - pressed_y;
				new_quaternion_local = OvrQuaternion::make_euler_quat(-move_y, 0.0f, 0.0f) * default_transform.quaternion;
			}
			else if (kAxisNameY == current_axis_name)
			{
				float move_x = a_mouse_x - pressed_x;
				new_quaternion_local = OvrQuaternion::make_euler_quat(0.0f, move_x, 0.0f) * default_transform.quaternion;
			}
			else if (kAxisNameZ == current_axis_name)
			{
				float move_y = a_mouse_y - pressed_y;
				new_quaternion_local = OvrQuaternion::make_euler_quat(0.0f, 0.0f, move_y) * default_transform.quaternion;	
			}
			OvrQuaternion new_quaternion_world = new_quaternion_local * world_transform.quaternion;
			SetQuaternion(new_quaternion_world);
			a_selected_actor->SetQuaternion(new_quaternion_local);
		}
	}

	OvrIndicator::AxisName OvrIndicator::FindPickAxisName(const OvrRay3f& a_ray)
	{
		if (current_axis_type < kAxisRotation || current_axis_type > kAxisScale)
		{
			return kAxisNameNone;
		}

		OvrIndicator::AxisName result = kAxisNameNone;

		float min_dis = 100000.0f;
		for (ovr::uint32 i = 0; i < 3; i++)
		{
			float dis;
			if (mesh_component[current_axis_type *3 + i]->Raycast(a_ray, dis))
			{
				//选中
				if (dis < min_dis)
				{
					result = (OvrIndicator::AxisName)(i + 1);
					min_dis = dis;
				}
			}
		}
		return result;
	}
	void OvrIndicator::SetAxisName(AxisName a_axis_name) 
	{
		if (current_axis_name == a_axis_name)
		{
			return;
		}
		
		//复原
		if (current_axis_name > kAxisNameNone && current_axis_name <= kAxisNameZ)
		{
			mesh_component[current_axis_type * 3 + current_axis_name - 1]->SetMaterial(0, material[current_axis_name - 1]);
		}

		//设置为新的
		if (a_axis_name > kAxisNameNone && a_axis_name <= kAxisNameZ)
		{
			mesh_component[current_axis_type * 3 + a_axis_name - 1]->SetMaterial(0, material[3]);
		}
		current_axis_name = a_axis_name;
	}

	void OvrIndicator::SetAxisFixedSize() 
	{
		ovr_engine::OvrCameraComponent* component = scene_editor->GetSceneContext()->GetSceneCamera()->GetCurrentCameraComponent();

		float dist = (component->GetCameraPos() - GetWorldPosition()).length();
		OvrVector3 scale = OvrVector3(0.3f* dist, 0.3f* dist, 0.3f*dist);
		SetScale(scale);
	}

	bool OvrIndicator::IsChild(ovr_engine::OvrActor* a_host_actor, ovr_engine::OvrActor* a_actor) 
	{
		ovr::uint32 child_count = a_host_actor->GetChildCount();
		for (ovr::uint32 i = 0; i < child_count; i++)
		{
			if (a_host_actor->GetChild(i) == a_actor)
			{
				//跟自己相等
				return true;
			}
			if (IsChild(a_host_actor->GetChild(i), a_actor))
			{
				//是它的子
				return true;
			}
		}
		return false;
	}
}
