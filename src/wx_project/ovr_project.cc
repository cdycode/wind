﻿#include "headers.h"

#include <memory>

#include "wx_project/ovr_project.h"
#include "wx_project/ovr_asset_manager.h"
#include <wx_core/ovr_scene_context.h>
#include <wx_core/ovr_sequence.h>
#include <wx_project/ovr_render_scene.h>
#include <wx_project/ovr_scene_editor.h>
#include <wx_project/ovr_timeline_editor.h>
#include <wx_plugin/wx_i_archive_project.h>

namespace ovr_project 
{
	static std::unique_ptr<OvrProject> sg_project = nullptr;
	OvrProject* OvrProject::GetIns()
	{
		if (nullptr == sg_project)
		{
			sg_project.reset(new OvrProject());
		}
		return sg_project.get();
	}

	void OvrProject::DelIns()
	{
		if (nullptr != sg_project)
			sg_project.reset(nullptr);
	}


	OvrProject::OvrProject()
	{
	}
	OvrProject::~OvrProject()
	{
		Close();
	}

	bool OvrProject::Init() 
	{
		ovr_core::OvrCore::Init();
		return true;
	}

	void OvrProject::Uninit() 
	{
		ovr_core::OvrCore::Uninit();
	}

	bool OvrProject::Create(const char* a_project_file_path, IOvrRenderContext* a_context, IPlayerNotify* a_notify)
	{
		//检测文件是否存在
		if (_access(a_project_file_path, 0) == 0)
		{
			return false;
		}

		if (OvrAssetManager::CreateProject(a_project_file_path))
		{
			bool result = Open(a_project_file_path, a_context, a_notify);
			if (result)
				need_saveed = true;
			return result;
		}
		return false;
	}

	bool OvrProject::Open(const char* a_project_file_path, IOvrRenderContext* a_context, IPlayerNotify* a_notify)
	{
		if (absolute_file_path == a_project_file_path)
		{
			return true;
		}

		//检测文件是否存在
		if (_access(a_project_file_path, 0) != 0)
		{
			return false;
		}

		Close();

		asset_manager = new OvrAssetManager;
		render_thread = new OvrRenderScene;
		if (nullptr == asset_manager || nullptr == render_thread)
		{
			return false;
		}

		//获取工作目录
		OvrString work_dir;
		if (!OvrFileTools::GetPathFromFullPath(a_project_file_path, work_dir))
		{
			return false;
		}

		//初始化core
		ovr_core::OvrCore::GetIns()->SetWorkDir(work_dir);

		if (!asset_manager->Open(a_project_file_path) || !render_thread->Init(a_context, a_notify))
		{
			return false;
		}
		absolute_file_path = a_project_file_path;
		OvrFileTools::GetFileAllNameFromPath(a_project_file_path, file_path);

		return wx_plugin::wxIArchiveProject::Load()->LoadProject(this, file_path);
	}

	void OvrProject::Close()
	{
		if (nullptr != render_thread)
		{
			render_thread->Uninit();
			delete render_thread;
			render_thread = nullptr;
		}

		if (nullptr != asset_manager)
		{
			asset_manager->Close();
			delete asset_manager;
			asset_manager = nullptr;
		}
	}

	OvrAssetManager* OvrProject::GetAssetManager()
	{
		return asset_manager;
	}

	const OvrString& OvrProject::GetWorkDir()const
	{
		if (nullptr == asset_manager) {
			static const OvrString nulldir = OvrString();
			return nulldir;
		}
		return asset_manager->GetWorkDir();
	}

	void OvrProject::ResizeRenderWindow(int a_new_width, int a_new_height)
	{
		if (nullptr != render_thread)
		{
			render_thread->Reshape(a_new_width, a_new_height);
		}
	}

	bool OvrProject::Save()
	{
		SaveAll();
		return true;
	}

	bool OvrProject::SaveAll()
	{
		if (!wx_plugin::wxIArchiveProject::Load()->SaveProject(this, file_path))
			return false;

		OvrSceneEditor* temp_scene = render_thread->GetCurrentScene();
		if (nullptr != temp_scene)
		{
			if (!wx_plugin::wxIArchiveProject::Load()->SaveScene(temp_scene))
				return false;
		}
		OvrTimelineEditor* temp_sequence = render_thread->GetCurrentSequence();
		if (nullptr != temp_sequence)
		{
			if (!wx_plugin::wxIArchiveProject::Load()->SaveSequence(temp_sequence))
				return false;
		}

		need_saveed = false;
		return true;
	}

	void OvrProject::OpenSence(const char* a_unique_name)
	{
		OvrString file_path;
		if (asset_manager->GetSceneFilePath(a_unique_name, file_path))
		{
			OpenSceneFromFile(file_path.GetStringConst());
		}
	}

	void OvrProject::OpenSceneFromFile(const char* a_file_path)
	{
		render_thread->OpenScene(a_file_path);
	}

	OvrSceneEditor*	OvrProject::GetCurrentSceneEditor()
	{
		return render_thread->GetCurrentScene();
	}

	ovr_core::OvrSceneContext* OvrProject::GetCurrentScene()
	{
		return render_thread->GetCurrentScene()->GetSceneContext();
	}

	void OvrProject::CloseSence()
	{
		render_thread->CloseScene();
	}

	void OvrProject::OpenSequence(const char* a_unique_name)
	{
		OvrString file_path;
		if (asset_manager->GetSequenceFilePath(a_unique_name, file_path))
		{
			OpenSequenceFromFile(file_path.GetStringConst());
		}
	}

	void OvrProject::OpenSequenceFromFile(const char* a_file_path)
	{
		render_thread->OpenSequence(a_file_path);
	}

	OvrTimelineEditor* OvrProject::GetCurrentTimelineEditor() 
	{
		return render_thread->GetCurrentSequence();
	}

	ovr_core::OvrSequence* OvrProject::GetCurrentSequence()
	{
		if (nullptr != render_thread->GetCurrentSequence())
		{
			return render_thread->GetCurrentSequence()->GetSequence();
		}
		return nullptr;
	}
	void OvrProject::CloseSequence()
	{
		render_thread->CloseSequence();
	}

	OvrCommandManager*	OvrProject::GetCommandManager()
	{
		return render_thread->GetCommandManager();
	}
}

