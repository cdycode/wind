﻿#include "../headers.h"
#include <wx_project/command/ovr_command_actor.h>
#include <wx_core/ovr_scene_context.h>
#include <wx_core/ovr_scene_camera.h>
#include <wx_project/ovr_project.h>
#include <wx_project/ovr_scene_editor.h>
#include <wx_engine/ovr_resource.h>
#include <wx_engine/ovr_actor.h>
#include <wx_engine/ovr_mesh_component.h>
#include <wx_engine/ovr_text_component.h>
#include <wx_engine/ovr_camera_component.h>
#include <wx_engine/ovr_texture_manager.h>
#include <wx_engine/ovr_material_manager.h>
#include <wx_engine/ovr_mesh_manager.h>
#include <wx_engine/ovr_skeleton_manager.h>
#include <wx_core/component/ovr_audio_component.h>

namespace ovr_project 
{
	void OvrCommandActor::GetPlacePosition(OvrVector3& a_position)
	{
		OvrRay3f ray = ovr_project::OvrProject::GetIns()->GetCurrentScene()->GetSceneCamera()->GeneratePickRay(0.5200f, 0.5200f);
		a_position = ray.GetOrigin() + ray.GetDir() * 2;
	}

	bool OvrCommandLoadActor::Do() 
	{
		ovr_engine::OvrActor* new_actor = ovr_project::OvrProject::GetIns()->GetCurrentScene()->CreateActor(mesh_file_path);
		if (is_forward)
		{
			OvrVector3 new_position;
			GetPlacePosition(new_position);
			new_actor->SetPosition(new_position);
		}
		if (display_name.GetStringSize() > 0)
		{
			new_actor->SetDisplayName(display_name.GetStringConst());
		}

		OvrSceneListener* listener = OvrProject::GetIns()->GetListener();
		if (nullptr != listener)
		{
			listener->OnAddActor(new_actor->GetUniqueName());
		}
		return true;
	}

	bool OvrCommandCreateActor::Do() 
	{
		ovr_engine::OvrActor* new_actor = ovr_project::OvrProject::GetIns()->GetCurrentScene()->CreateActor();
		if (nullptr == new_actor)
		{
			return false;
		}
		if (is_forward)
		{
			OvrVector3 new_position;
			GetPlacePosition(new_position);
			new_actor->SetPosition(new_position);
		}
		if (display_name.GetStringSize() > 0)
		{
			new_actor->SetDisplayName(display_name.GetStringConst());
		}
		switch (actor_type)
		{
		case kInvalid:
			assert(0);
			break;
		case k3DAudio:
		{
			ovr_core::OvrAudioComponent* audio_component = new_actor->AddComponent<ovr_core::OvrAudioComponent>();
		}
			break;
		case kEmptyActor:
			//不添加任何组件
			break;
		case kText: 
		{
			ovr_engine::OvrTextComponent* text_component = new_actor->AddComponent<ovr_engine::OvrTextComponent>();
			text_component->SetText("text");
		}
			break;
		case kCameraActor:
			new_actor->AddComponent<ovr_engine::OvrCameraComponent>();
			break;
		case kParticle:
			assert(0);
			//new_actor->AddComponent<ovr_engine::OvrParticleComponent>();
			break;
		default:
			break;
		}
		OvrSceneListener* listener = OvrProject::GetIns()->GetListener();
		if (nullptr != listener)
		{
			listener->OnAddActor(new_actor->GetUniqueName());
		}
		return true;
	}

	bool OvrCreateVideoActor::Do()
	{
		ovr_engine::OvrActor* new_actor = ovr_project::OvrProject::GetIns()->GetCurrentScene()->CreateActor();
		if (is_forward)
		{
			OvrVector3 new_position;
			GetPlacePosition(new_position);
			new_actor->SetPosition(new_position);
		}
		if (display_name.GetStringSize() > 0)
		{
			new_actor->SetDisplayName(display_name.GetStringConst());
		}

		ovr_engine::OvrVideoComponent* video_component = new_actor->AddComponent<ovr_engine::OvrVideoComponent>();
		if (nullptr != video_component)
		{
			video_component->SetVideoType(video_type);
		}

		OvrSceneListener* listener = OvrProject::GetIns()->GetListener();
		if (nullptr != listener)
		{
			listener->OnAddActor(new_actor->GetUniqueName());
		}
		return true;
	}

	bool OvrCommandCreateLight::Do() 
	{
		ovr_engine::OvrActor* new_actor = ovr_project::OvrProject::GetIns()->GetCurrentScene()->CreateActor();
		if (is_forward)
		{
			OvrVector3 new_position;
			GetPlacePosition(new_position);
			new_actor->SetPosition(new_position);
		}
		if (display_name.GetStringSize() > 0)
		{
			new_actor->SetDisplayName(display_name.GetStringConst());
		}

		ovr_engine::OvrLightComponent* light_component = new_actor->AddComponent<ovr_engine::OvrLightComponent>();

		light_component->SetLightType(light_type);
		light_component->EnableCastShadow(true);

		light_component->SetDiffuseColor(OvrVector4(1.0f, 1.0f, 1.0f, 1.0f));
		light_component->SetRange(20.0f);
		light_component->SetIntensity(1.0f);

		OvrSceneListener* listener = OvrProject::GetIns()->GetListener();
		if (nullptr != listener)
		{
			listener->OnAddActor(new_actor->GetUniqueName());
		}
		return true;
	}

	bool OvrRemoveActor::Do()
	{
		OvrProject::GetIns()->GetCurrentSceneEditor()->RemoveActor(actor_unique_name);
		return true;
	}

	bool OvrCopyActor::Do()
	{
		ovr_project::OvrProject* project = ovr_project::OvrProject::GetIns();
		ovr_core::OvrSceneContext* scene_context = project->GetCurrentScene();
		scene_context->CopyActor(actor_unique_name);

		return true;
	}

	bool OvrRelationChangedActor::Do()
	{
		std::function<ovr_engine::OvrActor *(const OvrString &)> actor_id_to_ptr =
			[](const OvrString & str_id)->ovr_engine::OvrActor * {
			ovr_engine::OvrActor * actor = nullptr;

			ovr_project::OvrProject* prj = ovr_project::OvrProject::GetIns();
			ovr_core::OvrSceneContext * scene_ctx = prj->GetCurrentScene();


			if (str_id.GetStringSize() != 0)
			{
				actor = scene_ctx->GetActorByUniqueName(str_id);
			}
			else
			{
				ovr_engine::OvrScene* scene = scene_ctx->GetScene();
				actor = scene->GetRoot();
			}

			return actor;
		};

		auto parent_actor = actor_id_to_ptr(parent_actor_unique_name);
		auto child_actor = actor_id_to_ptr(child_actor_unique_name);

		if (parent_actor != child_actor)
			parent_actor->AddChild(child_actor, true);

		return true;
	}
	bool OvrHotUpdateImportFile::Do()
	{
		std::list<OvrString>::iterator it = file_to_update.begin();
		while (it != file_to_update.end())
		{
			OvrString stufi;
			OvrFileTools::GetFileSuffixFromPath(*it, stufi);
			ovr_engine::OvrResource* p = nullptr;
			if(stufi == OvrString("tex"))
			{
				p = ovr_engine::OvrTextureManager::GetIns()->GetByName(*it); 
			}
			else if (stufi == OvrString("mesh"))
			{
				p = ovr_engine::OvrMeshManager::GetIns()->GetByName(*it);
			}
			else if (stufi == OvrString("mat"))
			{
				p = ovr_engine::OvrMaterialManager::GetIns()->GetByName(*it);
			}
			if (stufi == OvrString("skeleton"))
			{
				p = ovr_engine::OvrSkeletonManager::GetIns()->GetByName(*it);
			}

			if (p != nullptr)
				p->Reload();
			file_to_update.pop_front();
			it = file_to_update.begin();
		}
		return true;
	}
}
