﻿#include "../headers.h"
#include <wx_project/command/ovr_command_component.h>

#include <wx_core/ovr_scene_context.h>
#include <wx_core/ovr_scene_camera.h>
#include <wx_project/ovr_project.h>
#include <wx_engine/ovr_resource.h>
#include <wx_engine/ovr_actor.h>

#include <wx_engine/ovr_static_mesh_component.h>
#include <wx_engine/ovr_skinned_mesh_component.h>

#include <wx_engine/ovr_text_component.h>
#include <wx_engine/ovr_camera_component.h>
#include <wx_engine/ovr_box_component.h>
#include <wx_engine/ovr_texture_manager.h>
#include <wx_engine/ovr_material_manager.h>
#include <wx_engine/ovr_mesh_manager.h>

#include <wx_engine/ovr_skeleton_manager.h>

namespace ovr_project 
{
	OvrComponentCmdAdd::OvrComponentCmdAdd(ovr_engine::OvrActor* a_actor, int a_component_type, const OvrString& a_param)
	{
		actor = a_actor;
		component_type = a_component_type;
		param = a_param;
	}

	bool OvrComponentCmdAdd::Do()
	{
		ovr_engine::OvrComponent* com = nullptr;
		switch (component_type)
		{
		case ovr_engine::kComponentLight:		com = actor->AddComponent<ovr_engine::OvrLightComponent>();			break;
		case ovr_engine::kComponentVideo:		com = actor->AddComponent<ovr_engine::OvrVideoComponent>();			break;
		case ovr_engine::kComponentText:		com = actor->AddComponent<ovr_engine::OvrTextComponent>();			break;
		case ovr_engine::kComponentBoxCollider:	com = actor->AddComponent<ovr_engine::OvrBoxColliderComponent>();	break;
		case ovr_engine::kComponentCamera:		com = actor->AddComponent<ovr_engine::OvrCameraComponent>();		break;
		case ovr_engine::kComponentStaticMesh:	com = CreateMeshComponent(true);									break;
		case ovr_engine::kComponentSkinnedMesh:	com = CreateMeshComponent(false);									break;
		}

		if (com != nullptr)
		{
			OvrProject::GetIns()->GetListener()->OnComponentAdd(actor, com);
		}
		return true;
	}

	ovr_engine::OvrComponent* OvrComponentCmdAdd::CreateMeshComponent(bool is_static)
	{
		if (param.GetStringSize() > 0)
		{
			ovr_engine::OvrMesh* mesh = ovr_engine::OvrMeshManager::GetIns()->Load(param);
			if (mesh != nullptr)
			{
				ovr_engine::OvrMeshComponent* com = nullptr;
				if (is_static)
				{
					com = actor->AddComponent<ovr_engine::OvrStaticMeshComponent>();
				}
				else
				{
					com = actor->AddComponent<ovr_engine::OvrSkinnedMeshComponent>();
				}
				com->SetMesh(mesh);
				return com;
			}
		}

		return nullptr;
	}

	OvrComponentCmdDel::OvrComponentCmdDel(ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_compnent)
	{
		actor = a_actor;
		component = a_compnent;
	}

	bool OvrComponentCmdDel::Do()
	{
		int com_type = component->GetType();

		actor->RemoveComponent(component);
		OvrProject::GetIns()->GetListener()->OnComponentDel(actor, com_type);

		return true;
	}

	bool OvrComponentCmdVideoChangeVideoType::Do()
	{
		component->SetVideoType(ovr_engine::VideoType(new_type));
		OvrProject::GetIns()->GetListener()->OnComponentChangeVideoType(actor, component);
		return true;
	}
	
	bool OvrComponentCmdVideoChangeMovieType::Do()
	{
		component->SetMovieType(ovr_engine::MovieType(new_type));
		OvrProject::GetIns()->GetListener()->OnComponentChangeMovieType(actor, component);
		return true;
	}
}

