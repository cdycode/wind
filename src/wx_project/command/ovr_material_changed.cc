﻿#include "../headers.h"
#include <wx_project/command/ovr_material_changed.h>
#include <wx_engine/ovr_texture_manager.h>
#include <wx_engine/ovr_material_manager.h>
#include <wx_engine/ovr_material.h>
#include <wx_engine/ovr_mesh_component.h>
#include <wx_engine/ovr_actor.h>

namespace ovr_project 
{
	bool OvrChangeMaterial::Do() 
	{
		ovr_engine::OvrMaterial* material = ovr_engine::OvrMaterialManager::GetIns()->Load(new_material_path);
		if (nullptr != material)
		{
			auto com = changed_actor->GetComponent<ovr_engine::OvrMeshComponent>();
			com->SetMaterial(material_index, material);
		}
		return true;
	}

	void OvrChangeMaterial::SetNewMaterial(ovr::uint32 a_index, const OvrString& a_new_material_path)
	{
		material_index		= a_index;
		new_material_path	= a_new_material_path;
	}

	bool OvrMaterialChangeTexture::Do()
	{
		if (texture_file_path.GetStringSize() > 0)
		{
			ovr_engine::OvrTexture* texinfo = ovr_engine::OvrTextureManager::GetIns()->Load(texture_file_path);;
			if (nullptr != texinfo)
			{
				material->GetProgramParameters()->SetNameConstant(texture_name, texinfo);
			}
		}

		return true;
	}

	void OvrMaterialChangeTexture::SetTexture(const OvrString& a_texture_name, const OvrString& a_texture_file_path)
	{
		texture_name = a_texture_name;
		texture_file_path = a_texture_file_path;
	}

	bool OvrTextActorChangeTexture::Do() 
	{
		if (texture_file_path.GetStringSize() > 0)
		{
			ovr_engine::OvrTexture* texinfo = ovr_engine::OvrTextureManager::GetIns()->Load(texture_file_path);;
			if (nullptr != texinfo)
			{
				//text_actor->SetBackgroundTexture(texinfo);
			}
		}

		return true;
	}

	void OvrSetMaterialShader::SetShaderInfo(ovr_engine::OvrMaterial* a_material, ovr_engine::OvrGpuProgram* a_shader)
	{
		material = a_material; 
		shader = a_shader; 
	}

	bool OvrSetMaterialShader::Do()
	{
		material->SetProgram(shader);
		return true;
	}

	bool OvrParticleActorChangeTexture::Do()
	{
		if (texture_file_path.GetStringSize() <= 0)
			return false;

		ovr_engine::OvrTexture* texinfo = ovr_engine::OvrTextureManager::GetIns()->Load(texture_file_path);;
		if (nullptr != texinfo)
		{
			//particle_actor->SetTexture(texinfo);
		}
	
		return false;
	}

	bool OvrChangeMesh::Do()
	{
		//只允许骨骼和静态mesh 替换
		ovr_engine::OvrMesh* mesh = ovr_engine::OvrMeshManager::GetIns()->Load(new_mesh_path);
		if (mesh != nullptr)
		{
			auto com = changed_actor->GetComponent<ovr_engine::OvrMeshComponent>();
			com->SetMesh(mesh);
		}
		return true;
	}
}