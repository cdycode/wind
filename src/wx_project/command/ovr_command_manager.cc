﻿#include "../headers.h"
#include <wx_project/command/ovr_command_manager.h>

namespace ovr_project 
{
	OvrCommandManager::OvrCommandManager() 
	{
	}
	OvrCommandManager::~OvrCommandManager() 
	{
		auto command_it = command_list.begin();
		while (command_it != command_list.end())
		{
			IOvrCommand* temp_command = *command_it;
			delete temp_command;
			command_it++;
		}
		command_list.clear();
	}

	//添加命令
	void OvrCommandManager::PushCommand(IOvrCommand* a_command) 
	{
		std::unique_lock<std::mutex> local_lock(command_mutex);
		command_list.push_back(a_command);
	}

	//执行命令
	bool OvrCommandManager::Update() 
	{
		std::unique_lock<std::mutex> local_lock(command_mutex);
		auto command_it = command_list.begin();
		while (command_it != command_list.end())
		{
			IOvrCommand* temp_command = *command_it;
			temp_command->Do();

			delete temp_command;
			command_it++;
		}
		command_list.clear();
		return true;
	}
}
