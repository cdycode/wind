﻿#ifndef WX_AV_MEDIA_WX_AUDIO_DECODER_H_
#define WX_AV_MEDIA_WX_AUDIO_DECODER_H_

namespace wx_av_media 
{
	class wxMiniAudioDecoder;
class wxAudioDecoder : public wx_plugin::IOvrAudioDecoder
{
public:
	wxAudioDecoder();
	virtual ~wxAudioDecoder();

	virtual bool Open(const char* a_file_path)override;
	virtual void Close()override;

	virtual bool GetFileAudioInfo(wx_plugin::StruAudioStreamInfo& a_video_info)override;
	virtual bool SetAudioOutFormat(int a_channels, int a_sample_bits, int a_sample_rate, int a_frame_ms)override;

	virtual bool StartDecoding()override;
	virtual bool Seek(long long a_new_pos)override;
	virtual bool Resume()override;

	virtual wx_plugin::wxFrameBuffer* PopFrame() override;
	virtual void			PushFrame(wx_plugin::wxFrameBuffer* a_frame_buffer) override;

private:
	
	void ReadPacketThreadFunc(void);

	void	DoSeek(long long a_seek_pos);
	void	DecodeOneFrame();

private:
	bool				is_open;
	bool				is_running;		//是否正在运行
	std::thread*		work_thread = nullptr;

	bool				if_file_end = false;		//已到文件结尾

	AVFormatContext*	format_context;
	AVDictionary*		format_opts;
	int					audio_stream_index;

	wxMiniAudioDecoder*	audio_decoder;

	// seek flag
	bool				is_seek = false;		//只有Seek和Play 两种状态
	long long			current_seek_pos = 0;
	long long			next_seek_pos = 0;

};

}

#endif
