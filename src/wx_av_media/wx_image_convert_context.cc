﻿#include "headers.h"
#include <wx_plugin/wx_i_av_media.h>
#include "wx_image_convert_context.h"

namespace wx_av_media 
{

AVPixelFormat wxMediaTypeConversion::ConvertVideoPixelFormat(wx_plugin::ENU_VIDEO_PIXEL_FORMAT a_pixel_type)
{
	AVPixelFormat lFormat = AV_PIX_FMT_NONE;
	switch (a_pixel_type)
	{
	case wx_plugin::kPixelYUV420P:
		lFormat = AV_PIX_FMT_YUV420P;
		break;
	case wx_plugin::kPixelYUV422P:
		lFormat = AV_PIX_FMT_YUV422P;
		break;
	case wx_plugin::kPixelYUYV422:
		lFormat = AV_PIX_FMT_YUYV422;
		break;
	case wx_plugin::kPixelUYVY422:
		lFormat = AV_PIX_FMT_UYVY422;
		break;
	case wx_plugin::kPixelRGB24:
		lFormat = AV_PIX_FMT_RGB24;
		break;
	case wx_plugin::kPixelBGR24:
		lFormat = AV_PIX_FMT_BGR24;
		break;
	case wx_plugin::kPixelARGB32:
		lFormat = AV_PIX_FMT_ARGB;
		break;
	case wx_plugin::kPixelBGRA32:
		lFormat = AV_PIX_FMT_BGRA;
		break;
	case wx_plugin::kPixelRGBA32:
		lFormat = AV_PIX_FMT_RGBA;
		break;
	case wx_plugin::kPixelRGB565LE:
		lFormat = AV_PIX_FMT_RGB565LE;
		break;
	case wx_plugin::kPixelRGB565BE:
		lFormat = AV_PIX_FMT_RGB565BE;
		break;
	default:
		break;
	}
	return lFormat;
}

wx_plugin::ENU_VIDEO_PIXEL_FORMAT wxMediaTypeConversion::ConvertAVPixelFormat(AVPixelFormat a_pixel_format)
{
	wx_plugin::ENU_VIDEO_PIXEL_FORMAT lFormat = wx_plugin::kPixelRGB24;
	switch (a_pixel_format)
	{
	case AV_PIX_FMT_YUV420P:
		lFormat = wx_plugin::kPixelYUV420P;
		break;
	case AV_PIX_FMT_YUV422P:
		lFormat = wx_plugin::kPixelYUV422P;
		break;
	case AV_PIX_FMT_YUYV422:
		lFormat = wx_plugin::kPixelYUYV422;
		break;
	case AV_PIX_FMT_UYVY422:
		lFormat = wx_plugin::kPixelUYVY422;
		break;
	case AV_PIX_FMT_RGB24:
		lFormat = wx_plugin::kPixelRGB24;
		break;
	case AV_PIX_FMT_BGR24:
		lFormat = wx_plugin::kPixelBGR24;
		break;
	case AV_PIX_FMT_ARGB:
		lFormat = wx_plugin::kPixelARGB32;
		break;
	case AV_PIX_FMT_BGRA:
		lFormat = wx_plugin::kPixelBGRA32;
		break;
	case AV_PIX_FMT_RGBA:
		lFormat = wx_plugin::kPixelRGBA32;
		break;
	case AV_PIX_FMT_RGB565LE:
		lFormat = wx_plugin::kPixelRGB565LE;
		break;
	case AV_PIX_FMT_RGB565BE:
		lFormat = wx_plugin::kPixelRGB565BE;
		break;
	default:
		break;
	}
	return lFormat;
}

int wxMediaTypeConversion::GetBytesFromSampleFormat(AVSampleFormat a_sample_format)
{
	int iBytes = 0;
	switch (a_sample_format)
	{
	case AV_SAMPLE_FMT_S16:
	case AV_SAMPLE_FMT_S16P:
		iBytes = 2;
		break;
	case AV_SAMPLE_FMT_S32:
	case AV_SAMPLE_FMT_S32P:
		iBytes = 4;
		break;
	case AV_SAMPLE_FMT_FLT:
	case AV_SAMPLE_FMT_FLTP:
		iBytes = 4;
		break;
	case AV_SAMPLE_FMT_DBL:
	case AV_SAMPLE_FMT_DBLP:
		iBytes = 8;
		break;
	default:
		break;
	}
	return iBytes;
}
AVSampleFormat wxMediaTypeConversion::GetAudioSampleFormat(int a_sample_bytes)
{
	AVSampleFormat lFormat = AV_SAMPLE_FMT_NONE;
	int iBytes = 0;
	switch (a_sample_bytes)
	{
	case 1:
		lFormat = AV_SAMPLE_FMT_U8;
		break;
	case 2:
		lFormat = AV_SAMPLE_FMT_S16;
		break;
	case 4:
		lFormat = AV_SAMPLE_FMT_S32;
		break;
	case 8:
		lFormat = AV_SAMPLE_FMT_DBL;
		break;
	default:
		break;
	}
	return lFormat;
}

//计算视频帧的大小
int wxMediaTypeConversion::GetVideoFrameSize(int a_width, int a_height, wx_plugin::ENU_VIDEO_PIXEL_FORMAT a_pixel_type)
{
	int iFrameSize = 0;
	switch (a_pixel_type)
	{
	case wx_plugin::kPixelYUV420P:
		iFrameSize = a_width*a_height *3/2;
		break;
	case wx_plugin::kPixelYUV422P:
	case wx_plugin::kPixelUYVY422:
	case wx_plugin::kPixelYUYV422:
		iFrameSize = a_width*a_height * 2;
		break;
	case wx_plugin::kPixelRGB24:
	case wx_plugin::kPixelBGR24:
		iFrameSize = a_width*a_height * 3;
		break;
	case wx_plugin::kPixelARGB32:
	case wx_plugin::kPixelBGRA32:
	case wx_plugin::kPixelRGBA32:
		iFrameSize = a_width*a_height * 4;
		break;
	case wx_plugin::kPixelRGB565LE:
	case wx_plugin::kPixelRGB565BE:
		iFrameSize = a_width*a_height * 2;
		break;
	default:
		break;
	}
	return iFrameSize;
}

float wxMediaTypeConversion::GetBytesFromePixFmt(wx_plugin::ENU_VIDEO_PIXEL_FORMAT a_pixel_type)
{
	float fBytes = 0.0f;
	switch (a_pixel_type)
	{
	case wx_plugin::kPixelYUV420P:
		fBytes = 3.0f / 2.0f;
		break;
	case wx_plugin::kPixelYUV422P:
	case wx_plugin::kPixelUYVY422:
	case wx_plugin::kPixelYUYV422:
		fBytes = 2.0f;
		break;
	case wx_plugin::kPixelRGB24:
	case wx_plugin::kPixelBGR24:
		fBytes = 3.0f;
		break;
	case wx_plugin::kPixelARGB32:
	case wx_plugin::kPixelBGRA32:
	case wx_plugin::kPixelRGBA32:
		fBytes = 4.0f;
		break;
	case wx_plugin::kPixelRGB565LE:
	case wx_plugin::kPixelRGB565BE:
		fBytes = 2.0f;
		break;
	default:
		break;
	}
	return fBytes;
}

wxImageConvertContext::wxImageConvertContext(void)
	:img_convert_ctx(NULL)
	,video_out_frame(NULL)
	,out_width(0)
	,out_height(0)
	,out_pixel_type(wx_plugin::kPixelUnknown)
	,out_data_size(0)
{}
wxImageConvertContext::~wxImageConvertContext(void)
{}

bool wxImageConvertContext::Open(AVPixelFormat a_input_format, \
	int a_in_width, int a_in_height, wx_plugin::ENU_VIDEO_PIXEL_FORMAT a_out_type, int a_out_width, int a_out_height)
{
	AVPixelFormat lOutFormat = wxMediaTypeConversion::ConvertVideoPixelFormat(a_out_type);
	if (AV_PIX_FMT_NONE == lOutFormat)
	{
		return false;
	}

	Close();

	AVPixelFormat pixFormat;
	switch (a_input_format) 
	{
	case AV_PIX_FMT_YUVJ420P :
		pixFormat = AV_PIX_FMT_YUV420P;
		break;
	case AV_PIX_FMT_YUVJ422P  :
		pixFormat = AV_PIX_FMT_YUV422P;
		break;
	case AV_PIX_FMT_YUVJ444P   :
		pixFormat = AV_PIX_FMT_YUV444P;
		break;
	case AV_PIX_FMT_YUVJ440P :
		pixFormat = AV_PIX_FMT_YUV440P;
	default:
		pixFormat = a_input_format;
		break;
	}

	img_convert_ctx = sws_getCachedContext(img_convert_ctx,
		a_in_width, a_in_height, pixFormat, a_out_width, a_out_height,
		lOutFormat, SWS_BICUBIC, NULL, NULL, NULL);
	if (NULL == img_convert_ctx)
	{
		Close();
		return false;
	}

	video_out_frame = av_frame_alloc();
	if (NULL == video_out_frame)
	{
		return false;
	}

	out_data_size = wxMediaTypeConversion::GetVideoFrameSize(a_out_width, a_out_height, a_out_type);
	out_width = a_out_width;
	out_height = a_out_height;
	out_pixel_type = a_out_type;
	return true;
}

void wxImageConvertContext::Close(void)
{
	if (NULL != video_out_frame)
	{
		av_frame_free(&video_out_frame);
	}
	if (NULL != img_convert_ctx)
	{
		sws_freeContext(img_convert_ctx);
		img_convert_ctx = NULL;
	}
}

int wxImageConvertContext::ConvertFrame(AVFrame* a_src_frame, char* a_out_buffer, int a_buffer_size)
{
	if (NULL != img_convert_ctx && NULL != video_out_frame)
	{
		FillOutFrame(a_out_buffer, a_buffer_size);
		sws_scale(img_convert_ctx, a_src_frame->data, a_src_frame->linesize,
			0, a_src_frame->height, video_out_frame->data, video_out_frame->linesize);
		return out_data_size;
	}
	return 0;
}

bool wxImageConvertContext::ConvertFrame(AVFrame* a_src_frame, uint8_t* a_out_buffer[], int a_buffer_size[])
{
	if (NULL != img_convert_ctx)
	{
		sws_scale(img_convert_ctx, a_src_frame->data, a_src_frame->linesize,
			0, a_src_frame->height, a_out_buffer, a_buffer_size);
		return true;
	}
	return false;
}

void wxImageConvertContext::FillOutFrame(char* a_out_buffer, int a_buffer_size)
{
	if (wx_plugin::kPixelYUV420P == out_pixel_type)
	{
		video_out_frame->data[0] = (uint8_t*)a_out_buffer;
		video_out_frame->data[1] = (uint8_t*)a_out_buffer + out_width*out_height;
		video_out_frame->data[2] = (uint8_t*)a_out_buffer + out_width*out_height*5/4;
		video_out_frame->linesize[0] = out_width;
		video_out_frame->linesize[1] = out_width/2;
		video_out_frame->linesize[2] = out_width/2;
	}
	else if (wx_plugin::kPixelYUV422P == out_pixel_type)
	{
		video_out_frame->data[0] = (uint8_t*)a_out_buffer;
		video_out_frame->data[1] = (uint8_t*)a_out_buffer + out_width*out_height;
		video_out_frame->data[2] = (uint8_t*)a_out_buffer + out_width*out_height*3/2;
		video_out_frame->linesize[0] = out_width;
		video_out_frame->linesize[1] = out_width/2;
		video_out_frame->linesize[2] = out_width/2;
	}
	else if (wx_plugin::kPixelRGB24 == out_pixel_type || wx_plugin::kPixelBGR24 == out_pixel_type)
	{
		video_out_frame->data[0] = (uint8_t*)a_out_buffer;
		video_out_frame->data[1] = NULL;
		video_out_frame->data[2] = NULL;
		video_out_frame->linesize[0] = out_width*3;
		video_out_frame->linesize[1] = 0;
		video_out_frame->linesize[2] = 0;
	}
}
}
