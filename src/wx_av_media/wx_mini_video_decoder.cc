﻿#include "headers.h"
#include "wx_image_convert_context.h"
#include "wx_mini_video_decoder.h"
#include "wx_video_processing.h"

#include "wx_frame_buffer.h"

namespace wx_av_media 
{

wxMiniVideoDecoder::wxMiniVideoDecoder()
:is_set_output_format(false)
, video_codec_ctx(NULL)
, video_frame(NULL)
, video_processing(NULL)
{
	video_processing = new(std::nothrow) wxVideoProcessing;
}

wxMiniVideoDecoder::~wxMiniVideoDecoder(void)
{
	Close();
	if (NULL != video_processing)
	{
		video_processing->Close();
		delete video_processing;
		video_processing = NULL;
	}
}

int wxMiniVideoDecoder::Open(AVCodecContext* a_codec_context)
{
	if (NULL == video_processing)
	{
		return 1;
	}

	Close();

	// Get a pointer to the codec context for the video stream
	video_codec_ctx = a_codec_context;

	// Find the decoder for the video stream
	AVCodec* pVideoCodec = avcodec_find_decoder(video_codec_ctx->codec_id);
	if (NULL == pVideoCodec)
	{
		return 1;
	}

	// Open codec
	if (avcodec_open2(video_codec_ctx, pVideoCodec, NULL) < 0)
	{
		return 1;
	}

	// Allocate video frame
	video_frame = av_frame_alloc();
	if (NULL == video_frame)
	{
		return 1;
	}

	video_processing->Open(video_codec_ctx->pix_fmt, video_codec_ctx->width, video_codec_ctx->height);
	return 0;
}
void wxMiniVideoDecoder::Close(void)
{
	is_set_output_format = false;

	//LOGE("m_oFrameBufferList.Reset()");
	if (NULL != video_codec_ctx)
	{
		avcodec_close(video_codec_ctx);
		video_codec_ctx = NULL;
	}
	//LOGE("avcodec_close(m_pVideoCodecCtx)");
	if (NULL != video_frame)
	{
		av_frame_free(&video_frame);
		video_frame = NULL;
	}
	//LOGE("av_frame_free(&m_pVideoFrame)");
	if (NULL != video_processing)
	{
		video_processing->Close();
	}

	frame_buffer_list.Close();
	UninitBuffer();
	//LOGE("m_oFrameBufferList.Close");
}

int wxMiniVideoDecoder::SetVideoOutFormat(int a_width, int a_height, wx_plugin::ENU_VIDEO_PIXEL_FORMAT a_pixel_format)
{
	if (NULL == video_codec_ctx)
	{
		return 3;
	}
	if (NULL == video_processing)
	{
		return 1;
	}

	if (is_set_output_format)
	{
		return 0;
	}
	is_set_output_format = true;

	int iBufferSize = wxMediaTypeConversion::GetVideoFrameSize(a_width, a_height, a_pixel_format);
	if (!InitBuffer(iBufferSize))
	{
		return 1;
	}
	
	return video_processing->SetVideoOutFormat(a_width, a_height, a_pixel_format);
}

wx_plugin::wxFrameBuffer* wxMiniVideoDecoder::PopFrame()
{
	if (NULL == video_codec_ctx)
	{
		return nullptr;
	}

	//获取结果Buffer
	return frame_buffer_list.PopResultFrontNoBlock();;
}

void wxMiniVideoDecoder::PushFrame(wx_plugin::wxFrameBuffer* a_frame_buffer)
{
	if (nullptr != a_frame_buffer)
	{
		frame_buffer_list.PushFreeBack(a_frame_buffer);
	}
}

int wxMiniVideoDecoder::GetVideoFrameCnt(void)
{
	return frame_buffer_list.GetResultListSize();
}


bool wxMiniVideoDecoder::DecodePackage(AVPacket* a_packet)
{
	if (NULL == a_packet || a_packet->size < 0)
	{
		//			LOGI("LingVideoDecoder::WorkThreadFunc NULL == pPacket");
		return false;
	}

	//解码
	int iFinished = 0;
	if (avcodec_decode_video2(video_codec_ctx, video_frame, &iFinished, a_packet) < 0)
	{
		//解码失败
		return false;
	}

	if (0 != iFinished)
	{
		//LOGI("LingVideoDecoder::WorkThreadFunc begin DealOneFrame");
		DealOneFrame();

		return true;
		//LOGI("LingVideoDecoder::WorkThreadFunc end DealOneFrame");
	}
	return false;
}

void wxMiniVideoDecoder::Seek()
{
	// seek packet
	// 清除解码器上下文
	avcodec_flush_buffers(video_codec_ctx);
}

void wxMiniVideoDecoder::NotifyFrame()
{
	frame_buffer_list.Close();
}

inline void wxMiniVideoDecoder::DealOneFrame(void)
{
	//获得一帧数据
	//LOGE("LingVideoDecoder::WorkThreadFunc In : Free %d , Result %d", m_oFrameBufferList.GetFreeListSize(), m_oFrameBufferList.GetResultListSize());
	wx_plugin::wxFrameBuffer* pBuffer = frame_buffer_list.PopFreeFront();
	//LOGE("LingVideoDecoder::WorkThreadFunc Call DealOneFrame m_oFrameBufferList.GetFreeBufferBlock()");
	if (NULL != pBuffer)
	{
		pBuffer->data_size = video_processing->Processing(video_frame, pBuffer->buffer, pBuffer->buffer_size);
		pBuffer->pts = (long long)(av_frame_get_best_effort_timestamp(video_frame) * (1000 * av_q2d(av_codec_get_pkt_timebase(video_codec_ctx))));

		//LOGD("LingVideoDecoder::WorkThreadFunc Decode Video pts: %lld\n", pBuffer->m_llPTS);
		frame_buffer_list.PushResultBack(pBuffer);
	}
}

bool wxMiniVideoDecoder::InitBuffer(int a_buffer_size)
{
	UninitBuffer();

	for (int i = 0; i < DEF_LING_VIDEO_BUFFER_CNT; i++)
	{
		wx_plugin::wxFrameBuffer* new_buffer = new wx_plugin::wxFrameBuffer;
		if (nullptr == new_buffer)
		{
			continue;
		}
		if (new_buffer->Open(a_buffer_size))
		{
			frame_buffer_list.PushFreeBack(new_buffer);
		}
		else 
		{
			delete new_buffer;
		}
	}

	frame_buffer_list.Open();
	return frame_buffer_list.GetFreeListSize() > 0;
}

void wxMiniVideoDecoder::UninitBuffer() 
{
	while (frame_buffer_list.GetFreeListSize() > 0)
	{
		wx_plugin::wxFrameBuffer* frame_buffer = frame_buffer_list.PopFreeFrontNoBlock();
		if (nullptr != frame_buffer)
		{
			frame_buffer->Close();
			delete frame_buffer;
		}
	}
	while (frame_buffer_list.GetResultListSize() > 0)
	{
		wx_plugin::wxFrameBuffer* frame_buffer = frame_buffer_list.PopResultFrontNoBlock();
		if (nullptr != frame_buffer)
		{
			frame_buffer->Close();
			delete frame_buffer;
		}
	}
}

}
