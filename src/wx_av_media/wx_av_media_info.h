﻿#ifndef WX_AV_MEDIA_WX_AV_MEDIA_INFO_H_
#define WX_AV_MEDIA_WX_AV_MEDIA_INFO_H_

namespace wx_av_media 
{
	class wxMediaInfo: public wx_plugin::wxIMediaInfo
	{
	public:
		wxMediaInfo() {}
		virtual ~wxMediaInfo() {}

		virtual bool	Open(const char* a_file_path) override;

		virtual bool	IsOwnVideo()const override { return is_own_video; }
		virtual bool	IsOwnAudio()const override { return is_own_audio; }

		virtual const wx_plugin::StruVideoStreamInfo&	GetVideoInfo()const override { return video_info; }
		virtual const wx_plugin::StruAudioStreamInfo&	GetAudioInfo()const override { return audio_info; }

	private:

		bool		is_own_video = false;					//是否有音频流
		bool		is_own_audio = false;					//是否有音频流

		wx_plugin::StruVideoStreamInfo	video_info;
		wx_plugin::StruAudioStreamInfo	audio_info;
	};
}

#endif
