﻿#include "headers.h"
#include "wx_audio_decoder.h"
#include "wx_mini_audio_decoder.h"
#include "wx_image_convert_context.h"

//定义最大Packet数目  视频和音频总和的Packet数目不超过20个
#define DEF_LING_MIN_FRAMES 20
#define DEF_LING_ERR_SEEKPOS	(-9999)	
namespace wx_av_media 
{

wxAudioDecoder::wxAudioDecoder(void)
:is_open(false)
, is_running(false)
, format_context(NULL)
, format_opts(NULL)
, audio_stream_index(-1)
, audio_decoder(NULL)
{
	audio_decoder = new(std::nothrow) wxMiniAudioDecoder();
}

wxAudioDecoder::~wxAudioDecoder(void)
{
	Close();
	if (NULL != audio_decoder)
	{
		delete audio_decoder;
		audio_decoder = NULL;
	}
}

bool wxAudioDecoder::Open(const char* a_file_name)
{
	if (is_open)
	{
		return true;
	}
	if (NULL == a_file_name)
	{
		return false;
	}

	if (NULL == audio_decoder)
	{
		return false;
	}

	Close();

	//创建Context
	format_context = avformat_alloc_context();

	//寻找数据流
	int scan_all_pmts_set = 0;
	if (!av_dict_get(format_opts, "scan_all_pmts", NULL, AV_DICT_MATCH_CASE))
	{
		av_dict_set(&format_opts, "scan_all_pmts", "1", AV_DICT_DONT_OVERWRITE);
		scan_all_pmts_set = 1;
	}

	// Open video file
	if (0 != avformat_open_input(&format_context, a_file_name, NULL, &format_opts))
	{
		return 1;
	}

	if (scan_all_pmts_set)
		av_dict_set(&format_opts, "scan_all_pmts", NULL, AV_DICT_MATCH_CASE);

	AVDictionaryEntry *t;
	if ((t = av_dict_get(format_opts, "", NULL, AV_DICT_IGNORE_SUFFIX))) 
	{
		return false;
	}

	// Retrieve stream information
	if (avformat_find_stream_info(format_context, NULL) < 0)
	{
		return false;
	}
	
	audio_stream_index = av_find_best_stream(format_context, AVMEDIA_TYPE_AUDIO, -1, -1, NULL, 0);

	if (0 > audio_stream_index)
	{
		return false;
	}


	if (audio_stream_index >= 0)
	{
		if (0 != audio_decoder->Open(format_context->streams[audio_stream_index]->codec))
		{
			audio_decoder->Close();
		}
	}

	is_open = true;
	return true;
}
void wxAudioDecoder::Close(void)
{
	//LOGE("LingFileDecoder::Close");

	is_open = false;
	is_running = false;

	//等待工作线程退出
	if (nullptr != work_thread)
	{
		audio_decoder->NotifyFrame();
		work_thread->join();
		delete work_thread;
		work_thread = nullptr;
	}
	
	//LOGE("LingFileDecoder -- m_pVideoDecoder->Close()");
	if (NULL != audio_decoder)
	{
		audio_decoder->Close();
	}
	//LOGE("LingFileDecoder -- m_pAudioDecoder->Close()");
	
	//LOGE("LingFileDecoder::Close thread exit");

	//关闭文件上下文
	if (NULL != format_context)
	{
		avformat_close_input(&format_context);
		format_context = NULL;
	}
	if (NULL != format_opts)
	{
		av_dict_free(&format_opts);
		format_opts = NULL;
	}
	//重置数据流
	audio_stream_index = -1;
}


bool wxAudioDecoder::GetFileAudioInfo(wx_plugin::StruAudioStreamInfo& a_audio_info)
{
	if (audio_stream_index < 0)
	{
		return false;
	}

	AVCodecContext* pContext = format_context->streams[audio_stream_index]->codec;

	a_audio_info.channel_num	= pContext->channels; // 根据音频信息填充
	a_audio_info.sample_rate	= pContext->sample_rate;
	a_audio_info.sample_bits	= wxMediaTypeConversion::GetBytesFromSampleFormat(pContext->sample_fmt) * 8;
	a_audio_info.length		= format_context->duration/1000;

	return true;
}

bool wxAudioDecoder::SetAudioOutFormat(int a_channels, int a_sample_bits, int a_sample_rate, int a_frame_ms)
{
	if (!is_open)
	{
		return false;
	}
	if (is_running)
	{
		return false;
	}
	if (NULL == audio_decoder)
	{
		return false;
	}

	return 0 == audio_decoder->SetAudioOutFormat(a_channels, a_sample_bits, a_sample_rate, a_frame_ms);
}

//开始解码
bool wxAudioDecoder::StartDecoding()
{
	if (!is_open)
	{
		return false;
	}

	if (is_running)
	{
		return false;
	}

	is_running = true;
	work_thread = new std::thread(&wxAudioDecoder::ReadPacketThreadFunc, this);
	if (nullptr == work_thread)
	{
		return false;
	}

	return true;
}

wx_plugin::wxFrameBuffer* wxAudioDecoder::PopFrame()
{
	if (NULL != audio_decoder)
	{
		return audio_decoder->PopFrame();
	}
	return nullptr;
}

void wxAudioDecoder::PushFrame(wx_plugin::wxFrameBuffer* a_frame_buffer)
{
	if (NULL != audio_decoder)
	{
		audio_decoder->PushFrame(a_frame_buffer);
	}
}


bool wxAudioDecoder::Seek(long long a_new_pos)
{
	//LOGI("LingFileDecoder::Seek begin");

	if (NULL == format_context)
	{
		return false;
	}
	if ( a_new_pos < 0 || a_new_pos >= (format_context->duration / 1000))
	{
		return false;
	}
	
	next_seek_pos = a_new_pos * 1000;
	is_seek = true;

	//LOGI("LingFileDecoder::Seek end");
	return true;
}

bool wxAudioDecoder::Resume()
{
	is_seek = false;
	return true;
}


void wxAudioDecoder::ReadPacketThreadFunc(void)
{
#ifdef LING_ANDROID_NDK
	pthread_t th_id = pthread_self();
	LOGD("Enter: %s %s, thread id = %lu\n", __FILE__, __FUNCTION__, th_id);
#endif
	//LOGI("LingFileDecoder::ReadPacketThreadFunc Begin Free Packet Size:%d", m_lstFreePacket.Size());
	if (!is_open || !is_running)
	{
		//LOGI("LingFileDecoder::ReadPacketThreadFunc !m_bOpen || !m_bRunning");
		return;
	}

	// Read frames and save first five frames to disk
	while (is_running)
	{
		if (is_seek)
		{
			if (current_seek_pos != next_seek_pos)
			{
				DoSeek(next_seek_pos);
			}
			else
			{
				OvrTime::SleepMs(10);
				continue;
			}
		}
		else
		{
			if (current_seek_pos != next_seek_pos)
			{
				DoSeek(next_seek_pos);
			}
		}

		DecodeOneFrame();
	}

#ifdef LING_ANDROID_NDK
	//LOGI("LingFileDecoder::ReadPacketThreadFunc  exit");
#endif
}
 

void wxAudioDecoder::DoSeek(long long a_seek_pos)
{
	current_seek_pos = a_seek_pos;

	avformat_seek_file(format_context, -1, 0, current_seek_pos, current_seek_pos, AVSEEK_FLAG_BACKWARD);
	audio_decoder->Seek();
}

void wxAudioDecoder::DecodeOneFrame() 
{
	bool is_one_frame = false;
	while (!is_one_frame)
	{
		AVPacket* pPacket = new(std::nothrow)AVPacket;
		if (NULL == pPacket)
		{
			//LOGI("LingFileDecoder::ReadPacketThreadFunc NULL == pPacket");
			OvrTime::SleepMs(10);
			continue;
		}
		//LOGI("LingFileDecoder::ReadPacketThreadFunc end m_lstFreePacket.BlockPopFront()");

		int iResult = av_read_frame(format_context, pPacket);
		if (iResult < 0)
		{
#ifdef			LING_ANDROID_NDK
			char szBuffer[256];
			memset(szBuffer, 0, 256);
			av_strerror(iResult, szBuffer, 255);
			LOGE("LingFileDecoder::ReadPacketThreadFunc End of the file, result:%d, error string:%s", iResult, szBuffer);
#endif
			delete pPacket;
			pPacket = NULL;
			if_file_end = true;
			break;
		}
		// Is this a packet from the video stream?
		if (pPacket->stream_index == audio_stream_index)
		{
			is_one_frame = audio_decoder->DecodePackage(pPacket);
		}

		delete pPacket;
		pPacket = NULL;
	}
}


}
