﻿#ifndef WX_AV_MEDIA_WX_VIDEO_PROCESSING_H_
#define WX_AV_MEDIA_WX_VIDEO_PROCESSING_H_

namespace wx_av_media 
{

	enum ENU_VIDEO_PLAY_MODE
	{
		ENU_VIDEO_PLAY_UNKNOWN = 0,
		ENU_VIDEO_PLAY_ORIGINAL,		// 原画
		ENU_VIDEO_PLAY_DOUBLE,			// 左右双屏播放
		ENU_VIDEO_PLAY_3D_SBS,			// 3D左右格式，Side By Side
		ENU_VIDEO_PLAY_3D_TB,			// 3D上下格式模式播放， Top & Button
	};

class wxImageConvertContext;
class wxVideoProcessing
{
public:
	wxVideoProcessing(void);
	~wxVideoProcessing(void);

	void Open(AVPixelFormat a_input_format, int a_input_width, int a_input_height);
	void Close(void);

	int SetVideoOutFormat(int a_width, int a_height, wx_plugin::ENU_VIDEO_PIXEL_FORMAT a_pixel_format);

	//设置3D模式
	int SetVideoPlayMode(ENU_VIDEO_PLAY_MODE a_play_mode);

	int Processing(AVFrame* a_frame, char* a_buffer, int a_buffer_size);

private:
	// Picture2Buffer
	int Pict2Buf_Original(char* a_buffer, int a_buffer_size);
	
	int Pict2Buf_Double(char* a_buffer, int a_buffer_size);

	int Pict2Buf_3D_TB(char* a_buffer, int a_buffer_size);

	int Pict2Buf_3D_SBS(char* a_buffer, int a_buffer_size);

	//初始化播放模式
	bool InitPlayMode(void);

	//重置转换Context
	bool ResetImageConvertCtx(void);

private:
	void CloseImageConvertContext(void);
private:
	
	AVPixelFormat			input_pixel_format;
	int						width;
	int						height;

	int						out_width;
	int						out_height;
	wx_plugin::ENU_VIDEO_PIXEL_FORMAT	out_pixel_type;	//输出像素格式

	//播放模式
	ENU_VIDEO_PLAY_MODE		new_play_mode;
	unsigned int			new_count;		//设置播放模式计数

	ENU_VIDEO_PLAY_MODE		current_mode;		//当前播放Mode
	unsigned int			current_count;	//当前设置计数

	//用于图像转换
	wxImageConvertContext*	image_convert_ctx;
	AVPicture				av_picture;

	int						convert_width;	//转换的Width
	int						convert_height;	//转换的Height
	int						half_black_size;	//黑场数据大小
	int						convert_pitch;	//转换后的行间距
};
}
#endif
