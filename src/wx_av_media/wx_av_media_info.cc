﻿#include "headers.h"
#include "wx_av_media_info.h"
#include "wx_image_convert_context.h"

namespace wx_av_media 
{
	bool wxMediaInfo::Open(const char* a_file_path) 
	{
		//初始化
		is_own_audio = false;
		is_own_video = false;

		bool is_success = false;
		

		//创建Context
		AVFormatContext* format_context = avformat_alloc_context();
		AVDictionary*	format_opts = nullptr;

		//寻找数据流
		int scan_all_pmts_set = 0;
		if (!av_dict_get(format_opts, "scan_all_pmts", NULL, AV_DICT_MATCH_CASE))
		{
			av_dict_set(&format_opts, "scan_all_pmts", "1", AV_DICT_DONT_OVERWRITE);
			scan_all_pmts_set = 1;
		}

        int video_stream_index = -1;
        int audio_stream_index = -1;
        AVDictionaryEntry *t;

		// Open video file
		if (0 != avformat_open_input(&format_context, a_file_path, NULL, &format_opts))
		{
			goto OpenEnd;
		}

		if (scan_all_pmts_set)
			av_dict_set(&format_opts, "scan_all_pmts", NULL, AV_DICT_MATCH_CASE);

		if ((t = av_dict_get(format_opts, "", NULL, AV_DICT_IGNORE_SUFFIX)))
		{
			goto OpenEnd;
		}

		// Retrieve stream information
		if (avformat_find_stream_info(format_context, NULL) < 0)
		{
			goto OpenEnd;
		}

		video_stream_index = av_find_best_stream(format_context, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
		audio_stream_index = av_find_best_stream(format_context, AVMEDIA_TYPE_AUDIO, -1, -1, NULL, 0);

		is_success = true;		

		//打开相应的解码器
		if (video_stream_index >= 0)
		{
			AVCodecContext* pContext = format_context->streams[video_stream_index]->codec;

			is_own_video = true;

			video_info.frame_width = pContext->width;
			video_info.frame_height = pContext->height;
			video_info.pixel_format = wxMediaTypeConversion::ConvertAVPixelFormat(pContext->pix_fmt);
			video_info.frame_rate.numerator = format_context->streams[video_stream_index]->r_frame_rate.num;
			video_info.frame_rate.denominator = format_context->streams[video_stream_index]->r_frame_rate.den;
			video_info.length = format_context->duration / 1000;

		}

		if (audio_stream_index >= 0)
		{
			AVCodecContext* pContext = format_context->streams[audio_stream_index]->codec;

			is_own_audio = true;
			audio_info.channel_num	= pContext->channels; // 根据音频信息填充
			audio_info.sample_rate	= pContext->sample_rate;
			audio_info.sample_bits	= wxMediaTypeConversion::GetBytesFromSampleFormat(pContext->sample_fmt) * 8;
			audio_info.length		= format_context->duration / 1000;
		}

	OpenEnd:
		//关闭文件上下文
		if (NULL != format_context)
		{
			avformat_close_input(&format_context);
			format_context = NULL;
		}
		if (NULL != format_opts)
		{
			av_dict_free(&format_opts);
			format_opts = NULL;
		}
		return is_success;
	}
}
