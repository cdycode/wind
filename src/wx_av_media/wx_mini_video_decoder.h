﻿#ifndef WX_AV_MEDIA_WX_MINI_VIDEO_DECODER_H_
#define WX_AV_MEDIA_WX_MINI_VIDEO_DECODER_H_

#include "wx_frame_buffer.h"

namespace wx_av_media 
{
class wxVideoProcessing;
class wxMiniVideoDecoder
{
public:
	wxMiniVideoDecoder();
	~wxMiniVideoDecoder(void);

	int		Open(AVCodecContext* a_codec_context);
	void	Close(void);

	int	 SetVideoOutFormat(int a_width, int a_height, wx_plugin::ENU_VIDEO_PIXEL_FORMAT a_pixel_format);

	wx_plugin::wxFrameBuffer* PopFrame();
	void			PushFrame(wx_plugin::wxFrameBuffer* a_frame_buffer);

	int GetVideoFrameCnt(void);

	// add new function
	bool	DecodePackage(AVPacket* a_packet);
	void	Seek();
	void	NotifyFrame();
private:
	inline void DealOneFrame(void);

	bool	InitBuffer(int a_buffer_size);
	void	UninitBuffer();

private:
	
	bool				is_set_output_format;

	AVCodecContext*		video_codec_ctx;
	AVFrame*			video_frame;

	wxVideoProcessing*	video_processing;

	wxBlockDoubleQueue<wx_plugin::wxFrameBuffer>	frame_buffer_list;
};

}
#endif