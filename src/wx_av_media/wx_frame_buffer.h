﻿#ifndef WX_AV_MEDIA_WX_FRAME_BUFFER_H_
#define WX_AV_MEDIA_WX_FRAME_BUFFER_H_

//Buffer的缓冲数目
#define DEF_LING_VIDEO_BUFFER_CNT 5

#define DEF_LING_AUDIO_BUFFER_CNT 5

//Seek的取值
#define DEF_LING_SEEK_VALUE -1

//文件播放结束取值
#define DEF_LING_FILE_END_VALUE -2

namespace wx_av_media 
{


class wxFrameBufferList
{
public:
	wxFrameBufferList(void);
	~wxFrameBufferList(void);

	//分配Buffer空间
	bool Open(int a_buffer_cnt, int a_buffer_size);
	void Close(void);

	//重置 可以让阻塞的调用立即返回  
	void Reset(void);

	//Seek状体操作
	bool IsSeekStatus(void)const{ return is_seeking; }
	void SetSeekStatus(void);

	//清楚Resultbuffer 并放置SeekBuffer
	void PushSeekBuffer(void);

	//获取空闲的Buffer
	wx_plugin::wxFrameBuffer*	GetFreeBufferBlock(void);

	//获取结果Buffer
	wx_plugin::wxFrameBuffer*	GetResultBufferBlock(void);

	int GetResultListSize(void)const { return result_buffer_list.size(); }
	int GetFreeListSize(void)const { return free_buffer_list.size(); }

	//释放空闲Buffer
	void PushFreeBuffer(wx_plugin::wxFrameBuffer* a_free_buffer);

	//装入SeekBuffer
	void PushResultBuffer(wx_plugin::wxFrameBuffer* a_result_buffer);

private:
	bool						is_working;
	bool						is_seeking;
	std::mutex					free_mutex;		//两个锁一起使用时 规定先获取m_oResultMutex锁 然后再获取m_oFreeMutex锁
	std::mutex					result_mutex;		//涉及到双锁操作的函数有：Close、PushSeekBuffer和GetResultBuffer
	std::condition_variable				free_condition;
	std::condition_variable				result_condition;
	std::list<wx_plugin::wxFrameBuffer*>	free_buffer_list;
	std::list<wx_plugin::wxFrameBuffer*>	result_buffer_list;

	wx_plugin::wxFrameBuffer				seek_buffer;
};

}

#endif