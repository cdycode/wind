﻿#ifndef WX_AV_MEDIA_WX_IMAGE_CONVERT_CONTEXT_H_
#define WX_AV_MEDIA_WX_IMAGE_CONVERT_CONTEXT_H_

namespace wx_av_media 
{

class wxMediaTypeConversion
{
public:
	wxMediaTypeConversion(void){}
	~wxMediaTypeConversion(void){}

	static AVPixelFormat ConvertVideoPixelFormat(wx_plugin::ENU_VIDEO_PIXEL_FORMAT a_pixel_type);
	static wx_plugin::ENU_VIDEO_PIXEL_FORMAT ConvertAVPixelFormat(AVPixelFormat a_pixel_format);

	static int GetBytesFromSampleFormat(AVSampleFormat a_sample_format);
	static AVSampleFormat GetAudioSampleFormat(int a_sample_bytes);

	//计算视频帧的大小
	static int GetVideoFrameSize(int a_width, int a_height, wx_plugin::ENU_VIDEO_PIXEL_FORMAT a_pixel_type);
	static float GetBytesFromePixFmt(wx_plugin::ENU_VIDEO_PIXEL_FORMAT a_pixel_type);
};


class wxImageConvertContext
{
public:
	wxImageConvertContext(void);
	~wxImageConvertContext(void);

	bool Open(AVPixelFormat a_in_format, int a_in_width, int a_in_height,\
		wx_plugin::ENU_VIDEO_PIXEL_FORMAT a_out_type, int a_out_width, int a_out_height);
	void Close(void);

	int  ConvertFrame(AVFrame* a_src_frame, char* a_out_buffer, int a_buffer_size);

	bool ConvertFrame(AVFrame* a_src_frame, uint8_t* a_out_buffer[], int a_buffer_size[]);

private:
	void FillOutFrame(char* a_out_buffer, int a_buffer_size);
	
private:
	struct SwsContext*		img_convert_ctx;
	AVFrame*				video_out_frame;
	int						out_width;
	int						out_height;
	wx_plugin::ENU_VIDEO_PIXEL_FORMAT	out_pixel_type;
	int						out_data_size;
};

}

#endif

