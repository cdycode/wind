﻿#ifndef WX_AV_MEDIA_WX_VIDEO_DECODER_H_
#define WX_AV_MEDIA_WX_VIDEO_DECODER_H_

#define  MAX_PKT_DATA_SIZE	(20 * 1024 *1024)
#define  MIN_PKT_NUM		( 5 )


namespace wx_av_media 
{
class wxMiniVideoDecoder;

// Class Define : LingFileDecoder
class  wxVideoDecoder : public wx_plugin::wxIVideoDecoder
{
public:
	wxVideoDecoder(void);
	virtual ~wxVideoDecoder(void);

	virtual bool Open(const char* a_file_path) override;
	virtual void Close() override;

	virtual bool GetFileVideoInfo(wx_plugin::StruVideoStreamInfo& a_video_info) override;
	virtual bool SetVideoOutFormat(int a_width, int a_height, wx_plugin::ENU_VIDEO_PIXEL_FORMAT a_pixel_format) override;

	virtual bool StartDecoding() override;
	virtual bool Seek(long long a_new_pos) override;
	virtual bool Resume() override;

	virtual wx_plugin::wxFrameBuffer* PopFrame() override;
	virtual void			PushFrame(wx_plugin::wxFrameBuffer* a_frame_buffer) override;

	//获取音视频结果Buffer的数目
	int GetVideoFrameCnt(void);

private:
	void ReadPacketThreadFunc(void);

	bool	DecodeOneFrame();
	void	DoSeek(long long a_seek_pos);
private:
	bool				is_open;
	bool				is_running;		//是否正在运行
	
	std::thread*		work_thread = nullptr;

	bool				is_file_end = false;		//已到文件结尾


	AVFormatContext*	format_context;
	AVDictionary*		format_opts;
	int					video_stream_index;
	wx_plugin::StruVideoFrameRate	frame_rate;

	wxMiniVideoDecoder*	video_decoder;

	// seek flag
	bool				is_seek = false;		//只有Seek和Play 两种状态
	long long			current_seek_pos = 0;
	long long			next_seek_pos = 0;
};

}
#endif

