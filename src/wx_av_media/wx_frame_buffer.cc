﻿#include "headers.h"
#include "wx_frame_buffer.h"

namespace wx_av_media 
{

wxFrameBufferList::wxFrameBufferList(void)
:is_working(false)
, is_seeking(false)
{
	seek_buffer.data_size = DEF_LING_SEEK_VALUE;
	seek_buffer.pts = DEF_LING_SEEK_VALUE;
}
wxFrameBufferList::~wxFrameBufferList(void)
{}

//分配Buffer空间
bool wxFrameBufferList::Open(int a_buffer_cnt, int a_buffer_size)
{
	Close();

	std::unique_lock<std::mutex> auto_lock(free_mutex);
	for (int i = 0; i < a_buffer_cnt; i++)
	{
		wx_plugin::wxFrameBuffer* pBuffer = new wx_plugin::wxFrameBuffer;
		if (NULL == pBuffer)
		{
			break;
		}
		if (!pBuffer->Open(a_buffer_size))
		{
			delete pBuffer;
			pBuffer = NULL;
			break;
		}
		free_buffer_list.push_back(pBuffer);
	}

	is_working = (a_buffer_cnt == free_buffer_list.size());
	return is_working;
}
void wxFrameBufferList::Close(void)
{
	Reset();

	//获取读写权限
	std::unique_lock<std::mutex> lResultLock(result_mutex);
	std::unique_lock<std::mutex> lFreeLock(free_mutex);

	//释放空闲Buffer
	while (free_buffer_list.size() > 0)
	{
		wx_plugin::wxFrameBuffer* pBuffer = free_buffer_list.front();
		free_buffer_list.pop_front();
		if (NULL != pBuffer)
		{
			pBuffer->Close();
			delete pBuffer;
		}
	}

	//是否结果Buffer
	while (result_buffer_list.size() > 0)
	{
		wx_plugin::wxFrameBuffer* pBuffer = result_buffer_list.front();
		result_buffer_list.pop_front();
		if (NULL != pBuffer)
		{
			pBuffer->Close();
			delete pBuffer;
		}
	}
}

//重置 可以让阻塞的调用立即返回
void wxFrameBufferList::Reset(void)
{
	//状态设置
	is_working = false;
	is_seeking = false;

	//释放信号 保证不存在等待情况
	free_condition.notify_one();
	result_condition.notify_one();
}

void wxFrameBufferList::SetSeekStatus(void)
{
	std::unique_lock<std::mutex> lAutoLock(result_mutex);
	is_seeking = true;
}

//放置SeekBuffer
void wxFrameBufferList::PushSeekBuffer(void)
{
	std::unique_lock<std::mutex> lResultLock(result_mutex);
	std::unique_lock<std::mutex> lFreeLock(free_mutex);

	//清空ResultBuffer
	while (result_buffer_list.size() > 0)
	{
		wx_plugin::wxFrameBuffer* pBuffer = result_buffer_list.front();
		result_buffer_list.pop_front();
		if (NULL != pBuffer)
		{
			free_buffer_list.push_back(pBuffer);
		}
	}
	//放置SeekBuffer
	result_buffer_list.push_back(&seek_buffer);
	result_condition.notify_one();

	//LOGI("LingFrameBufferList::PushSeekBuffer one Seek buffer,Result %d, Free %d ", m_lstResultBuffer.size(), m_lstFreeBuffer.size());
}

wx_plugin::wxFrameBuffer* wxFrameBufferList::GetFreeBufferBlock(void)
{
	std::unique_lock<std::mutex> auto_lock(free_mutex);

	wx_plugin::wxFrameBuffer* pBuffer = NULL;

	if (free_buffer_list.size() > 0)
	{
		pBuffer = free_buffer_list.front();
		free_buffer_list.pop_front();
	}
	else
	{
		while (is_working && free_buffer_list.size() < 1)
		{
			free_condition.wait(auto_lock);
		}
		if (free_buffer_list.size() > 0)
		{
			pBuffer = free_buffer_list.front();
			free_buffer_list.pop_front();
		}
	}
	return pBuffer;
}

//获取结果Buffer
wx_plugin::wxFrameBuffer* wxFrameBufferList::GetResultBufferBlock(void)
{
	std::unique_lock<std::mutex> auto_lock(result_mutex);

	wx_plugin::wxFrameBuffer* pBuffer = NULL;
	while (is_working)
	{
		if (result_buffer_list.size() > 0)
		{
			//不空就返回第一个元素
			pBuffer = result_buffer_list.front();
			result_buffer_list.pop_front();

			//判断是否处于Seek状态
			if (!is_seeking)
			{
				break;
			}
			else
			{
				if (pBuffer->data_size == DEF_LING_SEEK_VALUE)
				{
					//LOGI("LingFrameBufferList::GetResultBufferBlock seek Status  findSeek Buffer");
					is_seeking = false;
				}
				else
				{
					//LOGI("LingFrameBufferList::GetResultBufferBlock seek Status  Not findSeek Buffer");
					//还未找到SeekBuffer 继续
					PushFreeBuffer(pBuffer);
				}
				pBuffer = NULL;
			}
			
		}
		else
		{
			//LOGI("LingFrameBufferList::GetResultBufferBlock Wait new Buffer");
			//没有元素就等待
			result_condition.wait(auto_lock);
			//LOGI("LingFrameBufferList::GetResultBufferBlock  End Wait new Buffer");
		}
	}

	return pBuffer;
}

//释放空闲Buffer
void wxFrameBufferList::PushFreeBuffer(wx_plugin::wxFrameBuffer* a_free_buffer)
{
	if (NULL != a_free_buffer)
	{
		std::unique_lock<std::mutex> lAutoLock(free_mutex);
		free_buffer_list.push_back(a_free_buffer);
		free_condition.notify_one();
	}
}

//装入SeekBuffer
void wxFrameBufferList::PushResultBuffer(wx_plugin::wxFrameBuffer* a_result_buffer)
{
	if (NULL != a_result_buffer)
	{
		std::unique_lock<std::mutex> lAutoLock(result_mutex);
		result_buffer_list.push_back(a_result_buffer);
		result_condition.notify_one();
	}
}

}
