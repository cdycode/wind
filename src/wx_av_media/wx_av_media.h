﻿#ifndef WX_AV_MEDIA_WX_AV_MEDIA_H__
#define WX_AV_MEDIA_WX_AV_MEDIA_H__

#ifdef _WIN32
#	ifdef WXAVMEDIA_EXPORTS
#		define WX_AV_MEDIA_API __declspec(dllexport)
#	else
#		define WX_AV_MEDIA_API __declspec(dllimport)
#	endif
#else
#	define WX_AV_MEDIA_API  
#endif


namespace wx_av_media 
{
	class wxAVMedia : public wx_plugin::IOvrAVMedia
	{
	public:
		wxAVMedia() { display_name = "AV media"; }
		virtual ~wxAVMedia() {}

	public:
		virtual	bool				Init() override;
		virtual void				Uninit() override;

		virtual wx_plugin::wxIVideoDecoder*	CreateVideoDecoder() override;
		virtual void						DestoryVideoDecoder(wx_plugin::wxIVideoDecoder*& a_video_decoder) override;
		virtual wx_plugin::IOvrAudioDecoder*	CreateAudioDecoder() override;
		virtual void						DestoryAudioDecoder(wx_plugin::IOvrAudioDecoder*& a_audio_decoder) override;

		virtual wx_plugin::wxIMediaInfo*	CreateMediaInfo() override;
		virtual void						DestoryMediaInfo(wx_plugin::wxIMediaInfo*& a_info) override;

		virtual wx_plugin::IOvrAVEncoder*CreateAVEncoder() override;
		virtual void					DestoryAVEncoder(wx_plugin::IOvrAVEncoder*& a_encoder) override;
	};
}

#if defined(__cplusplus)
extern "C"
{
#endif
	WX_AV_MEDIA_API  wx_plugin::wxIPlugin* StartPlugin();
	WX_AV_MEDIA_API  void StopPlugin();
#if defined(__cplusplus)
}
#endif

#endif
