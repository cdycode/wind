﻿#include "headers.h"
#include "wx_audio_processing.h"
#include "wx_image_convert_context.h"

namespace wx_av_media 
{
wxAudioProcessing::wxAudioProcessing(void)
:audio_resample_ctx(NULL)
, in_sample_rate(0)
, in_channels(0)
, in_channel_layout(AV_CH_LAYOUT_STEREO)
, in_sample_format(AV_SAMPLE_FMT_NONE)
, out_sample_rate(0)
, out_channels(0)
, out_sample_format(AV_SAMPLE_FMT_NONE)
, out_block_align(0)
{}
wxAudioProcessing::~wxAudioProcessing(void)
{
	Close();
}

bool wxAudioProcessing::Open(int a_audio_channels, int64_t a_in_channel_layout, AVSampleFormat a_sample_format, int a_in_audio_sample_rates)
{
	in_sample_rate		 = a_in_audio_sample_rates;
	in_channels		 = a_audio_channels;
	in_channel_layout = a_in_channel_layout;
	in_sample_format	 = a_sample_format;
	return true;
}

void wxAudioProcessing::Close(void)
{
	if (NULL != audio_resample_ctx)
	{
		swr_close(audio_resample_ctx);
		swr_free(&audio_resample_ctx);
		audio_resample_ctx = NULL;
	}
}

int wxAudioProcessing::SetOutputFmt(int a_out_audio_channels, int a_aut_audio_sample_bits, int a_out_audio_sample_rates)
{
	if (NULL != audio_resample_ctx)
	{
		Close();
	}

	if (a_out_audio_channels < 1 || a_out_audio_channels > 7)
	{
		return 2;
	}

	out_sample_rate	= a_out_audio_sample_rates;
	out_channels		= a_out_audio_channels;

	//决定传出去的音频格式 都是采用声道交错存放的方式
	out_sample_format = wxMediaTypeConversion::GetAudioSampleFormat(a_aut_audio_sample_bits/8);

	if (in_channels == out_channels && in_sample_format == out_sample_format && in_sample_rate == out_sample_rate)
	{
		return 0;
	}

	int64_t i64OutChannelLayout = in_channel_layout;
	if (in_channels != out_channels)
	{
		switch (out_channels)
		{
		case 1:
			i64OutChannelLayout = AV_CH_LAYOUT_MONO;
			break;
		case 2:
			i64OutChannelLayout = AV_CH_LAYOUT_STEREO;
			break;
		case 3:
			i64OutChannelLayout = AV_CH_LAYOUT_2_1;
			break;
		case 4:
			i64OutChannelLayout = AV_CH_LAYOUT_2_2;
			break;
		case 5:
			i64OutChannelLayout = AV_CH_LAYOUT_5POINT0;
			break;
		case 6:
			i64OutChannelLayout = AV_CH_LAYOUT_6POINT0;
			break;
		case 7:
			i64OutChannelLayout = AV_CH_LAYOUT_7POINT0;
			break;
		default:
			out_channels		= 2;
			i64OutChannelLayout = AV_CH_LAYOUT_STEREO;
			break;
		}
	}
	

	audio_resample_ctx = swr_alloc_set_opts(NULL, i64OutChannelLayout, out_sample_format, \
		out_sample_rate, in_channel_layout, in_sample_format, in_sample_rate, 0, NULL);

	if (NULL == audio_resample_ctx || swr_init(audio_resample_ctx) < 0)
	{
		Close();
		return 1;
	}

	out_block_align = out_channels* (a_aut_audio_sample_bits / 8);
	return 0;
}

int wxAudioProcessing::ResampleAudio(AVFrame* a_audio_raw_frame, char* a_out_buffer, int a_out_buffer_size)
{
	int iOutSize = 0;
	if (NULL != audio_resample_ctx)
	{
		//int data_size = av_samples_get_buffer_size(NULL, av_frame_get_channels(apAudioRawFrame),
		//	apAudioRawFrame->nb_samples, (AVSampleFormat)apAudioRawFrame->format, 1);

		int out_count = (int64_t)a_audio_raw_frame->nb_samples * out_sample_rate / in_sample_rate + 256;	//修正错误
		int out_size = av_samples_get_buffer_size(NULL, out_channels, out_count, out_sample_format, 0);
		if (out_size > a_out_buffer_size)
		{
			//严重错误
			out_size = a_out_buffer_size;
		}
		int iOutSamples = swr_convert(audio_resample_ctx, (uint8_t**)&a_out_buffer, out_size, (const uint8_t **)a_audio_raw_frame->extended_data, a_audio_raw_frame->nb_samples);
		iOutSize = iOutSamples*out_block_align;
	}
	else
	{
		iOutSize = av_samples_get_buffer_size(NULL, in_channels, a_audio_raw_frame->nb_samples, in_sample_format, 0);
		if (iOutSize > a_out_buffer_size)
		{
			//严重错误
			iOutSize = a_out_buffer_size;
		}
		memcpy(a_out_buffer, a_audio_raw_frame->data[0], iOutSize);
	}
	return iOutSize;
}

}

