﻿#ifndef WX_AV_MEDIA_WX_MINI_AUDIO_DECODER_H_
#define WX_AV_MEDIA_WX_MINI_AUDIO_DECODER_H_

#include "wx_frame_buffer.h"

namespace wx_av_media 
{
class wxAudioProcessing;
class  wxMiniAudioDecoder
{
public:
	wxMiniAudioDecoder();
	~wxMiniAudioDecoder(void);

	int	Open(AVCodecContext* a_codec_context);
	void Close(void);

	int SetAudioOutFormat(int a_channels, int a_sample_bits, int a_sample_rate, int a_frame_ms);

	wx_plugin::wxFrameBuffer* PopFrame();
	void			PushFrame(wx_plugin::wxFrameBuffer* a_frame_buffer);

	// add new function
	bool	DecodePackage(AVPacket* a_packet);
	void	Seek();
	void	NotifyFrame() { frame_buffer_list.Close(); }
private:

	bool DecodeOneAudioFrame(AVPacket* a_packet);
	inline void DealOneFrame();

	bool	InitBuffer(int a_buffer_size);
	void	UninitBuffer();

private:
	bool				is_set_out_format;

	AVCodecContext*		audio_codec_ctx;
	AVFrame*			audio_frame;

	wx_plugin::wxFrameBuffer*				convert_buffer;
	
	AVRational          pts_per_byte;

	wxAudioProcessing*			audio_processing;

	wxBlockDoubleQueue<wx_plugin::wxFrameBuffer>		frame_buffer_list;
	wx_plugin::wxFrameBuffer*								current_audio_frame = nullptr;

	int64_t next_pts;
	AVRational next_pts_tb;
};

}

#endif