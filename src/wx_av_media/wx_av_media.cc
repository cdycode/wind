﻿#include "headers.h"
#include "wx_av_media.h"
#include "wx_audio_decoder.h"
#include "wx_video_decoder.h"
#include "wx_av_media_info.h"
#include "wx_av_encoder.h"
namespace wx_av_media 
{
	bool wxAVMedia::Init() 
	{
		avcodec_register_all();
		av_register_all();
		avformat_network_init();
		return true;
	}

	void wxAVMedia::Uninit() 
	{
	}
	
	wx_plugin::wxIVideoDecoder* wxAVMedia::CreateVideoDecoder()
	{
		return new(std::nothrow) wxVideoDecoder;
	}
	void wxAVMedia::DestoryVideoDecoder(wx_plugin::wxIVideoDecoder*& a_video_decoder)
	{
		if (nullptr != a_video_decoder)
		{
			delete a_video_decoder;
			a_video_decoder = nullptr;
		}
	}
	wx_plugin::IOvrAudioDecoder* wxAVMedia::CreateAudioDecoder()
	{
		return new(std::nothrow) wxAudioDecoder;
	}
	void wxAVMedia::DestoryAudioDecoder(wx_plugin::IOvrAudioDecoder*& a_audio_decoder)
	{
		if (nullptr != a_audio_decoder)
		{
			delete a_audio_decoder;
			a_audio_decoder = nullptr;
		}
	}

	wx_plugin::wxIMediaInfo* wxAVMedia::CreateMediaInfo()
	{
		return new wxMediaInfo;
	}
	void wxAVMedia::DestoryMediaInfo(wx_plugin::wxIMediaInfo*& a_info)
	{
		if (nullptr != a_info)
		{
			delete a_info;
			a_info = nullptr;
		}
	}

	wx_plugin::IOvrAVEncoder* wxAVMedia::CreateAVEncoder()
	{
		return new(std::nothrow) wxAVEncoder();
	}
	void wxAVMedia::DestoryAVEncoder(wx_plugin::IOvrAVEncoder*& a_encoder)
	{
		if (a_encoder != nullptr)
		{
			delete a_encoder;
			a_encoder = nullptr;
		}
	}	
}

static wx_av_media::wxAVMedia* sg_av_media = nullptr;
WX_AV_MEDIA_API wx_plugin::wxIPlugin* StartPlugin()
{
	if (nullptr == sg_av_media)
	{
		sg_av_media = new wx_av_media::wxAVMedia;
	}
	return sg_av_media;
}

WX_AV_MEDIA_API void StopPlugin()
{
	if (nullptr != sg_av_media)
	{
		sg_av_media->Unload();
		delete sg_av_media;
		sg_av_media = nullptr;
	}
}
