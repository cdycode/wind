﻿#include "headers.h"
#include "wx_mini_audio_decoder.h"
#include "wx_audio_processing.h"
#include "wx_frame_buffer.h"

namespace wx_av_media 
{

wxMiniAudioDecoder::wxMiniAudioDecoder()
:is_set_out_format(false)
, audio_codec_ctx(NULL)
, audio_frame(NULL)
, convert_buffer(NULL)
, audio_processing(NULL)
, next_pts(AV_NOPTS_VALUE)
{
	pts_per_byte.num = 0;
	pts_per_byte.den = 1;
	next_pts_tb.den = 1;
	next_pts_tb.num = 0;
}
wxMiniAudioDecoder::~wxMiniAudioDecoder(void)
{
	Close();

	if (NULL != audio_processing)
	{
		audio_processing->Close();
		delete audio_processing;
		audio_processing = NULL;
	}
}

int	wxMiniAudioDecoder::Open(AVCodecContext* a_codec_context)
{
	Close();

	// Get a pointer to the codec context for the audio stream
	audio_codec_ctx = a_codec_context;

	// Find the decoder for the audio stream
	AVCodec* pAudioCodec = avcodec_find_decoder(audio_codec_ctx->codec_id);
	if (NULL == pAudioCodec)
	{
		return 1;
	}

	// Open codec
	if (avcodec_open2(audio_codec_ctx, pAudioCodec, NULL) < 0)
	{
		return 1;
	}

	// Allocate audio frame
	audio_frame = av_frame_alloc();
	if (NULL == audio_frame)
	{
		return 1;
	}

	if (NULL == audio_processing)
	{
		audio_processing = new(std::nothrow) wxAudioProcessing;
		if (NULL == audio_processing)
		{
			return 1;
		}
	}

	audio_processing->Open(audio_codec_ctx->channels, audio_codec_ctx->channel_layout, audio_codec_ctx->sample_fmt, audio_codec_ctx->sample_rate);

	return 0;
}
void wxMiniAudioDecoder::Close(void)
{
	is_set_out_format = false;

	if (NULL != audio_codec_ctx)
	{
		avcodec_close(audio_codec_ctx);
		audio_codec_ctx = NULL;
	}
	if (NULL != audio_frame)
	{
		av_frame_free(&audio_frame);
		audio_frame = NULL;
	}
	if (NULL != audio_processing)
	{
		audio_processing->Close();
	}
	if (NULL != convert_buffer)
	{
		convert_buffer->Close();
		delete convert_buffer;
		convert_buffer = NULL;
	}

	frame_buffer_list.Close();
	UninitBuffer();
}

int wxMiniAudioDecoder::SetAudioOutFormat(int a_channels, int a_sample_bits, int a_sample_rate, int a_frame_ms)
{
	if (NULL == audio_codec_ctx)
	{
		return 3;
	}
	if (NULL == audio_processing)
	{
		return 3;
	}

	if (is_set_out_format)
	{
		return 0;
	}
	is_set_out_format = true;

	// 与视频同帧率时一帧音频数据的大小
	int iBufferSize = ((a_sample_rate*(a_channels * (a_sample_bits / 8))) / 4);

	//分配音频转换用的中间Buffer
	convert_buffer = new(std::nothrow) wx_plugin::wxFrameBuffer;
	if (NULL == convert_buffer)
	{
		return 1;
	}
	if (!convert_buffer->Open(iBufferSize * 3 / 2))
	{
		return 1;
	}

	pts_per_byte.num = 1000;
	pts_per_byte.den = (a_channels * a_sample_rate * a_sample_bits) / 8;

	iBufferSize = (a_sample_rate  * a_frame_ms * a_channels * (a_sample_bits/8)) / 1000;
	if (!InitBuffer(iBufferSize))
	{
		return 1;
	}

	return audio_processing->SetOutputFmt(a_channels, a_sample_bits, a_sample_rate);
}

wx_plugin::wxFrameBuffer* wxMiniAudioDecoder::PopFrame()
{
	if (NULL == audio_codec_ctx)
	{
		return nullptr;
	}

	//获取结果Buffer
	return frame_buffer_list.PopResultFrontNoBlock();
}

void wxMiniAudioDecoder::PushFrame(wx_plugin::wxFrameBuffer* a_frame_buffer)
{
	if (nullptr != a_frame_buffer)
	{
		frame_buffer_list.PushFreeBack(a_frame_buffer);
	}
}

bool wxMiniAudioDecoder::DecodePackage(AVPacket* a_packet) 
{
	bool is_one_frame = false;
	if (nullptr == current_audio_frame)
	{
		current_audio_frame = frame_buffer_list.PopFreeFront();
		if (nullptr == current_audio_frame)
		{
			return false;
		}
		current_audio_frame->data_size = 0;
	}

	//解码
	int iFinished = 0;
	if (avcodec_decode_audio4(audio_codec_ctx, audio_frame, &iFinished, a_packet) < 0 || iFinished == 0)
	{
		return false;
	}

	//格式转换
	DealOneFrame();

	int copy_size = 0;
	while (copy_size < convert_buffer->data_size)
	{
		//如果是新的current_audio_frame 需要更新其pts
		if (current_audio_frame->data_size == 0)
		{
			current_audio_frame->pts = convert_buffer->pts + copy_size * pts_per_byte.num / pts_per_byte.den;
		}

		int need_size = current_audio_frame->buffer_size - current_audio_frame->data_size;
		int left_size = convert_buffer->data_size - copy_size;
		if (need_size <= left_size)
		{
			//能填满current_audio_frame
			memcpy(current_audio_frame->buffer + current_audio_frame->data_size, convert_buffer->buffer + copy_size, need_size);
			current_audio_frame->data_size += need_size;
			copy_size += need_size;

			//将填满的Buffer放到结果队列中
			frame_buffer_list.PushResultBack(current_audio_frame);

			//再弹出一个空白的frame
			current_audio_frame = frame_buffer_list.PopFreeFront();
			if (nullptr == current_audio_frame)
			{
				return false;
			}
			current_audio_frame->data_size = 0;

			is_one_frame = true;
		}
		else 
		{
			memcpy(current_audio_frame->buffer + current_audio_frame->data_size, convert_buffer->buffer + copy_size, left_size);
			current_audio_frame->data_size += left_size;
			copy_size += left_size;
		}
	}

	return is_one_frame;
}

void wxMiniAudioDecoder::Seek() 
{
	avcodec_flush_buffers(audio_codec_ctx);
	if (nullptr != current_audio_frame)
	{
		frame_buffer_list.PushFreeBack(current_audio_frame);
		current_audio_frame = nullptr;
	}
}

inline void wxMiniAudioDecoder::DealOneFrame()
{
	// audio resampling 
	convert_buffer->data_size = audio_processing->ResampleAudio(audio_frame, convert_buffer->buffer, convert_buffer->buffer_size);

	AVRational tb;
	tb.num = 1;
	tb.den = audio_frame->sample_rate;

	if (audio_frame->pts != AV_NOPTS_VALUE)
	{
		convert_buffer->pts = (long long)(audio_frame->pts * (1000 * av_q2d(audio_codec_ctx->time_base)));
		audio_frame->pts = av_rescale_q(audio_frame->pts, audio_codec_ctx->time_base, tb);
	}
	else if (audio_frame->pkt_pts != AV_NOPTS_VALUE)
	{
		convert_buffer->pts = (long long)(audio_frame->pkt_pts * (1000 * av_q2d(av_codec_get_pkt_timebase(audio_codec_ctx))));
		audio_frame->pts = av_rescale_q(audio_frame->pkt_pts, av_codec_get_pkt_timebase(audio_codec_ctx), tb);
	}
	else if (next_pts != AV_NOPTS_VALUE)
	{
		convert_buffer->pts = (long long)(next_pts * (1000 * av_q2d(next_pts_tb)));
		audio_frame->pts = av_rescale_q(next_pts, next_pts_tb, tb);
	}
	if (audio_frame->pts != AV_NOPTS_VALUE)
	{
		next_pts = audio_frame->pts + audio_frame->nb_samples;
		next_pts_tb = tb;
	}
}

bool wxMiniAudioDecoder::InitBuffer(int a_buffer_size)
{
	UninitBuffer();

	current_audio_frame = nullptr;

	for (int i = 0; i < DEF_LING_VIDEO_BUFFER_CNT; i++)
	{
		wx_plugin::wxFrameBuffer* new_buffer = new wx_plugin::wxFrameBuffer;
		if (nullptr == new_buffer)
		{
			continue;
		}
		if (new_buffer->Open(a_buffer_size))
		{
			frame_buffer_list.PushFreeBack(new_buffer);
		}
		else
		{
			delete new_buffer;
		}
	}

	frame_buffer_list.Open();
	return frame_buffer_list.GetFreeListSize() > 0;
}

void wxMiniAudioDecoder::UninitBuffer()
{
	if (nullptr != current_audio_frame)
	{
		frame_buffer_list.PushFreeBack(current_audio_frame);
		current_audio_frame = nullptr;
	}
	while (frame_buffer_list.GetFreeListSize() > 0)
	{
		wx_plugin::wxFrameBuffer* frame_buffer = frame_buffer_list.PopFreeFrontNoBlock();
		if (nullptr != frame_buffer)
		{
			frame_buffer->Close();
			delete frame_buffer;
		}
	}
	while (frame_buffer_list.GetResultListSize() > 0)
	{
		wx_plugin::wxFrameBuffer* frame_buffer = frame_buffer_list.PopResultFrontNoBlock();
		if (nullptr != frame_buffer)
		{
			frame_buffer->Close();
			delete frame_buffer;
		}
	}
}

}

