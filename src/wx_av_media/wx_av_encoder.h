#ifndef WX_AV_MEDIA_WX_AV_ENCODER_H__
#define WX_AV_MEDIA_WX_AV_ENCODER_H__
#include "headers.h"
#include "wx_av_media.h"
namespace wx_av_media
{
	class wxAVEncoder :public wx_plugin::IOvrAVEncoder
	{
	public:
		wxAVEncoder() {}
		virtual ~wxAVEncoder(){}


		virtual bool	Open(const OvrString& a_out_file_path)override;
		virtual void	Close()override;

		virtual void	SetVideoFormat(wx_plugin::ENU_VIDEO_PIXEL_FORMAT a_pixel_format, ovr::uint32 a_frame_width, ovr::uint32 a_frame_height, ovr::uint32 a_frame_rate)override;
		virtual void	SetAudioFormat(ovr::uint32 a_channel_num, ovr::uint32 a_sample_rate, ovr::uint32 a_sample_bits)override;

		virtual bool	StartEncoding()override;
		virtual void	EndEncoding()override;

		virtual bool	PushVideoFrame(const char* a_raw_data, ovr::uint32 a_data_size)override;
		virtual bool	PushAudioFrame(const char* a_raw_data, ovr::uint32 a_data_size)override;
	private:
		
		typedef struct OutputStream
		{
			AVStream *av_stream;
			
			int64_t next_pts;
			int samples_count;

			AVFrame *av_frame;
			AVFrame *av_tmp_frame;

			float t, tincr, tincr2;

			struct SwsContext *sws_ctx;
			struct SwrContext *swr_ctx;
		} OutputStream;
		void AddStream(OutputStream *a_ost, AVFormatContext *a_oc, AVCodec **a_codec, enum AVCodecID a_codec_id, ovr::uint32 a_parm1, ovr::uint32 a_parm2, ovr::uint32 a_parm3, ovr::uint32 a_parm4);
		void OpenVideo(AVFormatContext *a_oc, AVCodec *a_codec, OutputStream *a_ost, AVDictionary *a_opt_arg);
		void OpenAudio(AVFormatContext *a_oc, AVCodec *a_codec, OutputStream *a_ost, AVDictionary *a_opt_arg);
		AVFrame* GetVideoFrame(const char* a_raw_data, ovr::uint32 a_data_size);
		AVFrame* GetAudioFrame(const char* a_raw_data, ovr::uint32 a_data_size);
		int  WriteFrame(AVFormatContext *a_fmt_ctx, const AVRational *a_time_base, AVStream *a_st, AVPacket *a_pkt);
		AVFrame *AllocAudioFrame(enum AVSampleFormat a_sample_fmt, uint64_t a_channel_layout, int a_sample_rate, int a_nb_samples);
		AVFrame *AllocVideoFrame(enum AVPixelFormat a_pix_fmt, int a_width, int a_height);
		bool	RGB24ToYUV420(const char* a_raw_data, ovr::uint32 a_data_size);
		void    CloseStream(AVFormatContext *a_oc, OutputStream *a_ost);
	private:
		AVFormatContext *format_context = NULL;
		AVOutputFormat  *output_format = NULL;
		AVCodec			*video_codec = NULL;
		AVCodec			*audio_codec = NULL;
		OutputStream    *video_stream = NULL;
		OutputStream    *audio_stream = NULL;
		int is_have_video = 0, is_have_audio = 0;
		int is_encode_video = 0, is_encode_audio = 0;
		OvrString   output_file;
		
	};
}
#endif
