#include "headers.h"
#include "wx_av_encoder.h"
namespace wx_av_media
{
	bool wxAVEncoder::Open(const OvrString& a_out_file_path)
	{
		AVDictionary* opt = NULL;
		video_stream = new OutputStream();
		audio_stream = new OutputStream();
		*video_stream = { 0 };
		*audio_stream = { 0 };
		int ret = 0;
		output_file = a_out_file_path;
		//m_OutPutFile = "D:\\my_video.avi";
		//sprintf_s(m_FilePath, "%s", "D:\\my_video.mp4");
		//av_register_all();
		avformat_alloc_output_context2(&format_context, NULL, NULL, output_file.GetStringConst());
		if (!format_context)
		{
			printf("Could not deduce output format from file extension: using MPEG.\n");
			avformat_alloc_output_context2(&format_context, NULL, "mpeg", output_file.GetStringConst());
		}
		
		if (!format_context)
			return false;

		output_format = format_context->oformat;
		return true;
	}
	void wxAVEncoder::Close()
	{

	}

	void wxAVEncoder::SetVideoFormat(wx_plugin::ENU_VIDEO_PIXEL_FORMAT a_pixel_format, ovr::uint32 a_frame_width, ovr::uint32 a_frame_height, ovr::uint32 a_frame_rate)
	{
		if (output_format->video_codec != AV_CODEC_ID_NONE)
		{
			AddStream(video_stream, format_context, &video_codec, output_format->video_codec,(ovr::uint32)a_pixel_format,a_frame_width,a_frame_height,a_frame_rate);
			is_have_video = 1;
			is_encode_video = 1;
		}
	}
	void wxAVEncoder::SetAudioFormat(ovr::uint32 a_channel_num, ovr::uint32 a_sample_rate, ovr::uint32 a_sample_bits)
	{
		if(output_format->audio_codec != AV_CODEC_ID_NONE)
		{
			AddStream(audio_stream, format_context, &audio_codec, output_format->audio_codec,a_channel_num,a_sample_rate,a_sample_bits,0);
			is_have_audio = 1;
			is_encode_audio = 1;
		}
	}

	bool wxAVEncoder::StartEncoding()
	{
		if (is_have_video)
		{
			OpenVideo(format_context, video_codec, video_stream, NULL);
		}
		if (is_have_audio)
		{
			OpenAudio(format_context, audio_codec, audio_stream, NULL);
		}

		int ret = 0;
		av_dump_format(format_context, 0, output_file.GetStringConst(), 1);
		if (!(output_format->flags & AVFMT_NOFILE))
		{
			ret = avio_open(&format_context->pb, output_file.GetStringConst(), AVIO_FLAG_WRITE);
			if (ret < 0)
			{
				//fprintf(stderr, "Could not open '%s': %s\n", a_out_file_path.GetStringConst(),"");
				//fprintf(stderr, "Could not open audio codec: %s\n", av_err2str(ret));
				return false;
			}
		}
		//Write header
		ret = avformat_write_header(format_context, nullptr);
		if (ret < 0)
		{
			char temp[64] = { 0 };
			fprintf(stderr, "Error occurred when opening output file: %s\n", av_make_error_string(temp, AV_ERROR_MAX_STRING_SIZE, ret));
			return false;
		}
		return true;
	}
	void wxAVEncoder::EndEncoding()
	{
		av_write_trailer(format_context);

		/* Close each codec. */
		if (is_have_video)
			CloseStream(format_context, video_stream);
		if (is_have_audio)
			CloseStream(format_context, audio_stream);

		if (!(output_format->flags & AVFMT_NOFILE))
			/* Close the output file. */
			avio_closep(&format_context->pb);

		/* free the stream */
		avformat_free_context(format_context);

		if (video_stream)
		{
			delete video_stream;
			video_stream = nullptr;
		}
		if (audio_stream)
		{
			delete audio_stream;
			audio_stream = nullptr;
		}
	}

	bool wxAVEncoder::PushVideoFrame(const char* a_raw_data, ovr::uint32 a_data_size)
	{
		int ret;
		AVCodecContext *c;
		AVFrame *frame;
		int got_packet = 0;
		AVPacket pkt = { 0 };

		c = video_stream->av_stream->codec;

		frame = GetVideoFrame(a_raw_data,a_data_size);

		if (frame == NULL)
			return false;
		av_init_packet(&pkt);
		if (format_context->oformat->flags & AVFMT_RAWPICTURE) 
		{
			/* a hack to avoid data copy with some raw video muxers */
			AVPacket pkt;
			av_init_packet(&pkt);

			if (!frame)
				return 1;

			pkt.flags |= AV_PKT_FLAG_KEY;
			pkt.stream_index = video_stream->av_stream->index;
			pkt.data = (uint8_t *)frame;
			pkt.size = sizeof(AVPicture);

			pkt.pts = pkt.dts = frame->pts;
			av_packet_rescale_ts(&pkt, c->time_base, video_stream->av_stream->time_base);

			ret = av_interleaved_write_frame(format_context, &pkt);
		}
		else 
		{
			ret = avcodec_encode_video2(c, &pkt, frame, &got_packet);
			if (ret < 0)
			{
				//fprintf(stderr, "Error encoding video frame: %s\n", av_err2str(ret));/Wv:18
				exit(1);
			}

			if (got_packet)
			{
				ret = WriteFrame(format_context, &c->time_base, video_stream->av_stream, &pkt);
			}
			else
			{
				ret = 0;
			}
		}

		

		if (ret < 0) 
		{
			//fprintf(stderr, "Error while writing video frame: %s\n", av_err2str(ret));
			return false;
		}

		return (frame || got_packet) ? 0 : 1;
	}
	bool wxAVEncoder::PushAudioFrame(const char* a_raw_data, ovr::uint32 a_data_size)
	{
		AVCodecContext *c;
		AVPacket pkt = { 0 }; // data and size must be 0;
		AVFrame *frame;
		int ret;
		int got_packet;
		int dst_nb_samples;

		av_init_packet(&pkt);
		c = audio_stream->av_stream->codec;

		frame = GetAudioFrame(a_raw_data, a_data_size);

		if (frame)
		{
			/* convert samples from native format to destination codec format, using the resampler */
			/* compute destination number of samples */
			dst_nb_samples = (int)av_rescale_rnd(swr_get_delay(audio_stream->swr_ctx, c->sample_rate) + frame->nb_samples,c->sample_rate, c->sample_rate, AV_ROUND_UP);
			av_assert0(dst_nb_samples == frame->nb_samples);

			/* when we pass a frame to the encoder, it may keep a reference to it
			* internally;
			* make sure we do not overwrite it here
			*/
			ret = av_frame_make_writable(audio_stream->av_frame);
			if (ret < 0)
				exit(1);

			/* convert to destination format */
			ret = swr_convert(audio_stream->swr_ctx,audio_stream->av_frame->data, dst_nb_samples,(const uint8_t **)frame->data, frame->nb_samples);
			if (ret < 0) 
			{
				fprintf(stderr, "Error while converting\n");
				exit(1);
			}
			frame = audio_stream->av_frame;

			frame->pts = av_rescale_q(audio_stream->samples_count, av_make_q( 1, c->sample_rate ), c->time_base);
			audio_stream->samples_count += dst_nb_samples;
		}
		else
			return false;
		ret = avcodec_encode_audio2(c, &pkt, frame, &got_packet);
		if (ret < 0)
		{
			//fprintf(stderr, "Error encoding audio frame: %s\n", av_err2str(ret));
			exit(1);
		}

		if (got_packet)
		{
			ret = WriteFrame(format_context, &c->time_base, audio_stream->av_stream, &pkt);
			if (ret < 0)
			{
				//fprintf(stderr, "Error while writing audio frame: %s\n",av_err2str(ret));
				exit(1);
			}
		}

		return (frame || got_packet) ? 0 : 1;
		
		return true;
	}

	void wxAVEncoder::AddStream(OutputStream *a_ost, AVFormatContext *a_oc, AVCodec **a_codec, enum AVCodecID a_codec_id, ovr::uint32 a_parm1, ovr::uint32 a_parm2, ovr::uint32 a_parm3, ovr::uint32 a_parm4)
	{
		AVCodecContext *codec_context;
		int i;

		*a_codec = avcodec_find_encoder(a_codec_id);
		if (!(*a_codec)) 
		{
			fprintf(stderr, "Could not find encoder for '%s'\n",avcodec_get_name(a_codec_id));
			exit(1);
		}

		a_ost->av_stream = avformat_new_stream(a_oc, *a_codec);
		if (!a_ost->av_stream) 
		{
			fprintf(stderr, "Could not allocate stream\n");
			exit(1);
		}

		a_ost->av_stream->id = a_oc->nb_streams - 1;
		codec_context = a_ost->av_stream->codec;
		if (!codec_context) 
		{
			fprintf(stderr, "Could not alloc an encoding context\n");
			exit(1);
		}
		
		
		switch ((*a_codec)->type)
		{
		case AVMEDIA_TYPE_AUDIO:
			//ovr::uint32 a_channel_num, ovr::uint32 a_sample_rate, ovr::uint32 a_sample_bits
			codec_context->sample_fmt = (*a_codec)->sample_fmts ? (*a_codec)->sample_fmts[0] : AV_SAMPLE_FMT_FLTP;
			codec_context->bit_rate = 64000;
			codec_context->sample_rate = 44100;
			if ((*a_codec)->supported_samplerates)
			{
				codec_context->sample_rate = (*a_codec)->supported_samplerates[0];
				for (i = 0; (*a_codec)->supported_samplerates[i]; i++)
				{
					if ((*a_codec)->supported_samplerates[i] == 44100)
						codec_context->sample_rate = 44100;
				}
			}
			codec_context->channels = av_get_channel_layout_nb_channels(codec_context->channel_layout);
			codec_context->channel_layout = AV_CH_LAYOUT_STEREO;
			if ((*a_codec)->channel_layouts)
			{
				codec_context->channel_layout = (*a_codec)->channel_layouts[0];
				for (i = 0; (*a_codec)->channel_layouts[i]; i++)
				{
					if ((*a_codec)->channel_layouts[i] == AV_CH_LAYOUT_STEREO)
						codec_context->channel_layout = AV_CH_LAYOUT_STEREO;
				}
			}
			codec_context->channels = av_get_channel_layout_nb_channels(codec_context->channel_layout);
			a_ost->av_stream->time_base = av_make_q( 1, codec_context->sample_rate);
			break;

		case AVMEDIA_TYPE_VIDEO:
			codec_context->codec_id = a_codec_id;
			
			codec_context->bit_rate = 400000;
			/* Resolution must be a multiple of two. */
			codec_context->width = a_parm2;
			codec_context->height = a_parm3;
			/* timebase: This is the fundamental unit of time (in seconds) in terms
			* of which frame timestamps are represented. For fixed-fps content,
			* timebase should be 1/framerate and timestamp increments should be
			* identical to 1. */
			a_ost->av_stream->time_base = av_make_q(1, a_parm4);
			codec_context->time_base = a_ost->av_stream->time_base;

			codec_context->gop_size = 12; /* emit one intra frame every twelve frames at most */
			codec_context->pix_fmt = AV_PIX_FMT_YUV420P;
			if (codec_context->codec_id == AV_CODEC_ID_MPEG2VIDEO)
			{
				/* just for testing, we also add B-frames */
				codec_context->max_b_frames = 2;
			}
			if (codec_context->codec_id == AV_CODEC_ID_MPEG1VIDEO)
			{
				codec_context->mb_decision = 2;
			}
			break;

		default:
			break;
		}

		/* Some formats want stream headers to be separate. */
		if (a_oc->oformat->flags & AVFMT_GLOBALHEADER)
			codec_context->flags |= CODEC_FLAG_GLOBAL_HEADER;
		
	}
	void wxAVEncoder::OpenVideo(AVFormatContext *a_oc, AVCodec *a_codec, OutputStream *a_ost, AVDictionary *a_opt_arg)
	{
		int ret;
		AVCodecContext *c = a_ost->av_stream->codec;
		AVDictionary *opt = NULL;

		av_dict_copy(&opt, a_opt_arg, 0);

		ret = avcodec_open2(c, a_codec, &opt);
		av_dict_free(&opt);
		if (ret < 0) 
		{
			//fprintf(stderr, "Could not open video codec: %s\n", av_err2str(ret));
			exit(1);
		}

		/* allocate and init a re-usable frame */
		//ost->av_frame = alloc_picture(c->pix_fmt, c->width, c->height);
		a_ost->av_frame = AllocVideoFrame(c->pix_fmt, c->width, c->height);
		if (!a_ost->av_frame) 
		{
			fprintf(stderr, "Could not allocate video frame\n");
			exit(1);
		}
		//存放rgb数据,转换为YUV后存入av_frame;
		a_ost->av_tmp_frame = AllocVideoFrame(AV_PIX_FMT_RGB24, c->width, c->height);
		if (!a_ost->av_frame)
		{
			fprintf(stderr, "Could not allocate video frame\n");
			exit(1);
		}
	}
	void wxAVEncoder::OpenAudio(AVFormatContext *a_oc, AVCodec *a_codec, OutputStream *a_ost, AVDictionary *a_opt_arg)
	{
		AVCodecContext *c;
		int nb_samples;
		int ret = 0;
		AVDictionary *opt = NULL;

		c = a_ost->av_stream->codec;
		if (a_codec->id == AV_CODEC_ID_AAC)
			c->strict_std_compliance = FF_COMPLIANCE_EXPERIMENTAL;
		/* open it */
		//av_dict_copy(&opt, opt_arg, 0);
		ret = avcodec_open2(c, a_codec, &opt);
		//av_dict_free(&opt);
		if (ret < 0) 
		{
			//fprintf(stderr, "Could not open audio codec: %s\n", av_err2str(ret));
			exit(1);
		}

		/* init signal generator */
		a_ost->t = 0;
		a_ost->tincr = (float)(2 * M_PI * 110.0f / c->sample_rate);
		/* increment frequency by 110 Hz per second */
		a_ost->tincr2 = (float)(2 * M_PI * 110.0 / c->sample_rate / c->sample_rate);

		if (c->codec->capabilities)
			nb_samples = 10000;
		else
			nb_samples = c->frame_size;

		a_ost->av_frame = av_frame_alloc();
		a_ost->av_frame = AllocAudioFrame(c->sample_fmt, c->channel_layout,c->sample_rate, nb_samples);
		a_ost->av_tmp_frame = AllocAudioFrame(AV_SAMPLE_FMT_S16, c->channel_layout,c->sample_rate, nb_samples);
				
		/* create resampler context */
		a_ost->swr_ctx = swr_alloc();
		if (!a_ost->swr_ctx) 
		{
			fprintf(stderr, "Could not allocate resampler context\n");
			exit(1);
		}

		/* set options */
		av_opt_set_int(a_ost->swr_ctx, "in_channel_count", c->channels, 0);
		av_opt_set_int(a_ost->swr_ctx, "in_sample_rate", c->sample_rate, 0);
		av_opt_set_sample_fmt(a_ost->swr_ctx, "in_sample_fmt", AV_SAMPLE_FMT_S16, 0);
		av_opt_set_int(a_ost->swr_ctx, "out_channel_count", c->channels, 0);
		av_opt_set_int(a_ost->swr_ctx, "out_sample_rate", c->sample_rate, 0);
		av_opt_set_sample_fmt(a_ost->swr_ctx, "out_sample_fmt", c->sample_fmt, 0);

		/* initialize the resampling context */
		if ((ret = swr_init(a_ost->swr_ctx)) < 0) 
		{
			fprintf(stderr, "Failed to initialize the resampling context\n");
			exit(1);
		}
	}
	AVFrame* wxAVEncoder::GetVideoFrame(const char* a_raw_data, ovr::uint32 a_data_size)
	{
		if (RGB24ToYUV420(a_raw_data, a_data_size))
		{
			video_stream->av_frame->pts = video_stream->next_pts++;
			return video_stream->av_frame;
		}
			
		return NULL;
	}
	AVFrame* wxAVEncoder::GetAudioFrame(const char* a_raw_data, ovr::uint32 a_data_size)
	{
		AVFrame *frame = audio_stream->av_frame;
		//int j, i, v;
		//int16_t *q = (int16_t*)frame->data[0];

		/* check if we want to generate more frames */
		//if (av_compare_ts(m_audio_stream->next_pts, m_audio_stream->av_stream->time_base,STREAM_DURATION, (AVRational) { 1, 1 }) >= 0)
			//return NULL;

		/*for (j = 0; j < frame->nb_samples; j++)
		{
			v = (int)(sin(ost->t) * 10000);
			for (i = 0; i < ost->enc->channels; i++)
				*q++ = v;
			ost->t += ost->tincr;
			ost->tincr += ost->tincr2;
		}*/
		if ((ovr::int32)a_data_size > frame->nb_samples)
		{

		}
		memcpy(frame->data[0], a_raw_data, min((ovr::int32)a_data_size,frame->nb_samples));
		
		frame->pts = audio_stream->next_pts;
		audio_stream->next_pts += frame->nb_samples;

		return frame;
	}
	int wxAVEncoder::WriteFrame(AVFormatContext *a_fmt_ctx, const AVRational *a_time_base, AVStream *a_st, AVPacket *a_pkt)
	{
		/* rescale output packet timestamp values from codec to stream timebase */
		av_packet_rescale_ts(a_pkt, *a_time_base, a_st->time_base);
		a_pkt->stream_index = a_st->index;

		/* Write the compressed frame to the media file. */
		//log_packet(fmt_ctx, pkt);
		return av_interleaved_write_frame(a_fmt_ctx, a_pkt);
	}
	AVFrame *wxAVEncoder::AllocAudioFrame(enum AVSampleFormat a_sample_fmt,uint64_t a_channel_layout,int a_sample_rate, int a_nb_samples)
	{
		AVFrame *frame = av_frame_alloc();
		int ret;

		if (!frame) 
		{
			fprintf(stderr, "Error allocating an audio frame\n");
			exit(1);
		}

		frame->format = a_sample_fmt;
		frame->channel_layout = a_channel_layout;
		frame->sample_rate = a_sample_rate;
		frame->nb_samples = a_nb_samples;

		if (a_nb_samples) 
		{
			ret = av_frame_get_buffer(frame, 0);
			if (ret < 0) 
			{
				fprintf(stderr, "Error allocating an audio buffer\n");
				exit(1);
			}
		}

		return frame;
	}
	AVFrame *wxAVEncoder::AllocVideoFrame(enum AVPixelFormat a_pix_fmt, int a_width, int a_height)
	{
		AVFrame *picture;
		int ret;

		picture = av_frame_alloc();
		if (!picture)
			return NULL;

		picture->format = a_pix_fmt;
		picture->width = a_width;
		picture->height = a_height;

		/* allocate the buffers for the frame data */
		ret = av_frame_get_buffer(picture, 32);
		if (ret < 0) 
		{
			fprintf(stderr, "Could not allocate frame data.\n");
			return NULL;
		}

		return picture;
	}
	bool	wxAVEncoder::RGB24ToYUV420(const char* a_raw_data, ovr::uint32 a_data_size)
	{
		int ret = 0;
		int width = 0, height = 0;
		width = video_stream->av_frame->width;
		height = video_stream->av_frame->height;
		av_frame_make_writable(video_stream->av_tmp_frame);
		//此处该加一个判断 width*height*3 == a_data_size??;
		memcpy(video_stream->av_tmp_frame->data[0], a_raw_data, a_data_size);

		video_stream->sws_ctx = sws_getContext(width, height, AV_PIX_FMT_RGB24,width, height, AV_PIX_FMT_YUV420P,SWS_BICUBIC, NULL, NULL, NULL);  //上下文  
		if (!video_stream->sws_ctx)
		{
			fprintf(stderr, "Can't get swsContext\n");
			exit(1);
		}
		ret = sws_scale(video_stream->sws_ctx, video_stream->av_tmp_frame->data, video_stream->av_tmp_frame->linesize,
			0, height, video_stream->av_frame->data, video_stream->av_frame->linesize);
		return true;
	}
	void wxAVEncoder::CloseStream(AVFormatContext *a_oc, OutputStream *a_ost)
	{
		//avcodec_free_context(&ost->av_codec_context);
		av_frame_free(&a_ost->av_frame);
		av_frame_free(&a_ost->av_tmp_frame);
		sws_freeContext(a_ost->sws_ctx);
		swr_free(&a_ost->swr_ctx);
	}
}
