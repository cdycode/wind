﻿#ifndef WX_AV_MEDIA_WX_AUDIO_PROCESSING_H_
#define WX_AV_MEDIA_WX_AUDIO_PROCESSING_H_

namespace wx_av_media 
{

class wxAudioProcessing
{
public:
	wxAudioProcessing(void);
	~wxAudioProcessing(void);

	bool Open(int a_in_audio_channels, int64_t a_in_channel_layout, AVSampleFormat a_sample_format, int a_in_audio_sample_rates);
	void Close(void);

	int SetOutputFmt(int a_out_audio_channels, int a_out_audio_sample_bits, int a_out_audio_sample_rates);

	//返回数据大小
	int ResampleAudio(AVFrame* a_audio_raw_frame, char* a_out_buffer, int a_out_buffer_size);

private:
	SwrContext*		audio_resample_ctx;

	int				in_sample_rate;
	int				in_channels;
	int64_t			in_channel_layout;
	AVSampleFormat	in_sample_format;

	int				out_sample_rate;
	int				out_channels;
	AVSampleFormat	out_sample_format;
	int				out_block_align;
};
}
#endif