﻿#include "headers.h"
#include "wx_video_processing.h"
#include "wx_image_convert_context.h"

namespace wx_av_media
{

	wxVideoProcessing::wxVideoProcessing(void)
		: input_pixel_format(AV_PIX_FMT_NONE)
		, width(0)
		, height(0)
		, out_width(0)
		, out_height(0)
		, out_pixel_type(wx_plugin::kPixelUnknown)
		, new_play_mode(ENU_VIDEO_PLAY_ORIGINAL)
		, new_count(0)
		, current_mode(ENU_VIDEO_PLAY_ORIGINAL)
		, current_count(0)
		, image_convert_ctx(NULL)
		, convert_width(0)
		, convert_height(0)
		, half_black_size(0)
		, convert_pitch(0)
	{
		memset(&av_picture, 0, sizeof(AVPicture));
	}

	wxVideoProcessing::~wxVideoProcessing(void)
	{
		Close();
	}

	void wxVideoProcessing::Open(AVPixelFormat a_input_format, int a_input_width, int a_input_height)
	{
		input_pixel_format = a_input_format;
		width = a_input_width;
		height = a_input_height;
	}

	void wxVideoProcessing::Close(void)
	{
		CloseImageConvertContext();

		input_pixel_format = AV_PIX_FMT_NONE;
		width = 0;
		height = 0;
		out_width = 0;
		out_height = 0;
		out_pixel_type = wx_plugin::kPixelUnknown;
		new_play_mode = ENU_VIDEO_PLAY_ORIGINAL;
		new_count = 0;
		current_mode = ENU_VIDEO_PLAY_ORIGINAL;
		current_count = 0;
		image_convert_ctx = NULL;
		convert_width = 0;
		convert_height = 0;
		half_black_size = 0;
		convert_pitch = 0;
	}

	int wxVideoProcessing::SetVideoOutFormat(int a_width, int a_height, wx_plugin::ENU_VIDEO_PIXEL_FORMAT a_pixel_format)
	{
		CloseImageConvertContext();

		out_width = a_width;
		out_height = a_height;
		out_pixel_type = a_pixel_format;

		convert_width = a_width;
		convert_height = a_height;

		AVPixelFormat outPixfmt = wxMediaTypeConversion::ConvertVideoPixelFormat(out_pixel_type);
		if (out_height == height && out_width == width && input_pixel_format == outPixfmt)
		{
			return 0;
		}

		if (ResetImageConvertCtx())
		{
			return 0;
		}

		return 1;
	}

	//设置3D模式
	int wxVideoProcessing::SetVideoPlayMode(ENU_VIDEO_PLAY_MODE a_play_mode)
	{
		if (a_play_mode > ENU_VIDEO_PLAY_UNKNOWN && a_play_mode <= ENU_VIDEO_PLAY_3D_TB)
		{
			new_play_mode = a_play_mode;
			new_count++;
			return 0;
		}
		return 2;
	}

	int wxVideoProcessing::Processing(AVFrame* a_frame, char* a_buffer, int a_buffer_size)
	{
		//初始化
		char* pTempBuffer = a_buffer;
		int iDataSize = 0;

		//判断是否进行播放模式重置
		if (new_count != current_count)
		{
			if (new_play_mode != current_mode)
			{
				//重置播放模式
				current_mode = new_play_mode;
				InitPlayMode();
			}
			current_count++;
		}

		//数据填充
		if (NULL == image_convert_ctx)
		{
			//不需要转换 直接填充数据
			int iIndex = 0;
			for (; a_frame->linesize[iIndex] != 0; iIndex++)
			{
				int iCopySize = a_frame->linesize[iIndex] * height;
				if (iDataSize + iCopySize <= a_buffer_size)
				{
					iDataSize += iCopySize;
					memcpy(pTempBuffer, a_frame->data[iIndex], iCopySize);
					pTempBuffer += iCopySize;
				}
			}
		}
		else
		{
			//进行数据转换
			image_convert_ctx->ConvertFrame(a_frame, av_picture.data, av_picture.linesize);

			//填充数据	
			if (ENU_VIDEO_PLAY_ORIGINAL == current_mode)
			{
				iDataSize = Pict2Buf_Original(a_buffer, a_buffer_size);
			}
			else if (ENU_VIDEO_PLAY_DOUBLE == current_mode)
			{
				iDataSize = Pict2Buf_Double(a_buffer, a_buffer_size);
			}
			else if (ENU_VIDEO_PLAY_3D_SBS == current_mode)
			{
				iDataSize = Pict2Buf_3D_SBS(a_buffer, a_buffer_size);
			}
			else if (ENU_VIDEO_PLAY_3D_TB == current_mode)
			{
				iDataSize = Pict2Buf_3D_TB(a_buffer, a_buffer_size);
			}
		}

		return iDataSize;
	}

	int wxVideoProcessing::Pict2Buf_Original(char* a_buffer, int a_buffer_size)
	{
		char * pTempBuffer = a_buffer;
		AVPixelFormat outPixfmt = wxMediaTypeConversion::ConvertVideoPixelFormat(out_pixel_type);
		memset(pTempBuffer, 0, a_buffer_size);
		return avpicture_layout(&av_picture, outPixfmt, convert_width, convert_height, (unsigned char*)pTempBuffer, a_buffer_size);
	}

	int wxVideoProcessing::Pict2Buf_Double(char* a_buffer, int a_buffer_size)
	{
		//全部置黑
		memset(a_buffer, 0, a_buffer_size);

		char * pDstBuffer = a_buffer;
		int iDataSize = half_black_size;		//跳过黑数据
		pDstBuffer += half_black_size;

		int iIndex = 0;
		uint8_t *pSrcData = NULL;
		for (; av_picture.linesize[iIndex] != 0; iIndex++)
		{
			pSrcData = av_picture.data[iIndex];
			for (int i = convert_height; i > 0; i--)
			{
				// 左画面
				memcpy(pDstBuffer, pSrcData, convert_pitch);
				pDstBuffer += convert_pitch;

				// 右画面
				memcpy(pDstBuffer, pSrcData, convert_pitch);
				pDstBuffer += convert_pitch;

				iDataSize += 2 * convert_pitch;
				pSrcData += convert_pitch;
			}
		}

		iDataSize += half_black_size;
		return iDataSize;
	}


	int wxVideoProcessing::Pict2Buf_3D_TB(char* a_buffer, int a_buffer_size)
	{
		//全部置黑
		memset(a_buffer, 0, a_buffer_size);

		char * pTempBuffer = a_buffer;
		int iDataSize = 0;

		iDataSize = half_black_size;
		pTempBuffer += half_black_size;

		int iIndex = 0;
		uint8_t* src_l = NULL, *src_r = NULL;
		for (; av_picture.linesize[iIndex] != 0; iIndex++)
		{
			src_l = av_picture.data[iIndex];
			src_r = av_picture.data[iIndex] + wxMediaTypeConversion::GetVideoFrameSize(convert_width, (convert_height / 2), out_pixel_type);
			for (int i = convert_height / 2; i > 0; i--)
			{
				// 左画面
				memcpy(pTempBuffer, src_l, convert_pitch);
				pTempBuffer += convert_pitch;


				// 右画面
				memcpy(pTempBuffer, src_r, convert_pitch);
				pTempBuffer += convert_pitch;

				iDataSize += 2 * convert_pitch;
				src_l += convert_pitch;
				src_r += convert_pitch;
			}
		}

		iDataSize += half_black_size;

		return iDataSize;
	}

	int wxVideoProcessing::Pict2Buf_3D_SBS(char* a_buffer, int a_buffer_size)
	{
		//全部置黑
		memset(a_buffer, 0, a_buffer_size);

		char * pTempBuffer = a_buffer;

		int iDataSize = 0;
		AVPixelFormat outPixfmt = wxMediaTypeConversion::ConvertVideoPixelFormat(out_pixel_type);

		iDataSize = half_black_size;
		pTempBuffer += half_black_size;

		int iCopyedSize = avpicture_layout(&av_picture, outPixfmt, convert_width, convert_height, (unsigned char*)pTempBuffer, a_buffer_size);
		iDataSize += iCopyedSize;

		iDataSize += half_black_size;

		return iDataSize;
	}

	//初始化播放模式
	bool wxVideoProcessing::InitPlayMode(void)
	{
		bool bNeedConvert = true;

		AVPixelFormat outPixfmt = wxMediaTypeConversion::ConvertVideoPixelFormat(out_pixel_type);

		switch (current_mode)
		{
		case ENU_VIDEO_PLAY_ORIGINAL:
			// 原画模式	
			convert_width = out_width;
			convert_height = out_height;
			if (out_height == height && out_width == width && input_pixel_format == outPixfmt)
			{
				bNeedConvert = false;
			}
			break;
		case ENU_VIDEO_PLAY_DOUBLE:	// 双屏模式
			convert_width = out_width / 2;
			convert_height = out_height / 2;
			half_black_size = (int)(out_width * ((out_height - convert_height) / 2) * wxMediaTypeConversion::GetBytesFromePixFmt(out_pixel_type));
			convert_pitch = (int)(convert_width * wxMediaTypeConversion::GetBytesFromePixFmt(out_pixel_type));
			break;
		case ENU_VIDEO_PLAY_3D_SBS:	// 3D左右模式
			convert_width = out_width;
			convert_height = out_height / 2;
			half_black_size = (int)(out_width * ((out_height - convert_height) / 2) * wxMediaTypeConversion::GetBytesFromePixFmt(out_pixel_type));
			break;
		case ENU_VIDEO_PLAY_3D_TB:	// 3D上下格式
			convert_width = out_width / 2;
			convert_height = out_height;
			half_black_size = (int)(out_width * (out_height / 4) * wxMediaTypeConversion::GetBytesFromePixFmt(out_pixel_type));
			convert_pitch = (int)(convert_width * wxMediaTypeConversion::GetBytesFromePixFmt(out_pixel_type));
			break;
		default:
			bNeedConvert = false;
			break;
		}

		if (bNeedConvert)
		{
			return ResetImageConvertCtx();
		}
		else
		{
			CloseImageConvertContext();
		}

		return 0;
	}

	//重置转换Context
	bool wxVideoProcessing::ResetImageConvertCtx(void)
	{
		bool bSuccess = false;
		AVPixelFormat outPixfmt = wxMediaTypeConversion::ConvertVideoPixelFormat(out_pixel_type);

		if (NULL == image_convert_ctx)
		{
			//还没有创建  重新创建
			image_convert_ctx = new wxImageConvertContext();
			if (NULL != image_convert_ctx && image_convert_ctx->Open(input_pixel_format, width, height, out_pixel_type, convert_width, convert_height))
			{
				bSuccess = (0 == avpicture_alloc(&av_picture, outPixfmt, convert_width, convert_height));
			}
		}
		else
		{
			//已经创建 需要重置
			avpicture_free(&av_picture);
			image_convert_ctx->Close();
			if (image_convert_ctx->Open(input_pixel_format, width, height, out_pixel_type, convert_width, convert_height))
			{
				bSuccess = (0 == (avpicture_alloc(&av_picture, outPixfmt, convert_width, convert_height)));
			}
		}
		if (!bSuccess)
		{
			CloseImageConvertContext();
		}
		return bSuccess;
	}

	void wxVideoProcessing::CloseImageConvertContext(void)
	{
		if (NULL != image_convert_ctx)
		{
			image_convert_ctx->Close();
			delete image_convert_ctx;
			image_convert_ctx = NULL;
		}
		if (NULL != av_picture.data[0])
		{
			avpicture_free(&av_picture);
			av_picture.data[0] = NULL;
		}
		memset(&av_picture, 0, sizeof(AVPicture));
	}
}