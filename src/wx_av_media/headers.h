﻿#ifndef WX_AV_MEDIA_HEADERS_H_
#define WX_AV_MEDIA_HEADERS_H_

#include <new>
#include <mutex>
#include <condition_variable>

#ifdef _WIN32
#include <windows.h>
#endif

extern "C"
{
	#include <libavcodec/avcodec.h>
	#include <libavformat/avformat.h>
	#include <libswscale/swscale.h>
	#include <libavutil/imgutils.h>
	#include <libavutil/pixfmt.h>
	#include <libavutil/opt.h>
	#include <libavutil/mathematics.h>
	#include <libswresample/swresample.h>
	#include <libavutil/error.h>
	#include <libavutil/avassert.h>
};

#ifdef _WIN32
#	pragma comment( lib, "avcodec.lib")
#	pragma comment( lib, "avformat.lib")
#	pragma comment( lib, "avutil.lib")
#	pragma comment( lib, "swscale.lib")
#	pragma comment( lib, "swresample.lib")
#endif

#include <wx_base/wx_list.h>
#include <wx_base/wx_time.h>
#include <wx_plugin/wx_i_av_media.h>

#endif
