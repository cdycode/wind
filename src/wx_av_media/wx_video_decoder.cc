﻿#include "headers.h"
#include "wx_video_decoder.h"
#include "wx_mini_video_decoder.h"
#include "wx_image_convert_context.h"
#include <wx_base/wx_time.h>

//定义最大Packet数目  视频和音频总和的Packet数目不超过20个
#define DEF_LING_MIN_FRAMES 20
#define DEF_LING_ERR_SEEKPOS	(-9999)	

namespace wx_av_media
{
	wxVideoDecoder::wxVideoDecoder(void)
		:is_open(false)
		, is_running(false)
		, format_context(NULL)
		, format_opts(NULL)
		, video_stream_index(-1)
		, video_decoder(NULL)
	{
		video_decoder = new(std::nothrow) wxMiniVideoDecoder();
	}

	wxVideoDecoder::~wxVideoDecoder(void)
	{
		Close();
		if (NULL != video_decoder)
		{
			delete video_decoder;
			video_decoder = NULL;
		}
	}

	bool wxVideoDecoder::Open(const char* a_file_path)
	{
		if (is_open)
		{
			return true;
		}
		if (NULL == a_file_path)
		{
			return false;
		}

		if (NULL == video_decoder )
		{
			return false;
		}

		Close();

		//创建Context
		format_context = avformat_alloc_context();

		//寻找数据流
		int scan_all_pmts_set = 0;
		if (!av_dict_get(format_opts, "scan_all_pmts", NULL, AV_DICT_MATCH_CASE))
		{
			av_dict_set(&format_opts, "scan_all_pmts", "1", AV_DICT_DONT_OVERWRITE);
			scan_all_pmts_set = 1;
		}

		// Open video file
		if (0 != avformat_open_input(&format_context, a_file_path, NULL, &format_opts))
		{
			return false;
		}

		if (scan_all_pmts_set)
			av_dict_set(&format_opts, "scan_all_pmts", NULL, AV_DICT_MATCH_CASE);

		AVDictionaryEntry *t;
		if ((t = av_dict_get(format_opts, "", NULL, AV_DICT_IGNORE_SUFFIX)))
		{
			return false;
		}

		// Retrieve stream information
		if (avformat_find_stream_info(format_context, NULL) < 0)
		{
			return false;
		}
		video_stream_index = av_find_best_stream(format_context, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);

		if (0 > video_stream_index)
		{
			return false;
		}

		//打开相应的解码器
		if (video_stream_index >= 0)
		{
			if (0 != video_decoder->Open(format_context->streams[video_stream_index]->codec))
			{
				video_decoder->Close();
			}
			frame_rate.numerator = format_context->streams[video_stream_index]->r_frame_rate.num;
			frame_rate.denominator = format_context->streams[video_stream_index]->r_frame_rate.den;
		}

		is_open = true;
		return is_open;
	}
	void wxVideoDecoder::Close()
	{
		//LOGE("LingFileDecoder::Close");

		is_open = false;
		is_running = false;

		if (nullptr != work_thread)
		{
			video_decoder->NotifyFrame();
			work_thread->join();
			delete work_thread;
			work_thread = nullptr;
		}

		//关闭相应的编解码器
		if (NULL != video_decoder)
		{
			video_decoder->Close();
		}
		//LOGE("LingFileDecoder -- m_pVideoDecoder->Close()");
		
		//LOGE("LingFileDecoder::Close thread exit");

		//关闭文件上下文
		if (NULL != format_context)
		{
			avformat_close_input(&format_context);
			format_context = NULL;
		}
		if (NULL != format_opts)
		{
			av_dict_free(&format_opts);
			format_opts = NULL;
		}
		//重置数据流
		video_stream_index = -1;
		frame_rate.denominator = 1;
		frame_rate.numerator = 0;
	}

	//获取音视频信息 0 获取成功
	bool wxVideoDecoder::GetFileVideoInfo(wx_plugin::StruVideoStreamInfo& a_video_info)
	{
		if (video_stream_index < 0)
		{
			return false;
		}

		AVCodecContext* pContext = format_context->streams[video_stream_index]->codec;

		a_video_info.frame_width = pContext->width;
		a_video_info.frame_height = pContext->height;
		a_video_info.pixel_format = wxMediaTypeConversion::ConvertAVPixelFormat(pContext->pix_fmt);
		a_video_info.frame_rate.numerator = format_context->streams[video_stream_index]->r_frame_rate.num;
		a_video_info.frame_rate.denominator = format_context->streams[video_stream_index]->r_frame_rate.den;
		a_video_info.length = format_context->duration / 1000;
		return true;
	}
	
	bool wxVideoDecoder::SetVideoOutFormat(int a_width, int a_height, wx_plugin::ENU_VIDEO_PIXEL_FORMAT a_pixel_format)
	{
		if (!is_open)
		{
			return false;
		}
		if (is_running)
		{
			return false;
		}
		if (NULL == video_decoder)
		{
			return false;
		}
		return 0 == video_decoder->SetVideoOutFormat(a_width, a_height, a_pixel_format);
	}

	//开始解码
	bool wxVideoDecoder::StartDecoding()
	{
		if (!is_open || video_stream_index < 0)
		{
			return false;
		}

		if (is_running)
		{
			return true;
		}

		is_running = true;

		work_thread = new std::thread(&wxVideoDecoder::ReadPacketThreadFunc, this);
		if (nullptr == work_thread)
		{
			return false;
		}

		return true;
	}

	bool wxVideoDecoder::Seek(long long a_new_pos)
	{
		if (NULL == format_context)
		{
			return false;
		}
		if (a_new_pos < 0 || a_new_pos*1000 >= (format_context->duration))
		{
			return false;
		}
		next_seek_pos = a_new_pos * 1000;
		is_seek = true;
		return true;
	}

	bool wxVideoDecoder::Resume()
	{
		is_seek = false;
		return true;
	}

	wx_plugin::wxFrameBuffer* wxVideoDecoder::PopFrame()
	{
		if (NULL != video_decoder)
		{
			return video_decoder->PopFrame();
		}
		return nullptr;
	}

	void wxVideoDecoder::PushFrame(wx_plugin::wxFrameBuffer* a_frame_buffer)
	{
		if (NULL != video_decoder)
		{
			video_decoder->PushFrame(a_frame_buffer);
		}
	}

	//获取音视频结果Buffer的数目
	int wxVideoDecoder::GetVideoFrameCnt(void)
	{
		if (NULL != video_decoder)
		{
			return video_decoder->GetVideoFrameCnt();
		}
		return 0;
	}

	void wxVideoDecoder::ReadPacketThreadFunc(void)
	{
#ifdef LING_ANDROID_NDK
		pthread_t th_id = pthread_self();
		LOGD("Enter: %s %s, thread id = %lu\n", __FILE__, __FUNCTION__, th_id);
#endif
		//LOGI("LingFileDecoder::ReadPacketThreadFunc Begin Free Packet Size:%d", m_lstFreePacket.Size());
		if (!is_open || !is_running)
		{
			//LOGI("LingFileDecoder::ReadPacketThreadFunc !m_bOpen || !m_bRunning");
			return;
		}

		// Read frames and save first five frames to disk
		while (is_running)
		{
			if (is_seek)
			{
				if (current_seek_pos != next_seek_pos)
				{
					DoSeek(next_seek_pos);
				}
				else
				{
					OvrTime::SleepMs(10);
					continue;
				}
			}
			else 
			{
				if (current_seek_pos != next_seek_pos)
				{
					DoSeek(next_seek_pos);
				}
			}
		
			DecodeOneFrame();
		}

#ifdef LING_ANDROID_NDK
		//LOGI("LingFileDecoder::ReadPacketThreadFunc  exit");
#endif
	}

	bool wxVideoDecoder::DecodeOneFrame() 
	{
		bool is_one_frame = false;
		while (!is_one_frame)
		{
			AVPacket* pPacket = new(std::nothrow)AVPacket;
			if (NULL == pPacket)
			{
				//LOGI("LingFileDecoder::ReadPacketThreadFunc NULL == pPacket");
				OvrTime::SleepMs(10);
				continue;
			}

			int iResult = av_read_frame(format_context, pPacket);
			if (iResult < 0)
			{
#ifdef			LING_ANDROID_NDK
				char szBuffer[256];
				memset(szBuffer, 0, 256);
				av_strerror(iResult, szBuffer, 255);
				LOGE("LingFileDecoder::ReadPacketThreadFunc End of the file, result:%d, error string:%s", iResult, szBuffer);
#endif
				delete pPacket;
				pPacket = NULL;

				is_file_end = true;
				break;
			}

			if (pPacket->stream_index == video_stream_index)
			{
				is_one_frame = video_decoder->DecodePackage(pPacket);
			}
			delete pPacket;
			pPacket = NULL;
		}

		return is_one_frame;
	}

	void wxVideoDecoder::DoSeek(long long a_seek_pos) 
	{
		current_seek_pos = a_seek_pos;

		avformat_seek_file(format_context, -1, 0, current_seek_pos, current_seek_pos, AVSEEK_FLAG_BACKWARD);
		video_decoder->Seek();
	}

}
