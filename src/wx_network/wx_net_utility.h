﻿#ifndef __INS_NET_UTILITY_H_
#define __INS_NET_UTILITY_H_

namespace wxNetUtility
{
	bool CloseSocket(SOCKET a_socket);

	int GetSocketLastErrorID(void);
}
#endif