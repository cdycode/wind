﻿#include "stdafx.h"
#include "wx_tcp_toy_server.h"
#include <wx_base/wx_time.h>

wxTcpToyServer::wxTcpToyServer(void)
	:is_opened(false)
	,tcp_event(NULL)
	,listen_socket(INVALID_SOCKET)
	,curr_socket_id(0)
{}
wxTcpToyServer::~wxTcpToyServer(void)
{
	Close();
}

// 启动
bool wxTcpToyServer::Open(const char* a_listen_ip, unsigned short a_listen_port, wxITcpSvrEvent* a_tcp_event)
{
	if (is_opened || INVALID_SOCKET != listen_socket)
	{
		//TraceLog0("CInsTcpToyServer::Open 重新进行初始化\n");
		Close();
	}

	listen_socket = socket(AF_INET, SOCK_STREAM, 0);   
	if (INVALID_SOCKET == listen_socket)
	{
#ifdef _WIN32
		DWORD ldwErrorID = WSAGetLastError();
#else
        int ldwErrorID = errno;
#endif
		//TraceLog0("CInsTcpToyServer::Open 创建Socket失败,ErrorID:%d\n", ldwErrorID);
		return false;
	}

	sockaddr_in loLocalAddr;			//服务器IP
	// aszSvrAddr为ip地址
	memset((char *)&loLocalAddr, 0,sizeof(sockaddr_in));
	loLocalAddr.sin_family = AF_INET;
	loLocalAddr.sin_port	= htons(a_listen_port);
	if (NULL != a_listen_ip && strlen(a_listen_ip) > 0)
	{
		loLocalAddr.sin_addr.s_addr = inet_addr(a_listen_ip);
	}
	else
	{
		loLocalAddr.sin_addr.s_addr = ADDR_ANY;
	}
	if(0 != bind(listen_socket, (struct sockaddr *)&loLocalAddr, sizeof(loLocalAddr)))
	{
#ifdef _WIN32
		DWORD ldwErrorID = WSAGetLastError();
#else
        int ldwErrorID = errno;
#endif
		//TraceLog0("CInsTcpToyServer::Open 绑定地址(%s:%d)失败,ErrorID:%d\n", apListenIP, ausListenPort, ldwErrorID);
		Close();
		return false;
	}

	if(0 != listen(listen_socket, 20))
	{
#ifdef _WIN32
		DWORD ldwErrorID = WSAGetLastError();
#else
        int ldwErrorID = errno;
#endif
		//TraceLog0("CInsTcpToyServer::Open Listen(%s:%d)失败,ErrorID:%d\n", apListenIP, ausListenPort, ldwErrorID);
		Close();
		return false;
	}

	is_opened = true;
	tcp_event = a_tcp_event;

	work_thread = new std::thread(&wxTcpToyServer::WorkThreadFunc, this);
	if (nullptr == work_thread)
	{
		return false;
	}
	return true;
}

// 停止
void wxTcpToyServer::Close(void)
{
	is_opened = false;

	{
		std::unique_lock<std::recursive_mutex> lAutoLock(socket_id_mutex);
		SocketContextMap::iterator lSocketIt = map_socket_id.begin();
		while(lSocketIt != map_socket_id.end())
		{
#ifdef _WIN32
			closesocket(lSocketIt->second);
#else
            shutdown(lSocketIt->second->m_hSocket, SHUT_RDWR);
            close(lSocketIt->second->m_hSocket);
#endif
			lSocketIt++;
		}
		map_socket_id.clear();
	}
	
	if (INVALID_SOCKET != listen_socket)
	{
#ifdef _WIN32
		closesocket(listen_socket);
#else
        shutdown(listen_socket, SHUT_RDWR);
        close(listen_socket);
#endif
		listen_socket = INVALID_SOCKET;
	}

	if (nullptr != work_thread)
	{
		work_thread->join();
		delete work_thread;
		work_thread = nullptr;
	}
}

// 内存输出
void wxTcpToyServer::Dump(void)
{}

// 发送TCP包到目的端口
bool wxTcpToyServer::SendData(UINT64 a_client_id, const char* a_data, int a_data_len)
{
	SOCKET client_socket;
	if (GetSocket(a_client_id, client_socket))
	{
		return send(client_socket, a_data, a_data_len, 0) == a_data_len;
	}
	return false;
}

//断开指定连接
bool wxTcpToyServer::Disconnect(UINT64 a_client_id)
{
	std::unique_lock<std::recursive_mutex> lAutoLock(socket_id_mutex);
	SocketContextMap::iterator lSocketIt = map_socket_id.find(a_client_id);
	if (lSocketIt != map_socket_id.end())
	{
#ifdef _WIN32
		closesocket(lSocketIt->second);
#else
        shutdown(lSocketIt->second->m_hSocket, SHUT_RDWR);
        close(lSocketIt->second->m_hSocket);
#endif
		map_socket_id.erase(lSocketIt);
		return true;
	}
	return false;
}


void wxTcpToyServer::WorkThreadFunc(void)
{
	if (INVALID_SOCKET == listen_socket)
	{
		//TraceLog0("CInsTcpToyServer::WorkThreadFunc 监听Socket为NULL\n");
		return;
	}
	char* lpBuffer = new(std::nothrow) char[DEF_NET_PACKET_MAX_LEN];
	if (NULL == lpBuffer)
	{
		//TraceLog0("CInsTcpToyServer::WorkThreadFunc 分配内存失败\n");
		return;
	}
	fd_set	fdSocket;
	struct timeval tv = {1, 0};   
	while(is_opened)
	{
		FD_ZERO(&fdSocket);
		FD_SET(listen_socket, &fdSocket); 
		int maxFd = (int)listen_socket;
		{
			std::unique_lock<std::recursive_mutex> lAutoLock(socket_id_mutex);
			SocketContextMap::iterator lSocketIt = map_socket_id.begin();
			while(lSocketIt != map_socket_id.end())
			{
#ifndef _WIN32 
				if (lSocketIt->second->m_hSocket > maxFd)
				{
					maxFd = lSocketIt->second->m_hSocket;
				}
#endif
				FD_SET(lSocketIt->second, &fdSocket); 
				lSocketIt++;
			}
		}

		int ret = select(maxFd+1, &fdSocket, NULL, NULL, &tv); 
		if (SOCKET_ERROR == ret)
		{
#ifdef _WIN32
			DWORD ldwErrorID = WSAGetLastError();
#else
            int ldwErrorID = errno;
#endif
			//TraceLog0("CInsTcpToyServer::WorkThreadFunc 出现异常, ErrorID:%d\n", ldwErrorID);
			continue;
		}

		//ret == 0 超时
		if (ret > 0)
		{
			DealEvent(fdSocket, ret, lpBuffer, DEF_NET_PACKET_MAX_LEN);
		}	
	}
	delete []lpBuffer;
}

void wxTcpToyServer::DealEvent(fd_set& a_event, int a_socket_cnt, char* a_buffer, int a_buffer_len)
{
	if (FD_ISSET(listen_socket, &a_event))
	{
		a_socket_cnt--;
		sockaddr_in loRemoteAddr;			//服务器IP
		socklen_t iaddrSize = sizeof(sockaddr_in);  
		SOCKET lSocket = accept(listen_socket, (struct sockaddr *)&loRemoteAddr, &iaddrSize);
		if (INVALID_SOCKET != lSocket)
		{
			char remote_addr_string[DEF_WX_IP_ADDRESS_MAX_LEN];
			wxSocketAddress::InetV4NToP(loRemoteAddr.sin_addr.s_addr, remote_addr_string);
			tcp_event->OnNewClient(curr_socket_id, remote_addr_string, ntohs(loRemoteAddr.sin_port));
			std::unique_lock<std::recursive_mutex> lAutoLock(socket_id_mutex);
			map_socket_id.insert(std::make_pair(curr_socket_id++, lSocket));
		}
	}

	WORD lOutLen = 0;
	WORD lTimeStamp = 0;
	std::unique_lock<std::recursive_mutex> lAutoLock(socket_id_mutex);
	SocketContextMap::iterator lSocketIt = map_socket_id.begin();
	while(lSocketIt != map_socket_id.end() && a_socket_cnt > 0)
	{
		if (FD_ISSET(lSocketIt->second, &a_event))
		{
			a_socket_cnt--;
			int iDataLen = recv(lSocketIt->second, a_buffer, a_buffer_len, 0);
			if (iDataLen > 0)
			{
                tcp_event->OnRecvData(lSocketIt->first, a_buffer, iDataLen);
			}
			else
			{
				tcp_event->OnBreakClient(lSocketIt->first);
#ifdef _WIN32
				closesocket(lSocketIt->second);
#else
                shutdown(lSocketIt->second->m_hSocket, SHUT_RDWR);
                close(lSocketIt->second->m_hSocket);
#endif
				map_socket_id.erase(lSocketIt++);
				continue;
			}
			
		}
		lSocketIt++;
	}
}

bool	wxTcpToyServer::GetSocket(UINT64 a_client_id, SOCKET& a_socket)
{
	std::unique_lock<std::recursive_mutex> lAutoLock(socket_id_mutex);
	SocketContextMap::iterator lSocketIt = map_socket_id.find(a_client_id);
	if (lSocketIt != map_socket_id.end())
	{
		a_socket = lSocketIt->second;
		return true;
	}
	return false;
}

//创建和销毁实例
wxITcpToyServer* wxITcpToyServer::CreateIns(void)
{
	return new(std::nothrow)wxTcpToyServer;
}

void wxITcpToyServer::DestoryIns(wxITcpToyServer*& a_ins)
{
	if (NULL != a_ins)
	{
		a_ins->Close();
		delete a_ins;
		a_ins = NULL;
	}
}
