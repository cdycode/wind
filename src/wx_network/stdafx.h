﻿#ifndef WX_NETWORK_STDAFX_H_
#define WX_NETWORK_STDAFX_H_

#include <stdio.h>
#include <time.h>
#ifdef _WIN32
    #include <direct.h>
#else
    #include <string.h>
#endif

//stl
#include <new>
#include <map>
#include <condition_variable>

#ifdef _WIN32
    #include <WinSock2.h>
    #include <Ws2tcpip.h>
    #include <Mswsock.h>
#else
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <errno.h>
    #include <netdb.h>
    #include <unistd.h>
#endif

#include <thread>
#include <mutex>

#ifdef _WIN32
    #pragma comment( lib, "Ws2_32.lib")
#else
    #define INVALID_SOCKET -1
    #define SOCKET_ERROR -1
    #define ADDR_ANY 0
    typedef int SOCKET;
#endif

#endif
