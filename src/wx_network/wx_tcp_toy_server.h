﻿#ifndef WX_NETWORK_WX_TCP_TOY_SERVER_H_
#define WX_NETWORK_WX_TCP_TOY_SERVER_H_

#include <wx_network/wx_i_tcp_toy_server.h>

typedef std::map<UINT64, SOCKET> SocketContextMap;

class wxTcpToyServer : public wxITcpToyServer
{
public:
	wxTcpToyServer(void);
	~wxTcpToyServer(void);

	// 启动
	virtual bool Open(const char* a_listen_ip, unsigned short a_listen_port, wxITcpSvrEvent* a_tcp_event);

	// 停止
	virtual void Close(void);

	// 内存输出
	virtual void Dump(void);

	// 发送TCP包到目的端口
	virtual bool SendData(UINT64 a_client_id, const char* a_data, int a_data_len);

	//断开指定连接
	virtual bool Disconnect(UINT64 a_client_id);

private:

	void WorkThreadFunc(void);

	void DealEvent(fd_set& a_event, int a_socket_cnt, char* a_buffer, int a_buffer_len);

	bool	GetSocket(UINT64 a_client_id, SOCKET& a_socket);
private:
	bool				is_opened;
	wxITcpSvrEvent*	tcp_event;

	std::thread*		work_thread = nullptr;

	SOCKET				listen_socket;
	UINT64          	curr_socket_id;
	std::recursive_mutex			socket_id_mutex;
	SocketContextMap	map_socket_id;
};

#endif
