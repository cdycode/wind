﻿#include "stdafx.h"
#include <wx_network/wx_network.h>

bool sg_is_init_network = false;
bool InitInsNetwork()
{
	if (sg_is_init_network)
	{
		return true;
	}

#ifdef _WIN32
	//初始化Socket
	WORD wVersionRequested;
	WSADATA wsaData;
	wVersionRequested = MAKEWORD(2, 2);
	WSAStartup(wVersionRequested, &wsaData);
#endif 
	sg_is_init_network = true;
	return true;
}

void UninitInsNetwork(void)
{
	if (sg_is_init_network)
	{
#ifdef _WIN32
		WSACleanup();
#endif
	}
	sg_is_init_network = false;
}
