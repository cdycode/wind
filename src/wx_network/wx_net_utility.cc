﻿#include "stdafx.h"
#include "wx_net_utility.h"
#include <wx_network/wx_network.h>

namespace wxNetUtility
{
	//获取本机名字
	bool GetLocalHostName(char* a_buffer, int a_buffer_len)
	{
		if (0 != gethostname(a_buffer, a_buffer_len))
		{
            int ldwErrorID = GetSocketLastErrorID();
			//TraceLog0("insNetUtility::GetLocalHostName 获取电脑名字失败,错误ID为：%d\n", ldwErrorID);
			return false;
		}
		return true;
	}

	//根据IP获取主机名
	bool GetHostNameByIp(char* a_buffer, int a_buffer_len, const wxSocketAddress& a_address)
	{
		memset(a_buffer, 0, a_buffer_len);
#ifdef _WIN32
		struct sockaddr_in saGNI;
		saGNI.sin_family = AF_INET;
		saGNI.sin_addr.s_addr = a_address.ip_address;
		saGNI.sin_port = htons(a_address.port);
		if(0 != getnameinfo((struct sockaddr *) &saGNI, sizeof (struct sockaddr), a_buffer,a_buffer_len, NULL, NULL, NI_NOFQDN ))
		{
			wxSocketAddress::InetV4NToP(a_address.ip_address, a_buffer);
		}
		return true;
#endif
	}

	//获取指定机器名的IP地址
	int GetHostAllIp(const char* a_host_name, unsigned int* a_ip_address, int a_address_cnt)
	{
		if (NULL == a_host_name || NULL == a_ip_address || a_address_cnt <= 0)
		{
			return 0;
		}
		
		struct hostent *remoteHost;
		remoteHost = gethostbyname(a_host_name);

		if (remoteHost == NULL) 
		{
            int ldwErrorID = GetSocketLastErrorID();
			//TraceLog0("insNetUtility::GetHostAllIp 获取电脑(%s)的IP地址失败,错误ID为：%d\n", apHostName, ldwErrorID);
			return 0;
		}

		int liGotIpCnt = 0;
		if (remoteHost->h_addrtype == AF_INET)
		{
			for (int i = 0; i < a_address_cnt && 0 != remoteHost->h_addr_list[i]; i++)
			{
				a_ip_address[i] = *(u_long *) remoteHost->h_addr_list[i];
				liGotIpCnt++;
			}
		}
		return liGotIpCnt;
	}

	//获取本机所有的地址
	int GetLocalHostAllIp(unsigned int* a_ip_address, int a_address_cnt)
	{
		char szHostName[256];
		if (GetLocalHostName(szHostName, 256))
		{
			return GetHostAllIp(szHostName, a_ip_address, a_address_cnt);
		}
		return 0;
	}

    bool CloseSocket(SOCKET a_socket)
    {
#ifdef _WIN32
        closesocket(a_socket);
#else
        shutdown(a_socket, SHUT_RDWR);
        close(a_socket);
#endif
        return true;
    }
    int GetSocketLastErrorID(void)
    {
#ifdef _WIN32
        return WSAGetLastError();
#else
        return errno;
#endif
    }
}

