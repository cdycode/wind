﻿#include "stdafx.h"
#include <wx_network/wx_network.h>

wxSocketAddress::wxSocketAddress(void)
{
	memset(this, 0, sizeof(wxSocketAddress));
}

wxSocketAddress::wxSocketAddress(const char* a_ip, unsigned short a_port)
{
    wxSocketAddress::InetV4PToN(a_ip, ip_address);
    port = a_port;
}

wxSocketAddress::wxSocketAddress(unsigned int a_ip, unsigned short a_port)
{
    ip_address = a_ip;
    port = a_port;
}

wxSocketAddress::wxSocketAddress(const wxSocketAddress& a_address)
{
    ip_address = a_address.ip_address;
    port = a_address.port;
}

wxSocketAddress::~wxSocketAddress(void)
{
}

void wxSocketAddress::operator = (const wxSocketAddress& a_address)
{
	//memcpy(m_szIPAddress, aoAddress.m_szIPAddress, DEF_INS_IP_ADDRESS_MAX_LEN);
    ip_address = a_address.ip_address;
	port = a_address.port;
}

//判断是否相等
bool wxSocketAddress::operator == (const wxSocketAddress& a_address)
{
	return (port == a_address.port && ip_address == a_address.ip_address);
}

//将整数IP转为点分十进制
bool wxSocketAddress::InetV4NToP(unsigned int a_numeric_binary, char a_text_presentation[DEF_WX_IP_ADDRESS_MAX_LEN])
{
	struct in_addr lAddr;
	lAddr.s_addr = a_numeric_binary;
#ifdef _WIN32
	return (NULL != InetNtopA(AF_INET, &lAddr, a_text_presentation, DEF_WX_IP_ADDRESS_MAX_LEN));
#else
    return (NULL != inet_ntop(AF_INET, &lAddr, a_text_presentation, DEF_WX_IP_ADDRESS_MAX_LEN));
#endif
}

//将点分十进制转为整数IP
bool wxSocketAddress::InetV4PToN(const char* a_text_presentation, unsigned int& a_numeric_binary)
{
	struct in_addr lAddr;
#ifdef _WIN32
	if(1 == InetPtonA(AF_INET, a_text_presentation, &lAddr))
#else
    if(1 == inet_pton(AF_INET, a_text_presentation, &lAddr))
#endif
	{
		a_numeric_binary = lAddr.s_addr;
		return true;
	}
	return false;
}

