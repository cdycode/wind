﻿#ifndef WX_NETWORK_WX_BROADCAST_SOCKET_H_
#define WX_NETWORK_WX_BROADCAST_SOCKET_H_

#include <wx_network/wx_i_broadcast_socket.h>
class wxBroadcastSocket : public wxIBroadcastSocket
{
public:
	wxBroadcastSocket(void);
	virtual ~wxBroadcastSocket(void);

	//初始化和反初始化
	virtual bool Open(wxIBroadcastEvent* a_broadcast_event, unsigned short a_bind_port, unsigned short a_broadcast_port);
	virtual void Close(void);

	//发送广播数据
	virtual bool BroadcastData(const char* a_send_data,int a_data_len);

	//发送数据
	virtual bool SendData(const char* a_send_data,int a_data_len, const wxSocketAddress& a_dst_address);

	//定时日志输出
	virtual void Dump(void);

	//获取Socket绑定地址
	virtual bool GetLocalAddress(wxSocketAddress& a_dst_address);
private:
	//static unsigned WorkThread(void* apParam);
	void WorkThreadFunc(void);
private:
	std::thread*		work_thread = nullptr;

	bool			is_opened;
	SOCKET			socket_handle;
	unsigned short	bind_port;
	wxIBroadcastEvent*		broadcast_event;

	struct sockaddr_in		broadcast_address;
};
#endif
