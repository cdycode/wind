﻿#include "stdafx.h"
#include "wx_tcp_client.h"
#include <wx_base/wx_time.h>

wxTcpClient::wxTcpClient(void)
	:is_opened(false)
	,is_connected(false)
	,client_event(NULL)
	,is_sync(false)
	,socket_handle(INVALID_SOCKET)
{}

wxTcpClient::~wxTcpClient(void)
{}

//初始化和反初始化
bool wxTcpClient::Open(wxITcpClientEvent* client_event, bool a_is_sync /*= true*/)
{
	client_event = client_event;
	is_sync = a_is_sync;

	is_opened = true;
	is_connected = false;
	return true;
}

void wxTcpClient::Close(void)
{
	DisConnect();
}

//发起和断开连接
bool wxTcpClient::Connect( const wxSocketAddress& a_address )
{
	if (is_connected || INVALID_SOCKET != socket_handle)
	{
		//TraceLog0("CInsTcpClient::Connect 断开已经建立的连接\n");
		DisConnect();
	}

	sockaddr_in loSvrAddr;			//服务器IP
	// aszSvrAddr为ip地址
	memset((char *)&loSvrAddr, 0,sizeof(sockaddr_in));
	loSvrAddr.sin_family = AF_INET;
	loSvrAddr.sin_port	= htons(a_address.port);
	loSvrAddr.sin_addr.s_addr = a_address.ip_address; 

	socket_handle = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_handle == INVALID_SOCKET)
	{
		//TraceLog0("CInsTcpClient::Connect 创建Socket失败\n");
		return false;
	}

	if (connect(socket_handle, (struct sockaddr *)&loSvrAddr, sizeof(loSvrAddr)) < 0)
	{
		DWORD errorID = 0;
#ifdef _WIN32
		errorID = WSAGetLastError();
#else
		errorID = errno;
#endif
        char ip[DEF_NET_PACKET_MAX_LEN];
        wxSocketAddress::InetV4NToP(a_address.ip_address, ip);
		//TraceLog0("CInsTcpClient::Connect:连接服务器(%s:%d)失败!失败代码：%d\n", ip, address.m_usPort, errorID);
#ifdef _WIN32
		closesocket(socket_handle);
#else
        shutdown(socket_handle, SHUT_RDWR);
        close(socket_handle);
#endif
		socket_handle = INVALID_SOCKET;
		return false;
	}

	is_connected = true;

	is_opened = true;
	work_thread = new std::thread(&wxTcpClient::WorkThreadFunc, this);
	if (nullptr == work_thread)
	{
		return false;
	}
	return true;
}

bool wxTcpClient::Connect(const char *a_svr_addr, unsigned short a_svr_port)
{
    return Connect(wxSocketAddress(a_svr_addr, a_svr_port));
}

bool wxTcpClient::Connect( unsigned int a_svr_addr, unsigned short a_svr_port )
{
    return Connect(wxSocketAddress(a_svr_addr, a_svr_port));
}

void wxTcpClient::DisConnect(void)
{
	is_connected = false;

	if (INVALID_SOCKET != socket_handle)
	{
#ifdef _WIN32
		closesocket(socket_handle);
#else
        shutdown(socket_handle, SHUT_RDWR);
        close(socket_handle);
#endif
		socket_handle = INVALID_SOCKET;
	}

	if (nullptr != work_thread)
	{
		work_thread->join();
		delete work_thread;
		work_thread = nullptr;
	}
}

//是否连接上
bool wxTcpClient::IsConneted(void)
{
	return is_connected;
}

//发送数据
bool wxTcpClient::SendData(const char* a_send_data,int a_data_len)
{
	if (INVALID_SOCKET != socket_handle)
	{
		return send(socket_handle, a_send_data, a_data_len, 0) == a_data_len;
	}
	return false;
}

//定时日志输出
void wxTcpClient::Dump(void)
{}

void wxTcpClient::WorkThreadFunc(void)
{
	//参数校验
	if (INVALID_SOCKET == socket_handle)
	{
		//TraceLog0("CInsTcpClient::WorkThreadFunc Socket句柄无效\n");
		return;
	}
	if (NULL == client_event)
	{
		//TraceLog0("CInsTcpClient::WorkThreadFunc 回调指针为NULL\n");
		return;
	}

	char* lpBuffer = new(std::nothrow) char[DEF_NET_PACKET_MAX_LEN];
	if (NULL == lpBuffer)
	{
		//TraceLog0("CInsTcpClient::WorkThreadFunc 申请内存失败\n");
		return;
	}

	WORD lwOutLen = 0;
	WORD lwTimeStamp = 0;

	while(is_connected)
	{
		int iRecvLen = recv(socket_handle, lpBuffer, DEF_NET_PACKET_MAX_LEN, 0);
		if (iRecvLen > 0)
		{
			client_event->OnRecvData(lpBuffer, iRecvLen);
		}
		else
		{
			client_event->OnDisconnect();

            is_connected = false;

            if (INVALID_SOCKET != socket_handle)
            {
#ifdef _WIN32
                closesocket(socket_handle);
#else
                shutdown(socket_handle, SHUT_RDWR);
                close(socket_handle);
#endif
                socket_handle = INVALID_SOCKET;
            }
		}
	}

	if (NULL != lpBuffer)
	{
		delete []lpBuffer;
	}
}

//创建和销毁实例
wxITcpClient* wxITcpClient::CreateIns(void)
{
	return new(std::nothrow)wxTcpClient;
}

void wxITcpClient::DestoryIns(wxITcpClient*& a__ins)
{
	if (NULL != a__ins)
	{
		a__ins->Close();
		delete a__ins;
		a__ins = NULL;
	}
}
