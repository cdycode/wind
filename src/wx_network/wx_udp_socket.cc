﻿#include "stdafx.h"
#include "wx_udp_socket.h"
#include "wx_net_utility.h"
#include <wx_base/wx_time.h>

wxUdpSocket::wxUdpSocket(void)
	:is_opened(false)
	,socket_handle(INVALID_SOCKET)
	,bind_port(0)
	, recv_buf_size(2048)
	,udp_socket_event(NULL)
{
}
wxUdpSocket::~wxUdpSocket(void)
{
	Close();
}

//初始化和反初始化
bool wxUdpSocket::Open(wxIUdpRecvEvent* a_udp_socket_event, const char* a_bind_ip /*= 0*/, unsigned short a_bind_port/* = 0*/, int a_recv_buf_size /*= 2048*/)
{
	if (is_opened)
	{
		//TraceLog0("InsUdpSocket::Open 已经成功初始化,广播端口为:%d\n", m_usBindPort);
		return true;
	}

	bind_port		= a_bind_port;
	udp_socket_event	= a_udp_socket_event;
	recv_buf_size		= a_recv_buf_size;

	//创建socket
	socket_handle = socket(AF_INET, SOCK_DGRAM, 0);
	if(INVALID_SOCKET == socket_handle)
	{
        int ErrorID = wxNetUtility::GetSocketLastErrorID();
		//TraceLog0("InsUdpSocket::Open 创建Socket失败, errorID:%d\n", ErrorID);
		return false;
	}

	// 用来绑定套接字
	struct sockaddr_in loBindAddr;
	loBindAddr.sin_family = AF_INET;
	loBindAddr.sin_port   = htons(a_bind_port);
	if (NULL != a_bind_ip)
	{
        unsigned int uiAddr = 0;
        wxSocketAddress::InetV4PToN(a_bind_ip, uiAddr);
        loBindAddr.sin_addr.s_addr = uiAddr;
	}
	else
	{
		loBindAddr.sin_addr.s_addr = INADDR_ANY;
	}

	// 绑定套接字
	int errorID = bind(socket_handle, (struct sockaddr*)&loBindAddr, sizeof(struct sockaddr_in));
	if(SOCKET_ERROR == errorID)
	{
        errorID = wxNetUtility::GetSocketLastErrorID();
		//TraceLog0("InsUdpSocket::Open 绑定端口失败，错误ID为：%d\n", errorID);
		Close();
		return false;
	}
#ifdef _WIN32
	//解决winsocket的bug 避免数据接收线程中返回10054的错误
	DWORD dwBytesReturned = 0;  
	BOOL bNewBehavior = FALSE;  
	DWORD status;  
	status = WSAIoctl(socket_handle, SIO_UDP_CONNRESET,  
		&bNewBehavior, sizeof(bNewBehavior),  
		NULL, 0, &dwBytesReturned,  
		NULL, NULL);  
	if (SOCKET_ERROR == status)  
	{  
        int dwErr = wxNetUtility::GetSocketLastErrorID();
		//TraceLog0("InsUdpSocket::Open WSAIoctl(SIO_UDP_CONNRESET) Error：%d\n", dwErr);
		return false;
	}
#endif
	is_opened = true;
	work_thread = new std::thread(&wxUdpSocket::WorkThreadFunc, this);
	if (nullptr == work_thread)
	{
		return false;
	}
	return true;
}

void wxUdpSocket::Close(void)
{
	//TraceLog0("InsUdpSocket::Close 开始关闭，绑定端口为：%d\n", m_usBindPort);
	is_opened = false;
	
	if (INVALID_SOCKET != socket_handle)
	{
        wxNetUtility::CloseSocket(socket_handle);
		socket_handle = INVALID_SOCKET;
	}

	if (nullptr != work_thread)
	{
		work_thread->join();
		delete work_thread;
		work_thread = nullptr;
	}
}

//发送广播数据
bool wxUdpSocket::SendData(const char* a_send_data,int a_data_len, const wxSocketAddress& a_dst_address)
{
	int nSendSize = 0;
	if (is_opened && INVALID_SOCKET != socket_handle)
	{
		struct sockaddr_in loDstAddr;
		loDstAddr.sin_family = AF_INET;
		loDstAddr.sin_port   = htons(a_dst_address.port);
        loDstAddr.sin_addr.s_addr = a_dst_address.ip_address;
		//InetPtonA(AF_INET, aoDstAddress.m_szIPAddress, &(loDstAddr.sin_addr));

		nSendSize = sendto(socket_handle, a_send_data, a_data_len, 0, (struct sockaddr*)&loDstAddr, sizeof(struct sockaddr_in));
		if (nSendSize != a_data_len)
		{
            int ldwErrorID = wxNetUtility::GetSocketLastErrorID();
			//TraceLog0("InsUdpSocket::SendData 数据发送失败，错误ID为:%d\n", ldwErrorID);
		}
	}
	return nSendSize == a_data_len;
}

//定时日志输出
void wxUdpSocket::Dump(void)
{}

//获取Socket绑定地址
bool wxUdpSocket::GetLocalAddress(wxSocketAddress& a_dst_address)
{
	bool bSuccess = false;
	if (INVALID_SOCKET != socket_handle)
	{
	    struct sockaddr_in localAddr;
		socklen_t addr_len = sizeof(localAddr);

		if(SOCKET_ERROR != getsockname(socket_handle, (struct sockaddr*)&localAddr, &addr_len))
		{
            a_dst_address.ip_address = localAddr.sin_addr.s_addr;
			//insSocketAddress::InetV4NToP(localAddr.sin_addr.S_un.S_addr, aoDstAddress.m_szIPAddress);
			a_dst_address.port = ntohs(localAddr.sin_port);
			bSuccess = true;
		}
		else
		{
            int ldwErrorID = wxNetUtility::GetSocketLastErrorID();
			//TraceLog0("InsUdpSocket::GetLocalAddress 错误ID为:%d\n", ldwErrorID);
		}
	}
	return bSuccess;
}

void wxUdpSocket::WorkThreadFunc(void)
{
	char* szBuff = new char[recv_buf_size];
	struct sockaddr_in  fromAddress;
	socklen_t iAddressLen = sizeof(struct sockaddr_in);
	while(is_opened)
	{
		// 接收数据
		int nSendSize = recvfrom(socket_handle, szBuff, recv_buf_size, 0, (struct sockaddr*)&fromAddress, &iAddressLen);
		if(SOCKET_ERROR == nSendSize)
		{
            int ldwErrorID = wxNetUtility::GetSocketLastErrorID();
			//TraceLog0("InsUdpSocket::WorkThreadFunc 接收数据失败，错误ID为：%d\n", ldwErrorID);
			break;
		}
		if (NULL != udp_socket_event)
		{
			szBuff[nSendSize] = '\0';
			wxSocketAddress lAddress;
            lAddress.ip_address = fromAddress.sin_addr.s_addr;
			//InetNtopA(AF_INET, (void *)&(fromAddress.sin_addr), lAddress.m_szIPAddress, DEF_INS_IP_ADDRESS_MAX_LEN);
			lAddress.port = ntohs(fromAddress.sin_port);
			udp_socket_event->UdpRecvData(szBuff, nSendSize, lAddress);
		}
	}
}

bool wxUdpSocket::JoinGroup( char* a_group_ip )
{
    unsigned char ttl=255;
#ifdef _WIN32
    if (setsockopt(socket_handle,IPPROTO_IP,10/*IP_MULTICAST_TTL*/, (const char*)&ttl, sizeof(ttl)) == SOCKET_ERROR)
#else
    if (setsockopt(socket_handle,IPPROTO_IP,IP_MULTICAST_TTL, (const char*)&ttl, sizeof(ttl)) == SOCKET_ERROR)
#endif
    {
       //TraceLog0("InsUdpSocket::InsUdpSocket setsockopt1失败 错误ID为:%d\n", insNetUtility::GetSocketLastErrorID());
       return false;
    }

    struct ip_mreq {
        struct in_addr imn_multiaddr;
        struct in_addr imr_interface;
    } areq;
    areq.imn_multiaddr.s_addr = inet_addr(a_group_ip);
    areq.imr_interface.s_addr = htonl(INADDR_ANY);
#ifdef _WIN32
    if (setsockopt(socket_handle, IPPROTO_IP, 12/*IP_ADD_MEMBERSHIP*/, (const char*)&areq, sizeof(areq)) == SOCKET_ERROR)
#else
    if (setsockopt(socket_handle, IPPROTO_IP, IP_ADD_MEMBERSHIP, (const char*)&areq, sizeof(areq)) == SOCKET_ERROR)
#endif
    {
        //TraceLog0("InsUdpSocket::InsUdpSocket setsockopt2失败 错误ID为:%d\n", insNetUtility::GetSocketLastErrorID());
        return false;
    }

    return true;
}

wxIUdpSocket* wxIUdpSocket::CreateIns(void)
{
	return new(std::nothrow)wxUdpSocket;
}

void wxIUdpSocket::DestoryIns(wxIUdpSocket*& a_ins)
{
	if (NULL != a_ins)
	{
		a_ins->Close();
		delete a_ins;
		a_ins = NULL;
	}
}

