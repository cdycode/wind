﻿#include "stdafx.h"
#include "wx_broadcast_socket.h"
#include <wx_base/wx_time.h>
//#include <wx_base/wx_debug_trace.h>

wxBroadcastSocket::wxBroadcastSocket(void)
	:is_opened(false)
	,socket_handle(INVALID_SOCKET)
	,bind_port(0)
	,broadcast_event(NULL)
{
	memset(&broadcast_address, 0, sizeof(struct sockaddr_in));
}
wxBroadcastSocket::~wxBroadcastSocket(void)
{
	Close();
}

//初始化和反初始化
bool wxBroadcastSocket::Open(wxIBroadcastEvent* a_broadcast_event, unsigned short a_bind_port, unsigned short a_broadcast_port)
{
	if (is_opened)
	{
		//TraceLog0("InsBroadcastSocket::Open 已经成功初始化,广播端口为:%d\n", m_usBindPort);
		return true;
	}

	if (a_broadcast_port <= 0)
	{
		//TraceLog0("InsBroadcastSocket::Open 广播端口无效:%d\n", ausBroadcastPort);
		return false;
	}

	bind_port		= a_broadcast_port;
	broadcast_event	= a_broadcast_event;

	//创建socket
	socket_handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(INVALID_SOCKET == socket_handle)
	{
#ifdef _WIN32
		DWORD ErrorID = WSAGetLastError();
#else
        int ErrorID = errno;
#endif
		//TraceLog0("InsBroadcastSocket::Open 创建Socket失败, errorID:%d\n", ErrorID);
		return false;
	}

	// 初始化广播地址
	broadcast_address.sin_family = AF_INET;
	broadcast_address.sin_port = htons(a_broadcast_port);
	broadcast_address.sin_addr.s_addr = INADDR_BROADCAST;

	//设置该套接字为广播类型，
	bool bOpt = true;
	setsockopt(socket_handle, SOL_SOCKET, SO_BROADCAST, (char*)&bOpt, sizeof(bOpt));

	//绑定套接字
	struct sockaddr_in loBindAddr;
	loBindAddr.sin_family = AF_INET;
	loBindAddr.sin_port   = htons(a_bind_port);
	loBindAddr.sin_addr.s_addr = INADDR_ANY;
	int  errorID = bind(socket_handle, (struct sockaddr*)&loBindAddr, sizeof(struct sockaddr));
	if(SOCKET_ERROR == errorID)
	{
#ifdef _WIN32
		errorID = WSAGetLastError();
#else
        errorID = errno;
#endif
		//TraceLog0("InsBroadcastSocket::Open 绑定端口失败，错误ID为：%d\n", errorID);
		Close();
		return false;
	}

	is_opened = true;
	work_thread = new std::thread(&wxBroadcastSocket::WorkThreadFunc, this);
	if (nullptr == work_thread)
	{
		return false;
	}
	return true;
}

void wxBroadcastSocket::Close(void)
{
	is_opened = false;

	if (INVALID_SOCKET != socket_handle)
	{
#ifdef _WIN32
		closesocket(socket_handle);
#else
		shutdown(socket_handle, SHUT_RDWR);
		close(socket_handle);
#endif
		socket_handle = INVALID_SOCKET;
	}

	if (nullptr != work_thread)
	{
		work_thread->join();
		delete work_thread;
		work_thread = nullptr;
	}
}

//发送广播数据
bool wxBroadcastSocket::BroadcastData(const char* a_send_data,int a_data_len)
{
	int nSendSize = 0;
	if (is_opened && INVALID_SOCKET != socket_handle)
	{
		nSendSize = sendto(socket_handle, a_send_data, a_data_len, 0, (struct sockaddr*)&broadcast_address, sizeof(struct sockaddr_in));
		if (nSendSize != a_data_len)
		{
#ifdef _WIN32
			DWORD ldwErrorID = WSAGetLastError();
#else
            int ldwErrorID = errno;
#endif
			//TraceLog0("InsBroadcastSocket::SendData 数据发送失败，错误ID为:%d\n", ldwErrorID);
		}
	}
	return nSendSize == a_data_len;
}

//发送数据
bool wxBroadcastSocket::SendData(const char* a_send_data,int a_data_len, const wxSocketAddress& a_dst_address)
{
	int nSendSize = 0;
	if (is_opened && INVALID_SOCKET != socket_handle)
	{
		struct sockaddr_in loDstAddr;
		loDstAddr.sin_family = AF_INET;
		loDstAddr.sin_port   = htons(a_dst_address.port);
        loDstAddr.sin_addr.s_addr = a_dst_address.ip_address;
		//InetPtonA(AF_INET, aoDstAddress.m_szIPAddress, &(loDstAddr.sin_addr));

		nSendSize = sendto(socket_handle, a_send_data, a_data_len, 0, (struct sockaddr*)&loDstAddr, sizeof(struct sockaddr_in));
		if (nSendSize != a_data_len)
		{
#ifdef _WIN32
			DWORD ldwErrorID = WSAGetLastError();
#else
            int ldwErrorID = errno;
#endif
			//TraceLog0("InsUdpSocket::SendData 数据发送失败，错误ID为:%d\n", ldwErrorID);
		}
	}
	return nSendSize == a_data_len;
}

//定时日志输出
void wxBroadcastSocket::Dump(void)
{}

//获取Socket绑定地址
bool wxBroadcastSocket::GetLocalAddress(wxSocketAddress& a_dst_address)
{
	bool bSuccess = false;
	if (INVALID_SOCKET != socket_handle)
	{
		struct sockaddr_in localAddr;
		socklen_t addr_len = sizeof(localAddr);

		if(SOCKET_ERROR != getsockname(socket_handle, (struct sockaddr*)&localAddr, &addr_len))
		{
            a_dst_address.ip_address = localAddr.sin_addr.s_addr;
			//insSocketAddress::InetV4NToP(localAddr.sin_addr.S_un.S_addr, aoDstAddress.m_szIPAddress);
			a_dst_address.port = ntohs(localAddr.sin_port);
			bSuccess = true;
		}
		else
		{
#ifdef _WIN32
			DWORD ldwErrorID = WSAGetLastError();
#else
            int ldwErrorID = errno;
#endif
			//TraceLog0("InsBroadcastSocket::GetLocalAddress 错误ID为:%d\n", ldwErrorID);
		}
	}
	return bSuccess;
}

void wxBroadcastSocket::WorkThreadFunc(void)
{
	char szBuff[DEF_NET_PACKET_MAX_LEN+1];
	struct sockaddr_in fromAddress;
	socklen_t iAddressLen = sizeof(struct sockaddr_in);
	while(is_opened)
	{
		// 接收数据
		int nSendSize = recvfrom(socket_handle, szBuff, DEF_NET_PACKET_MAX_LEN, 0, (struct sockaddr*)&fromAddress, &iAddressLen);
		if(SOCKET_ERROR == nSendSize)
		{
#ifdef _WIN32
			DWORD ldwErrorID = WSAGetLastError();
#else
            int ldwErrorID = errno;
#endif
			//TraceLog0("InsBroadcastSocket::WorkThreadFunc 接收数据失败，错误ID为：%d\n", ldwErrorID);
			break;
		}
		if (NULL != broadcast_event)
		{
			szBuff[nSendSize] = '\0';
			wxSocketAddress lAddress;
            lAddress.ip_address = fromAddress.sin_addr.s_addr;
			//InetNtopA(AF_INET, (void *)&(fromAddress.sin_addr), lAddress.m_szIPAddress, DEF_INS_IP_ADDRESS_MAX_LEN);
			lAddress.port = ntohs(fromAddress.sin_port);
			broadcast_event->BroadcastRecvData(szBuff, nSendSize, lAddress);
		}
	}
}

wxIBroadcastSocket* wxIBroadcastSocket::CreateIns(void)
{
	return new(std::nothrow)wxBroadcastSocket;
}

void wxIBroadcastSocket::DestoryIns(wxIBroadcastSocket*& a_ins)
{
	if (NULL != a_ins)
	{
		a_ins->Close();
		delete a_ins;
		a_ins = NULL;
	}
}

