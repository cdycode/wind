﻿#ifndef WX_NETWORK_WX_TCP_CLIENT_H_
#define WX_NETWORK_WX_TCP_CLIENT_H_

#include <wx_network/wx_i_tcp_client.h>

class wxTcpClient : public wxITcpClient
{
public:
	wxTcpClient(void);
	virtual ~wxTcpClient(void);

	//初始化和反初始化
	virtual bool Open(wxITcpClientEvent* a_client_event, bool a_is_sync = true);
	virtual void Close(void);

	//发起和断开连接
    virtual bool Connect(const wxSocketAddress& a_address);
	virtual bool Connect(const char *a_svr_addr, unsigned short a_svr_port);
    virtual bool Connect(unsigned int a_svr_addr, unsigned short a_svr_port);
	virtual void DisConnect(void);

	//是否连接上
	virtual bool IsConneted(void);

	//发送数据
	virtual bool SendData(const char* a_send_data,int a_data_len);

	//定时日志输出
	virtual void Dump(void);

protected:
	void WorkThreadFunc(void);

private:
	bool					is_opened;		//是否已经打开
	bool					is_connected;
	std::thread*		work_thread = nullptr;

	wxITcpClientEvent*		client_event;
	bool					is_sync;

	SOCKET					socket_handle;
};

#endif
