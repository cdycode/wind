﻿#ifndef WX_NETWORK_WX_UDP_SOCKET_H_
#define WX_NETWORK_WX_UDP_SOCKET_H_

#include <wx_network/wx_i_udp_socket.h>

class wxUdpSocket : public wxIUdpSocket
{
public:
	wxUdpSocket(void);
	virtual ~wxUdpSocket(void);

	//初始化和反初始化
	virtual bool Open(wxIUdpRecvEvent* a_udp_socket_event, const char* a_bind_ip = 0, unsigned short ausBindPort = 0, int a_recv_buf_size = 2048);
	virtual void Close(void);

    virtual bool JoinGroup(char* a_group_ip);

	//发送广播数据
	virtual bool SendData(const char* a_send_data,int a_data_len, const wxSocketAddress& a_dst_address);

	//定时日志输出
	virtual void Dump(void);

	//获取Socket绑定地址
	virtual bool GetLocalAddress(wxSocketAddress& a_dst_address);
private:
	static unsigned WorkThread(void* a_param);
	void WorkThreadFunc(void);
private:
	bool			is_opened;
	std::thread*		work_thread = nullptr;
	SOCKET			socket_handle;
	unsigned short	bind_port;
	int				recv_buf_size;
	wxIUdpRecvEvent*	udp_socket_event;
};
#endif