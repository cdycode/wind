#pragma once

//c,c++ header
#include <cassert>
#include <functional>

//qt header
#include <QApplication>
#include <QDesktopWidget>
#include <QWindow>
#include <QIcon>
#include <QLabel>
#include <QDebug>
#include <QPainter>
#include <QPaintEvent>
#include <QIcon>
#include <QStyledItemDelegate>
#include <QAction>
#include <QList>
#include <QPalette>
#include <QPixmap>
#include <QTextEdit> 
#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QStackedLayout>
#include <QOpenGLContext>
#include <QSettings>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QMenu>
#include <QMenuBar>
#include <QToolBar>
#include <QToolButton>
#include <QCheckBox>
#include <QPushButton>
#include <QFrame>
#include <QSplitter>
#include <QSpacerItem>
#include <QAbstractItemView>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QListWidget>
#include <QTreeView>
#include <QHeaderView>
#include <QLineEdit>
#include <QMessageBox> 
#include <QDialog>
#include <QFileDialog>
#include <QComboBox> 
#include <QRect>
#include <QSize>
#include <QImage>
#include <QColor>
#include <QFontMetrics>

#include <QStringList>
#include <QString>
#include <list>
#include <mutex>
#include <condition_variable>

#include <QMouseEvent>
#include <QKeyEvent>
#include <QFocusEvent>
#include <QResizeEvent>
#include <QResizeEvent>
#include <QTimerEvent>
#include <QWheelEvent>
#include <QMimeData>

//import header
#include <wx_base/wx_type.h>
#include <wx_base/wx_time.h>
#include <wx_base/wx_string.h>
#include <wx_base/wx_vector3.h>

#include <wx_engine/ovr_actor.h>
//#include <wx_engine/ovr_empty_actor.h>
//#include <wx_engine/ovr_mesh_actor.h>

#include <wx_engine/ovr_scene.h>

#include <wx_asset_manager/ovr_archive_asset_manager.h>
#include <wx_asset_manager/ovr_archive_asset.h>
#include <wx_asset_manager/ovr_archive_texture.h>
#include <wx_asset_manager/ovr_archive_mesh.h>
#include <wx_asset_manager/ovr_archive_material.h>
#include <wx_asset_manager/ovr_archive_skeleton.h>
#include <wx_asset_manager/ovr_archive_pose.h>

#include <wx_project/ovr_project.h>
#include <wx_project/ovr_asset_manager.h>
#include <wx_core/ovr_scene_context.h>
#include <wx_core/ovr_sequence.h>

#include <wx_core/track/ovr_track.h>
#include <wx_core/track/ovr_actor_track.h>
#include <wx_core/track/ovr_event_track.h>
#include <wx_core/track/ovr_interactive_track.h>
#include <wx_core/track/ovr_background_audio_track.h>
#include <wx_core/track/ovr_mark_track.h>
#include <wx_core/track/ovr_clip.h>
#include <wx_core/track/ovr_float_track.h>

#include <wx_motion/ovr_skeletal_animation_sequence.h>
#include <wx_motion/ovr_pose_animation_sequence.h>
#include <wx_project/ovr_scene_event_manager.h>

#include <wx_core/visual_node/i_ovr_visual_node.h>
#include <wx_core/visual_node/i_ovr_node_cluster.h>
#include <wx_core/visual_node/i_ovr_node_manager.h>


#include "ovr_helper.h"

//include header
#include "project/ovr_project_manager.h"
#include "project/ovr_configer.h"

//common macro or fun
#define kPrjFileTail ".oproject"
#define kMatFileTail ".mat"
#define kSceneFileTail ".oscene"
#define kSeqFileTail ".oseqence"

enum ResDirType { kRDTRoot, kRDTRes, kRDTPrjScene, kRDTPrjSeq, kRDTPrjSys };

#define PRJ_DFAULT_SCENE_FILE "project/scene/default_scene.oscene"
#define PRJ_DFAULT_SEQ_FILE "project/sequence/default_seq.oseqence"

#define kTMLSectionHeadIndex 0
#define kTMLSectionLineIndex 1
#define kTMLSectionValueIndex 2
#define kTMLSectionLineWidth 1

#define kFloatZero 0.000001
#define Float_Equal(f1,f2) (fabs(f1 - f2) <= kFloatZero)

QString getImagePath(const char* name);
QString GetCaptureSavedFilePath(bool a_is_motion);
QString GetAppStyleSheet();

void ApplyWidgetStyleSheet(QWidget* a_widget);


const QString& getVScrollBarStyle();
const QString& getHScrollBarStyle();

const char* GetMenuStyle();

QIcon getActorIcon(ovr_engine::OvrActor* a_actor);

bool DragAsset(QObject* a_source, const QString& a_asset_path);

struct OvrDataItemString
{
	QString item_id;
	QString item_path;

	OvrDataItemString(const QString& a_id, const QString& a_path) {
		item_id = a_id;
		item_path = a_path;
	}

	void operator = (const OvrDataItemString& a_item)
	{
		item_id = a_item.item_id;
		item_path = a_item.item_path;
	}
};

struct OvrDataItemString2
{
	QString item_id;
	OvrString item_name;

	OvrDataItemString2(const QString& a_id, const OvrString& a_path) {
		item_id = a_id;
		item_name = a_path;
	}

	void operator = (const OvrDataItemString2& a_item)
	{
		item_id = a_item.item_id;
		item_name = a_item.item_name;
	}
};
