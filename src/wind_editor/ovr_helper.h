#pragma once

#include <QObject>

class OvrEventHelper;
class OvrEventReceiver;

class OvrQtOpenglContext;

class OvrHelper: public QObject
{
	Q_OBJECT

public:
	static OvrHelper* getInst();

	Q_INVOKABLE OvrHelper(QObject *parent = Q_NULLPTR);
	~OvrHelper();

	void warning(const char* a_text, QWidget* a_parent = nullptr);	
	void warning(const QString& a_text, QWidget* a_parent = nullptr);
	void warning(const char* a_text, const char* a_title, QWidget* a_parent = nullptr);

	void info(const char* a_text, const char* a_title, QWidget* a_parent);

	int  warningSave();

	void setGLHandle(QWindow* a_w) { gl_handle = a_w; }
	OvrQtOpenglContext* initOpenGL();

	int selectProjectFile(QString& a_file_path);
	
	QString getAppTitle(const QString& a_project_tile);
	void setAppTitle(const QString& a_project_tile);

	QWidget* getMainWindow();

	void showStatus(const QString& a_msg, int a_time);

	void registerEventListener(const QStringList& a_event_name, OvrEventReceiver* a_receiver);
	void registerEventListener(const QString& a_event_name, OvrEventReceiver* a_receiver);

	void unRegisterEventListener(const QStringList& a_event_name, OvrEventReceiver* a_receiver);
	void unRegisterEventListener(const QString& a_event_name, OvrEventReceiver* a_receiver);

signals:
	void onEventSend(QString a_event_name, QVariant a_param1, QVariant a_param2);

protected slots:
	void onEventReceive(QString a_event_name, QVariant a_param1, QVariant a_param2);

protected:
	void notifyEvent(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2, const QString& a_sender, bool a_is_async = true);
	
	void doWarning(const QString& a_text, QString a_title, QWidget* a_parent);
	void checkLogFlag();

	QMap<QString, OvrEventHelper*> events_map;

	QWidget* main_wnd = nullptr;
	QWindow* gl_handle = nullptr;
	OvrQtOpenglContext* opengl_context = nullptr;

	friend class OvrEventSender;
};

class OvrEventHelper
{
public:
	void addReceiver(OvrEventReceiver* a_receiver);
	void rmvReceiver(OvrEventReceiver* a_receiver);
	void notyfyAll(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2);
	int getCount() { return receivers.size(); }

protected:
	QList<OvrEventReceiver*> receivers;
};

class OvrEventReceiver
{
public:
	OvrEventReceiver();
	virtual ~OvrEventReceiver();

	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) = 0;
	virtual QString name() = 0;

protected:
	void registerEventListener(const QStringList& a_event_name);
	void registerEventListener(const QString& a_event_name);

	void unRegisterEventListener(const QStringList& a_event_name);
	void unRegisterEventListener(const QString& a_event_name);

private:
	bool is_registered = false;
	QStringList events;
};

class OvrEventSender
{
protected:
	void notifyEvent(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2, bool a_is_async = true) const;
	virtual QString senderName() const = 0 ;
};

struct MenuItemDef
{
	MenuItemDef(const char* a_title, const char* a_name, int a_data = 0)
	{
		title = a_title; name = a_name; data = a_data;
	}

	const char* title;
	const char* name;
	int data;
};

class QMenu;
class QAction;

void addMenuItems(QMenu* a_menu, const MenuItemDef* a_items, int a_cnt, QList<QAction*>* actions = nullptr);

QAction* addMenuItem(QMenu* a_menu, const char* a_title, const char* a_name, int a_data);

ovr_engine::OvrActor* OvrActorID2Actor(const QString& a_actor_id);