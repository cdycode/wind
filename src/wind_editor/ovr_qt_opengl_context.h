﻿#ifndef OVR_EDITOR_OVR_QT_OPENGL_CONTEXT_H_
#define OVR_EDITOR_OVR_QT_OPENGL_CONTEXT_H_

class QWindow;
class QOpenGLContext;

class OvrQtOpenglContext : public ovr_project::IOvrRenderContext 
{
public:
	OvrQtOpenglContext(QWindow* a_surface):render_surface(a_surface){}
	virtual ~OvrQtOpenglContext() {}

	//初始化和反初始化 分别在渲染线程启动和退出时调用
	virtual bool	OnInit() override;
	virtual void	OnUninit() override;

	//每帧都调用
	virtual bool	OnRender() override;

	//判断是否有效
	virtual bool	IsValid() override;

	virtual ovr::uint32	GetWidth() override;
	virtual ovr::uint32	GetHeight() override;

private:
	QWindow*		render_surface = nullptr;
	QOpenGLContext* opengl_context = nullptr;
};

#endif
