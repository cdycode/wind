#include "Header.h"
#include "mainwindow.h"

static void unInit()
{
	ovr_project::OvrProject::GetIns()->Close();
	ovr_project::OvrProject::GetIns()->Uninit();
	ovr_project::OvrProject::DelIns();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
	a.setWindowIcon(QIcon(getImagePath("app_icon.png")));

	//打开历史工程或者新建工程
	QString prj_path;
	int result = OvrHelper::getInst()->selectProjectFile(prj_path);
	if (result != 0)
	{
		ovr_project::OvrProject::GetIns()->Init();

		//创建主窗口
		MainWindow w;
		w.resize(1280, 800);

		OvrHelper::getInst()->setAppTitle("");

		w.showMaximized();

		if (!prj_path.isEmpty())
		{
			result == 1 ? OvrProjectManager::getInst()->openProject(prj_path) : OvrProjectManager::getInst()->newProject(prj_path);
		}

		result = a.exec();

		unInit();
	}
	return result;
}
