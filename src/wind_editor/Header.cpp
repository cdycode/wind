#include "Header.h"

#include <QDrag>

QString getImagePath(const char* name)
{
	QString dir = QApplication::applicationDirPath();
	dir += "/resource/";
	dir += name;
	return dir;
}

void init_list(QString& style_sheet)
{
	style_sheet += "";
}

QString GetAppStyleSheet()
{
	QString style_sheet;

	style_sheet += "QWidget {background:#3E3E3E; color:red}";

	init_list(style_sheet);

	{
		style_sheet += "QWidget#TimeLine_Bar {background:#3E3E3E}";
		style_sheet += "QWidget[class=OvrTimelineBar] {background:#3E3E3E}";
	}

	return style_sheet;
}

void ApplyWidgetStyleSheet(QWidget* a_widget)
{
	assert(a_widget != nullptr);

	QStyleOption opt;
	opt.init(a_widget);
	QPainter painter(a_widget);
	a_widget->style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, a_widget);
}

const QString& getVScrollBarStyle()
{
	static QString scroll_bar_style = ""
		"QScrollBar{background-color:rgb(35, 35, 35);width:16px; margin:16px 0 16px 0; border:0px solid rgb(135, 135, 35);}"
		"QScrollBar::handle{background:rgb(119, 119, 119); border: 2px solid rgb(35, 35, 35); background-clip: border; border-radius:7px;}"
		"QScrollBar::handle:hover{background:rgb(129, 129, 129);}"
		"QScrollBar::sub-line:vertical {background:rgba(35, 35, 35); height: 16px; subcontrol-position: top;subcontrol-origin: margin;}"
		"QScrollBar::sub-line:hover {background:rgba(135, 135, 135, 50); border-top-left-radius:4px; border-top-right-radius:4px; height: 15px; subcontrol-position: top;subcontrol-origin: margin;}"
		"QScrollBar::sub-line:pressed {background:rgba(135, 135, 135, 30); border-top-left-radius:4px; border-top-right-radius:4px; height: 15px; subcontrol-position: top;subcontrol-origin: margin;}"
		"QScrollBar::add-line:vertical {background:rgba(35, 35, 35); height: 16px; subcontrol-position: bottom;subcontrol-origin: margin;}"
		"QScrollBar::add-line:hover {background:rgba(135, 135, 135, 50); border-bottom-left-radius:4px; border-bottom-right-radius:4px; height: 15px; subcontrol-position: bottom;subcontrol-origin: margin;}"
		"QScrollBar::add-line:pressed {background:rgba(135, 135, 135, 30); border-bottom-left-radius:4px; border-bottom-right-radius:4px; height: 15px; subcontrol-position: bottom;subcontrol-origin: margin;}"
		"QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {background:rgb(35, 35, 35);}";

	static bool is_inited = false;
	if (!is_inited)
	{
		is_inited = true;
		scroll_bar_style += "QScrollBar::up-arrow {image: url(" + getImagePath("common/icon_scroll_top.png") + ");}";
		scroll_bar_style += "QScrollBar::down-arrow {image: url(" + getImagePath("common/icon_scroll_bottom.png") + ");}";
	}
	return scroll_bar_style;
}

const QString& getHScrollBarStyle()
{
	static QString h_scroll_bar_style = ""
		"QScrollBar:horizontal {background-color:rgb(35, 35, 35); height:16px; margin:0 16px 0 16px; border:0px solid rgb(135, 135, 35);}"
		"QScrollBar::handle:horizontal{background:rgb(119, 119, 119); border: 2px solid rgb(35, 35, 35); background-clip: border; border-radius:7px; min-width: 20px;}"
		"QScrollBar::handle:horizontal:hover{background:rgb(129, 129, 129);}"

		"QScrollBar::sub-line:horizontal {background:rgb(35, 35, 35); width:16px; subcontrol-position:left; subcontrol-origin:margin;}"
		"QScrollBar::sub-line:horizontal:hover {background:rgba(135, 135, 135, 50); border-top-left-radius:4px; border-bottom-left-radius:4px; width:15px; subcontrol-position: left;subcontrol-origin: margin;}"
		"QScrollBar::sub-line:horizontal:pressed {background:rgba(135, 135, 135, 30); border-top-left-radius:4px; border-bottom-left-radius:4px; width:15px; subcontrol-position: left;subcontrol-origin: margin;}"

		"QScrollBar::add-line:horizontal {background:rgb(35, 35, 35); width: 16px; subcontrol-position: right;subcontrol-origin: margin;}"
		"QScrollBar::add-line:horizontal:hover {background:rgba(135, 135, 135, 50); border-bottom-right-radius:4px; border-top-right-radius:4px; width: 15px; subcontrol-position: right; subcontrol-origin: margin;}"
		"QScrollBar::add-line:horizontal:pressed {background:rgba(135, 135, 135, 30); border-bottom-right-radius:4px; border-top-right-radius:4px; width: 15px; subcontrol-position: right; subcontrol-origin: margin;}"

		"QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal {background:rgb(35, 35, 35);}";

	static bool is_inited = false;
	if (!is_inited)
	{
		is_inited = true;
		h_scroll_bar_style += "QScrollBar::left-arrow {image: url(" + getImagePath("common/icon_scroll_left.png") + ");}";
		h_scroll_bar_style += "QScrollBar::right-arrow {image: url(" + getImagePath("common/icon_scroll_right.png") + ");}";
	}

	return h_scroll_bar_style;
}

const char* GetMenuStyle()
{
	static 	const char* strMenuStyle =
		"QMenu {background:rgb(39,39,39); color:rgb(205,205,205);"
		"selection-background-color: rgb(60,60,60);"
		"selection-color: rgb(205,205,205);}"
		"QMenu::item:disabled {color:gray}";
	return strMenuStyle;
}

QIcon getActorIcon(ovr_engine::OvrActor* a_actor)
{
	/*if (a_actor->GetType() == ovr_engine::kObjectMesh)
	{
		return QIcon(getImagePath("static_actor.png"));
	}
	else if (a_actor->GetType() == ovr_engine::kObject3DAudio)
	{
		return QIcon(getImagePath("audio.png"));
	}*/
	return QIcon(getImagePath("skeletal_actor.png"));
}

bool DragAsset(QObject* a_source, const QString& a_asset_path)
{
	QDrag *drag = new QDrag(a_source);
	{
		QMimeData *mimeData = new QMimeData;
		mimeData->setText(a_asset_path);
		mimeData->setProperty("drag_type", "asset_drag");
		mimeData->setProperty("drag_value", a_asset_path);
		drag->setMimeData(mimeData);
	}
	if (Qt::IgnoreAction == drag->exec(Qt::CopyAction | Qt::LinkAction))
	{
		qDebug() << "drag_drop not done";
		return false;
	}
	return true;
}
