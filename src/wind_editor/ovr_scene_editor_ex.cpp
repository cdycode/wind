#include "Header.h"
#include "ovr_scene_editor_ex.h"

#include <wx_core/ovr_scene_camera.h>
#include <wx_project/ovr_scene_editor.h>
#include "utils_ui/size_dock_widget.h"


/////////////////////////////////////////////////////
// class OvrSceneEditorPane

OvrSceneEditorPane::OvrSceneEditorPane(QWidget* parent, Qt::WindowFlags f)
	: QDockWidget(QString::fromLocal8Bit("预览窗口"), parent, f)
{
	setObjectName(QStringLiteral("dockWidget_right"));
	setFeatures(QDockWidget::NoDockWidgetFeatures);

	QSizePolicy size_policy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	size_policy.setHorizontalStretch(0);
	size_policy.setVerticalStretch(0);
	size_policy.setHeightForWidth(sizePolicy().hasHeightForWidth());
	setSizePolicy(size_policy);

	init_custom_ui();
}

void OvrSceneEditorPane::actionTriggered(QAction *action)
{
	auto scene = ovr_project::OvrProject::GetIns()->GetCurrentScene();
	
	QString name = action->objectName();
	if (name == "preview")
	{
		//onPreview(action);	
	}
	else if (name == "move")
	{
		onMove(action);
	}
	else if (name == "rotate")
	{
		onRotate(action);
	}
	else if (name == "scale")
	{
		onScale(action);
	}
}

OvrSceneEditorPane::~OvrSceneEditorPane()
{
}

void OvrSceneEditorPane::init_custom_ui()
{
	init_dock_title_bar();
	init_dock_content_widget();
}

void OvrSceneEditorPane::init_dock_title_bar()
{
	QWidget * new_title_widget = new QWidget();
	setTitleBarWidget(new_title_widget);

	QHBoxLayout * h_layout = new QHBoxLayout(new_title_widget);
	h_layout->setSpacing(0);
	h_layout->setContentsMargins(0, 0, 0, 0);

	auto res_bar = new QToolBar(new_title_widget);
	h_layout->addWidget(res_bar);
	{
		QAction * action = nullptr;

		action = res_bar->addAction(QIcon(getImagePath("save_big.png")), "");
		action->setToolTip(QString::fromLocal8Bit("保存场景"));
		action->setObjectName("save");

		action = res_bar->addAction(QIcon(getImagePath("icon_seperator.png")), "");
		action->setDisabled(true);

		action = res_bar->addAction(QIcon(getImagePath("move.png")), "");
		action->setToolTip(QString::fromLocal8Bit("移动"));
		action->setObjectName("move");
		action->setCheckable(true);
		action->setChecked(true);
		move_action = action;

		action = res_bar->addAction(QIcon(getImagePath("rotate.png")), "");
		action->setToolTip(QString::fromLocal8Bit("旋转"));
		action->setObjectName("rotate");
		action->setCheckable(true);
		rotate_action = action;

		action = res_bar->addAction(QIcon(getImagePath("scale.png")), "");
		action->setToolTip(QString::fromLocal8Bit("缩放"));
		action->setObjectName("scale");
		action->setCheckable(true);
		scale_action = action;

		action = res_bar->addAction(QIcon(getImagePath("icon_seperator.png")), "");
		action->setDisabled(true);

		//action = bar->addAction(QIcon(""), QString::fromLocal8Bit("预览"));
		//action->setToolTip(QString::fromLocal8Bit("预览模式"));
		//action->setObjectName("preview");
		//action->setCheckable(true);

		QLabel* space = new QLabel;
		space->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
		action = res_bar->addWidget(space);
		action->setDisabled(true);
#if 0
		action = res_bar->addAction(QIcon(getImagePath("icon_max.png")), "", [this]() {
			//setFloating(!isFloating());
			//if (!isFloating()) {
			//	dynamic_cast<MainWindow*>(parent())->setCentralWidget(this);
			//}
		});
		action->setToolTip(QString::fromLocal8Bit("放大窗口"));
		action->setObjectName("zoom");
#endif
	}
	connect(res_bar, &QToolBar::actionTriggered, this, &OvrSceneEditorPane::actionTriggered, Qt::QueuedConnection);
}

void OvrSceneEditorPane::init_dock_content_widget()
{
	SizeDockWidget * dockWidgetContents = new SizeDockWidget();
	QVBoxLayout* vbox_layout = new QVBoxLayout(dockWidgetContents);
	vbox_layout->setSpacing(0);
	vbox_layout->setContentsMargins(0, 0, 0, 0);
	setWidget(dockWidgetContents);

	dockWidgetContents->updateSizeHint(QSize(280, -1));

	auto scene_editor = new OvrSceneEditorEx();
	scene_editor->setObjectName("Scene_Editor");
	vbox_layout->addWidget(scene_editor);
}

void OvrSceneEditorPane::paintEvent(QPaintEvent *event)
{
	ApplyWidgetStyleSheet(this);
}

void OvrSceneEditorPane::onPreview(QAction* a_action)
{
	auto scene = ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor();

	if (scene != nullptr)
	{
		if (a_action->isChecked())
		{
			scene->SetMode(ovr_project::OvrSceneEditor::kPreview);
			a_action->setToolTip(QString::fromLocal8Bit("编辑模式"));
		}
		else
		{
			scene->SetMode(ovr_project::OvrSceneEditor::kEditor);
			a_action->setToolTip(QString::fromLocal8Bit("预览模式"));
		}
	}
}

void OvrSceneEditorPane::onMove(QAction* a_action)
{
	auto scene = ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor();

	if (scene == nullptr || !move_action->isChecked())
	{
		move_action->setChecked(true);
		return;
	}
	rotate_action->setChecked(false);
	scale_action->setChecked(false);

	scene->SetActorAixsType(ovr_project::kAxisTransform);
}

void OvrSceneEditorPane::onRotate(QAction* a_action)
{
	auto scene = ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor();

	if (scene == nullptr)
	{
		rotate_action->setChecked(false);
		return;
	}
	if (!rotate_action->isChecked())
	{
		rotate_action->setChecked(true);
		return;
	}
	move_action->setChecked(false);
	scale_action->setChecked(false);

	scene->SetActorAixsType(ovr_project::kAxisRotation);
}

void OvrSceneEditorPane::onScale(QAction* a_action)
{
	auto scene = ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor();

	if (scene == nullptr)
	{
		scale_action->setChecked(false);
		return;
	}
	if (!scale_action->isChecked())
	{
		scale_action->setChecked(true);
		return;
	}
	move_action->setChecked(false);
	rotate_action->setChecked(false);
	scene->SetActorAixsType(ovr_project::kAxisScale);
}

/////////////////////////////////////////////////////
// class OvrSceneEditorPane

OvrSceneEditorEx::OvrSceneEditorEx(QWidget* parent, Qt::WindowFlags f ):QWidget(parent, f)
{
	setFocusPolicy(Qt::StrongFocus);
	setMouseTracking(true);
	setAcceptDrops(true);

	setAttribute(Qt::WA_NativeWindow);	//make it native window for OpenGL rendering
	auto gl_handle = windowHandle();
	gl_handle->setSurfaceType(QWindow::OpenGLSurface);
	OvrHelper::getInst()->setGLHandle(gl_handle);
}

OvrSceneEditorEx::~OvrSceneEditorEx()
{
}

void OvrSceneEditorEx::paintEvent(QPaintEvent *e)
{
	ApplyWidgetStyleSheet(this);
}

void OvrSceneEditorEx::mousePressEvent(QMouseEvent *e)
{
	if (e->button() == Qt::LeftButton)
	{
		ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor()->GetEventManager()->MouseLeftButtonPressed(true);
	}
	else if (e->button() == Qt::RightButton)
	{
		pt_mouse_right_last = e->pos();
		is_right_mouse_down = true;
	}

	QWidget::mousePressEvent(e);
}

void OvrSceneEditorEx::mouseReleaseEvent(QMouseEvent *e)
{
	if (e->button() == Qt::LeftButton)
	{
		ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor()->GetEventManager()->MouseLeftButtonPressed(false);
		is_left_mouse_down = false;		
	}
	else if (e->button() == Qt::RightButton)
	{
		is_right_mouse_down = false;
	}
	is_mouse_move = false;

	QWidget::mouseReleaseEvent(e);
}

void OvrSceneEditorEx::mouseMoveEvent(QMouseEvent *e)
{
	if (is_left_mouse_down)
	{
		is_mouse_move = true;
	}
	if (is_right_mouse_down)
	{
		is_mouse_move = true;

		ovr_core::OvrSceneContext* scene = ovr_project::OvrProject::GetIns()->GetCurrentScene();
		if (scene != nullptr)
		{
			int lx = e->x() - pt_mouse_right_last.x();
			int ly = e->y() - pt_mouse_right_last.y();
			scene->GetSceneCamera()->Yaw(lx * 0.01);		//向左移动  相机也向左移动
			scene->GetSceneCamera()->Pitch(ly * -0.01);	//向上移动 相机也向上移动  
			pt_mouse_right_last = e->pos();
		}
	}
	ovr_core::OvrSceneContext* scene = ovr_project::OvrProject::GetIns()->GetCurrentScene();

	ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor()->GetEventManager()->MouseMove(e->x(), e->y());

	QWidget::mouseMoveEvent(e);
}

void OvrSceneEditorEx::keyPressEvent(QKeyEvent *event)
{
	ovr_core::OvrSceneContext* scene = ovr_project::OvrProject::GetIns()->GetCurrentScene();
	if (scene == nullptr)
	{
		QWidget::keyPressEvent(event);
		return;
	}
	int code = event->key();
	int a = 0;
	switch (code)
	{
	case Qt::Key_A:
		scene->GetSceneCamera()->MoveLeft();
		break;
	case Qt::Key_D:
		scene->GetSceneCamera()->MoveRight();
		break;
	case Qt::Key_W:
		scene->GetSceneCamera()->MoveForward();
		break;
	case Qt::Key_S:
		scene->GetSceneCamera()->MoveBackward();
		break;
	case Qt::Key_C:
		if (event->modifiers() == Qt::ControlModifier)
		{
			//copy
			CopyActor();
		}
		break;
	case Qt::Key_Delete:
		//delete
		RemoveActor();
		break;
	case Qt::Key_F2:
		//rename
		RenameActor();
		break;
	default:
		QWidget::keyPressEvent(event);
		return;
	}
	event->accept();
}

void OvrSceneEditorEx::keyReleaseEvent(QKeyEvent *event)
{
	QWidget::keyReleaseEvent(event);
}

void OvrSceneEditorEx::focusInEvent(QFocusEvent *event)
{
	QWidget::focusInEvent(event);
}
void OvrSceneEditorEx::focusOutEvent(QFocusEvent *event)
{
	QWidget::focusOutEvent(event);
}

void OvrSceneEditorEx::resizeEvent(QResizeEvent *event)
{
	ovr_project::OvrProject::GetIns()->ResizeRenderWindow(event->size().width(), event->size().height());
}

//key Ctrl + c 
void OvrSceneEditorEx::CopyActor()
{
	ovr_project::OvrSceneEditor* scene_editor = ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor();
	ovr_engine::OvrActor* src_actor = scene_editor->GetSelector();

	if (src_actor != nullptr)
	{
		OvrString src_actor_name = src_actor->GetDisplayName();
		OvrString src_actor_id = src_actor->GetUniqueName();

		//scene_editor->CopyActor(src_actor_id);
	}
}

//key delete
void OvrSceneEditorEx::RemoveActor()
{
	ovr_project::OvrSceneEditor* scene_editor = ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor();
	ovr_engine::OvrActor* remove_actor = scene_editor->GetSelector();

	if (remove_actor != nullptr)
	{
		scene_editor->RemoveActor(remove_actor->GetUniqueName());
	}
}

//key F2
void OvrSceneEditorEx::RenameActor()
{
	//功能暂留
}

void OvrSceneEditorEx::dragEnterEvent(QDragEnterEvent *event)
{
	QVariant item_type = event->mimeData()->property("drag_type");
	QVariant item_value = event->mimeData()->property("drag_value");
	if (item_type.isValid() && item_value.isValid())
	{
		if (item_type.toString() == "asset_drag" && item_value.toString().endsWith(".mesh"))
		{
			event->accept();
			ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor()->GetEventManager()->BeginDragAsset(item_value.toString().toLocal8Bit().constData());
		}
	}
}

void OvrSceneEditorEx::dragMoveEvent(QDragMoveEvent *event) 
{
	event->accept();
	ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor()->GetEventManager()->MouseMove(event->pos().rx(), event->pos().ry());
}

void OvrSceneEditorEx::dropEvent(QDropEvent *event) 
{
	event->accept();
	ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor()->GetEventManager()->EndDragAsset(true);
}

void OvrSceneEditorEx::dragLeaveEvent(QDragLeaveEvent *event) 
{
	event->accept();
	ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor()->GetEventManager()->EndDragAsset(false);
}
