﻿#include "Header.h"
#include "ovr_qt_opengl_context.h"

//初始化和反初始化 分别在渲染线程启动和退出时调用
bool OvrQtOpenglContext::OnInit()
{
	//设置抗锯齿
	QSurfaceFormat new_format = render_surface->requestedFormat();
	new_format.setSamples(4);
	render_surface->setFormat(new_format);

	opengl_context = new QOpenGLContext();
	opengl_context->setFormat(render_surface->requestedFormat());
	return opengl_context->create() && opengl_context->makeCurrent(render_surface);
}

void OvrQtOpenglContext::OnUninit()
{
	if (nullptr != opengl_context)
	{
		opengl_context->deleteLater();
		opengl_context = nullptr;
	}
	render_surface = nullptr;
}

//每帧都调用
bool OvrQtOpenglContext::OnRender()
{
	if (render_surface->isExposed())
	{
		opengl_context->makeCurrent(render_surface);
		opengl_context->swapBuffers(render_surface);
	}
	return true;
}

//判断是否有效
bool OvrQtOpenglContext::IsValid()
{
	return true;
}

ovr::uint32	OvrQtOpenglContext::GetWidth()
{
	if (nullptr != render_surface)
	{
		return render_surface->width();
	}
	return 0;
}
ovr::uint32	OvrQtOpenglContext::GetHeight() 
{
	if (nullptr != render_surface)
	{
		return render_surface->height();
	}
	return 0;
}
