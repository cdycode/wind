#include "Header.h"

#include "ovr_player_listener.h"

static OvrPlayerListener* g_listener = nullptr;

OvrPlayerListener* OvrPlayerListener::getInst()
{
	if (g_listener == nullptr)
	{
		g_listener = new OvrPlayerListener;
	}
	return g_listener;
}

OvrPlayerListener::OvrPlayerListener(QObject *parent):QObject(parent)
{}


OvrPlayerListener::~OvrPlayerListener()
{}


void OvrPlayerListener::OnPlayPosition(ovr::uint64 a_frame_index)
{
	emit onPlayPosChanged(a_frame_index);
}

//通知状态变化  在UI层 Seek也按暂停处理
void OvrPlayerListener::OnPlayStatus(ovr_core::OvrRenderStatus a_new_status)
{
	switch (a_new_status)
	{
	case ovr_core::kRenderPlay:
		emit onPlayStatusChanged("play");
		//notifyEvent("onPlayStatusChanged", "play", 0);
		break;
	case ovr_core::kRenderPause:
	case ovr_core::kRenderSeek:
	default:
		emit onPlayStatusChanged("pause");
		//notifyEvent("onPlayStatusChanged", "pause", 0);
		break;
	}
}

void OvrPlayerListener::OnOpenScene(ovr_core::OvrSceneContext* a_new_scene)
{
	//OvrProjectManager::getInst()->onOpenScene(a_new_scene);
	OvrHelper::getInst()->showStatus(QString::fromLocal8Bit("准备就绪"), 0);

	notifyEvent("onCurrentSceneChanged", 0, 0);

}

void OvrPlayerListener::OnOpenSequence(ovr_core::OvrSequence* a_new_sequence)
{
	notifyEvent("onOpenSequence", QVariant::fromValue((void*)a_new_sequence), 0);
}

void OvrPlayerListener::OnAddActor(const OvrString& a_actor_unique_name)
{
	notifyEvent("OnAddActor", QString::fromLocal8Bit(a_actor_unique_name.GetStringConst()), 0);
}
void OvrPlayerListener::OnRmvActor(const OvrString& a_actor_unique_name)
{
	notifyEvent("OnRmvActor", QString::fromLocal8Bit(a_actor_unique_name.GetStringConst()), 0);
}

void OvrPlayerListener::OnSetSelectedItem(const char* selected_id)
{
	notifyEvent("OnSetSelectedItem", QString::fromLocal8Bit(selected_id), 0);
}

void OvrPlayerListener::OnUpdateTransform(const char* selected_id)
{
	notifyEvent("OnUpdateTransform", QString::fromLocal8Bit(selected_id), 0);
}

void OvrPlayerListener::OnComponentAdd(ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_component)
{
	notifyEvent("OnComponentAdd", QVariant::fromValue((void*)a_actor), QVariant::fromValue((void*)a_component));
}

void OvrPlayerListener::OnComponentDel(ovr_engine::OvrActor* a_actor, int a_com_type)
{
	notifyEvent("OnComponentDel", QVariant::fromValue((void*)a_actor), a_com_type);
}

void OvrPlayerListener::OnComponentChangeVideoType(ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_component)
{
	notifyEvent("OnComponentChangeVideoType", QVariant::fromValue((void*)a_actor), QVariant::fromValue((void*)a_component));
}

void OvrPlayerListener::OnComponentChangeMovieType(ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_component)
{
	notifyEvent("OnComponentChangeMovieType", QVariant::fromValue((void*)a_actor), QVariant::fromValue((void*)a_component));
}