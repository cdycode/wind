#pragma once

#include <QObject>

class QSettings;

class OvrConfiger : public QObject
{
	Q_OBJECT

public:
	Q_INVOKABLE explicit OvrConfiger(QObject *parent = Q_NULLPTR);
	~OvrConfiger();

	void addMRUFile(const QString& a_prj_path);
	void removeMRUFile(const QString& a_prj_path);

	const QStringList& getMRUList() { return mru_list; }
	void clearMRUList();

	void setLastOpenPrjDir(const QString& a_dir);
	const QString& getLastOpenPrjDir() {
		return last_open_prj_dir;
	}

	const QString& getLastNewPrjDir() { return last_new_prj_dir; }
	void setLastNewPrjDir(const QString& a_dir);

	QString loadKeyValue(const QString& a_key, const QString& a_default_value, bool a_check_dir_exist = false);
	void setKeyValue(const QString& a_key, const QString& a_value);

protected:
	void init();
	void loadMRUList();
	void saveMRUFiles();
	void loadLastPrjDir();


	int mru_max;
	QStringList mru_list;
	QString last_new_prj_dir;
	QString last_open_prj_dir;

	QSettings* config;
};
