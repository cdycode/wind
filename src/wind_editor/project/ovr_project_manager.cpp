#include "Header.h"

#include "ovr_qt_opengl_context.h"
#include "ovr_player_listener.h"

static OvrProjectManager* g_inst = nullptr;

OvrProjectManager* OvrProjectManager::getInst()
{
	if (g_inst == nullptr)
	{
		g_inst = new OvrProjectManager;
	}
	return g_inst;
}

OvrProjectManager::OvrProjectManager(QObject *parent):QObject(parent)
{
	configer = new OvrConfiger(this);
}

bool OvrProjectManager::newProject(const QString& a_project_file)
{
	notifyEvent(QString::fromLocal8Bit("正在创建工程..."), 0, 0);

	resetProject();

	QDir dir(a_project_file);
	QString abs_file_path = dir.absolutePath();

	//create prj
	auto project_mainfest = ovr_project::OvrProject::GetIns();
	if (project_mainfest->Create(abs_file_path.toLocal8Bit().constData(), OvrHelper::getInst()->initOpenGL(), OvrPlayerListener::getInst()))
	{
		//create default scene
		if (project_mainfest->GetAssetManager()->CreateSence(PRJ_DFAULT_SCENE_FILE))
		{
			//get default scene info
			const ovr_project::OvrProjectFileInfo* scene_info = project_mainfest->GetAssetManager()->GetSceneInfo(PRJ_DFAULT_SCENE_FILE);
			if (nullptr != scene_info)
			{
				//create default sequence
				if (project_mainfest->GetAssetManager()->CreateSequcence(scene_info->unique_name.GetStringConst(), PRJ_DFAULT_SEQ_FILE))
				{
					onOpenPrjOK(abs_file_path, true);
					project_mainfest->OpenSceneFromFile(PRJ_DFAULT_SCENE_FILE);
					return true;
				}
			}
		}
	}
	onOpenPrjErr(abs_file_path, true);
	return false;
}

bool OvrProjectManager::openProject(const QString& a_project_file)
{
	OvrHelper::getInst()->showStatus(QString::fromLocal8Bit("正在打开工程..."), 0);

	resetProject();

	QDir dir(a_project_file);
	QString abs_file_path = dir.absolutePath();

	auto project_mainfest = ovr_project::OvrProject::GetIns();
	if (project_mainfest->Open(abs_file_path.toLocal8Bit().constData(), OvrHelper::getInst()->initOpenGL(), OvrPlayerListener::getInst()))
	{
		onOpenPrjOK(abs_file_path, false);
		return true;
	}

	onOpenPrjErr(abs_file_path, false);
	return false;
}


OvrProjectManager::~OvrProjectManager()
{
	ovr_project::OvrProject::DelIns();
}

bool OvrProjectManager::isNeedSave()
{
	return ovr_project::OvrProject::GetIns()->IsNeedSave();
}

void OvrProjectManager::saveIfNeeded()
{
	if (ovr_project::OvrProject::GetIns()->IsNeedSave())
	{
		if (QMessageBox::warning(OvrHelper::getInst()->getMainWindow(), "OVREdior", QString::fromLocal8Bit("工程已更改，是否保存？"), QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
		{
			saveProject();
		}
	}
}

bool OvrProjectManager::saveProject()
{
	if (ovr_project::OvrProject::GetIns()->IsNeedSave())
	{
		if (!ovr_project::OvrProject::GetIns()->Save()) //仅仅保存工程文件
		{
			return false;
		}
		configer->addMRUFile(project_file_path);
	}
	return true;
}

void OvrProjectManager::openScene(const QString& a_scene_id)
{
	assert(!a_scene_id.isEmpty());

	ovr_core::OvrSceneContext* scene_context = ovr_project::OvrProject::GetIns()->GetCurrentScene();
	if (scene_context != nullptr && a_scene_id == scene_context->GetScene()->GetUniqueName().GetStringConst())
	{
		return;
	}

	ovr_project::OvrProject::GetIns()->OpenSence(a_scene_id.toLocal8Bit().constData());
}

void OvrProjectManager::resetProject()
{
	ovr_project::OvrProject::GetIns()->Close();
	ovr_project::OvrProject::DelIns();
}

void OvrProjectManager::onOpenPrjOK(const QString& a_abs_file_path, bool a_is_new)
{
	OvrHelper::getInst()->showStatus(QString::fromLocal8Bit("工程已加载"), 0);

	ovr_project::OvrProject::GetIns()->SetListener(OvrPlayerListener::getInst());

	if (!a_is_new)
	{
		configer->addMRUFile(a_abs_file_path);
	}

	project_file_path = a_abs_file_path;
	QFileInfo fi(a_abs_file_path);
	project_dir = fi.path();
	OvrHelper::getInst()->setAppTitle(fi.baseName());
	notifyEvent("onProjectOpen", true, fi.path());
}

void OvrProjectManager::onOpenPrjErr(const QString& a_abs_file_path, bool a_is_new)
{
	if (!a_is_new)
	{
		configer->removeMRUFile(a_abs_file_path);
	}

	QString status = a_is_new ? QString::fromLocal8Bit("创建工程失败!") : QString::fromLocal8Bit("打开工程失败!");

	OvrHelper::getInst()->showStatus(status, 0);
	OvrHelper::getInst()->setAppTitle("");
	OvrHelper::getInst()->warning(status);
}

void OvrProjectManager::makeRelateDir(QString& a_dir)
{
	assert(a_dir.startsWith(project_dir));
	a_dir = a_dir.mid(project_dir.length() + 1);
}