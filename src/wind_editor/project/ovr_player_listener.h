#pragma once

#include <QObject>
#include <wx_project/ovr_project.h>

class OvrPlayerListener: public QObject, public ovr_project::IPlayerNotify, public ovr_project::OvrSceneListener, public OvrEventSender
{
	Q_OBJECT
public:
	static OvrPlayerListener* getInst();

	Q_INVOKABLE OvrPlayerListener(QObject *parent = Q_NULLPTR);
	~OvrPlayerListener();

	virtual QString senderName() const override { return "OvrPlayerListener"; }

	//OvrSceneListener
	virtual void OnAddActor(const OvrString& a_actor_unique_name) override;
	virtual void OnRmvActor(const OvrString& a_actor_unique_name);
	
	virtual void OnSetSelectedItem(const char* selected_id) override;
	virtual void OnUpdateTransform(const char* selected_id) override;

	virtual void OnComponentChangeVideoType(ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_component) override;
	virtual void OnComponentChangeMovieType(ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_component) override;

	
	virtual void OnComponentAdd(ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_component) override;
	virtual void OnComponentDel(ovr_engine::OvrActor* a_actor, int a_com_type) override;

	//IPlayerNotify
	virtual void OnPlayPosition(ovr::uint64 a_frame_index) override;
	virtual void OnPlayStatus(ovr_core::OvrRenderStatus a_new_status) override;
	virtual void OnOpenScene(ovr_core::OvrSceneContext* a_new_scene) override;
	virtual void OnOpenSequence(ovr_core::OvrSequence* a_new_sequence) override;

signals:
	void onPlayStatusChanged(QString a_status);
	void onPlayPosChanged(ovr::uint64 a_frame_index);
};

