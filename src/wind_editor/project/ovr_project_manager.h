#pragma once

#include <QObject>

class OvrConfiger;

class OvrProjectManager :public QObject, public OvrEventSender
{
	Q_OBJECT

public:
	static OvrProjectManager* getInst();
		
	Q_INVOKABLE explicit OvrProjectManager(QObject *parent = Q_NULLPTR);
	~OvrProjectManager();

	virtual QString senderName() const override { return "OvrProjectManager"; }

	bool newProject(const QString& a_project_file);
	bool openProject(const QString& a_project_file);

	bool isNeedSave();
	void saveIfNeeded();
	bool saveProject();

	OvrConfiger* getConfiger() { return configer; }

	void openScene(const QString& a_scene_id);
	void makeRelateDir(QString& a_dir);

protected:
	void onOpenPrjOK(const QString& a_abs_file_path, bool a_is_new);
	void onOpenPrjErr(const QString& a_abs_file_path, bool a_is_new);
	void resetProject();

	QString project_file_path;
	QString project_dir;

	OvrConfiger* configer;
};

