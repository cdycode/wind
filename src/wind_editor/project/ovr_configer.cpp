#include "Header.h"
#include "ovr_configer.h"

#define DEFAULT_MRU_COUNT 10
#define DEFAULT_MAX_MRU_COUNT 30

OvrConfiger::OvrConfiger(QObject *parent) :QObject(parent)
{
	init();

	loadMRUList();
	loadLastPrjDir();
}

OvrConfiger::~OvrConfiger()
{

}

void OvrConfiger::init()
{
	mru_max = DEFAULT_MRU_COUNT;

	config = new QSettings(QSettings::IniFormat, QSettings::UserScope, "Orange", "OVREditor", this);

	if (config->contains("mru_max"))
	{
		mru_max = config->value("mru_max").toInt();
		if (mru_max <= 0 || mru_max > DEFAULT_MAX_MRU_COUNT)
		{
			mru_max = DEFAULT_MRU_COUNT;
			config->setValue("mru_max", mru_max);
			config->sync();
		}
	}
	else
	{
		config->setValue("mru_max", mru_max);
		config->sync();
	}
}

void OvrConfiger::loadMRUList()
{
	QString key, value;
	for (int i = 0; i < mru_max; ++i)
	{
		key = QString::asprintf("mru_files/%d", i);
		if (config->contains(key))
		{
			value = config->value(key).toString();
			mru_list.append(value);
			continue;
		}
		break;
	}
}

void OvrConfiger::saveMRUFiles()
{
	QString key, value;
	int count = mru_list.count();
	for (int i = 0; i < mru_max; ++i)
	{
		key = QString::asprintf("mru_files/%d", i);
		if (i < count)
		{
			config->setValue(key, mru_list.at(i));
		}
		else if (config->contains(key))
		{
			config->remove(key);
		}
	}
	config->sync();
}

void OvrConfiger::addMRUFile(const QString& a_prj_path)
{
	assert(!a_prj_path.isEmpty());

	int index = mru_list.indexOf(a_prj_path);
	//添加新的
	if (index == -1)
	{
		mru_list.insert(0, a_prj_path);
		if (mru_list.count() >= mru_max)
		{
			mru_list.removeLast();
			qDebug() << "remove mru tail file for out of range";
		}
		saveMRUFiles();
	}
	//更新已有的
	else
	{
		//只有不是头的值才更新
		if (index > 0)
		{
			qDebug() << "update mru file";
			mru_list.move(index, 0);
			saveMRUFiles();
		}
	}
}

void OvrConfiger::setLastOpenPrjDir(const QString& a_dir)
{
	if (!a_dir.isEmpty() && a_dir != last_open_prj_dir)
	{
		QString key = "loadLastOpenPrjDir";
		last_open_prj_dir = a_dir;
		config->setValue(key, last_open_prj_dir);
		config->sync();
	}
}

void OvrConfiger::setLastNewPrjDir(const QString& a_dir)
{
	if (!a_dir.isEmpty() && a_dir != last_new_prj_dir)
	{
		QString key = "loadLastNewPrjDir";
		last_new_prj_dir = a_dir;
		config->setValue(key, last_new_prj_dir);
		config->sync();
	}
}

void OvrConfiger::loadLastPrjDir()
{
	last_new_prj_dir = loadKeyValue("loadLastNewPrjDir", QApplication::applicationDirPath(), true);
	last_open_prj_dir = loadKeyValue("loadLastOpenPrjDir", QApplication::applicationDirPath(), true);
}

void OvrConfiger::setKeyValue(const QString& a_key, const QString& a_value)
{
	assert(!a_key.isEmpty());
	config->setValue(a_key, a_value);
	config->sync();
}

QString OvrConfiger::loadKeyValue(const QString& a_key, const QString& a_default_value, bool a_check_dir_exist)
{
	QString value;
	if (config->contains(a_key))
	{
		value = config->value(a_key).toString();
		if (a_check_dir_exist)
		{
			QDir dir(value);
			if (!dir.exists())
			{
				value = a_default_value;
			}
		}
	}
	else
	{
		value = a_default_value;
	}
	return value;
}

void OvrConfiger::clearMRUList()
{
	mru_list.clear();
	saveMRUFiles();
}

void OvrConfiger::removeMRUFile(const QString& a_prj_path)
{
	int index = mru_list.indexOf(a_prj_path);
	if (index != -1)
	{
		QString key;
		int count = mru_list.count();
		mru_list.removeAt(index);
		key = QString::asprintf("mru_files/%d", count - 1);
		config->remove(key);
		saveMRUFiles();
	}
}