#include "Header.h"

#include "dialog/main_dlg.h"
#include "ovr_qt_opengl_context.h"

static OvrHelper* g_helper = nullptr;
static bool is_dump_event = false;

////////////////////////////////////////////////
// class OvrHelper

OvrHelper* OvrHelper::getInst()
{
	if (g_helper == nullptr)
	{
		g_helper = new OvrHelper;
	}
	return g_helper;
}

OvrHelper::OvrHelper(QObject *parent):QObject(parent)
{
	//checkLogFlag();

	connect(this, &OvrHelper::onEventSend, this, &OvrHelper::onEventReceive, Qt::QueuedConnection);
}

void OvrHelper::checkLogFlag()
{
	QString flag_file = QApplication::applicationDirPath() + "/dump_log.ini";
	if (QFile(flag_file).exists())
	{
		is_dump_event = true;
	}
}

OvrHelper::~OvrHelper()
{
	g_helper = nullptr;
}

void OvrHelper::warning(const char* a_text, QWidget* a_parent)
{
	doWarning(QString::fromLocal8Bit(a_text), QString(), a_parent);
}

void OvrHelper::warning(const QString& a_text, QWidget* a_parent)
{
	doWarning(a_text, QString(), a_parent);
}

void OvrHelper::warning(const char* a_text, const char* a_title, QWidget* a_parent)
{
	doWarning(QString::fromLocal8Bit(a_text), QString::fromLocal8Bit(a_title), a_parent);
}

void OvrHelper::info(const char* a_text, const char* a_title, QWidget* a_parent)
{
	QMessageBox msgBox(a_parent);

	msgBox.setIcon(QMessageBox::Icon::Information);
	msgBox.setText(QString::fromLocal8Bit(a_text));
	msgBox.setWindowTitle(QString::fromLocal8Bit(a_title));

	msgBox.setButtonText(QMessageBox::Ok, QString::fromLocal8Bit("确定"));

	msgBox.exec();
}

void OvrHelper::doWarning(const QString& a_text, QString a_title, QWidget* a_parent)
{
	if (a_parent == nullptr)
	{
		a_parent = getMainWindow();
	}

	if (a_title.isEmpty())
	{
		a_title = "OVREditor";
	}

	QMessageBox msgBox(a_parent);

	msgBox.setIcon(QMessageBox::Icon::Warning);
	msgBox.setText(a_text);
	msgBox.setWindowTitle(a_title);

	msgBox.setButtonText(QMessageBox::Ok, QString::fromLocal8Bit("确定"));

	msgBox.exec();
}

int OvrHelper::warningSave()
{
	QMessageBox::StandardButtons btns = QMessageBox::Close | QMessageBox::Cancel | QMessageBox::Save;
	QMessageBox box(QMessageBox::Warning, QString::fromLocal8Bit("关闭提示"), QString::fromLocal8Bit("工程已更改,是否保存?"), btns, getMainWindow());

	box.setButtonText(QMessageBox::Save, QString::fromLocal8Bit("保存"));
	box.setButtonText(QMessageBox::Close, QString::fromLocal8Bit("关闭"));
	box.setButtonText(QMessageBox::Cancel, QString::fromLocal8Bit("取消"));

	return box.exec();
}

OvrQtOpenglContext* OvrHelper::initOpenGL()
{
	if (opengl_context != nullptr)
	{
		delete opengl_context;
		opengl_context = nullptr;
	}

	assert(gl_handle != nullptr);

	opengl_context = new OvrQtOpenglContext(gl_handle);

	return opengl_context;
}


//return 1 if open, 2 if new
int OvrHelper::selectProjectFile(QString& a_file_path)
{
	a_file_path = "";

	MainDlg dlg;
	int ret = dlg.exec();
	a_file_path = ret == 0 ? "" : dlg.prj_file_path;

	return ret;
}

QString OvrHelper::getAppTitle(const QString& a_project_tile)
{
	QString title = a_project_tile;
	if (!title.isEmpty())
	{
		title += " - ";
	}
	title += "OVREditor(20180312)";
	return title;
}

void OvrHelper::setAppTitle(const QString& a_project_tile)
{
	getMainWindow()->setWindowTitle(getAppTitle(a_project_tile));
}

QWidget* OvrHelper::getMainWindow()
{
	if (main_wnd == nullptr)
	{
		for (auto w : QApplication::topLevelWidgets())
		{
			if (w->objectName() == "MainWindow")
			{
				main_wnd = w;
				break;
			}
		}
	}

	assert(main_wnd != nullptr);

	return main_wnd;
}

void OvrHelper::notifyEvent(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2, const QString& a_sender, bool a_is_async)
{
	if (a_is_async)
	{
		if (is_dump_event)
		{
			qDebug() << a_sender + QString::fromLocal8Bit(" 发送异步事件: ") << a_event_name;
		}

		emit onEventSend(a_event_name, a_param1, a_param2);
	}
	else
	{
		if (is_dump_event)
		{
			qDebug() << a_sender + QString::fromLocal8Bit(" 发送同步事件: ") << a_event_name;
		}

		onEventReceive(a_event_name, a_param1, a_param2);
	}
}

void OvrHelper::showStatus(const QString& a_msg, int a_time)
{
	notifyEvent("status", a_msg, a_time, "OvrHelper");
}

void OvrHelper::registerEventListener(const QStringList& a_event_name, OvrEventReceiver* a_receiver)
{
	for (auto name : a_event_name)
	{
		registerEventListener(name, a_receiver);
	}
}

void OvrHelper::registerEventListener(const QString& a_event_name, OvrEventReceiver* a_receiver)
{
	if (!a_event_name.isEmpty())
	{
		if (!events_map.contains(a_event_name))
		{
			if (is_dump_event)
			{
				qDebug() << "create event helper for " << a_event_name;
			}
			events_map[a_event_name] = new OvrEventHelper;
		}
		events_map[a_event_name]->addReceiver(a_receiver);
	}
}

void OvrHelper::unRegisterEventListener(const QStringList& a_event_name, OvrEventReceiver* a_receiver)
{
	for (auto name : a_event_name)
	{
		unRegisterEventListener(name, a_receiver);
	}

}

void OvrHelper::unRegisterEventListener(const QString& a_event_name, OvrEventReceiver* a_receiver)
{
	if (!a_event_name.isEmpty())
	{
		if (events_map.contains(a_event_name))
		{
			events_map[a_event_name]->rmvReceiver(a_receiver);
			if (events_map[a_event_name]->getCount() == 0)
			{
				if (is_dump_event)
				{
					qDebug() << "remove event helper for " << a_event_name;
				}
				auto obj = events_map[a_event_name];
				delete obj;
				events_map.remove(a_event_name);
			}
		}
	}
}

void OvrHelper::onEventReceive(QString a_event_name, QVariant a_param1, QVariant a_param2)
{
	if (events_map.contains(a_event_name))
	{
		events_map[a_event_name]->notyfyAll(a_event_name, a_param1, a_param2);
	}
	else
	{
		if (is_dump_event)
		{
			qDebug() << "事件未处理: " << a_event_name;
		}
	}
}

////////////////////////////////////
// class OvrEventHelper

void OvrEventHelper::addReceiver(OvrEventReceiver* a_receiver)
{
	if (receivers.indexOf(a_receiver) == -1)
	{
		if (is_dump_event)
		{
			qDebug() << "add event receiver";
		}
		receivers.push_back(a_receiver);
	}
}

void OvrEventHelper::rmvReceiver(OvrEventReceiver* a_receiver)
{
	if (is_dump_event)
	{
		qDebug() << "remove event receiver";
	}
	receivers.removeOne(a_receiver);
}

void OvrEventHelper::notyfyAll(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{
	assert(receivers.size() > 0);

	for (auto obj : receivers)
	{
		if (is_dump_event)
		{
			qDebug() << obj->name() << QString::fromLocal8Bit(" 处理事件: ") << a_event_name;
		}
		obj->onEventArrive(a_event_name, a_param1, a_param2);
	}
}

////////////////////////////////////
// class OvrEventHelper

OvrEventReceiver::OvrEventReceiver()
{

}

OvrEventReceiver::~OvrEventReceiver()
{
	if (events.size() > 0)
	{
		for (auto name : events)
		{
			OvrHelper::getInst()->unRegisterEventListener(name, this);
		}
		events.clear();
	}
}

void OvrEventReceiver::registerEventListener(const QStringList& a_event_name)
{
	for (auto name : a_event_name)
	{
		registerEventListener(name);
	}
}

void OvrEventReceiver::registerEventListener(const QString& a_event_name)
{
	if (events.indexOf(a_event_name) == -1)
	{
		OvrHelper::getInst()->registerEventListener(a_event_name, this);
		events << a_event_name;
	}
}

void OvrEventReceiver::unRegisterEventListener(const QStringList& a_event_name)
{
	for (auto name : a_event_name)
	{
		unRegisterEventListener(name);
	}
}

void OvrEventReceiver::unRegisterEventListener(const QString& a_event_name)
{
	if (!a_event_name.isEmpty() && events.indexOf(a_event_name) != -1)
	{
		OvrHelper::getInst()->unRegisterEventListener(a_event_name, this);
		events.removeOne(a_event_name);
	}
}

void OvrEventSender::notifyEvent(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2, bool a_is_async) const
{
	OvrHelper::getInst()->notifyEvent(a_event_name, a_param1, a_param2, senderName(), a_is_async);
}

void addMenuItems(QMenu* a_menu, const MenuItemDef* a_items, int a_cnt, QList<QAction*>* actions)
{
	for (int i = 0; i < a_cnt; ++i)
	{
		auto action = addMenuItem(a_menu, a_items[i].title, a_items[i].name, a_items[i].data);

		if (actions != nullptr && action != nullptr)
		{
			actions->push_back(action);
		}
	}
}

QAction* addMenuItem(QMenu* a_menu, const char* a_title, const char* a_name, int a_data)
{
	QAction* action = nullptr;

	auto title = QString::fromLocal8Bit(a_title);
	if (title.isEmpty())
	{
		a_menu->addSeparator();
	}
	else
	{
		action = a_menu->addAction(title);
		action->setObjectName(a_name);
		action->setData(a_data);
	}
	return action;
}

ovr_engine::OvrActor* OvrActorID2Actor(const QString& a_actor_id)
{
	if (!a_actor_id.isEmpty());
	{
		ovr_core::OvrSceneContext* scene = ovr_project::OvrProject::GetIns()->GetCurrentScene();
		if (scene != nullptr)
		{
			return scene->GetActorByUniqueName(a_actor_id.toLocal8Bit().constData());
		}
	}
	return nullptr;
}