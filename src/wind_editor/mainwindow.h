#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui 
{
	class MainWindow;
}

class MainWindow : public QMainWindow, public OvrEventReceiver, public OvrEventSender
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

	virtual QString senderName() const override { return "MainWindow"; }

	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;
	virtual QString name() override { return "MainWindow"; }

protected slots:
	void closeEvent(QCloseEvent *event);

protected:
	virtual void showEvent(QShowEvent *event) override;

private:
	void initStyle();

	void initUI();
	void initTitleBar();

	Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
