#include <Header.h>
#include "ovr_material_tree_item.h"

OvrMaterialTreeItem::OvrMaterialTreeItem()
	: QStandardItem()
{
}

OvrMaterialTreeItem::OvrMaterialTreeItem(const QString & text)
	: QStandardItem(text)
{
}

OvrMaterialTreeItem::OvrMaterialTreeItem(const QIcon & icon, const QString & text)
	: QStandardItem(icon, text)
{
}

OvrMaterialTreeItem::OvrMaterialTreeItem(int rows, int columns)
	: QStandardItem(rows, columns)
{
}

OvrMaterialTreeItem::~OvrMaterialTreeItem()
{
}

// OvrMaterialTreeItemGroup
OvrMaterialTreeItemDefault::OvrMaterialTreeItemDefault()
	: OvrMaterialTreeItem()
{
}

OvrMaterialTreeItemDefault::OvrMaterialTreeItemDefault(const QString & text)
	: OvrMaterialTreeItem(text)
{
}

OvrMaterialTreeItemDefault::OvrMaterialTreeItemDefault(const QIcon & icon, const QString & text)
	: OvrMaterialTreeItem(icon, text)
{
}

OvrMaterialTreeItemDefault::OvrMaterialTreeItemDefault(int rows, int columns)
	: OvrMaterialTreeItem(rows, columns)
{
}

OvrMaterialTreeItemDefault::~OvrMaterialTreeItemDefault()
{
}

// OvrMaterialTreeItemLineEdit
OvrMaterialTreeItemLineEdit::OvrMaterialTreeItemLineEdit(const QString &customData)
	: OvrMaterialTreeItem()
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemLineEdit::OvrMaterialTreeItemLineEdit(const QString &customData, const QString & text)
	: OvrMaterialTreeItem(text)
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemLineEdit::OvrMaterialTreeItemLineEdit(const QString &customData, const QIcon & icon, const QString & text)
	: OvrMaterialTreeItem(icon, text)
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemLineEdit::OvrMaterialTreeItemLineEdit(const QString &customData, int rows, int columns)
	: OvrMaterialTreeItem(rows, columns)
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemLineEdit::~OvrMaterialTreeItemLineEdit()
{
}

// OvrMaterialTreeItemPushButton
OvrMaterialTreeItemPushButton::OvrMaterialTreeItemPushButton(const QString &customData)
	: OvrMaterialTreeItem()
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemPushButton::OvrMaterialTreeItemPushButton(const QString &customData, const QString & text)
	: OvrMaterialTreeItem(text)
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemPushButton::OvrMaterialTreeItemPushButton(const QString &customData, const QIcon & icon, const QString & text)
	: OvrMaterialTreeItem(icon, text)
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemPushButton::OvrMaterialTreeItemPushButton(const QString &customData, int rows, int columns)
	: OvrMaterialTreeItem(rows, columns)
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemPushButton::~OvrMaterialTreeItemPushButton()
{
}

// OvrMaterialTreeItemCheckBox
OvrMaterialTreeItemCheckBox::OvrMaterialTreeItemCheckBox(bool value)
	: OvrMaterialTreeItem()
	, m_bChecked(value)
{
}

OvrMaterialTreeItemCheckBox::OvrMaterialTreeItemCheckBox(bool value, const QString & text)
	: OvrMaterialTreeItem(text)
	, m_bChecked(value)
{
}

OvrMaterialTreeItemCheckBox::OvrMaterialTreeItemCheckBox(bool value, const QIcon & icon, const QString & text)
	: OvrMaterialTreeItem(icon, text)
	, m_bChecked(value)
{
}

OvrMaterialTreeItemCheckBox::OvrMaterialTreeItemCheckBox(bool value, int rows, int columns)
	: OvrMaterialTreeItem(rows, columns)
	, m_bChecked(value)
{
}

OvrMaterialTreeItemCheckBox::~OvrMaterialTreeItemCheckBox()
{
}

// OvrMaterialTreeItemComboBox
OvrMaterialTreeItemComboBox::OvrMaterialTreeItemComboBox(int index)
	: OvrMaterialTreeItem()
	, m_nCurIndex(index)
{
}

OvrMaterialTreeItemComboBox::OvrMaterialTreeItemComboBox(int index, const QString & text)
	: OvrMaterialTreeItem(text)
	, m_nCurIndex(index)
{
}

OvrMaterialTreeItemComboBox::OvrMaterialTreeItemComboBox(int index, const QIcon & icon, const QString & text)
	: OvrMaterialTreeItem(icon, text)
	, m_nCurIndex(index)
{
}

OvrMaterialTreeItemComboBox::OvrMaterialTreeItemComboBox(int index, int rows, int columns)
	: OvrMaterialTreeItem(rows, columns)
	, m_nCurIndex(index)
{
}

OvrMaterialTreeItemComboBox::~OvrMaterialTreeItemComboBox()
{
}

// OvrMaterialTreeItemSpinBox
OvrMaterialTreeItemSpinBox::OvrMaterialTreeItemSpinBox(int value)
	: OvrMaterialTreeItem()
	, m_nCurValue(value)
{
}

OvrMaterialTreeItemSpinBox::OvrMaterialTreeItemSpinBox(int value, const QString & text)
	: OvrMaterialTreeItem(text)
	, m_nCurValue(value)
{
}

OvrMaterialTreeItemSpinBox::OvrMaterialTreeItemSpinBox(int value, const QIcon & icon, const QString & text)
	: OvrMaterialTreeItem(icon, text)
	, m_nCurValue(value)
{
}

OvrMaterialTreeItemSpinBox::OvrMaterialTreeItemSpinBox(int value, int rows, int columns)
	: OvrMaterialTreeItem(rows, columns)
	, m_nCurValue(value)
{
}

OvrMaterialTreeItemSpinBox::~OvrMaterialTreeItemSpinBox()
{
}

// OvrMaterialTreeItemDoubleSpinBox
OvrMaterialTreeItemDoubleSpinBox::OvrMaterialTreeItemDoubleSpinBox(double value)
	: OvrMaterialTreeItem()
	, m_dCurValue(value)
{
}

OvrMaterialTreeItemDoubleSpinBox::OvrMaterialTreeItemDoubleSpinBox(double value, const QString & text)
	: OvrMaterialTreeItem(text)
	, m_dCurValue(value)
{
}

OvrMaterialTreeItemDoubleSpinBox::OvrMaterialTreeItemDoubleSpinBox(double value, const QIcon & icon, const QString & text)
	: OvrMaterialTreeItem(icon, text)
	, m_dCurValue(value)
{
}

OvrMaterialTreeItemDoubleSpinBox::OvrMaterialTreeItemDoubleSpinBox(double value, int rows, int columns)
	: OvrMaterialTreeItem(rows, columns)
	, m_dCurValue(value)
{
}

OvrMaterialTreeItemDoubleSpinBox::~OvrMaterialTreeItemDoubleSpinBox()
{
}

// OvrMaterialTreeItemFilePathEdit
const QString OvrMaterialTreeItemFilePathEdit::cs_PathNull = QString::fromLocal8Bit("<NULL>");

OvrMaterialTreeItemFilePathEdit::OvrMaterialTreeItemFilePathEdit(const QString &customData)
	: OvrMaterialTreeItem()
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemFilePathEdit::OvrMaterialTreeItemFilePathEdit(const QString &customData, const QString & text)
	: OvrMaterialTreeItem(text)
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemFilePathEdit::OvrMaterialTreeItemFilePathEdit(const QString &customData, const QIcon & icon, const QString & text)
	: OvrMaterialTreeItem(icon, text)
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemFilePathEdit::OvrMaterialTreeItemFilePathEdit(const QString &customData, int rows, int columns)
	: OvrMaterialTreeItem(rows, columns)
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemFilePathEdit::~OvrMaterialTreeItemFilePathEdit()
{
}

// OvrMaterialTreeItemFilePathEdit
OvrMaterialTreeItemColorEdit::OvrMaterialTreeItemColorEdit(const QString &customData)
	: OvrMaterialTreeItem()
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemColorEdit::OvrMaterialTreeItemColorEdit(const QString &customData, const QString & text)
	: OvrMaterialTreeItem(text)
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemColorEdit::OvrMaterialTreeItemColorEdit(const QString &customData, const QIcon & icon, const QString & text)
	: OvrMaterialTreeItem(icon, text)
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemColorEdit::OvrMaterialTreeItemColorEdit(const QString &customData, int rows, int columns)
	: OvrMaterialTreeItem(rows, columns)
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemColorEdit::~OvrMaterialTreeItemColorEdit()
{
}

// OvrMaterialTreeItemCustomPopEdit
OvrMaterialTreeItemCustomPop::OvrMaterialTreeItemCustomPop(const QString &customData)
	: OvrMaterialTreeItem()
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemCustomPop::OvrMaterialTreeItemCustomPop(const QString &customData, const QString & text)
	: OvrMaterialTreeItem(text)
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemCustomPop::OvrMaterialTreeItemCustomPop(const QString &customData, const QIcon & icon, const QString & text)
	: OvrMaterialTreeItem(icon, text)
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemCustomPop::OvrMaterialTreeItemCustomPop(const QString &customData, int rows, int columns)
	: OvrMaterialTreeItem(rows, columns)
	, m_strCustomData(customData)
{
}

OvrMaterialTreeItemCustomPop::~OvrMaterialTreeItemCustomPop()
{
}

