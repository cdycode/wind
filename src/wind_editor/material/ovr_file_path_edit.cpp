#include <Header.h>
#include "ovr_file_path_edit.h"
#include "../dialog/ovr_file_dialog.h"

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QFileDialog>

OvrFilePathEdit::OvrFilePathEdit(QWidget *parent)
	: QFrame(parent)
{
	//ui.setupUi(this);
	init_custom_ui();
	set_connect_info();
}

OvrFilePathEdit::~OvrFilePathEdit()
{
}

void OvrFilePathEdit::paintEvent(QPaintEvent * event)
{
	ApplyWidgetStyleSheet(this);

}

void OvrFilePathEdit::setText(const QString & text)
{
	m_line_edit->setText(text);
	//m_lineEdit->selectAll();
}

void OvrFilePathEdit::setButtonText(const QString & text)
{
	m_btn_open->setText(text);
}

void OvrFilePathEdit::init_custom_ui()
{
	// 0. layout
	QHBoxLayout *horizontalLayout = new QHBoxLayout(this);
	horizontalLayout->setSpacing(1);
	//horizontalLayout->setContentsMargins(11, 11, 11, 11);
	horizontalLayout->setContentsMargins(0, 0, 0, 0);
	//horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));

	// 1. line edit
	m_line_edit = new QLineEdit(this);
	//lineEdit->setObjectName(QStringLiteral("lineEdit"));
	m_line_edit->setReadOnly(true);
	QObject::connect(m_line_edit, SIGNAL(textChanged(QString)), this, SLOT(change_edit_text(const QString &)));

	horizontalLayout->addWidget(m_line_edit);

	QString strStyleSheet = QString::fromLocal8Bit(""
		"background-color: rgb(164, 64, 64)); border: 1px solid rgb(35, 35, 35); border-radius: 0px;"
		//"QPushButton:hover{background-color: rgb(87, 87, 87);}"
		//"QPushButton:pressed{background-color: rgb(64, 64, 64);}"
		//"QPushButton:disabled{background-color: rgb(46, 46, 46); color:  rgb(140, 140, 140)}"
	);

	// 2. open btn
	m_btn_open = new QPushButton(QString::fromLocal8Bit("..."), this);
	//pushButton->setObjectName(QStringLiteral("pushButton"));
	m_btn_open->setMinimumWidth(18);
	//m_btn_open->setMinimumHeight(this->height());
	m_btn_open->setToolTip(QString::fromLocal8Bit("设置"));
	m_btn_open->setStyleSheet(strStyleSheet);

	QObject::connect(m_btn_open, SIGNAL(clicked()), this, SLOT(on_btn_open_clicked()));

	horizontalLayout->addWidget(m_btn_open);

	// 3. clear btn
	QPushButton* btn_clear = new QPushButton(QString::fromLocal8Bit("X"), this);
	//pushButton->setObjectName(QStringLiteral("pushButton"));
	btn_clear->setMinimumWidth(18);
	//btn_clear->setMinimumHeight(this->height());
	//btn_clear->setText(QString::fromLocal8Bit("X"));
	btn_clear->setToolTip(QString::fromLocal8Bit("清理"));
	btn_clear->setStyleSheet(strStyleSheet);

	QObject::connect(btn_clear, SIGNAL(clicked()), this, SLOT(on_btn_clear_clicked()));

	horizontalLayout->addWidget(btn_clear);

	QMetaObject::connectSlotsByName(this);
}

void OvrFilePathEdit::set_connect_info()
{
}

void OvrFilePathEdit::set_custom_style_sheet()
{
	//setStyleSheet(""
	//	"QPushButton{background-color: rgb(87, 87, 87));  border: 1px solid rgb(35, 35, 35); border-radius: 0px;}"
	//	"QPushButton:hover{background-color: rgb(118, 118, 118);}"
	//	"QPushButton:pressed{background-color: rgb(87, 87, 87);}"
	//	"QPushButton:disabled{background-color: rgb(67, 67, 67); color:  rgb(140, 140, 140)}"
	//);


}

void OvrFilePathEdit::change_edit_text(const QString &text)
{
	m_strCurPath = text;

	emit curPathChanged(m_strCurPath);
}

void OvrFilePathEdit::on_btn_clear_clicked()
{
	m_line_edit->clear();
	//m_lineEdit->setText("");
}

void OvrFilePathEdit::on_btn_open_clicked()
{
#if 1
	OvrFileDialog dlg;
	if (dlg.init(QString::fromLocal8Bit("贴图"), "tex files(*.tex)", m_line_edit->text()) && dlg.exec() == 1)
	{
		QString relative_path = dlg.getSelectFile();
		m_line_edit->setText(relative_path);
	}
#else
	QString title = QString::fromLocal8Bit("贴图");
	OvrString work_dir = ovr_project::OvrProject::GetIns()->GetWorkDir();
	QString strWorkDir = QString::fromLocal8Bit(work_dir.GetStringConst());
	QString strFilter = QString::fromLocal8Bit(
		"tex files(*.tex);;"
		"All files(*.*)"
	);

	QFileDialog * open_file_dlg = open_const_dir_dlg(title, strWorkDir, strFilter, this);
	if (open_file_dlg->exec() == QDialog::Accepted) 
	{
		QString strPath = open_file_dlg->selectedFiles()[0];
		int n_index = strPath.indexOf(strWorkDir);
		if (n_index >= 0)
		{  // 不在此目录里的文件不加载
			QString relative_path = strPath.right(strPath.length() - strWorkDir.length());
			m_line_edit->setText(relative_path);
		}
		else 
		{
			QMessageBox::warning(this, QString::fromLocal8Bit("贴图选择"),
				QString::fromLocal8Bit("非工程目录文件需要导入后才能使用！"));
		}
	}
#endif
}
