#pragma once

#include <QFrame>
//#include "ui_ovr_file_path_edit.h"

class OvrFilePathEdit : public QFrame
{
	Q_OBJECT

public:
	OvrFilePathEdit(QWidget *parent = Q_NULLPTR);
	~OvrFilePathEdit();

	virtual void paintEvent(QPaintEvent *event) override;

public:
	void setText(const QString & text);
	void setButtonText(const QString & text);

	void setCurPath(QString &a_cur_path) { m_strCurPath = a_cur_path; }
	QString getCurPath() { return m_strCurPath; }

Q_SIGNALS:
	void curPathChanged(QString);

private:
	void init_custom_ui();
	void set_connect_info();
	void set_custom_style_sheet();

private slots :
;
void change_edit_text(const QString &text);
void on_btn_clear_clicked();
void on_btn_open_clicked();

private:
	//Ui::OvrFilePathEdit ui;

	QString m_strCurPath;
	QLineEdit *m_line_edit = nullptr;
	QPushButton *m_btn_open = nullptr;
};
