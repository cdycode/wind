#pragma once

#include <memory>

#include <QDialog>
#include <QScrollArea>
#include <QLabel>
#include <QVector>
#include <QTreeView>

#include <QStandardItemModel>

#include "ui_ovr_material_editor.h"
#include "ovr_material_tree_item.h"

namespace ovr_engine
{
	class OvrActor;
}

namespace ovr_project
{
	struct OvrMaterialParam;
	class OvrMaterialProperty;
}


namespace ovr_asset
{
	class OvrSerializeManager;
}

class OvrResManager;
class OvrMaterialEditor : public QDialog
{
	Q_OBJECT

public:
	//OvrMaterialEditor(ovr_engine::OvrMaterial* a_mat, QWidget *parent = Q_NULLPTR);
	OvrMaterialEditor(const QString & a_mat_path, QWidget *parent = Q_NULLPTR);
	~OvrMaterialEditor();
	
private:
	Ui::OvrMaterialEditor ui;

	void init_custom_ui();
	void set_custom_style_sheet();
	void preset_connects();

	void init_ui_data();
	void init_tree_widget(QTreeView * a_tree);

	void set_tree_info(QTreeView * a_tree, const std::list<ovr_project::OvrMaterialParam>& a_properties);

	OvrMaterialTreeItemPushButton * get_push_button_item(const ovr_project::OvrMaterialParam& property);
	OvrMaterialTreeItemSpinBox * get_spin_box_item(const ovr_project::OvrMaterialParam& property);
	OvrMaterialTreeItemDoubleSpinBox * get_double_spin_box_item(const ovr_project::OvrMaterialParam& property);
	OvrMaterialTreeItemFilePathEdit * get_file_path_item(const ovr_project::OvrMaterialParam& property);
	OvrMaterialTreeItemComboBox * get_combo_box_item(const ovr_project::OvrMaterialParam & property);
	OvrMaterialTreeItemColorEdit * get_color_eidtor_item(const ovr_project::OvrMaterialParam & property);
	OvrMaterialTreeItemCustomPop * get_custom_pop_item(const ovr_project::OvrMaterialParam & property);

	void update_item_attribute(OvrMaterialTreeItem* a_item);

	QString color_setting_pop(OvrMaterialTreeItemCustomPop* pop_item, const QString &customData);
private:
	//ovr_engine::OvrMaterial* m_material = nullptr;

	std::unique_ptr<ovr_project::IOvrMaterialProperty> mat_property_mgr = nullptr;

	// 保存编辑过的类型及属性 的 原始信息
	OvrString original_material_type;
	QMap<OvrString, std::list<ovr_project::OvrMaterialParam>> all_original_edited_properties;

	QStandardItemModel * tree_model = nullptr;


	QPoint m_last;
	bool m_bIsPushed;

	bool m_bTreeItemChanged = false;

private slots:
	void on_ok_clicked();
	void on_cancel_clicked();
	void on_material_type_changed(int a_index);

	void on_material_set_msg_pop(int a_msg_type);

	void on_current_tree_modified(OvrMaterialTreeItem* a_item);

	void done(int r);
};

