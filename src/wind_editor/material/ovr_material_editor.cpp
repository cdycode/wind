#include "Header.h"
#include "ovr_material_editor.h"
#include "../res_view/ovr_res_manager.h"
#include "../utils_ui/title_bar.h"

#include <wx_project/property/i_ovr_material_property.h>

#include "ovr_material_tree_delegate.h"
#include  "../dialog/ovr_common_color_dlg.h"


OvrMaterialEditor::OvrMaterialEditor(const QString & a_mat_path, QWidget * parent)
	: QDialog(parent)
{
	ui.setupUi(this);

	QByteArray qbaMatPath = a_mat_path.toLocal8Bit();
	mat_property_mgr.reset(ovr_project::IOvrMaterialProperty::Create(qbaMatPath.constData()));

	init_custom_ui();
	set_custom_style_sheet();

	init_ui_data();
	preset_connects();

}

OvrMaterialEditor::~OvrMaterialEditor()
{
}

void OvrMaterialEditor::init_custom_ui()
{
	setWindowFlags(windowFlags() | Qt::FramelessWindowHint);

	ui.title_bar->setUiFlags(TitleBar::TitleFlag::TF_BTN_CLOSE);
	QString strTitle = QString::fromLocal8Bit("材质编辑");
	ui.title_bar->setTitle(strTitle);

	installEventFilter(ui.title_bar);
	setWindowTitle(strTitle);

	ui.ok_button->setDefault(true);
}

void OvrMaterialEditor::set_custom_style_sheet()
{
	// 0. Dialog
	QString strDefaultStyle = QString::fromLocal8Bit(
		"QWidget{color: rgb(204, 204, 204); background-color:rgb(48, 48, 48); font:12px \"微软雅黑\"; border:0px solid rgb(35, 35, 35); border-radius: 4px;}"
		"QWidget:focus{outline: none;}"

		"QToolTip{color: rgb(48,48,48); background-color:rgb(204, 204, 204);}"

		"QHeaderView::section {background-color:qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #616161, stop: 0.5 #505050, stop: 0.6 #434343, stop:1 #656565); color:rgb(204, 204, 204); padding-left:4px; border:1px solid #6c6c6c; }"

		"QTreeView{background-color: rgb(39, 39, 39); color: rgb(192, 192, 192); border: 1px solid rgb(24, 24, 24); border-top:0px;}"
		"QTreeView::item {height: 25px; border-bottom: 1px solid rgb(26, 26, 26); color: rgb(192, 192, 192);}"
		"QTreeView::item:!last {border-right: 1px solid rgb(26, 26, 26);}"
		"QTreeView::item::hover{background-color: rgb(46, 46, 46);}"
		"QTreeView::item:selected {background-color: rgb(64, 64, 64);}"
		"QTreeView::branch {height: 28px; border-bottom: 1px solid rgb(26, 26, 26);}"

		"QPushButton{width:138px; height:38px; color:rgb(204, 204, 204); border:1px solid rgb(30, 30, 30); border-radius:4px; background-color:qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); }"
		"QPushButton:hover{background-color:qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(153, 153, 153), stop:1 rgb(109, 109, 109)); }"
		"QPushButton:pressed{background-color:qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:1 rgb(118, 118, 118), stop:0 rgb(87, 87, 87)); }"
		"QPushButton:disabled{background-color:rgb(56, 56, 56); color:rgb(76, 76, 76);  border:1px solid rgb(76, 76, 76); }"

		"QLineEdit{background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); font-size:12px; selection-background-color:rgb(165, 165, 165); selection-color:rgb(32, 32, 32); border-radius:0px;}"
		"QComboBox{background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); font-size:12px; selection-background-color:rgb(165, 165, 165); selection-color:rgb(32, 32, 32); border-radius:0px;}"
		"QSpinBox{background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); font-size:12px; selection-background-color:rgb(165, 165, 165); selection-color:rgb(32, 32, 32); border-radius:0px;}"
		"QDoubleSpinBox{background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); font-size:12px; selection-background-color: rgb(165, 165, 165); selection-color: rgb(32, 32, 32); border-radius:0px;}"
	);
	QTextStream qtsTemp(&strDefaultStyle);
	qtsTemp << "QTreeView::branch:has-children:closed {image:url(" << getImagePath("icon_closefile_min.png") << ");}"
		<< "QTreeView::branch:has-children:open{image:url(" << getImagePath("icon_openfile_min.png") << ");}"
		;

	this->setStyleSheet(strDefaultStyle);

	QString strTitleBarStyle = QString::fromLocal8Bit(
		"QWidget{background-color: rgb(48,48,48); color:rgb(204, 204, 204); font-family:\"微软雅黑\"; border:0px solid rgb(76, 76, 76);}"
		"QLabel{font:normal 16px; font-family:\"微软雅黑\";}"
	);
	ui.title_bar->setStyleSheet(strTitleBarStyle);

	// 3. line
	QString strLineStyle = QString::fromLocal8Bit("background-color:rgb(59, 59, 59); border-top:1px solid rgb(38, 38, 38); border-bottom:1px solid rgb(48, 48, 48);");
	ui.line->setStyleSheet(strLineStyle);
}

void OvrMaterialEditor::init_ui_data()
{
	// set QLineEdit
	OvrString strMatPath = mat_property_mgr->GetMatPath();
	ui.material_path_edit->setText(QString::fromLocal8Bit(strMatPath.GetStringConst()));

	// set QComboBox
	ui.material_type_combo->setView(new QListView());

	std::list<OvrString> lstMatTypes;
	mat_property_mgr->GetTypeList(lstMatTypes);
	for (auto & typeString : lstMatTypes) {
		QString strTypeString = QString::fromLocal8Bit(typeString.GetStringConst());
		ui.material_type_combo->addItem(strTypeString);
	}

	original_material_type = mat_property_mgr->GetType();
	QString strOrgMatType = QString::fromLocal8Bit(original_material_type.GetStringConst());
	bool success = false;
	for (int i = 0; i < ui.material_type_combo->count(); i++) {
		QString strItemText = ui.material_type_combo->itemText(i);
		if (strItemText == strOrgMatType) {
			ui.material_type_combo->setCurrentIndex(i);
			success = true;
			break;
		}
	}

	//assert(success);

	//load_all_mat_type_org_values();
	init_tree_widget(ui.attributeEditorTree);

	auto mat_current_properties = mat_property_mgr->GetParamList();
	set_tree_info(ui.attributeEditorTree, mat_current_properties);

	// 保存原始信息
	all_original_edited_properties.insert(original_material_type, mat_current_properties);	
}

void OvrMaterialEditor::preset_connects()
{
	QObject::connect(ui.ok_button, SIGNAL(clicked()), this, SLOT(on_ok_clicked()));
	QObject::connect(ui.cancel_button, SIGNAL(clicked()), this, SLOT(on_cancel_clicked()));
	//QObject::connect(ui.pushButtonClose, SIGNAL(clicked()), this, SLOT(on_cancel_clicked()));

	QObject::connect(ui.material_type_combo, SIGNAL(currentIndexChanged(int)), this, SLOT(on_material_type_changed(int)));
}

void OvrMaterialEditor::init_tree_widget(QTreeView * a_tree)
{
	//QHeaderView * pHeaderView = new QHeaderView(Qt::Orientation::Horizontal,a_tree);
	//pHeaderView->setSectionResizeMode(QHeaderView::Stretch);
	//pHeaderView->setStretchLastSection(true);
	//a_tree->setHeader(pHeaderView);

	tree_model = new QStandardItemModel();

	// 1 .first
	// QStringList qslHeaders;
	//qslHeaders << QString::fromLocal8Bit("属性") << QString::fromLocal8Bit("值");
	//tree_model->setHorizontalHeaderLabels(qslHeaders);
	////tree_model->horizontalHeaderItem(0)->setWidth;
	//tree_model->horizontalHeaderItem()->setresize(QHeaderView::Stretch);
	 
	//// 2 .second
	//tree_model->setHorizontalHeaderItem(0, new QStandardItem(QString::fromLocal8Bit("属性")));
	//tree_model->setHorizontalHeaderItem(1, new QStandardItem(QString::fromLocal8Bit("值")));

	a_tree->setModel(tree_model);

	auto mat_tree_delegate = new OvrMaterialTreeDelegate();
	a_tree->setItemDelegate(mat_tree_delegate);
	connect(mat_tree_delegate, SIGNAL(send_material_set_msg(int)), this, SLOT(on_material_set_msg_pop(int)));

	a_tree->header()->setDefaultSectionSize(240);
}


void OvrMaterialEditor::set_tree_info(QTreeView * a_tree, const std::list<ovr_project::OvrMaterialParam>& a_properties)
{
	assert(tree_model != nullptr);
	if (!tree_model)
		return;

	tree_model->clear();  // 头也被清掉了
	tree_model->setHorizontalHeaderItem(0, new QStandardItem(QString::fromLocal8Bit("属性")));
	tree_model->setHorizontalHeaderItem(1, new QStandardItem(QString::fromLocal8Bit("值")));

	int n_sub_row = 0;
	for (auto &property : a_properties) {
		// 1.属性名，不可修改
		QString nAttributeName = QString::fromLocal8Bit(property.display_name.GetStringConst());
		OvrMaterialTreeItemDefault * pItem_Name = new OvrMaterialTreeItemDefault(nAttributeName);
		pItem_Name->setEditable(false);
		tree_model->appendRow(pItem_Name);

		// 2.属性值，需要修改
		OvrMaterialTreeItem * pItem_value = nullptr;
		switch (property.type)
		{
		case ovr_project::OvrMaterialParam::kInt:
			pItem_value = get_spin_box_item(property);
			break;
		case ovr_project::OvrMaterialParam::kFloat:
			pItem_value = get_double_spin_box_item(property);
			break;
		case ovr_project::OvrMaterialParam::kVector3:
		case ovr_project::OvrMaterialParam::kVector4:
		{
			QString current_value = QString::fromLocal8Bit(property.value.GetStringConst());
			pItem_value = new OvrMaterialTreeItemLineEdit(current_value);
			break;
		}
		case ovr_project::OvrMaterialParam::kTexture:
			pItem_value = get_push_button_item(property);
			break;
		case ovr_project::OvrMaterialParam::kEnum:
			pItem_value = get_combo_box_item(property);
			break;
		case ovr_project::OvrMaterialParam::kColor:
			//pItem_value = get_color_eidtor_item(property);
			pItem_value = get_custom_pop_item(property);
			;
			break;
		default:
			assert(false);
			pItem_value = new OvrMaterialTreeItemLineEdit();
			break;
		}
		pItem_value->setToolTip(QString::fromLocal8Bit("双击修改"));
		pItem_value->set_property_type(property.type);
		pItem_value->set_property_name(property.display_name);
		tree_model->setItem(n_sub_row, 1, pItem_value);

		////const char * img_path = "icon_model_max.png";
		//pItem_value->setIcon(QIcon(getImagePath("icon_model_max.png")));

		// 设置item 被修改的信号连接
		connect(pItem_value, SIGNAL(item_modified(OvrMaterialTreeItem*)),
			this, SLOT(on_current_tree_modified(OvrMaterialTreeItem*)));

		n_sub_row++;
	}

	a_tree->expandAll();
}

OvrMaterialTreeItemPushButton * OvrMaterialEditor::get_push_button_item(const ovr_project::OvrMaterialParam& property)
{
	//QString strTexPath = QString::fromLocal8Bit(property.value.GetStringConst());
	//if (strTexPath.isEmpty())
	//	strTexPath = OvrMaterialTreeItemFilePathEdit::cs_PathNull;
	QString strTexPath = QString::fromLocal8Bit(property.value.GetStringConst());
	QString	strItemText = strTexPath.isEmpty() ? OvrMaterialTreeItemFilePathEdit::cs_PathNull : strTexPath;

	auto buttonItem = new OvrMaterialTreeItemPushButton(strTexPath, strItemText);

	return buttonItem;
}

OvrMaterialTreeItemSpinBox * OvrMaterialEditor::get_spin_box_item(const ovr_project::OvrMaterialParam & property)
{
	OvrMaterialTreeItemSpinBox::EditorAttribute ea;
	ea.min_value = 0;
	ea.max_value = 1;
	ea.single_step = 0.1;

	int nValue = QString::fromLocal8Bit(property.value.GetStringConst()).toInt();
	QString strValue = QString::number(nValue);
	auto spinBoxItem = new OvrMaterialTreeItemSpinBox(nValue, strValue);
	spinBoxItem->setEditorAttribute(ea);

	return spinBoxItem;
}

OvrMaterialTreeItemDoubleSpinBox * OvrMaterialEditor::get_double_spin_box_item(const ovr_project::OvrMaterialParam& property)
{
	OvrMaterialTreeItemDoubleSpinBox::EditorAttribute ea;
	ea.min_value = 0;
	ea.max_value = 1;
	ea.single_step = 0.1;
	ea.prec = 2;

	double dValue = QString::fromLocal8Bit(property.value.GetStringConst()).toDouble();
	QString strValue = QString::number(dValue, 'f', ea.prec);
	auto spinBoxItem = new OvrMaterialTreeItemDoubleSpinBox(dValue, strValue);
	spinBoxItem->setEditorAttribute(ea);

	return spinBoxItem;
}

OvrMaterialTreeItemFilePathEdit * OvrMaterialEditor::get_file_path_item(const ovr_project::OvrMaterialParam& property)
{
	QString strTexPath = QString::fromLocal8Bit(property.value.GetStringConst());
	QString	strItemText = strTexPath.isEmpty()? OvrMaterialTreeItemFilePathEdit::cs_PathNull : strTexPath;

	auto pathItem = new OvrMaterialTreeItemFilePathEdit(strTexPath, strItemText);

	return pathItem;
}

OvrMaterialTreeItemComboBox * OvrMaterialEditor::get_combo_box_item(const ovr_project::OvrMaterialParam& property)
{
	int n_current_index = -1;
	OvrMaterialTreeItemComboBox::EditorAttribute ea;
	QString strCurValue = QString::fromLocal8Bit(property.value.GetStringConst());

	QString strEnums = QString::fromLocal8Bit(property.reserve.GetStringConst());
	if (!strEnums.isEmpty()) {
		QStringList qslEnumInfo = strEnums.split(QString(":"));


		for (int i = 0; i < qslEnumInfo.count(); i++) {

			QString straEnum = qslEnumInfo.at(i);
			ea.m_itemComboBoxList.push_back(QPair<int32_t, QString>(i, straEnum));

			if (straEnum == strCurValue) {
				n_current_index = i;
			}
		} // for
	} // if

	auto comboBoxItem = new OvrMaterialTreeItemComboBox(n_current_index, strCurValue);
	comboBoxItem->setEditorAttribute(ea);

	return comboBoxItem;
}

OvrMaterialTreeItemColorEdit * OvrMaterialEditor::get_color_eidtor_item(const ovr_project::OvrMaterialParam & property)
{
	QString strTexPath = QString::fromLocal8Bit(property.value.GetStringConst());

	auto buttonItem = new OvrMaterialTreeItemColorEdit(strTexPath, strTexPath);

	return buttonItem;
}

OvrMaterialTreeItemCustomPop * OvrMaterialEditor::get_custom_pop_item(const ovr_project::OvrMaterialParam & property)
{
	QString strTexPath = QString::fromLocal8Bit(property.value.GetStringConst());

	auto buttonItem = new OvrMaterialTreeItemCustomPop(strTexPath, strTexPath);

	std::function<QString(OvrMaterialTreeItemCustomPop* pop_item, const QString &str)> pop_fun =
		std::bind(&OvrMaterialEditor::color_setting_pop, this, std::placeholders::_1, std::placeholders::_2);
	buttonItem->set_custom_pop_fun(pop_fun);

	return buttonItem;
}

void OvrMaterialEditor::update_item_attribute(OvrMaterialTreeItem * a_item)
{
	OvrString property_name = a_item->get_property_name();
	switch (a_item->get_property_type())
	{
	case ovr_project::OvrMaterialParam::kInt:
	{
		OvrMaterialTreeItemSpinBox * real_item = dynamic_cast<OvrMaterialTreeItemSpinBox *>(a_item);
		QString strCurValue = QString::number(real_item->getCurValue());
		OvrString strRealValue = strCurValue.toLocal8Bit();
		mat_property_mgr->SetParamValue(property_name, strRealValue);
		break;
	}
	case ovr_project::OvrMaterialParam::kFloat:
	{
		OvrMaterialTreeItemDoubleSpinBox * real_item = dynamic_cast<OvrMaterialTreeItemDoubleSpinBox *>(a_item);
		QString strCurValue = QString::number(real_item->getCurValue());
		OvrString strRealValue = strCurValue.toLocal8Bit();
		mat_property_mgr->SetParamValue(property_name, strRealValue);
		break;
	}
	case ovr_project::OvrMaterialParam::kVector3:
	case ovr_project::OvrMaterialParam::kVector4:
	{
		OvrMaterialTreeItemLineEdit * real_item = dynamic_cast<OvrMaterialTreeItemLineEdit *>(a_item);
		QString strCurValue = real_item->getCustomData();
		OvrString strRealValue = strCurValue.toLocal8Bit();
		mat_property_mgr->SetParamValue(property_name, strRealValue);
		break;
	}
	case ovr_project::OvrMaterialParam::kTexture:
	{
		OvrMaterialTreeItemPushButton * real_item = dynamic_cast<OvrMaterialTreeItemPushButton *>(a_item);
		QString strCurValue = real_item->getCustomData();
		OvrString strRealValue = strCurValue.toLocal8Bit();
		mat_property_mgr->SetParamValue(property_name, strRealValue);
		break;
	}
	case ovr_project::OvrMaterialParam::kEnum:
	{
		OvrMaterialTreeItemComboBox * real_item = dynamic_cast<OvrMaterialTreeItemComboBox *>(a_item);
		int nCurIndex = real_item->getCurIndex();
		OvrMaterialTreeItemComboBox::EditorAttribute editor_attr = real_item->getEditorAttribute();
		auto realValue = editor_attr.m_itemComboBoxList.at(nCurIndex);

		OvrString strRealValue = realValue.second.toLocal8Bit();
		mat_property_mgr->SetParamValue(property_name, strRealValue);
		break;
	}
	case ovr_project::OvrMaterialParam::kColor:
	{
		OvrMaterialTreeItemCustomPop * real_item = dynamic_cast<OvrMaterialTreeItemCustomPop *>(a_item);
		QString strCurValue = real_item->getCustomData();
		OvrString strRealValue = strCurValue.toLocal8Bit();
		mat_property_mgr->SetParamValue(property_name, strRealValue);
		break;
	}
	default:
		assert(false);
		break;
	}
}

QString OvrMaterialEditor::color_setting_pop(OvrMaterialTreeItemCustomPop* pop_item, const QString & customData)
{
	QString strNewData;

	QColor theDefaultColor;
	QStringList qslEnumInfo;
	if (!customData.isEmpty()) {
		qslEnumInfo = customData.split(QString(":"));

		float r = qslEnumInfo.at(0).toFloat();
		float g = qslEnumInfo.at(1).toFloat();
		float b = qslEnumInfo.at(2).toFloat();
		float a = qslEnumInfo.at(3).toFloat();

		theDefaultColor.setRgbF(r, g, b, a);
	}

	//QColorDialog * color_dlg = new QColorDialog(this);

	////Qt::WindowFlags flags = Qt::WindowType::SubWindow;  // Dialog
	////color_dlg->setWindowFlags(color_dlg->windowFlags() | flags);

	//color_dlg->setOptions(QColorDialog::ShowAlphaChannel);
	////ui.gridLayout->addWidget(color_dlg, 5, 0, 1, 2);
	//color_dlg->exec();

	OvrCommonColorDlg commonColorDlg(theDefaultColor, this);

	connect(&commonColorDlg, &OvrCommonColorDlg::color_changed,
		[this, pop_item, &strNewData](float r, float g, float b, float a) {
		OvrString property_name = pop_item->get_property_name();

		QString strCurValue;
		QTextStream qtsTS(&strCurValue);
		qtsTS << QString::number(r) << ":"
			<< QString::number(g) << ":"
			<< QString::number(b) << ":"
			<< QString::number(a);

		OvrString strRealValue = strCurValue.toLocal8Bit();
		mat_property_mgr->SetParamValue(property_name, strRealValue);

		strNewData = strCurValue;
	});

	if (commonColorDlg.exec() == QDialog::Accepted) {
		return strNewData;	// 返回新值
	}

	return customData;		// 返回旧值
}

void OvrMaterialEditor::on_material_type_changed(int a_index)
{
	if (m_bTreeItemChanged) {
		// not need
		//auto result = QMessageBox::warning(this, QString::fromLocal8Bit("材质编辑"),
		//	QString::fromLocal8Bit("材质属性已修改，是否保存！"), QMessageBox::Ok| QMessageBox::Cancel);
		//if (QMessageBox::Ok == result) {
		//	//save the modify;
		//}

		m_bTreeItemChanged = false;
	}

	QString strCurType = ui.material_type_combo->currentText();
	OvrString current_material_type = strCurType.toLocal8Bit().constData();
	mat_property_mgr->SetType(current_material_type);
	auto &current_edit_properties = mat_property_mgr->GetParamList();
	set_tree_info(ui.attributeEditorTree, current_edit_properties);

	all_original_edited_properties.insert(current_material_type, current_edit_properties);
}

void OvrMaterialEditor::on_material_set_msg_pop(int a_msg_type)
{
	switch (a_msg_type)
	{
	case 0:
		QMessageBox::warning(this, QString::fromLocal8Bit("贴图选择"),
			QString::fromLocal8Bit("非工程目录文件需要导入后才能使用！"));
		break;
	default:
		assert(false);
		break;
	}
}

void OvrMaterialEditor::on_current_tree_modified(OvrMaterialTreeItem* a_item)
{
	update_item_attribute(a_item);

	m_bTreeItemChanged = true;
}

void OvrMaterialEditor::done(int r)
{
	if (r == QDialog::Accepted) {
		mat_property_mgr->Save();
	}
	else {
		mat_property_mgr->SetType(original_material_type);

		for (auto &property_values : all_original_edited_properties) {
			for (auto & property : property_values) {
				mat_property_mgr->SetParamValue(property.display_name, property.value);
			}
		}
	}

	QDialog::done(r);
}

void OvrMaterialEditor::on_ok_clicked()
{
	done(QDialog::Accepted);
}

void OvrMaterialEditor::on_cancel_clicked()
{
	done(QDialog::Rejected);
}

