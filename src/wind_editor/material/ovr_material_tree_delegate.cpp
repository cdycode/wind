#include <Header.h>
#include "ovr_material_tree_delegate.h"
#include "../dialog/ovr_file_dialog.h"

#include <QFileDialog>
#include <QRegExpValidator>
#include <QListView>
#include <QColorDialog>

OvrMaterialTreeDelegate::OvrMaterialTreeDelegate(QObject *parent)
	: QStyledItemDelegate(parent)
{
}

OvrMaterialTreeDelegate::~OvrMaterialTreeDelegate()
{
}

QWidget * OvrMaterialTreeDelegate::createEditor(QWidget * parent, const QStyleOptionViewItem & option, const QModelIndex & index) const
{
	const QStandardItemModel * standModel = dynamic_cast<const QStandardItemModel *>(index.model());
	QStandardItem * item = standModel->itemFromIndex(index);
	OvrMaterialTreeItem* matItem = dynamic_cast<OvrMaterialTreeItem*>(item);
	//m_matItem = matItem;


	QWidget * widget = nullptr;
	switch (matItem->getEditorType())
	{
	case eEditorBoxType::EBT_LINE_EDIT:
		widget = createLineEditEditor(parent, matItem);
		break;
	case eEditorBoxType::EBT_PUSH_BUTTON:
		widget = createButtonEditor(parent, matItem);
		break;
	case eEditorBoxType::EBT_CHECK_BOX:
		widget = createCheckBoxEditor(parent, matItem);
		break;
	case eEditorBoxType::EBT_COMBO_BOX:
		widget = createComboBoxEditor(parent, matItem);
		break;
	case eEditorBoxType::EBT_SPIN_BOX:
		widget = createSpinBoxEditor(parent, matItem);
		break;
	case eEditorBoxType::EBT_DOUBLE_SPIN_BOX:
		widget = createDoubleSpinBoxEditor(parent, matItem);
		break;
	case eEditorBoxType::EBT_FILE_PATH_EDIT:
		widget = createFilePathEditor(parent, matItem);
		break;
	case eEditorBoxType::EBT_DEFAULT:
		widget = createLineEditEditor(parent, matItem);
		break;
	case eEditorBoxType::EBT_COLOR_EDIT:
		widget = createColorEditor(parent, matItem);
		break;
	case eEditorBoxType::EBT_CUSTOM_POP:
		widget = createPopEditor(parent, matItem);
		break;
	default:
		widget = QStyledItemDelegate::createEditor(parent, option, index);	
		break;
	}

	return widget;
}

void OvrMaterialTreeDelegate::setEditorData(QWidget * editor, const QModelIndex & index) const
{
	const QStandardItemModel * standModel = dynamic_cast<const QStandardItemModel *>(index.model());
	QStandardItem * item = standModel->itemFromIndex(index);
	OvrMaterialTreeItem* matItem = dynamic_cast<OvrMaterialTreeItem*>(item);

	switch (matItem->getEditorType())
	{
	case eEditorBoxType::EBT_LINE_EDIT:
		setEditorData(dynamic_cast<QLineEdit *>(editor), matItem);
		break;
	case eEditorBoxType::EBT_PUSH_BUTTON:
		setEditorData(dynamic_cast<QPushButton *>(editor), matItem);
		break;
	case eEditorBoxType::EBT_CHECK_BOX:
		setEditorData(dynamic_cast<QCheckBox *>(editor), matItem);
		break;
	case eEditorBoxType::EBT_COMBO_BOX:
		setEditorData(dynamic_cast<QComboBox *>(editor), matItem);
		break;
	case eEditorBoxType::EBT_SPIN_BOX:
		setEditorData(dynamic_cast<QSpinBox *>(editor), matItem);
		break;
	case eEditorBoxType::EBT_DOUBLE_SPIN_BOX:
		setEditorData(dynamic_cast<QDoubleSpinBox *>(editor), matItem);
		break;
	case eEditorBoxType::EBT_FILE_PATH_EDIT:
		setEditorData(dynamic_cast<OvrFilePathEdit *>(editor), matItem);
		break;
	case eEditorBoxType::EBT_COLOR_EDIT:
		setEditorData(dynamic_cast<QLabel *>(editor), matItem);
		break;
	case eEditorBoxType::EBT_CUSTOM_POP:
		setPopEditorData(dynamic_cast<QPushButton *>(editor), matItem);
		break;
	case eEditorBoxType::EBT_DEFAULT:
		setEditorData(dynamic_cast<QLineEdit *>(editor), matItem);
		break;
	default:
		QStyledItemDelegate::setEditorData(editor, index);
		break;
	}
}

void OvrMaterialTreeDelegate::setModelData(QWidget * editor, QAbstractItemModel * model, const QModelIndex & index) const
{
	const QStandardItemModel * standModel = dynamic_cast<const QStandardItemModel *>(index.model());
	QStandardItem * item = standModel->itemFromIndex(index);
	OvrMaterialTreeItem* matItem = dynamic_cast<OvrMaterialTreeItem*>(item);

	switch (matItem->getEditorType())
	{
	case eEditorBoxType::EBT_LINE_EDIT:
		setModelData(dynamic_cast<QLineEdit *>(editor), dynamic_cast<QStandardItemModel *>(model), matItem);
		break;
	case eEditorBoxType::EBT_PUSH_BUTTON:
		setModelData(dynamic_cast<QPushButton *>(editor), dynamic_cast<QStandardItemModel *>(model), matItem);
		break;
	case eEditorBoxType::EBT_CHECK_BOX:
		setModelData(dynamic_cast<QCheckBox *>(editor), dynamic_cast<QStandardItemModel *>(model), matItem);
		break;
	case eEditorBoxType::EBT_COMBO_BOX:
		setModelData(dynamic_cast<QComboBox *>(editor), dynamic_cast<QStandardItemModel *>(model), matItem);
		break;
	case eEditorBoxType::EBT_SPIN_BOX:
		setModelData(dynamic_cast<QSpinBox *>(editor), dynamic_cast<QStandardItemModel *>(model), matItem);
		break;
	case eEditorBoxType::EBT_DOUBLE_SPIN_BOX:
		setModelData(dynamic_cast<QDoubleSpinBox *>(editor), dynamic_cast<QStandardItemModel *>(model), matItem);
		break;
	case eEditorBoxType::EBT_FILE_PATH_EDIT:
		setModelData(dynamic_cast<OvrFilePathEdit *>(editor), dynamic_cast<QStandardItemModel *>(model), matItem);
		break;
	case eEditorBoxType::EBT_COLOR_EDIT:
		setModelData(dynamic_cast<QLabel *>(editor), dynamic_cast<QStandardItemModel *>(model), matItem);
		break;
	case eEditorBoxType::EBT_CUSTOM_POP:
		setPopModelData(dynamic_cast<QPushButton *>(editor), dynamic_cast<QStandardItemModel *>(model), matItem);
		break;
	case eEditorBoxType::EBT_DEFAULT:
		setModelData(dynamic_cast<QLineEdit *>(editor), dynamic_cast<QStandardItemModel *>(model), matItem);
		break;
	default:
		QStyledItemDelegate::setModelData(editor, model, index);
		break;
	}
}


QLineEdit * OvrMaterialTreeDelegate::createLineEditEditor(QWidget * parent, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemLineEdit * cbItem = dynamic_cast<OvrMaterialTreeItemLineEdit*>(matItem);
	auto &itemAttrs = cbItem->getEditorAttribute();

	QLineEdit * editor_box = new QLineEdit(parent);
	editor_box->setMaxLength(itemAttrs.nMaxLength);
	editor_box->setInputMethodHints(itemAttrs.inputMethodHidnts);

	//QRegExp regExp = QRegExp(itemAttrs.strFilterExp);
	//QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExp, editor_box);
	//editor_box->setValidator(portRegExpValidator);

	//editor_box->setInputMask(itemAttrs.strMask);

	connect(editor_box, SIGNAL(textChanged(QString)),
		cbItem, SLOT(setCustomData(QString&)), Qt::QueuedConnection);

	return editor_box;
}

QPushButton * OvrMaterialTreeDelegate::createButtonEditor(QWidget * parent, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemPushButton * cbItem = dynamic_cast<OvrMaterialTreeItemPushButton*>(matItem);
	auto &itemAttrs = cbItem->getEditorAttribute();

	QPushButton * editor_box = new QPushButton(parent);

	connect(this, SIGNAL(on_finish_set_button_editor(OvrMaterialTreeItemPushButton*)),
		this, SLOT(on_open_tex_file_dlg(OvrMaterialTreeItemPushButton*)), Qt::QueuedConnection);

	//connect(editor_box, SIGNAL(clicked()), this, SLOT(on_open_material_clicked()), Qt::QueuedConnection);

	return editor_box;
}

QCheckBox * OvrMaterialTreeDelegate::createCheckBoxEditor(QWidget * parent, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemCheckBox * cbItem = dynamic_cast<OvrMaterialTreeItemCheckBox*>(matItem);
	auto &itemAttrs = cbItem->getEditorAttribute();

	QCheckBox * editor_box = new QCheckBox(parent);

	connect(editor_box, SIGNAL(clicked(bool)),
		cbItem, SLOT(SetChecked(bool)), Qt::QueuedConnection);

	return editor_box;
}

QComboBox * OvrMaterialTreeDelegate::createComboBoxEditor(QWidget * parent, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemComboBox * cbItem = dynamic_cast<OvrMaterialTreeItemComboBox*>(matItem);
	auto &itemAttrs = cbItem->getEditorAttribute();
	QComboBox * editor_box = new QComboBox(parent);
	//editor_box->setView(new QListView());

	for (auto &single : itemAttrs.m_itemComboBoxList) {
		editor_box->addItem(single.second, QVariant(single.first));
	}

	connect(editor_box, SIGNAL(currentIndexChanged(int)),
		cbItem, SLOT(set_currrent_index(int)), Qt::QueuedConnection);

	return editor_box;
}

QSpinBox * OvrMaterialTreeDelegate::createSpinBoxEditor(QWidget * parent, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemSpinBox * cbItem = dynamic_cast<OvrMaterialTreeItemSpinBox*>(matItem);
	auto &itemAttrs = cbItem->getEditorAttribute();

	QSpinBox * editor_box = new QSpinBox(parent);
	
	editor_box->setRange(itemAttrs.min_value, itemAttrs.max_value);
	editor_box->setSingleStep(itemAttrs.single_step);

	connect(editor_box, SIGNAL(valueChanged(int)),
		cbItem, SLOT(setCurValue(int)), Qt::QueuedConnection);

	return editor_box;
}

QDoubleSpinBox * OvrMaterialTreeDelegate::createDoubleSpinBoxEditor(QWidget * parent, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemDoubleSpinBox * cbItem = dynamic_cast<OvrMaterialTreeItemDoubleSpinBox*>(matItem);
	auto &itemAttrs = cbItem->getEditorAttribute();

	QDoubleSpinBox * editor_box = new QDoubleSpinBox(parent);

	editor_box->setRange(itemAttrs.min_value, itemAttrs.max_value);
	editor_box->setSingleStep(itemAttrs.single_step);
	editor_box->setDecimals(itemAttrs.prec);

	connect(editor_box, SIGNAL(valueChanged(double)),
		cbItem, SLOT(setCurValue(double)), Qt::QueuedConnection);

	return editor_box;
}

OvrFilePathEdit * OvrMaterialTreeDelegate::createFilePathEditor(QWidget * parent, OvrMaterialTreeItem * matItem) const
{
	OvrMaterialTreeItemFilePathEdit * cbItem = dynamic_cast<OvrMaterialTreeItemFilePathEdit*>(matItem);
	OvrFilePathEdit * editor_box = new OvrFilePathEdit(parent);

	connect(editor_box, SIGNAL(curPathChanged(QString)),
		cbItem, SLOT(setCustomData(QString&)), Qt::QueuedConnection);

	return editor_box;
}

QLabel * OvrMaterialTreeDelegate::createColorEditor(QWidget * parent, OvrMaterialTreeItem * matItem) const
{
	OvrMaterialTreeItemColorEdit * cbItem = dynamic_cast<OvrMaterialTreeItemColorEdit*>(matItem);
	auto &itemAttrs = cbItem->getEditorAttribute();

	QLabel * editor_box = new QLabel(parent);

	connect(this, SIGNAL(on_finish_set_color_editor(OvrMaterialTreeItemColorEdit*)),
		this, SLOT(on_open_set_color_dlg(OvrMaterialTreeItemColorEdit*)), Qt::QueuedConnection);

	//connect(editor_box, SIGNAL(clicked()), this, SLOT(on_open_material_clicked()), Qt::QueuedConnection);

	return editor_box;
}

QPushButton * OvrMaterialTreeDelegate::createPopEditor(QWidget * parent, OvrMaterialTreeItem * matItem) const
{
	OvrMaterialTreeItemCustomPop * cbItem = dynamic_cast<OvrMaterialTreeItemCustomPop*>(matItem);
	auto &itemAttrs = cbItem->getEditorAttribute();

	QPushButton * editor_box = new QPushButton(parent);

	connect(this, SIGNAL(on_pop_custom_editor(OvrMaterialTreeItemCustomPop*)),
		this, SLOT(on_open_custom_pop(OvrMaterialTreeItemCustomPop*)), Qt::QueuedConnection);

	//connect(editor_box, SIGNAL(clicked()), this, SLOT(on_open_material_clicked()), Qt::QueuedConnection);

	return editor_box;
}

// 2.
void OvrMaterialTreeDelegate::setEditorData(QLineEdit * editor, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemLineEdit * cbItem = dynamic_cast<OvrMaterialTreeItemLineEdit*>(matItem);
	//auto &itemAttrs = cbItem->getEditorAttribute();

	QString strInfo = cbItem->text();

	editor->setText(strInfo);
}

void OvrMaterialTreeDelegate::setEditorData(QPushButton * editor, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemPushButton * cbItem = dynamic_cast<OvrMaterialTreeItemPushButton*>(matItem);
	auto &itemAttrs = cbItem->getEditorAttribute();

	editor->setText(itemAttrs.strDefault);
	if (!itemAttrs.strIconPath.isEmpty())
		editor->setIcon(QIcon(itemAttrs.strIconPath));

	emit on_finish_set_button_editor(cbItem);

}

void OvrMaterialTreeDelegate::setEditorData(QCheckBox * editor, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemCheckBox * cbItem = dynamic_cast<OvrMaterialTreeItemCheckBox*>(matItem);

	editor->setChecked(cbItem->IsChecked());

}

void OvrMaterialTreeDelegate::setEditorData(QComboBox * editor, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemComboBox * cbItem = dynamic_cast<OvrMaterialTreeItemComboBox*>(matItem);
	
	int32_t nCurIndex = cbItem->getCurIndex();
	editor->count();

	for (size_t i = 0; i < editor->count(); i++)
	{
		QVariant qvData = editor->itemData(i);

		if (qvData.isValid() && qvData.toInt() == nCurIndex) {
			editor->setCurrentIndex(i);
			break;
		}
	}
}

void OvrMaterialTreeDelegate::setEditorData(QSpinBox * editor, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemSpinBox * cbItem = dynamic_cast<OvrMaterialTreeItemSpinBox*>(matItem);
	auto &itemAttrs = cbItem->getEditorAttribute();

	int32_t nCurValue = cbItem->getCurValue();

	if (nCurValue < itemAttrs.min_value) nCurValue = itemAttrs.min_value;
	if (nCurValue > itemAttrs.max_value) nCurValue = itemAttrs.max_value;

	editor->setValue(nCurValue);
}

void OvrMaterialTreeDelegate::setEditorData(QDoubleSpinBox * editor, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemDoubleSpinBox * cbItem = dynamic_cast<OvrMaterialTreeItemDoubleSpinBox*>(matItem);
	auto &itemAttrs = cbItem->getEditorAttribute();

	double dCurValue = cbItem->getCurValue();

	if (dCurValue < itemAttrs.min_value) dCurValue = itemAttrs.min_value;
	if (dCurValue > itemAttrs.max_value) dCurValue = itemAttrs.max_value;

	editor->setValue(dCurValue);
}

void OvrMaterialTreeDelegate::setEditorData(OvrFilePathEdit * editor, OvrMaterialTreeItem * matItem) const
{
	OvrMaterialTreeItemFilePathEdit * cbItem = dynamic_cast<OvrMaterialTreeItemFilePathEdit*>(matItem);
	auto &itemAttrs = cbItem->getEditorAttribute();

	QString strCurData = cbItem->getCustomData();
	editor->setText(strCurData);

	editor->setButtonText(itemAttrs.strDefault);
	//if (!itemAttrs.strIconPath.isEmpty())
	//	editor->setIcon(QIcon(itemAttrs.strIconPath));
}

void OvrMaterialTreeDelegate::setEditorData(QLabel * editor, OvrMaterialTreeItem * matItem) const
{
	OvrMaterialTreeItemColorEdit * cbItem = dynamic_cast<OvrMaterialTreeItemColorEdit*>(matItem);
	auto &itemAttrs = cbItem->getEditorAttribute();

	//editor->setText(itemAttrs.strDefault);
	//if (!itemAttrs.strIconPath.isEmpty())
	//	editor->setIcon(QIcon(itemAttrs.strIconPath));

	QString strColorInfo = cbItem->getCustomData();
	editor->setText(strColorInfo);

	emit(on_finish_set_color_editor(cbItem));

}

void OvrMaterialTreeDelegate::setPopEditorData(QPushButton * editor, OvrMaterialTreeItem * matItem) const
{
	OvrMaterialTreeItemCustomPop * cbItem = dynamic_cast<OvrMaterialTreeItemCustomPop*>(matItem);
	auto &itemAttrs = cbItem->getEditorAttribute();

	editor->setText(itemAttrs.strDefault);
	if (!itemAttrs.strIconPath.isEmpty())
		editor->setIcon(QIcon(itemAttrs.strIconPath));

	emit on_pop_custom_editor(cbItem);
}

// 3.
void OvrMaterialTreeDelegate::setModelData(QLineEdit * editor, QStandardItemModel * model, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemLineEdit * cbItem = dynamic_cast<OvrMaterialTreeItemLineEdit*>(matItem);

	QString strText = editor->text();

	cbItem->setCustomData(strText);
	//emit(editor->textChanged(strText));

	cbItem->setText(strText);

	disconnect(editor, SIGNAL(textChanged(QString)), cbItem, SLOT(setCustomData(QString&)));
}

void OvrMaterialTreeDelegate::setModelData(QPushButton * editor, QStandardItemModel * model, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemPushButton * cbItem = dynamic_cast<OvrMaterialTreeItemPushButton*>(matItem);

	disconnect(this, SIGNAL(on_finish_set_button_editor(OvrMaterialTreeItemPushButton*)),
		this, SLOT(on_open_tex_file_dlg(OvrMaterialTreeItemPushButton*)));
}

void OvrMaterialTreeDelegate::setModelData(QCheckBox * editor, QStandardItemModel * model, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemCheckBox * cbItem = dynamic_cast<OvrMaterialTreeItemCheckBox*>(matItem);

	bool bIsChecked = editor->isChecked();
	cbItem->SetChecked(bIsChecked);
	//emit(editor->clicked(bIsChecked));

	QString strItemText = bIsChecked ? QString::fromLocal8Bit("true") : QString::fromLocal8Bit("false");
	cbItem->setText(strItemText);

	disconnect(editor, SIGNAL(clicked(bool)), cbItem, SLOT(SetChecked(bool)));

}

void OvrMaterialTreeDelegate::setModelData(QComboBox * editor, QStandardItemModel * model, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemComboBox * cbItem = dynamic_cast<OvrMaterialTreeItemComboBox*>(matItem);

	auto &itemAttrs = cbItem->getEditorAttribute();

	QVariant qvCurData = editor->currentData();

	int32_t nCurIndex = qvCurData.toInt();
	//if (qvCurData.isValid()) {
	//	cbItem->setCurIndex(qvCurData.toInt());
	//}
	cbItem->set_currrent_index(nCurIndex);
	//emit(editor->currentIndexChanged(nCurIndex));

	for (auto & single : itemAttrs.m_itemComboBoxList) {
		if (single.first == nCurIndex) {
			cbItem->setText(single.second);
			break;
		}
	}

	disconnect(editor, SIGNAL(currentIndexChanged(int)), cbItem, SLOT(set_currrent_index(int)));
}

void OvrMaterialTreeDelegate::setModelData(QSpinBox * editor, QStandardItemModel * model, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemSpinBox * cbItem = dynamic_cast<OvrMaterialTreeItemSpinBox*>(matItem);

	int32_t nCurValue = editor->value();

	cbItem->setCurValue(nCurValue);
	//emit(editor->valueChanged(nCurValue));

	cbItem->setText(QString::number(nCurValue));

	disconnect(editor, SIGNAL(valueChanged(int)), cbItem, SLOT(setCurValue(int)));

}

void OvrMaterialTreeDelegate::setModelData(QDoubleSpinBox * editor, QStandardItemModel * model, OvrMaterialTreeItem* matItem) const
{
	OvrMaterialTreeItemDoubleSpinBox * cbItem = dynamic_cast<OvrMaterialTreeItemDoubleSpinBox*>(matItem);

	auto &itemAttrs = cbItem->getEditorAttribute();

	double dCurValue = editor->value();

	cbItem->setCurValue(dCurValue);
	//emit(editor->valueChanged(dCurValue));

	QString strText = QString::number(dCurValue, 'f', itemAttrs.prec);
	cbItem->setText(strText);

	disconnect(editor, SIGNAL(valueChanged(double)), cbItem, SLOT(setCurValue(double)));

}

void OvrMaterialTreeDelegate::setModelData(OvrFilePathEdit * editor, QStandardItemModel * model, OvrMaterialTreeItem * matItem) const
{
	OvrMaterialTreeItemFilePathEdit * cbItem = dynamic_cast<OvrMaterialTreeItemFilePathEdit*>(matItem);
	//auto &itemAttrs = cbItem->getEditorAttribute();

	QString strCurData = editor->getCurPath();
	cbItem->setCustomData(strCurData);
	//emit(editor->curPathChanged(strCurData));

	if (strCurData.isEmpty())
		strCurData = OvrMaterialTreeItemFilePathEdit::cs_PathNull;
	cbItem->setText(strCurData);

	disconnect(editor, SIGNAL(curPathChanged(QString)), cbItem, SLOT(setCustomData(QString&)));

}

void OvrMaterialTreeDelegate::setModelData(QLabel * editor, QStandardItemModel * model, OvrMaterialTreeItem * matItem) const
{
	OvrMaterialTreeItemColorEdit * cbItem = dynamic_cast<OvrMaterialTreeItemColorEdit*>(matItem);

	disconnect(this, SIGNAL(on_finish_set_color_editor(OvrMaterialTreeItemColorEdit*)),
		this, SLOT(on_finish_set_color_editor(OvrMaterialTreeItemColorEdit*)));
}

void OvrMaterialTreeDelegate::setPopModelData(QPushButton * editor, QStandardItemModel * model, OvrMaterialTreeItem * matItem) const
{
	OvrMaterialTreeItemPushButton * cbItem = dynamic_cast<OvrMaterialTreeItemPushButton*>(matItem);

	disconnect(this, SIGNAL(on_pop_custom_editor(OvrMaterialTreeItemCustomPop*)),
		this, SLOT(on_open_custom_pop(OvrMaterialTreeItemCustomPop*)));
}

void OvrMaterialTreeDelegate::on_open_tex_file_dlg(OvrMaterialTreeItemPushButton* a_mat_item)
{
#if 1
	OvrFileDialog dlg;
	if (dlg.init(QString::fromLocal8Bit("贴图"), "tex files(*.tex)", a_mat_item->getCustomData()) && dlg.exec() == 1)
	{
		QString relative_path = dlg.getSelectFile();
		a_mat_item->setText(relative_path);
		a_mat_item->setCustomData(relative_path);
		//emit(editor->curPathChanged(strCurData));
	}
#else
	QString title = QString::fromLocal8Bit("贴图");
	OvrString work_dir = ovr_project::OvrProject::GetIns()->GetWorkDir();
	QString strWorkDir = QString::fromLocal8Bit(work_dir.GetStringConst());
	QString strFilter = QString::fromLocal8Bit(
		"tex files(*.tex);;"
		"All files(*.*)"
	);

	QFileDialog * open_file_dlg = open_const_dir_dlg(title, strWorkDir, strFilter, OvrHelper::getInst()->getMainWindow());
	if (open_file_dlg->exec() == QDialog::Accepted) 
	{
		QString strPath = open_file_dlg->selectedFiles()[0];
		int n_index = strPath.indexOf(strWorkDir);
		if (n_index >= 0) 
		{  // 不在此目录里的文件不加载
			QString relative_path = strPath.right(strPath.length() - strWorkDir.length());
			a_mat_item->setText(relative_path);
			a_mat_item->setCustomData(relative_path);

			a_mat_item->alter_modify();
		}
		else {
			emit send_material_set_msg(0);
		}
	}
#endif
}

void OvrMaterialTreeDelegate::on_open_set_color_dlg(OvrMaterialTreeItemColorEdit * a_mat_item)
{
	QColorDialog colorDlg;

	QString strColorInfo = a_mat_item->getCustomData();

	QColor color;
	colorDlg.setCurrentColor(color);
	int nRes = colorDlg.exec();
	//if (QDialog::Rejected == nRes)
	//	return color;

	QColor curColor = colorDlg.currentColor();

	//a_mat_item->setCustomData();

}

void OvrMaterialTreeDelegate::on_open_custom_pop(OvrMaterialTreeItemCustomPop * a_mat_item)
{
	//QString strCustomData = a_mat_item->getCustomData();
	a_mat_item->exec_custom_fun();
}

void OvrMaterialTreeDelegate::on_open_material_clicked()
{
	//emit on_finish_set_button_editor(cbItem);
}

void OvrMaterialTreeDelegate::test_test()
{
}
