#ifndef __OVR_MATERIAL_TREE_ITEM_H__
#define __OVR_MATERIAL_TREE_ITEM_H__

#pragma once

#include <QStandardItem>
#include <QLineEdit>

//#include <wx_engine/i_ovr_material_renderer.h>
#include <wx_project/property/i_ovr_material_property.h>


enum eEditorBoxType
{
	EBT_DEFAULT,
	EBT_LINE_EDIT,
	EBT_PUSH_BUTTON,
	EBT_CHECK_BOX,
	EBT_COMBO_BOX,
	EBT_SPIN_BOX,
	EBT_DOUBLE_SPIN_BOX,
	EBT_FILE_PATH_EDIT,
	EBT_COLOR_EDIT,
	EBT_CUSTOM_POP,
};

class OvrMaterialTreeItem : public QObject, public QStandardItem
{
	Q_OBJECT

public:
	OvrMaterialTreeItem();
	explicit OvrMaterialTreeItem(const QString &text);
	OvrMaterialTreeItem(const QIcon &icon, const QString &text);
	explicit OvrMaterialTreeItem(int rows, int columns = 1);
	virtual ~OvrMaterialTreeItem();

	virtual eEditorBoxType getEditorType() const = 0;

	//void set_property(const ovr_engine::IMaterialRenderer::Property &a_property) { m_property = a_property; }
	//ovr_engine::IMaterialRenderer::Property &get_property() { return m_property; }

	ovr_project::OvrMaterialParam::Type get_property_type() { return cur_property_type; }
	void set_property_type(ovr_project::OvrMaterialParam::Type type) { cur_property_type = type; }

	OvrString get_property_name() { return cur_property_name; }
	void set_property_name(const OvrString & name) { cur_property_name = name; }

	bool is_modified() { return m_modified; }
	void set_modify(bool is_modify = true) { m_modified = is_modify; emit item_modified(this); }

Q_SIGNALS:
	void item_modified(OvrMaterialTreeItem*);

private:
	bool m_modified = false;
	ovr_project::OvrMaterialParam::Type cur_property_type = ovr_project::OvrMaterialParam::kInvalid;
	OvrString cur_property_name;
};


class OvrMaterialTreeItemDefault : public OvrMaterialTreeItem
{
public:
	OvrMaterialTreeItemDefault();
	explicit OvrMaterialTreeItemDefault(const QString &text);
	OvrMaterialTreeItemDefault(const QIcon &icon, const QString &text);
	explicit OvrMaterialTreeItemDefault(int rows, int columns = 1);
	virtual ~OvrMaterialTreeItemDefault();

	eEditorBoxType getEditorType() const override { return eEditorBoxType::EBT_DEFAULT; }
private:
};



class OvrMaterialTreeItemLineEdit : public OvrMaterialTreeItem
{
	Q_OBJECT

public:
	struct EditorAttribute
	{
		QString strDefault = QString("");
		uint32_t nMaxLength = uint32_t(-1);
		Qt::InputMethodHints inputMethodHidnts = Qt::ImhNone;  // ImhDigitsOnly
		QString strFilterExp = QString("");
		QString strMask = QString("");
	};

	explicit OvrMaterialTreeItemLineEdit(const QString &customData = QString());
	explicit OvrMaterialTreeItemLineEdit(const QString &customData, const QString &text);
	OvrMaterialTreeItemLineEdit(const QString &customData, const QIcon &icon, const QString &text);
	explicit OvrMaterialTreeItemLineEdit(const QString &customData, int rows, int columns = 1);
	virtual ~OvrMaterialTreeItemLineEdit();

	eEditorBoxType getEditorType() const override { return eEditorBoxType::EBT_LINE_EDIT; }

	QString getCustomData() { return m_strCustomData; }

	public Q_SLOTS:
	void setCustomData(QString &strCustomData) {
		if (m_strCustomData != strCustomData) {
			m_strCustomData = strCustomData;
			//emit item_modified(this);
			OvrMaterialTreeItem::set_modify();
		}
	}

public:
	EditorAttribute getEditorAttribute() const { return m_itemEditorAttribute; }
	void setEditorAttribute(const EditorAttribute &a_atti) {
		m_itemEditorAttribute = std::move(a_atti);
	}
private:
	EditorAttribute m_itemEditorAttribute;

	QString m_strCustomData;

};

class OvrMaterialTreeItemPushButton : public OvrMaterialTreeItem
{
	Q_OBJECT

public:
	struct EditorAttribute
	{
		QString strDefault = QString::fromLocal8Bit("编辑");
		QString strIconPath;
		//QIcon iconImg;
	};

	explicit OvrMaterialTreeItemPushButton(const QString &customData = QString());
	explicit OvrMaterialTreeItemPushButton(const QString &customData, const QString &text);
	OvrMaterialTreeItemPushButton(const QString &customData, const QIcon &icon, const QString &text);
	explicit OvrMaterialTreeItemPushButton(const QString &customData, int rows, int columns = 1);
	virtual ~OvrMaterialTreeItemPushButton();

	eEditorBoxType getEditorType() const override { return eEditorBoxType::EBT_PUSH_BUTTON; }

	QString getCustomData() { return m_strCustomData; }

	public Q_SLOTS:
	void setCustomData(QString &strCustomData) {
		if (m_strCustomData != strCustomData) {
			m_strCustomData = strCustomData;
			//emit item_modified(this);
			OvrMaterialTreeItem::set_modify();
		}
	}

public:
	EditorAttribute getEditorAttribute() const { return m_itemEditorAttribute; }
	void setEditorAttribute(const EditorAttribute &a_atti) {
		m_itemEditorAttribute = std::move(a_atti);
	}
private:
	EditorAttribute m_itemEditorAttribute;

	QString m_strCustomData;
};

class OvrMaterialTreeItemCheckBox : public OvrMaterialTreeItem
{
	Q_OBJECT

public:
	struct EditorAttribute
	{
		QString strDefault = QString("");
		QIcon iconImg;
	};

	explicit OvrMaterialTreeItemCheckBox(bool value = false);
	explicit OvrMaterialTreeItemCheckBox(bool value, const QString &text);
	OvrMaterialTreeItemCheckBox(bool value, const QIcon &icon, const QString &text);
	explicit OvrMaterialTreeItemCheckBox(bool value, int rows, int columns = 1);
	virtual ~OvrMaterialTreeItemCheckBox();

	eEditorBoxType getEditorType() const override { return eEditorBoxType::EBT_CHECK_BOX; }

	bool IsChecked() { return m_bChecked; }

	public Q_SLOTS:
	void SetChecked(bool a_checked) {
		if (m_bChecked != a_checked) {
			m_bChecked = a_checked;
			//emit item_modified(this);
			OvrMaterialTreeItem::set_modify();
		}
	}

public:
	EditorAttribute getEditorAttribute() const { return m_itemEditorAttribute; }
	void setEditorAttribute(const EditorAttribute &a_atti) {
		m_itemEditorAttribute = std::move(a_atti);
	}

private:
	EditorAttribute m_itemEditorAttribute;

	bool m_bChecked = false;

};

class OvrMaterialTreeItemComboBox : public OvrMaterialTreeItem
{
	Q_OBJECT

public:
	struct EditorAttribute
	{
		//QString strDefault = QString("");
		//QIcon iconImg;

		QVector<QPair<int32_t/*ID*/, QString/*text*/>> m_itemComboBoxList;
	};

	explicit OvrMaterialTreeItemComboBox(int index = -1);
	explicit OvrMaterialTreeItemComboBox(int index, const QString &text);
	OvrMaterialTreeItemComboBox(int index, const QIcon &icon, const QString &text);
	explicit OvrMaterialTreeItemComboBox(int index, int rows, int columns = 1);
	virtual ~OvrMaterialTreeItemComboBox();

	eEditorBoxType getEditorType() const override { return eEditorBoxType::EBT_COMBO_BOX; }

	int32_t getCurIndex() { return m_nCurIndex; }

	public Q_SLOTS:
	void set_currrent_index(int a_index) {
		if (m_nCurIndex != a_index) {
			m_nCurIndex = a_index;
			//emit item_modified(this);
			OvrMaterialTreeItem::set_modify();
		}
	}

public:
	EditorAttribute getEditorAttribute() const { return m_itemEditorAttribute; }
	void setEditorAttribute(const EditorAttribute &a_atti) {
		m_itemEditorAttribute = std::move(a_atti);
	}

private:
	EditorAttribute m_itemEditorAttribute;

	int32_t m_nCurIndex;
};


class OvrMaterialTreeItemSpinBox : public OvrMaterialTreeItem
{
	Q_OBJECT

public:
	struct EditorAttribute
	{
		int32_t min_value = int32_t(-1);
		int32_t max_value = int32_t(2 ^ 31 - 1);
		int32_t single_step = 1;
		//int32_t nDecimals = 1;
	};

	explicit OvrMaterialTreeItemSpinBox(int value = 0);
	explicit OvrMaterialTreeItemSpinBox(int value, const QString &text);
	OvrMaterialTreeItemSpinBox(int value, const QIcon &icon, const QString &text);
	explicit OvrMaterialTreeItemSpinBox(int value, int rows, int columns = 1);
	virtual ~OvrMaterialTreeItemSpinBox();

	eEditorBoxType getEditorType() const override { return eEditorBoxType::EBT_SPIN_BOX; }

	int getCurValue() { return m_nCurValue; }

	public Q_SLOTS:
	void setCurValue(int a_value) {
		if (m_nCurValue != a_value) {
			m_nCurValue = a_value;
			//emit item_modified(this);
			OvrMaterialTreeItem::set_modify();
		}
	}

public:
	EditorAttribute getEditorAttribute() const { return m_itemEditorAttribute; }
	void setEditorAttribute(const EditorAttribute &a_atti) {
		m_itemEditorAttribute = std::move(a_atti);
	}

private:
	EditorAttribute m_itemEditorAttribute;
	
	int m_nCurValue;
};


class OvrMaterialTreeItemDoubleSpinBox : public OvrMaterialTreeItem
{
	Q_OBJECT

public:
	struct EditorAttribute
	{
		double min_value = 0;
		double max_value = 1.;
		double single_step = 0.1;
		//int32_t nDecimals = 1;
		int32_t prec = 1;		// 精度
	};

	explicit OvrMaterialTreeItemDoubleSpinBox(double value = 0.0);
	explicit OvrMaterialTreeItemDoubleSpinBox(double value, const QString &text);
	OvrMaterialTreeItemDoubleSpinBox(double value, const QIcon &icon, const QString &text);
	explicit OvrMaterialTreeItemDoubleSpinBox(double value, int rows, int columns = 1);
	virtual ~OvrMaterialTreeItemDoubleSpinBox();

	eEditorBoxType getEditorType() const override { return eEditorBoxType::EBT_DOUBLE_SPIN_BOX; }

	double getCurValue() { return m_dCurValue; }

	public Q_SLOTS:
	void setCurValue(double a_value) {
		if (m_dCurValue != a_value) {
			m_dCurValue = a_value;
			//emit item_modified(this);
			OvrMaterialTreeItem::set_modify();
		}
	}

public:
	EditorAttribute getEditorAttribute() const { return m_itemEditorAttribute; }
	void setEditorAttribute(const EditorAttribute &a_atti) {
		m_itemEditorAttribute = std::move(a_atti);
	}

private:
	EditorAttribute m_itemEditorAttribute;

	double m_dCurValue;

};

class OvrMaterialTreeItemFilePathEdit : public OvrMaterialTreeItem
{
	Q_OBJECT

public:
	struct EditorAttribute
	{
		QString strDefault = QString::fromLocal8Bit("...");
		QString strIconPath;
	};

	const static QString cs_PathNull;

	explicit OvrMaterialTreeItemFilePathEdit(const QString &customData = QString());
	explicit OvrMaterialTreeItemFilePathEdit(const QString &customData, const QString &text);
	OvrMaterialTreeItemFilePathEdit(const QString &customData, const QIcon &icon, const QString &text);
	explicit OvrMaterialTreeItemFilePathEdit(const QString &customData, int rows, int columns = 1);
	virtual ~OvrMaterialTreeItemFilePathEdit();

	eEditorBoxType getEditorType() const { return eEditorBoxType::EBT_FILE_PATH_EDIT; }

	QString getCustomData() { return m_strCustomData; }

	public Q_SLOTS:
	void setCustomData(QString &strCustomData) {
		if (m_strCustomData != strCustomData) {
			m_strCustomData = strCustomData;
			//emit item_modified(this);
			OvrMaterialTreeItem::set_modify();
		}
	}

public:
	EditorAttribute getEditorAttribute() const { return m_itemEditorAttribute; }
	void setEditorAttribute(const EditorAttribute &a_atti) {
		m_itemEditorAttribute = std::move(a_atti);
	}
private:
	EditorAttribute m_itemEditorAttribute;

	QString m_strCustomData;
};

class OvrMaterialTreeItemColorEdit : public OvrMaterialTreeItem
{
	Q_OBJECT

public:
	struct EditorAttribute
	{
		//QString strDefault = QString::fromLocal8Bit("...");
		//QString strIconPath;
	};

	explicit OvrMaterialTreeItemColorEdit(const QString &customData = QString());
	explicit OvrMaterialTreeItemColorEdit(const QString &customData, const QString &text);
	OvrMaterialTreeItemColorEdit(const QString &customData, const QIcon &icon, const QString &text);
	explicit OvrMaterialTreeItemColorEdit(const QString &customData, int rows, int columns = 1);
	virtual ~OvrMaterialTreeItemColorEdit();

	eEditorBoxType getEditorType() const { return eEditorBoxType::EBT_COLOR_EDIT; }

	QString getCustomData() { return m_strCustomData; }

	public Q_SLOTS:
	void setCustomData(QString &strCustomData) {
		if (m_strCustomData != strCustomData) {
			m_strCustomData = strCustomData;
			//emit item_modified(this);
			OvrMaterialTreeItem::set_modify();
		}
	}

public:
	EditorAttribute getEditorAttribute() const { return m_itemEditorAttribute; }
	void setEditorAttribute(const EditorAttribute &a_atti) {
		m_itemEditorAttribute = std::move(a_atti);
	}
private:
	EditorAttribute m_itemEditorAttribute;

	// 颜色值使用逗号分隔
	QString m_strCustomData;
};

// 自定义弹出窗 Item， 弹窗由弹窗函数设置
class OvrMaterialTreeItemCustomPop : public OvrMaterialTreeItem
{
	Q_OBJECT

public:
	struct EditorAttribute
	{
		QString strDefault = QString::fromLocal8Bit("编辑");
		QString strIconPath;
	};

	explicit OvrMaterialTreeItemCustomPop(const QString &customData = QString());
	explicit OvrMaterialTreeItemCustomPop(const QString &customData, const QString &text);
	OvrMaterialTreeItemCustomPop(const QString &customData, const QIcon &icon, const QString &text);
	explicit OvrMaterialTreeItemCustomPop(const QString &customData, int rows, int columns = 1);
	virtual ~OvrMaterialTreeItemCustomPop();

	void set_custom_pop_fun(std::function<QString(OvrMaterialTreeItemCustomPop*,
		const QString &)> & a_fun) { m_funForPop = a_fun; };
	//std::function<void(QString &customData)> &getCustomPopFun(void) { return m_funForPop ; };
	void exec_custom_fun() { setCustomData(m_funForPop(this, m_strCustomData)); setText(m_strCustomData); };

	eEditorBoxType getEditorType() const override { return eEditorBoxType::EBT_CUSTOM_POP; }

	QString getCustomData() { return m_strCustomData; }

	public Q_SLOTS:
	void setCustomData(QString &strCustomData) {
		if (m_strCustomData != strCustomData) {
			m_strCustomData = strCustomData;
			//emit item_modified(this);
			OvrMaterialTreeItem::set_modify();
		}
	}

public:
	EditorAttribute getEditorAttribute() const { return m_itemEditorAttribute; }
	void setEditorAttribute(const EditorAttribute &a_atti) {
		m_itemEditorAttribute = std::move(a_atti);
	}
private:
	EditorAttribute m_itemEditorAttribute;

	// the function have a arg with custom data for init.
	// the function must be return a string for set the m_strCustomData
	std::function<QString(OvrMaterialTreeItemCustomPop*, const QString &)> m_funForPop;
	QString m_strCustomData;
};


#endif // !__OVR_MATERIAL_TREE_ITEM_H__
