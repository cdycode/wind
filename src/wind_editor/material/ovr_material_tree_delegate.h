#pragma once

#include <QStyledItemDelegate>

#include <QStandardItemModel>

#include <QVector>
#include <QComboBox>
#include <QPushButton>
#include <QCheckBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QDoubleSpinBox>

#include "ovr_material_tree_item.h"
#include "ovr_file_path_edit.h"


class OvrMaterialTreeDelegate final: public QStyledItemDelegate
{
	Q_OBJECT

public:

	explicit OvrMaterialTreeDelegate(QObject *parent = Q_NULLPTR);
	~OvrMaterialTreeDelegate();

	virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
	virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;
	virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

private:
	QLineEdit *createLineEditEditor(QWidget * parent, OvrMaterialTreeItem* matItem) const;
	QPushButton *createButtonEditor(QWidget * parent, OvrMaterialTreeItem* matItem) const;
	QCheckBox *createCheckBoxEditor(QWidget * parent, OvrMaterialTreeItem* matItem) const;
	QComboBox *createComboBoxEditor(QWidget * parent, OvrMaterialTreeItem* matItem) const;
	QSpinBox *createSpinBoxEditor(QWidget * parent, OvrMaterialTreeItem* matItem) const;
	QDoubleSpinBox *createDoubleSpinBoxEditor(QWidget * parent, OvrMaterialTreeItem* matItem) const;
	OvrFilePathEdit *createFilePathEditor(QWidget * parent, OvrMaterialTreeItem* matItem) const;
	QLabel *createColorEditor(QWidget * parent, OvrMaterialTreeItem* matItem) const;
	QPushButton *createPopEditor(QWidget * parent, OvrMaterialTreeItem* matItem) const;

	void setEditorData(QLineEdit * editor, OvrMaterialTreeItem* matItem) const;
	void setEditorData(QPushButton * editor, OvrMaterialTreeItem* matItem) const;
	void setEditorData(QCheckBox * editor, OvrMaterialTreeItem* matItem) const;
	void setEditorData(QComboBox * editor, OvrMaterialTreeItem* matItem) const;
	void setEditorData(QSpinBox * editor, OvrMaterialTreeItem* matItem) const;
	void setEditorData(QDoubleSpinBox * editor, OvrMaterialTreeItem* matItem) const;
	void setEditorData(OvrFilePathEdit * editor, OvrMaterialTreeItem* matItem) const;
	void setEditorData(QLabel * editor, OvrMaterialTreeItem* matItem) const;
	void setPopEditorData(QPushButton * editor, OvrMaterialTreeItem* matItem) const;

	void setModelData(QLineEdit * editor, QStandardItemModel *model, OvrMaterialTreeItem* matItem) const;
	void setModelData(QPushButton * editor, QStandardItemModel *model, OvrMaterialTreeItem* matItem) const;
	void setModelData(QCheckBox * editor, QStandardItemModel *model, OvrMaterialTreeItem* matItem) const;
	void setModelData(QComboBox * editor, QStandardItemModel *model, OvrMaterialTreeItem* matItem) const;
	void setModelData(QSpinBox * editor, QStandardItemModel *model, OvrMaterialTreeItem* matItem) const;
	void setModelData(QDoubleSpinBox * editor, QStandardItemModel *model, OvrMaterialTreeItem* matItem) const;
	void setModelData(OvrFilePathEdit * editor, QStandardItemModel *model, OvrMaterialTreeItem* matItem) const;
	void setModelData(QLabel * editor, QStandardItemModel *model, OvrMaterialTreeItem* matItem) const;
	void setPopModelData(QPushButton * editor, QStandardItemModel *model, OvrMaterialTreeItem* matItem) const;

Q_SIGNALS:
	void on_finish_set_button_editor(OvrMaterialTreeItemPushButton* a_mat_item) const;
	void send_material_set_msg(int a_msg_type) const;
	void on_finish_set_color_editor(OvrMaterialTreeItemColorEdit* a_mat_item) const;
	void on_pop_custom_editor(OvrMaterialTreeItemCustomPop* a_mat_item) const;

private Q_SLOTS:
	;
	void on_open_material_clicked();
	void on_open_tex_file_dlg(OvrMaterialTreeItemPushButton* a_mat_item);
	void on_open_set_color_dlg(OvrMaterialTreeItemColorEdit* a_mat_item);
	void on_open_custom_pop(OvrMaterialTreeItemCustomPop* a_mat_item);

	void test_test();
private:

	OvrMaterialTreeItem* m_matItem = nullptr;

};
