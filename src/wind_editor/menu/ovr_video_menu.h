#pragma once

#include <QObject>

class OvrVideoMenu :public QObject
{
	Q_OBJECT
public:
	Q_INVOKABLE explicit OvrVideoMenu(QMenu* a_menu);
	virtual ~OvrVideoMenu();

protected slots:
	void triggered(QAction *action);
};