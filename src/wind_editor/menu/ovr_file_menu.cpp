#include "Header.h"
#include "ovr_file_menu.h"

#include "../dialog/new_prj_dlg.h"

static 	MenuItemDef items[] = {
	{ "新建工程(&N)\tCtrl+N","file_new_prj" },
	{ "打开工程(&P)\tCtrl+O","file_open_prj" },
	{ "关闭工程(&C)\tCtrl+C","file_close_prj" },
	{ "","" },
	{ "保存","file_save_prj_pb" },
	{ "","" },
	{ "退出(&X)\tAlt+F4","file_exit" }
};

OvrFileMenu::OvrFileMenu(QMenuBar* a_menu_bar):QObject(a_menu_bar)
{
	QMenu* menu = a_menu_bar->addMenu(QString::fromLocal8Bit("文件(&F)"));
	connect(menu, &QMenu::triggered, this, &OvrFileMenu::triggered, Qt::QueuedConnection);

	addMenuItems(menu, items, sizeof(items) / sizeof(MenuItemDef));
}

void OvrFileMenu::triggered(QAction *action)
{
	auto project_manager = OvrProjectManager::getInst();

	QString obj_name = action->objectName();

	if (obj_name == "file_new_prj")				onNewPrj();
	else if (obj_name == "file_open_prj")		onOpenPrj();
	else if (obj_name == "file_save_prj_pb")	onSavePB();
	else if (obj_name == "file_exit")			onClose();
}

void OvrFileMenu::onNewPrj()
{
	OvrProjectManager::getInst()->saveIfNeeded();

	NewPrjDlg dlg(OvrHelper::getInst()->getMainWindow());
	if (dlg.exec() == 1)
	{
		OvrProjectManager::getInst()->newProject(dlg.prj_file_path);
	}
}

void OvrFileMenu::onOpenPrj()
{
	OvrProjectManager::getInst()->saveIfNeeded();
	QString dir = OvrProjectManager::getInst()->getConfiger()->getLastOpenPrjDir();
	QString fileName = QFileDialog::getOpenFileName(nullptr, QString::fromLocal8Bit("打开工程"), dir, QString::fromLocal8Bit("project file(*.oproject)"));
	if (!fileName.isEmpty())
	{
		OvrProjectManager::getInst()->openProject(fileName);
	}
}

void OvrFileMenu::onSavePB()
{
	if (!OvrProjectManager::getInst()->saveProject())
	{
		OvrHelper::getInst()->warning("工程保存失败！");
	}
}

void OvrFileMenu::onClose()
{
	OvrHelper::getInst()->getMainWindow()->close();
}

OvrFileMenu::~OvrFileMenu()
{

}