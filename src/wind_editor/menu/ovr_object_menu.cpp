#include "Header.h"
#include "ovr_object_menu.h"
#include "ovr_3dobj_menu.h"
#include "ovr_light_menu.h"
#include "ovr_video_menu.h"

#include <wx_project/command/ovr_command_actor.h>

static 	MenuItemDef items[] = {
	{ "音频", "3dAudio", ovr_project::OvrCommandCreateActor::k3DAudio },
	{ "文本", "text", ovr_project::OvrCommandCreateActor::kText },
	{ "相机", "camera", ovr_project::OvrCommandCreateActor::kCameraActor },
	{ "粒子", "particle", ovr_project::OvrCommandCreateActor::kParticle }
};

OvrObjectMenu::OvrObjectMenu(QMenuBar* a_menu_bar):QObject(a_menu_bar)
{
	QMenu* menu = a_menu_bar->addMenu(QString::fromLocal8Bit("对象(&O)"));

	QList<QAction*> actions;
	auto action = addMenuItem(menu, "空对象", "empty", ovr_project::OvrCommandCreateActor::kEmptyActor);
	actions.push_back(action);

	new Ovr3dobjMenu(menu);
	new OvrLightMenu(menu);
	new OvrVideoMenu(menu);

	addMenuItems(menu, items, sizeof(items) / sizeof(MenuItemDef), &actions);

	for (auto item : actions)
	{
		connect(item, &QAction::triggered, [this, item](bool) {
			triggered(item);
		});
	}
}

void OvrObjectMenu::triggered(QAction *action)
{
	OvrString display_name = action->objectName().toLocal8Bit().constData();
	auto actor_type = (ovr_project::OvrCommandCreateActor::ActorType)action->data().toInt();

	ovr_project::OvrCommandCreateActor* new_command = new ovr_project::OvrCommandCreateActor;
	new_command->SetActorType(actor_type);
	new_command->SetDisplayName(display_name);
	new_command->SetForwardPosition(true);

	ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new_command);
}

OvrObjectMenu::~OvrObjectMenu()
{

}