#include "Header.h"
#include "ovr_tool_menu.h"

#include "../dialog/ovr_ambient_color_dlg.h"

static 	MenuItemDef items[] = {
	{ "������(&L)","ambient_color" }
};

OvrToolMenu::OvrToolMenu(QMenuBar* a_menu_bar):QObject(a_menu_bar)
{
	QMenu* menu = a_menu_bar->addMenu(QString::fromLocal8Bit("����(&T)"));
	connect(menu, &QMenu::triggered, this, &OvrToolMenu::triggered, Qt::QueuedConnection);

	addMenuItems(menu, items, sizeof(items) / sizeof(MenuItemDef));
}

void OvrToolMenu::triggered(QAction *action)
{
	QString obj_name = action->objectName();

	if (obj_name == "ambient_color")
	{
		auto wnd = OvrHelper::getInst()->getMainWindow();
		OvrAmbientColorDlg ambientColorDlg(wnd);
		ambientColorDlg.exec();
	}
}

OvrToolMenu::~OvrToolMenu()
{

}