#include "Header.h"
#include "ovr_light_menu.h"

#include <wx_project/command/ovr_command_actor.h>

static 	MenuItemDef items[] = {
	{ "平行光(&S)", "DirectionalLight", ovr_engine::eDirectionLight },
	{ "点光灯(&P)", "PointLight", ovr_engine::ePointLight },
	{ "聚光灯(&F)", "SpotLight", ovr_engine::eSpotLight }
};

OvrLightMenu::OvrLightMenu(QMenu* a_menu):QObject(a_menu)
{
	QMenu* menu = a_menu->addMenu(QString::fromLocal8Bit("灯光(&L)"));
	connect(menu, &QMenu::triggered, this, &OvrLightMenu::triggered, Qt::QueuedConnection);

	addMenuItems(menu, items, sizeof(items) / sizeof(MenuItemDef));
}

void OvrLightMenu::triggered(QAction *action)
{
	auto light_type = (ovr_engine::OvrLightType)action->data().toInt();
	OvrString display_name = action->objectName().toLocal8Bit().constData();

	ovr_project::OvrCommandCreateLight* new_command = new ovr_project::OvrCommandCreateLight;
	new_command->SetLightType(light_type);
	new_command->SetDisplayName(display_name);
	new_command->SetForwardPosition(true);
	ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new_command);
}

OvrLightMenu::~OvrLightMenu()
{

}