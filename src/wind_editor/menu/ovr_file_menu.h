#pragma once

#include <QObject>

class OvrFileMenu :public QObject
{
	Q_OBJECT
public:
	Q_INVOKABLE explicit OvrFileMenu(QMenuBar* a_menu_bar);
	virtual ~OvrFileMenu();

protected slots:
	void triggered(QAction *action);

protected:
	void onOpenPrj();
	void onNewPrj();
	void onSavePB();
	void onClose();

};