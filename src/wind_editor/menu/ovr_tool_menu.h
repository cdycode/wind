#pragma once

#include <QObject>

class OvrToolMenu :public QObject
{
	Q_OBJECT
public:
	Q_INVOKABLE explicit OvrToolMenu(QMenuBar* a_menu_bar);
	virtual ~OvrToolMenu();

protected slots:
	void triggered(QAction *action);
};