#include "Header.h"
#include "ovr_3dobj_menu.h"

#include <wx_project/command/ovr_command_actor.h>

static 	MenuItemDef items[] = {
	{ "立方体(&T)", "cube" },
	{ "球体(&R)", "sphere" },
	{ "圆柱(&C)", "cylinder" },
	{ "面(&F)", "plane" } 
};

Ovr3dobjMenu::Ovr3dobjMenu(QMenu* a_menu):QObject(a_menu)
{
	QMenu* menu = a_menu->addMenu(QString::fromLocal8Bit("3D对象(&B)"));
	connect(menu, &QMenu::triggered, this, &Ovr3dobjMenu::triggered, Qt::QueuedConnection);

	addMenuItems(menu, items, sizeof(items) / sizeof(MenuItemDef));
}

void Ovr3dobjMenu::triggered(QAction *action)
{
	static QMap<OvrString, OvrString> name_path_map = {
		{ "cube","system/content/base_obj/Cube.mesh" },
		{ "sphere", "system/content/base_obj/pSphere.mesh" },
		{ "cylinder", "system/content/base_obj/pCylinder_3.mesh" },
		{ "plane", "system/content/base_obj/pPlane.mesh" }
	};

	OvrString display_name = action->objectName().toLocal8Bit().constData();
	OvrString file_path;
	if (name_path_map.contains(display_name))
	{
		file_path = name_path_map[display_name];
	}

	if (file_path.GetStringSize() > 0)
	{
		ovr_project::OvrCommandLoadActor* new_command = new ovr_project::OvrCommandLoadActor;
		new_command->SetFilePath(file_path);
		new_command->SetDisplayName(display_name);
		new_command->SetForwardPosition(true);
		ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new_command);
	}
}

Ovr3dobjMenu::~Ovr3dobjMenu()
{

}