#pragma once

#include <QObject>

class Ovr3dobjMenu :public QObject
{
	Q_OBJECT
public:
	Q_INVOKABLE explicit Ovr3dobjMenu(QMenu* a_menu);
	virtual ~Ovr3dobjMenu();

protected slots:
	void triggered(QAction *action);
};