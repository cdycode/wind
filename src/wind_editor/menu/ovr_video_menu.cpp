#include "Header.h"
#include "ovr_video_menu.h"

#include <wx_project/command/ovr_command_actor.h>

static 	MenuItemDef items[] = {
	{ "2D视频(&T)", "Video2D", ovr_engine::Video2D },
	{ "3D视频(&I)", "video3D", ovr_engine::Video3D },
	{ "180视频(&E)", "Video180", ovr_engine::Video180 },
	{ "全景视频(&A)", "videoPano", ovr_engine::Video360 },
	{ "全景盒子(&H)", "videoPanoBox", ovr_engine::Video360Box } 
};

OvrVideoMenu::OvrVideoMenu(QMenu* a_menu):QObject(a_menu)
{
	QMenu* menu = a_menu->addMenu(QString::fromLocal8Bit("视频(&V)"));
	connect(menu, &QMenu::triggered, this, &OvrVideoMenu::triggered, Qt::QueuedConnection);

	addMenuItems(menu, items, sizeof(items) / sizeof(MenuItemDef));
}

void OvrVideoMenu::triggered(QAction *action)
{
	auto video_type = (ovr_engine::VideoType)action->data().toInt();
	OvrString display_name = action->objectName().toLocal8Bit().constData();

	ovr_project::OvrCreateVideoActor* new_command = new ovr_project::OvrCreateVideoActor;
	new_command->SetVideoType(video_type);
	new_command->SetDisplayName(display_name);
	new_command->SetForwardPosition(true);

	ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new_command);
}

OvrVideoMenu::~OvrVideoMenu()
{

}