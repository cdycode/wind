#pragma once

#include <QObject>

class OvrObjectMenu :public QObject
{
	Q_OBJECT
public:
	Q_INVOKABLE explicit OvrObjectMenu(QMenuBar* a_menu_bar);
	virtual ~OvrObjectMenu();

protected slots:
	void triggered(QAction *action);
};