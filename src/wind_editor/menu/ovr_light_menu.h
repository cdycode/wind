#pragma once

#include <QObject>

class OvrLightMenu :public QObject
{
	Q_OBJECT
public:
	Q_INVOKABLE explicit OvrLightMenu(QMenu* a_menu);
	virtual ~OvrLightMenu();

protected slots:
	void triggered(QAction *action);
};