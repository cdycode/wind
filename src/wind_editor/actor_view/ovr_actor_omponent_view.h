#pragma once

#include <QWidget>

namespace ovr_project
{
	class IOvrActorProperty;
	class IOvrActorPropertyGroup;
	class OvrActorPropertyVector4f;
}

class OvrColorLabel;

class OvrComponentView : public QWidget, public OvrEventSender, public OvrEventReceiver
{
	Q_OBJECT
public:
	static OvrComponentView* CreateComponentItem(ovr_project::IOvrActorPropertyGroup* a_group);

	explicit OvrComponentView(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	virtual ~OvrComponentView();

	bool init(ovr_project::IOvrActorPropertyGroup* a_group);

	virtual QString senderName() const override { return "OvrComponentView"; }

	virtual QString name() override { return "OvrComponentView"; }
	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;

signals:
	void onResFileSelected(QString a_file_path);
	void onReload();

protected slots:
	void reload();

protected:
	void reloadComponent(bool a_is_layout_changed);

	bool init();

	void addField(ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group);
	void addFieldBool(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group);
	
	void addFieldString(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group);
	void addFieldText(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group);
	
	void addFieldFloat(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group);
	void addFieldSlide(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group);

	void addFieldVector2f(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group);
	void addFieldAlign(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group);
	
	void addFieldVector3f(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group);

	void addFieldVector4f(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group);
	void addFieldColor(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group);

	void addFieldRes(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group);
	void addFieldResImg(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group);

	void addFieldCombox(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group);

	void addFieldTitle(int a_row_count, const OvrString& a_name, bool a_is_sub_group);

	void addSubGroup(ovr_project::IOvrActorPropertyGroup* a_group);

	void OnUpdateTransform();
	void UpdateTransField(ovr_project::IOvrActorProperty* a_field);

	void updateVideoValue(ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_component);

	QPixmap getTexturePixmap(const QString & str_path);

	QColor chooseColor(const QColor& color, OvrColorLabel* a_color_label);
	void   changeFieldColor(OvrColorLabel* a_color_label, ovr_project::OvrActorPropertyVector4f* a_field);
	void   enableApplyButton(QToolButton* a_button, ovr_project::IOvrActorProperty* a_field, QString a_file_path);
	void   showMatEditor(ovr_project::IOvrActorProperty* a_field);

	QGridLayout* pane_layout;
	QVBoxLayout * base_layout = nullptr;

	bool atts_flag = false;
	bool is_transform = false;

	struct RTSUIField
	{
		QVector<QLineEdit*> widgets;
	};

	QMap<QString, RTSUIField> rts;
	ovr_project::IOvrActorPropertyGroup* bind_group = nullptr;

	QWidget* pane = nullptr;

	QString select_res_name;
};

///////////////////////////////////////////////////////////////////////////////////////////////////
class OvrLineEditEx : public QLineEdit
{
	Q_OBJECT
public:
	explicit OvrLineEditEx(const QString& a_text, QWidget *parent = Q_NULLPTR) :QLineEdit(a_text, parent)
	{
		setReadOnly(true);
		setToolTip(a_text);
	};

	~OvrLineEditEx() {};

	void setTextEx(const QString& a_text)
	{
		setText(a_text);
		setToolTip(a_text);
	}

Q_SIGNALS:
	void onMouseDbClick();

private:
	virtual void mouseDoubleClickEvent(QMouseEvent *event) override
	{
		emit onMouseDbClick();
	}
};

class OvrComboxEx :public QComboBox
{
	Q_OBJECT
public:
	explicit OvrComboxEx(QWidget *parent = Q_NULLPTR) :QComboBox(parent) 
	{
		connect(this, SIGNAL(currentIndexChanged(int)), this, SIGNAL(currentChanged(int)));
	}

signals:
	void currentChanged(int a_index);
};

#include <QButtonGroup>

class OvrButtonGroup : public QButtonGroup
{
	Q_OBJECT
public:
	explicit OvrButtonGroup(QObject *parent = Q_NULLPTR) :QButtonGroup(parent)
	{
		connect(this, SIGNAL(buttonClicked(int)), this, SIGNAL(alignButtonClicked(int)));
	}

signals:
	void alignButtonClicked(int id);

};