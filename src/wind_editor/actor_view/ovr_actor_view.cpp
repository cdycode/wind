#include "Header.h"
#include "ovr_actor_view.h"

#include <QSplitter>

#include "ovr_actor_outline_tree.h"
#include "ovr_actor_property_tree.h"
#include "../utils_ui/size_dock_widget.h"

#include <wx_project/command/ovr_command_actor.h>

OvrActorView::OvrActorView(QWidget* parent, Qt::WindowFlags f)
	:QDockWidget(QString::fromLocal8Bit("对象管理器"), parent, f)
{
	setObjectName(QStringLiteral("dockWidget_right"));
	setFeatures(QDockWidget::AllDockWidgetFeatures);
	setAllowedAreas(Qt::RightDockWidgetArea);

	init_custom_ui();
	preset_connects();
	set_custom_style_sheet();
}

OvrActorView::~OvrActorView()
{
}

void OvrActorView::CreateEmptyObject()
{
	ovr_project::OvrCommandCreateActor* new_command = new ovr_project::OvrCommandCreateActor;
	new_command->SetDisplayName("empty");
	new_command->SetForwardPosition(true);
	ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new_command);
}

void OvrActorView::paintEvent(QPaintEvent *e)
{
	ApplyWidgetStyleSheet(this);
}

void OvrActorView::mousePressEvent(QMouseEvent *event)
{
	qDebug() << rect() << "\t" << objectName();
	QWidget::mousePressEvent(event);
}

void OvrActorView::set_custom_style_sheet()
{
}

void OvrActorView::init_custom_ui()
{
	init_dock_title_bar();
	init_dock_content_widget();
}

void OvrActorView::init_dock_title_bar()
{
	QWidget * new_title_widget = new QWidget();
	setTitleBarWidget(new_title_widget);

	QHBoxLayout * h_layout = new QHBoxLayout(new_title_widget);
	h_layout->setSpacing(0);
	h_layout->setContentsMargins(0, 0, 0, 0);

	QToolBar *res_bar = new QToolBar(new_title_widget);
	h_layout->addWidget(res_bar);
	{
		QAction* action = res_bar->addAction(QIcon(""), QString::fromLocal8Bit("创建空对象"));
		action->setToolTip(QString::fromLocal8Bit("可以通过文件夹组织场景中的资源"));
		connect(action, SIGNAL(triggered()), this, SLOT(CreateEmptyObject()));

		QLabel* space = new QLabel();
		space->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
		action = res_bar->addWidget(space);
		action->setDisabled(true);

		action = res_bar->addAction(QIcon(getImagePath("icon_search_max.png")), "");
		action->setToolTip(QString::fromLocal8Bit("搜素"));

		action = res_bar->addAction(QIcon(getImagePath("icon_max.png")), "", [this]() { setFloating(!isFloating()); });
		action->setToolTip(QString::fromLocal8Bit("放大窗口"));
	}
}

void OvrActorView::init_dock_content_widget()
{
	SizeDockWidget * dockWidgetContents = new SizeDockWidget();
	QVBoxLayout* vbox_layout = new QVBoxLayout(dockWidgetContents);
	vbox_layout->setSpacing(0);
	vbox_layout->setContentsMargins(0, 0, 0, 0);
	this->setWidget(dockWidgetContents);

	dockWidgetContents->updateSizeHint(QSize(285, -1));

	///////////////////////////////////////////////////////////////////////
	QSplitter * splitter = new QSplitter(this);
	splitter->setOrientation(Qt::Vertical);

	actor_tree = new OvrActorOutlineTree(splitter);
	prop_pane = new OvrActorPropertyTree(splitter);

	vbox_layout->addWidget(splitter);
}

void OvrActorView::preset_connects()
{
}
