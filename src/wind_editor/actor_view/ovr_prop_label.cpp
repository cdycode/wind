#include "Header.h"
#include "ovr_prop_label.h"

OvrCdcLabel::OvrCdcLabel(QWidget *parent, Qt::WindowFlags f)
	:QLabel(parent, f)
{
	setToolTip(QString::fromLocal8Bit("˫���޸�"));
}

OvrCdcLabel::OvrCdcLabel(const QString &text, QWidget *parent, Qt::WindowFlags f) 
	: QLabel(text, parent, f)
{
	setToolTip(QString::fromLocal8Bit("˫���޸�"));
}

OvrCdcLabel::~OvrCdcLabel()
{
}

void OvrCdcLabel::mouseDoubleClickEvent(QMouseEvent * event)
{
	emit(onMouseDbClick());
}

//-------------------------------------------------------------------------------------
// OvrColorLabel
OvrColorLabel::OvrColorLabel(QWidget *parent, Qt::WindowFlags f)
	:OvrCdcLabel(parent, f)
{
}

OvrColorLabel::OvrColorLabel(const QString &text, QWidget *parent, Qt::WindowFlags f)
	:OvrCdcLabel(text, parent, f)
{
}

OvrColorLabel::~OvrColorLabel()
{
}

void OvrColorLabel::SetCurrentColor(const OvrVector4 &rgba)
{
	QColor curColor(rgba._x * 255, rgba._y * 255, rgba._z * 255, rgba._w * 255);
	SetCurrentColor(curColor);
}

void OvrColorLabel::SetCurrentColor(const QColor & color)
{
	QString strLabelSheet = QString().sprintf("QLabel{background:rgb(%d,%d,%d);}",
		color.red(), color.green(), color.blue(), color.alpha());
	this->setStyleSheet(strLabelSheet);
}


//-------------------------------------------------------------------------------------
// OvrTextureLabel
OvrTextureLabel::OvrTextureLabel(QWidget *parent, Qt::WindowFlags f)
	:OvrCdcLabel(parent, f)
{
}

OvrTextureLabel::OvrTextureLabel(const QString &text, QWidget *parent, Qt::WindowFlags f)
	: OvrCdcLabel(text, parent, f)
{
}

OvrTextureLabel::~OvrTextureLabel()
{
}

void OvrTextureLabel::SetTexture()
{
}

