#include "Header.h"

#include <QColorDialog>
#include <QFontComboBox>
#include <QPlainTextEdit>
#include <QRadioButton>

#include "ovr_actor_property_view.h"
#include "ovr_actor_outline_tree.h"

#include "../dialog/ovr_file_dialog.h"
#include "ovr_prop_label.h"

#include "../res_view/ovr_resource_tree_model.h"
#include "../res_view/ovr_resource_list_widget.h"

#include "../material/ovr_material_editor.h"

#include <wx_project/command/ovr_material_changed.h>
#include <wx_engine/ovr_particle_system.h>
#include <wx_engine/OvrParticleEmitter.h>
#include <wx_engine/ovr_particle_emitter_box.h>
#include <wx_engine/ovr_particle_emitter_line.h>
#include <wx_engine/ovr_particle_emitter_mesh.h>
#include <wx_engine/ovr_particle_emitter_point.h>
#include <wx_engine/ovr_particle_emitter_sphere.h>
#include <wx_engine/ovr_particle_emitter_circle.h>

#include <wx_engine/ovr_mesh_component.h>

#include <wx_base/wx_quaternion.h>
#include "wx_engine/ovr_material.h"

#include <wx_asset_manager/ovr_archive_asset_manager.h>
#include <wx_asset_manager/ovr_archive_texture.h>


//////////////////////////////////////////////////
// float 字串输入限制
const QString cs_reg_exp_filter = QString::fromLocal8Bit("^[+\\-]{0,1}\\d*\\.{0,1}\\d*$");
const QString s_label_style_sheet = QString::fromLocal8Bit("QLabel{background:rgb(92, 12, 235);}");
const QString s_text_edit_style_sheet = QString::fromLocal8Bit(
	"QPlainTextEdit{max-height:120px; border:1px solid rgb(76,76,76); background:rgb(62,62,62); color:rgb(204,204,204); selection-background-color:rgb(165, 165, 165); selection-color:rgb(32, 32, 32);}"
);
const QString s_tool_btn_style_sheet = QString::fromLocal8Bit("QToolButton{border:1px solid rgb(76, 76, 76); background:rgb(87, 87, 87); color:rgb(204, 204, 204);}");

#define M_PI       3.14159265358979323846
// 角度到弧度
double angle_to_arc(double a_angle)
{
	return a_angle*M_PI / 180.;
}

// 弧度到角度
double arc_to_angle(double a_arc)
{
	return a_arc * 180. / M_PI;
}

// class OvrPropGroup
OvrPropGroup::OvrPropGroup(ovr_engine::OvrActor* a_actor, QWidget* parent, Qt::WindowFlags f)
	:QWidget(parent, f)
{
	m_actor = a_actor;
}

OvrPropGroup::~OvrPropGroup()
{

}

void OvrPropGroup::init()
{
	beginAddItem();

	onAddingItems();

	endAddItem();

}

void OvrPropGroup::onAddingItems()
{
	assert(false);
}

void OvrPropGroup::paintEvent(QPaintEvent * e)
{
	ApplyWidgetStyleSheet(this);
}

void OvrPropGroup::onUpdateTransform()
{

}

void OvrPropGroup::onSetReName(QString a_actor_id)
{

}

void OvrPropGroup::onSetShowHide(bool isShow)
{
	
}

void OvrPropGroup::beginAddItem()
{
	pane_layout = new QGridLayout(this);
	pane_layout->setContentsMargins(OVR_PROP_ROW_MARGIN1, OVR_PROP_ROW_MARGIN, OVR_PROP_ROW_MARGIN, OVR_PROP_ROW_MARGIN);
	pane_layout->setVerticalSpacing(OVR_PROP_ROW_INTER_SPACE);
	pane_layout->setHorizontalSpacing(10);
}

void OvrPropGroup::endAddItem()
{
	onSetLayoutStyle();
}

QComboBox* OvrPropGroup::addCombo(const char* a_title)
{
	QLabel* label = new QLabel(QString::fromLocal8Bit(a_title));
	pane_layout->addWidget(label, group_row, 0, Qt::AlignLeft | Qt::AlignVCenter);

	QComboBox* value = new QComboBox;
	pane_layout->addWidget(value, group_row, 1/*, Qt::AlignLeft | Qt::AlignVCenter*/);

	++group_row;

	return value;
}

QCheckBox* OvrPropGroup::addCheck(const char* a_title)
{
	QLabel* label = new QLabel(QString::fromLocal8Bit(a_title));
	pane_layout->addWidget(label, group_row, 0, Qt::AlignLeft | Qt::AlignVCenter);

	QCheckBox* value = new QCheckBox;
	pane_layout->addWidget(value, group_row, 1, Qt::AlignLeft | Qt::AlignVCenter);

	++group_row;

	return value;
}

QLineEdit* OvrPropGroup::addEdit(const char* a_title)
{
	QLabel* label = new QLabel(QString::fromLocal8Bit(a_title));
	pane_layout->addWidget(label, group_row, 0, Qt::AlignLeft | Qt::AlignVCenter);

	QLineEdit* value = new QLineEdit(this);
	pane_layout->addWidget(value, group_row, 1/*, Qt::AlignLeft | Qt::AlignVCenter*/);

	++group_row;

	return value;
}

void OvrPropGroup::addXYZ(const char* a_title, QList<QLineEdit*>& a_list)
{
	int col = 0;
	QLabel* label = nullptr;
	QLineEdit* value = nullptr;

	label = new QLabel(QString::fromLocal8Bit(a_title));
	pane_layout->addWidget(label, group_row, col++, Qt::AlignLeft | Qt::AlignVCenter);

	const char* titles[] = { "X", "Y", "Z" };	
	for (int i = 0; i < 3; ++i)
	{
		label = new QLabel(QString::fromLocal8Bit(titles[i]));
		pane_layout->addWidget(label, group_row, col++, Qt::AlignRight | Qt::AlignVCenter);

		value = new QLineEdit;
		pane_layout->addWidget(value, group_row, col++/*, Qt::AlignLeft | Qt::AlignVCenter*/);

		QRegExp regExpPort = QRegExp(cs_reg_exp_filter);
		QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExpPort);
		value->setInputMethodHints(Qt::ImhDigitsOnly);
		value->setMaxLength(10);
		value->setValidator(portRegExpValidator);

		a_list.append(value);
	}

	++group_row;
}

void OvrPropGroup::addHBoxXYZ(const char * a_title, QList<QLineEdit*>& a_edit_list, QList<QLabel*>& a_label_list)
{
	QLabel* label = nullptr;
	QLineEdit* value = nullptr;

	label = new QLabel(QString::fromLocal8Bit(a_title));
	pane_layout->addWidget(label, group_row, 0, Qt::AlignLeft | Qt::AlignVCenter);

	QHBoxLayout * h_layout = new QHBoxLayout();
	h_layout->setSpacing(OVR_PROP_ROW_INTER_SPACE);

	const char* titles[] = { "X", "Y", "Z" };
	for (int i = 0; i < 3; ++i)
	{
		label = new QLabel(QString::fromLocal8Bit(titles[i]));
		h_layout->addWidget(label);

		value = new QLineEdit;
		h_layout->addWidget(value);

		QRegExp regExpPort = QRegExp(cs_reg_exp_filter);
		QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExpPort);
		value->setInputMethodHints(Qt::ImhDigitsOnly);
		value->setMaxLength(10);
		value->setValidator(portRegExpValidator);

		a_edit_list.append(value);
		a_label_list.append(label);
	}

	pane_layout->addLayout(h_layout, group_row, 1);

	++group_row;
}

QLineEdit* OvrPropGroup::addMeshMaterial(const char* a_title)
{
	int col = 0;

	QLabel* label = new QLabel(QString::fromLocal8Bit(a_title));
	pane_layout->addWidget(label, group_row, col++, Qt::AlignLeft | Qt::AlignVCenter);

	QLineEdit* value = new OvrLineEdit(this);
	mesh_material_value = value;
	mesh_material_value->setReadOnly(true);
	pane_layout->addWidget(value, group_row, col++/*, Qt::AlignLeft | Qt::AlignVCenter*/);

	apply_button = new QToolButton();
	apply_button->setText(QString::fromLocal8Bit("应用"));
	apply_button->setStyleSheet(s_tool_btn_style_sheet);
	pane_layout->addWidget(apply_button, group_row, col++, Qt::AlignLeft | Qt::AlignVCenter);

	locate_button = new QToolButton();
	locate_button->setText(QString::fromLocal8Bit("定位"));
	locate_button->setStyleSheet(s_tool_btn_style_sheet);
	pane_layout->addWidget(locate_button, group_row, col++, Qt::AlignLeft | Qt::AlignVCenter);

	connect(locate_button, SIGNAL(clicked(bool)), this, SLOT(onLocate(bool)), Qt::QueuedConnection);

	++group_row;
	return value;
}

QLineEdit *OvrPropGroup::addDigitesLineEdit(const QString & a_title)
{
	QLabel* label = new QLabel(a_title);
	pane_layout->addWidget(label, group_row, 0, 1, 1, Qt::AlignLeft | Qt::AlignVCenter);

	QLineEdit* value = new QLineEdit(this);
	pane_layout->addWidget(value, group_row, 1/*, 1, 1*//*, Qt::AlignLeft | Qt::AlignVCenter*/);

	QRegExp regExpPort = QRegExp(cs_reg_exp_filter);
	QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExpPort);
	value->setInputMethodHints(Qt::ImhDigitsOnly);
	value->setMaxLength(10);
	value->setValidator(portRegExpValidator);

	++group_row;
	return value;
}

OvrTextureLabel * OvrPropGroup::addTexturePreview(const QString & a_title)
{
	QLabel* label = new QLabel(a_title, this);
	pane_layout->addWidget(label, group_row, 0, 1, 1, Qt::AlignLeft | Qt::AlignVCenter);

	OvrTextureLabel* value = new OvrTextureLabel(this);
	value->setStyleSheet("QLabel{background:rgb(0, 0, 0);}");
	value->setMaximumSize(QSize(32, 32));
	value->setMinimumSize(QSize(32, 32));
	pane_layout->addWidget(value, group_row, 1, 1, 1, Qt::AlignLeft | Qt::AlignVCenter);

	++group_row;
	return value;
}

OvrColorLabel * OvrPropGroup::addColorSelLabel(const QString & a_title)
{
	QLabel* label = new QLabel(a_title, this);
	pane_layout->addWidget(label, group_row, 0, Qt::AlignLeft | Qt::AlignVCenter);

	OvrColorLabel* value = new OvrColorLabel(this);

	pane_layout->addWidget(value, group_row, 1);

	++group_row;

	return value;
}

QColor OvrPropGroup::getSelectedColor(const QColor color)
{
	QColorDialog colorDlg(this);
	//colorDlg. setOptions(QColorDialog::ShowAlphaChannel);
	colorDlg.setCurrentColor(color);

	//QString strDefaultStyle = QString::fromLocal8Bit(
	//	"QWidget{background:rgb(48, 48, 48); color:rgb(204, 204, 204);}"
	//	//"QPushButton{height: 25px;}"
	//);
	//colorDlg.setStyleSheet(strDefaultStyle);

	connect(&colorDlg, SIGNAL(currentColorChanged(const QColor&)), this, SLOT(onColorChanged(const QColor&)));

	int nRes = colorDlg.exec();
	if (QDialog::Rejected == nRes)
		return color;

	QColor curColor = colorDlg.currentColor();

	return curColor;
}

void OvrPropGroup::update_color_label_preview(QLabel * label, const QColor & color)
{
}

QPixmap OvrPropGroup::getTexturePixmap(const QString & relation_path)
{
	QByteArray qbaPath = relation_path.toLocal8Bit();
	auto arch_asset = ovr_asset::OvrArchiveAssetManager::GetIns()->LoadAsset(qbaPath.constData());
	auto material_file = dynamic_cast<ovr_asset::OvrArchiveTexture*>(arch_asset);

	if (!material_file) {
		return QPixmap(getImagePath("app_icon.png"));
	}

	auto rgb_buff = material_file->GetRGBABuffer();

	int n_size = material_file->GetDataSize();

	int width = material_file->GetWidth();
	int height = material_file->GetHeight();
	QImage texImg(rgb_buff, width, height, QImage::Format_RGB888);

	return QPixmap::fromImage(texImg);
}

void OvrPropGroup::onLocate(bool checked)
{
	notifyEvent("onLocateRes", mesh_material_value->text(), 0);
}

void OvrPropGroup::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void OvrPropGroup::showHideGroup()
{
	is_group_visible = !is_group_visible;

	is_group_visible ? group_pane->show() : group_pane->hide();
}

//////////////////////////////////////////////////
// class OvrPropBaseGroup

void OvrPropBaseGroup::editingFinished()
{
	QString strDisplayName = name_edit->text();
	if (strDisplayName.isEmpty())
	{
		name_edit->setText(QString::fromLocal8Bit(m_actor->GetDisplayName().GetStringConst()));
	}
	else if (strDisplayName != QString::fromLocal8Bit(m_actor->GetDisplayName().GetStringConst()))
	{
		QByteArray tmp = strDisplayName.toLocal8Bit();
		m_actor->SetDisplayName(tmp.constData());

		QString str_uuid = QString::fromLocal8Bit(m_actor->GetUniqueName().GetStringConst());
		OvrActorOutlineTree::getInst()->update_display_name(str_uuid, strDisplayName);
	}
}

void OvrPropBaseGroup::check_btn_changed(bool checked)
{
	QCheckBox * check_box = dynamic_cast<QCheckBox*>(sender());

	if (check_box == visible_check) {
		//选中， 不可见; 未选中， 可见
		m_actor->SetVisible(!checked);

		QString str_uuid = QString::fromLocal8Bit(m_actor->GetUniqueName().GetStringConst());
		OvrActorOutlineTree::getInst()->update_eye_status(str_uuid, !checked);
	}
	else if (check_box == moveable_check) {
		//选中，可移动; 未选中，不可移动
		m_actor->SetStatic(checked);
	}
	else if (check_box == focus_check) {
	}
	else if (check_box == shadow_check) {
		// 阴影设置
		//m_actor->SetCastShadow(checked);
	}
}

void OvrPropBaseGroup::onAddingItems()
{
	QLineEdit* class_edit = addEdit("类型");
	name_edit = addEdit("名称");
	name_edit->setMaxLength(64);
	name_edit->setText(QString::fromLocal8Bit(m_actor->GetDisplayName().GetStringConst()));
	connect(name_edit, &QLineEdit::editingFinished, this, &OvrPropBaseGroup::editingFinished, Qt::QueuedConnection);

	visible_check = addCheck("隐藏");
	moveable_check = addCheck("静止");
	focus_check = addCheck("焦点");
	shadow_check = addCheck("阴影");

	//获取是否选中
	visible_check->setChecked(!m_actor->IsVisible());
	moveable_check->setChecked(m_actor->IsStatic());
	//focus_check->setChecked(m_actor->IsIntersetTestEnable());
	//shadow_check->setChecked(m_actor->IsCastShadow());

	connect(OvrActorOutlineTree::getInst(), &OvrActorOutlineTree::actor_visible_changed,
		[this](ovr_engine::OvrActor * a_actor, bool b_visible) {
		if (a_actor == m_actor) {
			visible_check->setChecked(!b_visible);
		}
	});

	connect(visible_check, SIGNAL(clicked(bool)), this, SLOT(check_btn_changed(bool)), Qt::QueuedConnection);
	connect(moveable_check, SIGNAL(clicked(bool)), this, SLOT(check_btn_changed(bool)), Qt::QueuedConnection);
	connect(focus_check, SIGNAL(clicked(bool)), this, SLOT(check_btn_changed(bool)), Qt::QueuedConnection);
	connect(shadow_check, SIGNAL(clicked(bool)), this, SLOT(check_btn_changed(bool)), Qt::QueuedConnection);

	class_edit->setReadOnly(true);

	const char* object_type = "";
	/*switch (m_actor->GetType())
	{
	case ovr_engine::kObjectEmptyActor: 
		object_type = "EmptyActor";
		break;
	case ovr_engine::kObjectMesh: 
	{
		ovr_engine::OvrMeshActor* mesh_actor = (ovr_engine::OvrMeshActor*) m_actor;
		if (mesh_actor->HasSkeleton())
		{
			object_type = "SkeletalMesh";
		}
		else 
		{
			object_type = "StaticMesh";
		}
	}
		break;
	case ovr_engine::kObjectVideoScreen:
	{
		ovr_engine::OvrVideoScreenActor* video_actor = (ovr_engine::OvrVideoScreenActor*) m_actor;
		switch (video_actor->GetVideoType())
		{
		case ovr_engine::OvrVideoScreenActor::Video360Box:
			object_type = "VideoPanoBox";
			break;
		case ovr_engine::OvrVideoScreenActor::Video360:
			object_type = "VideoPano";
			break;
		case ovr_engine::OvrVideoScreenActor::Video180:
			object_type = "Video180";
			break;
		case ovr_engine::OvrVideoScreenActor::Video3D:
			object_type = "Video3D";
			break;
		case ovr_engine::OvrVideoScreenActor::Video2D:
			object_type = "Video2D";
			break;
		default:
			break;
		}
	}
		break;
	case ovr_engine::kObject3DAudio:
		object_type = "3DAudio";
		break;
	case ovr_engine::kObjectLight:
		object_type = "Light";
		break;
	case ovr_engine::kObjectText:
		object_type = "Text";
		break;
	case ovr_engine::kObjectCameraActor:
		object_type = "Camera";
		break;
	case ovr_engine::kObjectCollider:
		object_type = "Collider";
		break;
	case ovr_engine::kObjectParticle:
		object_type = "Particle";
		break;
	default:
		object_type = "unknown object";
		break;
	}*/
	class_edit->setText(object_type);
}

void OvrPropBaseGroup::onSetLayoutStyle()
{
	pane_layout->setColumnStretch(1, 100);
}

void OvrPropBaseGroup::onSetShowHide(bool isShow)
{
	if (isShow == false)
	{
		//不可见 选中
		visible_check->setCheckState(Qt::Checked);
	}
	else
	{
		visible_check->setCheckState(Qt::Unchecked);
	}

	m_actor->SetVisible(isShow);
}

void OvrPropBaseGroup::onSetReName(QString a_actor_id)
{
	name_edit->setReadOnly(false);
}

//////////////////////////////////////////////////
// class OvrPropTransGroup

void OvrPropTransGroup::onAddingItems()
{
	addXYZ("位置", trans);
	addXYZ("缩放", scale);
	addXYZ("旋转", rotate);

	const OvrTransform& rts = m_actor->GetTransform();
	float v[3] = { 0 };
	rts.quaternion.get_euler_angles(v[0], v[1], v[2]);
	for (int i = 0; i < 3; ++i)
	{
		//位置
		trans.at(i)->setText(QString::number(rts.position[i], 'f', 4));

		//缩放
		scale.at(i)->setText(QString::number(rts.scale[i], 'f', 4));

		//旋转
		int n_angle = arc_to_angle(v[i]);
		rotate.at(i)->setText(QString::number(n_angle));

		QString strToolInfo;
		if (0 == i) {
			strToolInfo = QString::fromLocal8Bit("(-90,90]");   // x
		}
		else if (1 == i) {
			strToolInfo = QString::fromLocal8Bit("(-180,180]");   // y
		}
		else if (2 == i) {
			strToolInfo = QString::fromLocal8Bit("(-180,180]");   // z
		}
		rotate.at(i)->setToolTip(strToolInfo);

		connect(trans.at(i), &QLineEdit::editingFinished, this, &OvrPropTransGroup::transEditingFinished, Qt::QueuedConnection);
		connect(scale.at(i), &QLineEdit::editingFinished, this, &OvrPropTransGroup::scaleEditingFinished, Qt::QueuedConnection);
		connect(rotate.at(i), &QLineEdit::editingFinished, this, &OvrPropTransGroup::rotateEditingFinished, Qt::QueuedConnection);
	}

	connect(OvrActorOutlineTree::getInst(), SIGNAL(onUpdateTransformToProp(ovr_engine::OvrActor*)),
		this, SLOT(on_update_transform(ovr_engine::OvrActor*)), Qt::QueuedConnection);

}

void OvrPropTransGroup::onUpdateTransform()
{
	//更新位置信息

	OvrTransform ts = m_actor->GetTransform();

	QLineEdit* value_edit_position = nullptr;
	QLineEdit* value_edit_scale = nullptr;
	QLineEdit* value_edit_rotate = nullptr;
	QString value_text;
	
	float v[3] = { 0 };
	ts.quaternion.get_euler_angles(v[0], v[1], v[2]);

	for (int i = 0; i < 3; ++i)
	{
		value_edit_position = trans.at(i);
		value_edit_position->setText(QString::number(ts.position[i], 'f', 4));

		value_edit_scale = scale.at(i);
		value_edit_scale->setText(QString::number(ts.scale[i], 'f', 4));

		int n_angle = arc_to_angle(v[i]);
		value_edit_rotate = rotate.at(i);
		value_edit_rotate->setText(QString::number(n_angle));
	}

}

void OvrPropTransGroup::transEditingFinished()
{
	OvrTransform rts = m_actor->GetTransform();

	QLineEdit* value_edit = nullptr;
	QString value_text;
	for (int i = 0; i < 3; ++i)
	{
		value_edit = trans.at(i);
		value_text = value_edit->text();
		//无效改动
		if (value_text.isEmpty() || value_text != value_text.trimmed())
		{
			value_edit->setText(QString::number(rts.position[i], 'f', 4));
			//break;
		}
		else if (value_text != QString::number(rts.position[i], 'f', 4))
		{	
			rts.position[i] = value_text.toFloat();
			m_actor->SetTransform(rts);
			//break;
		}
	}
}

void OvrPropTransGroup::scaleEditingFinished()
{
	OvrTransform rts = m_actor->GetTransform();

	QLineEdit* value_edit = nullptr;
	QString value_text;
	for (int i = 0; i < 3; ++i)
	{
		value_edit = scale.at(i);
		value_text = value_edit->text();
		//无效改动
		if (value_text.isEmpty() || value_text != value_text.trimmed())
		{
			value_edit->setText(QString::number(rts.scale[i], 'f', 4));
			//break;
		}
		else if (value_text != QString::number(rts.scale[i], 'f', 4))
		{
			rts.scale[i] = value_text.toFloat();
			m_actor->SetTransform(rts);
			//break;
		}
	}
}

void OvrPropTransGroup::rotateEditingFinished()
{
	OvrTransform rts = m_actor->GetTransform();
	float v[3] = { 0 };
	rts.quaternion.get_euler_angles(v[0], v[1], v[2]);

	QLineEdit* value_edit = nullptr;
	QString value_text;
	for (int i = 0; i < 3; ++i)
	{
		int n_old_angle = arc_to_angle(v[i]);
		value_edit = rotate.at(i);
		value_text = value_edit->text();
		//无效改动
		if (value_text.isEmpty() || value_text != value_text.trimmed())
		{
			value_edit->setText(QString::number(n_old_angle));
			//value_edit->setText(QString::number(v[i], 'f', 4));
		}
		else if (value_text != QString::number(n_old_angle))
		{
			float f_angle = value_text.toFloat();

			// 限制范围
			if (0 == i) {
				f_angle = f_angle < -90 ? -90 : f_angle > 90 ? 90 : f_angle;
			}
			else if (1 == i) {
				f_angle = f_angle < -180 ? -180 : f_angle > 180 ? 180 : f_angle;
			}
			else if (2 == i) {
				f_angle = f_angle < -180 ? -180 : f_angle > 180 ? 180 : f_angle;
			}

			int n_angle = static_cast<int>(f_angle);
			QString strNewAngle = QString::number(n_angle);
			if (strNewAngle != value_text)
				value_edit->setText(strNewAngle);

			v[i] = angle_to_arc(f_angle);
			rts.quaternion = OvrQuaternion::make_euler_quat(v[0], v[1], v[2]);
			m_actor->SetTransform(rts);
		}
	}
}

void OvrPropTransGroup::on_update_transform(ovr_engine::OvrActor* a_actor)
{
	if (m_actor == a_actor)
		onUpdateTransform();

}

//////////////////////////////////////////////////
// class OvrPropMeshGroup, OvrPropMaterialGroup

void OvrPropMeshGroup::onAddingItems()
{
	//mesh, StaticMeshGroupActor, SKeletalMeshActor
	const char* path = "";
	/*if (m_actor->GetType() == ovr_engine::kObjectMesh)
	{
		auto mesh_actor = dynamic_cast<ovr_engine::OvrMeshActor*>(m_actor);
		path = mesh_actor->GetMesh()->GetName().GetStringConst();
	}*/

	QLineEdit* editor = addMeshMaterial("网格");
	editor->setText(QString::fromLocal8Bit(path));
	connect(apply_button, &QToolButton::clicked, this, &OvrPropMeshGroup::onApply, Qt::QueuedConnection);
}

void OvrPropMeshGroup::onApply(bool checked)
{
	QString select_name = OvrResourceListWidget::getInst()->GetSelectName();
	if (select_name.isEmpty() || QFileInfo(select_name).isDir() ||
		!select_name.endsWith(".mesh", Qt::CaseInsensitive))
		return;

	QString material_path = select_name;
	OvrProjectManager::getInst()->makeRelateDir(material_path);
	mesh_material_value->setText(material_path);

	ovr_core::OvrSceneContext* scene_context = ovr_project::OvrProject::GetIns()->GetCurrentScene();

	std::string s_name = select_name.toStdString();
	//scene_context->ChangeMesh(s_name.c_str());
}

void OvrPropMaterialGroup::onAddingItems()
{
	auto item = m_actor->GetComponent<ovr_engine::OvrMeshComponent>();

	size_t mat_count = item->GetMaterialCount();
	for (size_t i = 0; i < mat_count; i++)
	{
		ovr_engine::OvrMaterial* material = item->GetMaterial(i);
		const char* path = material->GetName().GetStringConst();
		QString mat_path = QString::fromLocal8Bit(path);
		MatInfoUi mat_ui = addMeshMaterial(mat_path, QString::fromLocal8Bit("材质"));
		mat_ui.mat_index = i;
		m_vctMatsEditUI.append(mat_ui);
	}
}

void OvrPropMaterialGroup::onApply(bool checked)
{
	QString select_name = OvrResourceListWidget::getInst()->GetSelectName();

	if (select_name.isEmpty() || QFileInfo(select_name).isDir() ||
		!select_name.endsWith(".mat", Qt::CaseInsensitive)) 
		return;

	QString material_path = select_name;
	OvrProjectManager::getInst()->makeRelateDir(material_path);

	QToolButton * btn = dynamic_cast<QToolButton*>(sender());
	for (auto & ui_info : m_vctMatsEditUI) 
	{
		if (ui_info.apply_button == btn)
		{
			ovr_project::OvrChangeMaterial* change_material = new ovr_project::OvrChangeMaterial(m_actor);
			change_material->SetNewMaterial(ui_info.mat_index, material_path.toLocal8Bit().constData());
			ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(change_material);

			ui_info.material_editor->setText(material_path);
			ui_info.material_editor->setToolTip(material_path);
			ui_info.material_path = material_path;
			break;
		}
	}
}

void OvrPropMaterialGroup::onLocate(bool checked)
{
	QToolButton * btn = dynamic_cast<QToolButton*>(sender());

	for (auto & ui_info : m_vctMatsEditUI) {
		if (ui_info.locate_button == btn) 
		{
			QString cur_path = ui_info.material_editor->text();
			notifyEvent("onLocateRes", mesh_material_value->text(), 0);
			break;
		}
	}
}

void OvrPropMaterialGroup::onLineEditDoubleClicked()
{
	QLineEdit * editor = dynamic_cast<QLineEdit *>(sender());

	QString mat_path;
	for (auto & ui_info : m_vctMatsEditUI) 
	{
		if (ui_info.material_editor == editor) 
		{
			//// 找到
			mat_path = ui_info.material_path;
			break;
		}
	} // for

	if (mat_path.isEmpty())
		return;

	// 打开材质窗口
	OvrMaterialEditor matDlg(mat_path, this);
	matDlg.exec();
}

OvrPropMaterialGroup::MatInfoUi OvrPropMaterialGroup::addMeshMaterial(const QString& a_mat_path, const QString & a_title)
{
	int col = 0;
	MatInfoUi mat_info_ui;
	mat_info_ui.material_path = a_mat_path;

	QLabel* label = new QLabel(a_title, this);
	pane_layout->addWidget(label, group_row, col++, Qt::AlignLeft | Qt::AlignVCenter);

	QLineEdit* value = new OvrLineEdit(this);
	pane_layout->addWidget(value, group_row, col++/*, Qt::AlignLeft | Qt::AlignVCenter*/);
	mat_info_ui.material_editor = value;
	value->setReadOnly(true);
	value->setText(a_mat_path);
	value->setToolTip(a_mat_path);
	connect(value, SIGNAL(double_clicked()), this, SLOT(onLineEditDoubleClicked()), Qt::QueuedConnection);

	QToolButton* apply_btn = nullptr;

	apply_btn = new QToolButton(this);
	apply_btn->setText(QString::fromLocal8Bit("应用"));
	pane_layout->addWidget(apply_btn, group_row, col++, Qt::AlignLeft | Qt::AlignVCenter);
	apply_btn->setStyleSheet(s_tool_btn_style_sheet);
	mat_info_ui.apply_button = apply_btn;

	QToolButton* locale_btn = new QToolButton(this);
	locale_btn->setText(QString::fromLocal8Bit("定位"));
	pane_layout->addWidget(locale_btn, group_row, col++, Qt::AlignLeft | Qt::AlignVCenter);
	locale_btn->setStyleSheet(s_tool_btn_style_sheet);
	mat_info_ui.locate_button = locale_btn;

	connect(locale_btn, SIGNAL(clicked(bool)), this, SLOT(onLocate(bool)), Qt::QueuedConnection);
	connect(apply_btn, SIGNAL(clicked(bool)), this, SLOT(onApply(bool)), Qt::QueuedConnection);

	++group_row;
	return mat_info_ui;
}

//////////////////////////////////////////////////
// class OvrPropVideoGroup, OvrPropAudioGroup

void OvrPropVideo3DGroup::onAddingItems()
{
	QLineEdit * editor = addMeshMaterial("文件");
	//auto video_actor = dynamic_cast<ovr_engine::OvrVideoScreenActor*>(actor);
	//const char * path = video_actor->GetDisplayName().GetStringConst();
	//editor->setText(QString::fromLocal8Bit(path));

	adaptation_combo= addCombo("适配");
	adaptation_combo->addItem(QString::fromLocal8Bit("全屏"));
	adaptation_combo->addItem(QString::fromLocal8Bit("等比例缩放"));
	video_adaptation = VideoAdaptationType::kVideoFull;

	format_combo = addCombo("存储格式");
	format_combo->addItem(QString::fromLocal8Bit("左右"));
	format_combo->addItem(QString::fromLocal8Bit("上下"));
	//format_combo->addItem(QString::fromLocal8Bit("右左"));
	//format_combo->addItem(QString::fromLocal8Bit("下上"));
	
	QObject::connect(adaptation_combo, SIGNAL(currentIndexChanged(int)), this, SLOT(adaptationChanged(int)));
	QObject::connect(format_combo, SIGNAL(currentIndexChanged(int)), this, SLOT(formatChanged(int)));

	loop_check = addCheck("是否循环");
	connect(loop_check, &QCheckBox::stateChanged, this, &OvrPropVideo3DGroup::loopCheckChanged, Qt::QueuedConnection);

	video_format = VideoFormatType::kVideoLeftRight;
	SetVideoFormat(video_format, video_adaptation);
	//other
}

void OvrPropVideo3DGroup::loopCheckChanged(int a_state)
{
	//
}

void OvrPropVideo3DGroup::adaptationChanged(int a_index)
{
	/*if (m_actor->GetType() != ovr_engine::kObjectVideoScreen)
	{
		return;
	}
	ovr_engine::OvrVideoScreenActor* video_actor = (ovr_engine::OvrVideoScreenActor*)m_actor;
	if (video_actor->GetVideoType() != ovr_engine::OvrVideoScreenActor::Video3D)
	{
		return;
	}
		
	if (a_index == 0)
	{
		//全屏
		SetVideoFormat(video_format, VideoAdaptationType::kVideoFull);
	}
	else
	{
		//等比
		SetVideoFormat(video_format, VideoAdaptationType::kVideoAdaptation);
	}*/
}

void OvrPropVideo3DGroup::formatChanged(int a_index)
{
	/*if (m_actor->GetType() != ovr_engine::kObjectVideoScreen)
	{
		return;
	}
	ovr_engine::OvrVideoScreenActor* video_actor = (ovr_engine::OvrVideoScreenActor*)m_actor;
	if (video_actor->GetVideoType() != ovr_engine::OvrVideoScreenActor::Video3D)
	{
		return;
	}

	if (a_index == 0)
	{
		//左右
		SetVideoFormat(VideoFormatType::kVideoLeftRight, video_adaptation);
	}
	else if (a_index == 1)
	{
		//上下
		SetVideoFormat(VideoFormatType::kVideoTopBottom, video_adaptation);
	}
	else if (a_index == 2)
	{
		//右左
		SetVideoFormat(VideoFormatType::kVideoRightLeft, video_adaptation);
	}
	else if (a_index == 3)
	{
		//下上
		SetVideoFormat(VideoFormatType::kVideoBottomTop, video_adaptation);
	}*/
}

void OvrPropVideo3DGroup::SetVideoFormat(VideoFormatType a_format, VideoAdaptationType a_adaptation)
{
	/*ovr_engine::OvrVideoScreenActor * video_3d_actor = (ovr_engine::OvrVideoScreenActor *)m_actor;

	if (a_format == VideoFormatType::kVideoLeftRight)
	{
		if (a_adaptation == VideoAdaptationType::kVideoAdaptation)
		{
			video_3d_actor->SetMovieType(ovr_engine::e_movie_type_left_right_3d);
		}
		else
		{
			video_3d_actor->SetMovieType(ovr_engine::e_movie_type_left_right_3d_full);
		}
	}
	else if (a_format == VideoFormatType::kVideoTopBottom)
	{
		if (a_adaptation == VideoAdaptationType::kVideoAdaptation)
		{
			video_3d_actor->SetMovieType(ovr_engine::e_movie_type_top_bottom_3d);
		}
		else
		{
			video_3d_actor->SetMovieType(ovr_engine::e_movie_type_top_bottom_3d_full);
		}
	}
	else if (a_format == VideoFormatType::kVideoRightLeft)
	{
		if (a_adaptation == VideoAdaptationType::kVideoAdaptation)
		{
			video_3d_actor->SetMovieType(ovr_engine::e_movie_type_right_left_3d);
		}
		else
		{
			video_3d_actor->SetMovieType(ovr_engine::e_movie_type_right_left_3d_full);
		}
	}
	else if (a_format == VideoFormatType::kVideoTopBottom)
	{
		if (a_adaptation == VideoAdaptationType::kVideoAdaptation)
		{
			video_3d_actor->SetMovieType(ovr_engine::e_movie_type_bottom_top_3d);
		}
		else
		{
			video_3d_actor->SetMovieType(ovr_engine::e_movie_type_bottom_top_3d_full);
		}
	}*/
}

void OvrPropVideo180Group::onAddingItems()
{
	//addMeshMaterial("文件");
	QLineEdit * editor = addMeshMaterial("文件");
	//auto video_actor = dynamic_cast<ovr_engine::OvrVideoScreenActor*>(actor);
	//const char * path = video_actor->GetDisplayName().GetStringConst();
	//editor->setText(QString::fromLocal8Bit(path));

	format_combo = addCombo("存储格式");
	format_combo->addItem(QString::fromLocal8Bit("非3D"));
	format_combo->addItem(QString::fromLocal8Bit("左右"));
	format_combo->addItem(QString::fromLocal8Bit("上下"));
	//format_combo->addItem(QString::fromLocal8Bit("右左"));
	//format_combo->addItem(QString::fromLocal8Bit("下上"));

	QObject::connect(format_combo, SIGNAL(currentIndexChanged(int)), this, SLOT(formatChanged(int)));

	//adaptation_combo = addCombo("适配");
	//adaptation_combo->addItem(QString::fromLocal8Bit("全屏"));
	//adaptation_combo->addItem(QString::fromLocal8Bit("等比例缩放"));
	//QObject::connect(adaptation_combo, SIGNAL(currentIndexChanged(int)), this, SLOT(adaptationChanged(int)));

	loop_check = addCheck("是否循环");
	connect(loop_check, &QCheckBox::stateChanged, this, &OvrPropVideo180Group::loopCheckChanged, Qt::QueuedConnection);

	//ovr_engine::OvrVideoScreenActor * video_180_actor = (ovr_engine::OvrVideoScreenActor *)m_actor;
	//video_180_actor->SetMovieType(ovr_engine::e_movie_type_pano_180);
	//other
}

void OvrPropVideo180Group::loopCheckChanged(int a_state)
{

}

void OvrPropVideo180Group::formatChanged(int a_index)
{
	/*if (m_actor->GetType() != ovr_engine::kObjectVideoScreen)
	{
		return;
	}
	ovr_engine::OvrVideoScreenActor* video_actor = (ovr_engine::OvrVideoScreenActor*)m_actor;
	if (video_actor->GetVideoType() != ovr_engine::OvrVideoScreenActor::Video180)
	{
		return;
	}

	if (a_index == 0)
	{
		//非3D
		video_actor->SetMovieType(ovr_engine::e_movie_type_pano_180);
	}
	else if (a_index == 1)
	{
		//左右
		video_actor->SetMovieType(ovr_engine::e_movie_type_pano_180_LR);
	}
	else if (a_index == 2)
	{
		//上下
		video_actor->SetMovieType(ovr_engine::e_movie_type_pano_180_TD);
	}
	else if (a_index == 3)
	{
		//右左
		video_actor->SetMovieType(ovr_engine::e_movie_type_pano_180_RL);
	}
	else if (a_index == 4)
	{
		//下上
		video_actor->SetMovieType(ovr_engine::e_movie_type_pano_180_DT);
	}*/
}

void OvrPropVideo2DGroup::onAddingItems()
{
	//addMeshMaterial("文件");
	QLineEdit * editor = addMeshMaterial("文件");
	//auto video_actor = dynamic_cast<ovr_engine::OvrVideoScreenActor*>(actor);
	//const char * path = video_actor->GetDisplayName().GetStringConst();
	//editor->setText(QString::fromLocal8Bit(path));

	adaptation_combo = addCombo("适配");
	adaptation_combo->addItem(QString::fromLocal8Bit("全屏"));
	adaptation_combo->addItem(QString::fromLocal8Bit("等比例缩放"));
	QObject::connect(adaptation_combo, SIGNAL(currentIndexChanged(int)), this, SLOT(adaptationChanged(int)));

	loop_check = addCheck("是否循环");
	connect(loop_check, &QCheckBox::stateChanged, this, &OvrPropVideo2DGroup::loopCheckChanged, Qt::QueuedConnection);

	//ovr_engine::OvrVideoScreenActor * video_2d_actor = (ovr_engine::OvrVideoScreenActor *)m_actor;
	//video_2d_actor->SetMovieType(ovr_engine::e_movie_type_common_2d_full);
	//other
}

void OvrPropVideo2DGroup::loopCheckChanged(int a_state)
{

}

void OvrPropVideo2DGroup::adaptationChanged(int a_index)
{
	/*if (m_actor->GetType() != ovr_engine::kObjectVideoScreen)
	{
		return;
	}
	ovr_engine::OvrVideoScreenActor* video_actor = (ovr_engine::OvrVideoScreenActor*)m_actor;
	if (video_actor->GetVideoType() != ovr_engine::OvrVideoScreenActor::Video2D)
	{
		return;
	}

	if (a_index == 0)
	{
		//全屏
		video_actor->SetMovieType(ovr_engine::e_movie_type_common_2d_full);
	}
	else
	{
		//等比
		video_actor->SetMovieType(ovr_engine::e_movie_type_common_2d);
	}*/
}

void OvrPropVideo360Group::onAddingItems()
{
	//addMeshMaterial("文件");
	QLineEdit * editor = addMeshMaterial("文件");
	//auto video_actor = dynamic_cast<ovr_engine::OvrVideoScreenActor*>(actor);
	//const char * path = video_actor->GetDisplayName().GetStringConst();
	//editor->setText(QString::fromLocal8Bit(path));

	format_combo = addCombo("存储格式");
	format_combo->addItem(QString::fromLocal8Bit("非3D"));
	format_combo->addItem(QString::fromLocal8Bit("左右"));
	format_combo->addItem(QString::fromLocal8Bit("上下"));
	//format_combo->addItem(QString::fromLocal8Bit("右左"));
	//format_combo->addItem(QString::fromLocal8Bit("下上"));

	QObject::connect(format_combo, SIGNAL(currentIndexChanged(int)), this, SLOT(formatChanged(int)));

	loop_check = addCheck("是否循环");
	connect(loop_check, &QCheckBox::stateChanged, this, &OvrPropVideo360Group::loopCheckChanged, Qt::QueuedConnection);

	//ovr_engine::OvrVideoScreenActor * video_360_actor = (ovr_engine::OvrVideoScreenActor *)m_actor;
	//video_360_actor->SetMovieType(ovr_engine::e_movie_type_pano_360);
	//other
}

void OvrPropVideo360Group::loopCheckChanged(int a_state)
{

}

void OvrPropVideo360Group::formatChanged(int a_index)
{
	/*if (m_actor->GetType() != ovr_engine::kObjectVideoScreen)
	{
		return;
	}
	ovr_engine::OvrVideoScreenActor* video_actor = (ovr_engine::OvrVideoScreenActor*)m_actor;
	if (video_actor->GetVideoType() != ovr_engine::OvrVideoScreenActor::Video360)
	{
		return;
	}

	if (a_index == 0)
	{
		//非3D
		video_actor->SetMovieType(ovr_engine::e_movie_type_pano_360);
	}
	else if (a_index == 1)
	{
		//左右
		video_actor->SetMovieType(ovr_engine::e_movie_type_pano_360_LR);
	}
	else if (a_index == 2)
	{
		//上下
		video_actor->SetMovieType(ovr_engine::e_movie_type_pano_360_TD);
	}
	else if (a_index == 3)
	{
		//右左
		video_actor->SetMovieType(ovr_engine::e_movie_type_pano_360_RL);
	}
	else if (a_index == 4)
	{
		//下上
		video_actor->SetMovieType(ovr_engine::e_movie_type_pano_360_DT);
	}*/
}

void OvrPropVideo360BoxGroup::onAddingItems()
{
	//addMeshMaterial("文件");
	QLineEdit * editor = addMeshMaterial("文件");
	//auto video_actor = dynamic_cast<ovr_engine::OvrVideoScreenActor*>(actor);
	//const char * path = video_actor->GetDisplayName().GetStringConst();
	//editor->setText(QString::fromLocal8Bit(path));

	format_combo = addCombo("存储格式");
	format_combo->addItem(QString::fromLocal8Bit("非3D"));
	format_combo->addItem(QString::fromLocal8Bit("左右"));
	format_combo->addItem(QString::fromLocal8Bit("上下"));
	//format_combo->addItem(QString::fromLocal8Bit("右左"));
	//format_combo->addItem(QString::fromLocal8Bit("下上"));

	QObject::connect(format_combo, SIGNAL(currentIndexChanged(int)), this, SLOT(formatChanged(int)));

	loop_check = addCheck("是否循环");
	connect(loop_check, &QCheckBox::stateChanged, this, &OvrPropVideo360BoxGroup::loopCheckChanged, Qt::QueuedConnection);

	//ovr_engine::OvrVideoScreenActor * video_360_actor = (ovr_engine::OvrVideoScreenActor *)m_actor;
	//video_360_actor->SetMovieType(ovr_engine::e_movie_type_pano_cb);
	//other
}

void OvrPropVideo360BoxGroup::loopCheckChanged(int a_state)
{

}

void OvrPropVideo360BoxGroup::formatChanged(int a_index)
{
	/*if (m_actor->GetType() != ovr_engine::kObjectVideoScreen)
	{
		return;
	}
	ovr_engine::OvrVideoScreenActor* video_actor = (ovr_engine::OvrVideoScreenActor*)m_actor;
	if (video_actor->GetVideoType() != ovr_engine::OvrVideoScreenActor::Video360Box)
	{
		return;
	}

	if (a_index == 0)
	{
		//非3D
		video_actor->SetMovieType(ovr_engine::e_movie_type_pano_cb);
	}
	else if (a_index == 1)
	{
		//左右
		video_actor->SetMovieType(ovr_engine::e_movie_type_pano_cb_LR);
	}
	else if (a_index == 2)
	{
		//上下
		video_actor->SetMovieType(ovr_engine::e_movie_type_pano_cb_TD);
	}
	else if (a_index == 3)
	{
		//右左
		video_actor->SetMovieType(ovr_engine::e_movie_type_pano_cb_RL);
	}
	else if (a_index == 4)
	{
		//下上
		video_actor->SetMovieType(ovr_engine::e_movie_type_pano_cb_DT);
	}*/
}

void OvrProp3DAudioGroup::onAddingItems()
{
	//addMeshMaterial("文件");
	QLineEdit * editor = addMeshMaterial("文件");
	//auto video_actor = dynamic_cast<ovr_engine::OvrVideoScreenActor*>(actor);
	//const char * path = video_actor->GetDisplayName().GetStringConst();
	//editor->setText(QString::fromLocal8Bit(path));

	//adaptation_combo = addCombo("适配");
	//adaptation_combo->addItem(QString::fromLocal8Bit("全屏"));
	//adaptation_combo->addItem(QString::fromLocal8Bit("等比例缩放"));

	loop_check = addCheck("是否循环");
	connect(loop_check, &QCheckBox::stateChanged, this, &OvrProp3DAudioGroup::loopCheckChanged, Qt::QueuedConnection);

	//other
}

void OvrProp3DAudioGroup::loopCheckChanged(int a_state)
{

}

void OvrLightGroup::colorSelectPop()
{
	/*auto light_actor = dynamic_cast<ovr_engine::OvrLightActor*>(m_actor);
	ovr_engine::OvrLight &light = light_actor->GetLight();

	OvrVector4 rgba = light.GetDiffuseColor();
	QColor oldColor(rgba._x * 255, rgba._y * 255, rgba._z * 255, rgba._w * 255);
	QColor curColor = getSelectedColor(oldColor);
	if (curColor == oldColor) {
		// 相同时是被取消的，需要还原
		OvrVector4 newColor;
		newColor._x = curColor.red() / 255.;
		newColor._y = curColor.green() / 255.;
		newColor._z = curColor.blue() / 255.;
		newColor._w = curColor.alpha() / 255.;
		light.SetDiffuseColor(newColor);

		label_light_color->SetCurrentColor(curColor);
	}*/
}

void OvrLightGroup::LightAttiEditingFinished()
{
	/*auto light_actor = dynamic_cast<ovr_engine::OvrLightActor*>(m_actor);
	ovr_engine::OvrLight &light = light_actor->GetLight();

	auto get_edit_value = [](QLineEdit *edit)->float {
		QString strValue = edit->text();
		return strValue.toFloat();
	};

	QObject * cur_sender = dynamic_cast<QObject *>(sender());
	if (cur_sender == edit_light_intensity) {
		QLineEdit * cur_edit = dynamic_cast<QLineEdit *>(cur_sender);
		float f_trans = get_edit_value(cur_edit);
		light.SetIntensity(f_trans);
	}
	else if (cur_sender == edit_light_scope) {
		QLineEdit * cur_edit = dynamic_cast<QLineEdit *>(cur_sender);
		float f_trans = get_edit_value(cur_edit);
		light.SetRange(f_trans);
	}
	else if (cur_sender == edit_light_angle) {
		QLineEdit * cur_edit = dynamic_cast<QLineEdit *>(cur_sender);
		float f_trans = get_edit_value(cur_edit);
		light.SetFalloff(f_trans);
	}
	else if (cur_sender == edit_light_fall) {
		QLineEdit * cur_edit = dynamic_cast<QLineEdit *>(cur_sender);
		float f_trans = get_edit_value(cur_edit);
		light.SetExp(f_trans);
	}
	else if (cur_sender == chk_light_shade) {
		QCheckBox * cur_edit = dynamic_cast<QCheckBox *>(cur_sender);
		bool is_shadow = cur_edit->isChecked();
		light.SetCastShadow(is_shadow);
	}
	else {
		assert(false);
	}*/
}

void OvrLightGroup::onColorChanged(const QColor& color)
{
	/*auto light_actor = dynamic_cast<ovr_engine::OvrLightActor*>(m_actor);
	ovr_engine::OvrLight &light = light_actor->GetLight();

	OvrVector4 newColor;
	newColor._x = color.red() / 255.;
	newColor._y = color.green() / 255.;
	newColor._z = color.blue() / 255.;
	newColor._w = color.alpha() / 255.;
	light.SetDiffuseColor(newColor);

	label_light_color->SetCurrentColor(color);*/
}


void OvrLightGroup::onAddingItems()
{
	/*auto light_actor = dynamic_cast<ovr_engine::OvrLightActor*>(m_actor);
	ovr_engine::OvrLight &light = light_actor->GetLight();

	auto add_light_color_ui = [this](OvrColorLabel *& label, const OvrVector4& value, const QString & title) {
		label = addColorSelLabel(title);
		label->SetCurrentColor(value);
		connect(label, SIGNAL(double_clicked()), this, SLOT(colorSelectPop()), Qt::QueuedConnection);
	};

	auto add_light_edit_ui = [this](QLineEdit *& edit, double value, const QString & title) {
		edit = addDigitesLineEdit(title);
		edit->setText(QString::number(value, 'f', 4));
		connect(edit, SIGNAL(editingFinished()), this, SLOT(LightAttiEditingFinished()), Qt::QueuedConnection);
	};

	auto add_shade_ui = [this](QCheckBox *& edit, bool value, const char * title) {
		chk_light_shade = addCheck(title);
		chk_light_shade->setChecked(value);
		connect(chk_light_shade, SIGNAL(stateChanged()), this, SLOT(LightAttiEditingFinished()), Qt::QueuedConnection);
	};

	add_light_color_ui(label_light_color, light.GetDiffuseColor(), QString::fromLocal8Bit("颜色"));
	add_light_edit_ui(edit_light_intensity, light.GetIntensity(), QString::fromLocal8Bit("强度"));

	ovr_engine::OvrLightType light_type = light_actor->GetLightType();
	switch (light_type)
	{
	case ovr_engine::ePointLight:
		add_light_edit_ui(edit_light_scope, light.GetRange(), QString::fromLocal8Bit("范围"));
		break;
	case ovr_engine::eDirectionLight:
		break;
	case ovr_engine::eSpotLight:
		add_light_edit_ui(edit_light_scope, light.GetRange(), QString::fromLocal8Bit("范围"));
		add_light_edit_ui(edit_light_angle, light.GetFalloff(), QString::fromLocal8Bit("角度"));
		add_light_edit_ui(edit_light_fall, light.GetExp(), QString::fromLocal8Bit("衰减速度"));
		break;
	case ovr_engine::eLightNum:
		break;
	default:
		break;
	}
	
	add_shade_ui(chk_light_shade, light.IfCastShadow(), "启用阴影");*/
}


void OvrTextGroup::onAddingItems()
{
	/*auto text_actor = dynamic_cast<ovr_engine::OvrTextActor*>(m_actor);

	edit_text_content = addTextEdit(QString::fromLocal8Bit("文本"));
	const OvrString ovr_text = text_actor->GetText();
	QString strText = QString::fromUtf8(ovr_text.GetStringConst());
	edit_text_content->setPlainText(strText);
	connect(edit_text_content, SIGNAL(textChanged()), this, SLOT(textInfoChanged()), Qt::QueuedConnection);

	combo_font_select = addTextFontSel(QString::fromLocal8Bit("字体"));
	connect(combo_font_select, SIGNAL(currentFontChanged(QFont)), this, SLOT(fontChanged(const QFont&)), Qt::QueuedConnection);

	edit_text_scale = addTextScale(QString::fromLocal8Bit("缩放"));
	double text_scale = text_actor->GetTextScale();
	edit_text_scale->setText(QString::number(text_scale, 'f', 4));
	connect(edit_text_scale, SIGNAL(editingFinished()), this, SLOT(scaleChanged()), Qt::QueuedConnection);

	label_color_sel = addColorSelLabel(QString::fromLocal8Bit("颜色"));
	OvrVector4 rgba = text_actor->GetTextColor();
	label_color_sel->SetCurrentColor(rgba);
	connect(label_color_sel, SIGNAL(double_clicked()), this, SLOT(colorSelectPop()), Qt::QueuedConnection);*/
}

void OvrTextGroup::textInfoChanged()
{
	/*auto text_actor = dynamic_cast<ovr_engine::OvrTextActor*>(m_actor);
	QString strText = edit_text_content->toPlainText();
	OvrString ovr_text = strText.toUtf8();
	text_actor->SetText(ovr_text);*/
}

void OvrTextGroup::fontChanged(const QFont & a_font)
{
	//auto text_actor = dynamic_cast<ovr_engine::OvrTextActor*>(m_actor);
	//text_actor->
}

void OvrTextGroup::scaleChanged()
{
	/*auto text_actor = dynamic_cast<ovr_engine::OvrTextActor*>(m_actor);

	QString strTextScale = edit_text_scale->text();
	float text_scale = strTextScale.toFloat();
	text_actor->SetTextScale(text_scale);*/
}

void OvrTextGroup::colorSelectPop()
{
	/*auto text_actor = dynamic_cast<ovr_engine::OvrTextActor*>(m_actor);

	OvrVector4 rgba = text_actor->GetTextColor();
	QColor oldColor(rgba._x * 255, rgba._y * 255, rgba._z * 255, rgba._w * 255);
	QColor curColor = getSelectedColor(oldColor);

	//QColor curColor = QColorDialog::getColor(oldColor,this);
	if (curColor == oldColor) {
		// 没有更新有颜色要还原
		OvrVector4 newColor;
		newColor._x = curColor.red() / 255.;
		newColor._y = curColor.green() / 255.;
		newColor._z = curColor.blue() / 255.;
		newColor._w = curColor.alpha() / 255.;
		text_actor->SetTextColor(newColor);

		label_color_sel->SetCurrentColor(curColor);
	}*/
}

void OvrTextGroup::onColorChanged(const QColor& color)
{
	/*auto text_actor = dynamic_cast<ovr_engine::OvrTextActor*>(m_actor);
	OvrVector4 newColor;
	newColor._x = color.red() / 255.;
	newColor._y = color.green() / 255.;
	newColor._z = color.blue() / 255.;
	newColor._w = color.alpha() / 255.;
	text_actor->SetTextColor(newColor);

	label_color_sel->SetCurrentColor(color);*/
}


QPlainTextEdit* OvrTextGroup::addTextEdit(const QString & a_title)
{
	QLabel* label = new QLabel(a_title, this);
	pane_layout->addWidget(label, group_row, 0, Qt::AlignLeft | Qt::AlignTop);

	QPlainTextEdit* value = new QPlainTextEdit(this);
	value->setStyleSheet(s_text_edit_style_sheet);
	pane_layout->addWidget(value, group_row, 1, 1, 1);

	++group_row;

	return value;
}

QFontComboBox * OvrTextGroup::addTextFontSel(const QString & a_title)
{
	QLabel* label = new QLabel(a_title, this);
	pane_layout->addWidget(label, group_row, 0, Qt::AlignLeft | Qt::AlignVCenter);

	QFontComboBox* value = new QFontComboBox(this);
	pane_layout->addWidget(value, group_row, 1, 1, 1);

	++group_row;

	return value;
}

QLineEdit * OvrTextGroup::addTextScale(const QString & a_title)
{
	QLabel* label = new QLabel(a_title);
	pane_layout->addWidget(label, group_row, 0, Qt::AlignLeft | Qt::AlignVCenter);

	QLineEdit* value = new QLineEdit(this);
	value->setInputMethodHints(Qt::ImhDigitsOnly);
	value->setMaxLength(6);
	QRegExp regExpPort = QRegExp(cs_reg_exp_filter);
	QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExpPort);
	value->setValidator(portRegExpValidator);

	pane_layout->addWidget(value, group_row, 1, 1, 1);

	++group_row;

	return value;
}

void OvrTextMakeupGroup::onAddingItems()
{
	/*auto text_actor = dynamic_cast<ovr_engine::OvrTextActor*>(m_actor);

	addMarginUI(QString::fromLocal8Bit("对齐"), horiz_margin, horiz_margin_btns, vert_margin, vert_margin_btns);

	auto h_margin = text_actor->GetAlignHorizontalMethod();
	auto v_margin = text_actor->GetAlignVerticleMethod();

	switch (h_margin)
	{
	case ovr_engine::eTextAlignLeft: horiz_margin_btns[0]->setChecked(true); break;
	case ovr_engine::eTextAlignMiddle: horiz_margin_btns[1]->setChecked(true); break;
	case ovr_engine::eTextAlignRight: horiz_margin_btns[2]->setChecked(true); break;
	default: horiz_margin_btns[1]->setChecked(true); break;
	}

	switch (v_margin)
	{
	case ovr_engine::eTextAlignUp: vert_margin_btns[0]->setChecked(true); break;
	case ovr_engine::eTextAlignMiddle: vert_margin_btns[1]->setChecked(true); break;
	case ovr_engine::eTextAlignDown: vert_margin_btns[2]->setChecked(true); break;
	default: vert_margin_btns[1]->setChecked(true); break;
	}

	connect(horiz_margin_btns[0], SIGNAL(clicked()), this, SLOT(textMarginChanged()), Qt::QueuedConnection);
	connect(horiz_margin_btns[1], SIGNAL(clicked()), this, SLOT(textMarginChanged()), Qt::QueuedConnection);
	connect(horiz_margin_btns[2], SIGNAL(clicked()), this, SLOT(textMarginChanged()), Qt::QueuedConnection);
	connect(vert_margin_btns[0], SIGNAL(clicked()), this, SLOT(textMarginChanged()), Qt::QueuedConnection);
	connect(vert_margin_btns[1], SIGNAL(clicked()), this, SLOT(textMarginChanged()), Qt::QueuedConnection);
	connect(vert_margin_btns[2], SIGNAL(clicked()), this, SLOT(textMarginChanged()), Qt::QueuedConnection);

	addAlignSpace(QString::fromLocal8Bit("边缘空白"), edit_align_space_x, edit_align_space_y);
	float margin_x, margin_y;
	text_actor->GetTextMargin(margin_x, margin_y);
	edit_align_space_x->setText(QString::number(margin_x, 'f', 4));
	edit_align_space_y->setText(QString::number(margin_y, 'f', 4));
	connect(edit_align_space_x, SIGNAL(editingFinished()), this, SLOT(fontAlignXYChanged()), Qt::QueuedConnection);
	connect(edit_align_space_y, SIGNAL(editingFinished()), this, SLOT(fontAlignXYChanged()), Qt::QueuedConnection);

	edit_font_spacing = addDigitesLineEdit(QString::fromLocal8Bit("字间距"));
	float font_spacing = text_actor->GetWarpWidth();
	edit_font_spacing->setText(QString::number(font_spacing, 'f', 4));
	connect(edit_font_spacing, SIGNAL(editingFinished()), this, SLOT(fontSpacingChanged()), Qt::QueuedConnection);

	edit_line_spacing = addDigitesLineEdit(QString::fromLocal8Bit("行间距"));
	float line_spacing = text_actor->GetLineWarp();
	edit_font_spacing->setText(QString::number(line_spacing, 'f', 4));
	connect(edit_line_spacing, SIGNAL(editingFinished()), this, SLOT(lineSpacingChanged()), Qt::QueuedConnection);
	*/
}


void OvrTextMakeupGroup::textMarginChanged()
{
	/*auto text_actor = dynamic_cast<ovr_engine::OvrTextActor*>(m_actor);

	int n_horiz_id = horiz_margin->checkedId();
	int n_vert_id = vert_margin->checkedId();

	ovr_engine::OvrAlignMethod h_align;
	switch (n_horiz_id)
	{
	case 0: h_align = ovr_engine::eTextAlignLeft; break;
	case 1: h_align = ovr_engine::eTextAlignMiddle; break;
	case 2: h_align = ovr_engine::eTextAlignRight; break;
	default:h_align = ovr_engine::eTextAlignMiddle; break;
	}

	ovr_engine::OvrAlignMethod v_align;
	switch (n_vert_id)
	{
	case 0: v_align = ovr_engine::eTextAlignUp; break;
	case 1: v_align = ovr_engine::eTextAlignMiddle; break;
	case 2: v_align = ovr_engine::eTextAlignDown; break;
	default:v_align = ovr_engine::eTextAlignMiddle; break;
	}

	text_actor->SetAlignMethod(h_align, v_align);*/
}

void OvrTextMakeupGroup::fontAlignXYChanged()
{
	/*auto text_actor = dynamic_cast<ovr_engine::OvrTextActor*>(m_actor);

	QString strAlignX = edit_align_space_x->text();
	QString strAlignY = edit_align_space_y->text();

	float f_align_x = strAlignX.toFloat();
	float f_align_y = strAlignY.toFloat();

	text_actor->SetTextMargin(f_align_x, f_align_y);*/
}

void OvrTextMakeupGroup::fontSpacingChanged()
{
	/*auto text_actor = dynamic_cast<ovr_engine::OvrTextActor*>(m_actor);

	QString strFontSpacing = edit_font_spacing->text();

	float f_spacing = strFontSpacing.toFloat();

	text_actor->SetWarpWidth(f_spacing);*/
}

void OvrTextMakeupGroup::lineSpacingChanged()
{
	/*auto text_actor = dynamic_cast<ovr_engine::OvrTextActor*>(m_actor);

	QString strLineSpacing = edit_line_spacing->text();

	float f_spacing = strLineSpacing.toFloat();

	text_actor->SetLineWarp(f_spacing);*/
}

void OvrTextMakeupGroup::addMarginUI(const QString & a_title,
	QButtonGroup *&horiz_margin, std::array<QRadioButton*, 3>& horiz_btns,
	QButtonGroup *&vert_margin, std::array<QRadioButton*, 3> & vert_btns)
{
	QLabel* label = new QLabel(a_title, this);
	pane_layout->addWidget(label, group_row, 0, 1, 1, Qt::AlignLeft | Qt::AlignVCenter);

	QFrame * frame_h = new QFrame(this);
	frame_h->setStyleSheet(
		"QFrame{background-color:rgb(60, 60, 60); border-radius:2px}"
		//"QRadioButton{background:rgb(34, 34, 34);}"
		//"QToolTip{color:rgb(48,48,48); background-color:rgb(204,204,204);}"
	);
	QHBoxLayout * frame_layout_h = new QHBoxLayout(frame_h);
	frame_layout_h->setSpacing(4);
	frame_layout_h->setContentsMargins(6, 2, 6, 2);

	horiz_btns[0] = new QRadioButton(frame_h);
	horiz_btns[1] = new QRadioButton(frame_h);
	horiz_btns[2] = new QRadioButton(frame_h);

	auto generate_radio_btn_style_sheet = [](
		const char * unchecked_normal, const char * unchecked_hover, const char * unchecked_pressed,
		const char * checked_normal, const char * checked_hover, const char * checked_pressed
		)->QString {
		QString strStyleSheet;
		QTextStream qtsTemp(&strStyleSheet);
		qtsTemp <<"QRadioButton{spacing:0px;}"
			<< "QRadioButton::indicator {width: 20px; height: 20px;}"
			<< "QRadioButton{background-color:rgb(60, 60, 60);}"
			<< "QRadioButton::indicator::unchecked {image:url(" << getImagePath(unchecked_normal) << ");}"
			<< "QRadioButton::indicator::unchecked:hover {image:url(" << getImagePath(unchecked_hover) << ");}"
			<< "QRadioButton::indicator::unchecked:pressed {image:url(" << getImagePath(unchecked_pressed) << ");}"
			<< "QRadioButton::indicator::checked {image:url(" << getImagePath(checked_normal) << ");}"
			<< "QRadioButton::indicator::checked:hover {image:url(" << getImagePath(checked_hover) << ");}"
			<< "QRadioButton::indicator::checked:pressed {image:url(" << getImagePath(checked_pressed) << ");}"
			;
		return strStyleSheet;
	};

	QString strStyleSheetHorizLeft = generate_radio_btn_style_sheet(
		"align_btn/icon_left_nor.png",
		"align_btn/icon_left_highlight.png",
		"align_btn/icon_left_sel.png",
		"align_btn/icon_left_sel.png",
		"align_btn/icon_left_sel.png",
		"align_btn/icon_left_sel.png");
	horiz_btns[0]->setStyleSheet(strStyleSheetHorizLeft);

	QString strStyleSheetHorizCenter = generate_radio_btn_style_sheet(
		"align_btn/icon_horizon_nor.png",
		"align_btn/icon_horizon_highlight.png",
		"align_btn/icon_horizon_sel.png",
		"align_btn/icon_horizon_sel.png",
		"align_btn/icon_horizon_sel.png",
		"align_btn/icon_horizon_sel.png");
	horiz_btns[1]->setStyleSheet(strStyleSheetHorizCenter);

	QString strStyleSheetHorizRight = generate_radio_btn_style_sheet(
		"align_btn/icon_right_nor.png",
		"align_btn/icon_right_highlight.png",
		"align_btn/icon_right_sel.png",
		"align_btn/icon_right_sel.png",
		"align_btn/icon_right_sel.png",
		"align_btn/icon_right_sel.png");
	horiz_btns[2]->setStyleSheet(strStyleSheetHorizRight);

	horiz_btns[0]->setToolTip(QString::fromLocal8Bit("左对齐"));
	horiz_btns[1]->setToolTip(QString::fromLocal8Bit("水平居中"));
	horiz_btns[2]->setToolTip(QString::fromLocal8Bit("右对齐"));

	frame_layout_h->addWidget(horiz_btns[0]);
	frame_layout_h->addWidget(horiz_btns[1]);
	frame_layout_h->addWidget(horiz_btns[2]);

	horiz_margin = new QButtonGroup(frame_h);
	horiz_margin->addButton(horiz_btns[0], 0);
	horiz_margin->addButton(horiz_btns[1], 1);
	horiz_margin->addButton(horiz_btns[2], 2);

	QFrame *frame_v = new QFrame(this);
	frame_v->setStyleSheet(
		"QFrame{background-color:rgb(60, 60, 60); border-radius:2px}"
		//"QRadioButton{background-color:rgb(60, 60, 60);}"
		//"QToolTip{color:rgb(48,48,48); background-color:rgb(204,204,204);}"
	);
	QHBoxLayout * frame_layout_v = new QHBoxLayout(frame_v);
	frame_layout_v->setSpacing(4);
	frame_layout_v->setContentsMargins(6, 2, 6, 2);

	vert_btns[0] = new QRadioButton(frame_v);
	vert_btns[1] = new QRadioButton(frame_v);
	vert_btns[2] = new QRadioButton(frame_v);

	QString strStyleSheetVertTop = generate_radio_btn_style_sheet(
		"align_btn/icon_top_nor.png",
		"align_btn/icon_top_highlight.png",
		"align_btn/icon_top_sel.png",
		"align_btn/icon_top_sel.png",
		"align_btn/icon_top_sel.png",
		"align_btn/icon_top_sel.png");
	vert_btns[0]->setStyleSheet(strStyleSheetVertTop);

	QString strStyleSheetVertCetner = generate_radio_btn_style_sheet(
		"align_btn/icon_vertical_nor.png",
		"align_btn/icon_vertical_highlight.png",
		"align_btn/icon_vertical_sel.png",
		"align_btn/icon_vertical_sel.png",
		"align_btn/icon_vertical_sel.png",
		"align_btn/icon_vertical_sel.png");
	vert_btns[1]->setStyleSheet(strStyleSheetVertCetner);

	QString strStyleSheetVertBottom = generate_radio_btn_style_sheet(
		"align_btn/icon_bottom_nor.png",
		"align_btn/icon_bottom_highlight.png",
		"align_btn/icon_bottom_sel.png",
		"align_btn/icon_bottom_sel.png",
		"align_btn/icon_bottom_sel.png",
		"align_btn/icon_bottom_sel.png");
	vert_btns[2]->setStyleSheet(strStyleSheetVertBottom);

	vert_btns[0]->setToolTip(QString::fromLocal8Bit("顶端对齐"));
	vert_btns[1]->setToolTip(QString::fromLocal8Bit("垂直居中"));
	vert_btns[2]->setToolTip(QString::fromLocal8Bit("底端对齐"));

	frame_layout_v->addWidget(vert_btns[0]);
	frame_layout_v->addWidget(vert_btns[1]);
	frame_layout_v->addWidget(vert_btns[2]);

	vert_margin = new QButtonGroup(frame_v);
	vert_margin->addButton(vert_btns[0], 0);
	vert_margin->addButton(vert_btns[1], 1);
	vert_margin->addButton(vert_btns[2], 2);

	QHBoxLayout * h_layout = new QHBoxLayout();
	h_layout->setSpacing(OVR_PROP_ROW_INTER_SPACE);

	QSpacerItem* rightSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

	h_layout->addWidget(frame_h);
	h_layout->addWidget(frame_v);
	h_layout->addItem(rightSpacer);

	pane_layout->addLayout(h_layout, group_row, 1);

	group_row++;
}

void OvrTextMakeupGroup::addAlignSpace(const QString & a_title, QLineEdit *& x, QLineEdit *& y)
{
	QRegExp regExpPort = QRegExp(cs_reg_exp_filter);
	QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExpPort);

	// title
	QLabel* label_title = new QLabel(a_title, this);
	pane_layout->addWidget(label_title, group_row, 0, 1, 1, Qt::AlignLeft | Qt::AlignVCenter);

	QHBoxLayout * h_layout = new QHBoxLayout();
	h_layout->setSpacing(OVR_PROP_ROW_INTER_SPACE);
	// x
	QLabel* label_x = new QLabel(QString::fromLocal8Bit("X"), this);
	h_layout->addWidget(label_x);

	x = new QLineEdit(this);
	h_layout->addWidget(x);

	x->setInputMethodHints(Qt::ImhDigitsOnly);
	x->setMaxLength(10);
	x->setValidator(portRegExpValidator);

	// y
	QLabel* label_y = new QLabel(QString::fromLocal8Bit("Y"), this);
	h_layout->addWidget(label_y);

	y = new QLineEdit(this);
	h_layout->addWidget(y);

	y->setInputMethodHints(Qt::ImhDigitsOnly);
	y->setMaxLength(6);
	y->setValidator(portRegExpValidator);


	pane_layout->addLayout(h_layout, group_row, 1);

	++group_row;
}

void OvrTextOthersGroup::onAddingItems()
{
	/*auto text_actor = dynamic_cast<ovr_engine::OvrTextActor*>(m_actor);

	background_preview = addTexturePreview(QString::fromLocal8Bit("背景"));
	auto texture = text_actor->GetBackgroundTexture();
	//QPixmap qpixmap = getImagePath("app_icon.png");
	QPixmap qpixmap = getTexturePixmap(getImagePath("app_icon.png"));

	background_preview->setPixmap(qpixmap.scaled(background_preview->size()));
	connect(background_preview, SIGNAL(double_clicked()), this, SLOT(textBackgroundSelectPop()), Qt::QueuedConnection);

	edit_transparency = addDigitesLineEdit(QString::fromLocal8Bit("透明"));
	float f_trans = text_actor->GetTransparentValue();
	edit_transparency->setText(QString::number(f_trans, 'f', 4));

	connect(edit_transparency, SIGNAL(editingFinished()), this, SLOT(transparencyChanged()), Qt::QueuedConnection);
	*/
}

void OvrTextOthersGroup::textBackgroundSelectPop()
{
	/*auto text_actor = dynamic_cast<ovr_engine::OvrTextActor*>(m_actor);

#if 1
	OvrFileDialog dlg(this);
	if (dlg.init(QString::fromLocal8Bit("选择图片"), "tex files(*.tex)", "") && dlg.exec() == 1)
	{
		QString relative_path = dlg.getSelectFile();
		OvrString ovr_path = relative_path.toLocal8Bit();
		ovr_project::OvrTextActorChangeTexture* new_command = new ovr_project::OvrTextActorChangeTexture(text_actor);
		new_command->SetTexture(ovr_path);
		ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new_command);

		QPixmap qpixmap = getTexturePixmap(relative_path);
		background_preview->setPixmap(qpixmap.scaled(background_preview->size()));
	}
#else
	QString title = QString::fromLocal8Bit("选择图片");
	OvrString work_dir = ovr_project::OvrProject::GetIns()->GetWorkDir();
	QString strWorkDir = QString::fromLocal8Bit(work_dir.GetStringConst());
	QString strFilter = QString::fromLocal8Bit(
		"tex files(*.tex);;"
		"All files(*.*)"
	);
	QFileDialog * open_file_dlg = open_const_dir_dlg(title, strWorkDir, strFilter, this);
	if (open_file_dlg->exec() == QDialog::Accepted) 
	{
		QString strPath = open_file_dlg->selectedFiles()[0];
		int n_index = strPath.indexOf(strWorkDir);
		if (n_index >= 0) 
		{  // 不在此目录里的文件不加载
			QString relative_path = strPath.right(strPath.length() - strWorkDir.length());
			OvrString ovr_path = relative_path.toLocal8Bit();
			ovr_project::OvrTextActorChangeTexture* new_command = new ovr_project::OvrTextActorChangeTexture(text_actor);
			new_command->SetTexture(ovr_path);
			ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new_command);
		}
		else {
			QMessageBox::warning(this, title,
				QString::fromLocal8Bit("非工程目录文件需要导入后才能使用！"));
		}
	}
#endif
*/
}

void OvrTextOthersGroup::transparencyChanged()
{
	/*auto text_actor = dynamic_cast<ovr_engine::OvrTextActor*>(m_actor);

	QString strTrans = edit_transparency->text();

	float f_trans = strTrans.toFloat();

	text_actor->SetTransparentValue(f_trans);*/
}

//QLabel * OvrTextOthersGroup::addBackgroundUi(const QString & a_title)
//{
//	QLabel* label = new QLabel(a_title, this);
//	pane_layout->addWidget(label, group_row, 0, 1, 1, Qt::AlignLeft | Qt::AlignVCenter);
//
//	QLabel* value = new OvrCdcLabel(this);
//	value->setStyleSheet("QLabel{background:rgb(0, 0, 0);}");
//	value->setMaximumSize(QSize(32, 32));
//	value->setMinimumSize(QSize(32, 32));
//	pane_layout->addWidget(value, group_row, 1, 1, 1, Qt::AlignLeft | Qt::AlignVCenter);
//
//	++group_row;
//	return value;
//}

void OvrCameraGroup::onAddingItems()
{
	/*auto camera_actor = dynamic_cast<ovr_engine::OvrCameraActor*>(m_actor);

	addVisualAngleUi(QString::fromLocal8Bit("视场"), slider_visual_angle, edit_visual_angle);
	auto f_arc = camera_actor->GetFieldView();

	int n_angle =static_cast<int>(arc_to_angle(f_arc));
	slider_visual_angle->setValue(n_angle);
	edit_visual_angle->setText(QString::number(n_angle));
	connect(slider_visual_angle, SIGNAL(valueChanged(int)), this, SLOT(sliderVisualAngleChanged(int)), Qt::QueuedConnection);
	connect(edit_visual_angle, SIGNAL(editingFinished()), this, SLOT(textVisualAngleChanged()), Qt::QueuedConnection);

	edit_aspect_radio = addDigitesLineEdit(QString::fromLocal8Bit("宽高比"));
	float f_aspect = camera_actor->GetAspectRatio();
	edit_aspect_radio->setText(QString::number(f_aspect, 'f', 4));
	connect(edit_aspect_radio, SIGNAL(editingFinished()), this, SLOT(textAspectRadioChanged()), Qt::QueuedConnection);

	edit_neer_clip_plane = addDigitesLineEdit(QString::fromLocal8Bit("近裁剪面"));
	float f_neer_clip = camera_actor->GetNearClip();
	edit_neer_clip_plane->setText(QString::number(f_neer_clip, 'f', 4));
	connect(edit_neer_clip_plane, SIGNAL(editingFinished()), this, SLOT(textNeerClipPlaneChanged()), Qt::QueuedConnection);

	edit_far_clip_plane = addDigitesLineEdit(QString::fromLocal8Bit("远裁剪面"));
	float f_far_clip = camera_actor->GetFarClip();
	edit_far_clip_plane->setText(QString::number(f_far_clip, 'f', 4));
	connect(edit_far_clip_plane, SIGNAL(editingFinished()), this, SLOT(textFarClipPlaneChanged()), Qt::QueuedConnection);
	*/
}

void OvrCameraGroup::sliderVisualAngleChanged(int value)
{
	/*auto camera_actor = dynamic_cast<ovr_engine::OvrCameraActor*>(m_actor);

	//int angle = slider_visual_angle->value();
	int n_angle = value;
	edit_visual_angle->setText(QString::number(n_angle));

	float f_arc = angle_to_arc(n_angle);

	camera_actor->SetFieldView(f_arc);*/
}

void OvrCameraGroup::textVisualAngleChanged()
{
	/*auto camera_actor = dynamic_cast<ovr_engine::OvrCameraActor*>(m_actor);

	QString strAngle = edit_visual_angle->text();
	float f_angle = strAngle.toFloat();

	// 限制范围
	f_angle = f_angle < 1 ? 1 : f_angle > 179 ? 179 : f_angle;

	int n_angle = static_cast<int>(f_angle);
	slider_visual_angle->setValue(n_angle);
	QString strNewAngle = QString::number(n_angle);
	if (strNewAngle != strAngle)
		edit_visual_angle->setText(strNewAngle);

	float f_arc = angle_to_arc(f_angle);
	camera_actor->SetFieldView(f_arc);*/
}

void OvrCameraGroup::textAspectRadioChanged()
{
	/*auto camera_actor = dynamic_cast<ovr_engine::OvrCameraActor*>(m_actor);

	QString strValue = edit_aspect_radio->text();
	float f_value = strValue.toFloat();
	camera_actor->SetAspectRatio(f_value);*/
}

void OvrCameraGroup::textNeerClipPlaneChanged()
{
	/*auto camera_actor = dynamic_cast<ovr_engine::OvrCameraActor*>(m_actor);

	QString strValue = edit_neer_clip_plane->text();
	float f_value = strValue.toFloat();
	camera_actor->SetNearClip(f_value);*/
}

void OvrCameraGroup::textFarClipPlaneChanged()
{
	/*auto camera_actor = dynamic_cast<ovr_engine::OvrCameraActor*>(m_actor);

	QString strValue = edit_far_clip_plane->text();
	float f_value = strValue.toFloat();
	camera_actor->SetFarClip(f_value);*/
}

void OvrCameraGroup::addVisualAngleUi(const QString & a_title, QSlider *& slider, QLineEdit *& editer)
{
	QLabel* label = new QLabel(a_title, this);
	pane_layout->addWidget(label, group_row, 0, 1, 1, Qt::AlignLeft | Qt::AlignVCenter);


	QHBoxLayout * h_layout = new QHBoxLayout();
	h_layout->setSpacing(OVR_PROP_ROW_INTER_SPACE);
	slider = new QSlider(Qt::Orientation::Horizontal, this);
	slider->setRange(1, 179);
	slider->setSingleStep(1);
	slider->setMinimumWidth(130);

	h_layout->addWidget(slider);

	editer = new QLineEdit(this);
	editer->setMaximumWidth(50);
	//slider->setMinimumSize(QSize(32, 32));
	QRegExp regExpPort = QRegExp(cs_reg_exp_filter);
	QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExpPort);
	editer->setInputMethodHints(Qt::ImhDigitsOnly);
	editer->setMaxLength(10);
	editer->setValidator(portRegExpValidator);

	h_layout->addWidget(editer);

	pane_layout->addLayout(h_layout, group_row, 1);

	++group_row;
}

void OvrParticleGroup::onAddingItems()
{
	/*auto particle_actor = dynamic_cast<ovr_engine::OvrParticleActor*>(m_actor);
	auto particle_sys = particle_actor->GetParticleSystem();
	auto particle_emitter = particle_sys->GetEmitter();

	texture_preview = addTexturePreview(QString::fromLocal8Bit("纹理"));
	//auto material = particle_actor->();
	//OvrString tex_path = material->GetName();
	//auto texture = particle_actor->GetTe();
	//QPixmap qpixmap = getImagePath("app_icon.png");
	QPixmap qpixmap = getTexturePixmap(getImagePath("app_icon.png"));
	texture_preview->setPixmap(qpixmap.scaled(texture_preview->size()));
	connect(texture_preview, SIGNAL(double_clicked()), this, SLOT(textureSelectPop()), Qt::QueuedConnection);

	label_color_sel = addColorSelLabel(QString::fromLocal8Bit("颜色"));
	OvrVector4 rgba = particle_actor->GetParticleColor()[0];
	label_color_sel->SetCurrentColor(rgba);
	connect(label_color_sel, SIGNAL(double_clicked()), this, SLOT(colorSelectPop()), Qt::QueuedConnection);

	addHBoxXYZ("旋转", self_rotate);
	float x = 0, y = 0, z = 0;
	//particle_emitter->GetInitEulerAngle(x, y, z);

	std::vector<int> xyz_value = {(int)arc_to_angle(x), (int)arc_to_angle(y), (int)arc_to_angle(z)};
	std::vector<QString> xyz_tip = {
		QString::fromLocal8Bit("(-90,90)"), QString::fromLocal8Bit("(-180,180)"), QString::fromLocal8Bit("(-180,180)")
	};

	for (size_t i = 0; i < 3; i++) {
		QLineEdit * cur_rotate = self_rotate.at(i);
		cur_rotate->setText(QString::number(xyz_value[i]));
		cur_rotate->setToolTip(xyz_tip[i]);
		connect(cur_rotate, SIGNAL(editingFinished()), this, SLOT(rotateEditingFinished()), Qt::QueuedConnection);
	}

	check_to_camera = addCheck("朝向相机");
	bool isFaceCamera = particle_emitter != nullptr? particle_emitter->GetFaceCamera() : false;
	check_to_camera->setChecked(isFaceCamera);

	connect(check_to_camera, SIGNAL(clicked(bool)), this, SLOT(toCameraCheckFinished(bool)), Qt::QueuedConnection);

	add_region_setting_ui(diameter_region_low, diameter_region_hight, QString::fromLocal8Bit("直径范围"));
	float region_min = particle_emitter != nullptr ? particle_emitter->GetMinSize() : 0;
	float region_max = particle_emitter != nullptr ? particle_emitter->GetMaxSize() : 0;
	diameter_region_low->setText(QString::number(region_min, 'f', 4));
	diameter_region_hight->setText(QString::number(region_max, 'f', 4));
	connect(diameter_region_low, SIGNAL(editingFinished()), this, SLOT(lineEditFinished()), Qt::QueuedConnection);
	connect(diameter_region_hight, SIGNAL(editingFinished()), this, SLOT(lineEditFinished()), Qt::QueuedConnection);

	add_region_setting_ui(speed_region_low, speed_region_hight, QString::fromLocal8Bit("速度范围"));
	float speed_min = particle_emitter != nullptr ? particle_emitter->GetMinSpeed(): 0;
	float speed_max = particle_emitter != nullptr? particle_emitter->GetMaxSpeed(): 0;
	speed_region_low->setText(QString::number(speed_min, 'f', 4));
	speed_region_hight->setText(QString::number(speed_max, 'f', 4));
	connect(speed_region_low, SIGNAL(editingFinished()), this, SLOT(lineEditFinished()), Qt::QueuedConnection);
	connect(speed_region_hight, SIGNAL(editingFinished()), this, SLOT(lineEditFinished()), Qt::QueuedConnection);

	add_region_setting_ui(life_region_low, life_region_hight, QString::fromLocal8Bit("生命范围"));
	float life_min = particle_emitter != nullptr ? particle_emitter->GetMinLife() : 0;
	float life_max = particle_emitter != nullptr ? particle_emitter->GetMaxLife() : 0;
	life_region_low->setText(QString::number(life_min, 'f', 4));
	life_region_hight->setText(QString::number(life_max, 'f', 4));
	connect(life_region_low, SIGNAL(editingFinished()), this, SLOT(lineEditFinished()), Qt::QueuedConnection);
	connect(life_region_hight, SIGNAL(editingFinished()), this, SLOT(lineEditFinished()), Qt::QueuedConnection);*/
}

void OvrParticleGroup::textureSelectPop()
{
	/*auto particle_actor = dynamic_cast<ovr_engine::OvrParticleActor*>(m_actor);

	OvrFileDialog dlg(this);
	if (dlg.init(QString::fromLocal8Bit("选择纹理"), "tex files(*.tex)", "") && dlg.exec() == QDialog::Accepted)
	{
		QString relative_path = dlg.getSelectFile();
		OvrString ovr_path = relative_path.toLocal8Bit();
		auto new_command = new ovr_project::OvrParticleActorChangeTexture(particle_actor);
		new_command->SetTexture(ovr_path);
		ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new_command);

		QPixmap qpixmap = getTexturePixmap(relative_path);
		texture_preview->setPixmap(qpixmap.scaled(texture_preview->size()));
	}*/
}

void OvrParticleGroup::colorSelectPop()
{
	/*auto particle_actor = dynamic_cast<ovr_engine::OvrParticleActor*>(m_actor);

	OvrVector4 rgba = particle_actor->GetParticleColor()[0];
	QColor oldColor(rgba._x * 255, rgba._y * 255, rgba._z * 255, rgba._w * 255);
	QColor curColor = getSelectedColor(oldColor);
	if (curColor == oldColor) {
		// 没有更新有颜色要还原
		OvrVector4 newColor;
		newColor._x = curColor.red() / 255.;
		newColor._y = curColor.green() / 255.;
		newColor._z = curColor.blue() / 255.;
		newColor._w = curColor.alpha() / 255.;
		//particle_actor->setcolor(newColor);

		label_color_sel->SetCurrentColor(curColor);
	}*/
}

void OvrParticleGroup::onColorChanged(const QColor& color)
{
	/*auto particle_actor = dynamic_cast<ovr_engine::OvrParticleActor*>(m_actor);
	OvrVector4 newColor;
	newColor._x = color.red() / 255.;
	newColor._y = color.green() / 255.;
	newColor._z = color.blue() / 255.;
	newColor._w = color.alpha() / 255.;
	//particle_actor->setcolor(newColor);

	label_color_sel->SetCurrentColor(color);*/
}

void OvrParticleGroup::toCameraCheckFinished(bool to_camera)
{
	/*auto particle_actor = dynamic_cast<ovr_engine::OvrParticleActor*>(m_actor);
	//particle_actor->;
	auto particle_sys = particle_actor->GetParticleSystem();
	auto particle_emitter = particle_sys->GetEmitter();

	if (particle_emitter != nullptr) particle_emitter->SetFaceCamera(to_camera);*/
}

void OvrParticleGroup::rotateEditingFinished()
{
	/*QLineEdit * rotate_edit = dynamic_cast<QLineEdit *>(sender());

	auto particle_actor = dynamic_cast<ovr_engine::OvrParticleActor*>(m_actor);
	auto particle_sys = particle_actor->GetParticleSystem();
	auto particle_emitter = particle_sys->GetEmitter();

	QString strAngleX = self_rotate.at(0)->text();
	QString strAngleY = self_rotate.at(1)->text();
	QString strAngleZ = self_rotate.at(2)->text();
	float angle_x = strAngleX.toFloat();
	float angle_y = strAngleY.toFloat();
	float angle_z = strAngleZ.toFloat();

	// 限制范围
	angle_x = angle_x < -90 ? -90 : angle_x > 90 ? 90 : angle_x;
	angle_y = angle_y < -180 ? -180 : angle_y > 180 ? 180 : angle_y;
	angle_z = angle_z < -180 ? -180 : angle_z > 180 ? 180 : angle_z;

	int n_angle ;
	if (rotate_edit == self_rotate.at(0))
		n_angle = static_cast<int>(angle_x);
	else if (rotate_edit == self_rotate.at(1))
		n_angle = static_cast<int>(angle_y);
	else if (rotate_edit == self_rotate.at(2))
		n_angle = static_cast<int>(angle_z);
	QString strNewAngle = QString::number(n_angle);
	if (strNewAngle != rotate_edit->text())
		rotate_edit->setText(strNewAngle);

	float x = angle_to_arc(angle_x), y = angle_to_arc(angle_y), z = angle_to_arc(angle_z);
	//if (particle_emitter != nullptr) particle_emitter->SetInitEulerAngle(x, y, z);*/
}

void OvrParticleGroup::lineEditFinished()
{
	/*auto particle_actor = dynamic_cast<ovr_engine::OvrParticleActor*>(m_actor);

	QLineEdit * line_edit = dynamic_cast<QLineEdit *>(sender());
	QString strCurText = line_edit->text();

	bool bresult = false;
	float cur_value = strCurText.toFloat(&bresult);
	if (strCurText.startsWith("+.") || strCurText.startsWith("-.") || strCurText.startsWith("."))
		line_edit->setText(QString::number(cur_value, 'f', 4));

	auto particle_emitter = particle_actor->GetParticleSystem()->GetEmitter();
	if (line_edit == diameter_region_low || line_edit == diameter_region_hight) {
		QString strRegionMin = diameter_region_low->text(), strRegionMax = diameter_region_hight->text();
		float region_min = strRegionMin.toFloat(), region_max = strRegionMax.toFloat();

		if (particle_emitter != nullptr) particle_emitter->SetSize(region_max, region_min);
	}
	else if (line_edit == speed_region_low || line_edit == speed_region_hight) {
		QString strRegionMin = speed_region_low->text(), strRegionMax = speed_region_hight->text();
		float region_min = strRegionMin.toFloat(), region_max = strRegionMax.toFloat();

		if (particle_emitter != nullptr) particle_emitter->SetSpeed(region_max, region_min);
	}
	else if (line_edit == life_region_low || line_edit == life_region_hight) {
		QString strRegionMin = life_region_low->text(), strRegionMax = life_region_hight->text();
		float region_min = strRegionMin.toFloat(), region_max = strRegionMax.toFloat();

		if (particle_emitter != nullptr) particle_emitter->SetLife(region_max, region_min);
	}*/

}

void OvrParticleGroup::add_region_setting_ui(QLineEdit * &region_low, QLineEdit * &region_hight, const QString &a_title)
{
	QLabel* label = new QLabel(a_title, this);
	pane_layout->addWidget(label, group_row, 0, 1, 1, Qt::AlignLeft | Qt::AlignVCenter);

	QHBoxLayout * h_layout = new QHBoxLayout();
	h_layout->setSpacing(OVR_PROP_ROW_INTER_SPACE);

	auto get_edit_ui = [](QLineEdit *line_edit) {
		//QLineEdit* line_edit = new QLineEdit(this);
		QRegExp regExpPort = QRegExp(cs_reg_exp_filter);
		QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExpPort);
		line_edit->setInputMethodHints(Qt::ImhDigitsOnly);
		line_edit->setMaxLength(10);
		line_edit->setValidator(portRegExpValidator);

		return line_edit;
	};

	region_low = get_edit_ui(new QLineEdit(this));
	h_layout->addWidget(region_low);

	QLabel* label_link = new QLabel(QString::fromLocal8Bit("～"), this);
	h_layout->addWidget(label_link);

	region_hight = get_edit_ui(new QLineEdit(this));
	h_layout->addWidget(region_hight);

	pane_layout->addLayout(h_layout, group_row, 1);

	++group_row;
}

void OvrEmmitterGroup::onAddingItems()
{
	/*auto particle_actor = dynamic_cast<ovr_engine::OvrParticleActor*>(m_actor);
	auto particle_sys = particle_actor->GetParticleSystem();
	auto particle_emitter = particle_sys->GetEmitter();
	//particle_emitter->GetParticleEmitterTyep();

	combo_emmitter = addCombo("类型");

	static std::map<ovr_engine::OvrParticleEmitterType, QString> qm_emitter_type = {
		{ ovr_engine::eParticleBox , QString::fromLocal8Bit("立方体") },
		{ ovr_engine::eParticleCircle , QString::fromLocal8Bit("圆") },
		{ ovr_engine::eParticleLine , QString::fromLocal8Bit("线") },
		{ ovr_engine::eParticleMesh , QString::fromLocal8Bit("网格") },
		{ ovr_engine::eParticlePoint , QString::fromLocal8Bit("点") },
		{ ovr_engine::eParticleSphere , QString::fromLocal8Bit("球") },
	};

	for (auto &pair_info: qm_emitter_type)
	{
		combo_emmitter->addItem(pair_info.second, pair_info.first);
	}

	auto emmitter_type = (particle_emitter != nullptr)? particle_emitter->GetParticleEmitterTyep() : -1;

	for (size_t i = 0; i < combo_emmitter->count(); i++)
	{
		QVariant qva_data = combo_emmitter->itemData(i);
		int item_type = qva_data.toInt();

		if (item_type == emmitter_type) {
			combo_emmitter->setCurrentIndex(i);
			break;
		}
	}

	connect(combo_emmitter, SIGNAL(currentIndexChanged(int)), this, SLOT(update_sender_args_ui(int)));

	// update args ui.
	set_sender_args_ui();
	update_emitter_args_ui(emmitter_type);
	update_emitter_args(particle_emitter);*/
}

void OvrEmmitterGroup::set_sender_args_ui()
{
	label_title = new QLabel("");
	pane_layout->addWidget(label_title, group_row, 0, 1, 1, Qt::AlignLeft | Qt::AlignVCenter);

	QVBoxLayout * v_layout = new QVBoxLayout();
	{
		editer_args = new QLineEdit();
		//pane_layout->addWidget(value, group_row, 1);

		QRegExp regExpPort = QRegExp(cs_reg_exp_filter);
		QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExpPort);
		editer_args->setInputMethodHints(Qt::ImhDigitsOnly);
		editer_args->setMaxLength(10);
		editer_args->setValidator(portRegExpValidator);

		connect(editer_args, SIGNAL(editingFinished()), this, SLOT(argsEditFinished()));

		v_layout->addWidget(editer_args);
	}

	{
		QHBoxLayout * h_layout = new QHBoxLayout();
		h_layout->setSpacing(OVR_PROP_ROW_INTER_SPACE);

		auto add_xyz_edit_ui = [this](const QString &text, QLayout* layout, QLabel *&label, QLineEdit *&editor) {
			label = new QLabel(text);
			layout->addWidget(label);

			editor = new QLineEdit();
			QRegExp regExpPort = QRegExp(cs_reg_exp_filter);
			editor->setInputMethodHints(Qt::ImhDigitsOnly);
			editor->setMaxLength(10);
			editor->setValidator(new QRegExpValidator(regExpPort));

			layout->addWidget(editor);
			connect(editor, SIGNAL(editingFinished()), this, SLOT(argsEditFinished()));
		};

		add_xyz_edit_ui(QString::fromLocal8Bit("X"), h_layout, label_x, editer_cube_x);
		add_xyz_edit_ui(QString::fromLocal8Bit("Y"), h_layout, label_y, editer_cube_y);
		add_xyz_edit_ui(QString::fromLocal8Bit("Z"), h_layout, label_z, editer_cube_z);

		v_layout->addLayout(h_layout);
	}

	{
		QHBoxLayout * h_layout = new QHBoxLayout();
		h_layout->setSpacing(OVR_PROP_ROW_INTER_SPACE);

		editer_mesh_path = new QLineEdit();
		editer_mesh_path->setReadOnly(true);
		h_layout->addWidget(editer_mesh_path);

		btn_mesh_path = new QToolButton();
		btn_mesh_path->setText("guiesabgujio");  // QString::fromLocal8Bit("...")
		btn_mesh_path->setStyleSheet(s_tool_btn_style_sheet);
		h_layout->addWidget(btn_mesh_path);

		connect(btn_mesh_path, SIGNAL(clicked()), this, SLOT(argsEditFinished()));

		v_layout->addLayout(h_layout);
	}

	pane_layout->addLayout(v_layout, group_row, 1);

	++group_row;
}

void OvrEmmitterGroup::show_line_edit_ui(bool bshow)
{
	bshow ? editer_args->show() : editer_args->hide();
}

void OvrEmmitterGroup::show_cube_xy_ui(bool bshow)
{
	bshow ? label_x->show() : label_x->hide();
	bshow ? label_y->show() : label_y->hide();
	bshow ? label_z->show() : label_z->hide();
	bshow ? editer_cube_x->show() : editer_cube_x->hide();
	bshow ? editer_cube_y->show() : editer_cube_y->hide();
	bshow ? editer_cube_z->show() : editer_cube_z->hide();
}

void OvrEmmitterGroup::show_mesh_ui(bool bshow)
{
	bshow ? editer_mesh_path->show() : editer_mesh_path->hide();
	bshow ? btn_mesh_path->show() : btn_mesh_path->hide();
}

void OvrEmmitterGroup::update_emitter_args(ovr_engine::OvrParticleEmitter * emitter)
{
	if (!emitter)
		return;

	auto emmitter_type = emitter->GetParticleEmitterTyep();

	switch (emmitter_type)
	{
	case ovr_engine::eParticlePoint: {
		label_title->setText(QString::fromLocal8Bit("发散角度"));
		auto special_emitter = dynamic_cast<ovr_engine::OvrParticleEmitterPoint *>(emitter);
		float scatter_angle = special_emitter->GetScatterAngle();
		editer_args->setText(QString::number(scatter_angle, 'f', 4));
		break;
	}
	case ovr_engine::eParticleLine: {
		label_title->setText(QString::fromLocal8Bit("线长"));
		auto special_emitter = dynamic_cast<ovr_engine::OvrParticleEmitterLine *>(emitter);
		//float point_length = (special_emitter->GetEndPt() - special_emitter->GetBeginPt()).length();
		//float point_length = special_emitter->GetLineLength();
		//editer_args->setText(QString::number(point_length, 'f', 4));
		break;
	}
	case ovr_engine::eParticleCircle: {
		label_title->setText(QString::fromLocal8Bit("半径"));
		auto special_emitter = dynamic_cast<ovr_engine::OvrParticleEmitterCircle *>(emitter);
		float radius = special_emitter->GetRadius();
		editer_args->setText(QString::number(radius, 'f', 4));
		break;
	}
	case ovr_engine::eParticleSphere: {
		label_title->setText(QString::fromLocal8Bit("半径"));
		auto special_emitter = dynamic_cast<ovr_engine::OvrParticleEmitterSphere *>(emitter);
		float radius = special_emitter->GetRadius();
		editer_args->setText(QString::number(radius, 'f', 4));
		break;
	}
	case ovr_engine::eParticleBox: {
		label_title->setText(QString::fromLocal8Bit("尺寸"));
		auto special_emitter = dynamic_cast<ovr_engine::OvrParticleEmitterBox *>(emitter);
		auto box_size = special_emitter->GetBoxSize();
		editer_cube_x->setText(QString::number(box_size._x, 'f', 4));
		editer_cube_y->setText(QString::number(box_size._y, 'f', 4));
		editer_cube_z->setText(QString::number(box_size._z, 'f', 4));
		break;
	}
	case ovr_engine::eParticleMesh: {
		label_title->setText(QString::fromLocal8Bit("网格"));
		auto special_emitter = dynamic_cast<ovr_engine::OvrParticleEmitterMesh *>(emitter);

		//special_emitter->
		btn_mesh_path->setText(QString());

		break;
	}
	default:
		break;
	}
}

void OvrEmmitterGroup::update_emitter_args_ui(int a_type)
{
	switch (a_type)
	{
	case ovr_engine::eParticlePoint:
		label_title->setText(QString::fromLocal8Bit("发散角度"));
		show_line_edit_ui(true);
		show_cube_xy_ui(false);
		show_mesh_ui(false);
		break;
	case ovr_engine::eParticleLine:
		label_title->setText(QString::fromLocal8Bit("线长"));
		show_line_edit_ui(true);
		show_cube_xy_ui(false);
		show_mesh_ui(false);
		break;
	case ovr_engine::eParticleCircle:
		label_title->setText(QString::fromLocal8Bit("半径"));
		show_line_edit_ui(true);
		show_cube_xy_ui(false);
		show_mesh_ui(false);
		break;
	case ovr_engine::eParticleSphere:
		label_title->setText(QString::fromLocal8Bit("半径"));
		show_line_edit_ui(true);
		show_cube_xy_ui(false);
		show_mesh_ui(false);
		break;
	case ovr_engine::eParticleBox:
		label_title->setText(QString::fromLocal8Bit("尺寸"));
		show_line_edit_ui(false);
		show_cube_xy_ui(true);
		show_mesh_ui(false);
		break;
	case ovr_engine::eParticleMesh:
		label_title->setText(QString::fromLocal8Bit("网格"));
		show_line_edit_ui(false);
		show_cube_xy_ui(false);
		show_mesh_ui(true);
		break;
	default:
		show_line_edit_ui(false);
		show_cube_xy_ui(false);
		show_mesh_ui(false);

		break;
	}
}

void OvrEmmitterGroup::update_sender_args_ui(int index)
{
	/*QVariant qva_data = combo_emmitter->itemData(index);
	int emitter_type = qva_data.toInt();

	auto particle_actor = dynamic_cast<ovr_engine::OvrParticleActor*>(m_actor);
	auto particle_sys = particle_actor->GetParticleSystem();

	switch (emitter_type)
	{
	case ovr_engine::eParticlePoint:
		particle_sys->ChangeEmitter("OvrParticleEmitterPoint");
		break;
	case ovr_engine::eParticleLine:
		particle_sys->ChangeEmitter("OvrParticleEmitterLine");
		break;
	case ovr_engine::eParticleCircle:
		particle_sys->ChangeEmitter("OvrParticleEmitterCircle");
		break;
	case ovr_engine::eParticleSphere:
		particle_sys->ChangeEmitter("OvrParticleEmitterSphere");
		break;
	case ovr_engine::eParticleBox:
		particle_sys->ChangeEmitter("OvrParticleEmitterBox");
		break;
	case ovr_engine::eParticleMesh:
		particle_sys->ChangeEmitter("OvrParticleEmitterMesh");
		break;
	default:
		particle_sys->ChangeEmitter("error_emitter_name");
		assert(false);
		break;
	}

	ovr_engine::OvrParticleEmitter* particle_emitter = particle_sys->GetEmitter();
	int real_emitter_type = (particle_emitter != nullptr)? particle_emitter->GetParticleEmitterTyep() : -1;

	update_emitter_args_ui(real_emitter_type);
	update_emitter_args(particle_emitter);*/
}

void OvrEmmitterGroup::argsEditFinished()
{
	/*auto particle_actor = dynamic_cast<ovr_engine::OvrParticleActor*>(m_actor);
	auto particle_sys = particle_actor->GetParticleSystem();
	auto particle_emitter = particle_sys->GetEmitter();

	QVariant qva_data = combo_emmitter->currentData();
	int switch_on = qva_data.toInt();

	switch (switch_on)
	{
	case ovr_engine::eParticlePoint: {
		QString strCurValue = editer_args->text();
		float f_value = strCurValue.toFloat();
		auto special_emitter = dynamic_cast<ovr_engine::OvrParticleEmitterPoint*>(particle_emitter);
		//if (special_emitter != nullptr) special_emitter->SetScatterAngle(f_value); 
		break;
	}
	case ovr_engine::eParticleLine: {
		QString strCurValue = editer_args->text();
		float f_value = strCurValue.toFloat();

		auto special_emitter = dynamic_cast<ovr_engine::OvrParticleEmitterLine*>(particle_emitter);
		//if (special_emitter != nullptr) special_emitter->SetLineLength(f_value);
		break;
	}
	case ovr_engine::eParticleCircle: {
		QString strCurValue = editer_args->text();
		float f_value = strCurValue.toFloat();
		auto special_emitter = dynamic_cast<ovr_engine::OvrParticleEmitterCircle*>(particle_emitter);

		if (special_emitter != nullptr) special_emitter->SetRadius(f_value);
		break;
	}
	case ovr_engine::eParticleSphere: {
		QString strCurValue = editer_args->text();
		float f_value = strCurValue.toFloat();
		auto special_emitter = dynamic_cast<ovr_engine::OvrParticleEmitterSphere*>(particle_emitter);

		if (special_emitter != nullptr) special_emitter->SetRadius(f_value);
		break;
	}
	case ovr_engine::eParticleBox: {
		float f_value_x = editer_cube_x->text().toFloat();
		float f_value_y = editer_cube_y->text().toFloat();
		float f_value_z = editer_cube_z->text().toFloat();
		auto special_emitter = dynamic_cast<ovr_engine::OvrParticleEmitterBox*>(particle_emitter);

		OvrVector3 box_size(f_value_x, f_value_y, f_value_z);
		if (special_emitter != nullptr) special_emitter->SetBoxSize(box_size);
		break;
	}
	case ovr_engine::eParticleMesh: {
		auto special_emitter = dynamic_cast<ovr_engine::OvrParticleEmitterMesh*>(particle_emitter);
		if (special_emitter != nullptr) pathSelectPop(special_emitter);
		break;
	}
	default:
		break;
	}*/
}

void OvrEmmitterGroup::pathSelectPop(ovr_engine::OvrParticleEmitterMesh* mesh_emitter)
{
	if (!mesh_emitter)
		return;
	//auto particle_actor = dynamic_cast<ovr_engine::OvrParticleActor*>(m_actor);
	//auto particle_sys = particle_actor->GetParticleSystem();
	//auto particle_emitter = particle_sys->GetEmitter();

	OvrFileDialog dlg;
	if (dlg.init(QString::fromLocal8Bit("选择网格"), "mesh files(*.mesh)", "") && dlg.exec() == QDialog::Accepted)
	{
		QString relative_path = dlg.getSelectFile();
		OvrString ovr_path = relative_path.toLocal8Bit();
		//auto new_command = new ovr_project::OvrParticleActorChangeTexture(particle_actor);
		//new_command->SetTexture(ovr_path);
		//ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new_command);

		//mesh_emitter->Set

		//texture_preview->setPixmap(qpixmap.scaled(texture_preview->size()));
		editer_mesh_path->setText(relative_path);
	}
}
