#pragma once

#include <QLabel>

class OvrCdcLabel : public QLabel
{
	Q_OBJECT
public:
	explicit OvrCdcLabel(QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	explicit OvrCdcLabel(const QString &text, QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	~OvrCdcLabel();

Q_SIGNALS:
	void onMouseDbClick();

private:
	virtual void mouseDoubleClickEvent(QMouseEvent *event) override;
};

///////////////////////////////////////////////////////////////////////////////
class OvrColorLabel : public OvrCdcLabel
{
	Q_OBJECT
public:
	explicit OvrColorLabel(QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	explicit OvrColorLabel(const QString &text, QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	~OvrColorLabel();

public:
	void SetCurrentColor(const OvrVector4 &rgba);
	void SetCurrentColor(const QColor & color);

Q_SIGNALS:

private:

};

///////////////////////////////////////////////////////////////////////////////
class OvrTextureLabel : public OvrCdcLabel
{
	Q_OBJECT
public:
	explicit OvrTextureLabel(QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	explicit OvrTextureLabel(const QString &text, QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	~OvrTextureLabel();

public:
	void SetTexture();

Q_SIGNALS:

private:

};

