#include "Header.h"
#include "ovr_actor_outline_tree.h"

//#include <wx_engine/ovr_light_actor.h>
#include <wx_project/command/ovr_command_actor.h>
#include <wx_project/ovr_scene_editor.h>

const int OvrActorOutlineTree::CONST_UUID_ROLE = Qt::UserRole + 1;
const int OvrActorOutlineTree::CONST_TYPE_ROLE = Qt::UserRole + 2;
const int OvrActorOutlineTree::CONST_VISIBLE_ROLE = Qt::UserRole + 3;

static OvrActorOutlineTree* g_tree_inst = nullptr;

OvrActorOutlineTree::OvrActorOutlineTree(QWidget *parent)
	: QTreeWidget(parent)
{
	g_tree_inst = this;
	init_custom_ui();
	set_custom_style_sheet();

	QStringList e = { "onProjectOpen", "onCurrentSceneChanged", "OnAddActor" , 
		"OnRmvActor", "OnSetSelectedItem", "OnUpdateTransform", "onUpdateActorName" };

	registerEventListener(e);
}

OvrActorOutlineTree::~OvrActorOutlineTree()
{
	g_tree_inst = nullptr;
}

OvrActorOutlineTree * OvrActorOutlineTree::getInst()
{
	return g_tree_inst;
}


void OvrActorOutlineTree::init_custom_ui()
{
	//拖拽
	this->setDragEnabled(true);
	this->setDragDropMode(QAbstractItemView::InternalMove);
	this->setEditTriggers(QAbstractItemView::NoEditTriggers);

	this->setHeaderHidden(true);
	QStringList qslHeaderKLabels = {
		QString::fromLocal8Bit("对象") ,
	};
	this->setHeaderLabels(qslHeaderKLabels);

	this->setColumnWidth(0, 200);
}

void OvrActorOutlineTree::set_custom_style_sheet()
{
	QString strStyleSheet = QString::fromLocal8Bit(""
		"QTreeView{background-color:rgb(48, 48, 48); color:rgb(192, 192, 192); border:0px solid rgb(24, 24, 24); border-top:0px; padding-left:0px; show-decoration-selected: 1;}"
		"QTreeView::item{height:24px; border-bottom:1px solid rgb(39, 39, 39); color:rgb(192, 192, 192); outline:3px solid rgb(24, 124, 89);}"
		"QTreeView::item::hover{background-color: rgb(53, 53, 53);}"
		"QTreeView::item:selected{background-color: rgb(64, 64, 64);}"
		"QTreeView::branch{height:24px; border-bottom:1px solid rgb(39, 39, 39);}"
		"QTreeView::branch:hover{background-color:rgb(53, 53, 53);}"
		"QTreeView::branch:selected{background-color:rgb(64, 64, 64);}"
	);

	QTextStream qtsTemp(&strStyleSheet);
	qtsTemp << "QTreeView::branch:has-children:closed{image:url(" << getImagePath("outline/node_closed.png") << ");}"
		<< "QTreeView::branch:has-children:open{image:url(" << getImagePath("outline/node_opened.png") << ");}"
		<< "QTreeView::indicator{image:url(" << getImagePath("outline/icon_eye_open.png") << "); }"
		<< "QTreeView::indicator:unchecked{image:url(" << getImagePath("outline/icon_eye_close.png") << "); }"

		<< "QCheckBox::indicator{image:url(" << getImagePath("outline/icon_eye_open.png") << "); }"
		<< "QCheckBox::indicator:unchecked{image:url(" << getImagePath("outline/icon_eye_close.png") << "); }"
		;

	this->setStyleSheet(strStyleSheet);
}

void OvrActorOutlineTree::currentChanged(const QModelIndex & current, const QModelIndex & previous)
{
	// 处理默认行为
	QTreeWidget::currentChanged(current, previous);

	// 处理自定义行为
	int node_type = ActorOutlineItemType::kOITNone;
	ovr_engine::OvrActor* sel_actor = nullptr;
	if (current.isValid())
	{
		// 同一行的改变不需要再更新
		if ((current.parent() == previous.parent()) && (current.row() == previous.row()))
			return;

		this->scrollTo(current);		// 保证选择项可见

		node_type = current.data(CONST_TYPE_ROLE).toInt();

		QString item_id = current.data(CONST_UUID_ROLE).toString();
		sel_actor = actor_id_to_ptr(item_id);
	}

	emit onActorSelected(sel_actor, node_type);

	ovr_project::OvrProject * project = ovr_project::OvrProject::GetIns();
	ovr_project::OvrSceneEditor* scene_context = project->GetCurrentSceneEditor();
	scene_context->SetSelectedActor(sel_actor);
}

void OvrActorOutlineTree::mouseReleaseEvent(QMouseEvent * event)
{
	QModelIndex index = indexAt(event->pos());
	this->setCurrentIndex(index);

	if (event->button() != Qt::RightButton || !index.isValid())
	{
		QTreeView::mouseReleaseEvent(event);
		return;
	}

	create_sub_menu(index);
}

void OvrActorOutlineTree::dropEvent(QDropEvent * event)
{
	// 先执行默认行为, 用于生成标准动作
	QTreeWidget::dropEvent(event);

	auto mime_data = event->mimeData();
	if (!mime_data->hasFormat(CONST_MIME_DATA_TYPE))
		return;

	// 再处理自定义行为
	std::function<void(const QModelIndex &)> update_item_widget =
		[this, &update_item_widget](const QModelIndex &item_index) {
		auto cur_item = this->itemFromIndex(item_index);
		bool b_visible = cur_item->data(0, CONST_VISIBLE_ROLE).toBool();
		add_item_widget(cur_item, b_visible);

		for (size_t i = 0; ; i++)
		{
			auto child_index = item_index.child(i, 0);
			if (!child_index.isValid())
				break;

			update_item_widget(child_index);
		}
	};

	// 开始更新
	QByteArray encodeData = mime_data->data(CONST_MIME_DATA_TYPE);
	QDataStream stream(&encodeData, QIODevice::ReadWrite);
	while (!stream.atEnd())
	{
		QString str_uuid ;
		stream >> str_uuid;

		auto droped_actor = actor_id_to_ptr(str_uuid);
		QModelIndex droped_item_index = get_item_index(str_uuid);

		update_item_widget(droped_item_index);

		QString parent_uuid("");   // 如果 是 root 填空值即可
		QModelIndex parent_item_index = droped_item_index.parent();
		if (parent_item_index.isValid()) {
			parent_uuid = parent_item_index.data(CONST_UUID_ROLE).toString();
		}

		auto new_command = new ovr_project::OvrRelationChangedActor();
		new_command->SetChangeRelationName(
			parent_uuid.toLocal8Bit().constData(),
			str_uuid.toLocal8Bit().constData());
		ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new_command);
	} // while
}

QStringList OvrActorOutlineTree::mimeTypes() const
{
	QStringList qsl_types = QTreeWidget::mimeTypes();
	qsl_types << CONST_MIME_DATA_TYPE;

	return qsl_types;
}

QMimeData * OvrActorOutlineTree::mimeData(const QList<QTreeWidgetItem*> items) const
{
	QMimeData * def_mime_data = QTreeWidget::mimeData(items);
	if (!def_mime_data) def_mime_data = new QMimeData();

	QByteArray itemData;
	for (auto single_item : items) {
		QDataStream dataStream(&itemData, QIODevice::WriteOnly);

		dataStream << single_item->data(0, CONST_UUID_ROLE).toString();
	}
	def_mime_data->setData(CONST_MIME_DATA_TYPE, itemData);

	return def_mime_data;
}

void OvrActorOutlineTree::onUpdatedTransform(QString selected_id)
{
	ovr_core::OvrSceneContext* scene_context = ovr_project::OvrProject::GetIns()->GetCurrentScene();
	ovr_engine::OvrActor* actor = scene_context->GetActorByUniqueName(selected_id.toLocal8Bit().constData());

	emit onUpdateTransformToProp(actor);
}

void OvrActorOutlineTree::onSetedSelectedItem(QString str_id)
{
	QModelIndex sel_index = get_item_index(str_id);
	setCurrentIndex(sel_index);
	scrollTo(sel_index);
}

void OvrActorOutlineTree::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{
	if (a_event_name == "onProjectOpen")
	{
		onProjectOpen(a_param1.toBool(), a_param2.toString());
	}
	else if (a_event_name == "onCurrentSceneChanged")
	{
		onCurrentSceneChanged();
	}
	else if (a_event_name == "OnAddActor")
	{
		onActorAdded(a_param1.toString());
	}
	else if (a_event_name == "OnRmvActor")
	{
		onActorRemoved(a_param1.toString());
	}
	else if (a_event_name == "OnSetSelectedItem")
	{
		onSetedSelectedItem(a_param1.toString());
	}
	else if (a_event_name == "OnUpdateTransform")
	{
		onUpdatedTransform(a_param1.toString());
	}
	else if (a_event_name == "onUpdateActorName")
	{
		update_display_name(a_param1.toString(), a_param2.toString());
	}
}

void OvrActorOutlineTree::onProjectOpen(bool is_ok, QString a_prj_dir)
{
	clear();
}

void OvrActorOutlineTree::onCurrentSceneChanged()
{
	refreshActorTree();

	expandToDepth(1);
	setCurrentItem(topLevelItem(0));
}

void OvrActorOutlineTree::onActorAdded(QString a_actor_id)
{
	ovr_engine::OvrActor *actor = actor_id_to_ptr(a_actor_id);
	ovr_engine::OvrActor * parent_actor = actor->GetParent();

	OvrString ovr_id = parent_actor->GetUniqueName();

	QString strItemId = QString::fromLocal8Bit(ovr_id.GetStringConst());
	QModelIndex parent_index = get_item_index(strItemId);

	QModelIndex added_index;
	int row_count = this->model()->rowCount();
	if (parent_index.isValid()) {
		// 有父结点
		auto parent_item = this->itemFromIndex(parent_index);
		auto cur_item = add_actor(actor, parent_item, row_count);

		added_index = this->indexFromItem(cur_item);
	}
	else {
		// 没有父结点
		auto cur_item = add_actor(actor, this, row_count);
		added_index = this->indexFromItem(cur_item);
	}

	//QModelIndex added_index = get_item_index(a_actor_id);
	this->setCurrentIndex(added_index);
	this->scrollTo(added_index);
}

void OvrActorOutlineTree::onActorRemoved(QString a_actor_id)
{
	QModelIndex rmv_index = get_item_index(a_actor_id);
	QModelIndex parent_index = rmv_index.parent();

	this->model()->removeRow(rmv_index.row(), rmv_index.parent());
}

void OvrActorOutlineTree::refreshActorTree()
{
	clear();

	ovr_project::OvrProject* prj = ovr_project::OvrProject::GetIns();
	ovr_project::OvrSceneEditor*	scene_editor = prj->GetCurrentSceneEditor();
	ovr_core::OvrSceneContext* curr_scene = prj->GetCurrentScene();
	ovr_engine::OvrActor* root_actor = curr_scene->GetRootActor();

	for (ovr::uint32 index = 0, item_index = 0; index < root_actor->GetChildCount(); index++)
	{
		ovr_engine::OvrActor* child_actor = root_actor->GetChild(index);

		//过滤不用在场景中显示的Actor的  其子actor都不用再显示
		if (!scene_editor->IsShowInScene(child_actor))
			continue;

		auto cur_item = add_actor(child_actor, this, item_index++);
		add_child_actor(scene_editor, child_actor, cur_item);
	}
}

QModelIndex OvrActorOutlineTree::get_item_index(const QString & str_id)
{
	std::function<QModelIndex(const QModelIndex&, const QString &)> search_tree_fun =
		[this, &search_tree_fun](const QModelIndex &parIndex, const QString &strId)->QModelIndex {

		auto tree_model = this->model();
		for (int i = 0; i < tree_model->rowCount(parIndex); i++) {
			QModelIndex currentIndex = tree_model->index(i, 0, parIndex);
			QString itemId = currentIndex.data(CONST_UUID_ROLE).toString();
			if (itemId == strId) {
				return currentIndex;
			}

			// 进入下级搜索
			QModelIndex index = search_tree_fun(currentIndex, strId);
			if (index.isValid()) {
				return index;
			}
		}

		return QModelIndex();  // 没找到
	};

	QModelIndex mi = this->rootIndex();
	return search_tree_fun(mi, str_id);
}

void OvrActorOutlineTree::update_eye_status(const QString & str_uuid, bool is_show)
{
	QModelIndex sel_index = get_item_index(str_uuid);

	update_eye_status(sel_index, is_show);
}

void OvrActorOutlineTree::update_display_name(const QString & str_uuid, const QString & str_name)
{
	QModelIndex sel_index = get_item_index(str_uuid);

	update_display_name(sel_index, str_name);
}

void OvrActorOutlineTree::update_eye_status(const QModelIndex & current, bool is_show)
{
	if (!current.isValid())
		return;

	auto curr_item = itemFromIndex(current);
	QWidget * item_widget = this->itemWidget(curr_item, 0);

	QCheckBox * check_box = nullptr;
	for (auto child : item_widget->children()) {
		if (child->objectName() == CONST_CHECK_BOX_NAME) {
			check_box = dynamic_cast<QCheckBox *>(child);
			break;
		}
	}
	if (check_box != nullptr)
		check_box->setChecked(is_show);
}

void OvrActorOutlineTree::update_display_name(const QModelIndex & current, const QString & str_name)
{
	if (!current.isValid())
		return;

	auto curr_item = itemFromIndex(current);
	curr_item->setText(0, str_name);
}

QMenu* OvrActorOutlineTree::create_sub_menu(const QModelIndex & index)
{
	if (right_memu != nullptr)
		delete right_memu;

	right_memu = new QMenu(this);
	right_memu->addAction(QString::fromLocal8Bit("删除(&D)"), std::bind(&OvrActorOutlineTree::remove_actor, this, index));
	right_memu->addAction(QString::fromLocal8Bit("复制(&C)"), std::bind(&OvrActorOutlineTree::copy_actor, this, index));

	QPoint cursor_pos = cursor().pos();
	QDesktopWidget* desktopWidget = QApplication::desktop();
	//获取可用桌面大小
	//QRect deskRect = desktopWidget->availableGeometry();
	//获取设备屏幕大小
	QRect screenRect = desktopWidget->screenGeometry();
	//获取系统设置的屏幕个数（屏幕拷贝方式该值为1）
	//int nScreenCount = desktopWidget->screenCount();

	QPoint pop_pos;
	// 设置 X 坐标
	if (cursor_pos.x() + right_memu->width() > screenRect.width()) {
		pop_pos.setX(screenRect.width() - right_memu->width());
	}
	else {
		pop_pos.setX(cursor_pos.x());
	}

	// 设置 Y 坐标
	if (cursor_pos.y() + right_memu->height() > screenRect.height()) {
		pop_pos.setY(screenRect.height() - right_memu->height());
	}
	else {
		pop_pos.setY(cursor_pos.y());
	}

	right_memu->move(pop_pos);
	right_memu->show();

	return right_memu;
}

void OvrActorOutlineTree::remove_actor(const QModelIndex& index)
{
	QString strUuid = index.data(CONST_UUID_ROLE).toString();
	QByteArray str_id = strUuid.toLocal8Bit();

	//异步删除Actor
	ovr_project::OvrRemoveActor* rmv_actor = new ovr_project::OvrRemoveActor;
	rmv_actor->SetActorUniqueName(str_id.constData());
	ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(rmv_actor);
}

void OvrActorOutlineTree::copy_actor(const QModelIndex& index)
{
	QString strUuid = index.data(CONST_UUID_ROLE).toString();
	QByteArray str_id = strUuid.toLocal8Bit();

	//异步操作 Actor
	ovr_project::OvrCopyActor* cpy_actor = new ovr_project::OvrCopyActor;
	cpy_actor->SetActorUniqueName(str_id.constData());
	ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(cpy_actor);
}

ovr_engine::OvrActor * OvrActorOutlineTree::actor_id_to_ptr(const QString & str_id)
{
	ovr_engine::OvrActor * actor = nullptr;

	ovr_project::OvrProject* prj = ovr_project::OvrProject::GetIns();
	ovr_core::OvrSceneContext * scene_ctx = prj->GetCurrentScene();

	if (str_id.isEmpty())
	{
		ovr_engine::OvrScene* scene = scene_ctx->GetScene();
		actor = scene->GetRoot();
	}
	else
	{
		QByteArray qba_id = str_id.toLocal8Bit();
		actor = scene_ctx->GetActorByUniqueName(qba_id.constData());
	}

	return actor;
}

ActorOutlineItemType OvrActorOutlineTree::get_item_type(ovr_engine::OvrActor * a_actor)
{
	ActorOutlineItemType eItemType = kOITNone;
	/*switch (a_actor->GetType())
	{
	case ovr_engine::kObjectEmptyActor:
		eItemType = ActorOutlineItemType::kOITEmptyActor;
		break;
	case ovr_engine::kObjectMesh:
	{
		ovr_engine::OvrMeshActor* mesh_actor = dynamic_cast<ovr_engine::OvrMeshActor*>(a_actor);
		if (mesh_actor->HasSkeleton())
		{
			eItemType = ActorOutlineItemType::kOITSkeletalcActor;
		}
		else
		{
			eItemType = ActorOutlineItemType::kOITStaticActor;
		}
	}
	break;
	case  ovr_engine::kObjectVideoScreen:
	{
		ovr_engine::OvrVideoScreenActor* screen_actor = dynamic_cast<ovr_engine::OvrVideoScreenActor*>(a_actor);
		switch (screen_actor->GetVideoType())
		{
		case ovr_engine::OvrVideoScreenActor::Video360Box:
			eItemType = ActorOutlineItemType::kOITVideo360boxActor;
			break;
		case ovr_engine::OvrVideoScreenActor::Video360:
			eItemType = ActorOutlineItemType::kOITVideo360Actor;
			break;
		case ovr_engine::OvrVideoScreenActor::Video180:
			eItemType = ActorOutlineItemType::kOITVideo180Actor;
			break;
		case ovr_engine::OvrVideoScreenActor::Video3D:
			eItemType = ActorOutlineItemType::kOITVideo3DActor;
			break;
		case ovr_engine::OvrVideoScreenActor::Video2D:
			eItemType = ActorOutlineItemType::kOITVideo2DActor;
			break;
		default:
			eItemType = ActorOutlineItemType::kOITNone;
			qDebug() << "unknown actor, type: " << a_actor->GetType();
			break;
		}
	}
	break;
	case ovr_engine::kObject3DAudio:
		eItemType = ActorOutlineItemType::kOIT3DAudioActor;
		break;
	case ovr_engine::kObjectLight:
	{
		ovr_engine::OvrLightActor* light_actor = dynamic_cast<ovr_engine::OvrLightActor*>(a_actor);
		ovr_engine::OvrLight& light = light_actor->GetLight();
		switch (light.GetType())
		{
		case ovr_engine::ePointLight:
			eItemType = ActorOutlineItemType::kOITLightPoint;
			break;
		case ovr_engine::eDirectionLight:
			eItemType = ActorOutlineItemType::kOITLightDirect;
			break;
		case ovr_engine::eSpotLight:
			eItemType = ActorOutlineItemType::kOITLightSpot;
			break;
		default:
			eItemType = ActorOutlineItemType::kOITNone;
			qDebug() << "unknown actor, type: " << a_actor->GetType();
			break;
		}
	}
	break;
	case ovr_engine::kObjectText:
		eItemType = ActorOutlineItemType::kOITTextActor;
		break;
	case ovr_engine::kObjectCameraActor:
		eItemType = ActorOutlineItemType::kCameraActor;
		break;
	case ovr_engine::kObjectCollider:
		eItemType = ActorOutlineItemType::kOITColliderActor;
		break;
	case ovr_engine::kObjectParticle:
		eItemType = ActorOutlineItemType::kOITParticleActor;
		break;
	default:
		eItemType = ActorOutlineItemType::kOITNone;
		//assert(false);
		qDebug() << "unknown actor, type: " << a_actor->GetType();
		break;
	}*/

	return eItemType;
}

template<typename A>
inline QTreeWidgetItem * OvrActorOutlineTree::add_actor(ovr_engine::OvrActor * a_actor, A a_parent, int a_index)
{
	ActorOutlineItemType eItemType = get_item_type(a_actor);

	bool b_visible = a_actor->IsVisible();
	const char * actor_name = a_actor->GetDisplayName().GetStringConst();
	const char * actor_uuid = a_actor->GetUniqueName().GetStringConst();
	QString display_name = QString::fromLocal8Bit(actor_name);
	QString str_uuid = QString::fromLocal8Bit(actor_uuid);
	QTreeWidgetItem* cur_item = new QTreeWidgetItem(a_parent, { display_name });
	cur_item->setData(0, CONST_UUID_ROLE, str_uuid);
	cur_item->setData(0, CONST_TYPE_ROLE, eItemType);
	cur_item->setData(0, CONST_VISIBLE_ROLE, b_visible);
	cur_item->setIcon(0, item_icon(eItemType));

	add_item_widget(cur_item, b_visible);

	return cur_item;
}

void OvrActorOutlineTree::add_child_actor(ovr_project::OvrSceneEditor * a_scene_context, ovr_engine::OvrActor * a_actor, QTreeWidgetItem * a_parent)
{
	std::function<void(ovr_project::OvrSceneEditor*, ovr_engine::OvrActor*, QTreeWidgetItem*)> add_actor_fun =
		[this, &add_actor_fun](ovr_project::OvrSceneEditor* scene_context, ovr_engine::OvrActor* a_actor, QTreeWidgetItem* a_parent)
	{
		for (int index = 0, item_index = 0; index < a_actor->GetChildCount(); index++) 
		{
			ovr_engine::OvrActor * child_actor = a_actor->GetChild(index);

			//过滤不用在场景中显示的Actor的  其子actor都不用再显示
			if (!scene_context->IsShowInScene(a_actor))
				continue;

			QTreeWidgetItem* cur_item = add_actor(child_actor, a_parent, item_index++);
			add_actor_fun(scene_context, child_actor, cur_item);
		}
	};

	add_actor_fun(a_scene_context, a_actor, a_parent);
}

QIcon OvrActorOutlineTree::item_icon(int node_type)
{
	QIcon icon_item;
	switch (node_type)
	{
	case ActorOutlineItemType::kOITNone:
		icon_item = QIcon(getImagePath("outline/static_actor.png"));
		break;
	case ActorOutlineItemType::kOITFolder:
		icon_item = QIcon(getImagePath("icon_openfile_min.png"));
		break;
	case ActorOutlineItemType::kOITEmptyActor:
		icon_item = QIcon(getImagePath("outline/static_actor.png"));
		break;
	case ActorOutlineItemType::kOITStaticActor:
	case ActorOutlineItemType::kOITStaticActorGroup:
		icon_item = QIcon(getImagePath("outline/static_actor.png"));
		break;
	case ActorOutlineItemType::kOITSkeletalcActor:
		icon_item = QIcon(getImagePath("outline/skeletal_actor.png"));
		break;
	case ActorOutlineItemType::kCameraActor:
		icon_item = QIcon(getImagePath("outline/icon_camera_min.png"));
		break;
	case ActorOutlineItemType::kOITVideo3DActor:
	case ActorOutlineItemType::kOITVideo2DActor:
	case ActorOutlineItemType::kOITVideo180Actor:
	case ActorOutlineItemType::kOITVideo360Actor:
	case ActorOutlineItemType::kOITVideo360boxActor:
		icon_item = QIcon(getImagePath("outline/icon_video_min.png"));
		break;
	case ActorOutlineItemType::kOIT3DAudioActor:
		icon_item = QIcon(getImagePath("outline/icon_voice_min.png"));
		break;
	case ActorOutlineItemType::kOITLightPoint:
	case ActorOutlineItemType::kOITLightDirect:
	case ActorOutlineItemType::kOITLightSpot:
		icon_item = QIcon(getImagePath("outline/icon_light_min.png"));
		break;
	case ActorOutlineItemType::kOITTextActor:
		icon_item = QIcon(getImagePath("outline/icon_text_min.png"));
		break;
	default:
		icon_item = QIcon(getImagePath("outline/static_actor.png"));
		break;
	}

	return icon_item;
}

void OvrActorOutlineTree::add_item_widget(QTreeWidgetItem * a_item, bool a_visable)
{
	QWidget * base_widget = new QWidget();
	QHBoxLayout * h_box = new QHBoxLayout(base_widget);
	h_box->setContentsMargins(0, 0, 0, 0);

	QSpacerItem *horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
	h_box->addItem(horizontalSpacer);

	QCheckBox * check_box = new QCheckBox();
	check_box->setObjectName(CONST_CHECK_BOX_NAME);
	check_box->setChecked(a_visable);
	check_box->setToolTip(QString::fromLocal8Bit("点击设置"));

	h_box->addWidget(check_box);

	setItemWidget(a_item, 0, base_widget);

	// 同步状态值
	connect(check_box, &QCheckBox::stateChanged, [a_item](int state) {
		bool b_visible = state == Qt::Checked ? true : false;
		a_item->setData(0, CONST_VISIBLE_ROLE, b_visible);
	}	);

	connect(check_box, &QCheckBox::clicked, [this, a_item](bool b_visible) {
		QString str_uuid = a_item->data(0, CONST_UUID_ROLE).toString();

		auto actor = actor_id_to_ptr(str_uuid);
		actor->SetVisible(b_visible);
		a_item->setData(0, CONST_VISIBLE_ROLE, b_visible);

		emit(actor_visible_changed(actor, b_visible));
	});
}
