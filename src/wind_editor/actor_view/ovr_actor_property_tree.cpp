#include "Header.h"
#include "ovr_actor_property_tree.h"

#include "ovr_actor_property_view.h"
#include "ovr_actor_outline_tree.h"

#include "ovr_actor_omponent_view.h"
#include "wx_project/property/ovr_actor_property_group.h"
#include <QTimer>

static OvrActorPropertyTree* g_tree_inst = nullptr;

#define kCloseButtonSize 18
#define kCloseButtonRightMargin 5

#define kTypeField		 Qt::UserRole
#define kRemovedField	(Qt::UserRole + 1)
#define kGroupID		(Qt::UserRole + 2)

static int button_right_margin = 0;

OvrActorPropertyTree* OvrActorPropertyTree::getInst()
{
	return g_tree_inst;
}

OvrActorPropertyTree::OvrActorPropertyTree(QWidget *parent)
	: QTreeWidget(parent)
{
	g_tree_inst = this;

	setItemDelegate(new OvrPropertyItemDelegate);
	
	init_custom_ui();
	set_custom_style_sheet();
	preset_connects();

	QStringList events;
	events << "OnComponentAdd" << "OnComponentDel" << "onGroupReloaded" << "onResItemSelected";

	registerEventListener(events);
}

OvrActorPropertyTree::~OvrActorPropertyTree()
{
	g_tree_inst = nullptr;
}

void OvrActorPropertyTree::init_custom_ui()
{
	this->setSelectionMode(QAbstractItemView::NoSelection);
	this->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);

	this->setHeaderHidden(true);

	QStringList qslHeaderKLabels = {
		QString::fromLocal8Bit("���Ա༭��") ,
	};
	this->setHeaderLabels(qslHeaderKLabels);
}

void OvrActorPropertyTree::set_custom_style_sheet()
{
	QString strStyleSheet = QString::fromLocal8Bit(""
		"OvrActorPropertyTree{background-color:rgb(48, 48, 48); color:rgb(192, 192, 192); border:0px solid rgb(24, 24, 24); border-top:0px; padding-left:0px; show-decoration-selected: 1;}"
		"OvrActorPropertyTree::item{height:28px; border-bottom:1px solid rgb(48, 48, 48); color:rgb(192, 192, 192); outline:3px solid rgb(24, 124, 89);}"
		//"OvrActorPropertyTree::item:has-children{}"
		"OvrActorPropertyTree::item::hover{background-color: rgb(53, 53, 53);}"
		"OvrActorPropertyTree::item:selected{background-color: rgb(64, 64, 64);}"
		"OvrActorPropertyTree::branch{width: 0px; height:24px; border-bottom:1px solid rgb(48, 48, 48);}"
		"OvrActorPropertyTree::branch:hover{background-color:rgb(53, 53, 53);}"
		"OvrActorPropertyTree::branch:selected{background-color:rgb(64, 64, 64);}"
		"OvrActorPropertyTree::item:has-children,QTreeView::branch:has-children{background-color:rgb(39, 39, 39);}"
		"OvrActorPropertyTree::item:!has-children,QTreeView::branch:!has-children{background-color:rgb(39, 39, 39); padding-left: -45px; }"

		"QWidget{background:rgb(48, 48, 48); color:rgb(204, 204, 204); font-family:\"΢���ź�\"; font-size:12px; }"
	);

	QTextStream qtsTemp(&strStyleSheet);
	qtsTemp << "QTreeView::branch:has-children:closed{image:url(" << getImagePath("outline/node_closed.png") << ");}"
		<< "QTreeView::branch:has-children:open{image:url(" << getImagePath("outline/node_opened.png") << ");}";

	this->setStyleSheet(strStyleSheet);
}

void OvrActorPropertyTree::preset_connects()
{
	connect(OvrActorOutlineTree::getInst(), SIGNAL(onActorSelected(ovr_engine::OvrActor*, int)),
		this, SLOT(on_actor_selected(ovr_engine::OvrActor*, int)), Qt::QueuedConnection);
}

void OvrActorPropertyTree::mousePressEvent(QMouseEvent *event)
{
	if (!hitCloseButton(event))
	{
		QTreeWidget::mousePressEvent(event);
	}
}

void OvrActorPropertyTree::mouseDoubleClickEvent(QMouseEvent *event)
{
	if (!hitCloseButton(event))
	{
		QTreeWidget::mouseDoubleClickEvent(event);
	}
}

bool OvrActorPropertyTree::hitCloseButton(QMouseEvent *event)
{
	//is valid top node
	auto index = indexAt(event->pos());
	if (index.isValid() && !index.parent().isValid())
	{
		auto item = static_cast<QTreeWidgetItem*>(index.internalPointer());
		if (item != nullptr)
		{
			auto rc = visualItemRect(item);
			int r = rc.right() - button_right_margin;
			int l = r - kCloseButtonSize;
			int t = rc.center().y() - kCloseButtonSize / 2;

			QRect button_rc(l, t, kCloseButtonSize, kCloseButtonSize);
			if (button_rc.contains(event->pos()))
			{
				qDebug() << "close button pressed";
				event->accept();
				int group_type = item->data(0, kTypeField).toInt();
				root_group->RemoveComponent(group_type);
				return true;
			}
		}
	}
	return false;
}

void OvrActorPropertyTree::clearContext()
{
	clear();

	if (root_group != nullptr)
	{
		delete root_group;
		root_group = nullptr;
	}

	cur_actor = nullptr;
}

void OvrActorPropertyTree::on_actor_selected(ovr_engine::OvrActor* a_actor, int a_type)
{
	clearContext();

	if (a_actor != nullptr)
	{
		cur_actor = a_actor;

		intiComponentsView();

		expandAll();
		setExpanded(model()->index(model()->rowCount() - 1, 0), false);
	}
}

static const QString s_tool_btn_style_sheet = QString::fromLocal8Bit("QToolButton{border:1px solid rgb(76, 76, 76); background:rgb(87, 87, 87); color:rgb(204, 204, 204);}");

void OvrActorPropertyTree::intiComponentsView(bool a_is_load)
{
	if (a_is_load)
	{
		root_group = ovr_project::OvrActorPropertyGroupRoot::CreatePropertyGroups(cur_actor);
	}
	assert(root_group != nullptr);

	for (auto group : root_group->GetGroups())
	{
		addRowForGroup(model()->rowCount(), group);
	}

	addRowButton();
}

void OvrActorPropertyTree::addRowButton()
{
	QTreeWidgetItem * tool_item = new QTreeWidgetItem();

	auto button = new QToolButton();
	button->setText(QString::fromLocal8Bit("�������"));
	button->setStyleSheet(s_tool_btn_style_sheet);

	auto child_item = new QTreeWidgetItem;
	tool_item->addChild(child_item);

	addTopLevelItem(tool_item);

	setItemWidget(tool_item, 0, button);
	
	auto w = new OvrAddComponentBox;

	setItemWidget(child_item, 0, w);

	connect(button, &QToolButton::clicked, [this, tool_item, w](bool) {
		auto button_index = model()->index(model()->rowCount() - 1, 0);
		if (isItemExpanded(tool_item))
		{
			setItemExpanded(tool_item, false);
		}
		else
		{
			w->init(root_group, select_res_name);
			scrollTo(button_index.child(0, 0));
		}
	});

	connect(w->getListWidget(), &QListWidget::currentItemChanged, [this, tool_item, w](QListWidgetItem *current, QListWidgetItem *previous) {
		if (current != nullptr)
		{
			auto group_type = current->data(kTypeField).toInt();
			{
				auto path = select_res_name;
				if (!path.isEmpty())
				{
					OvrProjectManager::getInst()->makeRelateDir(path);
				}
				if ((group_type != ovr_project::kOPTGStaticMesh && group_type != ovr_project::kOPTGSkinMesh) || path.endsWith(".mesh"))
				{
					root_group->createComponent(group_type, path.toLocal8Bit().constData());
				}
			}
			QTimer::singleShot(1, Qt::CoarseTimer, [w]() {
				w->clear();
			});
		}
		setItemExpanded(tool_item, false);
	});
}

void OvrActorPropertyTree::addRowForGroup(int a_row_pos, ovr_project::IOvrActorPropertyGroup* a_group)
{
	auto context_widget = OvrComponentView::CreateComponentItem(a_group);
	if (context_widget != nullptr)
	{
		QString title = QString::fromLocal8Bit(a_group->property_name.GetStringConst());
		QTreeWidgetItem * title_item = new QTreeWidgetItem(QStringList(title));
		title_item->setData(0, kTypeField, a_group->GetGroupType());
		title_item->setData(0, kRemovedField, a_group->CanBeRemoved());
		title_item->setData(0, kGroupID, a_group->property_id.GetStringConst());

		QTreeWidgetItem* context_item = new QTreeWidgetItem;
		title_item->addChild(context_item);

		insertTopLevelItem(a_row_pos, title_item);

		setItemWidget(context_item, 0, context_widget);
	}	
}

void OvrActorPropertyTree::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{
	if (a_event_name == "OnComponentAdd")
	{
		auto actor = static_cast<ovr_engine::OvrActor*>(a_param1.value<void*>());
		auto com = static_cast<ovr_engine::OvrComponent*>(a_param2.value<void*>());
		onAddComponent(actor, com);
	}
	else if (a_event_name == "OnComponentDel")
	{
		auto actor = static_cast<ovr_engine::OvrActor*>(a_param1.value<void*>());
		onRemoveComponent(actor, a_param2.toInt());
	}
	else if (a_event_name == "onGroupReloaded")
	{
		onUpdateGroup(a_param1.toString());
	}
	else if (a_event_name == "onResItemSelected")
	{
		select_res_name = a_param1.toString();
		
		auto button_index = model()->index(model()->rowCount() - 1, 0);
		if (isExpanded(button_index))
		{
			auto item = static_cast<QTreeWidgetItem*>(button_index.child(0, 0).internalPointer());
			auto w = itemWidget(item, 0);
			static_cast<OvrAddComponentBox*>(w)->init(root_group, select_res_name);
		}
	}
}

void OvrActorPropertyTree::onUpdateGroup(const QString& a_group_id)
{
	for (int i = 0; i < topLevelItemCount(); ++i)
	{
		auto item = topLevelItem(i);
		if (a_group_id == item->data(0, kGroupID).toString())
		{
			auto index = model()->index(i, 0);
			setExpanded(index, false);
			setExpanded(index, true);
		}
	}
}

void OvrActorPropertyTree::onRemoveComponent(ovr_engine::OvrActor* a_actor, int a_com_type)
{
	if (root_group->RemoveGroupIncludeComponent(a_com_type))
	{
		clear();
		intiComponentsView(true);
		expandAll();
		setExpanded(model()->index(model()->rowCount() - 1, 0), false);
		scrollTo(model()->index(model()->rowCount() - 1, 0));
	}
}

void OvrActorPropertyTree::onAddComponent(ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_component)
{
	assert(root_group->GetActor() == a_actor);

	auto new_group = root_group->addGroupForComponent(a_component);

	int new_pos = model()->rowCount() - 1;
	addRowForGroup(new_pos, new_group);

	auto new_index = model()->index(new_pos, 0);
	setExpanded(new_index, true);
	scrollTo(model()->index(model()->rowCount() - 1, 0));
}

OvrPropertyItemDelegate::OvrPropertyItemDelegate(QObject *parent) :QStyledItemDelegate(parent)
{
	close_img = new QImage(getImagePath("pop_btn_close.png"));
	button_right_margin = (close_img->width() - kCloseButtonSize) / 2 + kCloseButtonRightMargin;
}

void OvrPropertyItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	QStyledItemDelegate::paint(painter, option, index);

	//top node
	if (!index.parent().isValid())
	{
		auto item = static_cast<QTreeWidgetItem*>(index.internalPointer());
		bool can_be_remove = item->data(0, kRemovedField).toBool();
		if (can_be_remove)
		{
			painter->save();

			QRect rc(QPoint(0,0), close_img->size());
			rc.moveCenter(option.rect.center());
			rc.moveRight(option.rect.right() - kCloseButtonRightMargin);
			painter->drawImage(rc, *close_img);

			painter->restore();
		}
	}
}

void OvrAddComponentBox::paintEvent(QPaintEvent *e)
{
	ApplyWidgetStyleSheet(this);
}

OvrAddComponentBox::OvrAddComponentBox(QWidget* parent, Qt::WindowFlags f):QWidget(parent, f)
{
	setFixedHeight(140);

	auto v_layout = new QVBoxLayout;
	v_layout->setSpacing(2);
	v_layout->setContentsMargins(50, 0, 50, 0);

	auto e = new QLineEdit;
	//e->setStyleSheet("background:rgb(204, 204, 204); color:rgb(20, 20, 20)");
	QString line_edit_style_sheet = QString::fromLocal8Bit(
		"QLineEdit {border:1px solid rgb(34,34,34); background:rgb(36,36,36); color:rgb(204,204,204); selection-background-color: rgb(165, 165, 165); selection-color: rgb(32, 32, 32); border-radius: 2px;}"
	);
	e->setStyleSheet(line_edit_style_sheet);

	e->setPlaceholderText(QString::fromLocal8Bit("������ؼ�������"));
	QAction * action = new QAction(QIcon(getImagePath("icon_search_min.png")), "");
	e->addAction(action, QLineEdit::LeadingPosition);
	v_layout->addWidget(e);

	auto w = new QListWidget;
	w->setAutoFillBackground(true);
	w->setStyleSheet("background:rgb(87, 87, 87); color:rgb(204, 204, 204)");
	v_layout->addWidget(w);

	list_widget = w;

	for (auto group_item : ovr_project::OvrActorPropertyGroupRoot::GetSupportPropertyGroups()->GetGroups())
	{
		auto title = QString::fromLocal8Bit(group_item->property_name.GetStringConst());
		auto data = group_item->GetGroupType();
		
		auto list_item = new QListWidgetItem(title);
		list_item->setTextAlignment(Qt::AlignCenter);
		list_item->setData(kTypeField, data);
		w->addItem(list_item);
	}

	setLayout(v_layout);
}

void OvrAddComponentBox::clear()
{
	list_widget->setCurrentRow(-1);
}

void OvrAddComponentBox::init(ovr_project::OvrActorPropertyGroupRoot* a_root, const QString& a_res_file)
{
	for (int i = 0; i < list_widget->count(); ++i)
	{
		auto item = list_widget->item(i);
		auto group_type = item->data(kTypeField).toInt();
		if (a_root->Contains(group_type))
		{
			item->setForeground(QColor(0, 160, 0));
		}
		else if (group_type == ovr_project::kOPTGStaticMesh || group_type == ovr_project::kOPTGSkinMesh)
		{
			bool is_err = a_res_file.isEmpty() || !a_res_file.endsWith(".mesh") || a_root->Contains(ovr_project::kOPTGStaticMesh) || a_root->Contains(ovr_project::kOPTGSkinMesh);
			is_err ? item->setForeground(QColor(160, 0, 0)) : item->setForeground(QColor(204, 204, 204));
		}
	}
}