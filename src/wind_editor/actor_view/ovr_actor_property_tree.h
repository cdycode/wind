#pragma once

#include <QWidget>
#include <QTreeWidget>

class OvrPropGroup;

namespace ovr_project
{
	class OvrActorPropertyGroupRoot;
	class IOvrActorPropertyGroup;
}

class OvrActorPropertyTree : public QTreeWidget, public OvrEventReceiver
{
	Q_OBJECT

public:
	static OvrActorPropertyTree* getInst();

	OvrActorPropertyTree(QWidget *parent);
	~OvrActorPropertyTree();

	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;
	virtual QString name() override { return "OvrActorPropertyTree"; }

private:
	virtual void mousePressEvent(QMouseEvent *event) override;
	virtual void mouseDoubleClickEvent(QMouseEvent *event) override;
	bool hitCloseButton(QMouseEvent *event);

	void clearContext();
	void init_custom_ui();
	void set_custom_style_sheet();
	void preset_connects();

	void intiComponentsView(bool a_is_load = true);

	void addRowForGroup(int a_row_pos, ovr_project::IOvrActorPropertyGroup* a_group);
	void addRowButton();

	void onAddComponent(ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_component);
	void onRemoveComponent(ovr_engine::OvrActor* a_actor, int a_com_type);
	void onUpdateGroup(const QString& a_group_id);

private Q_SLOTS:
	void on_actor_selected(ovr_engine::OvrActor* a_actor, int a_type);

private:
	ovr_project::OvrActorPropertyGroupRoot* root_group = nullptr;
	ovr_engine::OvrActor* cur_actor = nullptr;
	QString select_res_name;
};

class OvrPropertyItemDelegate : public QStyledItemDelegate
{
public:
	OvrPropertyItemDelegate(QObject *parent = Q_NULLPTR);

	virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

protected:
	QImage* close_img;
};

class OvrAddComponentBox :public QWidget
{
	Q_OBJECT
public:
	explicit OvrAddComponentBox(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());

	void clear();
	const QListWidget* getListWidget() { return list_widget; }

	void init(ovr_project::OvrActorPropertyGroupRoot* a_root, const QString& a_res_file);

protected:
	virtual void OvrAddComponentBox::paintEvent(QPaintEvent *e) override;
	QListWidget* list_widget = nullptr;
};