#pragma once

#include <QTreeWidget>

enum ActorOutlineItemType {
	kOITNone, kOITFolder, kOITEmptyActor, kOITStaticActor, kOITStaticActorGroup, kOITSkeletalcActor, kCameraActor,

	//av
	kOITVideo3DActor, kOITVideo2DActor, kOITVideo180Actor, kOITVideo360Actor, kOITVideo360boxActor, kOIT3DAudioActor,

	//light
	kOITLightPoint, kOITLightDirect, kOITLightSpot,

	// text
	kOITTextActor,

	kOITColliderActor,
	kOITParticleActor,
};
class QTreeWidgetItem;

class OvrActorOutlineTree : public QTreeWidget, public OvrEventReceiver
{
	Q_OBJECT

public:
	OvrActorOutlineTree(QWidget *parent);
	~OvrActorOutlineTree();

	static OvrActorOutlineTree* getInst();

	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;
	virtual QString name() override { return "OvrActorOutlineTree"; }

signals:
	void onActorSelected(ovr_engine::OvrActor* a_actor, int a_type);
	void onUpdateTransformToProp(ovr_engine::OvrActor* a_actor);
	void actor_visible_changed(ovr_engine::OvrActor * a_actor, bool a_visible);

private:
	void init_custom_ui();
	void set_custom_style_sheet();

protected:

	virtual void currentChanged(const QModelIndex &current, const QModelIndex &previous) override;
	virtual void mouseReleaseEvent(QMouseEvent *event) override;
	virtual void dropEvent(QDropEvent *event)override;

	virtual QStringList mimeTypes() const override;
	virtual QMimeData * mimeData(const QList<QTreeWidgetItem *> items) const override;

private slots:
	void onUpdatedTransform(QString selected_id);
	void onSetedSelectedItem(QString str_id);
	void onActorAdded(QString a_actor_id);
	void onActorRemoved(QString a_actor_id);

public:
	QModelIndex get_item_index(const QString &str_id);
	void update_eye_status(const QString & str_uuid, bool is_show);
	void update_display_name(const QString & str_uuid, const QString& str_name);

private:
	void onProjectOpen(bool is_ok, QString a_prj_dir);
	void onCurrentSceneChanged();

	void refreshActorTree();

	void update_eye_status(const QModelIndex &current, bool is_show);
	void update_display_name(const QModelIndex &current, const QString& str_name);

	QMenu* create_sub_menu(const QModelIndex & index);

	void remove_actor(const QModelIndex& index);
	void copy_actor(const QModelIndex& index);
	ovr_engine::OvrActor * actor_id_to_ptr(const QString & str_id);
	ActorOutlineItemType get_item_type(ovr_engine::OvrActor * a_actor);

	template<typename A> QTreeWidgetItem* add_actor(ovr_engine::OvrActor* a_actor, A a_parent, int a_index);

	void add_child_actor(ovr_project::OvrSceneEditor* a_scene_context, ovr_engine::OvrActor* a_actor, QTreeWidgetItem* a_parent);
	QIcon item_icon(int node_type);

	void add_item_widget(QTreeWidgetItem* a_item, bool a_visable);

private:
	static const int CONST_UUID_ROLE;
	static const int CONST_TYPE_ROLE;
	static const int CONST_VISIBLE_ROLE;

	const QString CONST_MIME_DATA_TYPE = "actor_tree_custom_data";
	const QString CONST_CHECK_BOX_NAME = "eye_check_box_object";

	QMenu* right_memu = nullptr;

};
