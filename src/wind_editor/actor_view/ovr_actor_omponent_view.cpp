#include "Header.h"
#include "ovr_actor_omponent_view.h"

#include "wx_project/property/i_ovr_actor_property_group.h"
#include "wx_project/property/ovr_actor_property.h"
#include "../material/ovr_material_editor.h"

#include "ovr_prop_label.h"

#include <QSpacerItem>
#include <QPlainTextEdit>
#include <QButtonGroup>
#include <QRadioButton>
#include <QFontComboBox>
#include <QColorDialog>

#define OVR_PROP_ROW_MARGIN 5
#define OVR_PROP_ROW_MARGIN1 10
#define OVR_PROP_ROW_INTER_SPACE 5

static const QString cs_reg_exp_filter = "^[+\\-]{0,1}\\d*\\.{0,1}\\d*$";
static const QString s_tool_btn_style_sheet = "QToolButton{border:1px solid rgb(76, 76, 76); background:rgb(87, 87, 87); color:rgb(204, 204, 204);}";
const QString s_text_edit_style_sheet = "QPlainTextEdit{max-height:120px; border:1px solid rgb(76,76,76); background:rgb(62,62,62); color:rgb(204,204,204); selection-background-color:rgb(165, 165, 165); selection-color:rgb(32, 32, 32);}";


OvrComponentView* OvrComponentView::CreateComponentItem(ovr_project::IOvrActorPropertyGroup* a_group)
{
	auto item = new OvrComponentView;
	if (item->init(a_group))
	{
		return item;
	}
	return nullptr;
}

OvrComponentView::OvrComponentView(QWidget* parent /* = Q_NULLPTR */, Qt::WindowFlags f /* = Qt::WindowFlags() */)
{
	base_layout = new QVBoxLayout(this);
	base_layout->setSpacing(0);
	base_layout->setMargin(0);

	connect(this, &OvrComponentView::onReload, this, &OvrComponentView::reload, Qt::QueuedConnection);
}

OvrComponentView::~OvrComponentView()
{
}

bool OvrComponentView::init(ovr_project::IOvrActorPropertyGroup* a_group)
{
	assert(a_group != nullptr);

	bind_group = a_group;
	
	if (bind_group->property_id == "group_trans")
	{
		is_transform = true;
		registerEventListener("OnUpdateTransform");
	}
	else if (bind_group->property_id == "group_static_mesh" || bind_group->property_id == "group_skin_mesh" || bind_group->property_id == "group_mat")
	{
		registerEventListener("onResItemSelected");
	}
	else if (bind_group->property_id == "group_video")
	{
		registerEventListener("OnComponentChangeVideoType");
		registerEventListener("OnComponentChangeMovieType");
	}

	return init();
}

bool OvrComponentView::init()
{
	pane = new QWidget;

	pane_layout = new QGridLayout(pane);
	pane_layout->setContentsMargins(OVR_PROP_ROW_MARGIN1, OVR_PROP_ROW_MARGIN, OVR_PROP_ROW_MARGIN, OVR_PROP_ROW_MARGIN);
	pane_layout->setVerticalSpacing(OVR_PROP_ROW_INTER_SPACE);
	pane_layout->setHorizontalSpacing(10);
	pane_layout->setColumnStretch(1, 100);

	for (auto item : bind_group->GetProperties())
	{
		addField(item, false);
	}

	base_layout->addWidget(pane);

	return true;
}

void OvrComponentView::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{
	if (a_event_name == "OnUpdateTransform")
	{
		if (bind_group->GetActor()->GetUniqueName() == a_param1.toString().toLocal8Bit())
		{
			OnUpdateTransform();
		}
	}
	else if (a_event_name == "onResItemSelected")
	{
		select_res_name = a_param1.toString();
		qDebug() << "res item selected, path: " << select_res_name;
		emit onResFileSelected(select_res_name);
	}
	else if (a_event_name == "OnComponentChangeVideoType" || a_event_name == "OnComponentChangeMovieType")
	{
		auto a_actor = static_cast<ovr_engine::OvrActor*>(a_param1.value<void*>());
		auto a_component = static_cast<ovr_engine::OvrComponent*>(a_param2.value<void*>());
		updateVideoValue(a_actor, a_component);
	}
}

void OvrComponentView::OnUpdateTransform()
{
	assert(is_transform);

	qDebug() << " OvrComponentView::OnUpdateTransform";

	bind_group->Reload();

	UpdateTransField(bind_group->GetProperty("pos"));
	UpdateTransField(bind_group->GetProperty("rotate"));
	UpdateTransField(bind_group->GetProperty("scale"));
}

void OvrComponentView::updateVideoValue(ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_component)
{
	if (bind_group->GetActor() == a_actor && a_component == bind_group->getComponent())
	{
		reload();
	}
}

void OvrComponentView::reload()
{
	if (bind_group->IsNeedReload())
	{
		bool is_layout_changed = bind_group->IsLayoutChanged();
		bind_group->Reload();
		reloadComponent(is_layout_changed);
	}
}

void OvrComponentView::UpdateTransField(ovr_project::IOvrActorProperty* a_field)
{
	auto field = static_cast<ovr_project::OvrActorPropertyVector3f*>(a_field);
	auto value = field->GetValue();

	auto& widgets = rts[a_field->property_id.GetStringConst()].widgets;

	for (int pos = 0; pos < 3; ++pos)
	{
		widgets[pos]->setText(QString::number(value[pos]));
	}
}

void OvrComponentView::addField(ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group)
{
	int row_count = pane_layout->rowCount();

	//lable
	addFieldTitle(row_count, a_field->property_name, a_is_sub_group);
	auto& atts = a_field->GetAttributes();
	atts_flag = atts.size() > 0;
	
	//value
	switch (a_field->property_type)
	{
	case ovr_project::kOPTBool:
		addFieldBool(row_count, a_field, a_is_sub_group);
		break;
	case ovr_project::kOPTFloat:
		a_field->GetAttribute("is_slide") == "true" ? addFieldSlide(row_count, a_field, a_is_sub_group) : addFieldFloat(row_count, a_field, a_is_sub_group);
		break;
	case ovr_project::kOPTVector2f:
		a_field->GetAttribute("is_align") == "true" ? addFieldAlign(row_count, a_field, a_is_sub_group) : addFieldVector2f(row_count, a_field, a_is_sub_group);
		break;
	case ovr_project::kOPTVector3f:
		addFieldVector3f(row_count, a_field, a_is_sub_group);
		break;
	case ovr_project::kOPTVector4f:
		a_field->GetAttribute("format") == "color" ? addFieldColor(row_count, a_field, a_is_sub_group) : addFieldVector4f(row_count, a_field, a_is_sub_group);
		break;
	case ovr_project::kOPTString:
		a_field->GetAttribute("is_text") == "true" ? addFieldText(row_count, a_field, a_is_sub_group) : addFieldString(row_count, a_field, a_is_sub_group);
		break;
	case ovr_project::kOPTComboBox:
		addFieldCombox(row_count, a_field, a_is_sub_group);
		break;
	case ovr_project::kOPTRes:
		a_field->GetAttribute("res_type") == "img" ? addFieldResImg(row_count, a_field, a_is_sub_group) : addFieldRes(row_count, a_field, a_is_sub_group);
		break;
	case ovr_project::kOPTGroup:
		pane_layout->addItem(new QSpacerItem(1,1, QSizePolicy::Maximum, QSizePolicy::Minimum), row_count, 1);
		addSubGroup(static_cast<ovr_project::IOvrActorPropertyGroup*>(a_field));
		break;
	default:
		assert(false);
		break;
	}

	if (atts_flag)
	{
		qDebug() << "atts tobe done for " << a_field->property_type_name.GetStringConst();
		for (auto att : atts)
		{
			qDebug() << "\t" << QString::fromLocal8Bit(att.first.GetStringConst())
				<< " : "
				<< QString::fromLocal8Bit(att.second.GetStringConst());
		}
		qDebug() << "";
	}
}

void OvrComponentView::addSubGroup(ovr_project::IOvrActorPropertyGroup* a_group)
{
	for (auto item : a_group->GetProperties())
	{
		addField(item, true);
	}
}

void OvrComponentView::addFieldBool(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group)
{
	auto field = static_cast<ovr_project::OvrActorPropertyBool*>(a_field);
	auto value = field->GetValue();

	auto w = new QCheckBox;
	pane_layout->addWidget(w, a_row_count, 1, Qt::AlignLeft | Qt::AlignVCenter);
	w->setChecked(value);

	connect(w, &QCheckBox::clicked, [this, w, field](bool a_checked) {
		qDebug() << QString::fromLocal8Bit(field->property_name.GetStringConst()) << " clicked " << a_checked;
		field->SetValue(a_checked);
	});
}

void OvrComponentView::addFieldString(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group)
{
	auto field = static_cast<ovr_project::OvrActorPropertyString*>(a_field);
	auto value = field->GetValue();

	QLineEdit* w = new QLineEdit;
	pane_layout->addWidget(w, a_row_count, 1);
	w->setText(QString::fromLocal8Bit(value.GetStringConst()));

	connect(w, &QLineEdit::editingFinished, [this, w, field]() {
		qDebug() << QString::fromLocal8Bit(field->property_name.GetStringConst()) << " edited " << w->text();
		if (field->SetValue(w->text().toLocal8Bit().constData()))
		{
			if (bind_group->property_id == "group_base" && field->property_id == "name")
			{
				auto actor_id = QString::fromLocal8Bit(bind_group->GetActor()->GetUniqueName().GetStringConst());
				notifyEvent("onUpdateActorName", actor_id, w->text());
			}
		}
	});
}

void OvrComponentView::addFieldText(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group)
{
	auto field = static_cast<ovr_project::OvrActorPropertyString*>(a_field);
	auto value = field->GetValue();

	atts_flag = false;

	QPlainTextEdit* w = new QPlainTextEdit(this);
	w->setStyleSheet(s_text_edit_style_sheet);
	pane_layout->addWidget(w, a_row_count, 1);
	w->setPlainText(QString::fromLocal8Bit(value.GetStringConst()));

	connect(w, &QPlainTextEdit::textChanged, [this, w, field]() {
		field->SetValue(w->toPlainText().toLocal8Bit().constData());
	});
}

void OvrComponentView::addFieldFloat(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group)
{
	auto field = static_cast<ovr_project::OvrActorPropertyFloat*>(a_field);
	auto value = field->GetValue();
	
	QLineEdit* w = new QLineEdit;
	pane_layout->addWidget(w, a_row_count, 1);
	w->setText(QString::number(value));

	connect(w, &QLineEdit::editingFinished, [this, w, field]() {
		qDebug() << QString::fromLocal8Bit(field->property_name.GetStringConst()) << " edited " << w->text();
		if (w->text().isEmpty())
		{
			auto value = field->GetValue();
			w->setText(QString::number(value));
			return;
		}
		field->SetValue(w->text().toFloat());
	});
}

void OvrComponentView::addFieldSlide(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group)
{
	auto field = static_cast<ovr_project::OvrActorPropertyFloat*>(a_field);
	auto value = field->GetValue();
	float min_v = 0, max_v = 1;
	field->GetRange(min_v, max_v);

	atts_flag = false;

	QHBoxLayout * h_layout = new QHBoxLayout;
	h_layout->setSpacing(OVR_PROP_ROW_INTER_SPACE);

	auto slider = new QSlider(Qt::Orientation::Horizontal, this);
	slider->setRange((int)min_v, (int)max_v);
	slider->setSingleStep(1);
	slider->setMinimumWidth(130);
	slider->setValue((int)value);
	h_layout->addWidget(slider);

	auto w = new QLineEdit(this);
	w->setMaximumWidth(50);
	//slider->setMinimumSize(QSize(32, 32));
	QRegExp regExpPort = QRegExp(cs_reg_exp_filter);
	QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExpPort);
	w->setInputMethodHints(Qt::ImhDigitsOnly);
	w->setMaxLength(10);
	w->setValidator(portRegExpValidator);
	w->setText(QString::number(value));
	h_layout->addWidget(w);

	connect(slider, &QSlider::valueChanged, [this, w, field](int a_value) {
		auto value = field->GetValue();
		if (a_value != (int)value)
		{
			if (field->SetValue(a_value))
			{
				w->setText(QString::number(a_value));
			}
		}
	});

	connect(w, &QLineEdit::editingFinished, [this, w, field, slider]() {
		qDebug() << QString::fromLocal8Bit(field->property_name.GetStringConst()) << " edited " << w->text();
		if (w->text().isEmpty())
		{
			auto value = field->GetValue();
			w->setText(QString::number(value));
			return;
		}
		auto value = w->text().toFloat();
		if (!field->SetValue(value))
		{
			auto value = field->GetValue();
			w->setText(QString::number(value));
			return;
		}
		slider->setValue((int)value);
	});

	pane_layout->addLayout(h_layout, a_row_count, 1);
}


void OvrComponentView::addFieldVector3f(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group)
{
	auto field = static_cast<ovr_project::OvrActorPropertyVector3f*>(a_field);
	auto value = field->GetValue();

	QHBoxLayout * h_layout = new QHBoxLayout();
	h_layout->setSpacing(OVR_PROP_ROW_INTER_SPACE);

	static const char* small_titles[] = { "X", "Y", "Z" };
	static int count = sizeof(small_titles) / sizeof(const char*);

	for (int pos = 0; pos < count; ++pos)
	{
		auto label = new QLabel(QString::fromLocal8Bit(small_titles[pos]));
		h_layout->addWidget(label);

		auto w = new QLineEdit;
		h_layout->addWidget(w);
		w->setText(QString::number(value[pos]));

		QRegExp regExpPort = QRegExp(cs_reg_exp_filter);
		QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExpPort);
		w->setInputMethodHints(Qt::ImhDigitsOnly);
		w->setMaxLength(10);
		w->setValidator(portRegExpValidator);

		connect(w, &QLineEdit::editingFinished, [this, w, field, pos]() {
			qDebug() << QString::fromLocal8Bit(field->property_name.GetStringConst()) << " edited " << pos << ": " << w->text();
			if (w->text().isEmpty())
			{
				auto value = field->GetValue();
				w->setText(QString::number(value[pos]));
				return;
			}
			auto cur_value = field->GetValue();
			cur_value[pos] = w->text().toFloat();
			field->SetValue(cur_value);
		});
		if (is_transform)
		{
			rts[a_field->property_id.GetStringConst()].widgets.append(w);
		}
	}

	pane_layout->addLayout(h_layout, a_row_count, 1);
}

void OvrComponentView::addFieldVector4f(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group)
{
	auto field = static_cast<ovr_project::OvrActorPropertyVector4f*>(a_field);
	auto value = field->GetValue();

	QHBoxLayout * h_layout = new QHBoxLayout();
	h_layout->setSpacing(OVR_PROP_ROW_INTER_SPACE);

	static const char* small_titles[] = {"X", "Y", "Z", "W"};
	static int count = sizeof(small_titles) / sizeof(const char*);

	for (int pos = 0; pos < count; ++pos)
	{
		auto label = new QLabel(QString::fromLocal8Bit(small_titles[pos]));
		h_layout->addWidget(label);

		auto w = new QLineEdit;
		h_layout->addWidget(w);
		w->setText(QString::number(value[pos]));

		QRegExp regExpPort = QRegExp(cs_reg_exp_filter);
		QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExpPort);
		w->setInputMethodHints(Qt::ImhDigitsOnly);
		w->setMaxLength(10);
		w->setValidator(portRegExpValidator);

		connect(w, &QLineEdit::editingFinished, [this, w, field, pos]() {
			qDebug() << QString::fromLocal8Bit(field->property_name.GetStringConst()) << " edited " << pos << ": " << w->text();
			if (w->text().isEmpty())
			{
				auto value = field->GetValue();
				w->setText(QString::number(value[pos]));
				return;
			}
			auto cur_value = field->GetValue();
			cur_value[pos] = w->text().toFloat();
			field->SetValue(cur_value);
		});
	}

	pane_layout->addLayout(h_layout, a_row_count, 1);
}

QColor Vector4f2Color(const OvrVector4& a_value)
{
	return QColor(a_value._x * 255, a_value._y * 255, a_value._z * 255, a_value._w * 255);
}

OvrVector4 Color2Vector4f(const QColor& a_color)
{
	OvrVector4 v4f;

	v4f._x = a_color.red()	/ 255.0f;
	v4f._y = a_color.green() / 255.0f;
	v4f._z = a_color.blue() / 255.0f;
	v4f._w = a_color.alpha() / 255.0f;

	return v4f;
}

void OvrComponentView::addFieldColor(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group)
{
	auto field = static_cast<ovr_project::OvrActorPropertyVector4f*>(a_field);
	auto value = field->GetValue();

	atts_flag = false;

	OvrColorLabel* w = new OvrColorLabel(this);
	pane_layout->addWidget(w, a_row_count, 1);
	w->SetCurrentColor(value);

	connect(w, &OvrColorLabel::onMouseDbClick, [this, w, field]() {
		changeFieldColor(w, field);
	});
}

void OvrComponentView::addFieldVector2f(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group)
{
	auto field = static_cast<ovr_project::OvrActorPropertyVector3f*>(a_field);
	auto value = field->GetValue();

	QHBoxLayout * h_layout = new QHBoxLayout();
	h_layout->setSpacing(OVR_PROP_ROW_INTER_SPACE);

	static const char* small_titles[] = { "X", "Y"};
	static int count = sizeof(small_titles) / sizeof(const char*);

	for (int pos = 0; pos < count; ++pos)
	{
		auto label = new QLabel(QString::fromLocal8Bit(small_titles[pos]));
		h_layout->addWidget(label);

		auto w = new QLineEdit;
		h_layout->addWidget(w);
		w->setText(QString::number(value[pos]));

		QRegExp regExpPort = QRegExp(cs_reg_exp_filter);
		QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExpPort);
		w->setInputMethodHints(Qt::ImhDigitsOnly);
		w->setMaxLength(10);
		w->setValidator(portRegExpValidator);

		connect(w, &QLineEdit::editingFinished, [this, w, field, pos]() {
			qDebug() << QString::fromLocal8Bit(field->property_name.GetStringConst()) << " edited " << pos << ": " << w->text();
			if (w->text().isEmpty())
			{
				auto value = field->GetValue();
				w->setText(QString::number(value[pos]));
				return;
			}
			auto cur_value = field->GetValue();
			cur_value[pos] = w->text().toFloat();
			field->SetValue(cur_value);
		});
	}

	pane_layout->addLayout(h_layout, a_row_count, 1);
}

void OvrComponentView::addFieldAlign(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group)
{
	auto field = static_cast<ovr_project::OvrActorPropertyVector2f*>(a_field);
	auto value = field->GetValue();

	atts_flag = false;

	QFrame * frame_h = new QFrame(this);
	frame_h->setStyleSheet(
		"QFrame{background-color:rgb(60, 60, 60); border-radius:2px}"
		//"QRadioButton{background:rgb(34, 34, 34);}"
		//"QToolTip{color:rgb(48,48,48); background-color:rgb(204,204,204);}"
	);

	QHBoxLayout * frame_layout_h = new QHBoxLayout(frame_h);
	frame_layout_h->setSpacing(4);
	frame_layout_h->setContentsMargins(6, 2, 6, 2);

	OvrButtonGroup * horiz_margin;
	OvrButtonGroup * vert_margin;

	std::array<QRadioButton*, 3> horiz_btns;
	std::array<QRadioButton*, 3> vert_btns;

	horiz_btns[0] = new QRadioButton(frame_h);
	horiz_btns[1] = new QRadioButton(frame_h);
	horiz_btns[2] = new QRadioButton(frame_h);

	auto generate_radio_btn_style_sheet = [](
		const char * unchecked_normal, const char * unchecked_hover, const char * unchecked_pressed,
		const char * checked_normal, const char * checked_hover, const char * checked_pressed
		)->QString {
		QString strStyleSheet;
		QTextStream qtsTemp(&strStyleSheet);
		qtsTemp << "QRadioButton{spacing:0px;}"
			<< "QRadioButton::indicator {width: 20px; height: 20px;}"
			<< "QRadioButton{background-color:rgb(60, 60, 60);}"
			<< "QRadioButton::indicator::unchecked {image:url(" << getImagePath(unchecked_normal) << ");}"
			<< "QRadioButton::indicator::unchecked:hover {image:url(" << getImagePath(unchecked_hover) << ");}"
			<< "QRadioButton::indicator::unchecked:pressed {image:url(" << getImagePath(unchecked_pressed) << ");}"
			<< "QRadioButton::indicator::checked {image:url(" << getImagePath(checked_normal) << ");}"
			<< "QRadioButton::indicator::checked:hover {image:url(" << getImagePath(checked_hover) << ");}"
			<< "QRadioButton::indicator::checked:pressed {image:url(" << getImagePath(checked_pressed) << ");}"
			;
		return strStyleSheet;
	};

	QString strStyleSheetHorizLeft = generate_radio_btn_style_sheet(
		"align_btn/icon_left_nor.png",
		"align_btn/icon_left_highlight.png",
		"align_btn/icon_left_sel.png",
		"align_btn/icon_left_sel.png",
		"align_btn/icon_left_sel.png",
		"align_btn/icon_left_sel.png");
	horiz_btns[0]->setStyleSheet(strStyleSheetHorizLeft);

	QString strStyleSheetHorizCenter = generate_radio_btn_style_sheet(
		"align_btn/icon_horizon_nor.png",
		"align_btn/icon_horizon_highlight.png",
		"align_btn/icon_horizon_sel.png",
		"align_btn/icon_horizon_sel.png",
		"align_btn/icon_horizon_sel.png",
		"align_btn/icon_horizon_sel.png");
	horiz_btns[1]->setStyleSheet(strStyleSheetHorizCenter);

	QString strStyleSheetHorizRight = generate_radio_btn_style_sheet(
		"align_btn/icon_right_nor.png",
		"align_btn/icon_right_highlight.png",
		"align_btn/icon_right_sel.png",
		"align_btn/icon_right_sel.png",
		"align_btn/icon_right_sel.png",
		"align_btn/icon_right_sel.png");
	horiz_btns[2]->setStyleSheet(strStyleSheetHorizRight);

	horiz_btns[0]->setToolTip(QString::fromLocal8Bit("左对齐"));
	horiz_btns[1]->setToolTip(QString::fromLocal8Bit("水平居中"));
	horiz_btns[2]->setToolTip(QString::fromLocal8Bit("右对齐"));

	frame_layout_h->addWidget(horiz_btns[0]);
	frame_layout_h->addWidget(horiz_btns[1]);
	frame_layout_h->addWidget(horiz_btns[2]);

	horiz_margin = new OvrButtonGroup(frame_h);
	horiz_margin->addButton(horiz_btns[0], 0);
	horiz_margin->addButton(horiz_btns[1], 1);
	horiz_margin->addButton(horiz_btns[2], 2);
	//horiz_btns[(int)value._x]->setChecked(true);

	QFrame *frame_v = new QFrame(this);
	frame_v->setStyleSheet(
		"QFrame{background-color:rgb(60, 60, 60); border-radius:2px}"
		//"QRadioButton{background-color:rgb(60, 60, 60);}"
		//"QToolTip{color:rgb(48,48,48); background-color:rgb(204,204,204);}"
	);
	QHBoxLayout * frame_layout_v = new QHBoxLayout(frame_v);
	frame_layout_v->setSpacing(4);
	frame_layout_v->setContentsMargins(6, 2, 6, 2);

	vert_btns[0] = new QRadioButton(frame_v);
	vert_btns[1] = new QRadioButton(frame_v);
	vert_btns[2] = new QRadioButton(frame_v);

	QString strStyleSheetVertTop = generate_radio_btn_style_sheet(
		"align_btn/icon_top_nor.png",
		"align_btn/icon_top_highlight.png",
		"align_btn/icon_top_sel.png",
		"align_btn/icon_top_sel.png",
		"align_btn/icon_top_sel.png",
		"align_btn/icon_top_sel.png");
	vert_btns[0]->setStyleSheet(strStyleSheetVertTop);

	QString strStyleSheetVertCetner = generate_radio_btn_style_sheet(
		"align_btn/icon_vertical_nor.png",
		"align_btn/icon_vertical_highlight.png",
		"align_btn/icon_vertical_sel.png",
		"align_btn/icon_vertical_sel.png",
		"align_btn/icon_vertical_sel.png",
		"align_btn/icon_vertical_sel.png");
	vert_btns[1]->setStyleSheet(strStyleSheetVertCetner);

	QString strStyleSheetVertBottom = generate_radio_btn_style_sheet(
		"align_btn/icon_bottom_nor.png",
		"align_btn/icon_bottom_highlight.png",
		"align_btn/icon_bottom_sel.png",
		"align_btn/icon_bottom_sel.png",
		"align_btn/icon_bottom_sel.png",
		"align_btn/icon_bottom_sel.png");
	vert_btns[2]->setStyleSheet(strStyleSheetVertBottom);

	vert_btns[0]->setToolTip(QString::fromLocal8Bit("顶端对齐"));
	vert_btns[1]->setToolTip(QString::fromLocal8Bit("垂直居中"));
	vert_btns[2]->setToolTip(QString::fromLocal8Bit("底端对齐"));

	frame_layout_v->addWidget(vert_btns[0]);
	frame_layout_v->addWidget(vert_btns[1]);
	frame_layout_v->addWidget(vert_btns[2]);

	vert_margin = new OvrButtonGroup(frame_v);
	vert_margin->addButton(vert_btns[0], 0);
	vert_margin->addButton(vert_btns[1], 1);
	vert_margin->addButton(vert_btns[2], 2);
	//vert_btns[(int)value._y]->setChecked(true);

	QHBoxLayout * h_layout = new QHBoxLayout();
	h_layout->setSpacing(OVR_PROP_ROW_INTER_SPACE);

	QSpacerItem* rightSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

	h_layout->addWidget(frame_h);
	h_layout->addWidget(frame_v);
	h_layout->addItem(rightSpacer);

	pane_layout->addLayout(h_layout, a_row_count, 1);
	connect(horiz_margin, &OvrButtonGroup::alignButtonClicked,  [this, horiz_margin, field](int index){
		auto value = field->GetValue();
		value._x = index;
		field->SetValue(value);
	});
	connect(vert_margin, &OvrButtonGroup::alignButtonClicked, [this, horiz_margin, field](int index) {
		auto value = field->GetValue();
		value._y = index;
		field->SetValue(value);
	});

}

void OvrComponentView::addFieldCombox(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group)
{
	auto field = static_cast<ovr_project::OvrActorPropertyComboBox*>(a_field);
	auto value = field->GetValue();

	if (a_field->GetAttribute("is_font") == "true")
	{
		QComboBox* w = new QFontComboBox;
		pane_layout->addWidget(w, a_row_count, 1);

		int pos = 0, sel_index = -1;
		for (auto& item : field->GetItems())
		{
			w->addItem(QString::fromLocal8Bit(item.item_name.GetStringConst()), item.item_id);

			if (sel_index == -1 && item.item_id == value)
			{
				sel_index = pos;
			}
			++pos;
		}
		w->setCurrentIndex(sel_index);

		/*connect(w, &OvrComboxEx::currentChanged, [this, w, field](int a_index) {
			auto value = w->itemData(a_index).toInt();
			auto text = w->itemText(a_index);
			qDebug() << QString::fromLocal8Bit(field->property_name.GetStringConst()) << " edited " << ": " << text;
			if (field->SetValue(value))
			{
				if (bind_group->IsNeedReload() && !bind_group->IsAsyncLoad())
				{
					emit onReload();
				}
			}
		});*/
	}
	else
	{
		OvrComboxEx* w = new OvrComboxEx;
		pane_layout->addWidget(w, a_row_count, 1);

		int pos = 0, sel_index = -1;
		for (auto& item : field->GetItems())
		{
			w->addItem(QString::fromLocal8Bit(item.item_name.GetStringConst()), item.item_id);

			if (sel_index == -1 && item.item_id == value)
			{
				sel_index = pos;
			}
			++pos;
		}
		w->setCurrentIndex(sel_index);

		connect(w, &OvrComboxEx::currentChanged, [this, w, field](int a_index) {
			auto value = w->itemData(a_index).toInt();
			auto text =  w->itemText(a_index);
			qDebug() << QString::fromLocal8Bit(field->property_name.GetStringConst()) << " edited " << ": " << text;
			if (field->SetValue(value))
			{
				if (bind_group->IsNeedReload() && !bind_group->IsAsyncLoad())
				{
					emit onReload();
				}
			}
		});
	}
}

void OvrComponentView::reloadComponent(bool a_is_layout_changed)
{
	base_layout->removeWidget(pane);
	delete pane;
	pane = nullptr;

	init();

	if (a_is_layout_changed)
	{
		notifyEvent("onGroupReloaded", QString::fromLocal8Bit(bind_group->property_id.GetStringConst()), 0);
	}
}

void OvrComponentView::addFieldRes(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group)
{
	auto field = static_cast<ovr_project::OvrActorPropertyRes*>(a_field);
	auto value = field->GetValue();
	QString value_text = QString::fromLocal8Bit(value.GetStringConst());
	qDebug() << "res_type: " << field->GetAttribute("res_type").GetStringConst() << ", value: " << value_text;

	atts_flag = false;

	QHBoxLayout * h_layout = new QHBoxLayout();
	h_layout->setSpacing(OVR_PROP_ROW_INTER_SPACE);

	auto w = new OvrLineEditEx(value_text);
	h_layout->addWidget(w);

	if (field->GetAttribute("res_type") == "mat")
	{
		connect(w, &OvrLineEditEx::onMouseDbClick, [this, field](){ showMatEditor(field); });
	}

	auto apply_button = new QToolButton();
	apply_button->setText(QString::fromLocal8Bit("应用"));
	apply_button->setStyleSheet(s_tool_btn_style_sheet);
	apply_button->setDisabled(true);
	h_layout->addWidget(apply_button, Qt::AlignLeft | Qt::AlignVCenter);

	connect(this, &OvrComponentView::onResFileSelected, [this, apply_button, field](QString a_file_path) {
		enableApplyButton(apply_button, field, a_file_path);
	});

	connect(apply_button, &QToolButton::clicked, [this, w, field](bool) {
		auto path = select_res_name;
		OvrProjectManager::getInst()->makeRelateDir(path);
		if (path != w->text())
		{
			w->setTextEx(path);
			field->SetValue(path.toLocal8Bit().constData());
		}
	});

	auto locate_button = new QToolButton();
	locate_button->setText(QString::fromLocal8Bit("定位"));
	locate_button->setStyleSheet(s_tool_btn_style_sheet);
	h_layout->addWidget(locate_button, Qt::AlignLeft | Qt::AlignVCenter);
	connect(locate_button, &QToolButton::clicked, [this,w](bool) {
		if (!w->text().isEmpty())
		{
			notifyEvent("onLocateRes", w->text(), 0);
		}
	});

	pane_layout->addLayout(h_layout, a_row_count, 1);
}

void OvrComponentView::showMatEditor(ovr_project::IOvrActorProperty* a_field)
{
	auto field = static_cast<ovr_project::OvrActorPropertyRes*>(a_field);
	auto value = field->GetValue();

	if (value.GetStringSize() > 0)
	{
		OvrMaterialEditor matDlg(QString::fromLocal8Bit(value.GetStringConst()), OvrHelper::getInst()->getMainWindow());
		matDlg.exec();
	}
}

void OvrComponentView::enableApplyButton(QToolButton* a_button, ovr_project::IOvrActorProperty* a_field, QString a_file_path)
{
	QFileInfo fi(a_file_path);
	if (fi.exists() && fi.isFile())
	{
		if (a_field->GetAttribute("res_type") == "mesh")
		{
			a_button->setEnabled(a_file_path.endsWith(".mesh"));
			return;
		}
		else if (a_field->GetAttribute("res_type") == "mat")
		{
			a_button->setEnabled(a_file_path.endsWith(".mat"));
			return;
		}
	}

	a_button->setEnabled(false);
}

void OvrComponentView::addFieldResImg(int a_row_count, ovr_project::IOvrActorProperty* a_field, bool a_is_sub_group)
{
	atts_flag = false;

	OvrTextureLabel* w = new OvrTextureLabel(this);
	w->setStyleSheet("QLabel{background:rgb(0, 0, 0);}");
	w->setMaximumSize(QSize(32, 32));
	w->setMinimumSize(QSize(32, 32));

	pane_layout->addWidget(w, a_row_count, 1, Qt::AlignLeft | Qt::AlignVCenter);

	QPixmap qpixmap = getTexturePixmap(getImagePath("app_icon.png"));
	w->setPixmap(qpixmap.scaled(w->size()));
	//connect(w, SIGNAL(double_clicked()), this, SLOT(textureSelectPop()), Qt::QueuedConnection);
}

void OvrComponentView::addFieldTitle(int a_row_count, const OvrString& a_name, bool a_is_sub_group)
{
	auto label = new QLabel(QString::fromLocal8Bit(a_name.GetStringConst()));

	Qt::Alignment align =  a_is_sub_group ? (Qt::AlignRight | Qt::AlignVCenter) : (Qt::AlignLeft | Qt::AlignVCenter);

	pane_layout->addWidget(label, a_row_count, 0, align);
}

QPixmap OvrComponentView::getTexturePixmap(const QString & relation_path)
{
	QByteArray qbaPath = relation_path.toLocal8Bit();
	auto arch_asset = ovr_asset::OvrArchiveAssetManager::GetIns()->LoadAsset(qbaPath.constData());
	auto material_file = dynamic_cast<ovr_asset::OvrArchiveTexture*>(arch_asset);

	if (!material_file) {
		return QPixmap(getImagePath("app_icon.png"));
	}

	auto rgb_buff = material_file->GetRGBABuffer();

	int n_size = material_file->GetDataSize();

	int width = material_file->GetWidth();
	int height = material_file->GetHeight();
	QImage texImg(rgb_buff, width, height, QImage::Format_RGB888);

	return QPixmap::fromImage(texImg);
}

QColor OvrComponentView::chooseColor(const QColor& color, OvrColorLabel* a_color_label)
{
	QColorDialog colorDlg(OvrHelper::getInst()->getMainWindow());
	colorDlg.setCurrentColor(color);

	connect(&colorDlg, &QColorDialog::currentColorChanged, [this, a_color_label](const QColor& a_color) {
		a_color_label->SetCurrentColor(a_color);
	});

	if (QDialog::Rejected == colorDlg.exec())
	{
		return color;
	}

	return colorDlg.currentColor();
}

void OvrComponentView::changeFieldColor(OvrColorLabel* a_color_label, ovr_project::OvrActorPropertyVector4f* a_field)
{
	auto value = a_field->GetValue();
	QColor color = Vector4f2Color(value);
	
	QColorDialog dlg(OvrHelper::getInst()->getMainWindow());
	dlg.setCurrentColor(color);

	connect(&dlg, &QColorDialog::currentColorChanged, [this, a_color_label, a_field](const QColor& a_color) {
		a_color_label->SetCurrentColor(a_color);
		a_field->SetValue(Color2Vector4f(a_color));
	});

	if (QDialog::Rejected == dlg.exec())
	{
		a_color_label->SetCurrentColor(color);
		a_field->SetValue(value);
	}
}
