#pragma once

#include <QWidget>
#include <QScrollArea>

#define OVR_PROP_BAR_HEIGHT 28
#define OVR_PROP_ROW_HEIGHT 25
#define OVR_PROP_ROW_INTER_SPACE 5
#define OVR_PROP_ROW_MARGIN 5
#define OVR_PROP_ROW_MARGIN1 10

class QGridLayout;
class QToolButton;
class QLineEdit;
class QCheckBox;
class QComboBox;
class QFontComboBox;
class QColorDialog;
class QPlainTextEdit;
class QRadioButton;
class QSlider;

class OvrColorLabel;
class OvrTextureLabel;

namespace ovr_engine {
class OvrParticleEmitter;
class OvrParticleEmitterMesh;
}

//class OvrPropTitleBar : public QWidget
//{
//	Q_OBJECT
//public:
//	explicit OvrPropTitleBar(const char* a_title, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
//
//	virtual QSize sizeHint() const override	{return QSize(1, OVR_PROP_BAR_HEIGHT); }
//
//protected:
//	virtual void mouseDoubleClickEvent(QMouseEvent *event);
//	virtual void paintEvent(QPaintEvent *event) override;
//	void init(const char* a_title);
//};

namespace ovr_engine
{
	class OvrActor;
}

class OvrPropGroup : public QWidget, public OvrEventSender
{
	Q_OBJECT
public:
	explicit OvrPropGroup(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	virtual ~OvrPropGroup();

	virtual QString senderName() const override { return "OvrPropGroup"; }

	enum VideoFormatType
	{
		kVideoNo3D = 0,
		kVideoLeftRight,
		kVideoTopBottom,
		kVideoRightLeft,
		kVideoBottomTop
	};

	enum VideoAdaptationType
	{
		kVideoFull = 0,
		kVideoAdaptation
	};

	void init();
	void showHideGroup();
	bool isGroupHide() { return !is_group_visible; }

	virtual void onUpdateTransform();
	virtual void onSetShowHide(bool isShow);
	virtual void onSetReName(QString a_actor_id);

signals:
	void reloadScene();
	void locateRes(QString a_path);

public slots:
	void onLocate(bool checked = false);
	virtual void onColorChanged(const QColor& color) {};

protected:
	virtual void resizeEvent(QResizeEvent *event) override;
	virtual void onSetLayoutStyle() {}
	virtual void onAddingItems();
	virtual void paintEvent(QPaintEvent *e) override;

	QCheckBox* addCheck(const char* a_title);
	QLineEdit* addEdit(const char* a_title);
	QComboBox* addCombo(const char* a_title);
	void addXYZ(const char* a_title, QList<QLineEdit*>& a_list);
	void addHBoxXYZ(const char* a_title, QList<QLineEdit*>& a_list, QList<QLabel*>& a_label_list = QList<QLabel*>());
	QLineEdit*  addMeshMaterial(const char* a_title);

	QLineEdit *addDigitesLineEdit(const QString & a_title);
	OvrTextureLabel *addTexturePreview(const QString & a_title);
	OvrColorLabel* addColorSelLabel(const QString & a_title);
	QColor getSelectedColor(const QColor color = QColor(0, 0, 0));

	void update_color_label_preview(QLabel* label, const QColor & color);

	QPixmap getTexturePixmap(const QString & str_path);

	QWidget* group_pane = nullptr;
	QGridLayout* pane_layout = nullptr;

	int group_row = 0;
	bool is_group_visible = true;
	ovr_engine::OvrActor* m_actor = nullptr;

	QToolButton* locate_button = nullptr;
	QToolButton* apply_button = nullptr;
	QLineEdit* mesh_material_value = nullptr;
private:
	void beginAddItem();
	void endAddItem();
};

class OvrPropBaseGroup : public OvrPropGroup
{
	Q_OBJECT
public:
	explicit OvrPropBaseGroup(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}

public slots :
	void editingFinished();
	void check_btn_changed(bool checked);

protected:
	virtual void onAddingItems() override;
	virtual void onSetLayoutStyle() override;
	virtual void onSetShowHide(bool isShow);
	virtual void onSetReName(QString a_actor_id);

	QLineEdit* name_edit;
	QCheckBox* visible_check = nullptr;
	QCheckBox* moveable_check = nullptr;
	QCheckBox* focus_check = nullptr;
	QCheckBox* shadow_check = nullptr;
};

class OvrPropTransGroup : public OvrPropGroup
{
	Q_OBJECT
public:
	explicit OvrPropTransGroup(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}

public slots :
	void transEditingFinished();
	void scaleEditingFinished();
	void rotateEditingFinished();
	void on_update_transform(ovr_engine::OvrActor* a_actor);

protected:
	virtual void onAddingItems() override;
	virtual void onUpdateTransform() override;

	QList<QLineEdit*> rotate;
	QList<QLineEdit*> trans;
	QList<QLineEdit*> scale;
};

class OvrPropMeshGroup : public OvrPropGroup
{
	Q_OBJECT
public:
	explicit OvrPropMeshGroup(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}
public slots :
	void onApply(bool checked = false);

protected:
	virtual void onAddingItems() override;
};

class OvrPropMaterialGroup : public OvrPropGroup
{
	Q_OBJECT
public:
	explicit OvrPropMaterialGroup(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}

	virtual QString senderName() const override { return "OvrPropMaterialGroup"; }

public slots:
	void onApply(bool checked = false);
	void onLocate(bool checked = false);

	void onLineEditDoubleClicked();
protected:
	virtual void onAddingItems() override;

protected:
	struct MatInfoUi
	{
		QToolButton* locate_button = nullptr;
		QToolButton* apply_button = nullptr;
		QLineEdit*  material_editor = nullptr;

		//ovr_engine::OvrMaterial* material = nullptr;
		QString material_path ;
		ovr::uint32 mat_index = -1;
	};

	//MatInfoUi addMeshMaterial(ovr_engine::OvrMaterial* a_mat, const QString& a_title);
	MatInfoUi addMeshMaterial(const QString& a_mat_path, const QString& a_title);

	QList<MatInfoUi> m_vctMatsEditUI;
};


//video
class OvrPropVideo3DGroup : public OvrPropGroup
{
	Q_OBJECT
public:
	explicit OvrPropVideo3DGroup(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}

	void SetVideoFormat(VideoFormatType a_format, VideoAdaptationType a_adaptation);
public slots :
	void loopCheckChanged(int a_state);
	void adaptationChanged(int a_index);
	void formatChanged(int a_index);

protected:
	virtual void onAddingItems() override;

	QCheckBox* loop_check;
	QComboBox* adaptation_combo;
	QComboBox* format_combo;

private:
	VideoFormatType video_format;
	VideoAdaptationType video_adaptation;
};

class OvrPropVideo2DGroup : public OvrPropGroup
{
	Q_OBJECT
public:
	explicit OvrPropVideo2DGroup(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}

	public slots :
	void loopCheckChanged(int a_state);
	void adaptationChanged(int a_index);
protected:
	virtual void onAddingItems() override;

	QCheckBox* loop_check;
	QComboBox* adaptation_combo;

private:
	VideoAdaptationType video_adaptation;
};

class OvrPropVideo180Group : public OvrPropGroup
{
	Q_OBJECT
public:
	explicit OvrPropVideo180Group(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}

	public slots :
	void loopCheckChanged(int a_state);
	void formatChanged(int a_index);

protected:
	virtual void onAddingItems() override;

	QCheckBox* loop_check;
	QComboBox* format_combo;
private:
	VideoFormatType video_format;
};

class OvrPropVideo360Group : public OvrPropGroup
{
	Q_OBJECT
public:
	explicit OvrPropVideo360Group(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}

	public slots :
	void loopCheckChanged(int a_state);
	void formatChanged(int a_index);

protected:
	virtual void onAddingItems() override;

	QCheckBox* loop_check;
	QComboBox* format_combo;

private:
	VideoFormatType video_format;
};

class OvrPropVideo360BoxGroup : public OvrPropGroup
{
	Q_OBJECT
public:
	explicit OvrPropVideo360BoxGroup(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}

	public slots :
	void loopCheckChanged(int a_state);
	void formatChanged(int a_index);

protected:
	virtual void onAddingItems() override;

	QCheckBox* loop_check;
	QComboBox* format_combo;

private:
	VideoFormatType video_format;
};

class OvrProp3DAudioGroup : public OvrPropGroup
{
	Q_OBJECT
public:
	explicit OvrProp3DAudioGroup(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}
	
	public slots :
	void loopCheckChanged(int a_state);
	//void adaptationChanged(int index);

protected:
	virtual void onAddingItems() override;

	QCheckBox* loop_check;
	//QComboBox* adaptation_combo;
};


//灯光 - 点光 平行 聚光
class OvrLightGroup : public OvrPropGroup
{
	Q_OBJECT
public:
	explicit OvrLightGroup(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}

public Q_SLOTS : ;
	void colorSelectPop();
	void LightAttiEditingFinished();
	virtual void onColorChanged(const QColor& color) override;

protected:
	virtual void onAddingItems() override;

private:
	OvrColorLabel * label_light_color = nullptr;// 灯光颜色
	QLineEdit* edit_light_intensity = nullptr;	// 强度
	QLineEdit* edit_light_scope = nullptr;		// 范围
	QLineEdit* edit_light_angle = nullptr;		// 角度
	QLineEdit* edit_light_fall = nullptr;		// 衰减速度
	QCheckBox* chk_light_shade = nullptr;		// 启用阴影
};

// 文本
class OvrTextGroup : public OvrPropGroup
{
	Q_OBJECT
public:
	OvrTextGroup(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}
	~OvrTextGroup() {};
protected:
	virtual void onAddingItems() override;

	private Q_SLOTS:;
	void textInfoChanged();
	void fontChanged(const QFont &a_font);
	void scaleChanged();
	void colorSelectPop();
	virtual void onColorChanged(const QColor& color) override;

private:
	QPlainTextEdit* addTextEdit(const QString & a_title);
	QFontComboBox* addTextFontSel(const QString & a_title);
	QLineEdit* addTextScale(const QString & a_title);
	//QLabel* addTextColorSel(const QString & a_title);

private:
	QPlainTextEdit * edit_text_content = nullptr;	// 文本内容
	QFontComboBox * combo_font_select = nullptr;	// 字体
	QLineEdit * edit_text_scale = nullptr;			// 缩放
	OvrColorLabel * label_color_sel = nullptr;		// 颜色选择
};


// 文本, 排版
class OvrTextMakeupGroup : public OvrPropGroup
{
	Q_OBJECT
public:
	OvrTextMakeupGroup(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}
	~OvrTextMakeupGroup() {};
protected:
	virtual void onAddingItems() override;

	private Q_SLOTS:;
	void textMarginChanged();
	void fontAlignXYChanged();
	void fontSpacingChanged();
	void lineSpacingChanged();

private:
	void addMarginUI(const QString & a_title,
		QButtonGroup *&horiz_margin, std::array<QRadioButton*, 3>& horiz_btns,
		QButtonGroup *&vert_margin, std::array<QRadioButton*, 3> & vert_btns);
	void addAlignSpace(const QString & a_title, QLineEdit *&x, QLineEdit *&y);
	//QLineEdit *addLineEdit(const QString & a_title);

private:
	QButtonGroup * horiz_margin;
	QButtonGroup * vert_margin;
	std::array<QRadioButton*, 3> horiz_margin_btns;
	std::array<QRadioButton*, 3> vert_margin_btns;
	QLineEdit * edit_align_space_x = nullptr;		// 边缘空白
	QLineEdit * edit_align_space_y = nullptr;		// 边缘空白
	QLineEdit * edit_font_spacing = nullptr;
	QLineEdit * edit_line_spacing = nullptr;
};

// 文本, 其它
class OvrTextOthersGroup : public OvrPropGroup
{
	Q_OBJECT
public:
	OvrTextOthersGroup(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}
	~OvrTextOthersGroup() {};

protected:
	virtual void onAddingItems() override;

	private Q_SLOTS:;
	void textBackgroundSelectPop();
	void transparencyChanged();

private:
	//QLabel * addBackgroundUi(const QString & a_title);
private:
	OvrTextureLabel *background_preview = nullptr; // 
	QLineEdit * edit_transparency = nullptr;
};

// 相机
class OvrCameraGroup : public OvrPropGroup
{
	Q_OBJECT
public:
	OvrCameraGroup(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}
	~OvrCameraGroup() {};

protected:
	virtual void onAddingItems() override;

	private Q_SLOTS:;
	void sliderVisualAngleChanged(int value);
	void textVisualAngleChanged();
	void textAspectRadioChanged();
	void textNeerClipPlaneChanged();
	void textFarClipPlaneChanged();

private:
	void addVisualAngleUi(const QString & a_title, QSlider*&slider, QLineEdit*&editer);
private:
	QSlider * slider_visual_angle = nullptr;		// 视场
	QLineEdit * edit_visual_angle = nullptr;		// 视场 edit
	QLineEdit * edit_aspect_radio = nullptr;		// 宽高比
	QLineEdit * edit_neer_clip_plane = nullptr;		// 近裁剪面
	QLineEdit * edit_far_clip_plane = nullptr;		// 远裁剪面
};

// 碰撞体
class OvrColliderGroup : public OvrPropGroup
{
	Q_OBJECT
public:
	OvrColliderGroup(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}
	~OvrColliderGroup() {};

protected:
	virtual void onAddingItems() override {};

private Q_SLOTS:;

private:
};

// 粒子： 粒子
class OvrParticleGroup : public OvrPropGroup
{
	Q_OBJECT
public:
	OvrParticleGroup(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}
	~OvrParticleGroup() {};

protected:
	virtual void onAddingItems() override;

private Q_SLOTS:;
	void textureSelectPop();
	void colorSelectPop();
	virtual void onColorChanged(const QColor& color) override;
	void toCameraCheckFinished(bool to_camera);

	void rotateEditingFinished();
	void lineEditFinished();

private:
	void add_region_setting_ui(QLineEdit *&region_low, QLineEdit *&region_hight, const QString &a_title);
private:

	QLabel * texture_preview = nullptr;
	OvrColorLabel * label_color_sel = nullptr;
	QList<QLineEdit*> self_rotate;
	QCheckBox * check_to_camera = nullptr;

	QLineEdit *diameter_region_low = nullptr, *diameter_region_hight = nullptr;	// 直径范围
	QLineEdit *speed_region_low = nullptr, *speed_region_hight = nullptr;		// 速度范围
	QLineEdit *life_region_low = nullptr, *life_region_hight = nullptr;			// 生命范围
};

// 粒子： 发射器
class OvrEmmitterGroup : public OvrPropGroup
{
	Q_OBJECT
public:
	OvrEmmitterGroup(ovr_engine::OvrActor* a_actor, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :OvrPropGroup(a_actor, parent, f)
	{
		//onAddingItems();
	}
	~OvrEmmitterGroup() {};

protected:
	virtual void onAddingItems() override;

private Q_SLOTS:;
	void update_sender_args_ui(int index);
	void argsEditFinished();

private:
	void set_sender_args_ui();
	void pathSelectPop(ovr_engine::OvrParticleEmitterMesh* mesh_emitter);

	void show_line_edit_ui(bool bshow);
	void show_cube_xy_ui(bool bshow);
	void show_mesh_ui(bool bshow);

	void update_emitter_args_ui(int a_type);
	void update_emitter_args(ovr_engine::OvrParticleEmitter * emitter);

private:
	QComboBox * combo_emmitter = nullptr;
	QLabel* label_title = nullptr;
	QLineEdit * editer_args = nullptr;									// 发散角度
	//QLineEdit * editer_angle = nullptr;								// 发散角度
	//QLineEdit * editer_line_len = nullptr;							// 线长
	//QLineEdit * editer_circle_radius = nullptr;						// 圆的半径
	//QLineEdit * editer_boll_radius = nullptr;							// 球的半径
	QLabel *label_x = nullptr, *label_y = nullptr, *label_z = nullptr;
	QLineEdit * editer_cube_x = nullptr, *editer_cube_y = nullptr, *editer_cube_z = nullptr;		// 立方体尺寸
	//QList<QLabel*> cube_labels;
	//QList<QLineEdit*> cube_editors;									// 立方体尺寸
	QLineEdit * editer_mesh_path = nullptr;								// 网格
	QToolButton * btn_mesh_path = nullptr;

};


///////////////////////////////////////////////////////////////////////////////////////////////////
class OvrLineEdit : public QLineEdit
{
	Q_OBJECT
public:
	explicit OvrLineEdit(QWidget *parent = Q_NULLPTR) :QLineEdit(parent) {};
	explicit OvrLineEdit(const QString &text, QWidget *parent = Q_NULLPTR) :QLineEdit(text, parent) {};
	~OvrLineEdit() {};

Q_SIGNALS:
	void double_clicked();
private:

	virtual void mouseDoubleClickEvent(QMouseEvent *event) override
	{
		emit (double_clicked());
	}
};
