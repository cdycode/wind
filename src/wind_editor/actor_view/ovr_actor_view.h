#pragma once

#include <QDockWidget>

class OvrActorOutlineTree;
class OVROutlinePropView;
class OvrActorPropertyTree;

class OvrActorView : public QDockWidget
{
	Q_OBJECT
public:
	explicit OvrActorView(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	~OvrActorView();
	protected slots:
	void CreateEmptyObject();

protected:
	void mousePressEvent(QMouseEvent *event) override;


private:
	void set_custom_style_sheet();
	void init_custom_ui();

	void init_dock_title_bar();
	void init_dock_content_widget();
	void preset_connects();

	void paintEvent(QPaintEvent *e) override;

private:
	OvrActorOutlineTree* actor_tree = nullptr;
	OvrActorPropertyTree * prop_pane = nullptr;
};
