#include "Header.h"
#include "ovr_style_button.h"

static QString title_bar_button_style = 
		"QPushButton{max-height:40px; width:40px; height:30px; background:rgb(48, 48, 48); border:0px solid rgb(30, 30, 30); border-radius: 0px;}"
		"QPushButton:hover{background:rgb(243, 45, 36);}"
		"QPushButton:pressed{background:rgb(221, 28, 19);}";

void OvrStyleButtonClose::init()
{
	QString str_style = title_bar_button_style;

	QTextStream ts(&str_style);
	ts << "QPushButton{image:url(" << getImagePath("pop_btn_close.png") << ");}";

	setStyleSheet(str_style);
	setText("");
}

void OvrStyleButtonMin::init()
{
	QString str_style = title_bar_button_style;

	QTextStream ts(&str_style);
	ts << "QPushButton{image:url(" << getImagePath("pop_btn_mini.png") << ");}";

	setStyleSheet(str_style);
	setText("");
}

void OvrStyleButtonMax::init()
{
	QString str_style = QString::fromLocal8Bit(
		"QCheckBox{spacing:0px;}"
		"QCheckBox::indicator{max-height:40px; width:40px; height:30px; background:rgb(48, 48, 48); border:0px solid rgb(30, 30, 30); border-radius:0px;}"
		"QCheckBox::indicator:hover{background:rgb(70, 70, 70);}"
		"QCheckBox::indicator:pressed{background:rgb(21, 21, 21);}"
	);

	QTextStream ts(&str_style);
	ts << "QCheckBox::indicator{image:url(" << getImagePath("pop_btn_biggest.png") << ");}"
		<< "QCheckBox::indicator:checked{image:url(" << getImagePath("pop_btn_small.png") << ");}";

	setStyleSheet(str_style);
	setText("");
}

void OvrStyleCommonButton::init()
{
	const char* str_style = "QPushButton{height:24;color: rgb(205, 205, 205); border: 1px solid rgb(30, 30, 30); border-radius: 4px; background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); }"
		"QPushButton:hover{background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(153, 153, 153), stop:1 rgb(109, 109, 109)); }"
		"QPushButton:pressed {background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:1 rgb(118, 118, 118), stop:0 rgb(87, 87, 87)); }"
		"QPushButton:disabled {background-color:rgb(56, 56, 56); color:rgb(76, 76, 76);  border: 1px solid rgb(76, 76, 76); }";

	setStyleSheet(str_style);
}