#ifndef __SIZE_DOCK_WIDGET_H__
#define __SIZE_DOCK_WIDGET_H__


class SizeDockWidget : public QWidget
{
   Q_OBJECT
public:
   explicit SizeDockWidget(QWidget * parent = 0, Qt::WindowFlags f = 0)
      : QWidget(parent,f) {
      m_szHint = geometry().size();
   }

   void updateSizeHint(QSize & szHint2Set) {
      m_szHint = szHint2Set;
      updateGeometry();
   }

   virtual QSize sizeHint() const {
      return m_szHint;
   }
protected:
   QSize m_szHint;
};

#endif // !__SIZE_DOCK_WIDGET_H__
