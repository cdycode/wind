#pragma once

#include <QMenuBar>
#include <QMenu>
#include <QWidget>
#include <QHBoxLayout>


#include <QDockWidget>
//#include "ui_main_frame_title_bar.h"
class TitleBar;

class MainFrameTitleBar : public QDockWidget
{
	Q_OBJECT

public:
	explicit MainFrameTitleBar(QWidget *parent = Q_NULLPTR);
	MainFrameTitleBar(const QString &strTitle, QWidget *parent = Q_NULLPTR);
	~MainFrameTitleBar();

public:
	QMenuBar * get_menu_bar();
	void set_menu_bar(QMenuBar * menuBar);

	void set_fixed_widget_height(int n_height);
protected:
	virtual bool eventFilter(QObject * obj, QEvent * event) override;
	virtual void paintEvent(QPaintEvent * event) override;
	//virtual void printEvent
private:
	//Ui::MainFrameTitleBar ui;

	void init_common_ui();

	void set_custom_style_sheet();

	void add_menu_test();


private:
	QWidget *dockWidgetContents = nullptr;
	QHBoxLayout *horizontalLayout = nullptr;
	
	TitleBar * title_bar = nullptr;
	QMenuBar * menuBar = nullptr;
};
