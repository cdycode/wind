#pragma once

#include <QWidget>

class OvrToolButton;

class OVRDockTitleBar : public QWidget
{
	Q_OBJECT
public:
	explicit OVRDockTitleBar(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());

	QWidget* setBarContext(QWidget* a_context);

protected:
	virtual void mouseDoubleClickEvent(QMouseEvent *event) override;
	virtual void paintEvent(QPaintEvent *event) override;

private:
	void initUI();
	void onDockOrFloat();
	void onMaxRestore();

	QAction* action_float = nullptr;		// 浮动按钮
	QAction* action_max_restore = nullptr;	// 最大化按钮

	QWidget* bar_context = nullptr;
	QHBoxLayout* h_layout = nullptr;
};
