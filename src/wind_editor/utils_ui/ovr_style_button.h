#pragma once

#include <QPushButton>


class OvrStyleButtonClose : public QPushButton
{
public:
	explicit OvrStyleButtonClose(QWidget *parent = Q_NULLPTR) :QPushButton(parent) {}
	explicit OvrStyleButtonClose(const QString &text, QWidget *parent = Q_NULLPTR) :QPushButton(text, parent) { }

	void init();
};

class OvrStyleButtonMin : public QPushButton
{
public:
	explicit OvrStyleButtonMin(QWidget *parent = Q_NULLPTR) :QPushButton(parent) {  }
	explicit OvrStyleButtonMin(const QString &text, QWidget *parent = Q_NULLPTR) :QPushButton(text, parent) { }

	void init();

};

#include <QCheckBox>
class OvrStyleButtonMax : public QCheckBox
{
public:
	explicit OvrStyleButtonMax(QWidget *parent = Q_NULLPTR) :QCheckBox(parent) { }
	explicit OvrStyleButtonMax(const QString &text, QWidget *parent = Q_NULLPTR) :QCheckBox(text, parent) { }

	void init();
};


class OvrStyleCommonButton : public QPushButton
{
public:
	explicit OvrStyleCommonButton(QWidget *parent = Q_NULLPTR) :QPushButton(parent) { init(); }
	explicit OvrStyleCommonButton(const QString &text, QWidget *parent = Q_NULLPTR) :QPushButton(text, parent) { init(); }

protected:
	void init();
};
