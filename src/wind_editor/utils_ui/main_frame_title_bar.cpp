#include "Header.h"
#include "main_frame_title_bar.h"
#include "title_bar.h"

MainFrameTitleBar::MainFrameTitleBar(QWidget *parent)
	: QDockWidget(parent)
{
	setObjectName(QStringLiteral("dockWidget_myTop"));
	set_fixed_widget_height(27);

	init_common_ui();
	set_custom_style_sheet();
	//add_menu_test();
}

MainFrameTitleBar::MainFrameTitleBar(const QString & strTitle, QWidget * parent)
	: QDockWidget(strTitle, parent)
{
	init_common_ui();
	set_custom_style_sheet();
}

MainFrameTitleBar::~MainFrameTitleBar()
{

}

QMenuBar * MainFrameTitleBar::get_menu_bar()
{
	return menuBar;
}

void MainFrameTitleBar::set_menu_bar(QMenuBar * menuBar)
{
	//this->setFixedHeight(-1);

	horizontalLayout->removeWidget(this->menuBar);
	this->menuBar->deleteLater();
	this->menuBar = menuBar;

	//this->setFixedHeight(height());
}

bool MainFrameTitleBar::eventFilter(QObject * obj, QEvent * event)
{
	switch (event->type())
	{
	case QEvent::WindowTitleChange:
	{
		QWidget *pWidget = qobject_cast<QWidget *>(obj);
		if (pWidget)
		{
			QString strWindowTitle = pWidget->windowTitle();
			this->setWindowTitle(strWindowTitle);
			return true;
		}
	}
	//case QEvent::WindowIconChange:
	//{
	//	QWidget *pWidget = qobject_cast<QWidget *>(obj);
	//	if (pWidget)
	//	{
	//		QIcon icon = pWidget->windowIcon();
	//		int n_h = title_logo->height();
	//		title_logo->setPixmap(icon.pixmap(QSize(n_h, n_h)));
	//		title_logo->setFixedWidth(5 + n_h + 5);
	//		return true;
	//	}
	//}
	}
	return QWidget::eventFilter(obj, event);
}

void MainFrameTitleBar::paintEvent(QPaintEvent *event)
{
	Q_UNUSED(event);

	ApplyWidgetStyleSheet(this);
}

void MainFrameTitleBar::init_common_ui()
{
	title_bar = new TitleBar(this, QString::fromLocal8Bit(""));
	installEventFilter(title_bar);
	setTitleBarWidget(title_bar);
	setAllowedAreas(Qt::TopDockWidgetArea);
	setFeatures(QDockWidget::NoDockWidgetFeatures);

	dockWidgetContents = new QWidget();
	//dockWidgetContents->setFixedHeight(27);
	horizontalLayout = new QHBoxLayout(dockWidgetContents);
	horizontalLayout->setSpacing(0);
	horizontalLayout->setContentsMargins(0, 0, 0, 0);
	this->setWidget(dockWidgetContents);

	menuBar = new QMenuBar(this);
	horizontalLayout->addWidget(menuBar);
}


void MainFrameTitleBar::set_custom_style_sheet()
{
	//QString strStyle = QString::fromLocal8Bit(
	//	//"QDockWidget{background:rgb(127, 27, 27);}"
	//	"QDockWidget{border:1px solid rgb(35, 35, 135);}"
	//	//"QDockWidget{color:rgb(160, 160, 160); background:rgb(39, 39, 39); font:14px \"微软雅黑\"; border:0px solid rgb(35, 35, 35);}"
	//	//"QToolTip{color:rgb(48,48,48); background-color:rgb(204,204,204);}"
	//);
	//this->setStyleSheet(strStyle);
	////dockWidgetContents->setStyleSheet(strStyle);
}


// only for test
void MainFrameTitleBar::add_menu_test()
{
	QMenu *menurtg = new QMenu(QString::fromLocal8Bit("文件(&F)"), this);
	menurtg->setObjectName(QStringLiteral("menurtg"));
	QMenu *menurtgy = new QMenu(QString::fromLocal8Bit("油荒(&F)"), this);
	menurtgy->setObjectName(QStringLiteral("menurtgy"));
	QMenu *menuhjj7y = new QMenu(QString::fromLocal8Bit("县城(&F)"), this);
	menuhjj7y->setObjectName(QStringLiteral("menuhjj7y"));
	QMenu *menusnbvr = new QMenu(QString::fromLocal8Bit("抈扔(&F)"), this);
	menusnbvr->setObjectName(QStringLiteral("menusnbvr"));


	QAction *action_open_file = new QAction(QString::fromLocal8Bit("文件(&F)"), this);
	action_open_file->setObjectName(QStringLiteral("action_open_file"));
	QAction *action_open_img = new QAction(QString::fromLocal8Bit("文件(&F)"), this);
	action_open_img->setObjectName(QStringLiteral("action_open_img"));
	QAction *action_open_other = new QAction(QString::fromLocal8Bit("文件(&F)"), this);
	action_open_other->setObjectName(QStringLiteral("action_open_other"));
	QAction *action_save = new QAction(QString::fromLocal8Bit("文件(&F)"), this);
	action_save->setObjectName(QStringLiteral("action_save"));
	QAction *action_close = new QAction(QString::fromLocal8Bit("文件(&F)"), this);
	action_close->setObjectName(QStringLiteral("action_close"));


	menurtg->addAction(action_open_file);
	menurtg->addAction(action_open_img);
	menurtg->addAction(action_open_other);
	menurtg->addSeparator();
	menurtg->addAction(action_save);
	menurtgy->addAction(action_close);
	//menurtgy->addAction(actionhn);
	//menurtgy->addAction(actionntr);
	//menurtgy->addAction(actionfghesr);
	menurtgy->addSeparator();

	menuBar->addMenu(menurtg);
	menuBar->addMenu(menurtgy);
	menuBar->addMenu(menuhjj7y);
	menuBar->addMenu(menusnbvr);
}

void MainFrameTitleBar::set_fixed_widget_height(int n_height)
{
	dockWidgetContents->setFixedHeight(n_height);
}
