#include "Header.h"
#include "title_bar.h"

TitleBar::TitleBar(QWidget *parent, TitleFlags flags)
	: QWidget(parent)
{
	init_common_ui();
	setUiFlags(flags);
	setTitle(QString(""));
	setTilteLogo(QIcon());
}

TitleBar::TitleBar(QWidget *parent, QString &title, TitleFlags flags)
	: QWidget(parent)
{
	init_common_ui();

	setUiFlags(flags);

	setTitle(title);
	setTilteLogo(QIcon(getImagePath("app_icon.png")));
}

TitleBar::TitleBar(QWidget *parent, QIcon & titleLogo, QString & title, TitleFlags flags)
	: QWidget(parent)
{
	init_common_ui();

	setUiFlags(flags);

	setTitle(title);
	setTilteLogo(titleLogo);
}

TitleBar::~TitleBar()
{
}

void TitleBar::setTitle(QString & title)
{
	title_text->setText(title);
}

void TitleBar::setTilteLogo(QIcon &logo)
{
	if (!logo.isNull()) {
		int n_h = title_logo->height();
		title_logo->setPixmap(logo.pixmap(QSize(n_h, n_h)));
		title_logo->setFixedWidth( n_h);
	}
	else {
		title_logo->setFixedWidth(0);
	}
}

void TitleBar::setUiFlags(TitleFlags flags)
{
	flags & TitleFlag::TF_BTN_MINIMIZE ? btn_minimize->show() : btn_minimize->hide();
	flags & TitleFlag::TF_BTN_MAXIMIZE ? btn_maximize->show() : btn_maximize->hide();
	flags & TitleFlag::TF_BTN_CLOSE ? btn_close->show() : btn_close->hide();
	flags & TitleFlag::TF_BTN_HELP ? btn_help->show() : btn_help->hide();

	tilte_flags = flags;
}

void TitleBar::paintEvent(QPaintEvent *event)
{
	ApplyWidgetStyleSheet(this);
	QWidget::paintEvent(event);
}

void TitleBar::mouseDoubleClickEvent(QMouseEvent * event)
{
	Q_UNUSED(event);

	if (tilte_flags & TitleFlag::TF_BTN_MAXIMIZE)
		emit btn_maximize->clicked();
}

void TitleBar::mousePressEvent(QMouseEvent * event)
{
#ifdef Q_OS_WIN
	if (ReleaseCapture())
	{
		QWidget *pWindow = this->window();
		if (pWindow->isTopLevel())
		{
			SendMessage(HWND(pWindow->winId()), WM_SYSCOMMAND, SC_MOVE + HTCAPTION, 0);
		}
	}
	event->ignore();
#else
#endif
}

bool TitleBar::eventFilter(QObject * obj, QEvent * event)
{
	switch (event->type())
	{
	case QEvent::WindowTitleChange:
	{
		QWidget *pWidget = qobject_cast<QWidget *>(obj);
		if (pWidget)
		{
			title_text->setText(pWidget->windowTitle());
			return true;
		}
	}
	case QEvent::WindowStateChange:
	case QEvent::Resize:
		updateMaximize();
		return true;
	}
	return QWidget::eventFilter(obj, event);
}

void TitleBar::init_common_ui()
{
	setupUi();
	setFixedHeight(30);
	set_custom_style_sheet();
}

void TitleBar::set_custom_style_sheet()
{
	QString strTitleBarStyle = QString::fromLocal8Bit(
		"QWidget{color:rgb(160, 160, 160); background:rgb(39, 39, 39); font:14px \"微软雅黑\"; border-top:0px solid rgb(35, 35, 35); border-bottom:1px solid rgb(35, 35, 35); padding-bottom:2px;}"
		"QWidget:focus{outline: none;}"
		"QToolTip{color:rgb(48,48,48); background-color:rgb(204,204,204);}"
	);
	setStyleSheet(strTitleBarStyle);

	// 关闭按钮
	QString strbtnCloseStyle =
		"QPushButton{max-height:40px; width:40px; height:30px; background:rgb(48, 48, 48); border:0px solid rgb(30, 30, 30); border-radius: 0px;}"
		"QPushButton:hover{background:rgb(243, 45, 36);}"
		"QPushButton:pressed{background:rgb(221, 28, 19);}";

	QTextStream qtsTempa(&strbtnCloseStyle);
	qtsTempa << "QPushButton{image:url(" << getImagePath("pop_btn_close.png") << ");}" ;

	btn_close->setStyleSheet(strbtnCloseStyle);

	// 最大化按钮
	QString strbtnMaxStyle = QString::fromLocal8Bit(
		"QCheckBox{spacing:0px;}"
		"QCheckBox::indicator{max-height:40px; width:40px; height:30px; background:rgb(48, 48, 48); border:0px solid rgb(30, 30, 30); border-radius:0px;}"
		"QCheckBox::indicator:hover{background:rgb(70, 70, 70);}"
		"QCheckBox::indicator:pressed{background:rgb(21, 21, 21);}"
	);

	QTextStream qtsTempb(&strbtnMaxStyle);
	qtsTempb << "QCheckBox::indicator{image:url(" << getImagePath("pop_btn_biggest.png") << ");}"
		<< "QCheckBox::indicator:checked{image:url(" << getImagePath("pop_btn_small.png") << ");}";

	btn_maximize->setStyleSheet(strbtnMaxStyle);

	// 最小化按钮
	QString strbtnMinStyle = QString::fromLocal8Bit(
		"QPushButton{max-height:40px; width:40px; height:30px; background:rgb(48, 48, 48); border:0px solid rgb(30, 30, 30); border-radius: 0px;}"
		"QPushButton:hover{background:rgb(70, 70, 70);}"
		"QPushButton:pressed{background:rgb(21, 21, 21);}"
	);

	QTextStream qtsTempc(&strbtnMinStyle);
	qtsTempc << "QPushButton{image:url(" << getImagePath("pop_btn_mini.png") << ");}";

	btn_minimize->setStyleSheet(strbtnMinStyle);

	// 帮助按钮
	QString strbtnHelpStyle = QString::fromLocal8Bit(
		"QPushButton{max-height:40px; width:40px; height:30px; background:rgb(48, 48, 48); border:0px solid rgb(30, 30, 30); border-radius: 0px; /*image:url();*/ }"
		"QPushButton:hover{background:rgb(70, 70, 70);}"
		"QPushButton:pressed{background:rgb(21, 21, 21);}"
	);
	btn_help->setStyleSheet(strbtnHelpStyle);
}

void TitleBar::updateMaximize()
{
	QWidget *pWindow = this->window();
	if (pWindow->isTopLevel())
	{
		bool bMaximize = pWindow->isMaximized();
		if (bMaximize)
		{
			btn_maximize->setToolTip(QString::fromLocal8Bit("还原"));
			btn_maximize->setChecked(true);
		}
		else
		{
			btn_maximize->setToolTip(QString::fromLocal8Bit("最大化"));
			btn_maximize->setChecked(false);
		}
	}
}

void TitleBar::on_btn_minimize_clicked()
{
	QWidget *pWindow = this->window();

	if (pWindow->isTopLevel())
		pWindow->showMinimized();
}

void TitleBar::on_btn_maximize_clicked()
{
	QWidget *pWindow = this->window();
	if (pWindow->isTopLevel())
		pWindow->isMaximized() ? pWindow->showNormal() : pWindow->showMaximized();

}

void TitleBar::on_btn_close_clicked()
{
	QWidget *pWindow = this->window();
	if (pWindow->isTopLevel())
	{
		pWindow->close();
	}
}

void TitleBar::setupUi()
{
	if (this->objectName().isEmpty())
		this->setObjectName(QStringLiteral("TitleBar"));
	//this->resize(594, 104);
	horizontalLayout = new QHBoxLayout(this);
	horizontalLayout->setSpacing(0);
	horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
	horizontalLayout->setContentsMargins(0, 0, 0, 0);
	horizontalLayout_tl = new QHBoxLayout();
	horizontalLayout_tl->setSpacing(9);
	horizontalLayout_tl->setObjectName(QStringLiteral("horizontalLayout_tl"));
	title_logo = new QLabel(this);
	title_logo->setObjectName(QStringLiteral("title_logo"));

	horizontalLayout_tl->addWidget(title_logo);

	title_text = new QLabel(this);
	title_text->setObjectName(QStringLiteral("title_text"));

	horizontalLayout_tl->addWidget(title_text);


	horizontalLayout->addLayout(horizontalLayout_tl);

	horizontalSpacer = new QSpacerItem(366, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

	horizontalLayout->addItem(horizontalSpacer);

	btn_help = new QPushButton(this);
	btn_help->setObjectName(QStringLiteral("btn_help"));
	QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
	sizePolicy.setHorizontalStretch(0);
	sizePolicy.setVerticalStretch(0);
	sizePolicy.setHeightForWidth(btn_help->sizePolicy().hasHeightForWidth());
	btn_help->setSizePolicy(sizePolicy);

	horizontalLayout->addWidget(btn_help);

	btn_minimize = new QPushButton(this);
	btn_minimize->setObjectName(QStringLiteral("btn_minimize"));
	sizePolicy.setHeightForWidth(btn_minimize->sizePolicy().hasHeightForWidth());
	btn_minimize->setSizePolicy(sizePolicy);

	horizontalLayout->addWidget(btn_minimize);

	btn_maximize = new QCheckBox(this);
	btn_maximize->setObjectName(QStringLiteral("btn_maximize"));
	sizePolicy.setHeightForWidth(btn_maximize->sizePolicy().hasHeightForWidth());
	btn_maximize->setSizePolicy(sizePolicy);

	horizontalLayout->addWidget(btn_maximize);

	btn_close = new QPushButton(this);
	btn_close->setObjectName(QStringLiteral("btn_close"));
	sizePolicy.setHeightForWidth(btn_close->sizePolicy().hasHeightForWidth());
	btn_close->setSizePolicy(sizePolicy);

	horizontalLayout->addWidget(btn_close);

	retranslateUi(this);

	QMetaObject::connectSlotsByName(this);
} // setupUi

void TitleBar::retranslateUi(QWidget *widget)
{
	widget->setWindowTitle("TitleBar");

	btn_help->setToolTip(QString::fromLocal8Bit("帮助"));
	btn_minimize->setToolTip(QString::fromLocal8Bit("最小化"));
	btn_maximize->setToolTip(QString::fromLocal8Bit("最大化"));
	btn_close->setToolTip(QString::fromLocal8Bit("关闭"));

	title_text->setText("");
	btn_minimize->setText("");
	btn_maximize->setText("");
	btn_close->setText("");
}
