#pragma once

#include <QWidget>
#include "ui_ovr_common_title_bar.h"

class OvrCommonTitleBar : public QWidget
{
	Q_OBJECT

public:
	OvrCommonTitleBar(QWidget *parent = Q_NULLPTR);
	~OvrCommonTitleBar();

	void init();

protected slots:
	void on_button_min_clicked(bool);
	void on_button_max_clicked(bool);
	void on_button_close_clicked(bool);

protected:
	virtual void paintEvent(QPaintEvent *event) override;
	virtual void mouseDoubleClickEvent(QMouseEvent *event) override;
	virtual void mousePressEvent(QMouseEvent *event) override;
	virtual void mouseMoveEvent(QMouseEvent *event) override;
	virtual void mouseReleaseEvent(QMouseEvent *event) override;
	virtual bool eventFilter(QObject * obj, QEvent * event) override;

private:
	void initStyle();
	void initUI();
	void initParentUI();

	Ui::OvrCommonTitleBar ui;
	bool is_pressed = false;
	QPoint pt_drag, pt_org_pos;
};
