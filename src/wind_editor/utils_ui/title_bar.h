#pragma once

#include <QWidget>

class TitleBar : public QWidget
{
	Q_OBJECT

public:
	enum TitleFlag
	{
		TF_NON_MASK = 0x00000000,
		TF_BTN_MINIMIZE = 0x00000001,
		TF_BTN_MAXIMIZE = 0x00000002,
		TF_BTN_CLOSE = 0x00000004,
		TF_BTN_HELP = 0x00000008,

		TF_BTN_COMMON = TF_BTN_MINIMIZE | TF_BTN_MAXIMIZE | TF_BTN_CLOSE,
		TF_BTN_ALL = TF_BTN_MINIMIZE | TF_BTN_MAXIMIZE | TF_BTN_CLOSE | TF_BTN_HELP,

		TF_FULL_MASK = 0xFFFFFFFF,

	};
	Q_DECLARE_FLAGS(TitleFlags, TitleFlag)

public:
	TitleBar(QWidget *parent = Q_NULLPTR, TitleFlags flags = TitleFlag::TF_BTN_COMMON);
	TitleBar(QWidget *parent, QString &title, TitleFlags flags = TitleFlag::TF_BTN_COMMON);
	TitleBar(QWidget *parent, QIcon &titleLogo, QString &title, TitleFlags flags = TitleFlag::TF_BTN_COMMON);
	~TitleBar();

public:
	void setTitle(QString &title);
	void setTilteLogo(QIcon &logo);

	void setUiFlags(TitleFlags flags);
	
protected:
	void paintEvent(QPaintEvent *event) override;
	void mouseDoubleClickEvent(QMouseEvent *event) override;
	void mousePressEvent(QMouseEvent *event) override;
	bool eventFilter(QObject *obj, QEvent *event) override;

private:
	void init_common_ui();
	void set_custom_style_sheet();

	void updateMaximize();
	void setupUi();
	void retranslateUi(QWidget * widget);

private Q_SLOTS:
	void on_btn_minimize_clicked();
	void on_btn_maximize_clicked();
	void on_btn_close_clicked();

private:
	//Ui::TitleBar ui;
	QHBoxLayout *horizontalLayout;
	QHBoxLayout *horizontalLayout_tl;
	QLabel *title_logo;
	QLabel *title_text;
	QSpacerItem *horizontalSpacer;
	QPushButton *btn_help;
	QPushButton *btn_minimize;
	QCheckBox* btn_maximize;
	QPushButton *btn_close;

	TitleFlags tilte_flags = TitleFlag::TF_BTN_COMMON;
};
