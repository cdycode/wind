#include "Header.h"
#include "ovr_dock_title_bar.h"

#include <QDockWidget>

////////////////////////////////////////////////////
// class OVRTMLTtitleBar

OVRDockTitleBar::OVRDockTitleBar(QWidget* parent, Qt::WindowFlags f) :QWidget(parent, f)
{
	const char* strTTStyle = "QWidget{background-color:rgb(62, 62, 62); color:rgb(204, 204, 204); font-family:\"微软雅黑\";}"	;
	setStyleSheet(strTTStyle);
	setFixedHeight(38);

	initUI();
}

void OVRDockTitleBar::initUI()
{
	h_layout = new QHBoxLayout;
	h_layout->setContentsMargins(0, 0, 0, 0);
	h_layout->setSpacing(0);
	setLayout(h_layout);

	h_layout->addStretch(0);

	auto sys_bar = new QToolBar;

	{
		auto action = sys_bar->addAction(QIcon(), "");
		action->setEnabled(false);
		action_max_restore = action;
		connect(action, &QAction::triggered, this, &OVRDockTitleBar::onMaxRestore, Qt::QueuedConnection);

		action = sys_bar->addAction(QIcon(getImagePath("icon_max.png")), "");
		action->setObjectName("float");
		action->setToolTip(QString::fromLocal8Bit("浮动窗口"));
		action_float = action;
		connect(action, &QAction::triggered, this, &OVRDockTitleBar::onDockOrFloat, Qt::QueuedConnection);
	}

	h_layout->addWidget(sys_bar);
}

QWidget* OVRDockTitleBar::setBarContext(QWidget* a_context)
{
	QWidget* result = nullptr;

	if (bar_context != a_context)
	{
		if (bar_context != nullptr)
		{
			h_layout->removeWidget(bar_context);
			result = bar_context;
		}

		h_layout->insertWidget(0, a_context);
		bar_context = a_context;
	}

	return result;
}

void OVRDockTitleBar::onDockOrFloat()
{
	auto dock_win = dynamic_cast<QDockWidget*>(parent());
	bool is_float = !dock_win->isFloating();
	dock_win->setFloating(is_float);

	if (is_float)
	{
		action_max_restore->setIcon(QIcon(getImagePath("pop_btn_biggest.png")));
		action_max_restore->setToolTip(QString::fromLocal8Bit("最大化"));
		action_max_restore->setEnabled(true);

		action_float->setToolTip(QString::fromLocal8Bit("停靠窗口"));
	}
	else
	{
		action_max_restore->setIcon(QIcon());
		action_max_restore->setEnabled(false);
	}
}

void OVRDockTitleBar::onMaxRestore()
{
	auto dock_win = dynamic_cast<QWidget*>(parent());
	if (dock_win->isMaximized()) 
	{
		dock_win->showNormal();
		action_max_restore->setIcon(QIcon(getImagePath("pop_btn_biggest.png")));
		action_max_restore->setToolTip(QString::fromLocal8Bit("最大化"));
	}
	else {
		dock_win->showMaximized();
		action_max_restore->setIcon(QIcon(getImagePath("pop_btn_small.png")));
		action_max_restore->setToolTip(QString::fromLocal8Bit("还原"));
	}
}

 void OVRDockTitleBar::paintEvent(QPaintEvent *event)
 {
 	ApplyWidgetStyleSheet(this);
	 QWidget::paintEvent(event);
 }


void OVRDockTitleBar::mouseDoubleClickEvent(QMouseEvent * event)
{
	if (dynamic_cast<QDockWidget*>(parent())->isFloating())
	{
		onMaxRestore();
		return;
	}

	QWidget::mouseDoubleClickEvent(event);
}