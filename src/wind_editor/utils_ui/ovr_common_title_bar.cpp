#include "Header.h"
#include "ovr_common_title_bar.h"

OvrCommonTitleBar::OvrCommonTitleBar(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	
}

void OvrCommonTitleBar::init()
{
	initUI();
	initStyle();
	initParentUI();

	ui.button_max->hide();
	ui.button_min->hide();
}

void OvrCommonTitleBar::initParentUI()
{
	parentWidget()->setWindowFlags(parentWidget()->windowFlags() & ~Qt::WindowContextHelpButtonHint | Qt::FramelessWindowHint);//| Qt::FramelessWindowHint
	parentWidget()->installEventFilter(this);
}

void OvrCommonTitleBar::initUI()
{
	setFixedHeight(41);

	ui.icon->setFixedWidth(ui.icon->height());
	auto app = static_cast<QApplication*>(QApplication::instance());
	ui.icon->setPixmap(app->windowIcon().pixmap(ui.icon->width(), ui.icon->height()));

	ui.title->setText(parentWidget()->windowTitle());
}

void OvrCommonTitleBar::initStyle()
{
	QString strTitleBarStyle = QString::fromLocal8Bit(
		"QWidget{color:rgb(160, 160, 160); background:rgb(39, 39, 39); font:14px \"΢���ź�\"; border-top:0px solid rgb(35, 35, 35); border-bottom:1px solid rgb(35, 35, 35); padding-bottom:2px;}"
		"QWidget:focus{outline: none;}"
		"QToolTip{color:rgb(48,48,48); background-color:rgb(204,204,204);}"
	);
	setStyleSheet(strTitleBarStyle);

	ui.button_min->init();
	ui.button_max->init();
	ui.button_close->init();

	ui.line->setFixedHeight(2);
	ui.line->setStyleSheet("background:rgb(80,80,80)");
}

OvrCommonTitleBar::~OvrCommonTitleBar()
{
}

void OvrCommonTitleBar::paintEvent(QPaintEvent *event)
{
	ApplyWidgetStyleSheet(this);
}

void OvrCommonTitleBar::mouseDoubleClickEvent(QMouseEvent *event)
{
	if (ui.button_max->isVisible())
	{
		ui.button_max->setChecked(!ui.button_max->isChecked());
		emit ui.button_max->clicked(ui.button_max->isChecked());
	}
}

void OvrCommonTitleBar::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		is_pressed = true;
		pt_drag = event->globalPos();
		pt_org_pos = parentWidget()->frameGeometry().topLeft();
	}
}

void OvrCommonTitleBar::mouseMoveEvent(QMouseEvent *event)
{
	if (is_pressed)
	{
		parentWidget()->move(pt_org_pos + event->globalPos() - pt_drag);
	}
}

void OvrCommonTitleBar::mouseReleaseEvent(QMouseEvent *event)
{
	is_pressed = false;
}

bool OvrCommonTitleBar::eventFilter(QObject * obj, QEvent * event)
{
	if (QEvent::WindowTitleChange == event->type())
	{
		ui.title->setText(parentWidget()->windowTitle());
		return true;
	}
	return QWidget::eventFilter(obj, event);
}

void OvrCommonTitleBar::on_button_min_clicked(bool)
{
	auto w = parentWidget();
	w->showMinimized();
}

void OvrCommonTitleBar::on_button_max_clicked(bool)
{
	auto w = parentWidget();
	w->isMaximized() ? w->showNormal() : w->showMaximized();
}

void OvrCommonTitleBar::on_button_close_clicked(bool)
{
	auto w = parentWidget();
	w->close();
}