#ifndef OVR_OPENGL_VIEW_H
#define OVR_OPENGL_VIEW_H

#include <QWidget>

class OvrQtOpenglContext;

class OvrOpenGLView : public QWidget
{
    Q_OBJECT
public:
	explicit OvrOpenGLView(QWidget *parent = 0, Qt::WindowFlags f = Qt::WindowFlags());
    ~OvrOpenGLView();

	OvrQtOpenglContext* renderContext();

	void addActor() {}

protected:
	void paintEvent(QPaintEvent *event);

	OvrQtOpenglContext* current_gl_context =  nullptr;
};

#endif // MAINWINDOW_H
