#pragma once

#include <QWidget>
#include <QDockWidget>

class OvrSceneEditorEx :	public QWidget
{
	Q_OBJECT
public:
	explicit OvrSceneEditorEx(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	~OvrSceneEditorEx();

protected:
	virtual void mousePressEvent(QMouseEvent *event) override;
	virtual void mouseReleaseEvent(QMouseEvent *e) override;
	virtual void mouseMoveEvent(QMouseEvent *e) override;
	virtual void keyPressEvent(QKeyEvent *event) override;
	virtual void keyReleaseEvent(QKeyEvent *event) override;
	virtual void focusInEvent(QFocusEvent *event) override;
	virtual void focusOutEvent(QFocusEvent *event) override;
	virtual void resizeEvent(QResizeEvent *event) override;

	virtual void dragEnterEvent(QDragEnterEvent *event) override;
	virtual void dragMoveEvent(QDragMoveEvent *event) override;
	virtual void dropEvent(QDropEvent *event) override;
	virtual void dragLeaveEvent(QDragLeaveEvent *event) override;

	void paintEvent(QPaintEvent *e) override;

	bool is_left_mouse_down = false;
	bool is_right_mouse_down = false;
	bool is_mouse_move = true;

	QPoint pt_mouse_right_last;

private:
	void CopyActor();
	void RemoveActor();
	void RenameActor();

private:
	float mouse_x_pos = 0;
	float mouse_y_pos = 0;

};

class OvrSceneEditorPane : public QDockWidget
{
	Q_OBJECT
public:
	explicit OvrSceneEditorPane(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	~OvrSceneEditorPane();

public slots:
	void actionTriggered(QAction *action);

protected:
	void paintEvent(QPaintEvent *event);

	void onPreview(QAction* a_action);
	void onMove(QAction* a_action);
	void onRotate(QAction* a_action);
	void onScale(QAction* a_action);

private:
	void init_custom_ui();

	void init_dock_title_bar();
	void init_dock_content_widget();

	QAction *move_action = nullptr;
	QAction *rotate_action = nullptr;
	QAction *scale_action = nullptr;
};

