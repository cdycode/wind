#pragma once

#include <QStyledItemDelegate>
#include <QLineEdit>


class OvrResListItemDelegate : public QStyledItemDelegate, public OvrEventSender
{
public:
	OvrResListItemDelegate(QObject *parent = Q_NULLPTR);
	~OvrResListItemDelegate();

	virtual QString senderName() const override { return "OvrResListItemDelegate"; }

	virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
	virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;
	virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
};
