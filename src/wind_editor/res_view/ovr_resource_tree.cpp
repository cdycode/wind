#include "Header.h"
#include "ovr_resource_tree.h"
#include "ovr_resource_tree_item.h"
#include "ovr_resource_menu_manger.h"

OvrResourceTree::OvrResourceTree(QWidget *parent):QTreeView(parent)
{
	setMinimumWidth(120);

	setObjectName("OvrResourceTree");
	setHeaderHidden(true);
	set_custom_style_sheet();

	connect(this, &QTreeView::clicked, [this](const QModelIndex &index) {
		onItemClicked(index);
	});

	QStringList e = { "onOpenResSubDir", "onLocateRes", "event_create_folder",
		"onEditResTreeItem", "onRenameResSubDir", "onRenamResCurDir",
		"onRenameResItemFinish", "onRemoveResTreeItem", "onRemoveResSubDir" };

	registerEventListener(e);
}

void OvrResourceTree::init()
{
	connect(model(), &QAbstractItemModel::modelReset, [this]() {
		onModelReset();
	});
}

void OvrResourceTree::onItemClicked(const QModelIndex &index)
{
	if (!first_clicked_record_index.isValid()) 
	{
		OvrResourceTreeItem* item = static_cast<OvrResourceTreeItem*>(index.internalPointer());
		if (item != nullptr && !item->isKeyNode()) 
		{
			first_clicked_record_index = index;
		}
	}
	else if (first_clicked_record_index == index) 
	{
		editItem(index);
	}
}

void OvrResourceTree::currentChanged(const QModelIndex &current, const QModelIndex &previous)
{
	QTreeView::currentChanged(current, previous);

	first_clicked_record_index = QModelIndex();

	QString path;
	int dir_type = -1;
	OvrResourceTreeItem* item = static_cast<OvrResourceTreeItem*>(current.internalPointer());
	if (item != nullptr)
	{
		path = item->data(1).toString();
		dir_type = item->dirType();
	}

	notifyEvent("onResDirEntered", path, dir_type);
}

void OvrResourceTree::contextMenuEvent(QContextMenuEvent *event)
{
	QModelIndex index = indexAt(event->pos());

	if (index.isValid())
	{
		auto memu_mgr = OvrResourceMenuManager::getInst();

		OvrResourceTreeItem* item = static_cast<OvrResourceTreeItem*>(index.internalPointer());
		memu_mgr->initResTreeMenu(item->dirType(), item->isKeyNode());

		memu_mgr->show(cursor().pos());
	}
}

void OvrResourceTree::set_custom_style_sheet()
{
	QString strDefaultStyle = QString::fromLocal8Bit(
		"QTreeView{background-color:rgb(39, 39, 39); color:rgb(192, 192, 192); border-left:1px solid rgb(24, 24, 24); border:none;}"
		"QTreeView::item{height:28px; border-top:1px solid rgb(26, 26, 26); color:rgb(192, 192, 192);}"
		"QTreeView::item:selected{background-color:rgb(64, 64, 64);}"
		"QTreeView::branch{height:28px; border-top:1px solid rgb(26, 26, 26);}"
		"QTreeView::branch:selected {background-color:rgb(64, 64, 64);}"
	);

	QTextStream qtsTemp(&strDefaultStyle);
	qtsTemp << "QTreeView::branch:has-children:closed{image:url(" << getImagePath("icon_closefile_min.png") << ");}"
		<< "QTreeView::branch:has-children:open{image:url(" << getImagePath("icon_openfile_min.png") << ");}"
		;

	setStyleSheet(strDefaultStyle);
}


void OvrResourceTree::onModelReset()
{
	expandToDepth(1);
	auto first = model()->index(0, 0);
	if (first.isValid())
	{
		setCurrentIndex(first);
	}
	else
	{
		notifyEvent("onResDirEntered", "", -1);
	}
}

void OvrResourceTree::onOpenSubDir(QString a_dir)
{
	QModelIndex index = currentIndex();
	assert(index.isValid());

	OvrResourceTreeItem* item = static_cast<OvrResourceTreeItem*>(index.internalPointer());
	assert(item != nullptr);

	int cnt = item->childCount();
	for (int i = 0; i < cnt; ++i)
	{
		OvrResourceTreeItem* child = dynamic_cast<OvrResourceTreeItem*>(item->child(i));
		if (child->data(1) == a_dir)
		{
			QModelIndex child_index = index.child(i, 0);
			setCurrentIndex(child_index);
			return;
		}
	}	
}

void OvrResourceTree::onLocateRes(const QString & a_path)
{
	if (a_path.isEmpty())
		return;

	int iIndex = a_path.lastIndexOf(QString::fromLocal8Bit("/"));
	QString res_name = a_path.right(a_path.length() - iIndex - 1);
	QString res_dir = a_path.left(iIndex + 1);

	locateDir(res_dir);

	notifyEvent("onLocateFile", res_name, 0);
}

void OvrResourceTree::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{
	if (a_event_name == "onLocateRes")
	{
		onLocateRes(a_param1.toString());
	}
	else if (a_event_name == "event_create_folder")
	{
		if (currentIndex().isValid())
		{
			bool is_tree = a_param1.toBool();
			notifyEvent("event_add_sub_folder", currentIndex(), is_tree, false);
		}
	}
	else if (a_event_name == "onEditResTreeItem")
	{
		editItem(a_param1.toModelIndex());
	}
	else if (a_event_name == "onOpenResSubDir")
	{
		onOpenSubDir(a_param1.toString());
	}
	else if (a_event_name == "onRenamResCurDir")
	{
		assert(a_param1.toModelIndex() == currentIndex());

		onRenamResCurDir(a_param2.toString());
	}
	else if (a_event_name == "onRenameResSubDir")
	{
		if (currentIndex().isValid())
		{
			onRenameSubItem(currentIndex(), a_param1.toString(), a_param2.toString());
		}
	}
	else if (a_event_name == "onRenameResItemFinish")
	{
		onRenameResItemFinish(a_param1.toString(), a_param2.toBool());
	}
	else if (a_event_name == "onRemoveResTreeItem")
	{
		if (currentIndex().isValid())
		{
			notifyEvent("onRemoveAssertDir", currentIndex(), 0, false);
		}
	}
	else if (a_event_name == "onRemoveResSubDir")
	{
		if (currentIndex().isValid())
		{
			onRemoveResSubDir(currentIndex(), a_param1.toString());
		}
	}
}

void OvrResourceTree::onRenamResCurDir(const QString& a_new_name)
{
	if (currentIndex().isValid())
	{
		closePersistentEditor(currentIndex());

		if (!a_new_name.isEmpty())
		{
			renameItem(currentIndex(), a_new_name, true);
		}
	}
}

void OvrResourceTree::onRemoveResSubDir(const QModelIndex& a_index, const QString& a_sub_path)
{
	assert(a_index.isValid() && !a_sub_path.isEmpty());

	auto item = static_cast<OvrResourceTreeItem*>(a_index.internalPointer());
	assert(item != nullptr);

	//search sub dir to be removed
	for (auto i = 0; i < item->childCount(); ++i)
	{
		auto child_item = item->child(i);
		if (child_item->data(1) == a_sub_path)
		{
			notifyEvent("onRemoveAssertDir", a_index.child(i, 0), true, false);
			break;
		}
	}
}

bool OvrResourceTree::onRenameSubItem(const QModelIndex& a_index, const QString& a_sub_path, const QString& a_new_name)
{
	assert(a_index.isValid());

	if (a_new_name.isEmpty())
	{
		return false;
	}

	auto item = static_cast<OvrResourceTreeItem*>(a_index.internalPointer());
	assert(item != nullptr);

	//search sub dir to be renamed
	for (auto i = 0; i < item->childCount(); ++i)
	{
		auto child_item = item->child(i);
		if (child_item->data(1) == a_sub_path)
		{
			return renameItem(a_index.child(i, 0), a_new_name, false);
		}
	}

	return false;
}

bool OvrResourceTree::renameItem(const QModelIndex& a_index, const QString& a_new_name, bool is_current)
{
	auto item = static_cast<OvrResourceTreeItem*>(a_index.internalPointer());

	QString old_path = item->data(1).toString();

	int index = old_path.lastIndexOf("/");
	assert(index >= 0);
	QString new_path = old_path.left(index + 1) + a_new_name;

	if (new_path == old_path)
	{
		return false;
	}

	QString event_name = is_current ? "onRenameCurAssertDir" : "onRenameSubAssertDir";
	notifyEvent(event_name, a_index, new_path, false);

	return true;
}

void OvrResourceTree::onRenameResItemFinish(const QString& a_new_path, bool a_is_sub)
{
	auto cur_index = currentIndex();
	if (cur_index.isValid())
	{
		auto item = static_cast<OvrResourceTreeItem*>(cur_index.internalPointer());
		//search sub dir to be renamed
		for (auto i = 0; i < item->childCount(); ++i)
		{
			auto item_path = item->child(i)->data(1);
			if (item_path == a_new_path)
			{
				auto child_index = cur_index.child(i, 0);
				if (!a_is_sub)
				{
					setCurrentIndex(child_index);
				}
				setExpanded(child_index, true);
				break;;
			}
		}
	}
}

void OvrResourceTree::locateDir(const QString &a_dir_path)
{
	// 不要最后一个斜杠，因为会产生空串
	QString res_dir = a_dir_path.endsWith("/") ? a_dir_path.left(a_dir_path.length() - 1) : a_dir_path;
	QStringList res_dir_node = res_dir.split("/");

	//进行递归搜索, 返回值：true，说明已找到，结束函数递归； false，说明未找到， 开始下次递归
	std::function<bool (const QModelIndex&, const QStringList&, int&)> search_tree_fun =
		[this, &search_tree_fun](const QModelIndex &parIndex,const QStringList &checkData, int &nTreeDepth) -> bool {
		nTreeDepth++;

		for (int i = 0; ; i++) {
			QModelIndex currentIndex = this->model()->index(i, 0, parIndex);
			if (!currentIndex.isValid())
				break;														// 已搜索到本层最后， 未找到

			OvrResourceTreeItem * item = static_cast<OvrResourceTreeItem*>(currentIndex.internalPointer());
			assert(item != nullptr);
			//QString data0 = item->data(0).toString();
			QString data1 = item->data(1).toString();
			int iIndex = data1.lastIndexOf(QString::fromLocal8Bit("/"));
			QString cur_name = data1.right(data1.length() - iIndex - 1);
			if (checkData[nTreeDepth] == cur_name) {
				if (checkData.size() <= nTreeDepth + 1) {
					this->setCurrentIndex(currentIndex);					// 已找到, 选中这个结点
					nTreeDepth--;
					return true;
				}
				if (search_tree_fun(currentIndex, checkData, nTreeDepth)) { // 进入下级搜索
					this->expand(currentIndex);								// 已找到, 展开结点
					nTreeDepth--;
					return true;
				}
			} // if(==)
		} // for(int i)

		nTreeDepth--;
		return false;
	};

	int nTreeDepth = -1;
	QModelIndex mi = this->rootIndex();
	search_tree_fun(mi, res_dir_node, nTreeDepth);
}

void OvrResourceTree::searchTree(const QModelIndex& a_index, const QString& a_text)
{
	assert(a_index.isValid());
	OvrResourceTreeItem* item = static_cast<OvrResourceTreeItem*>(a_index.internalPointer());
	qDebug() << item->data(0).toString();
}

void OvrResourceTree::editItem(QModelIndex a_index)
{
	qDebug() << "OvrResourceTree::editItem";

	if (!a_index.isValid())
	{
		a_index = currentIndex();
	}
	else
	{
		setCurrentIndex(a_index);
	}

	openPersistentEditor(a_index);
}

