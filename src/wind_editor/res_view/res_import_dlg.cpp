#include "Header.h"
#include "res_import_dlg.h"
#include "../utils_ui/title_bar.h"

#include <QScrollBar>

ResImportDlg::ResImportDlg(QWidget *parent)
	: QDialog(parent)
	, m_bCanceled(false)
{
	ui.setupUi(this);

	init_custom_ui();
	set_custom_style_sheet();
	preset_connects();
}

ResImportDlg::~ResImportDlg()
{
}

void ResImportDlg::closeEvent(QCloseEvent * e)
{
	//on_cancel_btn_clicked();

	QDialog::closeEvent(e);
}


void ResImportDlg::init_custom_ui()
{
	setWindowFlags(windowFlags() | Qt::FramelessWindowHint);

	ui.title_bar->setUiFlags(TitleBar::TitleFlag::TF_BTN_CLOSE);
	//ui.title_bar->setTitle(QString::fromLocal8Bit("添加标记"));

	installEventFilter(ui.title_bar);

	setWindowTitle(QString::fromLocal8Bit("资源导入"));

}

void ResImportDlg::set_custom_style_sheet()
{
	QString strDlgStyle = QString::fromLocal8Bit(
		"QWidget:focus{ outline: none;}"
		"QWidget{background-color: rgb(48,48,48);color:rgb(204,204,204);font-family:\"微软雅黑\";}"
		"QLabel{font-size:14px;}"
		"QProgressBar, QLineEdit{background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); font-size:12px; selection-background-color: rgb(165, 165, 165); selection-color: rgb(32, 32, 32);}"
		"QPushButton{background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); color: rgb(205, 205, 205); border: 0px solid rgb(23, 43, 54); border-radius: 2px;}"
		"QPushButton:hover{background-color:rgb(119, 119, 119);}"
		"QPushButton:pressed {background-color:rgb(72, 72, 72);}"
		"QProgressBar {border-radius: 2px;}"
		"QProgressBar::chunk{background-color: #BD7C12; width: 20px;}"

		"QListView {color: rgb(204, 204, 204); background-color: rgb(39, 39, 39); border:1px solid  rgb(30, 30, 30); border-radius: 0px;}"
		"QListView::item {height:25px; border-bottom:1px solid rgba(151,151,151, 38);}"
		"QListView::item:hover {background-color: rgb(40, 40, 40); color: rgb(205, 205, 205);}"
		"QListView::item:selected {background-color: rgb(43, 43, 43); selection-color: rgb(205, 205, 205);}"
	);
	this->setStyleSheet(strDlgStyle);

	QString strTitleBarStyle = QString::fromLocal8Bit(
		"QWidget{background-color: rgb(48,48,48); color:rgb(204, 204, 204); font-family:\"微软雅黑\"; border:0px solid rgb(76, 76, 76);}"
		"QLabel{font:normal 16px; font-family:\"微软雅黑\";}"
	);
	ui.title_bar->setStyleSheet(strTitleBarStyle);

	//
	QString strLineStyle = QString::fromLocal8Bit("background-color: rgb(59, 59, 59); border-top-color: rgb(38, 38, 38); border-bottom: 1px solid rgb(48, 48, 48);");
	ui.line->setStyleSheet(strLineStyle);

}

void ResImportDlg::preset_connects()
{
	//QObject::connect(ui.close_btn, SIGNAL(clicked()), this, SLOT(on_cancel_btn_clicked()));
}


//int ResImportDlg::ImportFile(const char* a_asset_file, const char* a_out_dir)
//{
//	assert(a_out_dir != nullptr && a_asset_file != nullptr);
//	ui.target->setText(QString::fromLocal8Bit(a_asset_file));
//	ui.output->setText(QString::fromLocal8Bit(a_out_dir));
//	ui.progress->setRange(0, 100);
//	ui.progress->setValue(0);
//
//	WorkerThread *workerThread = new WorkerThread();
//	workerThread->asset_file = a_asset_file;
//	workerThread->out_dir = a_out_dir;
//
//	connect(workerThread, &WorkerThread::enterRun, this, &ResImportDlg::startListen);
//	connect(workerThread, &WorkerThread::resultReady, this, &ResImportDlg::handleResults);
//	connect(workerThread, &WorkerThread::finished, workerThread, &QObject::deleteLater);
//	workerThread->start();
//
//	return exec();
//}

int ResImportDlg::ImportFile(const QStringList & paths_list, const QString& a_out_dir)
{
	assert(a_out_dir != nullptr && paths_list.size() != 0);


	//ui.target->setText(QString::fromLocal8Bit(a_asset_file));
	ui.target_files_list->addItems(paths_list);
	ui.output->setText(a_out_dir);
	ui.progress->setRange(0, 100);
	ui.progress->setValue(0);

	std::vector<std::string> vct_paths_list;
	for (auto & path : paths_list) 
	{
		QByteArray qadPath = path.toLocal8Bit();
		vct_paths_list.push_back(qadPath.constData());
	}

	workerThread = new WorkerThread();
	workerThread->vct_asset_files = vct_paths_list;
	QByteArray qbaOutDir = a_out_dir.toLocal8Bit();
	workerThread->out_dir = qbaOutDir.constData();

	connect(workerThread, &WorkerThread::enterRun, this, &ResImportDlg::startListen);
	connect(workerThread, &WorkerThread::resultReady, this, &ResImportDlg::handleResults);
	connect(workerThread, &WorkerThread::finished, workerThread, &QObject::deleteLater);
	connect(workerThread, &WorkerThread::file_conflict_notify, this, &ResImportDlg::handle_file_conflict);

	workerThread->start();

	return exec();
}

void ResImportDlg::startListen()
{
	timer_id = startTimer(100, Qt::VeryCoarseTimer);
}

void ResImportDlg::timerEvent(QTimerEvent *event)
{
	int per = ovr_project::OvrProject::GetIns()->GetAssetManager()->GetImportProgress();
	ui.progress->setValue(per);
}

void ResImportDlg::handleResults(bool a_is_ok)
{
	killTimer(timer_id);
	int ret = m_bCanceled? -1: a_is_ok ? 1 : 0;
	done(ret);
}

void ResImportDlg::on_cancel_btn_clicked()
{
	m_bCanceled = true;
	ovr_project::OvrProject::GetIns()->GetAssetManager()->CancelImport();
}

void ResImportDlg::handle_file_conflict(const QString & file_path)
{
	if (workerThread->m_all_covered) {
		workerThread->m_current_covered = true;
		workerThread->notify_ok();
		return;
	}

	if (workerThread->m_no_all_covered) {
		workerThread->m_current_covered = false;
		workerThread->notify_ok();
		return;
	}
	QString strShowInfo = QString::fromLocal8Bit("文件: ") + file_path + QString::fromLocal8Bit("已存在,是否覆盖?");
	QMessageBox::StandardButtons btns = QMessageBox::YesAll | QMessageBox::NoAll | QMessageBox::Yes | QMessageBox::No;
	QMessageBox box(QMessageBox::Warning, QString::fromLocal8Bit("文件覆盖提示"), strShowInfo, btns, this);
	box.setButtonText(QMessageBox::YesAll, QString::fromLocal8Bit("覆盖所有"));
	box.setButtonText(QMessageBox::NoAll, QString::fromLocal8Bit("跳过所有"));
	box.setButtonText(QMessageBox::Yes, QString::fromLocal8Bit("覆盖"));
	box.setButtonText(QMessageBox::No, QString::fromLocal8Bit("跳过"));

	int result = box.exec();
	if (QMessageBox::YesAll == result) {
		workerThread->m_all_covered = true;
		workerThread->m_current_covered = true;
	}
	else if (QMessageBox::NoAll == result) {
		workerThread->m_no_all_covered = true;
		workerThread->m_current_covered = false;
	}
	else if (QMessageBox::Yes == result) {
		workerThread->m_current_covered = true;
	}
	else if (QMessageBox::No == result) {
		workerThread->m_current_covered = false;
	}
	else {
		workerThread->m_current_covered = false;
	}

	workerThread->notify_ok();
}


void WorkerThread::run()
{
	emit enterRun();

	auto asset_mgr = ovr_project::OvrProject::GetIns()->GetAssetManager();

	std::function<bool(const char *, bool)> file_notify_fun =
		std::bind(&WorkerThread::file_notify_callback,
		this, std::placeholders::_1, std::placeholders::_2);
	bool is_ok = asset_mgr->ImportFiles(vct_asset_files, out_dir, file_notify_fun);

	emit resultReady(is_ok);
}

void WorkerThread::notify_ok()
{
	cv.notify_one();
}

void WorkerThread::wait_ready()
{
	std::unique_lock<std::mutex> lk(mgr_mutex);
	cv.wait(lk);   // wait set m_current_covered
}

bool WorkerThread::file_notify_callback(const char* file_path, bool is_conflicted)
{
	if (!is_conflicted)
		return true;

	if (m_all_covered)
		return true;
	if (m_no_all_covered)
		return false;

	QString srFilePath = QString::fromLocal8Bit(file_path);
	emit file_conflict_notify(srFilePath);

	wait_ready();

	return m_current_covered;
}
