#include "Header.h"
#include "ovr_restree_item_delegate.h"
#include "ovr_resource_tree_item.h"
#include "ovr_resource_tree_model.h"
//#include "resource_tree_item.h"
#include "ovr_resource_tree.h"


//OvrResItemLineEdit::OvrResItemLineEdit(QWidget *parent)
//	:QLineEdit(parent)
//{
//}
//
//OvrResItemLineEdit::OvrResItemLineEdit(const QString & def_text, QWidget *parent)
//	: QLineEdit(def_text, parent)
//{
//
//}
//
//OvrResItemLineEdit::~OvrResItemLineEdit()
//{
//}
//
//void OvrResItemLineEdit::focusOutEvent(QFocusEvent *e)
//{
//	emit mouse_out_focus();
//}


///////////////////////////////////////////////////////////////////////////////////////////////////////
OvrResTreeItemDelegate::OvrResTreeItemDelegate(QObject *parent):QStyledItemDelegate(parent)
{
	item_image = new QImage(getImagePath("icon_closefile_min.png"));
	icon_size.setWidth(item_image->width());
	icon_size.setHeight(item_image->height());
}


void OvrResTreeItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	assert(index.column() == 0);

	OvrResourceTreeItem* item = static_cast<OvrResourceTreeItem*>(index.internalPointer());
	if (item->childCount() == 0)
	{
		QRect rc(option.rect.left() - icon_size.width()-1,
			(option.rect.height() - icon_size.height()) / 2 + option.rect.top(),
			icon_size.width(), icon_size.height());

		painter->drawImage(rc, *item_image);
	}
	QStyledItemDelegate::paint(painter, option, index);
}

QWidget * OvrResTreeItemDelegate::createEditor(QWidget * parent, const QStyleOptionViewItem & option, const QModelIndex & index) const
{
	OvrResourceTreeItem * item = static_cast<OvrResourceTreeItem*>(index.internalPointer());

	QLineEdit * editor_box = new QLineEdit(parent);
	editor_box->setMaxLength(100);
	QRegExp regExp = QRegExp("[^\\\\/*:|?\"\\s]*");
	QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExp, editor_box);
	editor_box->setValidator(portRegExpValidator);
	editor_box->setContextMenuPolicy(Qt::NoContextMenu);
	editor_box->setFocus();

	return editor_box;
}

void OvrResTreeItemDelegate::setEditorData(QWidget * editor, const QModelIndex & index) const
{
	auto item = static_cast<OvrResourceTreeItem*>(index.internalPointer());
	QLineEdit * line_editor = dynamic_cast<QLineEdit *>(editor);
	line_editor->setText(item->data(0).toString());
}

void OvrResTreeItemDelegate::setModelData(QWidget * editor, QAbstractItemModel * model, const QModelIndex & index) const
{
	QLineEdit * line_editor = dynamic_cast<QLineEdit *>(editor);
	OvrResourceTreeItem * item = static_cast<OvrResourceTreeItem*>(index.internalPointer());

	qDebug() << "OvrResTreeItemDelegate::setModelData, " <<item->data(1).toString() << " => " << line_editor->text();

	notifyEvent("onRenamResCurDir", index, line_editor->text(), false);
	//QStyledItemDelegate::setModelData(editor, model, index);
}
