#pragma once

#include <QDialog>
#include <QThread>

#include "ui_res_import_dlg.h"

class WorkerThread;
class ResImportDlg : public QDialog
{
	Q_OBJECT

public:
	ResImportDlg(QWidget *parent = Q_NULLPTR);
	~ResImportDlg();

	// -1. 被取消； 0. 导入失败； 1. 成功
	//int ImportFile(const char* a_asset_file, const char* a_out_dir);
	int ImportFile(const QStringList &paths_list, const QString& a_out_dir);

Q_SIGNALS:
	void operate(const QString &);


public Q_SLOTS:
	void handleResults(bool a_is_ok);
	void startListen();
	void on_cancel_btn_clicked();

	void handle_file_conflict(const QString& file_path);

private:
	void init_custom_ui();
	void set_custom_style_sheet();
	void preset_connects();


protected:
	//void mousePressEvent(QMouseEvent *event);
	//void mouseMoveEvent(QMouseEvent *event);
	//void mouseReleaseEvent(QMouseEvent *event);
	//void changeEvent(QEvent *event);

	virtual void closeEvent(QCloseEvent *e) override;

	void timerEvent(QTimerEvent *event) override;
	int timer_id;

private:
	Ui::ResImportDlg ui;

private:
	QPoint m_last;
	bool m_bIsPushed;

	bool m_bCanceled;

	WorkerThread *workerThread = nullptr;

};

class WorkerThread : public QThread
{
	Q_OBJECT
public:
	explicit WorkerThread(QObject *parent = Q_NULLPTR) :QThread(parent)
	{}


	std::vector<std::string> vct_asset_files;
	const char* asset_file;
	const char* out_dir;

	// 本次处理，当前文件的覆盖处理；
	bool m_current_covered = false;

	// 本次处理是否都进行覆盖处理
	bool m_all_covered = false;
	// 本次处理是否都不进行覆盖处理
	bool m_no_all_covered = false;


public:
	void notify_ok();
	void wait_ready();

protected:
	void run() Q_DECL_OVERRIDE;

Q_SIGNALS:
	void resultReady(bool a_is_ok);
	void enterRun();
	void file_conflict_notify(const QString& file_path);

private:
	bool file_notify_callback(const char* file_path, bool is_conflicted);

private:
	// 通知等待
	std::mutex mgr_mutex;
	std::condition_variable cv;

};