#include "Header.h"
#include "ovr_res_manager.h"

#include "ovr_resource_tree.h"
#include "ovr_resource_tree_model.h"
#include "ovr_restree_item_delegate.h"
#include "ovr_resource_list_widget.h"
#include "ovr_res_list_item_delegate.h"

#include "../utils_ui/size_dock_widget.h"

static OvrResManager* g_res_mgr_inst = nullptr;

OvrResManager* OvrResManager::getInst()
{
	if (g_res_mgr_inst == nullptr)
	{
		new OvrResManager();
	}
	return g_res_mgr_inst;

}

OvrResManager::OvrResManager(QWidget* parent, Qt::WindowFlags f)
	:QWidget(parent, f)
{
	g_res_mgr_inst = this;

	initUI();
}

OvrResManager::~OvrResManager()
{}

void OvrResManager::paintEvent(QPaintEvent *e)
{
	ApplyWidgetStyleSheet(this);
}

void OvrResManager::initUI()
{
	QVBoxLayout* v_layout = new QVBoxLayout(this);
	v_layout->setSpacing(1);
	v_layout->setMargin(0);

	QSplitter * splitter = new QSplitter;
	splitter->setOrientation(Qt::Horizontal);
	v_layout->addWidget(splitter);

	res_tree = new OvrResourceTree(splitter);
	res_tree->setItemDelegate(new OvrResTreeItemDelegate);
	res_tree->setModel(OvrResourceTreeModel::getInst());
	res_tree->init();

	list_wnd = new OvrResourceListWidget(splitter);
	list_wnd->setItemDelegate(new OvrResListItemDelegate);
}

////////////////////////////////////////////////////////
//class OvrResManagerDock

OvrResManagerDock::OvrResManagerDock(QWidget* parent, Qt::WindowFlags f):QDockWidget(QString::fromLocal8Bit("资源管理器"), parent, f)
{
	initStyle();
	initDockBar();
	setWidget(new OvrResManager);

	QStringList e = { "onResBarAction" };
	registerEventListener(e);
}

void OvrResManagerDock::initStyle()
{
	setFeatures(QDockWidget::AllDockWidgetFeatures);
	setAllowedAreas(Qt::LeftDockWidgetArea);

	QSizePolicy size_policy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	size_policy.setHorizontalStretch(0);
	size_policy.setVerticalStretch(0);
	size_policy.setHeightForWidth(sizePolicy().hasHeightForWidth());
	setSizePolicy(size_policy);
}

void OvrResManagerDock::initDockBar()
{
	QWidget * title_pane = new QWidget();
	title_pane->setContentsMargins(0, 0, 0, 0);
	setTitleBarWidget(title_pane);

	QHBoxLayout * h_layout = new QHBoxLayout(title_pane);
	h_layout->setSpacing(0);
	h_layout->setContentsMargins(0, 0, 0, 0);

	h_layout->addWidget(new OvrResBar);
}

void OvrResManagerDock::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{
	if (a_event_name == "onResBarAction")
	{
		auto action = static_cast<QAction*>(a_param1.value<void*>());
		if (action->objectName() == "res_zoom")
		{
			setFloating(!isFloating());
		}
	}
}
////////////////////////////////////////////////////////
//class OvrResBar

OvrResBar::OvrResBar(QWidget *parent) : QToolBar(parent)
{
	QAction* action = addAction(QIcon(""), QString::fromLocal8Bit("创建"));
	action->setObjectName("res_create");
	action->setToolTip(QString::fromLocal8Bit("创建..."));

	action = addAction(QIcon(""), QString::fromLocal8Bit("导入"));
	action->setObjectName("res_import");
	action->setToolTip(QString::fromLocal8Bit("导入可用资源"));

	QLabel* space = new QLabel();
	space->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	action = addWidget(space);
	action->setDisabled(true);

	action = addAction(QIcon(getImagePath("icon_search_max.png")), "");
	action->setObjectName("res_search");
	action->setToolTip(QString::fromLocal8Bit("搜素"));

	action = addAction(QIcon(getImagePath("icon_listone.png")), "");
	action->setObjectName("res_view_mode");
	action->setToolTip(QString::fromLocal8Bit("切换视图模式"));

	action = addAction(QIcon(getImagePath("icon_max.png")), "");
	action->setObjectName("res_zoom");
	action->setToolTip(QString::fromLocal8Bit("放大窗口"));

	connect(this, &QToolBar::actionTriggered, [this](QAction* a_action) {
		notifyEvent("onResBarAction", QVariant::fromValue((void*)a_action), 0);
	});
}