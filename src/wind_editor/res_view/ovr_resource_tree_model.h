#pragma once

#include <QAbstractItemModel>

#include "ovr_resource_tree_item.h"

class OvrResourceTreeItem;
class QAction;

class OvrResourceTreeModel :public QAbstractItemModel, public OvrEventReceiver, public OvrEventSender
{
	Q_OBJECT
public:
	static OvrResourceTreeModel* getInst();

	explicit OvrResourceTreeModel(QObject *parent = 0);
	~OvrResourceTreeModel();

	virtual QString senderName() const override { return "OvrResourceTreeModel"; }

	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;
	virtual QString name() override { return "OvrResourceTreeModel"; }

	//for reading
	QVariant data(const QModelIndex &index, int role) const override;
	Qt::ItemFlags flags(const QModelIndex &index) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
	QModelIndex parent(const QModelIndex &index) const override;
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;

	const QString& projectDir() { return prj_dir; }
	QString getContentDir();
	const QString& getRootDir();

protected:
	void onProjectOpen(bool a_is_ok, QString a_prj_dir);
	void searchProjectDir(const QString& a_prj_dir);
	
	bool removeAssertDir(const QModelIndex& a_index);
	void removeResInDir(QString a_res_dir, int a_dir_type);

	void onRemoveResAssetFile(const QStringList& a_target, int a_dir_type);
	bool removeResAsset(QString a_target_id, int a_type);

	bool onRenameAssertFile(const QStringList& a_files, int a_type);
	bool onRenameAssertDir(const QModelIndex& a_index, const QString& a_new_path);
	void renameResInDir(OvrResourceTreeItem* a_item, const QString& a_new_path);
	bool renameResItem(const char* a_item_id, const char* a_new_file_path, int a_type);

	void searchProjectDir(OvrResourceTreeItem* node, const QString& path);
	void makeSureKeyDirExist(const QString& a_prj_dir);

	void onAddSubFolder(const QModelIndex& a_index, bool is_tree_menu);
	QModelIndex addSubDir(const QModelIndex& a_index, QString& a_dir_name);

	void onCreateAsset(const QString& a_action, const QString& a_out_dir);
	void createMat(const QString& a_out_dir_str);
	void createScene(const QString& a_out_dir_str);
	void createSeq(const QString& a_out_dir_str);

	void getResItemsInDir(const QString& a_path, int a_type, QList<OvrDataItemString>& a_files);
	void getResItemsInDir(const QString& a_path, const std::list<ovr_project::OvrProjectFileInfo>& a_file_infos, QList<OvrDataItemString>& a_files);

private:
	OvrResourceTreeItem* root_Item;
	QString prj_dir;

	const static QString cs_default_folder_prefix;
	const static QString cs_default_mat_prefix;
	const static QString cs_default_plot_prefix;
	const static QString cs_default_seq_prefix;
	const static QString cs_default_sence_prefix;
};

