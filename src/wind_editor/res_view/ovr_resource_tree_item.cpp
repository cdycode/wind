#include "Header.h"
#include "ovr_resource_tree_item.h"

#include "ovr_res_manager.h"

OvrResourceTreeItem::OvrResourceTreeItem(const QList<QVariant> &data, OvrResourceTreeItem* parent) : QObject(parent)
{
	m_parentItem = parent;
    m_itemData = data;
	if (parent != nullptr)
	{
		parent->appendChild(this);
	}
}

OvrResourceTreeItem::~OvrResourceTreeItem()
{
	clearChild();
}

void OvrResourceTreeItem::clearChild()
{
	if (m_childItems.size() > 0)
	{
		qDeleteAll(m_childItems);
		m_childItems.clear();
	}
}

void OvrResourceTreeItem::appendChild(OvrResourceTreeItem *item)
{
    m_childItems.append(item);
}

int OvrResourceTreeItem::childCount() const
{
	return m_childItems.count();
}

OvrResourceTreeItem *OvrResourceTreeItem::child(int row)
{
    return m_childItems.value(row);
}

void OvrResourceTreeItem::removeChild(int a_row)
{
	assert(a_row >= 0 && a_row < m_childItems.count());
	OvrResourceTreeItem * item = m_childItems.value(a_row);
	m_childItems.removeAt(a_row);
	item->deleteLater();
}

int OvrResourceTreeItem::columnCount() const
{
    return m_itemData.count();
}

QVariant OvrResourceTreeItem::data(int column) const
{
    return m_itemData.value(column);
}

void OvrResourceTreeItem::setData(int column, const QVariant& data)
{
	if (column < 0 || column >= m_itemData.size())
		return;
	m_itemData.replace(column, data);
}

OvrResourceTreeItem *OvrResourceTreeItem::parentItem()
{
    return m_parentItem;
}

QList<OvrResourceTreeItem*>& OvrResourceTreeItem::children()
{
	return m_childItems;
}

int OvrResourceTreeItem::row() const
{
	if (m_parentItem)
	{
		return m_parentItem->m_childItems.indexOf(const_cast<OvrResourceTreeItem*>(this));
	}

    return 0;
}

OvrResourceTreeItem* OvrResourceTreeItem::appendItem(const QString& a_title, const QString& a_path)
{
	QList<QVariant> data;
	data << a_title << a_path;
	OvrResourceTreeItem* item = new OvrResourceTreeItem(data, this);
	item->setDirType(dir_type);
	return item;
}

OvrResourceTreeItem* OvrResourceTreeItem::appendItem(const QString& a_title, const QString& a_path, int a_type)
{
	QList<QVariant> data;
	data << a_title << a_path;
	OvrResourceTreeItem* item = new OvrResourceTreeItem(data, this);
	item->setDirType(a_type);
	item->setKeyNode();
	return item;
}