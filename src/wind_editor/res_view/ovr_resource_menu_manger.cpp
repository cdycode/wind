#include <Header.h>
#include "ovr_resource_menu_manger.h"

#include <memory>
#include <functional>


const QList<OvrResourceMenuManager::MenuInfo> OvrResourceMenuManager::sc_CommonResMenu
{
	{"导入资源",	"res_import",		"", 		false, false },
	{ "",			"",					"",			false, true },			// addSeparator
	{"新建",		"new_create",		"", 		true , false, QList<OvrResourceMenuManager::MenuInfo>
		// have sub menu case
		{{"文件夹",	"create_folder",		"", 		false, false },
		{"",		"", "", 							false, true },		// addSeparator
		{"材质",	"create_mat",			"", 		false, false },
		{"",		"",						"",			false, true },		// addSeparator
		{"场景",	"create_sence",			"", 		false, false },
		{"片段",	"create_seq",			"", 		false, false }},
	},
	{"重命名",		"res_mgr_rename",	"", 		false, false },
	{"删除",		"res_mgr_delete",	"", 		false, false },
};


OvrResourceMenuManager::OvrResourceMenuManager(QWidget * parent)
{
	createMenu(parent);
}


OvrResourceMenuManager::~OvrResourceMenuManager()
{}

OvrResourceMenuManager * OvrResourceMenuManager::getInst()
{
	static std::unique_ptr<OvrResourceMenuManager> menu_mgr(new OvrResourceMenuManager());

	OvrResourceMenuManager * menu_mgr_ptr = menu_mgr.get();

	return menu_mgr_ptr;
}

void OvrResourceMenuManager::createMenu(QWidget * parent)
{
	std::function<void (const QList<OvrResourceMenuManager::MenuInfo> &, QMenu *)> generate_menu_fun =
		[this, &generate_menu_fun](const QList<OvrResourceMenuManager::MenuInfo> &menu_list, QMenu * parent) {
		for (auto &menu : menu_list) 
		{
			if (menu.isSeparator) {
				parent->addSeparator();
			}
			else if (menu.hasSubMenu) {
				QMenu * sub_menu_item = new QMenu(menu.strTitle, parent);
				if (!menu.strIcon.isEmpty()) {
					QString strImgPath = getImagePath(menu.strIcon.toLocal8Bit().data());
					sub_menu_item->setIcon(QIcon(strImgPath));
				}

				sub_menu_item->setObjectName(menu.strObjName);
				generate_menu_fun(menu.subMenus, sub_menu_item);

				parent->addMenu(sub_menu_item);
				initMenuObjectPtr(sub_menu_item);
			}
			else {
				QAction * acton_item = new QAction(menu.strTitle, parent);
				if (!menu.strIcon.isEmpty()) {
					QString strImgPath = getImagePath(menu.strIcon.toLocal8Bit().data());
					acton_item->setIcon(QIcon(strImgPath));
				}

				acton_item->setObjectName(menu.strObjName);

				parent->addAction(acton_item);
				initMenuObjectPtr(acton_item);
				m_AllMenuActions.push_back(acton_item);
			} // else
		} // for
	};

	res_mgr_menu = new QMenu(parent);
	generate_menu_fun(sc_CommonResMenu, res_mgr_menu);

	setStyleSheet(res_mgr_menu);
}

void OvrResourceMenuManager::initResTreeMenu(int a_dir_type, bool a_is_key_dir)
{
	m_dir_type = ResDirType(a_dir_type);
	m_is_selected = true;
	m_is_selectd_folder = true;
	m_eCurWidgetType = CWT_RES_TREE;

	disableAllActions();

	switch (a_dir_type)
	{
	case kRDTRes:
		action_res_import->setEnabled(true);
		create_mat->setEnabled(true);
		break;
	case kRDTPrjScene:
		create_sence->setEnabled(true);
		break;
	case kRDTPrjSeq:
		create_seq->setEnabled(true);
		break;
	default:
		assert(false);
		break;
	}

	create_folder->setEnabled(true);
	if (!a_is_key_dir)
	{
		res_mgr_rename->setEnabled(true);
		res_mgr_delete->setEnabled(true);
	}

}

void OvrResourceMenuManager::initListMenu(int a_dir_type, bool a_is_selected, bool is_selectd_folder)
{
	// save for  use after. only for list
	m_dir_type = ResDirType(a_dir_type);
	m_is_selected = a_is_selected;
	m_is_selectd_folder = is_selectd_folder;

	m_eCurWidgetType = CWT_RES_LIST;

	disableAllActions();

	bool is_res_dir = a_dir_type == kRDTRes;

	create_folder->setEnabled(true);
	if (is_res_dir)
	{
		//在资源目录内

		create_mat->setEnabled(true);
		action_res_import->setEnabled(true);

		if (a_is_selected)
		{
			if (is_selectd_folder)
			{
				//选中资源目录
			}
			res_mgr_rename->setEnabled(true);
			res_mgr_delete->setEnabled(true);
			//action_folder_move->setEnabled(true);
		}
		else
		{
			//资源目录内空白处右键单击
		}
	}
	else
	{
		//在工程目录内
		if (a_is_selected)
		{
			res_mgr_rename->setEnabled(true);
			res_mgr_delete->setEnabled(true);
			//action_folder_move->setEnabled(true);
		}

		switch (a_dir_type)
		{
		case kRDTPrjScene:
			create_sence->setEnabled(true);
			break;
		case kRDTPrjSeq:
			create_seq->setEnabled(true);
			break;
		default:
			assert(false);
			break;
		}
	}
}

void OvrResourceMenuManager::show(const QPoint& point)
{
	QObject::disconnect(new_create, SIGNAL(triggered(QAction*)), this, SLOT(onMenutriggered(QAction*)));

	res_mgr_menu->show();
	res_mgr_menu->move(point);

	QObject::connect(res_mgr_menu, SIGNAL(triggered(QAction*)), this, SLOT(onMenutriggered(QAction*)), Qt::UniqueConnection);
}

void OvrResourceMenuManager::showCreateMenu(const QPoint& point)
{
	QObject::disconnect(res_mgr_menu, SIGNAL(triggered(QAction*)), this, SLOT(onMenutriggered(QAction*)));

	new_create->show();
	new_create->move(point);

	QObject::connect(new_create, SIGNAL(triggered(QAction*)), this, SLOT(onMenutriggered(QAction*)), Qt::UniqueConnection);
}

void OvrResourceMenuManager::setStyleSheet(QMenu * menu)
{
	QString strMenuStyle = QString::fromLocal8Bit(
		"QMenu {background:rgb(39,39,39); color:rgb(205,205,205); selection-background-color: rgb(60,60,60); selection-color: rgb(205,205,205);}\n"
		"QMenuBar::item {color:rgb(205,205,205);}\n"
		"QMenuBar::item:selected { background-color: rgb(60,60,60);}\n"
		"QMenu::item:disabled {color:gray}"
	);

	menu->setStyleSheet(strMenuStyle);
}

void OvrResourceMenuManager::initMenuObjectPtr(QMenu * menu)
{
	QString strMenuObjName = menu->objectName();
	if (strMenuObjName == QString::fromLocal8Bit("new_create")) {
		new_create = menu;
	}
	else {
		assert(false);
	}
}

void OvrResourceMenuManager::initMenuObjectPtr(QAction * action)
{
	QString strActionObjName = action->objectName();

	if (strActionObjName == QString::fromLocal8Bit("res_import")) 
	{
		action_res_import = action;
	}
	else if (strActionObjName == QString::fromLocal8Bit("create_folder")) 
	{
		create_folder = action;
	}
	else if (strActionObjName == QString::fromLocal8Bit("create_mat")) 
	{
		create_mat = action;
	}
	else if (strActionObjName == QString::fromLocal8Bit("create_sence")) 
	{
		create_sence = action;
	}
	else if (strActionObjName == QString::fromLocal8Bit("create_seq")) 
	{
		create_seq = action;
	}
	else if (strActionObjName == QString::fromLocal8Bit("res_mgr_rename")) 
	{
		res_mgr_rename = action;
	}
	else if (strActionObjName == QString::fromLocal8Bit("res_mgr_delete")) 
	{
		res_mgr_delete = action;
	}
	else 
	{
		assert(false);
	}
}

void OvrResourceMenuManager::disableAllActions()
{
	for (auto action : m_AllMenuActions) 
	{
		action->setDisabled(true);
	}
}

QMenu * OvrResourceMenuManager::createMenuItem(QString & strTitle, QString & strObjName, QWidget * parent)
{
	QMenu *  menu = new QMenu(strTitle, parent);
	menu->setObjectName(strObjName);

	return menu;
}

QAction * OvrResourceMenuManager::createAcionItem(QString & strTitle, QString & strObjName, QWidget * parent)
{
	QAction *action = new QAction(strTitle, parent);
	action->setObjectName(strObjName);
	return nullptr;
}

void OvrResourceMenuManager::onMenutriggered(QAction * action)
{
	OvrResourceTreeModel * res_model = OvrResourceTreeModel::getInst();
	QString name = action->objectName();

	bool is_tree_menu = m_eCurWidgetType == CWT_RES_TREE;

	bool is_done = true;
	if (name == "res_import")
	{
		notifyEvent("event_import_res", 0, 0);
	}
	else if (name == "create_folder")
	{
		notifyEvent("event_create_folder", is_tree_menu, 0);
	}
	else if (name == "res_mgr_delete")
	{
		QString event_name = is_tree_menu ? "onRemoveResTreeItem" : "onRemoveResListItem";
		notifyEvent(event_name, 0, 0);
	}
	else if (name == "res_mgr_rename")
	{
		QString event_name = is_tree_menu ? "onEditResTreeItem" : "onEditResListItem";
		notifyEvent(event_name, QModelIndex(), "");
	}
	else if (name == "create_mat" || name == "create_sence" || name == "create_seq")
	{
		notifyEvent("event_create_asset", name, 0);
	}
	else
	{
		is_done = false;
	}
}


