#pragma once
#include <QMenu>

#include "ovr_resource_tree_item.h"
#include "ovr_resource_tree_model.h"

class OvrResourceMenuManager : public QObject, public OvrEventSender
{
	Q_OBJECT

public:
	OvrResourceMenuManager(QWidget * parent = nullptr);
	~OvrResourceMenuManager();

	virtual QString senderName() const override { return "OvrResourceMenuManager"; }

	static OvrResourceMenuManager * getInst();

	void createMenu(QWidget * parent = nullptr);

	void initResTreeMenu(int a_dir_type, bool a_is_key_dir);
	void initListMenu(int a_dir_type, bool a_is_selected, bool is_selectd_folder);

	void show(const QPoint& point);
	void showCreateMenu(const QPoint& point);

private:
	struct MenuInfo
	{
		QString strTitle;
		QString strObjName;		// must be unique
		QString strIcon;
		bool hasSubMenu = false;
		bool isSeparator = false;
		QList<MenuInfo> subMenus;

		MenuInfo() {}
		MenuInfo(const char *a_title, const char *a_objName, const char *a_icon, bool a_hasSubMenu, bool a_isSeparator) {
			strTitle = QString::fromLocal8Bit(a_title);
			strObjName = QString::fromLocal8Bit(a_objName);
			strIcon = QString::fromLocal8Bit(a_icon);
			hasSubMenu = a_hasSubMenu;
			isSeparator = a_isSeparator;
		}
		MenuInfo(const char *a_title, const char *a_objName, const char *a_icon, bool a_hasSubMenu, bool a_isSeparator, QList<MenuInfo> & a_sub_menu) {
			strTitle = QString::fromLocal8Bit(a_title);
			strObjName = QString::fromLocal8Bit(a_objName);
			strIcon = QString::fromLocal8Bit(a_icon);
			hasSubMenu = a_hasSubMenu;
			isSeparator = a_isSeparator;

			subMenus = a_sub_menu;
		}
	};

	const static QList<MenuInfo> sc_CommonResMenu;

	QVector<QAction *> m_AllMenuActions;

	enum eCurWidgetType
	{
		CWT_NULL_INFO,
		CWT_RES_TREE,
		CWT_RES_LIST,
	};

	eCurWidgetType m_eCurWidgetType = CWT_NULL_INFO;
	ResDirType m_dir_type;
	bool m_is_selected;
	bool m_is_selectd_folder;

	QMenu * res_mgr_menu = nullptr;				// ��

	QAction* action_res_import = nullptr;

	QMenu * new_create = nullptr;
	QAction* create_folder = nullptr;
	QAction* create_mat = nullptr;
	QAction* create_sence = nullptr;
	QAction* create_seq = nullptr;

	QAction* res_mgr_rename = nullptr;
	QAction* res_mgr_delete = nullptr;

	void setStyleSheet(QMenu * menu);
	void initMenuObjectPtr(QMenu * menu);
	void initMenuObjectPtr(QAction * action);


	void disableAllActions();

	QMenu * createMenuItem(QString &strTitle, QString &strObjName, QWidget *parent = nullptr);
	QAction * createAcionItem(QString &strTitle, QString &strObjName, QWidget *parent = nullptr);

	private slots:
	;
	void onMenutriggered(QAction * action);
};

