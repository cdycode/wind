#pragma once

#include <QListWidget>
#include <QLabel>

#define kItemPath (Qt::UserRole)
#define kItemDirFlag (Qt::UserRole + 1)
#define kItemID (Qt::UserRole + 2)

class OvrResourceListWidget : public QListWidget, public OvrEventReceiver, public OvrEventSender
{
	Q_OBJECT
public:
	static OvrResourceListWidget* getInst();
	explicit OvrResourceListWidget(QWidget *parent = Q_NULLPTR);
	~OvrResourceListWidget();

	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;
	virtual QString name() override { return "OvrResourceListWidget"; }

	virtual QString senderName() const override { return "OvrResourceListWidget"; }

	void refreshCurrentDir();

	void select_item(const QString& str_text);

	QString currentDir() { return current_dir; }
	int currentDirType() { return current_dir_type; }

	// 如果选择的是list 中目录，返回选中目录的路径，如果选择的是list 文件或未选择，返回的是当前list 路径
	QString currentSelDir();

	QString GetSelectName();

	int GetItemCount(QString a_type, QStringList &name_list);

protected:
	void onBarActionTriggered(QAction *action);

	QModelIndex locateFile(const QString &a_res_name);
	void editItem(const QModelIndex & a_index);

	void onItemDoubleClicked(const QModelIndex &index);


	virtual void currentChanged(const QModelIndex &current, const QModelIndex &previous) override;
	virtual void mousePressEvent(QMouseEvent *event) override;
	virtual void mouseMoveEvent(QMouseEvent *event) override;
	virtual void contextMenuEvent(QContextMenuEvent *event) override;

	void setSelectItem(const QString& a_name);

	template<class T> QString appendChildItems(QString a_dir, const T& a_items);

	bool canBeDraged(const QString& a_asset_path);

	//event
protected:
	void onResDirEntered(QString a_dir, int a_dir_type);
	void onEditNewItem(const QString& a_res_file);
	void onRenameResListItem(const QModelIndex& a_index, const QString& value);
	void onRenameResItemFinish(const QString& a_new_path, bool a_is_sub);
	void onRemoveListItem();
	void onRemoveResItemFinish();
	void onImportRes();

protected:
	bool selectImportFiles(QStringList& a_files);
	bool importRes(const QStringList& a_files);

	QIcon file_icon;
	QIcon fold_icon;
	QIcon model_icon;

	QSize	big_size, small_size;
	QSize   big_hit_size, small_hit_size;

	int current_dir_type = -1;
	QString current_dir;

	bool is_item_click_down = false;
	QListWidgetItem* drag_item = nullptr;

	bool bIconMode = false;

	QString select_name = "";

private:
	void init_custom_ui(bool bBigMode);
	void set_custom_style_sheet(bool bBigMode);

};

