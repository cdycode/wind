#include "Header.h"
#include "ovr_resource_tree_model.h"

#include "ovr_resource_tree_item.h"

static QString generate_default_text(const QString &a_dir, const QString &a_prefix, const QString & a_ext = QString(""))
{
	QDir dir(a_dir);
	if (!dir.exists())
		return QString();

	const QFileInfoList& items = dir.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot);

	QString strDefault;
	for (size_t i = 0; ; i++) {
		QString strSerialNum = i ? QString("_") + QString::number(i) : "";
		strDefault = a_prefix + strSerialNum;
		bool bTextExist = false;
		for (auto &item : items) {
			if (item.fileName() == strDefault + a_ext) {
				bTextExist = true;
				break;
			}
		}
		if (!bTextExist) {
			break;
		}
	}

	return strDefault;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
struct ResKeyDir 
{ 
	const char* path;
	const char* title;
	int dir_type;
};

static QList<ResKeyDir> sub_key_dirs = {
	{"content", "资源", kRDTRes},
	{"project/scene", "场景", kRDTPrjScene},
	{"project/sequence", "片段", kRDTPrjSeq},
	//{"system", "系统", kRDTPrjSys}
};

const QString OvrResourceTreeModel::cs_default_folder_prefix = QString::fromLocal8Bit("新文件夹");
const QString OvrResourceTreeModel::cs_default_mat_prefix = QString::fromLocal8Bit("新材质");
const QString OvrResourceTreeModel::cs_default_plot_prefix = QString::fromLocal8Bit("新剧情");
const QString OvrResourceTreeModel::cs_default_seq_prefix = QString::fromLocal8Bit("新片段");
const QString OvrResourceTreeModel::cs_default_sence_prefix = QString::fromLocal8Bit("新场景");


static OvrResourceTreeModel* g_model_inst = nullptr;

OvrResourceTreeModel* OvrResourceTreeModel::getInst()
{
	if (g_model_inst == nullptr)
	{
		g_model_inst = new OvrResourceTreeModel;
	}
	return g_model_inst;
}

OvrResourceTreeModel::OvrResourceTreeModel(QObject *parent) :QAbstractItemModel(parent)
{
	QList<QVariant> rootData;
	rootData << "Dir";
	root_Item = new OvrResourceTreeItem(rootData);

	QStringList e = { "onProjectOpen", "event_add_sub_folder", "event_create_asset_in_dir",
		"onRenameCurAssertDir", "onRenameSubAssertDir", "onRenameAssertFile",
		"onRemoveAssertDir", "onRemoveResAssetFile" };

	registerEventListener(e);
}

OvrResourceTreeModel::~OvrResourceTreeModel()
{
	delete root_Item;
}

int OvrResourceTreeModel::columnCount(const QModelIndex &parent) const
{
	if (parent.isValid())
		return static_cast<OvrResourceTreeItem*>(parent.internalPointer())->columnCount();
	else
		return root_Item->columnCount();
}

QVariant OvrResourceTreeModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	QVariant qvar = QVariant();
	OvrResourceTreeItem *cur_item = static_cast<OvrResourceTreeItem*>(index.internalPointer());
	switch (role)
	{
	case Qt::DisplayRole:
		qvar = cur_item->data(index.column());
		break;
	case Qt::DecorationRole:
		//qvar = QIcon(getImagePath("icon_closefile_min.png"));
		break;
	case Qt::ForegroundRole:
		break;
	case Qt::BackgroundRole:
		break;
	case Qt::CheckStateRole:
		break;
	case Qt::SizeHintRole:
		break;
	case Qt::UserRole:
		break;
	default:
		qvar = QVariant();
		break;
	}

	return qvar;
}

Qt::ItemFlags OvrResourceTreeModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return 0;

	return QAbstractItemModel::flags(index);
}

QVariant OvrResourceTreeModel::headerData(int section, Qt::Orientation orientation,	int role) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
		return root_Item->data(section);

	return QVariant();
}

QModelIndex OvrResourceTreeModel::index(int row, int column, const QModelIndex &parent) const
{
	if (!hasIndex(row, column, parent))
		return QModelIndex();

	OvrResourceTreeItem *parentItem = nullptr;

	if (!parent.isValid())
	{
		parentItem = root_Item;
	}
	else
	{
		parentItem = static_cast<OvrResourceTreeItem*>(parent.internalPointer());
	}

	//OvrResourceTreeItem *childItem = parentItem->child(row);
	OvrResourceTreeItem *childItem = dynamic_cast<OvrResourceTreeItem*>(parentItem->child(row));
	if (childItem)
	{
		return createIndex(row, column, childItem);
	}
	else
	{
		return QModelIndex();
	}
}

QModelIndex OvrResourceTreeModel::parent(const QModelIndex &index) const
{
	if (!index.isValid())
		return QModelIndex();

	OvrResourceTreeItem *childItem = static_cast<OvrResourceTreeItem*>(index.internalPointer());
	OvrResourceTreeItem *parentItem = childItem->parentItem();
	//OvrResourceTreeItem *parentItem = dynamic_cast<OvrResourceTreeItem*>(childItem->parent());

	if (parentItem == root_Item)
		return QModelIndex();

	return createIndex(parentItem->row(), 0, parentItem);
}

int OvrResourceTreeModel::rowCount(const QModelIndex &parent) const
{
	OvrResourceTreeItem* parentItem = nullptr;
	if (parent.column() > 0)
	{
		return 0;
	}

	if (!parent.isValid())
	{
		parentItem = root_Item;
	}
	else
	{
		parentItem = static_cast<OvrResourceTreeItem*>(parent.internalPointer());
	}

	return parentItem->childCount();
}

void OvrResourceTreeModel::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{
	if (a_event_name == "onProjectOpen")
	{
		onProjectOpen(a_param1.toBool(), a_param2.toString());
	}
	else if (a_event_name == "event_add_sub_folder")
	{
		onAddSubFolder(a_param1.toModelIndex(), a_param2.toBool());
	}
	else if (a_event_name == "event_create_asset_in_dir")
	{

	}
	else if (a_event_name == "onRenameCurAssertDir" || a_event_name == "onRenameSubAssertDir")
	{
		auto new_path = a_param2.toString();
		if (onRenameAssertDir(a_param1.toModelIndex(), new_path))
		{
			bool is_sub = a_event_name == "onRenameSubAssertDir";
			notifyEvent("onRenameResItemFinish", new_path, is_sub, false);
		}
	}
	else if (a_event_name == "onRenameAssertFile")
	{
		onRenameAssertFile(a_param1.toStringList(), a_param2.toInt());
	}
	else if (a_event_name == "onRemoveAssertDir")
	{
		if (removeAssertDir(a_param1.toModelIndex()))
		{
			bool is_sub_dir = a_param2.toBool();
			if (is_sub_dir)
			{
				notifyEvent("onRemoveResItemFinish", 0, 0, false);
			}
		}
	}
	else if (a_event_name == "onRemoveResAssetFile")
	{
		onRemoveResAssetFile(a_param1.toStringList(), a_param2.toInt());
	}
}

void OvrResourceTreeModel::onRemoveResAssetFile(const QStringList& a_target, int a_dir_type)
{
	assert(a_target.size() == 2);

	if (removeResAsset(a_target[0], a_dir_type))
	{
		QFile file_obj(a_target[1]);
		if (file_obj.exists())
		{
			bool ret = file_obj.remove();
			assert(ret);
		}
		notifyEvent("onRemoveResItemFinish", 0, 0, false);
	}
}

void OvrResourceTreeModel::onProjectOpen(bool a_is_ok, QString a_prj_dir)
{
	beginResetModel();
	root_Item->clearChild();
	if (a_is_ok && !a_prj_dir.isEmpty())
	{
		searchProjectDir(a_prj_dir);
	}
	endResetModel();
}

bool OvrResourceTreeModel::onRenameAssertFile(const QStringList& a_files, int a_type)
{
	assert(a_files.size() == 3);

	auto item_id = a_files[0];
	auto new_file_path = a_files[1];
	OvrProjectManager::getInst()->makeRelateDir(new_file_path);

	renameResItem(item_id.toLocal8Bit().constData(), new_file_path.toLocal8Bit().constData(), a_type);

	QString path_from = a_files[2];
	QString path_to = a_files[1];

	QFile file_obj(path_from);
	if (file_obj.exists())
	{
		if (!file_obj.rename(path_to))
		{
			qDebug() << "rename asset file failed: " << path_from;
		}
	}

	bool is_sub = true;
	notifyEvent("onRenameResItemFinish", a_files[1], is_sub, false);

	return true;
}

bool OvrResourceTreeModel::removeAssertDir(const QModelIndex& a_index)
{
	assert(a_index.isValid());

	auto item = static_cast<OvrResourceTreeItem*>(a_index.internalPointer());
	QString res_dir = item->data(1).toString();

	//remove asset refrence
	removeResInDir(res_dir, item->dirType());

	//remove tree node
	auto parent_item = item->parentItem();
	beginRemoveRows(a_index.parent(), a_index.row(), a_index.row());
	parent_item->removeChild(a_index.row());
	endRemoveRows();

	//remove dir
	QDir dir(res_dir);
	if (dir.exists())
	{
		qDebug() << "to remove folder " << res_dir;
		if (!dir.removeRecursively())
		{
			qDebug() << "remove folder failded";
		}
	}

	return true;
}

void OvrResourceTreeModel::removeResInDir(QString a_res_dir, int a_dir_type)
{
	OvrProjectManager::getInst()->makeRelateDir(a_res_dir);

	QList<OvrDataItemString> res_files;
	getResItemsInDir(a_res_dir, a_dir_type, res_files);

	for (auto target : res_files)
	{
		removeResAsset(target.item_id, a_dir_type);
	}
}

bool OvrResourceTreeModel::removeResAsset(QString a_target_id, int a_type)
{
	auto item_id = a_target_id.toLocal8Bit().constData();

	auto mgr = ovr_project::OvrProject::GetIns()->GetAssetManager();

	bool is_ok = false;
	switch (a_type)
	{
	case kRDTRes:
		OvrProjectManager::getInst()->makeRelateDir(a_target_id);
		is_ok = mgr->DeleteAsset(a_target_id.toLocal8Bit().constData());
		break;
	case kRDTPrjScene:
		is_ok = mgr->RemoveSceneFile(item_id);
		break;
	case kRDTPrjSeq:
		is_ok = mgr->RemoveSequenceFile(item_id);
		break;
	default:
		assert(false);
		break;
	}

	assert(is_ok);
	return is_ok;
}

bool OvrResourceTreeModel::onRenameAssertDir(const QModelIndex& a_index, const QString& a_new_path)
{
	auto item = static_cast<OvrResourceTreeItem*>(a_index.internalPointer());
	QString old_path = item->data(1).toString();

	//rename asset
	renameResInDir(item, a_new_path);

	//rename dir
	qDebug() << "to rename folder " << old_path;
	if (!QDir().rename(old_path, a_new_path))
	{
		qDebug() << "rename folder failded";
	}

	auto parent_item = item->parentItem();
	auto parent_index = a_index.parent();
	auto parent_child_count = parent_item->childCount();

	//remove children
	beginRemoveRows(parent_index, 0, parent_child_count - 1);
	parent_item->clearChild();
	endRemoveRows();

	//reload children
	beginInsertRows(parent_index, 0, parent_child_count - 1);
	searchProjectDir(parent_item, parent_item->data(1).toString());
	endInsertRows();

	return true;
}

void OvrResourceTreeModel::renameResInDir(OvrResourceTreeItem* a_item, const QString& a_new_path)
{
	//绝对路径
	QString old_path = a_item->data(1).toString();
	QString new_path = a_new_path;

	//相对路径
	OvrProjectManager::getInst()->makeRelateDir(old_path);
	OvrProjectManager::getInst()->makeRelateDir(new_path);

	//获取所属资源列表
	QList<OvrDataItemString> res_files;
	getResItemsInDir(old_path, a_item->dirType(), res_files);

	for (auto target : res_files)
	{
		QString new_file_path = target.item_path;
		new_file_path.replace(0, old_path.length(), new_path);

		renameResItem(target.item_id.toLocal8Bit().constData(), new_file_path.toLocal8Bit().constData(), a_item->dirType());
	}
}

void OvrResourceTreeModel::getResItemsInDir(const QString& a_path, const std::list<ovr_project::OvrProjectFileInfo>& a_file_infos, QList<OvrDataItemString>& a_files)
{
	for (auto a_file : a_file_infos)
	{
		auto file_path = QString::fromLocal8Bit(a_file.file_path.GetStringConst());
		if (file_path.startsWith(a_path))
		{
			a_files.append(OvrDataItemString(QString::fromLocal8Bit(a_file.unique_name.GetStringConst()), file_path));
		}
	}
}

void OvrResourceTreeModel::getResItemsInDir(const QString& a_path, int a_type, QList<OvrDataItemString>& a_files)
{
	if (a_type == kRDTRes)
	{
		for (auto a_file : ovr_project::OvrProject::GetIns()->GetAssetManager()->GetAssetFileList())
		{
			auto file_path = QString::fromLocal8Bit(a_file.file_path.GetStringConst());
			if (file_path.startsWith(a_path))
			{
				a_files.append(OvrDataItemString(file_path, file_path));
			}
		}
	}
	else if (a_type == kRDTPrjScene)
	{
		getResItemsInDir(a_path, ovr_project::OvrProject::GetIns()->GetAssetManager()->GetSceneFileList(), a_files);

	}
	else if (a_type == kRDTPrjSeq)
	{
		getResItemsInDir(a_path, ovr_project::OvrProject::GetIns()->GetAssetManager()->GetSequenceFileList(), a_files);
	}
	else
	{
		assert(false);
	}
}

bool OvrResourceTreeModel::renameResItem(const char* a_item_id, const char* a_new_file_path, int a_type)
{
	auto mgr = ovr_project::OvrProject::GetIns()->GetAssetManager();

	bool is_ok = false;
	switch (a_type)
	{
	case kRDTRes:
		is_ok = mgr->RenameAssetFile(a_item_id, a_new_file_path);
		break;
	case kRDTPrjScene:
		is_ok = mgr->RenameSceneFilePath(a_item_id, a_new_file_path);
		break;
	case kRDTPrjSeq:
		is_ok = mgr->RenameSequenceFilePath(a_item_id, a_new_file_path);
		break;
	default:
		assert(false);
		break;
	}

	return is_ok;
}


void OvrResourceTreeModel::searchProjectDir(OvrResourceTreeItem* node, const QString& path)
{
	QDir dir(path);
	if (!dir.exists())
	{
		return;
	}

	const QFileInfoList& items = dir.entryInfoList(QDir::AllDirs | QDir::NoDotAndDotDot);
	for (auto item : items)
	{
		OvrResourceTreeItem* sub_node = node->appendItem(item.fileName(), item.filePath());
		searchProjectDir(sub_node, item.filePath());
	}
}

void OvrResourceTreeModel::searchProjectDir(const QString& a_prj_dir)
{
	makeSureKeyDirExist(a_prj_dir);

	prj_dir = a_prj_dir;

	for (auto sub_dir : sub_key_dirs)
	{
		QString path = a_prj_dir + "/" + QString::fromLocal8Bit(sub_dir.path);
		OvrResourceTreeItem* sub_node = root_Item->appendItem(QString::fromLocal8Bit(sub_dir.title), path, sub_dir.dir_type);
		searchProjectDir(sub_node, path);
	}
}

void OvrResourceTreeModel::makeSureKeyDirExist(const QString& a_prj_dir)
{
	//create sub key dir if not exist
	QDir dir(a_prj_dir);
	for (auto item : sub_key_dirs)
	{
		dir.mkpath(item.path);
	}
	//临时目录
	dir.mkpath("temp");
}

void OvrResourceTreeModel::onAddSubFolder(const QModelIndex& a_index, bool is_tree_menu)
{
	assert(a_index.isValid());

	QString dir_name;
	auto result = addSubDir(a_index, dir_name);
	if (result.isValid())
	{
		notifyEvent("event_sub_folder_added", result, dir_name);

		if (is_tree_menu)
		{
			notifyEvent("onEditResTreeItem", result, dir_name);
		}
		else
		{
			notifyEvent("onEditResListItem", result, dir_name);
		}
	}
}

QModelIndex OvrResourceTreeModel::addSubDir(const QModelIndex& a_index, QString& a_dir_name)
{
	assert(a_index.isValid());

	QModelIndex result;

	OvrResourceTreeItem* item = static_cast<OvrResourceTreeItem*>(a_index.internalPointer());
	QString path = item->data(1).toString();
	QString str_default_name = generate_default_text(path, cs_default_folder_prefix);

	if (QDir(path).mkdir(str_default_name))
	{
		int row_pos = item->childCount();
		beginInsertRows(a_index, row_pos, row_pos);
		item->appendItem(str_default_name, path + "/" + str_default_name);
		endInsertRows();
		result = a_index.child(row_pos, 0);

		a_dir_name = str_default_name;
	}

	return result;
}

void OvrResourceTreeModel::onCreateAsset(const QString& a_action, const QString& a_out_dir)
{
	if (a_action == "create_mat")
	{
		createMat(a_out_dir);
	}
	else if (a_action == "create_sence" )
	{
		createScene(a_out_dir);
	}
	else if (a_action == "create_seq")
	{
		createSeq(a_out_dir);
	}
}

QString OvrResourceTreeModel::getContentDir()
{
	return prj_dir + "/content";
}

const QString& OvrResourceTreeModel::getRootDir()
{
	return prj_dir;
}

void OvrResourceTreeModel::createMat(const QString& a_out_dir_str)
{
	QString str_name_ext = QString::fromLocal8Bit(kMatFileTail);
	QString str_default_name = generate_default_text(a_out_dir_str, cs_default_mat_prefix, str_name_ext);
	QString abs_file_path = a_out_dir_str + "/" + str_default_name + str_name_ext;
	QString relate_path = abs_file_path;
	OvrProjectManager::getInst()->makeRelateDir(relate_path);

	QByteArray ovr_path = relate_path.toLocal8Bit();
	if (ovr_project::OvrProject::GetIns()->GetAssetManager()->CreateMat(ovr_path.constData()))
	{
		notifyEvent("onEditResListItem", QModelIndex(), str_default_name);
	}	
}

void OvrResourceTreeModel::createScene(const QString& a_out_dir_str)
{
	QString str_default_name = generate_default_text(a_out_dir_str, cs_default_sence_prefix, kSceneFileTail);
	QString abs_file_path = a_out_dir_str + "/" + str_default_name + kSceneFileTail;
	QString relate_path = abs_file_path;
	OvrProjectManager::getInst()->makeRelateDir(relate_path);

	QByteArray ovr_path = relate_path.toLocal8Bit();
	if (ovr_project::OvrProject::GetIns()->GetAssetManager()->CreateSence(ovr_path.constData()))
	{
		notifyEvent("onEditResListItem", QModelIndex(), str_default_name);
	}
}

void OvrResourceTreeModel::createSeq(const QString& a_out_dir_str)
{
	QString str_default_name = generate_default_text(a_out_dir_str, cs_default_seq_prefix, kSeqFileTail);
	QString abs_file_path = a_out_dir_str + "/" + str_default_name + kSeqFileTail;
	QString relate_path = abs_file_path;
	OvrProjectManager::getInst()->makeRelateDir(relate_path);
	const char* scene_id = ovr_project::OvrProject::GetIns()->GetCurrentScene()->GetUniqueName().GetStringConst();
	if (ovr_project::OvrProject::GetIns()->GetAssetManager()->CreateSequcence(scene_id, relate_path.toLocal8Bit().constData()))
	{
		notifyEvent("onEditResListItem", QModelIndex(), str_default_name);
	}
}
