#include <Header.h>
#include "ovr_res_list_item_delegate.h"

OvrResListItemDelegate::OvrResListItemDelegate(QObject *parent)
	: QStyledItemDelegate(parent)
{}

OvrResListItemDelegate::~OvrResListItemDelegate()
{}

QWidget * OvrResListItemDelegate::createEditor(QWidget * parent, const QStyleOptionViewItem & option, const QModelIndex & index) const
{
	QListWidgetItem * item = static_cast<QListWidgetItem*>(index.internalPointer());

	QLineEdit * editor_box = new QLineEdit(parent);
	editor_box->setMaxLength(100);

	QRegExp regExp = QRegExp("[^\\\\/*:|?\"\\s]*");
	QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExp, editor_box);
	editor_box->setValidator(portRegExpValidator);
	editor_box->setContextMenuPolicy(Qt::NoContextMenu);
	editor_box->setFocus();

	return editor_box;
}

void OvrResListItemDelegate::setEditorData(QWidget * editor, const QModelIndex & index) const
{
	QListWidgetItem * item = static_cast<QListWidgetItem*>(index.internalPointer());
	QLineEdit * line_editor = dynamic_cast<QLineEdit *>(editor);

	QString strInfo = item->data(0).toByteArray();

	QString strNoExt = strInfo.left(strInfo.lastIndexOf("."));
	line_editor->setText(strNoExt);
}

void OvrResListItemDelegate::setModelData(QWidget * editor, QAbstractItemModel * model, const QModelIndex & index) const
{
	QLineEdit * line_editor = dynamic_cast<QLineEdit *>(editor);
	notifyEvent("onRenameResListItem", index, line_editor->text(), false);
}

