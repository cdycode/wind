#pragma once

#include <QStyledItemDelegate>
#include <QSize>
#include <QLineEdit>


class OvrResTreeItemDelegate : public QStyledItemDelegate, public OvrEventSender
{
public:
    OvrResTreeItemDelegate(QObject *parent = Q_NULLPTR);

	virtual QString senderName() const override { return "OvrResTreeItemDelegate"; }

	virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

	virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
	virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;
	virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

protected:
	QImage* item_image;
	QSize icon_size;
};

//class OvrResItemLineEdit : public QLineEdit
//{
//	Q_OBJECT
//public:
//	//OvrResItemLineEdit();
//	explicit OvrResItemLineEdit(QWidget *parent = Q_NULLPTR);
//	explicit OvrResItemLineEdit(const QString & def_text, QWidget *parent = Q_NULLPTR);
//	~OvrResItemLineEdit();
//
//Q_SIGNALS:
//	void mouse_out_focus();
//
//protected:
//
//	virtual void focusOutEvent(QFocusEvent *e) override;
//
//
//private:
//
//
//};
