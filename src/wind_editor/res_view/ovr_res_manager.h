#pragma once

#include <QtWidgets/QDockWidget>

class OvrResourceTree;
class OvrResourceListWidget;

class OvrResManager : public QWidget
{
	Q_OBJECT
public:
	static OvrResManager* getInst();

	explicit OvrResManager(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	~OvrResManager();

	virtual QSize sizeHint() const override { return QSize(285, 1); }

protected:
	virtual void paintEvent(QPaintEvent *e) override;

private:
	void initUI();

	OvrResourceTree * res_tree = nullptr;
	OvrResourceListWidget* list_wnd = nullptr;
};

class OvrResManagerDock : public QDockWidget, public OvrEventReceiver
{
public:
	explicit OvrResManagerDock(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());

	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;
	virtual QString name() override { return "OvrResManagerDock"; }

protected:
	void initStyle();
	void initDockBar();
};

class OvrResBar :public QToolBar, public OvrEventSender
{
public:
	explicit OvrResBar(QWidget *parent = Q_NULLPTR);

	virtual QString senderName() const override { return "OvrResBar"; }
};