#include "Header.h"
#include "ovr_resource_list_widget.h"

#include "ovr_resource_menu_manger.h"

#include "../tml_view/ovr_time_line_tree.h"
#include "../material/ovr_material_editor.h"

#include "res_import_dlg.h"

static OvrResourceListWidget* g_res_list_inst = nullptr;

///////////////////////////////////////////////////////////////////////////////////////////////////
// class OvrResourceListWidget

OvrResourceListWidget* OvrResourceListWidget::getInst()
{
	if (g_res_list_inst == nullptr)
	{
		new OvrResourceListWidget;
	}
	return g_res_list_inst;
}

OvrResourceListWidget::OvrResourceListWidget(QWidget *parent):QListWidget(parent)
{
	g_res_list_inst = this;

	file_icon = QIcon(getImagePath("icon_model_max.png"));
	fold_icon = QIcon(getImagePath("icon_file_max.png"));
	model_icon = QIcon(getImagePath("icon_person_max.png"));

	big_size = QSize(50, 50), small_size = QSize(30, -1);
	big_hit_size = QSize(50, 70), small_hit_size = QSize(-1, -1);

	init_custom_ui(bIconMode);
	set_custom_style_sheet(bIconMode);

	setEditTriggers(QAbstractItemView::EditKeyPressed | QAbstractItemView::SelectedClicked);

	connect(this, &OvrResourceListWidget::doubleClicked, [this](const QModelIndex &index) {
		onItemDoubleClicked(index);
	});

	QStringList e = { "onResBarAction", "onLocateFile", "event_sub_folder_added",
		"event_create_asset", "onEditResListItem" , "onResDirEntered",
		"onRenameResListItem" , "onRenameResItemFinish", "onResDirRefresh",
		"onRemoveResListItem" , "onRemoveResItemFinish", "event_import_res" };

	registerEventListener(e);
}


OvrResourceListWidget::~OvrResourceListWidget()
{}

void OvrResourceListWidget::refreshCurrentDir()
{
	if (current_dir.length() > 0 && current_dir_type != -1)
	{
		onResDirEntered(current_dir, current_dir_type);
	}
}

void OvrResourceListWidget::select_item(const QString & str_text)
{
	for (int i = 0; i < this->count(); i++) {
		auto item = this->item(i);
		if (item->text() == str_text) {
			this->setCurrentRow(i);
			break;
		}
	}
}

QModelIndex OvrResourceListWidget::locateFile(const QString & a_res_name)
{
	if (a_res_name.isEmpty())
	{
		return QModelIndex();
	}

	QModelIndex cur_index;
	for (int i = 0; i < count(); i++) 
	{
		QString strUiResName = item(i)->text();
		if (strUiResName == a_res_name) 
		{		// 找到，设置，退出
			setCurrentRow(i);
			cur_index = currentIndex();
			scrollTo(cur_index);
			break;
		}
	}

	return cur_index;
}

QString OvrResourceListWidget::currentSelDir()
{
	QString strCurPath;
	auto cur_sel_item = currentItem();
	if (cur_sel_item != nullptr && cur_sel_item->data(kItemDirFlag).toBool())
	{
		strCurPath = cur_sel_item->data(kItemPath).toString();;
	}
	else
	{
		strCurPath = current_dir;
	}

	return strCurPath;
}

QString OvrResourceListWidget::GetSelectName()
{
	auto curr_item = this->currentItem();
	if (!curr_item)
		select_name = "";
	else
		select_name = curr_item->data(kItemPath).toString();
	return select_name;
}

static bool isSameDir(const QString& a_dir, const QString& a_file)
{
	int index = a_file.lastIndexOf("/");
	assert(index > 0);
	QString file_dir = a_file.left(index);
	return a_dir.endsWith(file_dir, Qt::CaseInsensitive);
}

template<class T>
QString OvrResourceListWidget::appendChildItems(QString a_dir, const T& a_items)
{
	QString first_id;
	for (auto info : a_items)
	{
		const char* id = info.unique_name.GetStringConst();
		const char* path = info.file_path.GetStringConst();

		const QString str_id = QString::fromLocal8Bit(id);
		const QString str_path = QString::fromLocal8Bit(path);
		if (!isSameDir(a_dir, str_path))
		{
			continue;
		}
		
		QString local_name = QFileInfo(str_path).baseName();

		QString str_prj_path = OvrResourceTreeModel::getInst()->projectDir() + "/";
		QListWidgetItem* list_item = new QListWidgetItem(file_icon, local_name, this);
		list_item->setData(kItemPath, str_prj_path + str_path);
		list_item->setData(kItemDirFlag, false);
		list_item->setData(kItemID, str_id);
		list_item->setToolTip(local_name);
		auto flags = list_item->flags();
		list_item->setFlags(flags | Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable);

		if (first_id.isEmpty())
		{
			first_id = str_id;
		}
	}
	return first_id;
}

void OvrResourceListWidget::onResDirEntered(QString a_dir, int a_dir_type)
{
	clear();
	current_dir = "";
	current_dir_type = -1;

	if (a_dir.isEmpty() || a_dir_type == -1)
	{
		return;
	}

	QDir dir(a_dir);
	if (!dir.exists())
	{
		return;
	}

	current_dir = a_dir;
	current_dir_type = a_dir_type;

	//add dir
	{
		const QFileInfoList& items = dir.entryInfoList(QDir::AllDirs | QDir::NoDotAndDotDot);
		for (auto item : items)
		{
			QListWidgetItem* list_item = new QListWidgetItem(fold_icon, item.fileName(), this);
			list_item->setData(kItemPath, item.filePath());
			list_item->setData(kItemDirFlag, true);
			list_item->setToolTip(item.fileName());
			auto flags = list_item->flags();
			list_item->setFlags(flags | Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable);
		}
	}

	//add file
	{
		//add for res
		if (current_dir_type == kRDTRes || current_dir_type == kRDTPrjSys)
		{
			const QFileInfoList& items = dir.entryInfoList(QDir::Files | QDir::NoSymLinks);
			for (auto item : items)
			{
				auto list_item = new QListWidgetItem(file_icon, item.fileName(), this);
				list_item->setData(kItemPath, item.filePath());
				list_item->setData(kItemDirFlag, false);
				list_item->setToolTip(item.fileName());
				list_item->setFlags(list_item->flags() | Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable);
			}
		}
		//add for scene, plot , sequence
		else
		{
			auto mgr = ovr_project::OvrProject::GetIns()->GetAssetManager();
			switch (current_dir_type)
			{
			case kRDTPrjScene:
				appendChildItems(a_dir, mgr->GetSceneFileList());
				break;
			case kRDTPrjSeq:
				appendChildItems(a_dir, mgr->GetSequenceFileList());
				break;
			default:
				qDebug() << "invalid resource dir: " << a_dir;
				assert(false);
				break;
			}
		}
	}
}

void OvrResourceListWidget::currentChanged(const QModelIndex &current, const QModelIndex &previous)
{
	if (previous.isValid())
	{
		QListWidgetItem* prev_item = static_cast<QListWidgetItem*>(previous.internalPointer());
		closePersistentEditor(prev_item);
		qDebug() << "close list item editor";
	}

	if (current.isValid())
	{
		QListWidgetItem* item = static_cast<QListWidgetItem*>(current.internalPointer());
		if (item != nullptr)
		{
			setSelectItem(item->data(Qt::UserRole).toString());
			return;
		}
	}

	setSelectItem("");
}

void OvrResourceListWidget::setSelectItem(const QString& a_name)
{
	if (select_name != a_name)
	{
		select_name = a_name;
		qDebug() << "OvrResourceListWidget selected: " << select_name;
		notifyEvent("onResItemSelected", select_name, 0);
	}
}

void OvrResourceListWidget::onItemDoubleClicked(const QModelIndex &index)
{
	assert(index.isValid());
	QListWidgetItem* list_item = static_cast<QListWidgetItem*>(index.internalPointer());
	assert(list_item != nullptr);
	//open dir
	if (list_item->data(kItemDirFlag).toBool())
	{
		qDebug() << "open dir: " << list_item->data(kItemPath).toString();
		notifyEvent("onOpenResSubDir", list_item->data(kItemPath), 0);
	}
	//open asset
	else
	{
		QString select_name = list_item->data(kItemPath).toString();

		switch (current_dir_type)
		{
		case kRDTPrjScene:
		{
			QString id = list_item->data(kItemID).toString();
			OvrProjectManager::getInst()->openScene(id);
			break;
		}
		case kRDTRes:
		{
			if (!select_name.isEmpty() && QFileInfo(select_name).isFile() &&
				select_name.endsWith(".mat", Qt::CaseInsensitive)) {

				QString relate_path = select_name;
				OvrProjectManager::getInst()->makeRelateDir(relate_path);

				OvrMaterialEditor matEditorDlg(relate_path, this);
				matEditorDlg.exec();
			}
			break;
		}
		default:
			break;
		}
	}
}

void OvrResourceListWidget::onRenameResListItem(const QModelIndex& a_index, const QString& value)
{
	if (value.isEmpty())
	{
		return;
	}

	auto list_item = static_cast<QListWidgetItem*>(a_index.internalPointer());
	if (list_item != nullptr)
	{
		QString item_path = list_item->data(kItemPath).toString();
		//目录
		if (list_item->data(kItemDirFlag).toBool())
		{
			notifyEvent("onRenameResSubDir", item_path, value, false);
		}
		//文件
		else
		{
			QFileInfo fi(item_path);
			QString new_name = fi.fileName().replace(fi.baseName(), value);
			QString new_path = fi.path() + "/" + new_name;

			QStringList target;
			QString item_id = current_dir_type == kRDTRes ? item_path : list_item->data(kItemID).toString();
			target << item_id << new_path << item_path;
			notifyEvent("onRenameAssertFile", target, current_dir_type, false);
		}
	}
}

void OvrResourceListWidget::onRenameResItemFinish(const QString& a_new_path, bool a_is_sub)
{
	if (a_is_sub)
	{
		refreshCurrentDir();
		QFileInfo fi(a_new_path);
		locateFile(current_dir_type == kRDTRes ? fi.fileName() : fi.baseName());
	}
}

void OvrResourceListWidget::onRemoveListItem()
{
	if (!currentIndex().isValid())
	{
		return;
	}

	auto list_item = static_cast<QListWidgetItem*>(currentIndex().internalPointer());
	if (list_item != nullptr)
	{
		QString item_path = list_item->data(kItemPath).toString();
		//目录
		if (list_item->data(kItemDirFlag).toBool())
		{
			notifyEvent("onRemoveResSubDir", item_path, 0);
		}
		//文件
		else
		{
			QStringList target;
			QString item_id = current_dir_type == kRDTRes ? item_path : list_item->data(kItemID).toString();
			target << item_id << item_path;

			notifyEvent("onRemoveResAssetFile", target, current_dir_type, false);
		}
	}
}

void OvrResourceListWidget::onRemoveResItemFinish()
{
	if (currentIndex().isValid())
	{
		model()->removeRow(currentIndex().row(), currentIndex().parent());
	}
}

void OvrResourceListWidget::onImportRes()
{
	if (current_dir_type != kRDTRes)
	{
		OvrHelper::getInst()->showStatus(QString::fromLocal8Bit("请选择资源文件夹后再进行操作"), 3000);
		return;
	}

	QStringList sel_files;
	if (selectImportFiles(sel_files))
	{
		importRes(sel_files);
	}
}

void OvrResourceListWidget::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{
	if (a_event_name == "onResBarAction")
	{
		auto action = static_cast<QAction*>(a_param1.value<void*>());
		onBarActionTriggered(action);
	}
	else if (a_event_name == "onLocateFile")
	{
		locateFile(a_param1.toString());
	}
	else if (a_event_name == "event_sub_folder_added")
	{
		refreshCurrentDir();
		locateFile(a_param2.toString());
	}
	else if (a_event_name == "event_create_asset")
	{
		auto action = a_param1.toString();
		auto out_dir = current_dir;
		notifyEvent("event_create_asset_in_dir", action, out_dir);
	}
	else if (a_event_name == "onEditResListItem")
	{
		onEditNewItem(a_param2.toString());
	}
	else if (a_event_name == "onResDirEntered")
	{
		onResDirEntered(a_param1.toString(), a_param2.toInt());
	}
	else if (a_event_name == "onResDirRefresh")
	{
		refreshCurrentDir();
	}
	else if (a_event_name == "onRenameResListItem")
	{
		onRenameResListItem(a_param1.toModelIndex(), a_param2.toString());
	}
	else if (a_event_name == "onRenameResItemFinish")
	{
		onRenameResItemFinish(a_param1.toString(), a_param2.toBool());
	}
	else if (a_event_name == "onRemoveResListItem")
	{
		onRemoveListItem();
	}
	else if (a_event_name == "onRemoveResItemFinish")
	{
		onRemoveResItemFinish();
	}
	else if (a_event_name == "event_import_res")
	{
		onImportRes();
	}
}

void OvrResourceListWidget::onEditNewItem(const QString& a_res_file)
{
	if (a_res_file.isEmpty())
	{
		editItem(currentIndex());
	}
	else
	{
		refreshCurrentDir();
		editItem(locateFile(a_res_file));
	}
}

void OvrResourceListWidget::onBarActionTriggered(QAction *action)
{
	if (action->objectName() == "res_create")
	{
		auto index = currentIndex();
		bool is_item_selected = index.isValid();
		bool is_dir_selected = false;

		//当前树节点的路径类型有效
		if (!current_dir.isEmpty() && current_dir_type != -1)
		{
			if (is_item_selected)
			{
				QListWidgetItem* list_item = static_cast<QListWidgetItem*>(index.internalPointer());
				if (list_item->data(kItemDirFlag).toBool())
				{
					is_dir_selected = true;
				}
			}
			OvrResourceMenuManager * memu_mgr = OvrResourceMenuManager::getInst();
			memu_mgr->initListMenu(current_dir_type, is_item_selected, is_dir_selected);

			QWidget* p = action->parentWidget();
			assert(p->inherits("QToolBar"));
			QToolBar* bar = static_cast<QToolBar*>(p);
			QWidget* w = bar->widgetForAction(action);
			QPoint pt = w->geometry().bottomLeft();
			pt.setY(bar->height() + 1);
			memu_mgr->showCreateMenu(bar->mapToGlobal(pt));
		}
		else
		{
			OvrHelper::getInst()->showStatus(QString::fromLocal8Bit("请选择资源文件夹后再进行操作..."), 3000);
		}
	}
	else if (action->objectName() == "res_import")
	{
		onImportRes();
	}
	else if (action->objectName() == "res_search")
	{

	}
	else if (action->objectName() == "res_view_mode")
	{
		bIconMode = !bIconMode;
		const char* icon_name = bIconMode ? "icon_listtwo.png" : "icon_listone.png";
		action->setIcon(QIcon(getImagePath(icon_name)));
		setIconSize(bIconMode ? big_size: small_size);
		setViewMode(bIconMode ? IconMode : ListMode);
		set_custom_style_sheet(bIconMode);
	}
	else if (action->objectName() == "res_zoom")
	{

	}
	else
	{
		assert(false);
	}
}

void OvrResourceListWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		drag_item = itemAt(event->pos());
		if (drag_item != nullptr && drag_item->data(kItemDirFlag).toBool())
		{
			drag_item = nullptr;
		}
	}
	QListWidget::mousePressEvent(event);
}

void OvrResourceListWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (drag_item != nullptr)
	{
		QString asset_path = drag_item->data(kItemPath).toString();
		if (canBeDraged(asset_path))
		{
			QString work_dir = QString::fromLocal8Bit(ovr_project::OvrProject::GetIns()->GetWorkDir().GetStringConst());
			asset_path = asset_path.mid(work_dir.length());
			DragAsset(this, asset_path);
			drag_item = nullptr;
		}
	}
	QListWidget::mouseMoveEvent(event);
}

bool OvrResourceListWidget::canBeDraged(const QString& a_asset_path)
{
	static QString files_canbe_draged = ".mesh;.wav;.mp3;.mp4;.ani;.pani;";
	int index = a_asset_path.lastIndexOf(".");
	if (index != -1)
	{
		if (files_canbe_draged.indexOf(a_asset_path.mid(index) + ";") != -1)
		{
			return true;
		}
	}
	return false;
}

void OvrResourceListWidget::contextMenuEvent(QContextMenuEvent *event)
{
	QModelIndex index = indexAt(event->pos());
	bool is_item_selected = index.isValid();
	setCurrentIndex(index);

	//当前树节点的路径类型有效
	if (!current_dir.isEmpty() && current_dir_type != -1)
	{
		bool is_dir_selected = false;
		if (is_item_selected)
		{
			QListWidgetItem* list_item = static_cast<QListWidgetItem*>(index.internalPointer());
			if (list_item->data(kItemDirFlag).toBool())
			{
				is_dir_selected = true;
			}
		}

		auto memu_mgr = OvrResourceMenuManager::getInst();
		memu_mgr->initListMenu(current_dir_type, is_item_selected, is_dir_selected);
		memu_mgr->show(cursor().pos());
	}
}

void OvrResourceListWidget::init_custom_ui(bool bBigMode)
{
	setIconSize(bBigMode ? big_size : small_size);
	setViewMode(bIconMode ? QListView::ViewMode::IconMode : QListView::ViewMode::ListMode);
	setMovement(QListView::Movement::Static);
}

void OvrResourceListWidget::set_custom_style_sheet(bool bBigMode)
{
	QString strDlgStyle = QString::fromLocal8Bit(
		"QListWidget{color:rgb(204, 204, 204); background-color:rgb(39, 39, 39); border:none; border-top:1px solid rgb(30, 30, 30);}"
		//"QListWidget::item {border:1px solid rgb(30, 30, 30); height: 70px; width: 50px; margin: 5px;}"
		"QListWidget::item:hover{background-color:rgb(53, 53, 53); color:rgb(204, 204, 204);}"
		//"QListWidget::item:focus {selection-background-color: rgb(212, 212, 212);}"
		"QListWidget::item:selected{background-color:rgb(83, 83, 83); selection-color:rgb(204, 204, 204);}"
	);

	QString strStyleSmall = QString::fromLocal8Bit("QListWidget::item {border-bottom:1px solid rgb(30, 30, 30); height: 28px; }");
	QString strStyleBig = QString::fromLocal8Bit("QListWidget::item {border:0px solid rgb(30, 30, 30); height: 70px; width: 50px; margin: 5px;}");

	strDlgStyle += bBigMode ? strStyleBig : strStyleSmall;

	setStyleSheet(strDlgStyle);
}

int OvrResourceListWidget::GetItemCount(QString a_type, QStringList &name_list)
{
	int count = 0;
	name_list.clear();

	for (int i = 0; i < this->count(); i++) 
	{
		QString strUiResName = this->item(i)->text();
		int temp_index = strUiResName.lastIndexOf(QString::fromLocal8Bit("."));
		QString temp_type = strUiResName.right(strUiResName.length() - temp_index - 1);

		if (temp_type == a_type) {
			count++;
			name_list.append(strUiResName);
		}
	}

	return count;
}

void OvrResourceListWidget::editItem(const QModelIndex & a_index)
{
	if (a_index.isValid())
	{
		QListWidgetItem * item = static_cast<QListWidgetItem *>(a_index.internalPointer());
		openPersistentEditor(item);
	}

}

bool OvrResourceListWidget::selectImportFiles(QStringList& a_files)
{
	QString title = QString::fromLocal8Bit("导入资源");
	QString dir = OvrProjectManager::getInst()->getConfiger()->loadKeyValue("lastImportDlgDir", "", true);
	QString strFilter = QString::fromLocal8Bit(
		"All files(*.*);;"
		"Model files(*.fbx);;"
		"Images files(*.png *.jpeg *.jpg *.tif *.tiff *.bmp *.svg *.tga *.ico *.gif *.raw);;"
		"Video files(*.mp4 *.mov *.m4v *.mkv *.mpg *.avi *.ts *.rmvb *.rm *.wmv);;"
		"Audio files(*.mp3 *.mp2 *.ogg *.aac *.wma *.mka)"
	);

	QFileDialog open_file_dlg(nullptr, title, dir, strFilter);
	open_file_dlg.setFileMode(QFileDialog::FileMode::ExistingFiles);

	if (open_file_dlg.exec() == QDialog::Rejected) 
	{
		return false;
	}

	QString newLastDir = open_file_dlg.directory().absolutePath();
	OvrProjectManager::getInst()->getConfiger()->setKeyValue("lastImportDlgDir", newLastDir);

	a_files = open_file_dlg.selectedFiles();

	return a_files.size() > 0;
}

bool OvrResourceListWidget::importRes(const QStringList& a_files)
{
	assert(a_files.size() > 0);

	auto out_sel_dir = currentSelDir();
	auto out_relate_dir = out_sel_dir;

	OvrProjectManager::getInst()->makeRelateDir(out_relate_dir);
	out_relate_dir += "/";

	ResImportDlg dlg(OvrHelper::getInst()->getMainWindow());
	int ret = dlg.ImportFile(a_files, out_relate_dir);

	if (ret == 1)
	{
		if (current_dir == out_sel_dir)
		{
			refreshCurrentDir();
		}
		else
		{
			notifyEvent("onOpenResSubDir", out_sel_dir, 0);
		}
		return true;
	}
	else if (ret == 0)
	{
		OvrHelper::getInst()->warning("资源导入失败！", "资源管理");
	}

	return false;
}