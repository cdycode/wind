
#pragma once


#include <QObject>

class OvrResourceTreeItem : public QObject
{
	Q_OBJECT

public:
    explicit OvrResourceTreeItem(const QList<QVariant> &data, OvrResourceTreeItem* parentItem = nullptr);
    virtual ~OvrResourceTreeItem();

	int childCount() const;
	OvrResourceTreeItem *child(int row);
	void appendChild(OvrResourceTreeItem *child);

    int columnCount() const;
	QVariant data(int column) const;
	void setData(int column, const QVariant& data);

    int row() const;

    OvrResourceTreeItem *parentItem();
	QList<OvrResourceTreeItem*> &children();

	void clearChild();

	OvrResourceTreeItem* appendItem(const QString& a_title, const QString& a_path);
	OvrResourceTreeItem* appendItem(const QString& a_title, const QString& a_path, int a_type);
	void removeChild(int a_row);

	int dirType() { return dir_type; }
	void setDirType(int a_type) { dir_type = a_type; }

	bool isKeyNode() { return is_key_node; }
	void setKeyNode() { is_key_node = true; }

	//void setFlags(Qt::ItemFlag a) override;
protected:
	OvrResourceTreeItem *m_parentItem;
	QList<OvrResourceTreeItem*> m_childItems;
    QList<QVariant> m_itemData;

	int dir_type = kRDTRoot;
	bool is_key_node = false;
};
