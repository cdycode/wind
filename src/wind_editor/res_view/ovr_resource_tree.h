#pragma once

#include <QTreeView>

class OvrResourceTree :	public QTreeView, public OvrEventReceiver, public OvrEventSender
{
	Q_OBJECT
public:
	explicit OvrResourceTree(QWidget *parent = Q_NULLPTR);
	void init();

	virtual QString senderName() const override { return "OvrResourceTree"; }

	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;
	virtual QString name() override { return "OvrResourceTree"; }

protected:
	virtual void contextMenuEvent(QContextMenuEvent *event) override;

	void editItem(QModelIndex a_index);
	void onLocateRes(const QString & a_path);
	void locateDir(const QString &a_dir_path);

	bool onRenameSubItem(const QModelIndex& a_index, const QString& a_sub_path, const QString& a_new_name);
	void onRenamResCurDir(const QString& a_new_name);
	bool renameItem(const QModelIndex& a_index, const QString& a_new_name, bool is_current);
	void onRenameResItemFinish(const QString& a_new_path, bool a_is_sub);

	void onRemoveResSubDir(const QModelIndex& a_index, const QString& a_sub_path);

	void onOpenSubDir(QString a_dir);
	void onModelReset();
	void onItemClicked(const QModelIndex &index);
	void currentChanged(const QModelIndex &current, const QModelIndex &previous) override;
	void searchTree(const QModelIndex& a_index, const QString& a_text);

private:
	QModelIndex first_clicked_record_index;

private:
	void set_custom_style_sheet();
};
