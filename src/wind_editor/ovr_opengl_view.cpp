#include "Header.h"
#include "ovr_opengl_view.h"

#include "ovr_qt_opengl_context.h"
#include "wx_project/ovr_preview_scene.h"

OvrOpenGLView::OvrOpenGLView(QWidget *parent, Qt::WindowFlags f) :QWidget(parent, f)
{
	setAttribute(Qt::WA_NativeWindow);
	windowHandle()->setSurfaceType(QWindow::OpenGLSurface);
	setStyleSheet("background:black");

	current_gl_context = new OvrQtOpenglContext(windowHandle());

	//ovr_project::OvrPreviewScene::GetIns()->Init(current_gl_context);
}

OvrOpenGLView::~OvrOpenGLView()
{
	//ovr_project::OvrPreviewScene::GetIns()->Uninit();
	//ovr_project::OvrPreviewScene::DelIns();

	if (current_gl_context != nullptr)
	{
		delete current_gl_context;
	}
}

void OvrOpenGLView::paintEvent(QPaintEvent *event)
{
	ApplyWidgetStyleSheet(this);
}

OvrQtOpenglContext* OvrOpenGLView::renderContext()
{
	return current_gl_context;
}