#pragma once

#include "ovr_obj_editor_item.h"
#include "ovr_obj_editor_Field_context.h"

namespace ovr_core
{
	class IOvrVisualNode;
	class OvrNodeInterface;
	struct OvrNodeParam;
}
class OvrInterfaceItemEx;
class OvrObjEditorContainer;

class OvrObjEditorField : public OvrInterfaceHelperEx
{
public:
	OvrObjEditorField(ovr_core::OvrNodeInterface* a_field_conn);
	OvrObjEditorField(const ovr_core::OvrNodeParam* a_field_param, bool a_is_in);
	~OvrObjEditorField();

	void init();
	void setNodeObject(ovr_core::IOvrVisualNode* a_obj) { node_obj = a_obj; }
	void setContainer(OvrObjEditorContainer* a_container) { container = a_container; }

	OvrInterfaceItemEx* getConnPoint() { return conn_point; }
	const QList<OvrObjEditorFieldContext*>& getFieldContexts() { return field_contexts; }

	//OvrInterfaceHelperEx
	const QString& getFieldName() override { return field_name; }
	virtual bool canBeConnected(OvrInterfaceItemEx* a_interface) override;
	virtual bool canCeateNewConnection() override;
	virtual void buildConnection(OvrInterfaceItemEx* a_interface) override;
	virtual bool disConnectOut(OvrInterfaceItemEx* a_interface) override;
	virtual bool isIn() override { return is_in; }

protected:
	void initConnPoint();
	QString getInterfaceColor();
	bool isSimpleInput();

	void addCheck(OvrObjEditorFieldContext* context, const QString& a_value);
	QLineEdit* addInput(OvrObjEditorFieldContext* context, const QString& a_value);
	QLabel* addColor(OvrObjEditorFieldContext* context, const QString& a_value);

	void addInput(OvrObjEditorFieldContext* context, int a_count);
	void addResButton(OvrObjEditorFieldContext* context);
	void addMarkButton(OvrObjEditorFieldContext* context);
	void notify();

	bool is_in;
	int field_type;
	QString field_name;
	QString field_title;
	QString field_value;
	QString field_option;

	OvrInterfaceItemEx* conn_point = nullptr;
	QList<OvrObjEditorFieldContext*> field_contexts;

	ovr_core::IOvrVisualNode* node_obj = nullptr;
	OvrObjEditorContainer* container = nullptr;
};

class QColorLabel :public QLabel
{
	Q_OBJECT
public:

signals:
	void onSelectColor();

protected:
	bool event(QEvent *event) override
	{
		if (event->type() == QEvent::MouseButtonPress)//MouseButtonDblClick
		{
			event->accept();
			emit onSelectColor();
			return true;
		}
		return QLabel::event(event);
	}
};
