#pragma once

#include <QGraphicsWidget>
#include "ovr_obj_editor_Field.h"

class QGraphicsGridLayout;
class QGraphicsLinearLayout;

class OvrObjEditorContainer :public QGraphicsWidget
{
	Q_OBJECT

public:
	enum FieldPos {
		kFPIn, kFPInTitle,

		kFPInSubTitle0, kFPValue0, 
		kFPInSubTitle1, kFPValue1,
		kFPInSubTitle2, kFPValue2,
		kFPInSubTitle3, kFPValue3,

		kFPButton, 

		kFPOutTitle, kFPOut, 

		kFPCount
	};
	OvrObjEditorContainer(QGraphicsItem *parent = Q_NULLPTR, Qt::WindowFlags wFlags = Qt::WindowFlags());
	~OvrObjEditorContainer();

	void init(ovr_core::IOvrVisualNode* a_node_obj);

	const QString& getObjectID();

	ovr_core::IOvrVisualNode* getNodeObject() { return node_obj; }

	OvrInterfaceItemEx* getConnPoint(const OvrString& a_field_name);

	const QMap<QString, OvrObjEditorField*>& getFields() { return fields; }

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = Q_NULLPTR) override;

	void reload();

signals:
	void onContainerPosChanged();
	void reloadObj(OvrObjEditorContainer* a_obj);

protected:
	QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

	void initContext();

	void addTitle(const QString& a_text);
	QString getNodeObjTitle();
	void addSpace();
	void addField(OvrObjEditorField* a_field);
	void addMidTitle(int a_row, const QString& a_text, bool is_in);
	void AddSmallTitle(int a_row, const QString& a_text);
	void addCheck(int a_row, QCheckBox* a_button);
	void addInput(int a_row, QLineEdit* a_input);
	void addLabel(int a_row, QLabel* a_input);
	void addInput(int a_row, int a_pos, const QString& a_text, QLineEdit* a_input);
	void addButton(int a_row, QPushButton* a_button);
	void checkInParams();

	QPainterPath shape() const override;
	QPainterPath outlineShape() const;

	QGraphicsGridLayout *grid_layout;
	QGraphicsLinearLayout* line_layout = nullptr;

	int in_row_pos = 0;
	int out_row_pos = 0;

	ovr_core::IOvrVisualNode* node_obj = nullptr;
	QString container_id;
	QMap<QString, OvrObjEditorField*> fields;
	bool has_vector4 = false;
};