#include "Header.h"
#include "ovr_obj_editor_cluster_manager.h"

template<class T>
class OvrObjEditorClusterManagerEx : public OvrObjEditorClusterManager
{
public:
	OvrObjEditorClusterManagerEx(T* a_track, ovr_core::IOvrNodeCluster::Type a_type, const char* a_name) :track(a_track)
	{
		obj_type = a_type;
		editor_name = QString::fromLocal8Bit(a_name);
	}

	bool loadCluster(const char* a_area_name) override
	{
		cluster = track->LoadCluster(a_area_name);
		return cluster != nullptr;
	}
	void saveCluster() override
	{
		track->SaveCluster(cluster);
	}
	void closeCluster() override
	{
		track->CloseCluster(cluster);
	}

protected:
	T* track = nullptr;
};

const QString& OvrObjEditorClusterManager::getActorClassID()
{
	if (actor_class_id.isEmpty())
	{
		actor_class_id = QString::fromLocal8Bit(ovr_core::IOvrNodeManager::GetIns()->GetActorNodeID().GetStringConst());
	}

	assert(!actor_class_id.isEmpty());
	return actor_class_id;
}

bool OvrObjEditorClusterManager::getNodeClasses(std::list<ovr_core::OvrNodeInfo>& a_node_infos)
{
	assert(obj_type != ovr_core::IOvrNodeCluster::kInvalid);

	if (obj_type != ovr_core::IOvrNodeCluster::kInvalid)
	{
		a_node_infos = ovr_core::IOvrNodeManager::GetIns()->GetNodeIDList(obj_type);
		return true;
	}
	return false;
}

const QString& OvrObjEditorClusterManager::getEditorName()
{
	assert(!editor_name.isEmpty());

	if (wnd_title.isEmpty())
	{
		wnd_title = area_name + " - " + editor_name;
	}
	return wnd_title;
}

ovr_core::IOvrVisualNode* OvrObjEditorClusterManager::CreateNode(const QString& a_node_class)
{
	return cluster->CreateNode(a_node_class.toLocal8Bit().constData());
}

void OvrObjEditorClusterManager::DestoryNode(ovr_core::IOvrVisualNode* a_node)
{
	if (cluster != nullptr);
	{
		cluster->DestoryNode(a_node);
	}
}

bool OvrObjEditorClusterManager::GetAllNodeMap(std::map<OvrString, ovr_core::IOvrVisualNode*>& a_map)
{
	if (cluster != nullptr);
	{
		a_map = cluster->GetAllNodeMap();
		return true;
	}
	return false;
}

void OvrObjEditorClusterManager::setViewOffset(float x, float y)
{
	cluster->SetViewOffset(x, y);
}

void OvrObjEditorClusterManager::setViewScale(float a_scale)
{
	cluster->SetViewScale(a_scale);
}

float OvrObjEditorClusterManager::getViewX()
{
	return cluster->GetViewX();
}

float OvrObjEditorClusterManager::getViewY()
{
	return cluster->GetViewY();
}

float OvrObjEditorClusterManager::getViewScale()
{
	return cluster->GetViewScale();
}

OvrObjEditorClusterManager* CreateObjClusterManager(ovr_core::OvrEventTrack* track)
{
	return new OvrObjEditorClusterManagerEx<ovr_core::OvrEventTrack>(track, ovr_core::IOvrNodeCluster::kEvent, "�¼��༭��");
}

OvrObjEditorClusterManager* CreateObjClusterManager(ovr_core::OvrInterActiveTrack* track)
{
	return new OvrObjEditorClusterManagerEx<ovr_core::OvrInterActiveTrack>(track, ovr_core::IOvrNodeCluster::kArea, "�����༭��");
}