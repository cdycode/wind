#include "Header.h"
#include "ovr_obj_editor_item.h"

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QStyleOptionGraphicsItem>

#define kDefaultTextColor	"#CCCCCC"
#define kLineColor			"#5c5c5c"
#define kDragLineColor		"#CCCCCC"

void ovr_graphicsItem_highlightSelected(QGraphicsItem *item, QPainter *painter, const QStyleOptionGraphicsItem *option, const QPainterPath& path)
{
	const QRectF murect = painter->transform().mapRect(QRectF(0, 0, 1, 1));
	if (qFuzzyIsNull(qMax(murect.width(), murect.height())))
		return;

	const QRectF mbrect = painter->transform().mapRect(item->boundingRect());
	if (qMin(mbrect.width(), mbrect.height()) < qreal(1.0))
		return;

	qreal itemPenWidth = 1.0;
	const qreal pad = itemPenWidth / 2;
	const qreal penWidth = 0; // cosmetic pen

	const QColor fgcolor = option->palette.windowText().color();
	const QColor bgcolor( // ensure good contrast against fgcolor
		fgcolor.red() > 127 ? 0 : 255,
		fgcolor.green() > 127 ? 0 : 255,
		fgcolor.blue() > 127 ? 0 : 255);

	painter->setPen(QPen(bgcolor, penWidth, Qt::SolidLine));
	painter->setBrush(Qt::NoBrush);
	painter->drawPath(path);

	painter->setPen(QPen(option->palette.windowText(), 0, Qt::DashLine));
	painter->setBrush(Qt::NoBrush);
	painter->drawPath(path);
}

//////////////////////////////////////////////////
// class OvrTextItem

OvrTextItemEx::OvrTextItemEx(const QString &a_text, TextSize a_font_size, QGraphicsItem* a_parent) :QGraphicsTextItem(a_text, a_parent), QGraphicsLayoutItem()
{
	setGraphicsItem(this);

	QFont new_font = font();
	new_font.setPointSize(a_font_size);
	setFont(new_font);
	setDefaultTextColor(QColor(kDefaultTextColor));
}

QSizeF OvrTextItemEx::sizeHint(Qt::SizeHint which, const QSizeF &constraint) const
{
	return boundingRect().size();
}

void OvrTextItemEx::setGeometry(const QRectF &geom)
{
	prepareGeometryChange();
	QGraphicsLayoutItem::setGeometry(geom);
	setPos(geom.topLeft());
}

//////////////////////////////////////////////////
// class OvrLineItem

OvrLineItemEx::OvrLineItemEx() :QGraphicsLayoutItem()
{
	setGraphicsItem(this);

	setPen(QPen(kLineColor));
}

void OvrLineItemEx::setGeometry(const QRectF &geom)
{
	prepareGeometryChange();	
	QGraphicsLayoutItem::setGeometry(geom);	
	setPos(geom.topLeft());
	setLine(0, 0, geom.width(), 0);
}

QSizeF OvrLineItemEx::sizeHint(Qt::SizeHint which, const QSizeF &constraint) const
{
	switch (which) 
	{
	case Qt::MinimumSize:
	case Qt::PreferredSize:
		return QSize(3, line_v_space);
	case Qt::MaximumSize:
		return QSizeF(1000, line_v_space);
	default:
		break;
	}
	return constraint;
}

QRectF OvrLineItemEx::boundingRect() const
{
	QRectF rc(QPointF(0, 0), geometry().size());
	return rc;
}

QPainterPath OvrLineItemEx::shape() const
{
	QPainterPath path;
	path.addRect(boundingRect());
	return path;
}

//////////////////////////////////////////////////
// class OvrInterfaceItem

OvrInterfaceItemEx::OvrInterfaceItemEx(InterfaceShape a_shape, OvrInterfaceHelperEx* a_helper):QGraphicsObject(), QGraphicsLayoutItem()
, conn_shape(a_shape)
, helper(a_helper)
{
	setGraphicsItem(this);

	setAcceptHoverEvents(true);
	setFlag(QGraphicsItem::ItemIsSelectable);
}

void OvrInterfaceItemEx::setColor(const QString& a_color)
{
	bgn_color.setNamedColor(a_color);
}

void OvrInterfaceItemEx::setGeometry(const QRectF &geom)
{
	prepareGeometryChange();
	QGraphicsLayoutItem::setGeometry(geom);
	setPos(geom.topLeft());
}

QSizeF OvrInterfaceItemEx::sizeHint(Qt::SizeHint which, const QSizeF &constraint) const
{
	switch (which)
	{
	case Qt::MinimumSize:
	case Qt::PreferredSize:
	case Qt::MaximumSize:
		return QSize(i_w_h, i_w_h);
	default:
		break;
	}
	return constraint;
}

QRectF OvrInterfaceItemEx::boundingRect() const
{
	QRectF rc(0, 0, i_w_h, i_w_h);
	return rc;
}

QPainterPath OvrInterfaceItemEx::shape() const
{
	QPainterPath path;
	path.addRect(boundingRect());
	return path;
}

void OvrInterfaceItemEx::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
	is_hover_in = true;
	QGraphicsObject::hoverEnterEvent(event);
}

void OvrInterfaceItemEx::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
	is_hover_in = false;
	QGraphicsObject::hoverLeaveEvent(event);
}

void OvrInterfaceItemEx::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	calcCustomeBrush(painter);

	if (conn_shape == kShapePoint)
	{
		painter->drawEllipse(boundingRect());
	}
	else
	{
		static QPointF points[3] = {
			QPointF(0,0), QPointF(i_w_h, i_w_h / 2), QPointF(0, i_w_h)
		};
		painter->drawPolygon(points, 3);
	}

	if (option->state & QStyle::State_Selected)
	{
		ovr_graphicsItem_highlightSelected(this, painter, option, shape());
	}
}

QVariant OvrInterfaceItemEx::itemChange(GraphicsItemChange change, const QVariant &value)
{
	if (change == ItemSceneChange)
	{
		//被移除场景
		if (scene() != nullptr && value.value<void*>() == nullptr)
		{
			//删除连接线
			for (auto item : conn_lines)
			{
				item->removeInterface(this);
				scene()->removeItem(item);				
				delete item;
			}
			conn_lines.clear();
		}
	}
	return value;
}

void OvrInterfaceItemEx::calcCustomeBrush(QPainter *painter)
{
	QColor fill_color = is_hint ? bgn_color.darker() : bgn_color;

	painter->setPen(fill_color);
	is_hover_in || conn_lines.size() > 0 ? painter->setBrush(fill_color) : painter->setBrush(Qt::transparent);
}

bool OvrInterfaceItemEx::canBeConnected(QGraphicsItem* a_interface)
{
	if (a_interface != nullptr && a_interface->type() == OvrInterfaceItemEx::Type)
	{
		return canBeConnected(qgraphicsitem_cast<OvrInterfaceItemEx*>(a_interface));
	}
	return false;
}

bool OvrInterfaceItemEx::isConnected()
{
	for (auto item : conn_lines)
	{
		if (item->isConnected())
		{
			return true;
		}
	}
	return false;
}

bool OvrInterfaceItemEx::isNotConnected()
{
	return !isConnected();
}

bool OvrInterfaceItemEx::isNotConnected(OvrInterfaceItemEx* a_interface)
{
	for (auto conn_line : conn_lines)
	{
		if (conn_line->hasInterface(a_interface))
		{
			return false;
		}
	}
	return true;
}

bool OvrInterfaceItemEx::isLineSelected()
{
	bool is_selected = false;

	for (auto item : conn_lines)
	{
		if (item->isSelected())
		{
			is_selected = true;
			break;
		}
	}
	return is_selected;
}


bool OvrInterfaceItemEx::canBeConnected(OvrInterfaceItemEx* a_interface)
{
	assert(helper != nullptr);

	if (a_interface != this)
	{
		return helper->canBeConnected(a_interface);
	}
	return false;
}

void OvrInterfaceItemEx::buildConnection(OvrInterfaceItemEx* a_interface)
{
	assert(helper != nullptr);

	helper->buildConnection(a_interface);
}

bool OvrInterfaceItemEx::disConnectOut()
{
	assert(helper != nullptr);

	return helper->disConnectOut(this);
}

void OvrInterfaceItemEx::addConnLine(OvrConnLineItemEx* a_line)
{
	assert(a_line != nullptr);
	assert(conn_lines.indexOf(a_line) == -1);

	conn_lines.push_back(a_line);

	update();
}

void OvrInterfaceItemEx::removeConnLine(OvrConnLineItemEx* a_line)
{
	int index = conn_lines.indexOf(a_line);
	if (index != -1)
	{
		conn_lines.removeAt(index);
	}
}

void OvrInterfaceItemEx::updateConnectedLine()
{
	for (auto item : conn_lines)
	{
		item->updatePos();
	}
}

void OvrInterfaceItemEx::on_hint_for_connection(OvrInterfaceItemEx* a_point)
{
	if (a_point != this)
	{
		if (!canBeConnected(a_point))
		{
			is_hint = true;
			update();
		}
	}
}

void OvrInterfaceItemEx::on_remove_hint_for_connection()
{
	if (is_hint)
	{
		is_hint = false;
		update();
	}
}

////////////////////////////////////////////////////////////
// class OvrConnLineItem

OvrConnLineItemEx::OvrConnLineItemEx(const QString& a_color, QGraphicsItem *parent) :QGraphicsPathItem(parent)
{
	setPen(QPen(QColor(a_color), 3, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	setFlag(QGraphicsItem::ItemIsSelectable, true);
}

OvrConnLineItemEx::~OvrConnLineItemEx()
{
}

void OvrConnLineItemEx::removeConn()
{
	if (interface_from != nullptr)
	{
		interface_from->removeConnLine(this);
		interface_from = nullptr;
	}
	if (interface_to != nullptr)
	{
		interface_to->removeConnLine(this);
		interface_to = nullptr;
	}
}

bool OvrConnLineItemEx::disConnect()
{
	assert(interface_from != nullptr && interface_to != nullptr);

	bool is_ok = interface_from->disConnectOut() || interface_to->disConnectOut();
	assert(is_ok);

	return is_ok;
}

bool OvrConnLineItemEx::isConnected()
{
	return interface_from != nullptr && interface_to != nullptr;
}

void OvrConnLineItemEx::removeInterface(OvrInterfaceItemEx* a_point)
{
	if (interface_from == a_point)
	{
		interface_from = nullptr;
	}
	else if (interface_to == a_point)
	{
		interface_to = nullptr;
	}
}

bool OvrConnLineItemEx::hasInterface(OvrInterfaceItemEx* a_point)
{
	if (a_point != nullptr)
	{
		return interface_from == a_point || interface_to == a_point;
	}
	return false;
}

void OvrConnLineItemEx::dragEndTo(OvrInterfaceItemEx* a_from, const QPointF& a_end)
{
	QPointF pt_start = a_from->sceneBoundingRect().center();
	setPos(pt_start);

	QPainterPath conn_path;
	conn_path.moveTo(0, 0);

	QPointF end_point = mapFromScene(a_end);
	qreal distance = abs(end_point.x() / 2);
 	if (a_from->getHelper()->isIn())
 	{
 		distance = -distance;
 	}

	qreal x1 = distance, x2 = end_point.x() - distance;
	qreal y1 = 0, y2 = end_point.y();

	//控制点1
	QPointF ct1(x1, y1);//-end_point.y() * 0.5
	QPointF ct2(x2, y2); // * 1.5

	conn_path.cubicTo(ct1, ct2, end_point);

	prepareGeometryChange();
	setPath(conn_path);
}

void OvrConnLineItemEx::updatePos()
{
	if (interface_from != nullptr && interface_to != nullptr)
	{
		dragEndTo(interface_from, interface_to->sceneBoundingRect().center());
	}
}

void OvrConnLineItemEx::setConnFromTo(OvrInterfaceItemEx* a_from, OvrInterfaceItemEx* a_to)
{
	assert(a_from != nullptr && a_to != nullptr);

	interface_from = a_from;
	interface_to = a_to;

	interface_from->addConnLine(this);
	interface_to->addConnLine(this);

	setZValue(-1);
}

void OvrConnLineItemEx::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	if (option->state & QStyle::State_Selected)
	{
		painter->save();
		QStyleOptionGraphicsItem opt = *option;
		opt.state &= ~QStyle::State_Selected;
		QGraphicsPathItem::paint(painter, &opt, widget);
		painter->restore();

		ovr_graphicsItem_highlightSelected(this, painter, option, path().translated(0, 10));
		ovr_graphicsItem_highlightSelected(this, painter, option, path().translated(0, -10));
	}
	else
	{
		QGraphicsPathItem::paint(painter, option, widget);
	}
}

QVariant OvrConnLineItemEx::itemChange(GraphicsItemChange change, const QVariant &value)
{
	if (change == ItemSceneChange)
	{
		//被移除场景
		if (scene() != nullptr && value.value<void*>() == nullptr)
		{
			//解除连接关系
			removeConn();
		}
	}
	return value;
}
