#include "Header.h"
#include "ovr_obj_editor_view.h"


#define kViewUpdateMode QGraphicsView::SmartViewportUpdate
//MinimalViewportUpdate FullViewportUpdate BoundingRectViewportUpdate SmartViewportUpdate NoViewportUpdate

#include <QOpenGLWidget>
static QOpenGLWidget* createOpenGLView()
{
	QOpenGLWidget* opengl_view = new QOpenGLWidget;
	{
		//���������
		QSurfaceFormat f = opengl_view->format();
		f.setSamples(4);
		opengl_view->setFormat(f);
	}
	return opengl_view;
}

//////////////////////////////////////////////////
// class OvrObjEditorView

OvrObjEditorView::OvrObjEditorView(QWidget *parent):QGraphicsView(parent)
{
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	setBackgroundBrush(QBrush(Qt::black));
	setViewportUpdateMode(kViewUpdateMode);

	setRenderHint(QPainter::Antialiasing);
	setRenderHint(QPainter::TextAntialiasing);
	//setRenderHint(QPainter::SmoothPixmapTransform);
	setOptimizationFlag(QGraphicsView::DontSavePainterState);
	setOptimizationFlag(QGraphicsView::DontAdjustForAntialiasing);
	
	setViewport(createOpenGLView());
}


OvrObjEditorView::~OvrObjEditorView()
{
}

void OvrObjEditorView::resizeEvent(QResizeEvent *event)
{
	QGraphicsView::resizeEvent(event);

	updateViewForScene();
}

void OvrObjEditorView::updateViewForScene()
{
	qreal x = sceneRect().x(), y = sceneRect().y();
	setSceneRect(x, y, width(), height());
}

void OvrObjEditorView::mouseMoveEvent(QMouseEvent *event)
{
	QGraphicsView::mouseMoveEvent(event);

	if (is_begin_drag_scene)
	{
		int dx = event->x() - pt_drag_scene.x();
		int dy = event->y() - pt_drag_scene.y();
		if (scene() != nullptr)
		{
			qreal factor = transform().mapRect(QRectF(0, 0, 1, 1)).width();
			qreal x = pos_drag_scene.x() - dx / factor;
			qreal y = pos_drag_scene.y() - dy / factor;
			setSceneRect(x, y, width(), height());
			emit onViewOffsetChanged(x, y);
		}
	}
}

void OvrObjEditorView::mousePressEvent(QMouseEvent *event)
{
	QGraphicsView::mousePressEvent(event);

	if (event->button() == Qt::RightButton)
	{
		is_begin_drag_scene = true;
		pt_drag_scene = event->pos();
		pos_drag_scene = sceneRect().topLeft();
	}
}

void OvrObjEditorView::mouseReleaseEvent(QMouseEvent *event)
{
	QGraphicsView::mouseReleaseEvent(event);

	if (event->button() == Qt::RightButton)
	{
		is_begin_drag_scene = false;
	}
}

void OvrObjEditorView::wheelEvent(QWheelEvent *event)
{
	QGraphicsView::wheelEvent(event);

	float v = event->angleDelta().y() > 0 ? 1.1 : 1 / 1.1;
	qreal factor = transform().scale(v, v).mapRect(QRectF(0, 0, 1, 1)).width();
	scale(v, v);
	emit onViewScaleChanged(factor);
}

void OvrObjEditorView::onDrag(QPointF a_pt)
{
	if (scene() != nullptr)
	{
		a_pt.setX(a_pt.x() - width() / 2);
		a_pt.setY(a_pt.y() - height() / 2);
		setSceneRect(a_pt.x(), a_pt.y(), width(), height());
		emit onViewOffsetChanged(a_pt.rx(), a_pt.ry());
		update();
	}
}

void OvrObjEditorView::restoreOffset(float a_x, float a_y)
{
	QPointF pt(a_x, a_y);
	setSceneRect(pt.x(), pt.y(), width(), height());
}

void OvrObjEditorView::restoreScale(float a_value)
{
	QMatrix matrix;
	matrix.scale(a_value, a_value);
	setMatrix(matrix);
}
//////////////////////////////////////////////////
// class OvrObjEditorAbrevView

OvrObjEditorAbrevView::OvrObjEditorAbrevView(QWidget *parent) :QGraphicsView(parent)
{
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	setInteractive(false);

	setBackgroundBrush(QColor("#101010"));
	setViewportUpdateMode(NoViewportUpdate);

	setRenderHint(QPainter::Antialiasing);
	setRenderHint(QPainter::TextAntialiasing);
	setRenderHint(QPainter::SmoothPixmapTransform);
	setOptimizationFlag(QGraphicsView::DontSavePainterState);
	setOptimizationFlag(QGraphicsView::DontAdjustForAntialiasing);

	setViewport(createOpenGLView());
}

void OvrObjEditorAbrevView::mousePressEvent(QMouseEvent *event)
{
	QGraphicsView::mousePressEvent(event);

	if (scene() != nullptr)
	{
		is_drag = true;
		
		emit onDrag(mapToScene(event->pos()));
	}
}

void OvrObjEditorAbrevView::mouseMoveEvent(QMouseEvent *event)
{
	QGraphicsView::mouseMoveEvent(event);
	if (is_drag)
	{
		emit onDrag(mapToScene(event->pos()));
	}
}

void OvrObjEditorAbrevView::mouseReleaseEvent(QMouseEvent *event)
{
	QGraphicsView::mouseReleaseEvent(event);
	is_drag = false;
}

void OvrObjEditorAbrevView::delayUpdate(int a_interval)
{
	if (a_interval == 0)
	{
		timerEvent(nullptr);
	}
	else
	{
		if (timer_id != 0)
		{
			killTimer(timer_id);
		}
		timer_id = startTimer(a_interval);
	}
}

void OvrObjEditorAbrevView::timerEvent(QTimerEvent *event)
{	
	if (timer_id != 0)
	{
		killTimer(timer_id);
		timer_id = 0;
	}
	qDebug() << "OvrObjEditorAbrevView updated";

	fitInView(scene()->sceneRect().adjusted(-50, -50, 50, 50));
}