#pragma once

#include <QGraphicsScene>

class OvrConnLineItemEx;
class OvrInterfaceItemEx;
class OvrObjEditorContainer;
class OvrObjEditorClusterManager;
class QGraphicsRectItem;

namespace ovr_project
{
	class IOvrVisualNode;
}

class OvrObjEditorScene :public QGraphicsScene
{
	Q_OBJECT
public:
	OvrObjEditorScene(QObject *parent = nullptr);
	~OvrObjEditorScene();

	void init(OvrObjEditorClusterManager* a_obj_manager);

	void onDragItem(const QString& a_text, const QString& a_type, const QString& a_value, QObject* a_sender);

signals:
	void on_hint_for_connection(OvrInterfaceItemEx* a_point);
	void on_remove_hint_for_connection();

public slots:
	void reloadObj(OvrObjEditorContainer* a_obj);

protected:
	bool loadNodeObject();
	bool restoreConnection();
	bool restoreConnection(ovr_core::IOvrVisualNode* a_obj);

	void restoreConnection(OvrInterfaceItemEx* a_from, OvrInterfaceItemEx* a_to);
	OvrInterfaceItemEx* getConnPoint(const OvrString& a_id, const OvrString& a_field_id);

	OvrObjEditorContainer* createContainerObject(const QString& a_item_type, const QString& a_value);
	OvrObjEditorContainer* createContainerObject(ovr_core::IOvrVisualNode* a_node);
	void initContinerSlot(OvrObjEditorContainer* a_container);
	bool createPixmap(OvrObjEditorContainer* a_obj, QPixmap& a_pixmap);

	void dragEnterEvent(QGraphicsSceneDragDropEvent *event) override;
	void dragMoveEvent(QGraphicsSceneDragDropEvent *event) override;
	void dragLeaveEvent(QGraphicsSceneDragDropEvent *event) override;
	void dropEvent(QGraphicsSceneDragDropEvent *event) override;

	void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
	void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

	void keyPressEvent(QKeyEvent *event) override;

	bool bringUp();
	bool endBringUp();

	bool tryConnectInterFace();
	bool doConnectInterFace(const QPointF& a_scene_pt);
	bool endConnectInterFace(const QPointF& a_scene_pt);
	void removeInterfaceConnectLine(OvrInterfaceItemEx* a_point);

	bool trySelectItems(const QPointF& a_scene_pt);
	bool doSelectItems(const QPointF& a_scene_pt);
	bool endSelectItems();

	bool removeSelectedItems();

	OvrInterfaceItemEx* getConnectEndItem(const QPointF& a_pt);

	QGraphicsItem* bring_item = nullptr;

	OvrConnLineItemEx* conn_line = nullptr;
	OvrInterfaceItemEx* from_interface = nullptr;

	QGraphicsRectItem* select_rect = nullptr;
	QPointF pt_select_from;

	OvrObjEditorClusterManager* obj_manager = nullptr;
	OvrObjEditorContainer* drag_obj = nullptr;

	QMap<QString, OvrObjEditorContainer*> container_map;
};

