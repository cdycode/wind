#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_ovr_obj_editor_wnd.h"

class OvrObjEditorScene;
class OvrObjEditorDockTree;
class OvrObjEditorView;
class OvrObjEditorAbrevView;

class OvrObjEditorWnd : public QMainWindow, public OvrEventReceiver
{
	Q_OBJECT

public:
	static OvrObjEditorWnd* createInst();

	void init(class OvrObjEditorClusterManager* a_obj_manager);

	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;
	virtual QString name() override { return "OvrObjEditorWnd"; }

public slots:
	void onDragItem(const QString& a_text, const QString& a_type, const QString& a_value);

protected:
	OvrObjEditorWnd(QWidget *parent = Q_NULLPTR);
	~OvrObjEditorWnd();

	void closeEvent(QCloseEvent *event) override;

	void initTopDock();

	void initLeftDock();
	void addActor(QTreeWidgetItem* a_item, ovr_engine::OvrActor* a_actor);
	QTreeWidgetItem* addTreeItem(QTreeWidgetItem* a_parent_item, const QString& a_title, const QString& a_type_name, const QString& a_type_value);

	void initRightDock();
	void addNodeClassItem(const ovr_core::OvrNodeInfo& a_info);
	void addNodeClassItem(const QString& a_main_class, const QString& a_sub_class, const QString& a_node_class);
	QTreeWidgetItem* getRootItem(const QString& a_main_class);

	void initContext();

private:
	void init_title_bar();
	void init_custom_ui();
	void set_custom_style_sheet();
	void preset_connects();

protected slots:

private:
	void debug();

	Ui::OvrObjEditorWnd ui;

	OvrObjEditorDockTree* obj_list;

	OvrObjEditorDockTree* op_list;
	QMap<QString, QTreeWidgetItem*> root_entry;

	OvrObjEditorScene* scene = nullptr;
	OvrObjEditorView* editor_view = nullptr;
	OvrObjEditorAbrevView* abrev_view = nullptr;

	OvrObjEditorClusterManager* obj_manager = nullptr;
};

class OvrObjEditorDockTree :public QTreeWidget
{
	Q_OBJECT

public:
	explicit OvrObjEditorDockTree(QWidget *parent = Q_NULLPTR);
	~OvrObjEditorDockTree();
	QSize sizeHint() const override { return QSize(200, 500); }

signals:
	void onDragItem(const QString& a_text, const QString& a_type, const QString& a_value);

protected:
	void mousePressEvent(QMouseEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *event) override;
	void resizeEvent(QResizeEvent *event) override;

	QTreeWidgetItem* drag_item = nullptr;
};
