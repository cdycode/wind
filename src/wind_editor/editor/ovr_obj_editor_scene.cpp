#include "Header.h"
#include "ovr_obj_editor_scene.h"

#include "ovr_obj_editor_item.h"
#include "ovr_obj_editor_container.h"

#include "ovr_obj_editor_cluster_manager.h"

#include <QGraphicsSceneMouseEvent>
#include <QDrag>
#include <QMimeData>


OvrObjEditorScene::OvrObjEditorScene(QObject *parent):QGraphicsScene(parent)
{
	setItemIndexMethod(QGraphicsScene::NoIndex);
}

OvrObjEditorScene::~OvrObjEditorScene()
{
	obj_manager->saveCluster();
	obj_manager->closeCluster();
	delete obj_manager;
}

void OvrObjEditorScene::init(OvrObjEditorClusterManager* a_obj_manager)
{
	assert(a_obj_manager != nullptr);
	obj_manager = a_obj_manager;

	loadNodeObject() && restoreConnection();
}

bool OvrObjEditorScene::loadNodeObject()
{
	std::map<OvrString, ovr_core::IOvrVisualNode*> node_maps;
	obj_manager->GetAllNodeMap(node_maps);

	ovr_core::IOvrVisualNode* node_obj = nullptr;
	OvrObjEditorContainer* container = nullptr;

	//�ָ�����
	for (auto node : node_maps)
	{
		node_obj = node.second;
		assert(node_obj != nullptr);

		container = createContainerObject(node_obj);
		if (container != nullptr)
		{
			container->setPos(node_obj->GetPosistionX(), node_obj->GetPosistionY());
			addItem(container);
			container_map[container->getObjectID()] = container;
		}
	}

	return node_maps.size() > 0;
}

bool OvrObjEditorScene::restoreConnection()
{
	std::map<OvrString, ovr_core::IOvrVisualNode*> node_maps;
	obj_manager->GetAllNodeMap(node_maps);

	for (auto node : node_maps)
	{
		for (auto conn : node.second->GetOutConnects())
		{
			OvrInterfaceItemEx* from = getConnPoint(node.first, conn.local_interface);
			OvrInterfaceItemEx* to = getConnPoint(conn.other_node->GetUniqueName(), conn.other_interface);
			restoreConnection(from, to);
		}
	}

	return node_maps.size() > 0;
}

bool OvrObjEditorScene::restoreConnection(ovr_core::IOvrVisualNode* a_obj)
{
	std::map<OvrString, ovr_core::IOvrVisualNode*> node_maps;
	obj_manager->GetAllNodeMap(node_maps);

	for (auto node : node_maps)
	{
		if (node.second->GetUniqueName() == a_obj->GetUniqueName())
		{
			for (auto conn : node.second->GetOutConnects())
			{
				OvrInterfaceItemEx* from = getConnPoint(node.first, conn.local_interface);
				OvrInterfaceItemEx* to = getConnPoint(conn.other_node->GetUniqueName(), conn.other_interface);
				restoreConnection(from, to);
			}
		}
		else
		{
			for (auto conn : node.second->GetOutConnects())
			{
				if (conn.other_node->GetUniqueName() == a_obj->GetUniqueName())
				{
					OvrInterfaceItemEx* from = getConnPoint(node.first, conn.local_interface);
					OvrInterfaceItemEx* to = getConnPoint(conn.other_node->GetUniqueName(), conn.other_interface);
					restoreConnection(from, to);
				}
			}
		}
	}

	return node_maps.size() > 0;
}

void OvrObjEditorScene::reloadObj(OvrObjEditorContainer* a_obj)
{
	ovr_core::IOvrVisualNode* node_obj = a_obj->getNodeObject();

	removeItem(a_obj);
	a_obj->deleteLater();

	OvrObjEditorContainer*container = createContainerObject(node_obj);
	if (container != nullptr)
	{
		container->setPos(node_obj->GetPosistionX(), node_obj->GetPosistionY());
		addItem(container);
		container_map[container->getObjectID()] = container;
		restoreConnection(node_obj);
	}
}

OvrInterfaceItemEx* OvrObjEditorScene::getConnPoint(const OvrString& a_id, const OvrString& a_field_id)
{
	if (container_map.contains(a_id.GetStringConst()))
	{
		return container_map[a_id.GetStringConst()]->getConnPoint(a_field_id);
	}
	return nullptr;
}

void OvrObjEditorScene::restoreConnection(OvrInterfaceItemEx* a_from, OvrInterfaceItemEx* a_to)
{
	assert(a_from != nullptr && a_to != nullptr);

	OvrConnLineItemEx* conn_line = new OvrConnLineItemEx(a_from->getColor());
	conn_line->setConnFromTo(a_from, a_to);
	addItem(conn_line);
	conn_line->updatePos();
}

void OvrObjEditorScene::mousePressEvent(QGraphicsSceneMouseEvent *event) 
{
	QGraphicsScene::mousePressEvent(event);

	if (event->button() == Qt::LeftButton)
	{
		bringUp() || tryConnectInterFace() || trySelectItems(event->scenePos());
	}
}

void OvrObjEditorScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsScene::mouseMoveEvent(event);

	doConnectInterFace(event->scenePos()) || doSelectItems(event->scenePos());
}

void OvrObjEditorScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsScene::mouseReleaseEvent(event);

	endBringUp() || endConnectInterFace(event->scenePos()) || endSelectItems();
}

void OvrObjEditorScene::keyPressEvent(QKeyEvent *event)
{
	QGraphicsScene::keyPressEvent(event);

	if (event->key() == Qt::Key_Delete)
	{
		removeSelectedItems();
	}
}

bool OvrObjEditorScene::removeSelectedItems()
{
	QList<OvrObjEditorContainer*> container_items;
	QList<OvrConnLineItemEx*> conn_items;

	for (auto item : selectedItems())
	{
		if (item->type() == OvrObjEditorContainer::Type)
		{
			container_items.push_back(qgraphicsitem_cast<OvrObjEditorContainer*>(item));
		}
		else if (item->type() == OvrConnLineItemEx::Type)
		{
			conn_items.push_back(qgraphicsitem_cast<OvrConnLineItemEx*>(item));
		}
	}
	if (container_items.size() <= 0 && conn_items.size() <= 0)
	{
		return false;
	}

	//remove lines
	for (int i = 0; i < conn_items.count(); ++i)
	{
		conn_items.at(i)->disConnect();
		removeItem(conn_items.at(i));
		delete conn_items.at(i);
	}

	//remove container obj
	for (int i = container_items.count() - 1; i >= 0; --i)
	{
		container_map.remove(container_items.at(i)->getObjectID());
		obj_manager->DestoryNode(container_items.at(i)->getNodeObject());
		removeItem(container_items.at(i));
		delete container_items.at(i);
	}

	return true;
}

bool OvrObjEditorScene::bringUp()
{
	QGraphicsItem* item = mouseGrabberItem();
	if (item != nullptr && item->type() == OvrObjEditorContainer::Type)
	{
		item->setZValue(item->zValue() + 1);
		bring_item = item;
		return true;
	}
	return false;
}

bool OvrObjEditorScene::endBringUp()
{
	if (bring_item != nullptr)
	{
		bring_item->setZValue(bring_item->zValue() - 1);
		bring_item = nullptr;
		return true;
	}
	return false;
}

bool OvrObjEditorScene::tryConnectInterFace()
{
	QGraphicsItem* item = mouseGrabberItem();
	if (item != nullptr && item->type() == OvrInterfaceItemEx::Type)
	{
		from_interface = static_cast<OvrInterfaceItemEx*>(item);
		return true;
	}
	return false;
}

bool OvrObjEditorScene::doConnectInterFace(const QPointF& a_scene_pt)
{
	if (from_interface != nullptr)
	{
		if (conn_line == nullptr)
		{
			conn_line = new OvrConnLineItemEx(from_interface->getColor());
			conn_line->setPos(from_interface->sceneBoundingRect().center());
			addItem(conn_line);
			emit on_hint_for_connection(from_interface);
		}
		conn_line->dragEndTo(from_interface, a_scene_pt);
		return true;
	}
	return false;
}

bool OvrObjEditorScene::endConnectInterFace(const QPointF& a_scene_pt)
{
	bool is_done = from_interface != nullptr || conn_line != nullptr;

	if (conn_line != nullptr)
	{
		emit on_remove_hint_for_connection();

		bool is_ok = false;

		OvrInterfaceItemEx* end_interface = getConnectEndItem(a_scene_pt);
		if (end_interface != nullptr && end_interface != from_interface)
		{
			if (from_interface->canBeConnected(end_interface))
			{
				is_ok = true;
				if (!from_interface->getHelper()->canCeateNewConnection())
				{
					removeInterfaceConnectLine(from_interface);
				}
				if (!end_interface->getHelper()->canCeateNewConnection())
				{
					removeInterfaceConnectLine(end_interface);
				}

				conn_line->setConnFromTo(from_interface, end_interface);
				conn_line->updatePos();
				conn_line->setSelected(true);
				from_interface->buildConnection(end_interface);
			}
		}
		if (!is_ok)
		{
			removeItem(conn_line);
			delete conn_line;
			//show actor list menu here
		}
		conn_line = nullptr;
	}
	
	from_interface = nullptr;

	return is_done;
}

void OvrObjEditorScene::removeInterfaceConnectLine(OvrInterfaceItemEx* a_point)
{
	auto& conn_lines = a_point->getConnLines();
	assert(conn_lines.count() == 1);

	auto line_item = conn_lines.last();
	line_item->disConnect();
	removeItem(line_item);
	delete line_item;
}

bool OvrObjEditorScene::trySelectItems(const QPointF& a_scene_pt)
{
	if (items(a_scene_pt).size() == 0)
	{
		select_rect = new QGraphicsRectItem;
		select_rect->setPen(QPen(Qt::white, 1, Qt::DotLine, Qt::RoundCap, Qt::RoundJoin));
		select_rect->setBrush(Qt::NoBrush);
		select_rect->setRect(QRectF());
		pt_select_from = a_scene_pt;
		addItem(select_rect);
		return true;
	}

	return false;
}

bool OvrObjEditorScene::doSelectItems(const QPointF& a_scene_pt)
{
	if (select_rect != nullptr)
	{
		QRectF rc(pt_select_from, a_scene_pt);
		select_rect->setRect(rc.normalized());
		return true;
	}
	return false;
}

bool OvrObjEditorScene::endSelectItems()
{
	if (select_rect != nullptr)
	{
		QList<QGraphicsItem *> selected_items = items(select_rect->rect());
		removeItem(select_rect);

		for (auto item : selected_items)
		{
			if (item->type() == OvrObjEditorContainer::Type || item->type() == OvrConnLineItemEx::Type)
			{
				item->setSelected(true);
			}
		}

		delete select_rect;
		select_rect = nullptr;
		return true;
	}

	return false;
}

OvrInterfaceItemEx* OvrObjEditorScene::getConnectEndItem(const QPointF& a_pt)
{
	for (auto item : items(a_pt))
	{
		if (item->type() == OvrInterfaceItemEx::Type)
		{
			return static_cast<OvrInterfaceItemEx*>(item);
		}
	}
	return nullptr;
}

void OvrObjEditorScene::onDragItem(const QString& a_text, const QString& a_type, const QString& a_value, QObject* a_sender)
{
	drag_obj = createContainerObject(a_type, a_value);
	if (drag_obj != nullptr)
	{
		QPixmap icon;
		if (createPixmap(drag_obj, icon))
		{
			QDrag *drag = new QDrag(a_sender);
			{
				QMimeData *mimeData = new QMimeData;
				mimeData->setText(a_text);
				mimeData->setProperty("item_type", a_type);
				mimeData->setProperty("item_value", a_value);
				drag->setMimeData(mimeData);
			}

			drag->setPixmap(icon);
			drag->setHotSpot(QPoint(icon.width() / 2, icon.height() / 2));

			Qt::DropAction dropAction = drag->exec(Qt::CopyAction | Qt::MoveAction | Qt::LinkAction);
			if (dropAction == Qt::IgnoreAction)
			{
				obj_manager->DestoryNode(drag_obj->getNodeObject());
				drag_obj->deleteLater();
				drag_obj = nullptr;
			}
		}
	}
}

OvrObjEditorContainer* OvrObjEditorScene::createContainerObject(const QString& a_item_type, const QString& a_value)
{
	ovr_core::IOvrVisualNode* node = obj_manager->CreateNode(a_item_type);
	if (a_item_type == obj_manager->getActorClassID())
	{
		for (auto item : node->GetInParamList())
		{
			if (item.type == ovr_core::OvrNodeParam::kParamActor)
			{
				node->SetParamValue(true, item.unique_name, a_value.toLocal8Bit().constData());
				break;
			}
		}
	}
	return createContainerObject(node);
}

OvrObjEditorContainer* OvrObjEditorScene::createContainerObject(ovr_core::IOvrVisualNode* a_node)
{
	if (a_node != nullptr)
	{
		OvrObjEditorContainer* container = new OvrObjEditorContainer;
		container->init(a_node);
		initContinerSlot(container);
		return container;
	}
	return nullptr;
}

void OvrObjEditorScene::initContinerSlot(OvrObjEditorContainer* a_container)
{
	connect(a_container, &OvrObjEditorContainer::reloadObj, this, &OvrObjEditorScene::reloadObj, Qt::QueuedConnection);

	for (auto field : a_container->getFields())
	{
		connect(this, &OvrObjEditorScene::on_hint_for_connection, field->getConnPoint(), &OvrInterfaceItemEx::on_hint_for_connection, Qt::QueuedConnection);
		connect(this, &OvrObjEditorScene::on_remove_hint_for_connection, field->getConnPoint(), &OvrInterfaceItemEx::on_remove_hint_for_connection, Qt::QueuedConnection);
	}
}

bool OvrObjEditorScene::createPixmap(OvrObjEditorContainer* a_obj, QPixmap& a_pixmap)
{
	assert(a_obj != nullptr);

	QSizeF sz = a_obj->size();
	a_obj->setPos(0, 0);

	QGraphicsScene scene;
	scene.addItem(a_obj);

	float scale_value = obj_manager->getViewScale();
	QPixmap pixmap(sz.width() * scale_value, sz.height() * scale_value);
	pixmap.fill(Qt::transparent);

	QPainter painter(&pixmap);
	painter.setRenderHint(QPainter::Antialiasing);
	scene.render(&painter);

	a_pixmap = pixmap;

	scene.removeItem(a_obj);
	return true;
}

void OvrObjEditorScene::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
	QVariant item_type = event->mimeData()->property("item_type");
	if (item_type.isValid())
	{
		event->accept();
	}
	else
	{
		QGraphicsScene::dragEnterEvent(event);
	}
}
void OvrObjEditorScene::dragMoveEvent(QGraphicsSceneDragDropEvent *event)
{
	if (drag_obj != nullptr)
	{
		event->accept();
	}
	else
	{
		QGraphicsScene::dragMoveEvent(event);
	}
}

void OvrObjEditorScene::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{ 
	QGraphicsScene::dragLeaveEvent(event); 
}

void OvrObjEditorScene::dropEvent(QGraphicsSceneDragDropEvent *event)
{
	event->accept();

	assert(drag_obj != nullptr);

	QSizeF sz = drag_obj->size();

	QPointF pt = event->scenePos();
	pt.setX(pt.x() - sz.width() / 2);
	pt.setY(pt.y() - sz.height() / 2);
	drag_obj->setPos(pt);

	addItem(drag_obj);

	clearSelection();
	drag_obj->setSelected(true);

	drag_obj = nullptr;

	update();
}