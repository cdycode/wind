#include "Header.h"
#include "ovr_obj_editor_container.h"

#include "ovr_obj_editor_item.h"

#include <QGraphicsProxyWidget>
#include <QGraphicsGridLayout>
#include <QGraphicsLinearLayout>
#include <QStyleOptionGraphicsItem>

#define kXRadius 15
#define kYRadius 15
#define kContainerBgnColor "#EE333333"
#define kContainerBorderColor "#5c5c5c"

OvrObjEditorContainer::OvrObjEditorContainer(QGraphicsItem *parent, Qt::WindowFlags wFlags) :QGraphicsWidget(parent, wFlags)
{
	setGeometry(0, 0, 500, 200);
	setMinimumWidth(200);
	
	setFlag(QGraphicsItem::ItemIsSelectable);
	setFlag(QGraphicsItem::ItemIsMovable);
	setFlag(QGraphicsItem::ItemSendsGeometryChanges);
	setFlag(QGraphicsItem::ItemUsesExtendedStyleOption);

	grid_layout = new QGraphicsGridLayout();
	grid_layout->setColumnSpacing(kFPIn, 0);
	grid_layout->setColumnSpacing(kFPOutTitle, 0);
	grid_layout->setColumnSpacing(kFPInSubTitle0, 0);
	grid_layout->setColumnSpacing(kFPInSubTitle1, 0);
	grid_layout->setColumnSpacing(kFPInSubTitle2, 0);
	grid_layout->setColumnSpacing(kFPInSubTitle3, 0);

	setLayout(grid_layout);
}


OvrObjEditorContainer::~OvrObjEditorContainer()
{
	qDeleteAll(fields);
}

void OvrObjEditorContainer::init(ovr_core::IOvrVisualNode* a_node_obj)
{
	assert(a_node_obj != nullptr);

	node_obj = a_node_obj;

	initContext();
	adjustSize();
}

const QString& OvrObjEditorContainer::getObjectID()
{
	if (container_id.isEmpty() && node_obj != nullptr)
	{
		container_id = node_obj->GetUniqueName().GetStringConst();
	}
	return container_id;
}

OvrInterfaceItemEx* OvrObjEditorContainer::getConnPoint(const OvrString& a_field_name)
{
	QString field_name = QString::fromLocal8Bit(a_field_name.GetStringConst());

	OvrObjEditorField* field = fields.contains(field_name) ? field = fields[field_name] : nullptr;
	if (field != nullptr)
	{
		return field->getConnPoint();
	}

	return nullptr;
}

QString OvrObjEditorContainer::getNodeObjTitle()
{
	QStringList l = QString::fromLocal8Bit(node_obj->GetNodeCategory().GetStringConst()).split("/");
	assert(l.size() == 2);
	return l.at(1);
}

void OvrObjEditorContainer::initContext()
{
	//添加对象标题
	//QString obj_tile = QString::fromLocal8Bit(node_obj->GetDisplayName().GetStringConst());
	QString obj_tile = getNodeObjTitle();
	addTitle(obj_tile);

	//添加连接字段
	const std::list<ovr_core::OvrNodeInterface>& fields = node_obj->GetInterface();
	if (fields.size() > 0)
	{
		//添加分隔线
		if (!obj_tile.isEmpty())
		{
			addSpace();
			in_row_pos = out_row_pos = grid_layout->rowCount();
		}
		for (auto conn : fields)
		{
			addField(new OvrObjEditorField(&conn));
		}
	}

	checkInParams();
	//添加参数
	auto in_params = node_obj->GetInParamList();
	auto out_params = node_obj->GetOutParamList();
	if (in_params.size() > 0 || out_params.size() > 0)
	{
		//添加分隔线
		if (fields.size() > 0 || (!obj_tile.isEmpty() && fields.size() <= 0))
		{
			addSpace();
			in_row_pos = out_row_pos = grid_layout->rowCount();
		}
		for (auto param : in_params)
		{
			addField(new OvrObjEditorField(&param, true));
		}
		for (auto param : out_params)
		{
			addField(new OvrObjEditorField(&param, false));
		}
	}
}

void OvrObjEditorContainer::checkInParams()
{
	auto in_params = node_obj->GetInParamList();
	for (auto param : in_params)
	{
		if (param.type == ovr_core::OvrNodeParam::kParamVector4)
		{
			has_vector4 = true;
			break;
		}
	}
}

void OvrObjEditorContainer::addTitle(const QString& a_text)
{
	assert(grid_layout->rowCount() == 0);
	assert(line_layout == nullptr);

	if (!a_text.isEmpty())
	{
		line_layout = new QGraphicsLinearLayout(Qt::Horizontal);
		OvrTextItemEx* text_item = new OvrTextItemEx(a_text, OvrTextItemEx::kTSBig);
		line_layout->addItem(text_item);
		grid_layout->addItem(line_layout, 0, kFPIn, 1, kFPCount, Qt::AlignCenter);
	}
}

void OvrObjEditorContainer::addField(OvrObjEditorField* a_field)
{
	a_field->setNodeObject(node_obj);

	a_field->init();

	int row_pos = a_field->isIn() ? in_row_pos : out_row_pos;

	//连接点
	FieldPos field_pos = a_field->isIn() ? kFPIn : kFPOut;
	grid_layout->addItem(a_field->getConnPoint(), row_pos, field_pos, Qt::AlignCenter);
	connect(this, &OvrObjEditorContainer::onContainerPosChanged, a_field->getConnPoint(), &OvrInterfaceItemEx::updateConnectedLine, Qt::QueuedConnection);

	if (a_field->getFieldContexts().size() == 1)
	{
		auto item = a_field->getFieldContexts().first();
		addMidTitle(row_pos, item->title1, a_field->isIn());
		assert(item->title2.isEmpty() || item->check_box == nullptr);
		AddSmallTitle(row_pos, item->title2);
		addCheck(row_pos, item->check_box);
		addInput(row_pos, item->input_edit);
		addLabel(row_pos, item->color_label);
		addButton(row_pos, item->button);
	}
	else
	{
		assert(a_field->getFieldContexts().size() == 3 || a_field->getFieldContexts().size() == 4);

		addMidTitle(row_pos, a_field->getFieldContexts().first()->title1, a_field->isIn());

		for (int i = 0; i < a_field->getFieldContexts().size(); ++i)
		{
			auto item = a_field->getFieldContexts().at(i);
			addInput(row_pos, i, item->title2, item->input_edit);
		}
	}
	++row_pos;

	a_field->isIn() ? in_row_pos = row_pos : out_row_pos = row_pos;

	a_field->setContainer(this);
	fields[a_field->getFieldName()] = a_field;	
}

void OvrObjEditorContainer::reload()
{
	emit reloadObj(this);
}

void OvrObjEditorContainer::addMidTitle(int a_row, const QString& a_text, bool is_in)
{
	if (!a_text.isEmpty())
	{
		int col_pos = is_in ? kFPInTitle : kFPOutTitle;
		grid_layout->addItem(new OvrTextItemEx(is_in ? a_text : (" " + a_text), OvrTextItemEx::kTSMid), a_row, col_pos, Qt::AlignLeft | Qt::AlignBottom);
	}
}

void OvrObjEditorContainer::AddSmallTitle(int a_row, const QString& a_text)
{
	if (!a_text.isEmpty())
	{
		grid_layout->addItem(new OvrTextItemEx(a_text, OvrTextItemEx::kTSSmall), a_row, kFPInSubTitle0, Qt::AlignLeft | Qt::AlignBottom);
	}
}

void OvrObjEditorContainer::addInput(int a_row, QLineEdit* a_input)
{
	if (a_input != nullptr)
	{
		QGraphicsProxyWidget* proxy_widget = new QGraphicsProxyWidget;
		proxy_widget->setWidget(a_input);
		int col_span = has_vector4 ? 7 : 5;
		grid_layout->addItem(proxy_widget, a_row, kFPValue0, 1, col_span, Qt::AlignLeft);
	}
}

void OvrObjEditorContainer::addLabel(int a_row, QLabel* a_input)
{
	if (a_input != nullptr)
	{
		QGraphicsProxyWidget* proxy_widget = new QGraphicsProxyWidget;
		proxy_widget->setWidget(a_input);
		int col_span = has_vector4 ? 7 : 5;
		grid_layout->addItem(proxy_widget, a_row, kFPValue0, 1, col_span, Qt::AlignLeft);
	}
}

void OvrObjEditorContainer::addInput(int a_row, int a_pos, const QString& a_text, QLineEdit* a_input)
{
	assert(a_pos >= 0 && a_pos <= 3);

	if (!a_text.isEmpty())
	{
		FieldPos text_pos = (FieldPos)(kFPInSubTitle0 + a_pos * 2);
		grid_layout->addItem(new OvrTextItemEx(a_text, OvrTextItemEx::kTSSmall), a_row, text_pos, Qt::AlignLeft | Qt::AlignBottom);
	}
	if (a_input != nullptr)
	{
		a_input->setFixedWidth(50);
		FieldPos input_pos = (FieldPos)(kFPValue0 + a_pos * 2);
		QGraphicsProxyWidget* proxy_widget = new QGraphicsProxyWidget;
		proxy_widget->setWidget(a_input);
		grid_layout->addItem(proxy_widget, a_row, input_pos, Qt::AlignLeft);
	}
}

void OvrObjEditorContainer::addButton(int a_row, QPushButton* a_button)
{
	if (a_button != nullptr)
	{
		a_button->setFixedSize(24, 19);
		QGraphicsProxyWidget* proxy_widget = new QGraphicsProxyWidget;
		proxy_widget->setWidget(a_button);
		grid_layout->addItem(proxy_widget, a_row, kFPButton, Qt::AlignRight | Qt::AlignTop);
		grid_layout->setColumnMinimumWidth(kFPButton, 27);
	}
}

void OvrObjEditorContainer::addCheck(int a_row, QCheckBox* a_button)
{
	if (a_button != nullptr)
	{
		a_button->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
		QGraphicsProxyWidget* proxy_widget = new QGraphicsProxyWidget;
		proxy_widget->setWidget(a_button);
		grid_layout->addItem(proxy_widget, a_row, kFPInSubTitle0, Qt::AlignLeft | Qt::AlignVCenter);
	}
}

//分隔线
void OvrObjEditorContainer::addSpace()
{
	grid_layout->addItem(new OvrLineItemEx, grid_layout->rowCount(), kFPIn, 1, kFPCount, Qt::AlignCenter);
}

void OvrObjEditorContainer::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	painter->setPen(QPen(kContainerBorderColor));
	painter->setBrush(QBrush(QColor(kContainerBgnColor)));
	painter->drawRoundedRect(boundingRect(), kXRadius, kYRadius);

	if (option->state & QStyle::State_Selected)
	{
		ovr_graphicsItem_highlightSelected(this, painter, option, outlineShape());
	}
}

QVariant OvrObjEditorContainer::itemChange(GraphicsItemChange change, const QVariant &value)
{
	//container位置改变
	if (change == QGraphicsItem::ItemPositionHasChanged)
	{
		emit onContainerPosChanged();
		node_obj->SetPosition(scenePos().rx(), scenePos().ry());
	}
	return value;
}
QPainterPath OvrObjEditorContainer::shape() const
{
	QPainterPath path;
	path.addRoundedRect(boundingRect(), kXRadius, kYRadius);
	return path;
}

QPainterPath OvrObjEditorContainer::outlineShape() const
{
	QPainterPath path;
	qreal v = 0.5;
	path.addRoundedRect(boundingRect().adjusted(v, v, -v, -v), kXRadius, kYRadius);
	return path;
}