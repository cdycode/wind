#include "Header.h"

#include "ovr_obj_editor_Field.h"
#include "ovr_obj_editor_Field_context.h"
#include "ovr_obj_editor_container.h"

#include "../dialog/ovr_jump_clip_dlg.h"
#include "../dialog/ovr_file_dialog.h"

#include <QColorDialog>

//////////////////////////////////////////////////
// class OvrObjEditorField

OvrObjEditorField::OvrObjEditorField(ovr_core::OvrNodeInterface* a_field_conn)
{
	is_in = a_field_conn->is_in;
	field_type = 0;
	field_title = QString::fromLocal8Bit(a_field_conn->name.GetStringConst());
	field_name = QString::fromLocal8Bit(a_field_conn->unique_name.GetStringConst());
}

OvrObjEditorField::OvrObjEditorField(const ovr_core::OvrNodeParam* a_field_param, bool a_is_in)
{
	is_in = a_is_in;
	field_type = a_field_param->type;
	field_title = QString::fromLocal8Bit(a_field_param->name.GetStringConst());
	field_name = QString::fromLocal8Bit(a_field_param->unique_name.GetStringConst());
	field_option = QString::fromLocal8Bit(a_field_param->type_option.GetStringConst());

	//默认认为显示的值就是实际值
	if (a_field_param->value_display.GetStringSize() > 0)
	{
		field_value = QString::fromLocal8Bit(a_field_param->value_display.GetStringConst());
	}
	else
	{
		field_value = QString::fromLocal8Bit(a_field_param->value.GetStringConst());
	}
}

OvrObjEditorField::~OvrObjEditorField()
{
	qDeleteAll(field_contexts);
}

void OvrObjEditorField::init()
{
	assert(conn_point == nullptr && field_contexts.size() == 0);

	initConnPoint();

	OvrObjEditorFieldContext* context = new OvrObjEditorFieldContext;
	field_contexts.push_back(context);

	context->title1 = field_title;

	if (isSimpleInput())
	{
		addInput(context, field_value);
	}
	else if (field_type == ovr_core::OvrNodeParam::kParamBool)
	{
		addCheck(context, field_value);
	}
	else if (field_type == ovr_core::OvrNodeParam::kParamVector3 || field_type == ovr_core::OvrNodeParam::kParamVector4)
	{
		int count = field_type == ovr_core::OvrNodeParam::kParamVector3 ? 3 : 4;
		addInput(context, count);
	}
	else if (field_type == ovr_core::OvrNodeParam::kParamActor)
	{
		if (is_in)
		{
			addInput(context, field_value)->setReadOnly(true);
		}
	}
	else if (field_type == ovr_core::OvrNodeParam::kParamResource)
	{
		addInput(context, field_value);
		addResButton(context);
	}
	else if (field_type == ovr_core::OvrNodeParam::kParamSequence)
	{
		context->setJumpInput();
		addInput(context, field_value);
		addMarkButton(context);
	}
	else if (field_type == ovr_core::OvrNodeParam::kParamColor)
	{
		addColor(context, field_value);
	}
	else if (field_type != ovr_core::OvrNodeParam::kParamInvalid)
	{
		assert(false);
	}
}

void OvrObjEditorField::addCheck(OvrObjEditorFieldContext* context, const QString& a_value)
{
	assert(is_in);

	context->check_box = new QCheckBox;
	context->setValue(a_value);

	QObject::connect(context->check_box, &QCheckBox::stateChanged, [this, context](int a_state) {
		QString new_value = a_state == Qt::Unchecked ? "0" : "1";
		context->setValue(new_value, false);
		notify();
	});
}

void OvrObjEditorField::addInput(OvrObjEditorFieldContext* context, int a_count)
{
	assert(is_in);

	static const char* sub_title[] = { "x","y","z","w" };
	QStringList value_list = field_value.split(":");

	QString value;
	for (int i = 0; i < a_count; ++i)
	{
		if (i != 0)
		{
			context = new OvrObjEditorFieldContext;
			field_contexts.push_back(context);
		}
		context->title2 = sub_title[i];
		value = value_list.size() > i ? value_list.at(i) : "";
		addInput(context, value);
	}
}

QLineEdit* OvrObjEditorField::addInput(OvrObjEditorFieldContext* context, const QString& a_value)
{
	assert(is_in);

	context->input_edit = new QLineEdit;
	context->setValue(a_value);

	QObject::connect(context->input_edit, &QLineEdit::editingFinished, [this, context]() {
		if (context->input_edit->text() != context->getValue())
		{
			context->setValue(context->input_edit->text(), false);
			notify();
		}
	});

	return context->input_edit;
}

QLabel* OvrObjEditorField::addColor(OvrObjEditorFieldContext* context, const QString& a_value)
{
	QColorLabel* color_label = new QColorLabel;
	qDebug() << color_label->height();
	color_label->setFixedHeight(20);
	color_label->setStyleSheet("border:1px solid #AAAAAA;");
	context->color_label = color_label;
	context->setValue(a_value);

	QObject::connect(color_label, &QColorLabel::onSelectColor, [this, context]() {
		QColorDialog dlg;
		dlg.setWindowTitle(QString::fromLocal8Bit("选择颜色"));
		auto colors = context->getColorF();
		QColor color;
		color.setRgbF(colors[0], colors[1], colors[2], colors[3]);
		dlg.setCurrentColor(color);
		if (dlg.exec() == QDialog::Accepted)
		{
			QColor color_new = dlg.currentColor();
			if (color_new != color)
			{
				QString value_new = QString::number(color_new.redF()) + ":"
					+ QString::number(color_new.greenF()) + ":"
					+ QString::number(color_new.blueF()) + ":"
					+ QString::number(color_new.alphaF());
				context->setValue(value_new);
				notify();
			}
		}
	});

	return color_label;
}

void OvrObjEditorField::addResButton(OvrObjEditorFieldContext* context)
{
	assert(is_in);

	context->button = new QPushButton("...");
	
	QObject::connect(context->button, &QPushButton::clicked, [this, context](bool checked) {
		QStringList strs = field_option.split(":");
		assert(strs.size() == 2);

		OvrFileDialog dlg;
		if (dlg.init(strs.at(0), strs.at(1), context->getValue()) && dlg.exec() == 1)
		{
			if (dlg.getSelectFile() != context->getValue())
			{
				context->setValue(dlg.getSelectFile(), true);
				notify();
			}
		}
	});
}

void OvrObjEditorField::addMarkButton(OvrObjEditorFieldContext* context)
{
	assert(is_in);

	context->button = new QPushButton("...");

	QObject::connect(context->button, &QPushButton::clicked, [this, context](bool checked) {
		QStringList values = context->getValue().split(":");
		QString seq_id = (values.size() > 0 || !values.at(0).isEmpty()) ? values.at(0) : "";
		int mark_key = (values.size() > 1 && !values.at(1).isEmpty()) ? values[1].toInt() : -1;

		OvrJumpClipDlg dlg(seq_id, mark_key);
		dlg.setModal(true);
		if (dlg.exec() == 1)
		{
			QString new_value = dlg.sequence_id + ":" + (dlg.mark_key < 0 ? "" : QString::number(dlg.mark_key));
			if (new_value != context->getValue())
			{
				context->setValue(new_value);
				notify();
			}
		}
	});
}

void OvrObjEditorField::initConnPoint()
{
	OvrInterfaceItemEx::InterfaceShape t = field_type != ovr_core::OvrNodeParam::kParamInvalid ?
		OvrInterfaceItemEx::kShapePoint : OvrInterfaceItemEx::kShapeTri;

	conn_point = new OvrInterfaceItemEx(t, this);
	conn_point->setColor(getInterfaceColor());
}

QString OvrObjEditorField::getInterfaceColor()
{
	QString color;
	switch (field_type)
	{
	case ovr_core::OvrNodeParam::kParamInt:
		color = "#9341C0";
		break;
	case ovr_core::OvrNodeParam::kParamFloat:
		color = "#9D0100";
		break;
	case ovr_core::OvrNodeParam::kParamBool:
		color = "#DE8930";
		break;
	case ovr_core::OvrNodeParam::kParamString:
		color = "#005EDB";
		break;
	case ovr_core::OvrNodeParam::kParamVector3:
		color = "#F7D946";
		break;
	case ovr_core::OvrNodeParam::kParamVector4:
		color = "#83A751";
		break;
	case ovr_core::OvrNodeParam::kParamResource:
		color = "#009EFF";
		break;
	case ovr_core::OvrNodeParam::kParamActor:
		color = "#00726E";
		break;
	case ovr_core::OvrNodeParam::kParamSequence:
		color = "#7463FF";
		break;
	case ovr_core::OvrNodeParam::kParamColor:
		color = "#D06B7D";
		break;
	case ovr_core::OvrNodeParam::kParamInvalid:
	default:
		color = "#AAAAAA";
		break;
	}
	return color;
}

bool OvrObjEditorField::isSimpleInput()
{
	return field_type == ovr_core::OvrNodeParam::kParamInt
		|| field_type == ovr_core::OvrNodeParam::kParamFloat
		|| field_type == ovr_core::OvrNodeParam::kParamString;
}

void OvrObjEditorField::notify()
{
	QString value;
	for (int i = 0; i < field_contexts.size(); ++i)
	{
		if (i > 0)
		{
			value += ":";
		}
		value += field_contexts.at(i)->getValue();
	}
	qDebug() << field_name << " new value: " << value;


	assert(node_obj != nullptr);

	int ret = node_obj->SetParamValue(is_in, field_name.toLocal8Bit().constData(), value.toLocal8Bit().constData());
	if (ret > 0)
	{
		field_value = value;
		if (ret == 2)
		{
			//need reload node obj
			container->reload();
		}
	}
	else
	{
		QStringList value_list = field_value.split(":");
		for (int i = 0; i < field_contexts.size(); ++i)
		{
			value = value_list.size() > i ? value_list.at(i) : "";
			field_contexts[i]->setValue(value);
		}
	}
}

//OvrInterfaceHelperEx
bool OvrObjEditorField::canBeConnected(OvrInterfaceItemEx* a_interface)
{
	assert(a_interface != nullptr && a_interface->getHelper() != nullptr);

	OvrObjEditorField* other_field = static_cast<OvrObjEditorField*>(a_interface->getHelper());

	bool is_ok = node_obj != other_field->node_obj
		&& is_in != other_field->is_in
		&& field_type == other_field->field_type
		&& conn_point->isNotConnected(a_interface);
		//&& canCeateNewConnection()
		//&& other_field->canCeateNewConnection();

	return is_ok;
}

bool OvrObjEditorField::canCeateNewConnection()
{
	bool result = false;

	//操作
	//输入操作可以被多个对象连接
	//输出操作只能接一个对象
	if (field_type == 0)
	{
		result = is_in ? true : conn_point->isNotConnected();
	}
	//参数
	//参数只接受一个输入
	//参数可以有多个输出
	else
	{
		result = is_in ? conn_point->isNotConnected() : true;
	}
	return result;
}

void OvrObjEditorField::buildConnection(OvrInterfaceItemEx* a_interface)
{
	OvrObjEditorField* other = static_cast<OvrObjEditorField*>(a_interface->getHelper());

	if (is_in)
	{
		other->node_obj->AddOutConnect(other->field_name.toLocal8Bit().constData(), node_obj, field_name.toLocal8Bit().constData());
	}
	else
	{
		node_obj->AddOutConnect(field_name.toLocal8Bit().constData(), other->node_obj, other->field_name.toLocal8Bit().constData());
	}
}

bool OvrObjEditorField::disConnectOut(OvrInterfaceItemEx* a_interface)
{
	if (!is_in && conn_point != nullptr)
	{
		node_obj->RmvOutConnect(field_name.toLocal8Bit().constData());
		return true;
	}
	return false;
}