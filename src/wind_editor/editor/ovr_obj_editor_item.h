#pragma once

#include <QGraphicsItem>
#include <QGraphicsTextItem>
#include <QGraphicsLineItem>
#include <QGraphicsPathItem>
#include <QGraphicsLayoutItem>

//////////////////////////////////////////////////
// class OvrTextItem

class OvrTextItemEx :public QGraphicsTextItem, public QGraphicsLayoutItem
{
public:
	enum TextSize { kTSSmall = 10, kTSMid, kTSBig };
	explicit OvrTextItemEx(const QString &a_text, TextSize a_font_size, QGraphicsItem *a_parent = nullptr);

	//QGraphicsLayoutItem
	void setGeometry(const QRectF &geom) override;
	QSizeF sizeHint(Qt::SizeHint which, const QSizeF &constraint = QSizeF()) const override;
};

//////////////////////////////////////////////////
// class OvrLineItemEx

class OvrLineItemEx : public QGraphicsLineItem, public QGraphicsLayoutItem
{
public:
	explicit OvrLineItemEx();

	//QGraphicsLayoutItem
	void setGeometry(const QRectF &geom) override;
	QSizeF sizeHint(Qt::SizeHint which, const QSizeF &constraint = QSizeF()) const override;

	//QGraphicsItem
	QRectF boundingRect() const override;

protected:
	QPainterPath shape() const override;

	const int line_v_space = 1;
};

enum OvrEditItemType { kOvrBegin, kOvrInterface, kOvrConnLine };

//////////////////////////////////////////////////
// class OvrInterfaceItemEx

class OvrInterfaceItemEx;
class OvrInterfaceHelperEx
{
public:
	virtual const QString& getFieldName() = 0;
	virtual bool canBeConnected(OvrInterfaceItemEx* a_interface) = 0;
	virtual bool canCeateNewConnection() = 0;
	virtual void buildConnection(OvrInterfaceItemEx* a_interface) = 0;
	virtual bool disConnectOut(OvrInterfaceItemEx* a_interface) = 0;
	virtual bool isIn() = 0;
};

//////////////////////////////////////////////////
// class OvrInterfaceItemEx

class OvrConnLineItemEx;
class OvrInterfaceItemEx : public QGraphicsObject, public QGraphicsLayoutItem
{
	Q_OBJECT
	Q_INTERFACES(QGraphicsLayoutItem)

public:
	enum InterfaceShape { kShapePoint, kShapeTri };
	explicit OvrInterfaceItemEx(InterfaceShape a_shape, OvrInterfaceHelperEx* a_helper);

	void setColor(const QString& a_color);
	QString getColor() { return bgn_color.name(); }

	bool canBeConnected(OvrInterfaceItemEx* a_interface);
	void buildConnection(OvrInterfaceItemEx* a_interface);
	bool disConnectOut();
	bool isConnected();
	bool isNotConnected();
	bool isNotConnected(OvrInterfaceItemEx* a_interface);

	void addConnLine(OvrConnLineItemEx* a_line);
	void removeConnLine(OvrConnLineItemEx* a_line);

	//QGraphicsLayoutItem
	void setGeometry(const QRectF &geom) override;
	QSizeF sizeHint(Qt::SizeHint which, const QSizeF &constraint = QSizeF()) const override;

	//QGraphicsItem
	enum {
		Type = UserType + kOvrInterface
	};
	int type() const override { return Type; }

	QRectF boundingRect() const override;
	virtual void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;
	virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) override;

	void setHelper(OvrInterfaceHelperEx* a_helper) { helper = a_helper; }
	OvrInterfaceHelperEx* getHelper() { return helper; }
	const QList<OvrConnLineItemEx*>& getConnLines() { return conn_lines; }

public slots:
	void updateConnectedLine();
	void on_hint_for_connection(OvrInterfaceItemEx* a_point);
	void on_remove_hint_for_connection();

protected:
	//QGraphicsItem
	QPainterPath shape() const override;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0) override;
	QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

	void calcCustomeBrush(QPainter *painter);
	bool canBeConnected(QGraphicsItem* a_interface);
	bool isLineSelected();

	InterfaceShape conn_shape;
	const qreal i_w_h = 13;

	bool is_hint = false;
	QList<OvrConnLineItemEx*> conn_lines;
	QColor bgn_color;
	bool is_hover_in = false;

	OvrInterfaceHelperEx* helper = nullptr;
};

//////////////////////////////////////////////////
// class OvrConnLineItem

class OvrConnLineItemEx : public QGraphicsPathItem
{
public:
	explicit OvrConnLineItemEx(const QString& a_color, QGraphicsItem *parent = Q_NULLPTR);
	~OvrConnLineItemEx();

	enum {
		Type = UserType + kOvrConnLine
	};
	int type() const override { return Type; }

	void dragEndTo(OvrInterfaceItemEx* a_from, const QPointF& a_end);

	void updatePos();

	void setConnFromTo(OvrInterfaceItemEx* a_from, OvrInterfaceItemEx* a_to);
	void removeConn();
	bool disConnect();
	bool isConnected();
	void removeInterface(OvrInterfaceItemEx* a_point);
	bool hasInterface(OvrInterfaceItemEx* a_point);

	OvrInterfaceItemEx* getFromInterface() { return interface_from; }
	OvrInterfaceItemEx* getToInterface() { return interface_to; }

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0) override;
	QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

protected:
	OvrInterfaceItemEx* interface_from = nullptr;
	OvrInterfaceItemEx* interface_to = nullptr;
};

void ovr_graphicsItem_highlightSelected(QGraphicsItem *item, QPainter *painter, 
	const QStyleOptionGraphicsItem *option, const QPainterPath& path);