#pragma once

namespace ovr_core
{
	class OvrEventTrack;
	class OvrInterActiveTrack;
	class IOvrNodeCluster;
	class IOvrVisualNode;
	class OvrNodeInfo;
}

class OvrObjEditorClusterManager
{
public:
	const QString& getActorClassID();
	bool getNodeClasses(std::list<ovr_core::OvrNodeInfo>& a_node_infos);

	virtual bool loadCluster(const char* a_area_name) = 0;
	virtual void saveCluster() = 0;
	virtual void closeCluster() = 0;

	const QString& getEditorName();
	bool isOK() { return cluster != nullptr; }

	ovr_core::IOvrVisualNode* CreateNode(const QString& a_node_class);
	void DestoryNode(ovr_core::IOvrVisualNode* a_node);
	bool GetAllNodeMap(std::map<OvrString, ovr_core::IOvrVisualNode*>& a_map);

	void setViewOffset(float x, float y);
	void setViewScale(float a_scale);

	float getViewX();
	float getViewY();
	float getViewScale();

protected:
	QString actor_class_id;

	QString wnd_title;
	QString area_name;
	QString editor_name;

	ovr_core::IOvrNodeCluster* cluster = nullptr;
	ovr_core::IOvrNodeCluster::Type obj_type = ovr_core::IOvrNodeCluster::kInvalid;
};


OvrObjEditorClusterManager* CreateObjClusterManager(ovr_core::OvrEventTrack* track);
OvrObjEditorClusterManager* CreateObjClusterManager(ovr_core::OvrInterActiveTrack* track);