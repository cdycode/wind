#include "Header.h"
#include "ovr_obj_editor_Field_context.h"

void OvrObjEditorFieldContext::setValue(const QString& a_value, bool a_update_control)
{
	value = a_value;

	if (a_update_control)
	{
		updateControl();
	}
}

void OvrObjEditorFieldContext::updateControl()
{
	if (input_edit != nullptr)
	{
		if (is_jump)
		{
			updateJumpValue();
		}
		else
		{
			input_edit->setText(value);
		}
	}
	else if (check_box != nullptr)
	{
		assert(value.isEmpty() || value == "0" || value == "1");

		Qt::CheckState state = value.isEmpty() || value == "0" ? Qt::Unchecked : Qt::Checked;
		check_box->setCheckState(state);

		if (value.isEmpty())
		{
			value = "0";
		}
	}

	else if (color_label != nullptr)
	{
		if (value.isEmpty())
		{
			value = "1.0:1.0:1.0:1.0";
		}

		QColor c;
		{
			QStringList colors = value.split(":");
			colors_f.clear();
			for (int i = 0; i < 4; ++i)
			{
				colors_f << (colors.size() > i && !colors[i].isEmpty() ? colors[i].toFloat() : 1.0);
			}
			c.setRgbF(colors_f[0], colors_f[1], colors_f[2], colors_f[3]);
		}
		QPalette palette = color_label->palette();
		palette.setColor(QPalette::Background, c);
		color_label->setPalette(palette);
	}
}

void OvrObjEditorFieldContext::updateJumpValue()
{
	QStringList tmp = value.split(":");
	QString seq_id = tmp.size() > 0 ? tmp[0] : "";
	QString mark_key = tmp.size() > 1 ? tmp[1] : "";

	QString seq_name = getSeqName();
	QString makr_name = getMarkName();

	QString text = seq_name.isEmpty() ? "" : seq_name + ":";
	text += makr_name;

	input_edit->setText(text);
}

QString OvrObjEditorFieldContext::getSeqName()
{
	QStringList tmp = value.split(":");
	QString target = tmp.size() > 0 ? tmp[0] : "";
	if (target.isEmpty())
	{
		return "";
	}

	OvrString tmp_seq_id = target.toLocal8Bit().constData();

	const std::list<OvrString>& id_list = ovr_project::OvrProject::GetIns()->GetCurrentScene()->GetSequenceNameList();
	ovr_project::OvrAssetManager* asset_manager = ovr_project::OvrProject::GetIns()->GetAssetManager();

	for (auto seq_id : id_list)
	{
		if (seq_id == tmp_seq_id)
		{
			OvrString asset_name, asset_path;
			bool ret = asset_manager->GetSequenceFilePath(seq_id, asset_path);
			assert(ret);

			ret = asset_manager->GetAssetDisplayName(asset_path, asset_name);
			assert(ret);

			return QString::fromLocal8Bit(asset_name.GetStringConst());
		}
	}

	return "";
}

QString OvrObjEditorFieldContext::getMarkName()
{
	QStringList tmp = value.split(":");
	QString target = tmp.size() > 1 ? tmp[1] : "";
	if (target.isEmpty())
	{
		return "";
	}

	int mark_key = target.toInt();

	ovr_core::OvrSequence* current_seq = ovr_project::OvrProject::GetIns()->GetCurrentSequence();
	QListWidgetItem* item_selected = nullptr;
	ovr_core::OvrMarkTrack* mark_track = current_seq->GetMarkInfo();
	for (auto item : mark_track->GetKeyFrames())
	{
		if (mark_key == item.first)
		{
			return QString::fromLocal8Bit(item.second.GetStringConst());
		}
	}
	return "";
}