#pragma once

class OvrObjEditorFieldContext
{
public:
	QString title1;
	QString title2;
	QLineEdit* input_edit = nullptr;
	QLabel* color_label = nullptr;
	QCheckBox* check_box = nullptr;
	QPushButton* button = nullptr;

	void setJumpInput() { is_jump = true; }

	void setValue(const QString& a_value, bool a_update_control = true);
	const QString& getValue() { return value; }

	const QList<float>& getColorF() { return colors_f; }

protected:
	void updateControl();
	void updateJumpValue();
	QString getSeqName();
	QString getMarkName();

	bool is_jump = false;

	QString value;
	QList<float> colors_f;
};