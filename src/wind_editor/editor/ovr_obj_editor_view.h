#pragma once

#include <QGraphicsView>

class OvrObjEditorAbrevView;

class OvrObjEditorView :public QGraphicsView
{
	Q_OBJECT
public:
	OvrObjEditorView(QWidget *parent = Q_NULLPTR);

	~OvrObjEditorView();

	void restoreOffset(float a_x, float a_y);
	void restoreScale(float a_value);

signals:
	void onViewOffsetChanged(float a_x, float a_y);
	void onViewScaleChanged(float a_scale);

public slots:
	void onDrag(QPointF a_pt);

protected:
	void resizeEvent(QResizeEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;
	void mousePressEvent(QMouseEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *event) override;
	void wheelEvent(QWheelEvent *event) override;

	void updateViewForScene();

	QPoint pt_drag_scene;
	QPointF pos_drag_scene;
	bool is_begin_drag_scene = false;
};

class OvrObjEditorAbrevView : public QGraphicsView
{
	Q_OBJECT
public:
	OvrObjEditorAbrevView(QWidget *parent = Q_NULLPTR);

	void delayUpdate(int a_interval = 300);

signals:
	void onDrag(QPointF a_pt);

protected:
	void mousePressEvent(QMouseEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *event) override;
	void timerEvent(QTimerEvent *event) override;

	bool is_drag = false;
	int timer_id = 0;
};