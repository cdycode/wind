#include "Header.h"

#include <wx_project/ovr_scene_editor.h>
#include "ovr_obj_editor_wnd.h"

#include "ovr_obj_editor_scene.h"
#include "ovr_obj_editor_view.h"

#include "ovr_obj_editor_cluster_manager.h"
#include "../utils_ui/main_frame_title_bar.h"

#define kEditorItemType (Qt::UserRole + 1)
#define kEditorItemValue (Qt::UserRole + 2)

static OvrObjEditorWnd* g_editor_inst = nullptr;

OvrObjEditorWnd* OvrObjEditorWnd::createInst()
{
	QRect rc;
	if (g_editor_inst != nullptr)
	{
		rc = g_editor_inst->geometry();
		g_editor_inst->close();
	}

	g_editor_inst = new OvrObjEditorWnd;
	if (!rc.isEmpty())
	{
		g_editor_inst->setGeometry(rc);
	}

	return g_editor_inst;
}

OvrObjEditorWnd::~OvrObjEditorWnd()
{
}

void OvrObjEditorWnd::closeEvent(QCloseEvent *event)
{
	QMainWindow::closeEvent(event);
	g_editor_inst = nullptr;
}

void OvrObjEditorWnd::init(OvrObjEditorClusterManager* a_obj_manager)
{
	assert(a_obj_manager != nullptr && a_obj_manager->isOK());
	obj_manager = a_obj_manager;

	init_title_bar();
	initLeftDock();
	initRightDock();
	initContext();

	setWindowTitle(obj_manager->getEditorName());

	connect(obj_list, &OvrObjEditorDockTree::onDragItem, this, &OvrObjEditorWnd::onDragItem, Qt::DirectConnection);
	connect(op_list, &OvrObjEditorDockTree::onDragItem, this, &OvrObjEditorWnd::onDragItem, Qt::DirectConnection);
}

void OvrObjEditorWnd::onDragItem(const QString& a_text, const QString& a_type, const QString& a_value)
{
	editor_view->setFocus();

	scene->onDragItem(a_text, a_type, a_value, sender());
}

OvrObjEditorWnd::OvrObjEditorWnd(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	setWindowFlags((windowFlags() | Qt::FramelessWindowHint) & ~Qt::WindowContextHelpButtonHint);
	setAttribute(Qt::WA_DeleteOnClose, true);

	set_custom_style_sheet();
	preset_connects();

	registerEventListener("mainwnd_close");
}


void OvrObjEditorWnd::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{
	assert(a_event_name == "mainwnd_close");
	close();
}

void OvrObjEditorWnd::init_title_bar()
{
	MainFrameTitleBar* dockWidget = new MainFrameTitleBar(QString::fromLocal8Bit("����༭��"), this);
	dockWidget->setObjectName(QStringLiteral("dockWidget_myTop"));

	addDockWidget(Qt::TopDockWidgetArea, dockWidget);
	installEventFilter(dockWidget);
}

void OvrObjEditorWnd::init_custom_ui()
{

}

void OvrObjEditorWnd::set_custom_style_sheet()
{	// 0. main window
	QString strDefaultStyle = QString::fromLocal8Bit(""
		"QMainWindow{background:rgb(24, 24, 24); color:rgb(204, 204, 204); font-size:12px; font-family:\"΢���ź�\"; /*margin: 2px; border:2px;*/ border-radius:4px;}"
		"QMainWindow::separator{color:rgb(204, 204, 204); background-color: rgba(24, 24, 24); width:2px; height:2px;}"

		"QMenu{background:rgb(39,39,39); color:rgb(204,204,204); selection-background-color:rgb(60,60,60); selection-color: rgb(204,204,204);}"
		"QMenuBar{background-color: rgb(39,39,39); /*border-top:1px solid rgb(39,39,39); padding: 3px;*/}"
		"QMenuBar::item {color:rgb(204, 204, 204);}"
		"QMenuBar::item:selected { background-color: rgb(60,60,60);}"
		"QMenu::item:disabled {color:gray}"

		"QDockWidget{border:2px solid rgb(24, 24, 24); color:rgb(204, 204, 204); }"
		"QDockWidget::title{background:rgb(24, 24, 24); color:rgb(204, 204, 204); text-align: center; background: #3C3C3C; padding-left: 5px;}"

		"QSplitter::handle{background:rgb(24, 24, 24);}"  // all default splitter

		"QWidget{font-family:\"΢���ź�\";}"
		"QWidget:focus{outline: none;}"

		"QLineEdit{background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); font-size:12px; selection-background-color: rgb(165, 165, 165); selection-color: rgb(32, 32, 32);}"
		"QPushButton{background-color:qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87));}"
		"QPushButton:hover{background-color:rgb(118, 118, 118);}"
		"QPushButton:pressed{background-color:rgb(87, 87, 87);}"
		"QPushButton:disabled{background-color:rgb(67, 67, 67); color:rgb(140, 140, 140)}"

		"QToolBar{background-color:rgb(39, 39, 39); color:rgb(204, 204, 204);}"
		"QToolBar QToolButton{background:rgb(39, 39, 39); color:rgb(204, 204, 204);}"
		"QToolBar QToolButton:hover{background-color:rgb(83, 83, 83);}"
		"QToolBar QToolButton:checked{background-color:rgb(83, 83, 83);}"
		"QToolBar QLabel{background-color:rgb(39, 39, 39);}"

		//"QMessageBox{background-color:qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); }"
		"QMessageBox{color:rgb(204, 204, 204); background-color:rgb(48, 48, 48); border-radius:2px; messagebox-text-interaction-flags:5; }"
		"QMessageBox QLabel{color:rgb(204, 204, 204); background-color:rgb(48, 48, 48); selection-background-color:rgb(204, 204, 204);selection-color:rgb(48, 48, 48);; messagebox-text-interaction-flags:5 }"
		"QMessageBox QPushButton{width:70px; height:25px; color:rgb(204, 204, 204); background-color:qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); border-radius: 2px;}"
		"QMessageBox QPushButton:hover{background-color:rgb(118, 118, 118);}"
		"QMessageBox QPushButton:pressed{background-color:rgb(87, 87, 87);}"
		"QMessageBox QPushButton:disabled{background-color:rgb(67, 67, 67); color:rgb(140, 140, 140)}"
	
		"QTreeView{background-color:rgb(39, 39, 39); color: rgb(192, 192, 192); border:1px solid rgb(24, 24, 24); border-top:0px;}"
		"QTreeView::item{height:28px; border-top:1px solid rgb(26, 26, 26); color:rgb(192, 192, 192);}"
		"QTreeView::item::hover{background-color:rgb(46, 46, 46);}"
		"QTreeView::item:selected{background-color:rgb(64, 64, 64);}"
		"QTreeView::branch{height:28px; border-top:1px solid rgb(26, 26, 26);}"
		"QTreeView::branch:selected {background-color:rgb(64, 64, 64);}"

		// ��ֱ������
		"QScrollBar:vertical{background-color:rgb(35, 35, 35);width:16px; margin:16px 0 16px 0; border:0px solid rgb(135, 135, 35);}"
		"QScrollBar::handle:vertical{background:rgb(119, 119, 119); border: 2px solid rgb(35, 35, 35); background-clip: border; border-radius:7px; min-height: 20px;}"
		"QScrollBar::handle:vertical:hover{background:rgb(129, 129, 129);}"

		"QScrollBar::sub-line:vertical {background:rgb(35, 35, 35); height: 16px; subcontrol-position: top;subcontrol-origin: margin;}"
		"QScrollBar::sub-line:vertical:hover {background:rgba(135, 135, 135, 50); border-top-left-radius:4px; border-top-right-radius:4px; height: 15px; subcontrol-position: top;subcontrol-origin: margin;}"
		"QScrollBar::sub-line:vertical:pressed {background:rgba(135, 135, 135, 30); border-top-left-radius:4px; border-top-right-radius:4px; height: 15px; subcontrol-position: top;subcontrol-origin: margin;}"

		"QScrollBar::add-line:vertical {background:rgb(35, 35, 35); height: 16px; subcontrol-position: bottom;subcontrol-origin: margin;}"
		"QScrollBar::add-line:vertical:hover {background:rgba(135, 135, 135, 50); border-bottom-left-radius:4px; border-bottom-right-radius:4px; height: 15px; subcontrol-position: bottom;subcontrol-origin: margin;}"
		"QScrollBar::add-line:vertical:pressed {background:rgba(135, 135, 135, 30); border-bottom-left-radius:4px; border-bottom-right-radius:4px; height: 15px; subcontrol-position: bottom;subcontrol-origin: margin;}"

		"QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {background:rgb(35, 35, 35);}"

		// ˮƽ������
		"QScrollBar:horizontal {background-color:rgb(35, 35, 35); height:16px; margin:0 16px 0 16px; border:0px solid rgb(135, 135, 35);}"
		"QScrollBar::handle:horizontal{background:rgb(119, 119, 119); border: 2px solid rgb(35, 35, 35); background-clip: border; border-radius:7px; min-width: 20px;}"
		"QScrollBar::handle:horizontal:hover{background:rgb(129, 129, 129);}"

		"QScrollBar::sub-line:horizontal {background:rgb(35, 35, 35); width:16px; subcontrol-position:left; subcontrol-origin:margin;}"
		"QScrollBar::sub-line:horizontal:hover {background:rgba(135, 135, 135, 50); border-top-left-radius:4px; border-bottom-left-radius:4px; width:15px; subcontrol-position: left;subcontrol-origin: margin;}"
		"QScrollBar::sub-line:horizontal:pressed {background:rgba(135, 135, 135, 30); border-top-left-radius:4px; border-bottom-left-radius:4px; width:15px; subcontrol-position: left;subcontrol-origin: margin;}"

		"QScrollBar::add-line:horizontal {background:rgb(35, 35, 35); width: 16px; subcontrol-position: right;subcontrol-origin: margin;}"
		"QScrollBar::add-line:horizontal:hover {background:rgba(135, 135, 135, 50); border-bottom-right-radius:4px; border-top-right-radius:4px; width: 15px; subcontrol-position: right; subcontrol-origin: margin;}"
		"QScrollBar::add-line:horizontal:pressed {background:rgba(135, 135, 135, 30); border-bottom-right-radius:4px; border-top-right-radius:4px; width: 15px; subcontrol-position: right; subcontrol-origin: margin;}"

		"QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal {background:rgb(35, 35, 35);}"
	);

	QTextStream qtsTemp(&strDefaultStyle);
	qtsTemp << "QScrollBar::up-arrow{image:url(" << getImagePath("icon_scroll_top.png") << ");}"
		<< "QScrollBar::down-arrow{image:url(" << getImagePath("icon_scroll_bottom.png") << ");}"
		<< "QScrollBar::left-arrow{image:url(" << getImagePath("icon_scroll_left.png") << "); }"
		<< "QScrollBar::right-arrow{image:url(" << getImagePath("icon_scroll_right.png") << "); }"
		<< "QComboBox::drop-down{image:url(" << getImagePath("icon_selectlist.png") << "); }"
		;

	this->setStyleSheet(strDefaultStyle);

}

void OvrObjEditorWnd::preset_connects()
{
}

void OvrObjEditorWnd::initTopDock()
{
	//setWindowFlags((windowFlags() | Qt::FramelessWindowHint) & ~Qt::WindowContextHelpButtonHint);
	//QWidget* foo = new QWidget(ui.dock_top);
	//ui.dock_top->setTitleBarWidget(foo);

#if 0
	QWidget* bar = new QWidget;
	bar->setFixedHeight(24);
	QPushButton* button = new QPushButton("debug", bar);
	ui.dock_top->setWidget(bar);

	connect(button, &QPushButton::clicked, [this]() {
		debug();
	});
#endif
}

void OvrObjEditorWnd::debug()
{
	qDebug() << "scene->sceneRect" << scene->sceneRect();
	qDebug() << "view->sceneRect" << editor_view->sceneRect();
}

void OvrObjEditorWnd::initLeftDock()
{
	obj_list = new OvrObjEditorDockTree(this);
	obj_list->setColumnCount(1);
	obj_list->header()->hide();
	ui.dock_left->setWidget(obj_list);

	ovr_project::OvrSceneEditor*	scene_editor = ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor();
	ovr_core::OvrSceneContext* scene_context = ovr_project::OvrProject::GetIns()->GetCurrentScene();
	
	ovr_engine::OvrActor* root_actor = scene_context->GetRootActor();
	for (int i = 0; i < root_actor->GetChildCount(); ++i)
	{
		if (scene_editor->IsShowInScene(root_actor->GetChild(i)))
		{
			addActor(nullptr, root_actor->GetChild(i));
		}
	}

// 	const std::map<OvrString, ovr_engine::OvrActor*>& actors = scene_context->GetActorList();
// 	for (auto actor : actors)
// 	{
// 		if (scene_context->IsShowInScene(actor.second))
// 		{
// 			addActor(nullptr, actor.second);
// 		}
// 	}
	obj_list->expandAll();
}

void OvrObjEditorWnd::addActor(QTreeWidgetItem* a_item, ovr_engine::OvrActor* a_actor)
{
	QString title = QString::fromLocal8Bit(a_actor->GetDisplayName().GetStringConst());

	QTreeWidgetItem* item = addTreeItem(a_item, title, obj_manager->getActorClassID(), a_actor->GetUniqueName().GetStringConst());
	item->setIcon(0, getActorIcon(a_actor));

	for (int i = 0; i < a_actor->GetChildCount(); ++i)
	{
		addActor(item, a_actor->GetChild(i));
	}

	if (a_item == nullptr)
	{
		obj_list->addTopLevelItem(item);
	}
}

QTreeWidgetItem* OvrObjEditorWnd::addTreeItem(QTreeWidgetItem* a_parent_item, const QString& a_title, const QString& a_type_name, const QString& a_type_value)
{
	assert(!a_title.isEmpty());

	QStringList data;
	data << a_title;

	QTreeWidgetItem* item = new QTreeWidgetItem(a_parent_item, data);
	item->setData(0, kEditorItemType, a_type_name);
	item->setData(0, kEditorItemValue, a_type_value);

	return item;
}

void OvrObjEditorWnd::initRightDock()
{
	QFrame* frame = new QFrame;
	frame->setFixedWidth(202);

	QVBoxLayout* l_layout = new QVBoxLayout;
	l_layout->setSpacing(0);
	l_layout->setMargin(0);

	op_list = new OvrObjEditorDockTree();
	op_list->setColumnCount(1);
	op_list->header()->hide();

	std::list<ovr_core::OvrNodeInfo> node_infos;
	obj_manager->getNodeClasses(node_infos);

	for (auto node_info : node_infos)
	{
		addNodeClassItem(node_info);
	}
	op_list->expandAll();

	l_layout->addWidget(op_list);

	abrev_view = new OvrObjEditorAbrevView;
	abrev_view->setFixedHeight(150);
	l_layout->addWidget(abrev_view);

	frame->setLayout(l_layout);

	//ui.dock_right->setWidget(op_list);
	ui.dock_right->setWidget(frame);
}

void OvrObjEditorWnd::addNodeClassItem(const ovr_core::OvrNodeInfo& a_info)
{
	QString node_id = QString::fromLocal8Bit(a_info.node_id.GetStringConst());
	QString node_category = QString::fromLocal8Bit(a_info.node_category.GetStringConst());
	QStringList classes = node_category.split("/");
	assert(classes.size() == 2);

	QString main_class = classes.at(0);
	QString sub_class = classes.at(1);

	addNodeClassItem(main_class, sub_class, node_id);
}

void OvrObjEditorWnd::addNodeClassItem(const QString& a_main_class, const QString& a_sub_class, const QString& a_node_class)
{
	QTreeWidgetItem* root = getRootItem(a_main_class);

	addTreeItem(root, a_sub_class, a_node_class, "");
}

QTreeWidgetItem* OvrObjEditorWnd::getRootItem(const QString& a_main_class)
{
	if (root_entry.contains(a_main_class))
	{
		return root_entry[a_main_class];
	}

	QTreeWidgetItem* root = addTreeItem(nullptr, a_main_class, "", "");
	root->setIcon(0, QIcon(getImagePath("static_actor.png")));

	op_list->addTopLevelItem(root);
	root_entry[a_main_class] = root;
	return root;
}

void OvrObjEditorWnd::initContext()
{
	scene = new OvrObjEditorScene(this);
	scene->init(obj_manager);

	editor_view = new OvrObjEditorView;
	setCentralWidget(editor_view);
	editor_view->restoreOffset(obj_manager->getViewX(), obj_manager->getViewY());
	editor_view->restoreScale(obj_manager->getViewScale());

	editor_view->setScene(scene);
	abrev_view->setScene(scene);
	abrev_view->delayUpdate(0);

	connect(scene, &QGraphicsScene::changed, [this](const QList<QRectF> &region){
		if (region.size() > 0)
		{
			abrev_view->delayUpdate();
		}
	});
	connect(abrev_view, &OvrObjEditorAbrevView::onDrag, editor_view, &OvrObjEditorView::onDrag, Qt::QueuedConnection);

	connect(editor_view, &OvrObjEditorView::onViewOffsetChanged, [this](float x, float y) {
		obj_manager->setViewOffset(x, y);
	});
	connect(editor_view, &OvrObjEditorView::onViewScaleChanged, [this](float a_value) {
		obj_manager->setViewScale(a_value);
	});
}

////////////////////////////////////////////////////////////////////
// class OvrObjEditorDockTree

OvrObjEditorDockTree::OvrObjEditorDockTree(QWidget *parent) :QTreeWidget(parent)
{
	//const char* split_style = "background:#494949;color:#CCCCCC;font-family:\"΢���ź�\";border:0px";
	//setStyleSheet(split_style);
	setFixedWidth(200);
}

OvrObjEditorDockTree::~OvrObjEditorDockTree()
{
	qDebug();
}

void OvrObjEditorDockTree::mousePressEvent(QMouseEvent *event)
{
	QTreeWidget::mousePressEvent(event);
	if (event->button() == Qt::LeftButton)
	{
		drag_item = itemAt(event->pos());
	}
}

void OvrObjEditorDockTree::mouseMoveEvent(QMouseEvent *event)
{
	QTreeWidget::mouseMoveEvent(event);

	if (drag_item != nullptr)
	{
		QString item_type = drag_item->data(0, kEditorItemType).toString();
		QString item_value = drag_item->data(0, kEditorItemValue).toString();
		emit onDragItem(drag_item->data(0, 0).toString(), item_type, item_value);
	}
}

void OvrObjEditorDockTree::mouseReleaseEvent(QMouseEvent *event)
{
	QTreeWidget::mouseReleaseEvent(event);
	drag_item = nullptr;
}

void OvrObjEditorDockTree::resizeEvent(QResizeEvent *event)
{
	QTreeWidget::resizeEvent(event);
}