#include "Header.h"
#include "ovr_save_record_dlg.h"
#include "../res_view/ovr_resource_tree_model.h"
#include "../res_view/ovr_resource_list_widget.h"
#include "../tml_view/ovr_time_line_tree.h"

QString GetCaptureSavedFilePath(bool a_is_motion)
{
	QString caption = a_is_motion ? QString::fromLocal8Bit("保存动作捕捉数据") : QString::fromLocal8Bit("保存表情捕捉数据");
	QString content_dir = OvrResourceTreeModel::getInst()->getContentDir();
	QString filter = a_is_motion ? QObject::tr("motion file(*.ani)") : QObject::tr("face file(*.pani)");
	QString file_path = QFileDialog::getSaveFileName(nullptr, caption, content_dir, filter);
	if (file_path.length() > 0 && !file_path.startsWith(content_dir))
	{
		QMessageBox::warning(nullptr, QString::fromLocal8Bit("文件路径不符合规范"), QString::fromLocal8Bit("请将捕捉文件保存在content目录下！"));
		return  "";
	}
	return file_path;
}

OVRSaveRecordDlg::OVRSaveRecordDlg(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

OVRSaveRecordDlg::~OVRSaveRecordDlg()
{
}


int OVRSaveRecordDlg::init(const std::map<OvrString, ovr_core::OvrActorTrack*>& a_actor_tracks)
{
	QTreeWidget* tree = ui.item_tree;
	tree->setColumnWidth(0, 160);
	tree->setColumnWidth(1, 80);
	tree->setColumnWidth(2, 200);
	int count = 0;
	for (auto actor_track : a_actor_tracks)
	{
		for (auto child_track : actor_track.second->GetChildren())
		{
			/*if (child_track->GetTrackType() == ovr_core::kTrackSkeletalCapture || child_track->GetTrackType() == ovr_core::kTrackMorphCapture)
			{
				ovr_core::OvrActorSubCaptureTrack* capture_track = static_cast<ovr_core::OvrActorSubCaptureTrack*>(child_track);
				if (capture_track->IsModified())
				{
					QTreeWidgetItem* item = new QTreeWidgetItem(tree);
					//actor
					item->setText(0, QString::fromLocal8Bit(actor_track.second->GetActor()->GetDisplayName().GetStringConst()));
					//track type
					item->setText(1, QString::fromLocal8Bit(child_track->GetDisplayName().GetStringConst()));
					//range
					{
						ovr::uint64 begine_time, end_time;
						capture_track->GetRange(begine_time, end_time);
						QString frame_range;
						QTextStream(&frame_range) << begine_time << " - " << end_time;
						item->setText(2, frame_range);
					}
					//save
					item->setText(3, QString::fromLocal8Bit("点击此处保存"));
					item->setData(3, Qt::UserRole, (int)capture_track);
					item->setTextColor(3, Qt::red);
					++count;
				}
			}*/

		}
	}
	return count;
}

void OVRSaveRecordDlg::on_item_tree_itemClicked(QTreeWidgetItem *item, int column)
{
	/*qDebug() << "clicked: " << item->text(0) << ":" << item->text(1) << ":col " << column;
	if (column == 3)
	{
		void* child_track = (void*)item->data(3, Qt::UserRole).toInt();
		ovr_core::OvrActorSubCaptureTrack* capture_track = static_cast<ovr_core::OvrActorSubCaptureTrack*>(child_track);
		if (capture_track->IsModified())
		{
			bool is_motion = ovr_core::kTrackSkeletalCapture == capture_track->GetTrackType();
			QString file_path = GetCaptureSavedFilePath(is_motion);
			if (!file_path.isEmpty())
			{
				if (capture_track->Save(file_path.toLocal8Bit().constData(),
					OvrResourceTreeModel::getInst()->getRootDir().toLocal8Bit().constData()))
				{
					notifyEvent("onResDirRefresh", 0, 0, false);
					item->setText(3, QString::fromLocal8Bit("已保存"));
					item->setTextColor(3, Qt::white);
					OvrTimeLineTree::getInst()->update();
				}
				else
				{
					QMessageBox::warning(nullptr, QString::fromLocal8Bit("保存捕捉文件"), QString::fromLocal8Bit("保存失败！"));
				}
			}
		}
	}*/
}