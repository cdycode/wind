#pragma once

#include <QDialog>
#include "ui_new_prj_dlg.h"

class NewPrjDlg : public QDialog
{
	Q_OBJECT

public:
	NewPrjDlg(QWidget *parent = Q_NULLPTR);
	~NewPrjDlg();
	QString prj_file_path;

public slots:
	void on_new_btn_clicked();
	void on_prj_dir_set_clicked();
	void on_cancel_btn_clicked();

private:
	void init_custom_ui();
	void set_custom_style_sheet();
	void preset_connects();

protected:
	void initNewProjectDir();

	Ui::NewPrjDlg ui;

	QString prj_dir;

private:
	QPoint m_last;
	bool m_bIsPushed;

};
