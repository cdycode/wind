#pragma once

#include <QDialog>
#include "ui_ovr_common_dialog.h"

class OvrCommonDialog : public QDialog
{
	Q_OBJECT

public:
	OvrCommonDialog(QWidget *parent = Q_NULLPTR);
	~OvrCommonDialog();

	void setClient(QWidget* a_client);

private:
	void initStyle();

	Ui::OvrCommonDailog ui;
};
