#include "Header.h"
#include "new_prj_dlg.h"

#include "../utils_ui/title_bar.h"

NewPrjDlg::NewPrjDlg(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	init_custom_ui();
	set_custom_style_sheet();
	preset_connects();

	initNewProjectDir();
}

NewPrjDlg::~NewPrjDlg()
{
}

void NewPrjDlg::init_custom_ui()
{
	setWindowFlags(windowFlags() | Qt::FramelessWindowHint);

	ui.title_bar->setUiFlags(TitleBar::TitleFlag::TF_BTN_CLOSE);

	QString strTitle = QString::fromLocal8Bit("新建工程");
	ui.title_bar->setTitle(strTitle);
	this->installEventFilter(ui.title_bar);

	setWindowTitle(strTitle);

	QRegExp regExp = QRegExp("[^\\\\/*:|?\"\\s]*");
	QRegExp regExpPort = QRegExp(regExp);
	QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExpPort);
	ui.prj_name->setMaxLength(100);
	ui.prj_name->setValidator(portRegExpValidator);

}

void NewPrjDlg::set_custom_style_sheet()
{
	QString strDlgStyle = QString::fromLocal8Bit(
		"QWidget:focus{ outline: none;}"
		"QWidget{background-color: rgb(48,48,48);color:rgb(204, 204, 204); font-family:\"微软雅黑\";}"
		"QLabel{font-size:14px;}"
		"QLineEdit{height: 26px; border-radius: 2px; background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); border-radius:2px; font-size:12px; selection-background-color: rgb(165, 165, 165); selection-color: rgb(32, 32, 32);}"
		"QPushButton{width: 138px; height: 38px; color: rgb(205, 205, 205); border: 1px solid rgb(30, 30, 30); border-radius: 4px; background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); }"
		"QPushButton:hover{background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(153, 153, 153), stop:1 rgb(109, 109, 109)); }"
		"QPushButton:pressed {background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:1 rgb(118, 118, 118), stop:0 rgb(87, 87, 87)); }"
		"QPushButton:disabled {background-color:rgb(56, 56, 56); color:rgb(76, 76, 76);  border: 1px solid rgb(76, 76, 76); }"

		"QMessageBox {color: rgb(204, 204, 204); background-color: rgba(48, 48, 48); border-radius: 2px; messagebox-text-interaction-flags: 5 }"
		"QMessageBox QPushButton {border: 1px solid rgb(30, 30, 30); border-radius: 2px; width: 70px; height: 25px; background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgba(118, 118, 118, 255), stop:1 rgba(87, 87, 87, 255));}"
	);
	this->setStyleSheet(strDlgStyle);

	QString strTitleBarStyle = QString::fromLocal8Bit(
		"QWidget{background-color: rgb(48,48,48); color:rgb(204, 204, 204); font-family:\"微软雅黑\"; border:0px solid rgb(76, 76, 76);}"
		"QLabel{font:normal 16px; font-family:\"微软雅黑\";}"
	);
	ui.title_bar->setStyleSheet(strTitleBarStyle);

	//
	QString strBtnSetStyle = QString::fromLocal8Bit(
		"QPushButton{background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(56, 56, 56), stop:1 rgb(50, 50, 50)); color: rgb(204, 204, 204); border: 1px solid rgb(76, 76, 76); border-radius: 2px;}"
		"QPushButton:hover{background-color:rgb(76, 76, 76);}"
		"QPushButton:pressed {background-color:rgb(56, 56, 56);}"
	);
	ui.prj_dir_set->setStyleSheet(strBtnSetStyle);

	//
	QString strLineStyle = QString::fromLocal8Bit("background-color: rgb(59, 59, 59); border-top: 1px solid rgb(38, 38, 38); border-bottom: 1px solid rgb(48, 48, 48);");
	ui.line->setStyleSheet(strLineStyle);

}

void NewPrjDlg::preset_connects()
{
	//QObject::connect(ui.close_btn, SIGNAL(clicked()), this, SLOT(on_cancel_btn_clicked()));
}

void NewPrjDlg::on_new_btn_clicked()
{
	if (ui.prj_name->text().isEmpty())
	{
		OvrHelper::getInst()->warning("工程名不能为空！", this);
		return;
	}

	prj_file_path = prj_dir + "/" + ui.prj_name->text() + "/" + ui.prj_name->text() + kPrjFileTail;
	QFile file(prj_file_path);
	if (file.exists())
	{
		OvrHelper::getInst()->warning("工程已存在！", this);
		return;
	}
	QDir dir(prj_dir);
	bool create_ok = dir.mkdir(ui.prj_name->text());
	assert(create_ok);

	done(1);
}

void NewPrjDlg::on_prj_dir_set_clicked()
{
	QString dir = QFileDialog::getExistingDirectory(nullptr, QString::fromLocal8Bit("创建工程"), prj_dir);
	if (dir.isEmpty() || prj_dir == dir)
	{
		return;
	}
	prj_dir = dir;
	OvrProjectManager::getInst()->getConfiger()->setLastNewPrjDir(prj_dir);
	ui.prj_path->setText(prj_dir );
}

void NewPrjDlg::on_cancel_btn_clicked()
{
	done(0);
}

void NewPrjDlg::initNewProjectDir()
{
	prj_dir = OvrProjectManager::getInst()->getConfiger()->getLastNewPrjDir();
	ui.prj_path->setText(prj_dir);
}