#include "Header.h"
#include "ovr_common_color_dlg.h"
#include <QColorDialog> 

OvrCommonColorDlg::OvrCommonColorDlg(QColor color, QWidget *parent, Qt::WindowFlags f)
	: QDialog(parent, f)
	, source_color_red(color.redF())
	, source_color_green(color.greenF())
	, source_color_blue(color.blueF())
	, source_transparency(color.alphaF())
{
	ui.setupUi(this);

	init_curstom_ui();
	set_custom_style_sheet();
	preset_connects();
}

OvrCommonColorDlg::~OvrCommonColorDlg()
{

}


void OvrCommonColorDlg::init_curstom_ui()
{
	setWindowFlags(windowFlags() | Qt::FramelessWindowHint);

	ui.title_bar->setUiFlags(TitleBar::TitleFlag::TF_BTN_CLOSE);

	QString strTitle = QString::fromLocal8Bit("��ɫ����");
	ui.title_bar->setTitle(strTitle);

	installEventFilter(ui.title_bar);
	setWindowTitle(strTitle);

	QString cs_reg_exp_filter = QString::fromLocal8Bit("^[+\\-]{0,1}\\d*\\.{0,1}\\d*$");
	QRegExp regExpPos = QRegExp(cs_reg_exp_filter);
	QRegExpValidator *posRegExpValidator = new QRegExpValidator(regExpPos);
	ui.lineEdit_r->setInputMethodHints(Qt::ImhDigitsOnly);
	ui.lineEdit_r->setMaxLength(10);
	ui.lineEdit_r->setValidator(posRegExpValidator);

	ui.lineEdit_g->setInputMethodHints(Qt::ImhDigitsOnly);
	ui.lineEdit_g->setMaxLength(10);
	ui.lineEdit_g->setValidator(posRegExpValidator);

	ui.lineEdit_b->setInputMethodHints(Qt::ImhDigitsOnly);
	ui.lineEdit_b->setMaxLength(10);
	ui.lineEdit_b->setValidator(posRegExpValidator);

	ui.lineEdit_a->setInputMethodHints(Qt::ImhDigitsOnly);
	ui.lineEdit_a->setMaxLength(10);
	ui.lineEdit_a->setValidator(posRegExpValidator);


	//QString regExp = QString("[^\\\\/*:|?\"\\s]*");
	//QRegExp regExpPort = QRegExp(regExp);
	//QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExpPort);
	//ui.key_name->setMaxLength(100);
	//ui.key_name->setValidator(portRegExpValidator);

	//setSampleColor(current_color_red, current_color_green, current_color_blue);

	//QColorDialog * color_dlg = new QColorDialog(this);

	//Qt::WindowFlags flags = Qt::WindowType::SubWindow;  // Dialog
	//color_dlg->setWindowFlags(color_dlg->windowFlags() | flags);

	//ui.gridLayout->addWidget(color_dlg, 5, 0, 1, 2);

	setCurrentColor(source_color_red, source_color_green, source_color_blue);
	ui.horizontalSlider_a->setValue(source_transparency * 100);

	ui.button_ok->setDefault(true);
}

void OvrCommonColorDlg::set_custom_style_sheet()
{
	QString strDlgStyle = QString::fromLocal8Bit(""
		"QPushButton{width:138px; height:38px; color:rgb(204, 204, 204); border:1px solid rgb(30, 30, 30); border-radius:4px; background-color:qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); }"
		"QPushButton:hover{background-color:qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(153, 153, 153), stop:1 rgb(109, 109, 109)); }"
		"QPushButton:pressed {background-color:qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:1 rgb(118, 118, 118), stop:0 rgb(87, 87, 87)); }"
		"QPushButton:disabled {background-color:rgb(56, 56, 56); color:rgb(76, 76, 76);  border:1px solid rgb(76, 76, 76); }"
	);
	this->setStyleSheet(strDlgStyle);

	QString strTitleBarStyle = QString::fromLocal8Bit(
		"QWidget{background-color: rgb(48,48,48); color:rgb(204, 204, 204); font-family:\"΢���ź�\"; border:0px solid rgb(76, 76, 76);}"
		"QLabel{font:normal 16px; font-family:\"΢���ź�\";}"
	);
	ui.title_bar->setStyleSheet(strTitleBarStyle);

	QString strLineStyle = QString::fromLocal8Bit("background-color:rgb(59, 59, 59); border-top:1px solid rgb(38, 38, 38); border-bottom:1px solid rgb(48, 48, 48);");
	ui.line->setStyleSheet(strLineStyle);
}

void OvrCommonColorDlg::preset_connects()
{
}


void OvrCommonColorDlg::mouseDoubleClickEvent(QMouseEvent *event) 
{
	if ((childAt(event->pos()) == ui.color_show))
	{
		QColorDialog colorDialog(this);
		colorDialog.setCurrentColor(QColor(source_color_red * 255, source_color_green * 255, source_color_blue * 255));

		//connect(&colorDialog, &QColorDialog::colorSelected, this, &OvrCommonColorDlg::setColor);
		connect(&colorDialog, &QColorDialog::currentColorChanged, this, &OvrCommonColorDlg::setColor);

		if (QDialog::Rejected == colorDialog.exec()) 
		{
			//ȡ����ǰ������
			setColor(QColor(source_color_red * 255, source_color_green * 255, source_color_blue * 255));
		}
	}
}

void OvrCommonColorDlg::closeEvent(QCloseEvent *event)
{
	on_button_cancel_clicked();
}

//void OvrCommonColorDlg::on_close_button_clicked()
//{
//	ovr_project::OvrProject::GetIns()->GetCurrentScene()->GetScene()->SetAmbientColor(OvrVector4(current_color_red, current_color_green, current_color_blue, 1.0f));
//	done(0);
//}

void OvrCommonColorDlg::on_button_cancel_clicked() 
{
	emit(color_changed(source_color_red, source_color_green, source_color_blue, source_transparency));
	//emit(color_changed(QColor(current_color_red*255, current_color_green * 255, current_color_blue * 255, current_transparency * 255)));

	done(QDialog::Rejected);
}

void OvrCommonColorDlg::on_button_ok_clicked() 
{
	source_color_red	= ui.lineEdit_r->text().toFloat();
	source_color_green = ui.lineEdit_g->text().toFloat();
	source_color_blue	= ui.lineEdit_b->text().toFloat();
	source_transparency = ui.lineEdit_a->text().toFloat();

	done(QDialog::Accepted);
}

void OvrCommonColorDlg::on_lineEdit_r_editingFinished()
{
	check_color_ui(ui.lineEdit_r);

	updateColor();
}

void OvrCommonColorDlg::on_lineEdit_g_editingFinished()
{
	check_color_ui(ui.lineEdit_g);

	updateColor();
}

void OvrCommonColorDlg::on_lineEdit_b_editingFinished()
{
	check_color_ui(ui.lineEdit_b);

	updateColor();
}
void OvrCommonColorDlg::on_horizontalSlider_a_valueChanged(int value)
{
	float new_value = value / 100.0f;
	ui.lineEdit_a->setText(QString("%1").arg(new_value));
	updateColor();
}

void OvrCommonColorDlg::on_lineEdit_a_editingFinished()
{
	check_color_ui(ui.lineEdit_a);

	float value = ui.lineEdit_a->text().toFloat();
	ui.horizontalSlider_a->setValue(value * 100);
}

void OvrCommonColorDlg::setColor(const QColor &color)
{
	float red = color.red() / 255.0f;
	float green = color.green() / 255.0f;
	float blue = color.blue() / 255.0f;
	float intensity = ui.lineEdit_a->text().toFloat();

	setCurrentColor(red, green, blue);

	emit(color_changed(red, green, blue, intensity));
	//emit(color_changed(QColor(red * 255, green * 255, blue * 255, transparency * 255)));
}

void OvrCommonColorDlg::setSampleColor(float a_red, float a_green, float a_blue) 
{
	QString styleSheet = QString("background:rgb(%1,%2,%3)")
		.arg(a_red * 255).arg(a_green * 255).arg(a_blue * 255);

	ui.color_show->setStyleSheet(styleSheet);
}

void OvrCommonColorDlg::setCurrentColor(float a_red, float a_green, float a_blue)
{
	setSampleColor(a_red, a_green, a_blue);

	ui.lineEdit_r->setText(QString("%1").arg(a_red));
	ui.lineEdit_g->setText(QString("%1").arg(a_green));
	ui.lineEdit_b->setText(QString("%1").arg(a_blue));
}

void OvrCommonColorDlg::updateColor() 
{
	float color_red = ui.lineEdit_r->text().toFloat();
	float color_green = ui.lineEdit_g->text().toFloat();
	float color_blue = ui.lineEdit_b->text().toFloat();
	float transparency = ui.lineEdit_a->text().toFloat();

	setSampleColor(color_red, color_green, color_blue);

	emit(color_changed(color_red, color_green, color_blue, transparency));
	//emit(color_changed(QColor(red * 255, green * 255, blue * 255, transparency * 255)));
}

void OvrCommonColorDlg::check_color_ui(QLineEdit * line_edit)
{
	float color = line_edit->text().toFloat();
	float fmt_color = color < 0 ? 0 : color > 1 ? 1 : color;

	if (color > 1 || color< 0 || std::fabs(color - fmt_color) > 1E-4)
		line_edit->setText(QString("%1").arg(fmt_color));
}



