#pragma once

#include <QDialog>
#include "ui_ovr_save_record_dlg.h"

namespace ovr_project
{
	class OvrActorTrack;
}
class OvrString;

class OVRSaveRecordDlg : public QDialog, public OvrEventSender
{
	Q_OBJECT

public:
	OVRSaveRecordDlg(QWidget *parent = Q_NULLPTR);
	~OVRSaveRecordDlg();

	virtual QString senderName() const override { return "OVRSaveRecordDlg"; }

	int init(const std::map<OvrString, ovr_core::OvrActorTrack*>& a_actor_tracks);

public slots:
	void on_item_tree_itemClicked(QTreeWidgetItem *item, int column);

private:
	Ui::OVRSaveRecordDlg ui;
};
