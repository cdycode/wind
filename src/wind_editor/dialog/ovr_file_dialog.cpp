#include "Header.h"
#include "ovr_file_dialog.h"
#include "ovr_file_dialog_helper.h"

#include <cassert>
#include <QDebug>
#include <QPushButton>
#include <QStandardItemModel>
#include <QKeyEvent>
#include <QDialogButtonBox>
#include <QTimer>

static QString last_open_dir;

OvrFileDialog::OvrFileDialog(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
	setSizeGripEnabled(true);

	init_custom_ui();
	set_custom_style_sheet();
	preset_connects();

	initStyle();
	initToolBar();
	initSideBar();
	initContentView();
	initBottomUI();
}

OvrFileDialog::~OvrFileDialog()
{
}

void OvrFileDialog::init_custom_ui()
{
}

void OvrFileDialog::set_custom_style_sheet()
{
	QString strDefaultStyle = QString::fromLocal8Bit(
		"QWidget:focus{outline: none;}"
		"QWidget{background-color:rgb(48,48,48); color:rgb(204, 204, 204); font-family:\"微软雅黑\";}"
		"QLabel{font-size:14px;}"
		"QLineEdit{height:20px; border-radius:2px; background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); border-radius:2px; font-size:12px; selection-background-color:rgb(165, 165, 165); selection-color: rgb(32, 32, 32);}"
		"QPushButton{width:70; height:20px; color:rgb(205, 205, 205); border:1px solid rgb(30, 30, 30); border-radius:4px; background-color:qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); }"
		"QPushButton:hover{background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(153, 153, 153), stop:1 rgb(109, 109, 109)); }"
		"QPushButton:pressed {background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:1 rgb(118, 118, 118), stop:0 rgb(87, 87, 87)); }"
		"QPushButton:disabled {background-color:rgb(56, 56, 56); color:rgb(76, 76, 76);  border: 1px solid rgb(76, 76, 76); }"

		// QComboBox
		"QComboBox{height:20px; border:1px solid rgb(76,76,76); background:rgb(87,87,87); color:rgb(204,204,204); selection-background-color:rgb(165, 165, 165); selection-color:rgb(32, 32, 32);}"
		"QComboBox QAbstractItemView {border:0px; background-color:rgb(255, 255, 255); color:rgb(81, 81, 81); selection-background-color:rgb(161, 161, 161); selection-color:rgb(255, 255, 255);}"
		"QComboBox QAbstractItemView::item {background:rgb(255, 255, 255); color:rgb(81, 81, 81); height:23px;}"
		"QComboBox QAbstractItemView::item:focus {selection-background-color:rgb(94, 94, 94);}"
		"QComboBox QAbstractItemView::item:selected {background-color: rgb(161, 161, 161); color:rgb(255, 255, 255);}"

		"QMessageBox {color: rgb(204, 204, 204); background-color: rgba(48, 48, 48); border-radius: 2px; messagebox-text-interaction-flags: 5 }"
		"QMessageBox QPushButton {border: 1px solid rgb(30, 30, 30); border-radius: 2px; width: 70px; height: 25px; background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgba(118, 118, 118, 255), stop:1 rgba(87, 87, 87, 255));}"

		// 垂直滚动条
		"QScrollBar:vertical{background-color:rgb(35, 35, 35);width:16px; margin:16px 0 16px 0; border:0px solid rgb(135, 135, 35);}"
		"QScrollBar::handle:vertical{background:rgb(119, 119, 119); border: 2px solid rgb(35, 35, 35); background-clip: border; border-radius:7px; min-height: 20px;}"
		"QScrollBar::handle:vertical:hover{background:rgb(129, 129, 129);}"

		"QScrollBar::sub-line:vertical {background:rgb(35, 35, 35); height: 16px; subcontrol-position: top;subcontrol-origin: margin;}"
		"QScrollBar::sub-line:vertical:hover {background:rgba(135, 135, 135, 50); border-top-left-radius:4px; border-top-right-radius:4px; height: 15px; subcontrol-position: top;subcontrol-origin: margin;}"
		"QScrollBar::sub-line:vertical:pressed {background:rgba(135, 135, 135, 30); border-top-left-radius:4px; border-top-right-radius:4px; height: 15px; subcontrol-position: top;subcontrol-origin: margin;}"

		"QScrollBar::add-line:vertical {background:rgb(35, 35, 35); height: 16px; subcontrol-position: bottom;subcontrol-origin: margin;}"
		"QScrollBar::add-line:vertical:hover {background:rgba(135, 135, 135, 50); border-bottom-left-radius:4px; border-bottom-right-radius:4px; height: 15px; subcontrol-position: bottom;subcontrol-origin: margin;}"
		"QScrollBar::add-line:vertical:pressed {background:rgba(135, 135, 135, 30); border-bottom-left-radius:4px; border-bottom-right-radius:4px; height: 15px; subcontrol-position: bottom;subcontrol-origin: margin;}"

		"QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {background:rgb(35, 35, 35);}"

		// 水平滚动条
		"QScrollBar:horizontal {background-color:rgb(35, 35, 35); height:16px; margin:0 16px 0 16px; border:0px solid rgb(135, 135, 35);}"
		"QScrollBar::handle:horizontal{background:rgb(119, 119, 119); border: 2px solid rgb(35, 35, 35); background-clip: border; border-radius:7px; min-width: 20px;}"
		"QScrollBar::handle:horizontal:hover{background:rgb(129, 129, 129);}"

		"QScrollBar::sub-line:horizontal {background:rgb(35, 35, 35); width:16px; subcontrol-position:left; subcontrol-origin:margin;}"
		"QScrollBar::sub-line:horizontal:hover {background:rgba(135, 135, 135, 50); border-top-left-radius:4px; border-bottom-left-radius:4px; width:15px; subcontrol-position: left;subcontrol-origin: margin;}"
		"QScrollBar::sub-line:horizontal:pressed {background:rgba(135, 135, 135, 30); border-top-left-radius:4px; border-bottom-left-radius:4px; width:15px; subcontrol-position: left;subcontrol-origin: margin;}"

		"QScrollBar::add-line:horizontal {background:rgb(35, 35, 35); width: 16px; subcontrol-position: right;subcontrol-origin: margin;}"
		"QScrollBar::add-line:horizontal:hover {background:rgba(135, 135, 135, 50); border-bottom-right-radius:4px; border-top-right-radius:4px; width: 15px; subcontrol-position: right; subcontrol-origin: margin;}"
		"QScrollBar::add-line:horizontal:pressed {background:rgba(135, 135, 135, 30); border-bottom-right-radius:4px; border-top-right-radius:4px; width: 15px; subcontrol-position: right; subcontrol-origin: margin;}"

		"QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal {background:rgb(35, 35, 35);}"
	);

	QTextStream qtsTemp(&strDefaultStyle);
	qtsTemp << "QScrollBar::up-arrow{image:url(" << getImagePath("icon_scroll_top.png") << ");}"
		<< "QScrollBar::down-arrow{image:url(" << getImagePath("icon_scroll_bottom.png") << ");}"
		<< "QScrollBar::left-arrow{image:url(" << getImagePath("icon_scroll_left.png") << "); }"
		<< "QScrollBar::right-arrow{image:url(" << getImagePath("icon_scroll_right.png") << "); }"
		<< "QComboBox::drop-down{image:url(" << getImagePath("icon_selectlist.png") << "); }"
		;

	this->setStyleSheet(strDefaultStyle);
}

void OvrFileDialog::preset_connects()
{
}


void OvrFileDialog::initStyle()
{
	scrollBarStyle = "";

	treeStyle =
		"QHeaderView::section{background-color:qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #616161, stop: 0.5 #505050, stop: 0.6 #434343, stop:1 #656565); color:rgb(204, 204, 204); padding-left:4px; border:1px solid #6c6c6c; }"

		"QTreeView{background-color:rgb(48, 48, 48); color:rgb(192, 192, 192); border:1px solid rgb(24, 24, 24); padding-left:0px; show-decoration-selected: 1;}"
		"QTreeView::item{/*height:24px;*/ border-bottom:1px solid rgb(39, 39, 39); color:rgb(192, 192, 192);}"
		"QTreeView::item::hover{background-color: rgb(53, 53, 53);}"
		"QTreeView::item:selected{background-color: rgb(64, 64, 64);}"
		"QTreeView::branch{/*height:24px;*/ border-bottom: 1px solid rgb(39, 39, 39);}"
		"QTreeView::branch:selected {background-color: rgb(64, 64, 64);}"
		;

	QTextStream qtsTempb(&treeStyle);
	qtsTempb << "QTreeView::branch:has-children:closed{image:url(" << getImagePath("icon_closefile_min.png") << ");}"
		<< "QTreeView::branch:has-children:open{image:url(" << getImagePath("icon_openfile_min.png") << ");}"
		;

}

void OvrFileDialog::initToolBar()
{
	ui.backButton->setIcon(style()->standardIcon(QStyle::SP_ArrowBack, 0, this));
	ui.backButton->setAutoRaise(true);
	ui.backButton->setToolTip(QString::fromLocal8Bit("Back"));
	ui.backButton->setEnabled(false);
	ui.backButton->hide();

	ui.forwardButton->setIcon(style()->standardIcon(QStyle::SP_ArrowForward, 0, this));
	ui.forwardButton->setAutoRaise(true);
	ui.forwardButton->setToolTip(QString::fromLocal8Bit("Forward"));
	ui.forwardButton->setEnabled(false);
	ui.forwardButton->hide();

	ui.toParentButton->setIcon(style()->standardIcon(QStyle::SP_FileDialogToParent, 0, this));
	ui.toParentButton->setAutoRaise(true);
	ui.toParentButton->setToolTip(QString::fromLocal8Bit("Parent Directory"));
	ui.toParentButton->setEnabled(false);

	ui.listModeButton->setIcon(style()->standardIcon(QStyle::SP_FileDialogListView, 0, this));
	ui.listModeButton->setAutoRaise(true);
	ui.listModeButton->setToolTip(QString::fromLocal8Bit("List View"));

	ui.detailModeButton->setIcon(style()->standardIcon(QStyle::SP_FileDialogDetailedView, 0, this));
	ui.detailModeButton->setAutoRaise(true);
	ui.detailModeButton->setToolTip(QString::fromLocal8Bit("Detail View"));

	QSize toolSize(ui.fileNameEdit->sizeHint().height(), ui.fileNameEdit->sizeHint().height());
	ui.backButton->setFixedSize(toolSize);
	ui.listModeButton->setFixedSize(toolSize);
	ui.detailModeButton->setFixedSize(toolSize);
	ui.forwardButton->setFixedSize(toolSize);
	ui.toParentButton->setFixedSize(toolSize);

	QObject::connect(ui.listModeButton, SIGNAL(clicked()), this, SLOT(showListView()));
	QObject::connect(ui.detailModeButton, SIGNAL(clicked()), this, SLOT(showDetailView()));
	QObject::connect(ui.toParentButton, SIGNAL(clicked()), this, SLOT(toParentDir()));

	//QString edit_style = "QLineEdit{background-color:rgb(62, 62, 62);color:#CCCCCC;border:1px solid rgb(76, 76, 76); font-size:12px; selection-background-color: rgb(165, 165, 165); selection-color: rgb(32, 32, 32); border-radius: 0px;}";
	//ui.curdir_edit->setStyleSheet(edit_style);

}

void OvrFileDialog::initSideBar()
{
	QList<int> sizes;
	sizes << 300 << 1000;
	ui.splitter->setSizes(sizes);

	size_bar_model = new QStandardItemModel(0, 1, this);
	ui.sidebar->setModel(size_bar_model);
	ui.sidebar->header()->hide();

	initSideBarStyle();

	connect(ui.sidebar, SIGNAL(onCurrentChanged(const QModelIndex, const QModelIndex)), this, SLOT(onUserSelectSidebar(const QModelIndex, const QModelIndex)), Qt::QueuedConnection);
}

void OvrFileDialog::initSideBarStyle()
{
	QString strStyle = "QWidget:focus{outline: none; border: 1px solid rgb(24, 24, 24);}";

	strStyle += treeStyle;
	strStyle += scrollBarStyle;

	ui.sidebar->setStyleSheet(strStyle);

	OvrSideBarItemDelegate* item_delegate = new OvrSideBarItemDelegate(this);
	ui.sidebar->setItemDelegate(item_delegate);
}

void OvrFileDialog::initContentView()
{
	file_sys_model = new QFileSystemModel(this);
	file_sys_model->setObjectName(QLatin1String("qt_filesystem_model"));
	file_sys_model->setNameFilterDisables(false);

	initListView();
	initTreeView();
}

void OvrFileDialog::initListView()
{
	ui.page->setLayout(ui.vboxlayout);

	ui.listView->setModel(file_sys_model);
	ui.listView->setViewMode(QListView::IconMode);
	ui.listView->setUniformItemSizes(true);
	ui.listView->setMovement(QListView::Static);
	ui.listView->setResizeMode(QListView::Adjust);
	ui.listView->setSpacing(1);
	ui.listView->setIconSize(QSize(100, 100));
	connect(ui.listView, &QListView::activated, this, &OvrFileDialog::onUserSelectItem, Qt::QueuedConnection);
	connect(ui.listView, &OvrSelectDlgListView::onCurrentChanged, this, &OvrFileDialog::onCurrentChanged, Qt::QueuedConnection);

	QString list_style =
		"QListView {color: rgb(204, 204, 204); background-color: rgb(39, 39, 39); border:1px solid  rgb(30, 30, 30); border-left:0px;}"
		"QListView::item:hover {background-color: rgb(53, 53, 53); color: rgb(204, 204, 204);}"
		"QListView::item:selected {background-color: rgb(83, 83, 83); selection-color: rgb(204, 204, 204);}"
		;
	list_style += scrollBarStyle;
	ui.listView->setStyleSheet(list_style);
}

void OvrFileDialog::initTreeView()
{
	ui.page_2->setLayout(ui.vboxlayout2);

	ui.treeView->setModel(file_sys_model);
	ui.treeView->setItemsExpandable(false);
	ui.treeView->setRootIsDecorated(false);
	ui.treeView->setSortingEnabled(true);
	ui.treeView->setStyleSheet(treeStyle + scrollBarStyle);
	QHeaderView *treeHeader = ui.treeView->header();
	QFontMetrics fm(ui.treeView->font());
	treeHeader->resizeSection(0, fm.width(QLatin1String("wwwwwwwwwwwwwwwwwww")));
	treeHeader->resizeSection(1, fm.width(QLatin1String("128.88 GB")));
	treeHeader->resizeSection(2, fm.width(QLatin1String("mp3Folder")));
	treeHeader->resizeSection(3, fm.width(QLatin1String("10/29/81 02:02PM")));
	treeHeader->setContextMenuPolicy(Qt::ActionsContextMenu);
	//treeHeader->setStyleSheet("QHeaderView::section {background:rgb(62, 62, 62);color:#DDDDDD;}");
	connect(ui.treeView, &QTreeView::activated, this, &OvrFileDialog::onUserSelectItem, Qt::QueuedConnection);
	connect(ui.treeView, &OvrSelectDlgTreeView::onCurrentChanged, this, &OvrFileDialog::onCurrentChanged, Qt::QueuedConnection);
}

void OvrFileDialog::initBottomUI()
{
	ui.buttonBox->button(QDialogButtonBox::Ok)->setText(QString::fromLocal8Bit("打开"));
	//ui.buttonBox->button(QDialogButtonBox::Ok)->setStyleSheet("QPushButton:disabled {color:gray}");
	ui.buttonBox->button(QDialogButtonBox::Cancel)->setText(QString::fromLocal8Bit("取消"));
	ui.buttonBox->button(QDialogButtonBox::Ok)->setDisabled(true);

	QObject::connect(ui.fileTypeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(fileNameFilterChanged(int)), Qt::QueuedConnection);
	QObject::connect(ui.fileNameEdit, SIGNAL(textChanged(const QString&)), this, SLOT(fileNameTextChanged(const QString&)));
}

void OvrFileDialog::showListView()
{
	ui.listModeButton->setDown(true);
	ui.detailModeButton->setDown(false);

	if (ui.stackedWidget->currentIndex() != 0);
	{
		ui.stackedWidget->setCurrentIndex(0);
	}
}

void OvrFileDialog::showDetailView()
{
	ui.listModeButton->setDown(false);
	ui.detailModeButton->setDown(true);

	if (ui.stackedWidget->currentIndex() != 1);
	{
		ui.stackedWidget->setCurrentIndex(1);
	}
}

void OvrFileDialog::showDefaultSideDir()
{
	if (!locateSideDir(last_open_dir))
	{
		ui.sidebar->setCurrentIndex(size_bar_model->index(0, 0));
	}
}

void OvrFileDialog::onUserSelectSidebar(const QModelIndex &current, const QModelIndex &previous)
{
	onUserSelectSidebar(current);
}

void OvrFileDialog::onUserSelectSidebar(const QModelIndex &index)
{
	if (index.isValid())
	{
		QModelIndex root_index = index;
		while (root_index.parent().isValid())
		{
			root_index = root_index.parent();
		}
		if (root_index.data(Qt::UserRole + 1).toString() != side_dir)
		{
			side_dir = root_index.data(Qt::UserRole + 1).toString();
			side_dir_text = root_index.data().toString();
			file_sys_model->setRootPath(side_dir);
		}

		QString path = index.data(Qt::UserRole + 1).toString();
		gotoDir(file_sys_model->index(path));
	}
}

bool OvrFileDialog::gotoDir(const QModelIndex& a_index)
{
	assert(a_index.isValid());

	QString new_dir = file_sys_model->filePath(a_index);
	if (new_dir == cur_dir)
	{
		return true;
	}

	updateCurrentDir(new_dir);

	ui.treeView->setRootIndex(a_index);
	ui.listView->setRootIndex(a_index);

	if (is_custom_filter_set)
	{
		updateFilter("");
	}
	if (!cur_file.isEmpty())
	{
		setCurFile("");
		updatefileName("");
	}

	ui.toParentButton->setDisabled(cur_dir == side_dir);
	return true;
}

void OvrFileDialog::updateCurrentDir(const QString& a_new_cur_dir)
{
	if (cur_dir == a_new_cur_dir)
	{
		return;
	}
	cur_dir = a_new_cur_dir;
	last_open_dir = cur_dir;

	QString text = side_dir_text;

	QString sub_dir = cur_dir.mid(side_dir.length());
	if (!sub_dir.isEmpty())
	{
		text += sub_dir.replace("/", " > ");
	}
	ui.curdir_edit->setText(text);
}

QString OvrFileDialog::getOwnedSideDir(const QString& a_path)
{
	for (int i = 0; i < size_bar_model->rowCount(); ++i)
	{
		QString result = size_bar_model->item(i)->data().toString();
		if (a_path.startsWith(result))
		{
			return result;
		}
	}
	return "";
}

bool OvrFileDialog::locateSideDir(QStandardItem* a_item, const QString& a_path)
{
	if (a_item->data().toString() == a_path)
	{
		ui.sidebar->setCurrentIndex(size_bar_model->indexFromItem(a_item));
		return true;
	}

	for (int i = 0; i < a_item->rowCount(); ++i)
	{
		QStandardItem* item = a_item->child(i);
		if (a_path.startsWith(item->data().toString(), Qt::CaseInsensitive))
		{
			if (locateSideDir(item, a_path))
			{
				return true;
			}
		}
	}
	return false;
}

bool OvrFileDialog::locateSideDir(const QString& a_path)
{
	if (a_path.isEmpty())
	{
		return false;
	}

	for (int i = 0; i < size_bar_model->rowCount(); ++i)
	{
		QStandardItem* item = size_bar_model->item(i);
		if (a_path.startsWith(item->data().toString(), Qt::CaseInsensitive))
		{
			if (locateSideDir(item, a_path))
			{
				return true;
			}
		}
	}
	return false;
}

void OvrFileDialog::onUserSelectItem(const QModelIndex &index)
{
	if (file_sys_model->isDir(index))
	{
		if (gotoDir(index))
		{
			QStandardItem* item = size_bar_model->itemFromIndex(ui.sidebar->currentIndex());
			QString path = file_sys_model->filePath(index);
			ui.sidebar->disconnect();
			locateSideDir(item, path);
			connect(ui.sidebar, SIGNAL(onCurrentChanged(const QModelIndex, const QModelIndex)), this, SLOT(onUserSelectSidebar(const QModelIndex, const QModelIndex)), Qt::QueuedConnection);

		}
	}
	else
	{
		if (cur_file != file_sys_model->filePath(index))
		{
			setCurFile(file_sys_model->filePath(index));
			updatefileName(file_sys_model->fileName(index));
		}
		done(1);
	}
}

void OvrFileDialog::onCurrentChanged(const QModelIndex &current, const QModelIndex &previous)
{
	bool is_list_change = sender() == ui.listView;

	is_list_change ? ui.treeView->disconnect() : ui.listView->disconnect();
	is_list_change ? ui.treeView->setCurrentIndex(current) : ui.listView->setCurrentIndex(current);

	if (!file_sys_model->isDir(current))
	{
		if (cur_file != file_sys_model->filePath(current))
		{
			setCurFile(file_sys_model->filePath(current));
			updatefileName(file_sys_model->fileName(current));
		}
	}

	if (is_list_change)
	{
		connect(ui.treeView, &QTreeView::activated, this, &OvrFileDialog::onUserSelectItem, Qt::QueuedConnection);
		connect(ui.treeView, &OvrSelectDlgTreeView::onCurrentChanged, this, &OvrFileDialog::onCurrentChanged, Qt::QueuedConnection);
	}
	else
	{
		connect(ui.listView, &QListView::activated, this, &OvrFileDialog::onUserSelectItem, Qt::QueuedConnection);
		connect(ui.listView, &OvrSelectDlgListView::onCurrentChanged, this, &OvrFileDialog::onCurrentChanged, Qt::QueuedConnection);
	}
}

void OvrFileDialog::toParentDir()
{
	QDir dir(cur_dir);
	if (dir.cdUp())
	{
		QString old_dir = cur_dir;
		gotoDir(file_sys_model->index(dir.path()));

		QModelIndex index = file_sys_model->index(old_dir);
		ui.treeView->setCurrentIndex(index);
		ui.listView->setCurrentIndex(index);

		ui.sidebar->disconnect();
		ui.sidebar->setCurrentIndex(ui.sidebar->currentIndex().parent());
		connect(ui.sidebar, SIGNAL(onCurrentChanged(const QModelIndex, const QModelIndex)), this, SLOT(onUserSelectSidebar(const QModelIndex, const QModelIndex)), Qt::QueuedConnection);
	}
}

void OvrFileDialog::fileNameFilterChanged(int index)
{
	if (index >= 0)
	{
		QStringList filters = ui.fileTypeCombo->itemData(index).toString().split(" ");
		file_sys_model->setNameFilters(filters);
	}
}

void OvrFileDialog::fileNameTextChanged(const QString& a_text)
{
	setCurFile("");
	updateFilter(a_text);
}

void OvrFileDialog::updateFilter(const QString& a_text)
{
	QStringList filters = file_sys_model->nameFilters();
	for (int i = 0; i < filters.count(); ++i)
	{
		QString filter = a_text + "*." + filters.at(i).split(".").at(1);
		filters.replace(i, filter);
	}
	file_sys_model->setNameFilters(filters);

	is_custom_filter_set = !a_text.isEmpty();
}

void OvrFileDialog::updatefileName(const QString& a_text)
{
	ui.fileNameEdit->disconnect();

	ui.fileNameEdit->setText(a_text);

	connect(ui.fileNameEdit, SIGNAL(textChanged(const QString&)), this, SLOT(fileNameTextChanged(const QString&)));
}

void OvrFileDialog::setCurFile(const QString& a_file_path)
{
	cur_file = a_file_path;
	ui.buttonBox->button(QDialogButtonBox::Ok)->setDisabled(cur_file.isEmpty());

	if (cur_file.isEmpty())
	{
		qDebug() << "clear selction";
		ui.treeView->selectionModel()->clear();
		ui.listView->selectionModel()->clear();
	}
}

void OvrFileDialog::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_Backspace)
	{
		if (cur_dir != side_dir)
		{
			toParentDir();
		}
	}

	QDialog::keyPressEvent(event);
}

void OvrFileDialog::on_buttonBox_accepted()
{
	qDebug() << "";
	done(1);
}

void OvrFileDialog::on_buttonBox_rejected()
{
	qDebug() << "";
	done(0);
}

void OvrFileDialog::setPrjectDir(const QString& a_path)
{
	work_dir = a_path;
}

//a_filter example: default select *.*
//	"abc (*.xxx)" 
//	"abc (*.xxx *.yyy)" 
//	"abc (*.xxx *.yyy);;xyz (*.xyz)" 
//a_select_file: default, select nothing
//  full path file to select first, must be in project dir
bool OvrFileDialog::init(const QString& a_title, const QString& a_filter, const QString& a_select_file)
{
	if (work_dir.isEmpty())
	{
		work_dir = QString::fromLocal8Bit(ovr_project::OvrProject::GetIns()->GetWorkDir().GetStringConst());
	}
	if (work_dir.isEmpty())
	{
		return false;
	}

	setWindowTitle(a_title);

	initFilters(a_filter);

	initSideBarData();

	showDetailView();

	QDir dir(work_dir);
	if (!browseFile(dir.filePath(a_select_file)))
	{
		showDefaultSideDir();
	}

	return true;
}

QString OvrFileDialog::getSelectFile()
{
	return cur_file.mid(work_dir.length());
}

void OvrFileDialog::initFilters(const QString& a_filters)
{
	QStringList filters = a_filters.split(";;");
	for (int i = 0; i < filters.count(); ++i)
	{
		addFilter(filters.at(i));
	}
	addFilter("all files (*.*)");
}

void OvrFileDialog::addFilter(const QString& a_filter)
{
	QRegExp rp("\\((.+)\\)");

	if (rp.indexIn(a_filter) > -1)
	{
		ui.fileTypeCombo->addItem(a_filter);
		ui.fileTypeCombo->setItemData(ui.fileTypeCombo->count() - 1, rp.cap(1));
	}
}

void OvrFileDialog::initSideBarData()
{
	assert(!work_dir.isEmpty());

	static const char* sub_titles[] = { "资源", "系统" };
	static const char* sub_paths[] = { "content" , "system" };
	static int side_item_count = sizeof(sub_paths) / sizeof(const char*);

	QDir dir(work_dir);
	for (int i = 0; i < side_item_count; ++i)
	{
		dir.mkpath(sub_paths[i]);
		dir.cd(sub_paths[i]);

		QStandardItem* item = new QStandardItem(QString::fromLocal8Bit(sub_titles[i]));
		item->setData(dir.path());
		size_bar_model->appendRow(item);
		fillChildDir(item, dir);

		dir.cd(work_dir);
	}
	ui.sidebar->expandAll();
}

void OvrFileDialog::fillChildDir(QStandardItem* a_item, QDir& a_dir)
{
	QFileInfoList file_infors = a_dir.entryInfoList(QDir::AllDirs| QDir::NoSymLinks| QDir::NoDotAndDotDot, QDir::Name);
	if (file_infors.count() > 0)
	{
		for (auto file_info : file_infors)
		{
			QString dir_name = file_info.baseName();
			QStandardItem* item = new QStandardItem(dir_name);
			item->setData(file_info.filePath());
			a_item->appendRow(item);			
			if (a_dir.cd(dir_name))
			{
				fillChildDir(item, a_dir);
				a_dir.cdUp();
			}
		}
	}
}

bool OvrFileDialog::browseFile(const QString& a_select_file)
{
	if (a_select_file.isEmpty())
	{
		return false;
	}

	QFileInfo file_info(a_select_file);
	if (!file_info.exists())
	{
		return false;
	}

	//检查是否在导航目录内
	if (locateSideDir(file_info.path()))
	{
		QTimer::singleShot(0, [this, a_select_file]() {
			gotoFile(a_select_file);
		});
		return true;
	}
	return false;
}

bool OvrFileDialog::gotoFile(QString a_select_file)
{
	//检查是否是有效文件
	QModelIndex file_index = file_sys_model->index(a_select_file);
	if (!file_index.isValid() || file_sys_model->isDir(file_index))
	{
		return false;
	}

	QDir dir(a_select_file);
	dir.cdUp();
	if (gotoDir(file_sys_model->index(dir.path())))
	{
		ui.listView->setCurrentIndex(file_index);
		return true;
	}
	return false;
}