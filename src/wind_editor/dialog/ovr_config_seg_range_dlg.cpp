#include "Header.h"
#include "ovr_config_seg_range_dlg.h"

OvrConfigSegRangeDlg::OvrConfigSegRangeDlg(int& a_secs, int& a_fps, QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	
	init_curstom_ui();
	set_custom_style_sheet();
	preset_connects();

	int index = ui.fps_list->findText(QString::number(a_fps));
	assert(index >= 0);
	ui.fps_list->setCurrentIndex(index);
	ui.range->setText(QString::number(a_secs));
}

OvrConfigSegRangeDlg::~OvrConfigSegRangeDlg()
{
}

int OvrConfigSegRangeDlg::getFps()
{
	return ui.fps_list->currentText().toInt();
}

int OvrConfigSegRangeDlg::getSecs()
{
	int result = ui.range->text().toInt();
	if (result < 1)
	{
		result = 1;
	}
	return result;
}

void OvrConfigSegRangeDlg::on_button_ok_clicked()
{
	done(1);
}

void OvrConfigSegRangeDlg::on_button_cancel_clicked()
{
	done(0);
}

void OvrConfigSegRangeDlg::init_curstom_ui()
{
	setWindowFlags(windowFlags() | Qt::FramelessWindowHint);

	ui.title_bar->setUiFlags(TitleBar::TitleFlag::TF_BTN_CLOSE);
	ui.title_bar->setTitle(QString::fromLocal8Bit("ʱ��������"));

	installEventFilter(ui.title_bar);

	ui.fps_list->setView(new QListView());

	//QString regPosExp = QString("\\d*");
	//QRegExp regExpPos = QRegExp(regPosExp);
	//QRegExpValidator *posRegExpValidator = new QRegExpValidator(regExpPos);
	//ui.key_pos->setInputMethodHints(Qt::ImhDigitsOnly);
	//ui.key_pos->setMaxLength(100);
	//ui.key_pos->setValidator(posRegExpValidator);

	//QString regExp = QString("[^\\\\/*:|?\"\\s]*");
	//QRegExp regExpPort = QRegExp(regExp);
	//QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExpPort);
	//ui.key_name->setMaxLength(100);
	//ui.key_name->setValidator(portRegExpValidator);

}

void OvrConfigSegRangeDlg::set_custom_style_sheet()
{
	QString strDlgStyle = QString::fromLocal8Bit(
		"QWidget:focus{ outline: none;}"
		"QWidget{background-color: rgb(48,48,48);color:rgb(204, 204, 204); font-family:\"΢���ź�\";}"
		"QLabel{font-size:14px;}"
		"QLineEdit{height: 26px; border-radius: 2px; background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); border-radius:2px; font-size:12px; selection-background-color: rgb(165, 165, 165); selection-color: rgb(32, 32, 32);}"
		"QPushButton{width: 138px; height: 38px; color: rgb(205, 205, 205); border: 1px solid rgb(30, 30, 30); border-radius: 4px; background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); }"
		"QPushButton:hover{background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(153, 153, 153), stop:1 rgb(109, 109, 109)); }"
		"QPushButton:pressed {background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:1 rgb(118, 118, 118), stop:0 rgb(87, 87, 87)); }"
		"QPushButton:disabled {background-color:rgb(56, 56, 56); color:rgb(76, 76, 76);  border: 1px solid rgb(76, 76, 76); }"
		"QMessageBox {color: rgb(204, 204, 204); background-color: rgba(48, 48, 48); border-radius: 2px; messagebox-text-interaction-flags: 5 }"
		"QMessageBox QPushButton {border: 1px solid rgb(30, 30, 30); border-radius: 2px; width: 70px; height: 25px; background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgba(118, 118, 118, 255), stop:1 rgba(87, 87, 87, 255));}"
		"QComboBox{height:26px; background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); font-size:12px; selection-background-color: rgb(165, 165, 165); selection-color: rgb(32, 32, 32); border-radius: 0px;}"
		"QComboBox QAbstractItemView {background-color: rgb(255, 255, 255); color:rgb(81, 81, 81);selection-background-color:rgb(161, 161, 161); selection-color:rgb(255, 255, 255);}"
		"QComboBox QAbstractItemView::item {height:23px;}"
		"QComboBox QAbstractItemView::item:focus {selection-background-color: rgb(94, 94, 94);}"
		"QComboBox QAbstractItemView::item:selected {background-color: rgb(161, 161, 161); color:rgb(255, 255, 255);}"
	);
	strDlgStyle += "QComboBox::drop-down {image: url(" + getImagePath("common/icon_selectlist.png") + ");}";
	strDlgStyle += getVScrollBarStyle();
	setStyleSheet(strDlgStyle);

	QString strTitleBarStyle = QString::fromLocal8Bit(
		"QWidget{background-color: rgb(48,48,48); color:rgb(204, 204, 204); font-family:\"΢���ź�\"; border:0px solid rgb(76, 76, 76);}"
		"QLabel{font:normal 16px; font-family:\"΢���ź�\";}"
	);
	ui.title_bar->setStyleSheet(strTitleBarStyle);

	//
	QString strLineStyle = QString::fromLocal8Bit("background-color:rgb(59, 59, 59); border-top:1px solid rgb(38, 38, 38); border-bottom:1px solid rgb(48, 48, 48);");
	ui.line->setStyleSheet(strLineStyle);
}

void OvrConfigSegRangeDlg::preset_connects()
{
}
