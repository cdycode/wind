#pragma once

#include <QDialog>
#include "ui_ovr_ambient_color_dlg.h"

class OvrAmbientColorDlg : public QDialog
{
	Q_OBJECT

public:
	OvrAmbientColorDlg(QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	~OvrAmbientColorDlg();

private:
	void init_curstom_ui();
	void set_custom_style_sheet();
	void preset_connects();

protected:

	//virtual void done(int) override;
	virtual void  mouseDoubleClickEvent(QMouseEvent *event) override;
	void closeEvent(QCloseEvent * event) override;


public slots:
	void on_button_cancel_clicked();
	void on_button_ok_clicked();
	void on_lineEdit_r_editingFinished();
	void on_lineEdit_g_editingFinished();
	void on_lineEdit_b_editingFinished();
	void on_horizontalSlider_I_valueChanged(int value);
	void on_lineEdit_I_textEdited(const QString &text);

	void setColor(const QColor &color);

private:
	//设置颜色样本的颜色
	void	setSampleColor(float a_red, float a_green, float a_blue);

	//设置文本的值 和颜色样本值
	void	setCurrentColor(float a_red, float a_green, float a_blue);

	//文本编辑完毕 需要进行更新
	void	updateColor();

private:
	Ui::OvrColorSelectDlg ui;

	float		current_color_red = 0.0f;
	float		current_color_green = 0.0f;
	float		current_color_blue = 0.0f;
	float		current_intensity = 0.0f;
};
