#pragma once

#include <QDialog>
#include "ui_ovr_text_setting_dlg.h"

class OvrTextSettingDlg : public QDialog
{
	Q_OBJECT

public:
	OvrTextSettingDlg(QWidget *parent = Q_NULLPTR);
	~OvrTextSettingDlg();

	void setValue(const OvrString& a_text, float a_value);
	const OvrString& getValueText() { return value_text; }
	float getValueTime() { return value_time; }

public slots:
	void on_button_cancel_clicked();
	void on_button_ok_clicked();

private:
	void init_curstom_ui();

	Ui::OvrTextSettingDlg ui;

	OvrString value_text;
	float value_time = 0;
};
