#include "Header.h"
#include "../utils_ui/title_bar.h"
#include "ovr_jump_clip_dlg.h"

OvrJumpClipDlg::OvrJumpClipDlg(const QString& a_seq_id, ovr::uint64 a_makr_key, QWidget *parent)
	: QDialog(parent)
	, sequence_id(a_seq_id)
	, mark_key(a_makr_key)
{
	ui.setupUi(this);

	init_curstom_ui();
	set_custom_style_sheet();
	preset_connects();

	setWindowTitle(QString::fromLocal8Bit("ѡ����ת��"));

	fillClip();
	fillMark();

	//QListWidget::currentItemChanged
	connect(ui.seq_list, &QListWidget::currentItemChanged, this, &OvrJumpClipDlg::clipItemChanged, Qt::QueuedConnection);
	connect(ui.mark_list, &QListWidget::currentItemChanged, this, &OvrJumpClipDlg::markItemChanged, Qt::QueuedConnection);

	//connect(ui.seq_list, &QListWidget::itemClicked, this, &OvrJumpClipDlg::clipItemChanged, Qt::QueuedConnection);
	//connect(ui.mark_list, &QListWidget::itemClicked, this, &OvrJumpClipDlg::markItemChanged, Qt::QueuedConnection);

	updateClipMark();
}

OvrJumpClipDlg::~OvrJumpClipDlg()
{
}

void OvrJumpClipDlg::fillClip()
{
	const std::list<OvrString>& id_list = ovr_project::OvrProject::GetIns()->GetCurrentScene()->GetSequenceNameList();
	ovr_project::OvrAssetManager* asset_manager = ovr_project::OvrProject::GetIns()->GetAssetManager();
	
	QListWidgetItem* current_item = nullptr;
	
	for (auto seq_id : id_list)
	{
		OvrString asset_name, asset_path;
		bool ret = asset_manager->GetSequenceFilePath(seq_id, asset_path);
		assert(ret);

		ret = asset_manager->GetAssetDisplayName(asset_path, asset_name);
		assert(ret);

		QString id = seq_id.GetStringConst();
		QListWidgetItem* list_item = new QListWidgetItem(QString::fromLocal8Bit(asset_name.GetStringConst()));
		list_item->setData(Qt::UserRole, id);
		ui.seq_list->addItem(list_item);

		if (current_item == nullptr && id == sequence_id)
		{
			current_item = list_item;
		}
	}

	if (current_item != nullptr)
	{
		ui.seq_list->setCurrentItem(current_item);
	}
}

void OvrJumpClipDlg::fillMark()
{
	ovr_core::OvrSequence* current_seq = ovr_project::OvrProject::GetIns()->GetCurrentSequence();
	QListWidgetItem* item_selected = nullptr;
	ovr_core::OvrMarkTrack* mark_track = current_seq->GetMarkInfo();
	for (auto item : mark_track->GetKeyFrames())
	{
		QListWidgetItem* list_item = new QListWidgetItem(QString::fromLocal8Bit(item.second.GetStringConst()));
		list_item->setData(Qt::UserRole, item.first);
		ui.mark_list->addItem(list_item);

		if (item_selected == nullptr && mark_key == item.first)
		{
			item_selected = list_item;
		}
	}

	if (item_selected != nullptr)
	{
		ui.mark_list->setCurrentItem(item_selected);
	}
}

void OvrJumpClipDlg::on_button_ok_clicked()
{
	done(1);
}

void OvrJumpClipDlg::on_button_cancel_clicked()
{
	done(0);
}

void OvrJumpClipDlg::on_button_reset_clicked()
{
	mark_key = -1;
	sequence_id = "";

	ui.clip_sel->setText("");

	done(2);
}

void OvrJumpClipDlg::clipItemChanged(QListWidgetItem *current, QListWidgetItem*)
{
	if (current != nullptr)
	{
		sequence_id = current->data(Qt::UserRole).toString();
		updateClipMark();
	}
}

void OvrJumpClipDlg::markItemChanged(QListWidgetItem *current, QListWidgetItem*)
{
	if (current != nullptr)
	{
		mark_key = current->data(Qt::UserRole).toInt();
		if (sequence_id.isEmpty())
		{
			ui.seq_list->setCurrentRow(0);
		}
		else
		{
			updateClipMark();
		}
	}
}

void OvrJumpClipDlg::updateClipMark()
{
	QString text;
	if (ui.seq_list->currentItem() != nullptr)
	{
		text = ui.seq_list->currentItem()->text() + ":";
	}

	if (ui.mark_list->currentItem() != nullptr)
	{
		text += ui.mark_list->currentItem()->text();
	}

	//QString value = sequence_id + ":" + (mark_key < 0 ? "" : QString::number(mark_key));
	ui.clip_sel->setText(text);
}

void OvrJumpClipDlg::init_curstom_ui()
{
	setWindowFlags(windowFlags() | Qt::FramelessWindowHint);

	ui.title_bar->setUiFlags(TitleBar::TitleFlag::TF_BTN_CLOSE);
	//ui.title_bar->setTitle(QString::fromLocal8Bit("ѡ����ת��"));

	installEventFilter(ui.title_bar);
}

void OvrJumpClipDlg::set_custom_style_sheet()
{
	QString strDlgStyle = QString::fromLocal8Bit(
		"QWidget:focus{ outline: none;}"
		"QWidget{background-color: rgb(48,48,48);color:rgb(204, 204, 204); font-family:\"΢���ź�\";}"
		
		"QLabel{font-size:14px; font-family:\"΢���ź�\";}"
		
		"QLineEdit{height:26px; border-radius:2px; background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); border-radius:2px; font-size:12px; selection-background-color:rgb(165, 165, 165); selection-color: rgb(32, 32, 32);}"
		
		"QPushButton{width:98px; height: 30px; color:rgb(205, 205, 205); border:1px solid rgb(30, 30, 30); border-radius:4px; background-color:qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); }"
		"QPushButton:hover{background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(153, 153, 153), stop:1 rgb(109, 109, 109)); }"
		"QPushButton:pressed{background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:1 rgb(118, 118, 118), stop:0 rgb(87, 87, 87)); }"
		"QPushButton:disabled{background-color:rgb(56, 56, 56); color:rgb(76, 76, 76);  border: 1px solid rgb(76, 76, 76); }"

		"QListWidget{color:rgb(204, 204, 204); background-color:rgb(39, 39, 39); border:1px solid rgb(30, 30, 30);}"
		"QListWidget::item {border:1px solid rgb(30, 30, 30); height:28px; margin: 5px;}"
		"QListWidget::item:hover{background-color:rgb(53, 53, 53); color:rgb(204, 204, 204);}"
		"QListWidget::item:focus{selection-background-color:rgb(212, 212, 212);}"
		"QListWidget::item:selected{background-color:rgb(83, 83, 83); selection-color:rgb(204, 204, 204);}"

		"QMessageBox{color: rgb(204, 204, 204); background-color: rgba(48, 48, 48); border-radius: 2px; messagebox-text-interaction-flags: 5 }"
		"QMessageBox QPushButton{border: 1px solid rgb(30, 30, 30); border-radius: 2px; width: 70px; height: 25px; background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgba(118, 118, 118, 255), stop:1 rgba(87, 87, 87, 255));}"
	);
	this->setStyleSheet(strDlgStyle);

	QString strTitleBarStyle = QString::fromLocal8Bit(
		"QWidget{background-color: rgb(48,48,48); color:rgb(204, 204, 204); font-family:\"΢���ź�\"; border:0px solid rgb(76, 76, 76);}"
		"QLabel{font:normal 16px; font-family:\"΢���ź�\";}"
	);
	ui.title_bar->setStyleSheet(strTitleBarStyle);

	//
	QString strLineStyle = QString::fromLocal8Bit("background-color:rgb(59, 59, 59); border-top:1px solid rgb(38, 38, 38); border-bottom:1px solid rgb(48, 48, 48);");
	ui.line->setStyleSheet(strLineStyle);
}

void OvrJumpClipDlg::preset_connects()
{
}
