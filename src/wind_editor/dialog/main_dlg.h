#pragma once

#include "ui_main_dlg.h"

class MainDlg : public QDialog
{
	Q_OBJECT
public:
	MainDlg(QWidget *parent = Q_NULLPTR);
	~MainDlg();

	QString prj_file_path;

	QString get_project_name();

public slots:
	void on_new_btn_clicked();
	void on_open_btn_clicked();
	void on_ok_btn_clicked();
	void on_clear_history_clicked();
	void on_prj_recent_itemDoubleClicked(QListWidgetItem *item);

	void on_exit_dlg_clicked();

protected:
	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void changeEvent(QEvent *event);

private:
	void init_curstom_ui();
	void set_custom_style_sheet();
	void preset_connects();

	bool is_project_exist(QString strPath);

	QString get_project_name(const QString& prj_path);
	
private:
	QPoint m_last;
	bool m_bIsPushed;

private:
	Ui::MainDlg ui;
};