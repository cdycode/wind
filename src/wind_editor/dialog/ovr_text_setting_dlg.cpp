#include "Header.h"
#include "ovr_text_setting_dlg.h"

OvrTextSettingDlg::OvrTextSettingDlg(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);

	QString strDlgStyle = QString::fromLocal8Bit(
		"QWidget:focus{ outline: none;}"
		"QWidget{background-color: rgb(48,48,48);color:rgb(204, 204, 204); font-family:\"微软雅黑\";}"
		"QLable{font-size:14px;}"
		"QLineEdit{height: 26px; border-radius: 2px; background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); border-radius:2px; font-size:12px; selection-background-color: rgb(165, 165, 165); selection-color: rgb(32, 32, 32);}"
		"QPushButton{height: 38px; color: rgb(205, 205, 205); border: 1px solid rgb(30, 30, 30); border-radius: 4px; background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); }"
		"QPushButton:hover{background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(153, 153, 153), stop:1 rgb(109, 109, 109)); }"
		"QPushButton:pressed {background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:1 rgb(118, 118, 118), stop:0 rgb(87, 87, 87)); }"
		"QPushButton:disabled {background-color:rgb(56, 56, 56); color:rgb(76, 76, 76);  border: 1px solid rgb(76, 76, 76); }"

		"QMessageBox{color:rgb(204, 204, 204); background-color:rgb(48, 48, 48); border-radius:2px; messagebox-text-interaction-flags:5; }"
		"QMessageBox QLabel{color:rgb(204, 204, 204); background-color:rgb(48, 48, 48); selection-background-color:rgb(204, 204, 204);selection-color:rgb(48, 48, 48);; messagebox-text-interaction-flags:5 }"
		"QMessageBox QPushButton{width:70px; height:25px; color:rgb(204, 204, 204); background-color:qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); border-radius: 2px;}"
		"QMessageBox QPushButton:hover{background-color:rgb(118, 118, 118);}"
		"QMessageBox QPushButton:pressed{background-color:rgb(87, 87, 87);}"
		"QMessageBox QPushButton:disabled{background-color:rgb(67, 67, 67); color:rgb(140, 140, 140)}"
	);
	setStyleSheet(strDlgStyle);

	QString strTitleBarStyle = QString::fromLocal8Bit(
		"QWidget{background-color: rgb(48,48,48); color:rgb(204, 204, 204); font-family:\"微软雅黑\"; border:0px solid rgb(76, 76, 76);}"
		"QLabel{font:normal 16px; font-family:\"微软雅黑\";}"
	);
	ui.title_bar->setStyleSheet(strTitleBarStyle);

	QString strLineStyle = QString::fromLocal8Bit("background-color:rgb(59, 59, 59); border-top:1px solid rgb(38, 38, 38); border-bottom:1px solid rgb(48, 48, 48);");
	ui.line->setStyleSheet(strLineStyle);

	init_curstom_ui();
}

OvrTextSettingDlg::~OvrTextSettingDlg()
{
}

void OvrTextSettingDlg::init_curstom_ui()
{
	setWindowFlags(windowFlags() | Qt::FramelessWindowHint);

	ui.title_bar->setUiFlags(TitleBar::TitleFlag::TF_BTN_CLOSE);
	ui.title_bar->setTitle(QString::fromLocal8Bit("文本设置"));

	installEventFilter(ui.title_bar);
	QString cs_reg_exp_filter = QString::fromLocal8Bit("^[+\\-]{0,1}\\d{1,}\\.{0,1}\\d*$");
	QRegExp regExpPos = QRegExp(cs_reg_exp_filter);
	QRegExpValidator *posRegExpValidator = new QRegExpValidator(regExpPos);
	ui.show_time->setInputMethodHints(Qt::ImhDigitsOnly);
	ui.show_time->setMaxLength(100);
	ui.show_time->setValidator(posRegExpValidator);

	ui.button_ok->setDefault(true);
}

void OvrTextSettingDlg::setValue(const OvrString& a_text, float a_value)
{
	ui.text_value->setText(QString::fromLocal8Bit(a_text.GetStringConst()));
	ui.show_time->setText(QString::number(a_value));
}

void OvrTextSettingDlg::on_button_cancel_clicked()
{
	done(QDialog::Rejected);
}

void OvrTextSettingDlg::on_button_ok_clicked()
{
	value_text = ui.text_value->toPlainText().toLocal8Bit().constData();
	if (ui.show_time->text().isEmpty())
	{
		OvrHelper::getInst()->warning("时间不能为空！", "文本设置");
		return;
	}

	if (ui.show_time->text().toFloat() < 0)
	{
		OvrHelper::getInst()->warning("时间必须为大于零的数！", "文本设置");
		return;
	}

	value_time = ui.show_time->text().toFloat();

	done(QDialog::Accepted);
}

