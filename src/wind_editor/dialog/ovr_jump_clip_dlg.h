#pragma once

#include <QDialog>
#include "ui_ovr_jump_clip_dlg.h"

class OvrJumpClipDlg : public QDialog
{
	Q_OBJECT
public:
	OvrJumpClipDlg(const QString& a_seq_id, ovr::uint64 a_makr_key, QWidget *parent = Q_NULLPTR);
	~OvrJumpClipDlg();

	QString sequence_id;
	int mark_key;

protected slots:
	void on_button_ok_clicked();
	void on_button_cancel_clicked();
	void on_button_reset_clicked();
	void clipItemChanged(QListWidgetItem* current, QListWidgetItem*);
	void markItemChanged(QListWidgetItem* current, QListWidgetItem*);

protected:
	void fillClip();
	void fillMark();
	void updateClipMark();

private:
	void init_curstom_ui();
	void set_custom_style_sheet();
	void preset_connects();

private:
	Ui::OvrJumpClipDlg ui;
};
