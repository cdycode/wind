#include "Header.h"
#include "main_dlg.h"

#include "new_prj_dlg.h"

#define kMRUPrjPath (Qt::UserRole + 1)

//////////////////////////////////////////////////////
MainDlg::MainDlg(QWidget *parent)
	: QDialog(parent)
	, m_bIsPushed(false)
{
	ui.setupUi(this);

	set_custom_style_sheet();
	init_curstom_ui();
	preset_connects();
}

MainDlg::~MainDlg()
{
}

void MainDlg::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		m_last = event->globalPos();
		m_bIsPushed = true;
	}
}

void MainDlg::mouseMoveEvent(QMouseEvent *event)
{
	if (event->buttons() & Qt::LeftButton) {
		if (!m_bIsPushed) 
		{
			return;
		}
		int dx = event->globalX() - m_last.x();
		int dy = event->globalY() - m_last.y();
		m_last = event->globalPos();
		move(x() + dx, y() + dy);
	}
}
void MainDlg::mouseReleaseEvent(QMouseEvent *event)
{
	m_bIsPushed = false;
}

void MainDlg::changeEvent(QEvent * event)
{
	m_bIsPushed = false;
}

void MainDlg::init_curstom_ui()
{
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint | Qt::FramelessWindowHint);

	QPixmap icon(getImagePath("app_icon.png"));
	ui.labelLogo->setPixmap(icon.scaled(ui.labelLogo->size()));
	const QStringList& prj_list = OvrProjectManager::getInst()->getConfiger()->getMRUList();
	int count = prj_list.count();
	QString prj_path, title;
	for (int i = 0; i < count; ++i)
	{
		prj_path = prj_list.at(i);
		QString prj_path_local = prj_path.replace("/", "\\");
		int iIndex = prj_path_local.lastIndexOf(QString::fromLocal8Bit("\\"));
		QString prj_name = prj_path_local.right(prj_path_local.length() - iIndex - 1);
		QString title = prj_name.split(QString::fromLocal8Bit("."))[0];
		QString itemInfo = QString::fromLocal8Bit("工    程： ")+ title
			+ QString::fromLocal8Bit("\n路    径： ") + prj_path_local;
		ui.prj_recent->addItem(itemInfo);
		ui.prj_recent->item(i)->setData(kMRUPrjPath, prj_list.at(i));
	}

	if (count > 0) {
		ui.prj_recent->setCurrentRow(0);
	}
}

void MainDlg::set_custom_style_sheet()
{
	// 0. Dialog
	QString strDefaultStyle = QString::fromLocal8Bit(
		"QWidget:focus{outline: none;}"
		"QWidget{color:rgb(204, 204, 204); background:rgb(48, 48, 48); font: 12px \"微软雅黑\"; border-radius:4px; selection-background-color: rgb(165, 165, 165); selection-color: rgb(32, 32, 32);}"

		"QPushButton{width: 138px; height: 38px; color: rgb(205, 205, 205); border: 1px solid rgb(30, 30, 30); border-radius: 4px; background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); }"
		"QPushButton:hover{background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(153, 153, 153), stop:1 rgb(109, 109, 109)); }"
		"QPushButton:pressed {background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:1 rgb(118, 118, 118), stop:0 rgb(87, 87, 87)); }"
		"QPushButton:disabled {background-color:rgb(56, 56, 56); color:rgb(76, 76, 76);  border: 1px solid rgb(76, 76, 76); }"

		"QMessageBox {color: rgb(205, 205, 205); background-color: rgba(48, 48, 48); border-radius: 2px; messagebox-text-interaction-flags: 5 }"
		"QMessageBox QPushButton {width: 70px; height: 25px;}"
	
		// 垂直滚动条
		"QScrollBar:vertical{background-color:rgb(35, 35, 35);width:16px; margin:16px 0 16px 0; border:0px solid rgb(135, 135, 35);}"
		"QScrollBar::handle:vertical{background:rgb(119, 119, 119); border: 2px solid rgb(35, 35, 35); background-clip: border; border-radius:7px; min-height: 20px;}"
		"QScrollBar::handle:vertical:hover{background:rgb(129, 129, 129);}"

		"QScrollBar::sub-line:vertical {background:rgb(35, 35, 35); height: 16px; subcontrol-position: top;subcontrol-origin: margin;}"
		"QScrollBar::sub-line:vertical:hover {background:rgba(135, 135, 135, 50); border-top-left-radius:4px; border-top-right-radius:4px; height: 15px; subcontrol-position: top;subcontrol-origin: margin;}"
		"QScrollBar::sub-line:vertical:pressed {background:rgba(135, 135, 135, 30); border-top-left-radius:4px; border-top-right-radius:4px; height: 15px; subcontrol-position: top;subcontrol-origin: margin;}"

		"QScrollBar::add-line:vertical {background:rgb(35, 35, 35); height: 16px; subcontrol-position: bottom;subcontrol-origin: margin;}"
		"QScrollBar::add-line:vertical:hover {background:rgba(135, 135, 135, 50); border-bottom-left-radius:4px; border-bottom-right-radius:4px; height: 15px; subcontrol-position: bottom;subcontrol-origin: margin;}"
		"QScrollBar::add-line:vertical:pressed {background:rgba(135, 135, 135, 30); border-bottom-left-radius:4px; border-bottom-right-radius:4px; height: 15px; subcontrol-position: bottom;subcontrol-origin: margin;}"

		"QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {background:rgb(35, 35, 35);}"
	
		// 水平滚动条
		"QScrollBar:horizontal {background-color:rgb(35, 35, 35); height:16px; margin:0 16px 0 16px; border:0px solid rgb(135, 135, 35);}"
		"QScrollBar::handle:horizontal{background:rgb(119, 119, 119); border: 2px solid rgb(35, 35, 35); background-clip: border; border-radius:7px; min-width: 20px;}"
		"QScrollBar::handle:horizontal:hover{background:rgb(129, 129, 129);}"

		"QScrollBar::sub-line:horizontal {background:rgb(35, 35, 35); width:16px; subcontrol-position:left; subcontrol-origin:margin;}"
		"QScrollBar::sub-line:horizontal:hover {background:rgba(135, 135, 135, 50); border-top-left-radius:4px; border-bottom-left-radius:4px; width:15px; subcontrol-position: left;subcontrol-origin: margin;}"
		"QScrollBar::sub-line:horizontal:pressed {background:rgba(135, 135, 135, 30); border-top-left-radius:4px; border-bottom-left-radius:4px; width:15px; subcontrol-position: left;subcontrol-origin: margin;}"

		"QScrollBar::add-line:horizontal {background:rgb(35, 35, 35); width: 16px; subcontrol-position: right;subcontrol-origin: margin;}"
		"QScrollBar::add-line:horizontal:hover {background:rgba(135, 135, 135, 50); border-bottom-right-radius:4px; border-top-right-radius:4px; width: 15px; subcontrol-position: right; subcontrol-origin: margin;}"
		"QScrollBar::add-line:horizontal:pressed {background:rgba(135, 135, 135, 30); border-bottom-right-radius:4px; border-top-right-radius:4px; width: 15px; subcontrol-position: right; subcontrol-origin: margin;}"

		"QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal {background:rgb(35, 35, 35);}"
	
	);

	QTextStream qtsTemp(&strDefaultStyle);
	qtsTemp << "QScrollBar::up-arrow{image:url(" << getImagePath("icon_scroll_top.png") << ");}"
		<< "QScrollBar::down-arrow{image:url(" << getImagePath("icon_scroll_bottom.png") << ");}"
		<< "QScrollBar::left-arrow{image:url(" << getImagePath("icon_scroll_left.png") << "); }"
		<< "QScrollBar::right-arrow{image:url(" << getImagePath("icon_scroll_right.png") << "); }"
		;

	this->setStyleSheet(strDefaultStyle);

	// 1. title 
	QString strTitleStyle = QString::fromLocal8Bit("color: rgb(255, 255, 255);font-size: 16pt; font-weight:bold;");
	ui.labelWindowTitle->setStyleSheet(strTitleStyle);

	// 关闭按钮
	QString strbtnCloseStyle = QString::fromLocal8Bit(
		"QPushButton{max-height:40px; width:40px; height:30px; background:rgb(48, 48, 48); border:0px solid rgb(30, 30, 30); border-radius: 0px; }"
		"QPushButton:hover{background:rgb(243, 45, 36);}"
		"QPushButton:pressed{background:rgb(221, 28, 19);}"
	);
	QTextStream qtsTempClsBtn(&strbtnCloseStyle);
	qtsTempClsBtn << "QPushButton{image:url(" << getImagePath("pop_btn_close.png") << ");}";
	ui.exit_dlg->setStyleSheet(strbtnCloseStyle);

	// 3. line
	QString strLineStyle = QString::fromLocal8Bit("background-color: rgb(56, 56, 56); border:1px solid  rgb(48, 48, 48);");
	ui.line->setStyleSheet(strLineStyle);

	// 4. scroll area
	QString strAreaStyle = QString::fromLocal8Bit(""
		"QListWidget {color: rgb(204, 204, 204); background-color: rgb(39, 39, 39); border:1px solid  rgb(30, 30, 30); border-radius: 0px;}"
		"QListWidget::item {height:68px; padding-left: 18px; padding-right: 18px; border-bottom:1px solid rgba(151,151,151, 38); background-clip: border; border-bottom-left-radius: 18px, 1px; border-bottom-right-radius: 18px, 1px;}"
		"QListWidget::item:hover {background-color: rgb(40, 40, 40); color: rgb(205, 205, 205);}"
		"QListWidget::item:selected {background-color: rgb(43, 43, 43); selection-color: rgb(205, 205, 205);}"
	);

	ui.prj_recent->setStyleSheet(strAreaStyle);
}

void MainDlg::preset_connects()
{
}

bool MainDlg::is_project_exist(QString strPath)
{
	if (strPath.isEmpty())
		return false;

	QFile checkFile(strPath);
	return checkFile.exists();
}

QString MainDlg::get_project_name()
{
	return get_project_name(prj_file_path);
}

QString MainDlg::get_project_name(const QString & prj_path)
{
	QString prj_path_tmp = prj_path;
	QString prj_path_local = prj_path_tmp.replace("/", "\\");
	int iIndex = prj_path_local.lastIndexOf(QString::fromLocal8Bit("\\"));
	QString prj_name = prj_path_local.right(prj_path_local.length() - iIndex - 1);
	QString prj_file_name = prj_name.split(QString::fromLocal8Bit("."))[0];

	return prj_file_name;
}

void MainDlg::on_new_btn_clicked()
{
	qDebug("on_new_btn_clicked");
	
	NewPrjDlg dlg(this);
	int ret = dlg.exec();
	if (ret == 1)
	{
		prj_file_path = dlg.prj_file_path;

		done(2);
	}
}

void MainDlg::on_open_btn_clicked()
{
	QString dir = OvrProjectManager::getInst()->getConfiger()->getLastOpenPrjDir();
	prj_file_path = QFileDialog::getOpenFileName(nullptr, QString::fromLocal8Bit("打开工程"), dir, QString::fromLocal8Bit("project file(*.oproject)"));
	if (!prj_file_path.isEmpty())
	{
		int index = prj_file_path.lastIndexOf("/");
		assert(index > 0);
		dir = prj_file_path.left(index);
		OvrProjectManager::getInst()->getConfiger()->setLastOpenPrjDir(dir);
		done(1);
	}
}

void MainDlg::on_ok_btn_clicked()
{
	qDebug("on_ok_btn_clicked");
	QListWidgetItem *item = ui.prj_recent->currentItem();

	on_prj_recent_itemDoubleClicked(item);
}

void MainDlg::on_prj_recent_itemDoubleClicked(QListWidgetItem *item)
{
	qDebug("on_prj_rencent_itemDoubleClicked");
	//assert(item != nullptr);

	if (!item)
		return;

	prj_file_path = item->data(kMRUPrjPath).toString();

	if (!is_project_exist(prj_file_path)) {
		QMessageBox::warning(this, this->windowTitle(), QString::fromLocal8Bit("工程文件不存在！"));
		ui.prj_recent->removeItemWidget(item);
		delete item;
		OvrProjectManager::getInst()->getConfiger()->removeMRUFile(prj_file_path);
		return;
	}

	done(1);
}

void MainDlg::on_clear_history_clicked()
{
	if (ui.prj_recent->count() > 0)
	{
		OvrProjectManager::getInst()->getConfiger()->clearMRUList();
		ui.prj_recent->clear();
	}
}
void MainDlg::on_exit_dlg_clicked()
{
	done(0);
}
