#pragma once

#include <QDialog>
#include <QColorDialog>
#include "ui_ovr_common_color_dlg.h"

class OvrCommonColorDlg : public QDialog
{
	Q_OBJECT

public:
	OvrCommonColorDlg(QColor color = QColor(), QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	~OvrCommonColorDlg();

private:
	void init_curstom_ui();
	void set_custom_style_sheet();
	void preset_connects();

protected:

	//virtual void done(int) override;
	virtual void  mouseDoubleClickEvent(QMouseEvent *event) override;
	void closeEvent(QCloseEvent * event) override;

Q_SIGNALS:
	void color_changed(float, float, float, float);
	//void color_changed(QColor);

public slots:
	void on_button_cancel_clicked();
	void on_button_ok_clicked();
	void on_lineEdit_r_editingFinished();
	void on_lineEdit_g_editingFinished();
	void on_lineEdit_b_editingFinished();
	void on_lineEdit_a_editingFinished();
	void on_horizontalSlider_a_valueChanged(int value);
	//void on_lineEdit_a_textEdited(const QString &text);

	void setColor(const QColor &color);

private:
	//设置颜色样本的颜色
	void	setSampleColor(float a_red, float a_green, float a_blue);

	//设置文本的值 和颜色样本值
	void	setCurrentColor(float a_red, float a_green, float a_blue);

	//文本编辑完毕 需要进行更新
	void	updateColor();

	void check_color_ui(QLineEdit * line_edit);

private:
	Ui::OvrCommonColortDlg ui;

	float source_color_red;
	float source_color_green;
	float source_color_blue;
	float source_transparency;
};
