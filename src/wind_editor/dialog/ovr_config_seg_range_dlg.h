#pragma once

#include <QDialog>
#include "ui_ovr_config_seg_range_dlg.h"

class OvrConfigSegRangeDlg : public QDialog
{
	Q_OBJECT

public:
	OvrConfigSegRangeDlg(int& a_secs, int& a_fps, QWidget *parent = Q_NULLPTR);
	~OvrConfigSegRangeDlg();

	int getFps();
	int getSecs();

public slots:
	void on_button_ok_clicked();
	void on_button_cancel_clicked();

private:
	void init_curstom_ui();
	void set_custom_style_sheet();
	void preset_connects();


private:
	Ui::OvrConfigSegRangeDlg ui;
};
