#pragma once

#include <QtWidgets/QDialog>
#include "ui_ovr_file_dialog.h"

class QFileSystemModel;
class QStandardItemModel;
class QStandardItem;

class OvrFileDialog : public QDialog
{
	Q_OBJECT

public:
	OvrFileDialog(QWidget *parent = Q_NULLPTR);
	~OvrFileDialog();

	void setPrjectDir(const QString& a_path);

	bool init(const QString& a_title, const QString& a_filter = "", const QString& a_select_file = "");

	QString getSelectFile();

private:
	void init_custom_ui();
	void set_custom_style_sheet();
	void preset_connects();

protected slots:
	void showListView();
	void showDetailView();
	void onUserSelectSidebar(const QModelIndex &index);
	void onUserSelectSidebar(const QModelIndex &current, const QModelIndex &previous);
	void onUserSelectItem(const QModelIndex &index);
	void onCurrentChanged(const QModelIndex &current, const QModelIndex &previous);
	void toParentDir();
	void fileNameFilterChanged(int index);
	void fileNameTextChanged(const QString& a_text);
	void on_buttonBox_accepted();
	void on_buttonBox_rejected();
	void showDefaultSideDir();

protected:
	void initStyle();
	void initToolBar();
	void initSideBar();
	void initSideBarStyle();
	void initContentView();
	void initListView();
	void initTreeView();
	void initBottomUI();
	bool gotoDir(const QModelIndex& a_index);
	void updateCurrentDir(const QString& a_new_cur_dir);
	void updatefileName(const QString& a_text);
	void updateFilter(const QString& a_text);
	void setCurFile(const QString& a_file_path);
	void keyPressEvent(QKeyEvent *event) override;

	void initSideBarData();
	void fillChildDir(QStandardItem* a_item, QDir& a_dir);
	void initFilters(const QString& a_filters);
	void addFilter(const QString& a_filter);
	bool browseFile(const QString& a_select_file);
	bool gotoFile(QString a_select_file);

	QString getOwnedSideDir(const QString& a_path);
	bool locateSideDir(const QString& a_path);
	bool locateSideDir(QStandardItem* a_item, const QString& a_path);

private:
	Ui::OvrFileDialog ui;

	QFileSystemModel* file_sys_model;
	QStandardItemModel* size_bar_model;

	QString cur_dir;
	QString cur_file;
	QString side_dir;
	QString side_dir_text;
	QString work_dir;
	bool is_custom_filter_set = false;
	QString treeStyle;
	QString scrollBarStyle;
};