#include "Header.h"
#include "ovr_common_dialog.h"

OvrCommonDialog::OvrCommonDialog(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setAttribute(Qt::WA_DeleteOnClose, true);
	initStyle();
}

OvrCommonDialog::~OvrCommonDialog()
{
}

void OvrCommonDialog::initStyle()
{
	ui.title_bar->init();
}

void OvrCommonDialog::setClient(QWidget* a_client)
{
	qDebug() << a_client->sizeHint();
	qDebug() << a_client->size();
	ui.client->setFixedSize(a_client->size());
	a_client->setParent(ui.client);
	setWindowTitle(a_client->windowTitle());
	adjustSize();
}