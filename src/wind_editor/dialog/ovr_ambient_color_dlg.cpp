#include "Header.h"
#include "ovr_ambient_color_dlg.h"
#include <QColorDialog> 

OvrAmbientColorDlg::OvrAmbientColorDlg(QWidget *parent, Qt::WindowFlags f)
	: QDialog(parent, f)
{
	ui.setupUi(this);
	//setWindowFlags((windowFlags() | Qt::FramelessWindowHint) & ~Qt::WindowContextHelpButtonHint);
	//ui.close_button->setIcons(getImagePath("pop_btn_close.png"));
	init_curstom_ui();
	set_custom_style_sheet();
	preset_connects();

	ui.color_show->setStyleSheet("background:rgb(255,0,0)");

	const OvrVector4& ambient_color = ovr_project::OvrProject::GetIns()->GetCurrentScene()->GetScene()->GetAmbientColor();
	
	current_color_red = ambient_color[0];
	current_color_green = ambient_color[1];
	current_color_blue = ambient_color[2];
	current_intensity = ambient_color[3];
	setCurrentColor(ambient_color[0], ambient_color[1], ambient_color[2]);
	ui.horizontalSlider_I->setValue(current_intensity * 100);
}

OvrAmbientColorDlg::~OvrAmbientColorDlg()
{

}


void OvrAmbientColorDlg::init_curstom_ui()
{
	setWindowFlags(windowFlags() | Qt::FramelessWindowHint);

	ui.title_bar->setUiFlags(TitleBar::TitleFlag::TF_BTN_CLOSE);

	QString strTitle = QString::fromLocal8Bit("����������");
	//ui.title_bar->setTitle(strTitle);

	installEventFilter(ui.title_bar);
	setWindowTitle(strTitle);

	QString cs_reg_exp_filter = QString::fromLocal8Bit("^[+\\-]{0,1}\\d{1,}\\.{0,1}\\d*$");
	QRegExp regExpPos = QRegExp(cs_reg_exp_filter);
	QRegExpValidator *posRegExpValidator = new QRegExpValidator(regExpPos);
	ui.lineEdit_r->setInputMethodHints(Qt::ImhDigitsOnly);
	ui.lineEdit_r->setMaxLength(100);
	ui.lineEdit_r->setValidator(posRegExpValidator);

	ui.lineEdit_g->setInputMethodHints(Qt::ImhDigitsOnly);
	ui.lineEdit_g->setMaxLength(100);
	ui.lineEdit_g->setValidator(posRegExpValidator);

	ui.lineEdit_b->setInputMethodHints(Qt::ImhDigitsOnly);
	ui.lineEdit_b->setMaxLength(100);
	ui.lineEdit_b->setValidator(posRegExpValidator);

	//QString regExp = QString("[^\\\\/*:|?\"\\s]*");
	//QRegExp regExpPort = QRegExp(regExp);
	//QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExpPort);
	//ui.key_name->setMaxLength(100);
	//ui.key_name->setValidator(portRegExpValidator);

	ui.button_ok->setDefault(true);
}

void OvrAmbientColorDlg::set_custom_style_sheet()
{
	QString strDlgStyle = QString::fromLocal8Bit(
		"QWidget:focus{ outline: none;}"
		"QWidget{background-color: rgb(48,48,48);color:rgb(204, 204, 204); font-family:\"΢���ź�\";}"
		"QLabel{font-size:14px;}"
		"QLineEdit{height: 26px; border-radius: 2px; background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); border-radius:2px; font-size:12px; selection-background-color: rgb(165, 165, 165); selection-color: rgb(32, 32, 32);}"
		"QPushButton{width: 138px; height: 38px; color: rgb(205, 205, 205); border: 1px solid rgb(30, 30, 30); border-radius: 4px; background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); }"
		"QPushButton:hover{background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(153, 153, 153), stop:1 rgb(109, 109, 109)); }"
		"QPushButton:pressed {background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:1 rgb(118, 118, 118), stop:0 rgb(87, 87, 87)); }"
		"QPushButton:disabled {background-color:rgb(56, 56, 56); color:rgb(76, 76, 76);  border: 1px solid rgb(76, 76, 76); }"

		"QMessageBox {color: rgb(204, 204, 204); background-color: rgba(48, 48, 48); border-radius: 2px; messagebox-text-interaction-flags: 5 }"
		"QMessageBox QPushButton {border: 1px solid rgb(30, 30, 30); border-radius: 2px; width: 70px; height: 25px; background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgba(118, 118, 118, 255), stop:1 rgba(87, 87, 87, 255));}"
	);
	this->setStyleSheet(strDlgStyle);

	QString strTitleBarStyle = QString::fromLocal8Bit(
		"QWidget{background-color: rgb(48,48,48); color:rgb(204, 204, 204); font-family:\"΢���ź�\"; border:0px solid rgb(76, 76, 76);}"
		"QLabel{font:normal 16px; font-family:\"΢���ź�\";}"
	);
	ui.title_bar->setStyleSheet(strTitleBarStyle);

	//
	QString strLineStyle = QString::fromLocal8Bit("background-color:rgb(59, 59, 59); border-top:1px solid rgb(38, 38, 38); border-bottom:1px solid rgb(48, 48, 48);");
	ui.line->setStyleSheet(strLineStyle);
}

void OvrAmbientColorDlg::preset_connects()
{
}


void OvrAmbientColorDlg::mouseDoubleClickEvent(QMouseEvent *event) 
{
	if ((childAt(event->pos()) == ui.color_show))
	{
		QColorDialog colorDialog;
		colorDialog.setCurrentColor(QColor(current_color_red * 255, current_color_green * 255, current_color_blue * 255));

		connect(&colorDialog, &QColorDialog::colorSelected, this, &OvrAmbientColorDlg::setColor);
		connect(&colorDialog, &QColorDialog::currentColorChanged, this, &OvrAmbientColorDlg::setColor);

		if (0 == colorDialog.exec()) 
		{
			//ȡ����ǰ������
			setColor(QColor(current_color_red * 255, current_color_green * 255, current_color_blue * 255));
		}
	}
}

void OvrAmbientColorDlg::closeEvent(QCloseEvent *event)
{
	on_button_cancel_clicked();
}

//void OvrAmbientColorDlg::on_close_button_clicked()
//{
//	ovr_project::OvrProject::GetIns()->GetCurrentScene()->GetScene()->SetAmbientColor(OvrVector4(current_color_red, current_color_green, current_color_blue, 1.0f));
//	done(0);
//}

void OvrAmbientColorDlg::on_button_cancel_clicked() 
{
	ovr_project::OvrProject::GetIns()->GetCurrentScene()->GetScene()->SetAmbientColor(OvrVector4(current_color_red, current_color_green, current_color_blue, 1.0f));
	done(0);
}

void OvrAmbientColorDlg::on_button_ok_clicked() 
{
	current_color_red	= ui.lineEdit_r->text().toFloat();
	current_color_green = ui.lineEdit_g->text().toFloat();
	current_color_blue	= ui.lineEdit_b->text().toFloat();
	current_intensity = ui.lineEdit_I->text().toFloat();

	done(0);
}

void OvrAmbientColorDlg::on_lineEdit_r_editingFinished()
{
	updateColor();
}

void OvrAmbientColorDlg::on_lineEdit_g_editingFinished()
{
	updateColor();
}

void OvrAmbientColorDlg::on_lineEdit_b_editingFinished()
{
	updateColor();
}
void OvrAmbientColorDlg::on_horizontalSlider_I_valueChanged(int value)
{
	float new_value = value / 100.0f;
	ui.lineEdit_I->setText(QString("%1").arg(new_value));
	updateColor();
}

void OvrAmbientColorDlg::on_lineEdit_I_textEdited(const QString &text) 
{
	float value = text.toFloat();
	ui.horizontalSlider_I->setValue(value * 100);
}

void OvrAmbientColorDlg::setColor(const QColor &color)
{
	float red = color.red() / 255.0f;
	float green = color.green() / 255.0f;
	float blue = color.blue() / 255.0f;
	float intensity = ui.lineEdit_I->text().toFloat();

	setCurrentColor(red, green, blue);

	ovr_project::OvrProject::GetIns()->GetCurrentScene()->GetScene()->SetAmbientColor(OvrVector4(red, green, blue, intensity));
}

void OvrAmbientColorDlg::setSampleColor(float a_red, float a_green, float a_blue) 
{
	QString styleSheet = QString("background:rgb(%1,%2,%3)")
		.arg(a_red * 255).arg(a_green * 255).arg(a_blue * 255);

	ui.color_show->setStyleSheet(styleSheet);
}

void OvrAmbientColorDlg::setCurrentColor(float a_red, float a_green, float a_blue)
{
	setSampleColor(a_red, a_green, a_blue);

	ui.lineEdit_r->setText(QString("%1").arg(a_red));
	ui.lineEdit_g->setText(QString("%1").arg(a_green));
	ui.lineEdit_b->setText(QString("%1").arg(a_blue));
}

void OvrAmbientColorDlg::updateColor() 
{
	float color_red = ui.lineEdit_r->text().toFloat();
	float color_green = ui.lineEdit_g->text().toFloat();
	float color_blue = ui.lineEdit_b->text().toFloat();
	float intensity = ui.lineEdit_I->text().toFloat();

	setSampleColor(color_red, color_green, color_blue);

	ovr_project::OvrProject::GetIns()->GetCurrentScene()->GetScene()->SetAmbientColor(OvrVector4(color_red, color_green, color_blue, intensity));
}



