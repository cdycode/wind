#pragma once

#include <QListView>
#include <QTreeView>

class OvrSelectDlgListView :public QListView
{
	Q_OBJECT
public:
	OvrSelectDlgListView(QWidget *parent = Q_NULLPTR) :QListView(parent) {}

signals:
	void onCurrentChanged(const QModelIndex &current, const QModelIndex &previous);

protected:
	virtual void currentChanged(const QModelIndex &current, const QModelIndex &previous) override;

};

class OvrSelectDlgTreeView :public QTreeView
{
	Q_OBJECT
public:
	OvrSelectDlgTreeView(QWidget *parent = Q_NULLPTR) :QTreeView(parent) {}

signals:
	void onCurrentChanged(const QModelIndex &current, const QModelIndex &previous);

protected:
	virtual void currentChanged(const QModelIndex &current, const QModelIndex &previous) override;

};

#include <QCompleter>
#include <QFileSystemModel>

class OvrFileComplete :public QCompleter
{
	Q_OBJECT

public:
	explicit OvrFileComplete(QFileSystemModel *model, QObject *parent = 0) : QCompleter(model, parent)
	{
		sourceModel = model;
#if defined(Q_OS_WIN)
		setCaseSensitivity(Qt::CaseInsensitive);
#endif
	}

	QString pathFromIndex(const QModelIndex &index) const override;
	QStringList splitPath(const QString& path) const override;

	QFileSystemModel *sourceModel;
};

#include <QStyledItemDelegate>

class OvrSideBarItemDelegate : public QStyledItemDelegate
{
public:
	OvrSideBarItemDelegate(QObject *parent = Q_NULLPTR);
	virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

protected:
	QImage* item_image;
	QSize icon_size;
};