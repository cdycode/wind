#include "Header.h"
#include "ovr_file_dialog_helper.h"

#include <QDebug>

void OvrSelectDlgListView::currentChanged(const QModelIndex &current, const QModelIndex &previous)
{
	QListView::currentChanged(current, previous);

	emit onCurrentChanged(current, previous);
}

void OvrSelectDlgTreeView::currentChanged(const QModelIndex &current, const QModelIndex &previous)
{
	QTreeView::currentChanged(current, previous);

	emit onCurrentChanged(current, previous);
}

QString OvrFileComplete::pathFromIndex(const QModelIndex &index) const
{
	const QFileSystemModel* dirModel = sourceModel;
	QString currentLocation = dirModel->rootPath();
	QString path = index.data(QFileSystemModel::FilePathRole).toString();

	if (!currentLocation.isEmpty() && path.startsWith(currentLocation)) 
	{
#if defined(Q_OS_UNIX)
		if (currentLocation == QDir::separator())
		{
			return path.mid(currentLocation.length());
		}
#endif
		if (currentLocation.endsWith(QLatin1Char('/')))
		{
			return path.mid(currentLocation.length());
		}
		else
		{
			return path.mid(currentLocation.length() + 1);
		}
	}
	return index.data(QFileSystemModel::FilePathRole).toString();
}

QStringList OvrFileComplete::splitPath(const QString &path) const
{
	if (path.isEmpty())
	{
		return QStringList(completionPrefix());
	}

	QString pathCopy = QDir::toNativeSeparators(path);
	QString sep = QDir::separator();

#if defined(Q_OS_WIN)
	if (pathCopy == QLatin1String("\\") || pathCopy == QLatin1String("\\\\"))
	{
		return QStringList(pathCopy);
	}

	QString doubleSlash(QLatin1String("\\\\"));
	if (pathCopy.startsWith(doubleSlash))
	{
		pathCopy = pathCopy.mid(2);
	}
	else
	{
		doubleSlash.clear();
	}

#elif defined(Q_OS_UNIX)
	{
		QString tildeExpanded = qt_tildeExpansion(pathCopy);
		if (tildeExpanded != pathCopy) 
		{
			QFileSystemModel *dirModel = sourceModel;
			dirModel->fetchMore(dirModel->index(tildeExpanded));
		}
		pathCopy = std::move(tildeExpanded);
	}
#endif

	QRegExp re(QLatin1Char('[') + QRegExp::escape(sep) + QLatin1Char(']'));

#if defined(Q_OS_WIN)
	QStringList parts = pathCopy.split(re, QString::SkipEmptyParts);
	if (!doubleSlash.isEmpty() && !parts.isEmpty())
	{
		parts[0].prepend(doubleSlash);
	}
	if (pathCopy.endsWith(sep))
	{
		parts.append(QString());
	}
#else
	QStringList parts = pathCopy.split(re);
	if (pathCopy[0] == sep[0]) // read the "/" at the beginning as the split removed it
	{
		parts[0] = sep[0];
	}
#endif

#if defined(Q_OS_WIN)
	bool startsFromRoot = !parts.isEmpty() && parts[0].endsWith(QLatin1Char(':'));
#else
	bool startsFromRoot = pathCopy[0] == sep[0];
#endif

	if (parts.count() == 1 || (parts.count() > 1 && !startsFromRoot)) 
	{
		const QFileSystemModel *dirModel = sourceModel;
		QString currentLocation = QDir::toNativeSeparators(dirModel->rootPath());

#if defined(Q_OS_WIN)
		if (currentLocation.endsWith(QLatin1Char(':')))
		{
			currentLocation.append(sep);
		}
#endif
		if (currentLocation.contains(sep) && path != currentLocation) 
		{
			QStringList currentLocationList = splitPath(currentLocation);
			while (!currentLocationList.isEmpty()
				&& parts.count() > 0
				&& parts.at(0) == QLatin1String("..")) 
			{
				parts.removeFirst();
				currentLocationList.removeLast();
			}
			if (!currentLocationList.isEmpty() && currentLocationList.constLast().isEmpty())
			{
				currentLocationList.removeLast();
			}
			return currentLocationList + parts;
		}
	}
	return parts;
}

OvrSideBarItemDelegate::OvrSideBarItemDelegate(QObject *parent) :QStyledItemDelegate(parent)
{
	item_image = new QImage(getImagePath("icon_closefile_min.png"));
	icon_size.setWidth(item_image->width());
	icon_size.setHeight(item_image->height());
}


void OvrSideBarItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	assert(index.column() == 0);

	if (index.model()->rowCount(index) == 0)
	{
		QRect rc(option.rect.left() - icon_size.width() - 1,
			(option.rect.height() - icon_size.height()) / 2 + option.rect.top(),
			icon_size.width(), icon_size.height());

		painter->drawImage(rc, *item_image);
	}
	QStyledItemDelegate::paint(painter, option, index);
}
