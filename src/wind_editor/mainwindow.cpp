#include "Header.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QStatusBar>

#include "menu/ovr_file_menu.h"
#include "menu/ovr_tool_menu.h"
#include "menu/ovr_object_menu.h"

#include "res_view/ovr_res_manager.h"
#include "actor_view/ovr_actor_view.h"
#include "tml_view/ovr_tml_main_pane.h"
#include "ovr_scene_editor_ex.h"

#include "utils_ui/main_frame_title_bar.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	initStyle();
	initUI();

	registerEventListener("status");
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::initTitleBar()
{
	MainFrameTitleBar* dockWidget = new MainFrameTitleBar(QString::fromLocal8Bit("�����ڿ�����"));

	auto menu_bar = dockWidget->get_menu_bar();
	new OvrFileMenu(menu_bar);
	new OvrToolMenu(menu_bar);
	new OvrObjectMenu(menu_bar);

	addDockWidget(Qt::TopDockWidgetArea, dockWidget);

	installEventFilter(dockWidget);
}

void MainWindow::initUI()
{
	initTitleBar();

	addDockWidget(Qt::LeftDockWidgetArea, new OvrResManagerDock);

	setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);
	addDockWidget(Qt::RightDockWidgetArea, new OvrActorView);

	addDockWidget(Qt::BottomDockWidgetArea, new OvrTMLMainPane, Qt::Orientation::Horizontal);
	setCentralWidget(new OvrSceneEditorPane);

	statusBar()->setStyleSheet("color:rgb(180,180,180)");
}

void MainWindow::initStyle()
{
	setWindowFlags(windowFlags() | Qt::FramelessWindowHint);

	// 0. main window
	QString strDefaultStyle = QString::fromLocal8Bit(""
		"QMainWindow{outline:0px solid rgb(24, 124, 89); background:rgb(24, 24, 24); color:rgb(204, 204, 204); font-size:12px; font-family:\"΢���ź�\"; /*margin: 2px; border:2px;*/ border-radius:4px;}"
		"QMainWindow::separator{color:rgb(204, 204, 204); background-color:rgb(24, 24, 24); width:2px; height:2px;}"

		"QMenu{background:rgb(39, 39, 39); color:rgb(204, 204, 204); selection-background-color:rgb(60, 60, 60); selection-color: rgb(204, 204, 204);}"
		"QMenuBar{background-color: rgb(39, 39, 39); /*border-top:1px solid rgb(39,39,39); padding: 3px;*/}"
		"QMenuBar::item {color:rgb(204, 204, 204);}"
		"QMenuBar::item:selected {background-color: rgb(60, 60, 60);}"
		"QMenu::item:disabled {color:gray}"

		"QStatusBar{border:none; background:rgb(45, 45, 45); color:rgb(204, 104, 104);}"

		"QDockWidget{border:2px solid rgb(24, 24, 24); background:rgb(36, 36, 36); color:rgb(204, 204, 204);}"
		"QDockWidget::title{background:rgb(24, 24, 24); color:rgb(204, 204, 204); text-align:center; background:#3C3C3C; padding-left:5px;}"

		"QSplitter::handle{background:rgb(24, 24, 24); width:1px; height:1px;}"  // all default splitter

		//"QWidget{background:rgb(48, 48, 48); color:rgb(204, 204, 204); font-family:\"΢���ź�\"; font-size:12px; }"
		"QWidget:focus{outline: none;}"

		//"QHeaderView::section{background-color:qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #616161, stop: 0.5 #505050, stop: 0.6 #434343, stop:1 #656565); color:rgb(204, 204, 204); padding-left:4px; border:1px solid #6c6c6c; }"
		"QHeaderView::section{height:24px; background:rgb(62, 62, 62); color:rgb(204, 204, 204); border:1px solid rgb(24, 24, 24);}"
		//"QTreeView{background-color:rgb(39, 39, 39); color:rgb(192, 192, 192); border:1px solid rgb(24, 24, 24); border-top:0px;}"
		//"QTreeView::item{height:28px; border-bottom:1px solid rgb(39, 39, 39); color:rgb(192, 192, 192);}"
		//"QTreeView::item::hover{background-color: rgb(53, 53, 53);}"
		//"QTreeView::item:selected{background-color: rgb(64, 64, 64);}"

		"QAbstractItemView{background-color:rgb(39, 39, 39); color:rgb(204, 204, 204); border:1px solid rgb(30, 30, 30); border-left:0px;}"
		"QAbstractItemView::item:hover{background-color:rgb(53, 53, 53); }"//color:rgb(204, 204, 204);
		"QAbstractItemView::item:selected{background-color:rgb(83, 83, 83); }"//selection-color:rgb(204, 204, 204);

		// ��ֱ������
		"QScrollBar:vertical{background-color:rgb(35, 35, 35);width:16px; margin:16px 0 16px 0; border:0px solid rgb(35, 35, 35);}"
		"QScrollBar::handle:vertical{background:rgb(119, 119, 119); border: 2px solid rgb(35, 35, 35); background-clip: border; border-radius:7px; min-height: 20px;}"
		"QScrollBar::handle:vertical:hover{background:rgb(129, 129, 129);}"

		"QScrollBar::sub-line:vertical {background:rgb(35, 35, 35); height:16px; subcontrol-position:top; subcontrol-origin:margin;}"
		"QScrollBar::sub-line:vertical:hover {background:rgba(135, 135, 135, 50); border-top-left-radius:4px; border-top-right-radius:4px; height:15px; subcontrol-position:top; subcontrol-origin:margin;}"
		"QScrollBar::sub-line:vertical:pressed {background:rgba(135, 135, 135, 30); border-top-left-radius:4px; border-top-right-radius:4px; height:15px; subcontrol-position:top; subcontrol-origin:margin;}"

		"QScrollBar::add-line:vertical {background:rgb(35, 35, 35); height:16px; subcontrol-position:bottom; subcontrol-origin:margin;}"
		"QScrollBar::add-line:vertical:hover {background:rgba(135, 135, 135, 50); border-bottom-left-radius:4px; border-bottom-right-radius:4px; height:15px; subcontrol-position:bottom; subcontrol-origin:margin;}"
		"QScrollBar::add-line:vertical:pressed {background:rgba(135, 135, 135, 30); border-bottom-left-radius:4px; border-bottom-right-radius:4px; height:15px; subcontrol-position:bottom; subcontrol-origin:margin;}"

		"QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {background:rgb(35, 35, 35);}"

		// ˮƽ������
		"QScrollBar:horizontal {background-color:rgb(35, 35, 35); height:16px; margin:0 16px 0 16px; border:0px solid rgb(35, 35, 35);}"
		"QScrollBar::handle:horizontal{background:rgb(119, 119, 119); border:2px solid rgb(35, 35, 35); background-clip: border; border-radius:7px; min-width:20px;}"
		"QScrollBar::handle:horizontal:hover{background:rgb(129, 129, 129);}"

		"QScrollBar::sub-line:horizontal {background:rgb(35, 35, 35); width:16px; subcontrol-position:left; subcontrol-origin:margin;}"
		"QScrollBar::sub-line:horizontal:hover {background:rgba(135, 135, 135, 50); border-top-left-radius:4px; border-bottom-left-radius:4px; width:15px; subcontrol-position:left; subcontrol-origin:margin;}"
		"QScrollBar::sub-line:horizontal:pressed {background:rgba(135, 135, 135, 30); border-top-left-radius:4px; border-bottom-left-radius:4px; width:15px; subcontrol-position:left; subcontrol-origin:margin;}"

		"QScrollBar::add-line:horizontal {background:rgb(35, 35, 35); width: 16px; subcontrol-position: right;subcontrol-origin: margin;}"
		"QScrollBar::add-line:horizontal:hover {background:rgba(135, 135, 135, 50); border-bottom-right-radius:4px; border-top-right-radius:4px; width:15px; subcontrol-position:right; subcontrol-origin:margin;}"
		"QScrollBar::add-line:horizontal:pressed {background:rgba(135, 135, 135, 30); border-bottom-right-radius:4px; border-top-right-radius:4px; width:15px; subcontrol-position:right; subcontrol-origin:margin;}"

		"QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal {background:rgb(35, 35, 35);}"

		// QLabel
		"QLabel{color:rgb(204, 204, 204);}"

		// QLineEdit
		"QLineEdit{background-color:rgb(62, 62, 62); color:rgb(204, 204, 204); border:1px solid rgb(76, 76, 76); selection-background-color: rgb(165, 165, 165); selection-color: rgb(32, 32, 32);}"

		"QSpinBox{background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); font-size:12px; selection-background-color:rgb(165, 165, 165); selection-color:rgb(32, 32, 32); border-radius: 0px;}"
		"QDoubleSpinBox{background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); font-size:12px; selection-background-color:rgb(165, 165, 165); selection-color:rgb(32, 32, 32); border-radius: 0px;}"

		// QPushButton
		"QPushButton{background-color:qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87));}"
		"QPushButton:hover{background-color:rgb(118, 118, 118);}"
		"QPushButton:pressed{background-color:rgb(87, 87, 87);}"
		"QPushButton:disabled{background-color:rgb(67, 67, 67); color:rgb(140, 140, 140)}"

		// QToolBar
		"QToolBar{background-color:rgb(39, 39, 39); color:rgb(204, 204, 204);}"
		"QToolBar QToolButton{background:rgb(39, 39, 39); color:rgb(204, 204, 204);}"
		"QToolBar QToolButton:hover{background-color:rgb(83, 83, 83);}"
		"QToolBar QToolButton:checked{background-color:rgb(83, 83, 83);}"
		"QToolBar QLabel{background-color:rgb(39, 39, 39);}"

		// QComboBox
		"QComboBox{border:1px solid rgb(76,76,76); background:rgb(87,87,87); color:rgb(204,204,204); selection-background-color:rgb(165, 165, 165); selection-color:rgb(32, 32, 32);}"
		"QComboBox QAbstractItemView {border:0px; background-color:rgb(255, 255, 255); color:rgb(81, 81, 81); selection-background-color:rgb(161, 161, 161); selection-color:rgb(255, 255, 255);}"
		"QComboBox QAbstractItemView::item {background:rgb(255, 255, 255); color:rgb(81, 81, 81); height:23px;}"
		"QComboBox QAbstractItemView::item:focus {selection-background-color:rgb(94, 94, 94);}"
		"QComboBox QAbstractItemView::item:selected {background-color: rgb(161, 161, 161); color:rgb(255, 255, 255);}"

		// QSlider  horizontal
		"QSlider::groove:horizontal{border:0px solid #424242; background:#424242; height:3px; border-radius:1px; padding-left:-1px; padding-right:-1px; margin-left:1px; margin-right:1px;}"
		"QSlider::sub-page:horizontal{background:#F5A623; border:1px solid #F5A623; height:10px; border-radius:2px;}"
		"QSlider::add-page:horizontal{background:#575757; border:0px solid #777; height:10px; border-radius:2px;}"
		"QSlider::handle:horizontal{background-color:rgba(255, 255, 255, 255); width: 10px; margin-top:-4px; margin-bottom:-4px; border-radius:5px;}"
		"QSlider::handle:horizontal:hover{background:rgba(255, 255, 110, 255); width: 10px; margin-top:-4px; margin-bottom:-4px; border-radius:5px;}"
		"QSlider::handle:horizontal:pressed{background:rgba(255, 255, 255, 255); width: 10px; margin-top:-4px; margin-bottom:-4px; border-radius:5px;}"
		"QSlider::groove:horizontal:disabled{border:0px solid #323232; background:#808080; height:3px; border-radius:1px; padding-left:-1px; padding-right:-1px;}"
		"QSlider::sub-page:horizontal:disabled{background:#00009C; border-color:#999;}"
		"QSlider::add-page:horizontal:disabled{background:#eee; border-color: #999;}"
		"QSlider::handle:horizontal:disabled{background:rgba(80, 80, 80, 255); width:12px; margin-top:-5px; margin-bottom:-5px; border-radius:5px;}"

		// QMessageBox
		//"QMessageBox{background-color:qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); }"
		"QMessageBox{color:rgb(204, 204, 204); background-color:rgb(48, 48, 48); border-radius:2px; messagebox-text-interaction-flags:5; }"
		"QMessageBox QLabel{color:rgb(204, 204, 204); background-color:rgb(48, 48, 48); selection-background-color:rgb(204, 204, 204);selection-color:rgb(48, 48, 48); messagebox-text-interaction-flags:5 }"
		"QMessageBox QPushButton{width:70px; height:25px; color:rgb(204, 204, 204); background-color:qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); border-radius: 2px;}"
		"QMessageBox QPushButton:hover{background-color:rgb(118, 118, 118);}"
		"QMessageBox QPushButton:pressed{background-color:rgb(87, 87, 87);}"
		"QMessageBox QPushButton:disabled{background-color:rgb(67, 67, 67); color:rgb(140, 140, 140)}"
	);

	QTextStream qtsTemp(&strDefaultStyle);
	qtsTemp << "QScrollBar::up-arrow{image:url(" << getImagePath("icon_scroll_top.png") << ");}"
		<< "QScrollBar::down-arrow{image:url(" << getImagePath("icon_scroll_bottom.png") << ");}"
		<< "QScrollBar::left-arrow{image:url(" << getImagePath("icon_scroll_left.png") << "); }"
		<< "QScrollBar::right-arrow{image:url(" << getImagePath("icon_scroll_right.png") << "); }"
		<< "QComboBox::drop-down{image:url(" << getImagePath("icon_selectlist.png") << "); }"
		;

	setStyleSheet(strDefaultStyle);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	if (OvrProjectManager::getInst()->isNeedSave())
	{
		int result = OvrHelper::getInst()->warningSave();
		if (QMessageBox::Save == result)
		{
			OvrProjectManager::getInst()->saveProject();
		}
		else if (QMessageBox::Cancel == result)
		{
			event->ignore();
			return;
		}
	}

	QMainWindow::closeEvent(event);

	if (event->isAccepted())
	{
		notifyEvent("mainwnd_close", 0, 0);
	}
}

void MainWindow::showEvent(QShowEvent * event)
{
	setAttribute(Qt::WA_Mapped);
	QMainWindow::showEvent(event);
}

void MainWindow::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{
	assert(a_event_name == "status");

	a_param1.toString();
	a_param2.toInt();
	statusBar()->showMessage(a_param1.toString(), a_param2.toInt());
}