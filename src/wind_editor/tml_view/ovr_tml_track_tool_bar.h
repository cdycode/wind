#pragma once

#include <QMenu>

class OvrTMLAddMenu;

//ʱ��������ߵ�widget
class OvrTMLTrackToolBar :public QWidget, public OvrEventSender
{
	Q_OBJECT
public:
	explicit OvrTMLTrackToolBar(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	virtual QString senderName() const override { return "OvrTrackToolBar"; }

public slots:
	void actionTriggered(QAction *action);

protected:
	void paintEvent(QPaintEvent *e) override;

	OvrTMLAddMenu* tml_menu = nullptr;
};

class OvrTMLAddMenu : public QMenu
{
	Q_OBJECT
public:
	explicit OvrTMLAddMenu(QWidget *parent = Q_NULLPTR);
	~OvrTMLAddMenu() {}

	void initContextMenu();

protected:
	void addActorMenu(QMenu* a_sub_menu, bool a_is_enable);
	QAction* createMenuItem(const char* a_title, const char* a_name, const QString& a_icon_path);
	void addIcon(int a_type, QAction* action);
};
