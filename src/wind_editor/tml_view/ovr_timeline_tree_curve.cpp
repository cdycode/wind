#include "Header.h"
#include "ovr_timeline_tree_curve.h"
#include "ovr_tree_item_delegate.h"
#include "ovr_time_line_tree_model.h"
#include "ovr_time_line_tree_item.h"
#include "ovr_timelinel_item.h"

#include "../project/ovr_player_listener.h"

#include <QPalette>

#include "ovr_tml_axis.h"
#include "ovr_time_line_curve_view.h"


//////////////////////////////////////////////////////////////
// class OvrTimeLineTreeCurve ovr_timeline_treecurve

void OvrTimeLineTreeCurve::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{
}

void OvrTimeLineTreeCurve::onPlayPosChanged(ovr::uint64 a_frame_index)
{
	if (isVisible())
	{
		updateItemIfNeeded();
	}
}

OvrTimeLineTreeCurve::OvrTimeLineTreeCurve(QWidget *parent) :QTreeView(parent)
{
	setModel(OvrTimeLineTreeModel::getInst());
	auto item_delegate = new OvrTreeItemDelegate(this);
	item_delegate->setShowValueOnly();
	item_delegate->setShowHeadOnly();
	setItemDelegate(item_delegate);

	header()->hide();
	setColumnWidth(0, 300);
	setColumnWidth(1, 0);
	setColumnWidth(2, 0);

	setUniformRowHeights(true);

	QByteArray open_node = getImagePath("icon_listclose_min.png").toLocal8Bit();
	QByteArray close_node = getImagePath("icon_listopen_min.png").toLocal8Bit();

	QString style_sheet = QString::asprintf(
		"QTreeView::branch:closed:has-children:!has-siblings,QTreeView::branch:closed:has-children:has-siblings {border-image:none;image:url(%s);}"
		"QTreeView::branch:open:has-children:!has-siblings,QTreeView::branch:open:has-children:has-siblings {border-image:none;image:url(%s);}"
		"QTreeView {border: 0px}"
		"QTreeView::item {border-bottom:1px solid rgba(151,151,151, 38); background-clip: border;}"
		"QTreeView::item:hover {background-color: rgb(40, 40, 40);}"
		"QTreeView::item:focus {selection-background-color: rgb(212, 212, 212);}"
		"QTreeView::item:selected {background-color: rgb(43, 43, 43);}"
		"QTreeView {background:rgb(25, 25, 25)}",
		close_node.constData(),
		open_node.constData()
	);
	setStyleSheet(style_sheet);
	QPalette pa = palette();
	pa.setColor(QPalette::Base, QColor("#252525"));
	pa.setColor(QPalette::AlternateBase, QColor("#222222"));
	setPalette(pa);
	setAlternatingRowColors(true);

	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	connect(OvrPlayerListener::getInst(), &OvrPlayerListener::onPlayPosChanged, this, &OvrTimeLineTreeCurve::onPlayPosChanged, Qt::QueuedConnection);
}

void OvrTimeLineTreeCurve::currentChanged(const QModelIndex &current, const QModelIndex &previous)
{
	if (current.isValid())
	{
		auto item = static_cast<OvrTimeLineTreeItem*>(current.internalPointer());
		if (item != nullptr)
		{			
			cur_actor = item->getActorTrack()->GetActor();
			emit onTrackSelected(cur_actor->GetUniqueName().GetStringConst(), item->getTrack());
			return;
		}
	}
	emit onTrackSelected("", nullptr);
}

//return true, if locate a new item
bool OvrTimeLineTreeCurve::locateItem(QModelIndex a_index)
{
	if (a_index.isValid())
	{
		if (a_index.column() != 0)
		{
			a_index = a_index.sibling(a_index.row(), 0);
		}

		collapseAll();
		setExpanded(a_index, true);

		auto parent_index = a_index.parent();
		while (true)
		{
			if (parent_index.isValid() && !isExpanded(parent_index))
			{
				setExpanded(parent_index, true);
				parent_index = parent_index.parent();
				continue;
			}
			break;
		}

		scrollTo(a_index);

		if (a_index != currentIndex())
		{
			setCurrentIndex(a_index);
			return true;
		}
	}
	emit OnSwitchBack();
	return false;
}

void OvrTimeLineTreeCurve::updateItemIfNeeded()
{
	auto& items = OvrTimeLineTreeItem::getItemsUpdateWithTime();
	for (auto it = items.begin(); it != items.end(); ++it)
	{
		OvrTimeLineTreeItem* item = it.key();
		updateItemIfNeeded(item, it.value());
	}
}

void OvrTimeLineTreeCurve::updateItemIfNeeded(OvrTimeLineTreeItem* a_item, int a_value)
{
	QList<OvrTimeLineTreeItem*> parents;
	int count = a_item->getParents(parents);

	//获取item的index
	QModelIndex index;
	for (int i = 0; i < count; ++i)
	{
		OvrTimeLineTreeItem* item = parents[i];
		QModelIndex item_index = model()->index(item->row(), 0, index);
		if (item == a_item)
		{
			index = item_index;
			break;
		}
		else if (!isExpanded(item_index))
		{
			return;
		}
		index = item_index;
	}
	if (!index.isValid())
	{
		return;
	}

	//更新头
	if ((a_value & kUpdateItemHead) > 0)
	{
		update(index);
	}
}