#include "Header.h"
#include "ovr_tml_axis.h"
#include "ovr_time_line_tree_model.h"
#include "../project/ovr_player_listener.h"

#include <QPainter>
#include <QScrollBar>
#include <QTimer>
#include <QMouseEvent>

static OvrTMLAxis* global_axis_x = nullptr;

OvrTMLAxisView* getGlobalAxisView()
{
	if (global_axis_x != nullptr)
	{
		return global_axis_x->getAxisView();
	}
	return nullptr;
}

void setGlobalAxisX(OvrTMLAxis* a_axis)
{
	global_axis_x = a_axis;
}

OvrTMLAxis* getGlobalAxisX()
{
	return global_axis_x;
}

//////////////////////////////////////////////////////
// class OvrTimeLineAxis

int   OvrTMLAxisView::share_total_secs = 300;
int   OvrTMLAxisView::share_total_frames = 300 * 30;
int   OvrTMLAxisView::share_fps = 30;
float OvrTMLAxisView::share_cur_frame = 0;
float OvrTMLAxisView::share_tml_wnd_port = 0;
float OvrTMLAxisView::share_pixel_per_unit = 0;

OvrTMLAxisView::TMLMode OvrTMLAxisView::share_tml_mode = kTMLFrame;

OvrTMLAxis::OvrTMLAxis(QWidget* parent, Qt::WindowFlags f) :QWidget(parent, f)
{
	setFixedHeight(28);

	QHBoxLayout* h_layout = new QHBoxLayout(this);
	h_layout->setMargin(0);
	h_layout->setSpacing(0);

	axis_view = new OvrTMLAxisView;
	h_layout->addWidget(axis_view);
}

void OvrTMLAxis::paintEvent(QPaintEvent *e)
{
	ApplyWidgetStyleSheet(this);
}

//////////////////////////////////////////////////////
// class OvrTimeLineAxisView

OvrTMLAxisView::OvrTMLAxisView(QWidget* parent /* = Q_NULLPTR */, Qt::WindowFlags f /* = Qt::WindowFlags() */) :QWidget(parent, f)
{
	QFontMetrics fm(font());
	tml_unit_line_info.unit_value_strig_width = fm.width("00:00:00:000");

	cursor_wnd = new OvrTMLAxisCursor(this);
	connect(cursor_wnd, &OvrTMLAxisCursor::cursorDragedTo, this, &OvrTMLAxisView::seekToDPX, Qt::QueuedConnection);
	connect(cursor_wnd, &OvrTMLAxisCursor::timeLineScale, [this](QPoint pt, int delta) {
		scaleTMLLine(pt.x(), delta < 0);
	});
	connect(OvrPlayerListener::getInst(), &OvrPlayerListener::onPlayPosChanged, this, &OvrTMLAxisView::onPlayPosChanged, Qt::QueuedConnection);
}

OvrTMLCursorLine* OvrTMLAxisView::createCursorLine(QWidget* a_parent)
{
	if (a_parent != nullptr)
	{
		if (cursor_line == nullptr)
		{
			cursor_line = new OvrTMLCursorLine(a_parent);
		}
		else if (cursor_line->parentWidget() != a_parent)
		{
			cursor_line->setParent(a_parent);
		}
	}
	else if (cursor_line != nullptr)
	{
		delete cursor_line;
		cursor_line = nullptr;
	}

	return cursor_line;
}

OvrTMLAxisCursorValue* OvrTMLAxisView::createCursorValue(QWidget* a_parent)
{
	if (a_parent != nullptr)
	{
		if (cursor_value == nullptr)
		{
			cursor_value = new OvrTMLAxisCursorValue(a_parent);
		}
		else if (cursor_value->parentWidget() != a_parent)
		{
			cursor_value->setParent(a_parent);
		}
	}
	else if (cursor_value != nullptr)
	{
		delete cursor_value;
		cursor_value = nullptr;
	}

	return cursor_value;
}

void OvrTMLAxisView::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{}

void OvrTMLAxisView::onPlayPosChanged(ovr::uint64 a_frame_index)
{
	if (!Float_Equal(cur_frame, a_frame_index))
	{
		cur_frame = a_frame_index;
		is_update_cursor = true;
		if (isVisible())
		{
			update();
		}
	}
}

bool OvrTMLAxisView::initUnitLineCount()
{
	int fps = getFPS();

	switch (tml_mode)
	{
	case kTMLFrame://5s 1s 10 5
		if (fps % 10 == 0)
		{
			tml_unit_line_info.setUnitLineCount(fps * 5, fps, 10, 5);
		}
		else if (fps % 5 == 0)
		{
			tml_unit_line_info.setUnitLineCount(fps * 5, fps, 5, 5);
		}
		else
		{
			assert(false);
		}
		break;
	case kTMLFrame_5://10s 5s 1s 10  
		assert(fps % 5 == 0);
		{
			tml_unit_line_info.setUnitLineCount(fps * 2, fps, fps / 5, 2);
		}
		break;
	case kTMLFrame_10://30s 10s 5s 1s
		assert(fps % 10 == 0);
		{
			tml_unit_line_info.setUnitLineCount(fps * 3, fps, fps / 10 * 5, fps / 10);
		}
		break;
	case kTMLSecs:	//1min 30s 10s 5s
		tml_unit_line_info.setUnitLineCount(60, 30, 10, 5);
		break;
	case kTMLSecs_5://5min 1min 30s 10s
		tml_unit_line_info.setUnitLineCount(60, 12, 6, 2);
		break;
	case kTMLSecs_10://10min 5min 1min 30s
		tml_unit_line_info.setUnitLineCount(60, 30, 6, 3);
		break;
	case kTMLSecs_30://30min 10min 5min 1min
		tml_unit_line_info.setUnitLineCount(60, 20, 6, 2);
		break;
	case kTMLMin:	 //60min 30min 10min 5min
		tml_unit_line_info.setUnitLineCount(60, 30, 10, 5);
		break;
	case kTMLMin_5:	 //4h 60min 30min 10min
		tml_unit_line_info.setUnitLineCount(48, 12, 6, 2);
		break;
	case kTMLMin_10: //12h 4h 60min 30min
		tml_unit_line_info.setUnitLineCount(72, 24, 6, 3);
		break;
	case kTMLMin_30: //24h 12h 4h 60min
		tml_unit_line_info.setUnitLineCount(48, 24, 8, 2);
		break;
	case kTMLHour:	//48h 24h 12h 4h
		tml_unit_line_info.setUnitLineCount(48, 24, 12, 4);
		break;
	case kTMLModeInvalid:
	case kTMLModeMax:
	default:
		assert(false);
		return false;
		break;
	}
	return true;
}

void OvrTMLAxisView::drawUnit(QPainter& painter)
{
	painter.setPen(QColor("#696969"));

	int unit_num = tml_wnd_port / pixel_per_unit;

	int hide_unit_count = tml_unit_line_info.getHideUnitValueCount();
	int hide_unit_num = unit_num - hide_unit_count;
	if (hide_unit_num < 0)
	{
		hide_unit_num = 0;
	}
	unit_num = hide_unit_num;


	int max_unit = getUnitNumber(getFrameTotal());
	float x_start = unit_num * pixel_per_unit;
	float x_end = tml_wnd_port + width();

	bool is_draw_value = false;
	bool is_out_range = false;

	for (float x_pos = x_start; x_pos <= x_end; ++unit_num, x_pos = unit_num * pixel_per_unit)
	{
		int line_size = tml_unit_line_info.calcUnitLineInfo(unit_num, is_draw_value);

		if (!is_out_range && unit_num > max_unit)
		{
			is_out_range = true;
			painter.setPen(QColor("#690000"));
		}
		painter.drawLine(QPointF(x_pos, height()), QPointF(x_pos, height() - line_size));

		if (is_draw_value)
		{
			int text_x = x_pos + 2, text_y = 12;
			drawUnitValue(painter, getFrameNumber(unit_num), text_x, text_y);
		}
	}
}

void OvrTMLAxisView::paintEvent(QPaintEvent *e)
{
	ApplyWidgetStyleSheet(this);

	QPainter painter(this);
	painter.setViewport(tml_x_zero_margin, 0, width(), height());
	painter.setWindow(tml_wnd_port, 0, width(), height());
	cur_trans = painter.combinedTransform();

	if (Float_Equal(pixel_per_unit, 0))
	{
		return;
	}

	drawUnit(painter);

	if (is_update_cursor)
	{
		updateCursor();
	}
}

void OvrTMLAxisView::showEvent(QShowEvent *event)
{
	if (Float_Equal(share_pixel_per_unit, 0))
	{
		adjustAxisView(QApplication::desktop()->width());
	}
	else
	{
		pixel_per_unit = share_pixel_per_unit;
		setTMLMode(share_tml_mode);
		initUnitDrawInfo();
		changeWndPort(share_tml_wnd_port);
		changeCurFrame(share_cur_frame);
	}
}

void OvrTMLAxisView::resizeEvent(QResizeEvent *event)
{
	updateCursor();

	QWidget::resizeEvent(event);
}

bool OvrTMLAxisView::changeCurFrame(float a_frame)
{
	if (!Float_Equal(cur_frame, a_frame))
	{
		cur_frame = a_frame;
		is_update_cursor = true;
		return true;
	}
	return false;
}

void OvrTMLAxisView::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		dragPress(event->x(), event->y());
	}
}

void OvrTMLAxisView::mouseMoveEvent(QMouseEvent *event)
{
	dragMove(event->x(), event->y());
}

void OvrTMLAxisView::mouseReleaseEvent(QMouseEvent *event)
{
	if (is_mouse_pressed && !is_mouse_moved)
	{
		seekToDPX(event->x(), true);
		emit mouseClick();
	}

	dragRelease(event->x(), event->y());
}

void OvrTMLAxisView::dragPress(int a_x, int a_y)
{
	is_mouse_pressed = true;
	is_mouse_moved = false;
	drag_x = a_x;
	drag_tml_wnd_port = tml_wnd_port;
}

void OvrTMLAxisView::dragRelease(int a_x, int a_y)
{
	is_mouse_pressed = false;
	is_mouse_moved = false;
}

void OvrTMLAxisView::seekToDPX(int a_x, bool can_be_round)
{
	seekToFrame(getFrameForDPX(a_x), can_be_round);
}

void OvrTMLAxisView::seekToFrame(float a_frame, bool can_be_round)
{
	if (a_frame < 0)
	{
		a_frame = 0;
	}

	int round_frame = round(a_frame);
	//qDebug() << "cur_frame: " << cur_frame << ", seekToFrame " << a_frame << ", round_frame: " << round_frame;

	if (round(cur_frame) != round_frame)
	{
		//qDebug() << "to seek timeline";
		if (!OvrTimeLineTreeModel::getInst()->seek(round_frame))
		{
			return;
		}
		//qDebug() << "seek timeline ok";
	}

	if (tml_mode == kTMLFrame && can_be_round)
	{
		a_frame = round_frame;
		//qDebug() << "round cur frame";
	}

	if (!Float_Equal(a_frame, cur_frame))
	{
		cur_frame = a_frame;
		share_cur_frame = cur_frame;
		updateCursor();
	}
}

void OvrTMLAxisView::dragMove(int a_x, int a_y)
{
	if (is_mouse_pressed)
	{
		resetScaleCenter();

		is_mouse_moved = true;

		if (moveTMLLine(a_x))
		{
			update();
		}
	}
}

int OvrTMLAxisView::getFrameTotal()
{
	return share_total_frames;
}

int OvrTMLAxisView::getFPS()
{
	return share_fps;
}

void OvrTMLAxisView::setRange(int a_secs, int a_fps)
{
	share_fps = a_fps;
	share_total_secs = a_secs;
	share_total_frames = a_secs * a_fps;
}

void OvrTMLAxisView::getRange(int& a_secs, int& a_fps)
{
	a_secs = share_total_secs;
	a_fps = share_fps;
}

void OvrTMLAxisView::initAlignPainer(QPainter& painter, QWidget* a_taget, int a_wnd_y, bool a_is_reverse_y)
{
	int x_offset = mapToGlobal(QPoint(0, 0)).x() - a_taget->mapToGlobal(QPoint(0, 0)).x();
	int view_port_x = x_offset + tml_x_zero_margin;

	painter.setViewport(view_port_x, 0, a_taget->width(), a_taget->height());
	int wnd_height = a_is_reverse_y ? -a_taget->height() : a_taget->height();
	painter.setWindow(tml_wnd_port, a_wnd_y, a_taget->width(), wnd_height);
}

void OvrTMLAxisView::initUnitDrawInfo()
{
	assert(pixel_per_unit > 0);
	tml_cur_view_space = (getUnitNumber(getFrameTotal())) * pixel_per_unit;
	tml_max_view_space = tml_cur_view_space + tml_unit_line_info.unit_value_strig_width;

	tml_unit_line_info.initUnitDrawInfo(pixel_per_unit);
}

void OvrTMLAxisView::wheelEvent(QWheelEvent *event)
{
	event->accept();

	scaleTMLLine(event->x(), event->delta() < 0);
}

bool OvrTMLAxisView::scaleTMLLine(int a_center_dpx, bool a_make_small)
{
	if (!Float_Equal(scale_center_dpx, a_center_dpx) || Float_Equal(tml_wnd_port, 0))
	{
		scale_center_dpx = a_center_dpx;
		scale_center_frame = getFrameForDPX(scale_center_dpx);
	}

	float new_tml_view_space = tml_cur_view_space * (a_make_small ? 10.0f / 11 : 1.1f);
	float new_tml_max_view_space = new_tml_view_space + tml_unit_line_info.unit_value_strig_width;

	//不能再缩小刻度
	if (a_make_small)
	{
		if (width() > new_tml_max_view_space)
		{
			new_tml_view_space = width() - tml_unit_line_info.unit_value_strig_width;
			if (Float_Equal(new_tml_view_space, tml_cur_view_space))
			{
				return false;
			}
		}
	}
	//不能再放大刻度
	else if (tml_mode == kTMLFrame && pixel_per_unit > kMaxFrameUnitSpace)
	{
		return false;
	}

	if (adjustAxisView(new_tml_view_space))
	{
		if (scale_center_frame < 0)
		{
			changeWndPort(0);
		}
		else
		{
			float new_scale_center_lpx = getLPXForFrame(scale_center_frame);
			float new_tml_wnd_port = new_scale_center_lpx - scale_center_dpx + tml_x_zero_margin;
			changeWndPort(new_tml_wnd_port);
		}
		update();
		emit onScaleChange(a_make_small);
		return true;
	}
	return false;
}

bool OvrTMLAxisView::adjustAxisView(float a_axis_space, int a_frame_size)
{
	const int min_unit_sapce = 5;

	if (a_frame_size == -1)
	{
		a_frame_size = getFrameTotal();
	}

	for (int mode = kTMLFrame; mode < kTMLModeMax; mode = nextMode(mode))
	{
		float new_pixel_per_unit = a_axis_space / getUnitNumber(a_frame_size, mode);
		if (new_pixel_per_unit < min_unit_sapce)
		{
			continue;
		}

		if (mode == kTMLFrame && new_pixel_per_unit > kMaxFrameUnitSpace)
		{
			new_pixel_per_unit = kMaxFrameUnitSpace;
		}

		if (mode < kTMLModeMax && mode > kTMLModeInvalid)
		{
			if (pixel_per_unit != new_pixel_per_unit || mode != tml_mode)
			{
				pixel_per_unit = new_pixel_per_unit;
				share_pixel_per_unit = pixel_per_unit;
				setTMLMode(mode);
				initUnitDrawInfo();
				is_update_cursor = true;
				return true;
			}
			return false;
		}
		break;
	}
	assert(false);
	return false;
}

int OvrTMLAxisView::nextMode(int a_mode)
{
	++a_mode;

	if (a_mode == kTMLFrame_5)
	{
		assert(share_fps % 5 == 0);
	}

	if (a_mode == kTMLFrame_10)
	{
		if (share_fps % 10 != 0)
		{
			++a_mode;
		}
	}
	return a_mode;
}

bool OvrTMLAxisView::makeSureVisible(int a_frame_head, int a_frame_end)
{
	resetScaleCenter();

	if (a_frame_end == -1)
	{
		if (adjustAxisView(width() - 1 * tml_x_zero_margin))
		//if (adjustAxisView(width()))
		{
			update();
			return true;
		}
		return false;
	}

	int frames_include = a_frame_end - a_frame_head;
	if (frames_include <= 0)
	{
		frames_include = 1;
	}

	bool is_changed = adjustAxisView(width() - 1 * tml_x_zero_margin, frames_include);
	//bool is_changed = adjustAxisView(width(), frames_include);

	int center_frame = a_frame_head + frames_include / 2;
	int space = getLPXForFrame(center_frame);
	is_changed = changeWndPort(space - width() / 2) || is_changed;

	if (is_changed)
	{
		update();
	}
	return is_changed;
}

float OvrTMLAxisView::getValuePerPixel()
{
	return getFrameNumber(1) / pixel_per_unit;
}

bool OvrTMLAxisView::moveTMLLine(float a_x)
{
	float new_wnd_port = drag_x - a_x + drag_tml_wnd_port;
	return changeWndPort(new_wnd_port);
}

bool OvrTMLAxisView::changeWndPort(float a_wnd_port)
{
	//右边到头
	if (width() + a_wnd_port > tml_max_view_space)
	{
		a_wnd_port = tml_max_view_space - width();
	}
	//左边到头
	if (a_wnd_port < 0)
	{
		a_wnd_port = 0;
	}

	if (tml_wnd_port != a_wnd_port)
	{
		is_update_cursor = true;
		tml_wnd_port = a_wnd_port;
		share_tml_wnd_port = tml_wnd_port;
		emit onWndPortChange();
		return true;
	}
	return false;
}

void OvrTMLAxisView::updateCursor()
{
	if (tml_mode <= kTMLModeInvalid)
	{
		return;
	}

	is_update_cursor = false;
	float dp_x = getDPXForFrame(cur_frame);
	bool to_be_hide = dp_x < 0;

	cursor_wnd->updatePos(dp_x, to_be_hide);

	int global_x = mapToGlobal(QPoint(dp_x, 0)).x();
	if (cursor_line != nullptr)
	{
		int y = cursor_wnd->geometry().bottom();
		int global_y = mapToGlobal(QPoint(0, y)).y();
		cursor_line->updatePos(global_x, global_y, to_be_hide);
	}

	if (cursor_value != nullptr)
	{
		cursor_value->updatePos(global_x, getFrameValueString(cur_frame), to_be_hide);
	}
}

bool OvrTMLAxisView::setTMLMode(int a_mode)
{
	if (a_mode != tml_mode)
	{
		tml_mode = (TMLMode)a_mode;
		share_tml_mode = tml_mode;
		initUnitLineCount();
		emit onTMLModeChange(getTMLModeString());
		return true;
	}
	return false;
}

QString OvrTMLAxisView::getTMLModeString()
{
	static const char* des[] =
	{
		"", "帧", "帧", "帧",
		"秒", "秒", "秒", "秒",
		"分", "分","分","分",
		"时", ""
	};
	return QString::fromLocal8Bit(des[tml_mode]);
}

QString OvrTMLAxisView::getTMLModeStringEx()
{
	static const char* des[] =
	{
		"", "帧", "5帧", "10帧",
		"秒", "5秒", "10秒", "30秒",
		"分", "5分","10分","30分",
		"时", ""
	};
	return QString::fromLocal8Bit(des[tml_mode]);
}

float OvrTMLAxisView::getFrameNumber(float a_unit_num)
{
	float result = 0;

	int fps = getFPS();
	switch (tml_mode)
	{
	case kTMLFrame:
		result = a_unit_num;
		break;
	case kTMLFrame_5:
		result = a_unit_num * 5;
		break;
	case kTMLFrame_10:
		result = a_unit_num * 10;
		break;
	case kTMLSecs:
		result = a_unit_num * fps;
		break;
	case kTMLSecs_5:
		result = a_unit_num * fps * 5;
		break;
	case kTMLSecs_10:
		result = a_unit_num * fps * 10;
		break;
	case kTMLSecs_30:
		result = a_unit_num * fps * 30;
		break;
	case kTMLMin:
		result = a_unit_num * 60 * fps;
		break;
	case kTMLMin_5:
		result = a_unit_num * fps * 60 * 5;
		break;
	case kTMLMin_10:
		result = a_unit_num * fps * 60 * 10;
		break;
	case kTMLMin_30:
		result = a_unit_num * fps * 60 * 30;
		break;
	case kTMLHour:
		result = a_unit_num * 60 * 60 * fps;
		break;
	default:
		assert(false);
		break;
	}
	return result;
}

float OvrTMLAxisView::getFrameForDPX(float a_x)
{
	QPointF pt_logic = cur_trans.inverted().map(QPointF(a_x, 0));
	float unit_num = pt_logic.x() / pixel_per_unit;

	return getFrameNumber(unit_num);
}

float OvrTMLAxisView::getFrameForGlobalDPX(float a_x)
{
	auto x_pos = mapFromGlobal(QPoint(a_x, 0)).x();
	return getFrameForDPX(x_pos);
}

float OvrTMLAxisView::getLPXForDPX(float a_x)
{
	QPointF pt_logic = cur_trans.inverted().map(QPointF(a_x, 0));

	return pt_logic.x();
}

float OvrTMLAxisView::getDPXForFrame(float a_frame)
{
	float lpx = getLPXForFrame(a_frame);
	QPointF pt_device = cur_trans.map(QPointF(lpx, 0));
	return pt_device.x();
}

float OvrTMLAxisView::getGlobalDPXForFrame(float a_frame)
{
	return mapToGlobal(QPoint(getDPXForFrame(a_frame), 0)).x();
}

float OvrTMLAxisView::getFrameForLPX(float a_x)
{
	float unit_num = a_x / pixel_per_unit;

	return getFrameNumber(unit_num);
}

float OvrTMLAxisView::getLPXForFrame(float a_frame)
{
	return getUnitNumber(a_frame) * pixel_per_unit;
}

float OvrTMLAxisView::getUnitNumber(float a_frame, int a_tml_mode)
{
	if (a_tml_mode <= kTMLModeInvalid)
	{
		a_tml_mode = tml_mode;
	}

	float result = 0;
	int fps = getFPS();

	switch (a_tml_mode)
	{
	case kTMLFrame:
		result = a_frame;
		break;
	case kTMLFrame_5:
		result = a_frame / 5;
		break;
	case kTMLFrame_10:
		result = a_frame / 10;
		break;
	case kTMLSecs:
		result = a_frame / fps;
		break;
	case kTMLSecs_5:
		result = a_frame / fps / 5;
		break;
	case kTMLSecs_10:
		result = a_frame / fps / 10;
		break;
	case kTMLSecs_30:
		result = a_frame / fps / 30;
		break;
	case kTMLMin:
		result = a_frame / fps / 60;
		break;
	case kTMLMin_5:
		result = a_frame / fps / 60 / 5;
		break;
	case kTMLMin_10:
		result = a_frame / fps / 60 / 10;
		break;
	case kTMLMin_30:
		result = a_frame / fps / 60 / 30;
		break;
	case kTMLHour:
		result = a_frame / fps / 60 / 60;
		break;
	default:
		assert(false);
		break;
	}
	return result;
}

int OvrTMLAxisView::getValueStringKeyIndex()
{
	int index = -1;

	switch (tml_mode)
	{
	case OvrTMLAxisView::kTMLFrame:
	case OvrTMLAxisView::kTMLFrame_5:
	case OvrTMLAxisView::kTMLFrame_10:
		index = 3;
		break;
	case OvrTMLAxisView::kTMLSecs:
	case OvrTMLAxisView::kTMLSecs_5:
	case OvrTMLAxisView::kTMLSecs_10:
	case OvrTMLAxisView::kTMLSecs_30:
		index = 2;
		break;
	case OvrTMLAxisView::kTMLMin:
	case OvrTMLAxisView::kTMLMin_5:
	case OvrTMLAxisView::kTMLMin_10:
	case OvrTMLAxisView::kTMLMin_30:
		index = 1;
		break;
	case OvrTMLAxisView::kTMLHour:
		index = 0;
		break;
	case OvrTMLAxisView::kTMLModeMax:
	case OvrTMLAxisView::kTMLModeInvalid:
	default:
		assert(false);
		break;
	}
	return index;
}

void OvrTMLAxisView::drawUnitValue(QPainter& painter, int a_frame_num, int a_x, int a_y)
{
	painter.save();

	QList<int> values;
	QString value_str = getFrameValueString(a_frame_num, &values);

	//绘制所有值域
	painter.setPen(QColor("#B8B8B8"));
	painter.drawText(a_x, a_y, value_str);

	painter.restore();
}

QString OvrTMLAxisView::getFrameValueString(int a_frame_num, QList<int>* a_values)
{
	static char buf[30] = { 0 };

	int fps = getFPS();

	int frames = a_frame_num % fps;
	int secs = a_frame_num / fps % 60;
	int mins = a_frame_num / fps / 60 % 60;
	int hours = a_frame_num / fps / 60 / 60 % 24;
	int days = a_frame_num / fps / 60 / 60 / 24;

	if (a_values != nullptr)
	{
		*a_values << hours << mins << secs << frames;
	}

	if (days == 0)
	{
		sprintf(buf, "%02d:%02d:%02d:%02d", hours, mins, secs, frames);
	}
	else
	{
		sprintf(buf, "%02d:%02d:%02d:%02d:%02d", days, hours, mins, secs, frames);

		if (a_values != nullptr)
		{
			a_values->insert(0, days);
		}
	}
	return buf;
}

/////////////////////////////////////////////////////////////////////
// class OvrTMLAxisCursor

OvrTMLAxisCursor::OvrTMLAxisCursor(QWidget *parent, Qt::WindowFlags f) :QLabel(parent, f)
{
	QPixmap bmp(getImagePath("time_line/icon_timeline_top.png"));
	setStyleSheet("background:transparent");
	setPixmap(bmp);
	adjustSize();
	auto sz = bmp.size();
	qDebug();
}

void OvrTMLAxisCursor::updatePos(int a_x, bool)
{
	a_x = a_x - width() / 2;
	if (a_x < -30 || a_x > parentWidget()->width() + 30)
	{
		if (isVisible() && !is_draging)
		{
			hide();
		}
		return;
	}

	move(a_x, parentWidget()->height() - height());
	if (isHidden())
	{
		show();
	}
}

void OvrTMLAxisCursor::mousePressEvent(QMouseEvent *e)
{
	e->accept();
	is_draging = true;
	x_press_center_offset = e->x() - width() / 2;
}

void OvrTMLAxisCursor::mouseReleaseEvent(QMouseEvent *e)
{
	e->accept();
	is_draging = false;

	int x_global_center = e->globalX() - x_press_center_offset;
	int cursor_dpx = parentWidget()->mapFromGlobal(QPoint(x_global_center, 0)).x();
	emit cursorDragedTo(cursor_dpx, true);
}

void OvrTMLAxisCursor::mouseMoveEvent(QMouseEvent *e)
{
	e->accept();
	if (is_draging)
	{
		int x_global_center = e->globalX() - x_press_center_offset;
		int cursor_dpx = parentWidget()->mapFromGlobal(QPoint(x_global_center, 0)).x();
		emit cursorDragedTo(cursor_dpx);
	}
}

void OvrTMLAxisCursor::wheelEvent(QWheelEvent * event)
{
	event->accept();
	emit timeLineScale(geometry().center(), event->delta());
}

/////////////////////////////////////////////////////////////////////
// class OvrTMLAxisCursorValue

OvrTMLAxisCursorValue::OvrTMLAxisCursorValue(QWidget *parent, Qt::WindowFlags f) :QLabel(parent, f)
{
	setFixedHeight(20);
	setStyleSheet("background:transparent;color:#C8C8C8");//DED4E3
}

void OvrTMLAxisCursorValue::updatePos(int a_global_x, const QString& a_value, bool)
{
	setText(a_value);
	adjustSize();

	int a_y = is_top ? 1 : parentWidget()->height() - height();
	auto pos_x = parentWidget()->mapFromGlobal(QPoint(a_global_x, 0)).x();
	pos_x += 8;

	if (pos_x < -100 || pos_x > parentWidget()->width())
	{
		if (isVisible())
		{
			hide();
		}
		return;
	}

	move(pos_x, a_y);

	if (isHidden())
	{
		show();
	}
}

/////////////////////////////////////////////////////////
// class OvrTMLCursorLine

OvrTMLCursorLine::OvrTMLCursorLine(QWidget* parent, Qt::WindowFlags f) :QWidget(parent, f)
{
	setStyleSheet("background:rgba(180,180,180,200)");
	setFixedWidth(1);
	winId();	//创建native窗口,避免拖拽时间轴的时候闪烁
}

void OvrTMLCursorLine::paintEvent(QPaintEvent *event)
{
	ApplyWidgetStyleSheet(this);
}

void OvrTMLCursorLine::wheelEvent(QWheelEvent *event)
{
	QPoint pt = getGlobalAxisView()->mapFromGlobal(event->globalPos());
	getGlobalAxisView()->scaleTMLLine(pt.x(), event->delta() < 0);
}

void OvrTMLCursorLine::updatePos(int a_global_x, int a_global_y, bool a_hide)
{
	auto pos_xy = parentWidget()->mapFromGlobal(QPoint(a_global_x, a_global_y));

	if (a_hide || pos_xy.x() > parentWidget()->width())
	{
		if (isVisible())
		{
			hide();
		}
		return;
	}

	move(pos_xy);
	if (isHidden())
	{
		show();
	}
}

//////////////////////////////////////////////////////////////////////////////
// struct  TMLUnitLineInfo

void TMLUnitLineInfo::setUnitLineCount(int a_big_unit, int a_mid_unit, int a_small_unit, int a_little_unit)
{
	big_unit = a_big_unit, mid_unit = a_mid_unit, small_unit = a_small_unit, little_unit = a_little_unit;
}

void TMLUnitLineInfo::initUnitDrawInfo(float a_pixel_per_unit)
{
	is_draw_mid_line_value = mid_unit * a_pixel_per_unit - unit_value_string_margin > unit_value_strig_width;

	is_draw_small_line_value = small_unit * a_pixel_per_unit - unit_value_string_margin > unit_value_strig_width;

	is_draw_little_line_value = little_unit * a_pixel_per_unit - unit_value_string_margin > unit_value_strig_width;

	is_draw_unit_line_value = a_pixel_per_unit - unit_value_string_margin > unit_value_strig_width;
}

int TMLUnitLineInfo::calcUnitLineInfo(int a_unit, bool& is_draw_value)
{
	int line_size = 2;

	is_draw_value = false;

	if (a_unit % big_unit == 0)
	{
		line_size = kLSBig;
		is_draw_value = true;
	}
	else if (a_unit % mid_unit == 0)
	{
		line_size = kLSMid;
		is_draw_value = is_draw_mid_line_value;
	}
	else if (a_unit % small_unit == 0)
	{
		line_size = kLSSmall;
		is_draw_value = is_draw_small_line_value;
	}
	else if (a_unit % little_unit == 0)
	{
		line_size = kLSLittle;
		is_draw_value = is_draw_little_line_value;
	}
	else
	{
		line_size = kLSBase;
		is_draw_value = is_draw_unit_line_value;
	}

	return line_size;
}

int TMLUnitLineInfo::getHideUnitValueCount()
{
	if (is_draw_unit_line_value)
	{
		return 1;
	}
	else if (is_draw_unit_line_value)
	{
		return little_unit;
	}
	else if (is_draw_small_line_value)
	{
		return small_unit;
	}
	else if (is_draw_mid_line_value)
	{
		return  mid_unit;
	}
	return big_unit;
}