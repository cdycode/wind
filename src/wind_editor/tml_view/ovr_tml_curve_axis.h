#pragma once

#include <QWidget>

class OvrTMLCurveAxis :public QWidget, public OvrEventReceiver
{
public:
	explicit OvrTMLCurveAxis(QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());

	virtual QString name() override { return "OvrTMLCurveAxis"; }
	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;

protected:
	virtual void mousePressEvent(QMouseEvent *e) override;
	virtual void mouseMoveEvent(QMouseEvent *e) override;
	virtual void mouseReleaseEvent(QMouseEvent *e) override;
	virtual void wheelEvent(QWheelEvent *event) override;

	virtual void paintEvent(QPaintEvent* e) override;

	bool fitWidth();
	bool fitHeight();
	bool fitWidthHeight();

protected:
	bool  changeWndPort(int a_wnd_port_y);
	void  calcUnitBase(float& a_unit_base_space, float& a_unit_base_value, float& a_unit_num_to_draw);

	void drawAxisY(QPainter& painter);
	void drawAxisYUnit(QPainter& painter, const QString& a_value, float a_y);
	void drawAxisYSmallUnit(QPainter& painter, float a_y, float a_small_unit_space, int a_head_line);

protected:
	virtual void onDrawContext(QPainter& painter) {}

	float getLPXForValue(float a_frame);
	float getValueForLPX(float a_lpx);

	float getLPYForValue(float a_value) { return getLPYForValue(a_value, ratio); }
	float getLPYForValue(float a_value, int a_ratio);
	float getValueForLPY(float a_lpy);

	OvrVector2 getValueForLP(const OvrVector2& a_lpt);
	OvrVector2 getLPForValue(const OvrVector2& a_value);

	virtual void getXRanges(int& a_min_frame, int& a_max_frame);
	virtual float getYRanges(float* a_min = nullptr, float* a_max = nullptr);

	bool is_auto_center = true;

	float ratio = 500;
	float wnd_port_y = 0;
	QTransform cur_trans;

	QPointF pt_right_drag;
	float right_drag_wnd_port_y = 0;
	float right_drag_wnd_port_x = 0;
	bool is_right_pressed = false;
};