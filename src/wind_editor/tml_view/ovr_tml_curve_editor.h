#pragma once

#include "ovr_tml_curve_axis.h"

class OvrTMLCurveEditor :public OvrTMLCurveAxis
{
public:
	explicit OvrTMLCurveEditor(QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());

protected:
	virtual void onDrawContext(QPainter& painter) override;
};