#pragma once

#include <QString>

//时间线上的时间表示
typedef struct OVR_TIME_LINE_TIME
{
	int		time_hours		= 0;	//小时
	int		time_minutes	= 0;	//分钟
	int		time_seconds	= 0;	//秒
	int		time_frames		= 0;	//帧
	int		frame_rate		= 120;	//每秒多少帧

	OVR_TIME_LINE_TIME(int a_hours, int a_minutes, int a_seconds, int a_frames, int a_frame_rate);
	OVR_TIME_LINE_TIME(int a_frame_rate);
	OVR_TIME_LINE_TIME() {}

	OVR_TIME_LINE_TIME& operator = (const OVR_TIME_LINE_TIME& a_time);

	void	operator ++	(void);
	// 两个时间点相差帧数. 如果T1+n=T2,则本运算返回值n=T2-T1
	int		operator -	(const OVR_TIME_LINE_TIME& a_time) const;
	bool	operator >	(const OVR_TIME_LINE_TIME& a_time) const;
	bool	operator <	(const OVR_TIME_LINE_TIME& a_time) const;
	bool	operator ==	(const OVR_TIME_LINE_TIME& a_time) const;
	bool	operator !=	(const OVR_TIME_LINE_TIME& a_time) const;
	bool	operator <=	(const OVR_TIME_LINE_TIME& a_time) const;
	bool	operator >=	(const OVR_TIME_LINE_TIME& a_time) const;

	void setTime(int a_hours = 0, int a_minutes = 0, int a_seconds = 0, int a_frames = 0);

	//增加帧
	bool addFrames(int a_frames);
	//增加秒
	bool addSeconds(int a_seconds);
	//增加分
	bool addMinutes(int a_minutes);
	//增加时
	bool addHours(int a_hours);

	void reset();	// reset to zero time
	void updateFps(int fps);
	int frameRate() const { return frame_rate; }
	bool isZeroTime(void) const;
	int totalFrames() const;
	int totalSeconds() const;
	int totalMinutes() const;

	QString toStringExactToHour() const;
	QString toStringExactToMinute() const;
	QString toStringExactToSecond() const;
	QString toStringExactToFrame() const;
	QString toString() const;

	// static methods
	static OVR_TIME_LINE_TIME timeRange(const OVR_TIME_LINE_TIME& t1, const OVR_TIME_LINE_TIME& t2);

}StuTimeLineTime;

Q_DECLARE_METATYPE(StuTimeLineTime);
