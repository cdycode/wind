#include "Header.h"
#include "ovr_tml_item_head_widget.h"

#include "ovr_tml_item.h"

OvrTMLItemHeadWidget::OvrTMLItemHeadWidget(QWidget *parent):QWidget(parent)
{
	setStyleSheet("background:transparent;");

	QHBoxLayout* h_layout = new QHBoxLayout(this);
	h_layout->setMargin(0);
	h_layout->setSpacing(0);
	
	title_label = new QLabel;
	h_layout->addWidget(title_label);

	h_layout->addStretch(0);

	fun_bar = new QToolBar;
	fun_bar->setIconSize({ 18,18 });
	connect(fun_bar, &QToolBar::actionTriggered, this, &OvrTMLItemHeadWidget::onBarAction, Qt::QueuedConnection);

	h_layout->addWidget(fun_bar);
}

OvrTMLItemHeadWidget::~OvrTMLItemHeadWidget()
{
	//qDebug() << "~OvrTMLItemHeadWidget entered, " << title_label->text();
}

void OvrTMLItemHeadWidget::paintEvent(QPaintEvent *event)
{
	ApplyWidgetStyleSheet(this);
}

void OvrTMLItemHeadWidget::initTrackContext(const QString& a_title, ovr_core::OvrTrack* a_track, OvrTMLItem* a_item)
{
	assert(!a_title.isEmpty());

	initTitleColor(a_track);

	title_label->setText(a_title);

	bind_item = a_item;
	
	auto track_type = a_track->GetTrackType();
	if (track_type == ovr_core::kTrackClip || track_type == ovr_core::kTrackPoint)
	{
		auto action = fun_bar->addAction(QIcon(getImagePath("icon_addtwo_nor.png")), "");
		action->setObjectName("add_key");
	}
	else if (a_track->HasSupportTrackList())
	{
		auto action = fun_bar->addAction(QString::fromLocal8Bit("�ӹ����"));
		action->setObjectName("add_child");
	}

	const char* icon_file = a_track->IsVisible() ? "time_line/icon_eye_open.png" : "time_line/icon_eye_close.png";
	auto action = fun_bar->addAction(QIcon(getImagePath(icon_file)), "");	//�ɼ�
	action->setObjectName("set_visible");
}

void OvrTMLItemHeadWidget::initTitleColor(ovr_core::OvrTrack* a_track)
{
	if (a_track->GetTrackType() == ovr_core::kTrackPoint)
	{
		if (a_track->GetDisplayName() == "X")
		{
			title_label->setStyleSheet("color:#B40000");
		}
		else if (a_track->GetDisplayName() == "Y")
		{
			title_label->setStyleSheet("color:#00B400");
		}
		else if (a_track->GetDisplayName() == "Z")
		{
			title_label->setStyleSheet("color:#0000B4");
		}
	}
}

void OvrTMLItemHeadWidget::onBarAction(QAction* action)
{
	if (bind_item->treeWidget()->currentItem() != bind_item)
	{
		bind_item->treeWidget()->setCurrentItem(bind_item);
	}

	if (action->objectName() == "add_child")
	{
		addChildTrack();
	}
	else if (action->objectName() == "set_visible")
	{
		bool is_visible = bind_item->getTrack()->IsVisible();
		bind_item->getTrack()->SetVisible(!is_visible);
		const char* icon_file = bind_item->getTrack()->IsVisible() ? "time_line/icon_eye_open.png" : "time_line/icon_eye_close.png";
		action->setIcon(QIcon(getImagePath(icon_file)));
	}
	else if (action->objectName() == "add_key")
	{
		addKeyFrame();
	}
}

void OvrTMLItemHeadWidget::addChildTrack()
{
	QMenu* menu = new QMenu;
	menu->setStyleSheet(GetMenuStyle());//144, 200, 246

	for (auto track_support : bind_item->getTrack()->GetSupportTrackList())
	{
		auto action = menu->addAction(QString::fromLocal8Bit(track_support.name.GetStringConst()));
		action->setObjectName("add_child_track");
		action->setProperty("main_type", track_support.track_type);
		action->setProperty("sub_type", QString::fromLocal8Bit(track_support.tag.GetStringConst()));
		action->setEnabled(track_support.is_enable);
	}
	
	auto result = menu->exec(QCursor::pos());
	if (result != nullptr)
	{
		int track_type = result->property("main_type").toInt();
		QString sub_type = result->property("sub_type").toString();
		bind_item->addChildItem(track_type, sub_type);
	}

	menu->deleteLater();
}

void OvrTMLItemHeadWidget::addKeyFrame()
{
	auto track = bind_item->getTrack();
	if (track->GetTrackType() == ovr_core::kTrackPoint)
	{		
		qDebug() << "todo: addKeyFrame for kTrackPoint";
	}
	else if (track->GetTrackType() == ovr_core::kTrackClip)
	{
		qDebug() << "todo: addKeyFrame for kTrackClip";
	}
}

void OvrTMLItemHeadWidget::contextMenuEvent(QContextMenuEvent *event)
{
	QMenu* menu = new QMenu;
	menu->setStyleSheet(GetMenuStyle());

	auto action = menu->addAction(QString::fromLocal8Bit("ɾ�� [") + title_label->text() + "]");
	action->setObjectName("remove");

	auto result = menu->exec(QCursor::pos());
	if (result != nullptr)
	{
		if (result->objectName() == "remove")
		{
			bind_item->remove();
		}
	}
	menu->deleteLater();
}