#pragma once

#include "ovr_time_line_tree_item.h"


//数据点轨道
//OvrPointTrack
//
class OvrTMLPointItem :public OvrTimeLineTreeItem
{
public:
	OvrTMLPointItem(const QString& a_title, OvrTimeLineTreeItem* a_parent);

	virtual bool isPointDataItem() override { return true; }
	virtual bool contains(int a_frame) override;
	virtual bool hitTest(int a_pos, bool is_for_resize = false) override;
	virtual bool dragingMove(int a_frame) override;
	virtual bool removeSelected() override;

	virtual const QString& getValueText();
	int GetPoints(QMap<ovr::uint64, char>& a_points, ovr::uint64 a_frame_begin, ovr::uint64 a_frame_end);

protected:
	template<class T> int  GetPointsFun(QMap<ovr::uint64, char>& a_points, ovr::uint64 a_frame_begin, ovr::uint64 a_frame_end, const T& maps);
	template<class T> bool containsFun(int a_frame, const T& maps);
	template<class T> bool hitTestFun(int a_pos, const T& maps);
	bool movePoint(ovr::uint64 a_from, ovr::uint64 a_to);

	QMap<ovr::uint64, char> points;
};

//数据片轨道
//数据片可以改变大小
class OvrTMLClipItem :public OvrTimeLineTreeItem
{
public:
	OvrTMLClipItem(const QString& a_title, OvrTimeLineTreeItem *a_parent_Item);
	~OvrTMLClipItem();

	ovr_core::OvrClipTrack* getClipTrack();

	virtual bool isClipDataItem() override { return true; }
	virtual bool contains(int a_frame) override;
	virtual bool splitFromCurrentCusor() override;
	virtual bool hitTest(int a_pos, bool is_for_resize = false) override;
	virtual bool removeSelected() override;
	virtual bool isSplitable() override;

	bool dragSelectedClipin(OvrTMLClipItem* a_item_from);

protected:
	virtual bool dragingMove(int a_frame) override;
	virtual bool dragingResize(int a_frame) override;
	DragMode calcDragMode(int a_pos, int a_head_pos, int a_tail_pos);
	virtual bool isResizeable() override;
	bool resizeClip(int a_from, int a_to, int a_start, int a_end);
	bool isAcceptAsset(const QString& a_asset_path) override;

	bool isInClip(int a_key_frame, int a_frame);
	bool splitClip(int a_from, int a_at, int a_mid_pos);

};