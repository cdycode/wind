#include "Header.h"
#include "ovr_tml_item_body_widget.h"

#include "ovr_tml_item.h"
#include "ovr_tml_axis.h"

OvrTMLItemBodyWidget::OvrTMLItemBodyWidget(QWidget *parent):QWidget(parent)
{
	setStyleSheet("background:transparent;");

	connect(getGlobalAxisView(), &OvrTMLAxisView::onWndPortChange, [this]() {
		update();
	});

	connect(getGlobalAxisView(), &OvrTMLAxisView::onScaleChange, [this](bool) {
		update();
	});
}

OvrTMLItemBodyWidget::~OvrTMLItemBodyWidget()
{
	//qDebug() << "~OvrTMLItemBodyWidget entered, ";;
}

void OvrTMLItemBodyWidget::paintEvent(QPaintEvent *event)
{
	ApplyWidgetStyleSheet(this);

	QPainter painter(this);
	getGlobalAxisView()->initAlignPainer(painter, this);

#if 0
	int x_start = getGlobalAxisView()->getWndPortX();

	if (x_start < 0)
	{
		x_start = 0;
	}
	float x_end = x_start + width();
#endif

}

void OvrTMLItemBodyWidget::initTrackContext(ovr_core::OvrTrack* a_track, OvrTMLItem* a_item)
{
	bind_item = a_item;
}

void OvrTMLItemBodyWidget::contextMenuEvent(QContextMenuEvent *event)
{

}