#pragma once

#include <QDialog>
#include "ui_ovr_add_mark_dlg.h"

class OvrAddMarkDlg : public QDialog
{
	Q_OBJECT

public:
	OvrAddMarkDlg(QWidget *parent = Q_NULLPTR);
	~OvrAddMarkDlg();

public slots:
	void on_button_ok_clicked();
	void on_button_cancel_clicked();

public:
	void setMode(int a_key_pos, const QString& a_key_name, QMap<ovr::uint64, QString>* a_key_items, const QString& a_key_word, bool a_is_add_new);
	const QString& getKeyName() { return key_name; }

private:
	void init_curstom_ui();
	void set_custom_style_sheet();
	void preset_connects();

private:
	Ui::OvrAddMarkDlg ui;
	bool is_add_new = true;
	int key_pos = -1;
	QString key_name;
	QMap<ovr::uint64, QString>* key_items = nullptr;
	QString key_word;
};
