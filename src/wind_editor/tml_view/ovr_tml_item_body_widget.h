#pragma once

#include <QWidget>

class OvrTMLItem;

class OvrTMLItemBodyWidget : public QWidget, public OvrEventSender
{
	Q_OBJECT

public:
	explicit OvrTMLItemBodyWidget(QWidget *parent = Q_NULLPTR);
	~OvrTMLItemBodyWidget();

	virtual QString senderName() const override { return "OvrTMLItemBodyWidget"; }

	void initTrackContext(ovr_core::OvrTrack* a_track, OvrTMLItem* a_item);

protected:
	virtual void paintEvent(QPaintEvent *event) override;
	virtual void contextMenuEvent(QContextMenuEvent *event) override;

	OvrTMLItem* bind_item = nullptr;
};