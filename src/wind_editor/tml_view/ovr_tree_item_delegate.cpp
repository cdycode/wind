#include "Header.h"
#include "ovr_tree_item_delegate.h"
#include "ovr_time_line_tree_item.h"
#include "ovr_timelinel_item.h"

#include "ovr_time_line_tree.h"

#include "ovr_tml_axis.h"

static int add_track_text_width = 0;
static int space_between_buttons = 5;
static int add_key_button_width = 18;// 13;

static int set_eye_width = 18;// 19;
static int set_eye_height = 18;// 15;

static int config_width = 16;
static int config_height = 15;

static int value_text_width = 0;

OvrTreeItemDelegate::OvrTreeItemDelegate(QObject *parent):QStyledItemDelegate(parent)
{
	actor_icon = new QIcon(QPixmap(getImagePath("time_line/static_actor.png")));
	ani_icon = new QIcon(QPixmap(getImagePath("time_line/skeletal_actor.png")));
	audio_icon = new QIcon(QPixmap(getImagePath("audio.png")));

	eye_img_opened = new QImage(getImagePath("time_line/icon_eye_open.png"));
	eye_img_closed = new QImage(getImagePath("time_line/icon_eye_close.png"));
	config_img = new QImage(getImagePath("config.png"));

	add_key_button_img_nor = new QImage(getImagePath("icon_addtwo_nor.png"));
	add_key_button_img_sel = new QImage(getImagePath("icon_addtwo_sel.png"));

	key_point_img = new QImage(getImagePath("time_line/icon_point.png"));
	key_point_img_sel = new QImage(getImagePath("time_line/icon_point_sel.png"));
	if (!key_point_img->isNull())
	{
		OvrTimeLineTreeItem::setKeyPointRange(key_point_img->width() / 2);
	}

	add_track_text = QString::fromLocal8Bit("加轨道＋");
}


void OvrTreeItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	painter->save();

	//绘制左边的节点
	if (index.column() == kTMLSectionHeadIndex)
	{
		drawTimeLineTitle(painter, option, index);		
	}
	else if (is_show_head_only)
	{
		QStyledItemDelegate::paint(painter, option, index);
	}
	//绘制节点的列数据
	else if (index.column() == kTMLSectionValueIndex)
	{
		drawTimeLineValue(painter, option, index);
	}
	else if (index.column() == kTMLSectionLineIndex)
	{
		QStyledItemDelegate::paint(painter, option, index);
	}
	painter->restore();
}

void OvrTreeItemDelegate::calcKeyButtonArea(const QRect& a_rc, OvrTimeLineTreeItem* a_item, OvrTMLKeyButtonArea& a_area) const
{
	QRect rc_prev;
	rc_prev.moveLeft(a_rc.left() + a_rc.width());

	//是否可见按钮
	if (!is_show_value_only)
	{
		QRect rc = a_rc;
		int w = set_eye_width;
		rc.moveLeft(rc_prev.left() - space_between_buttons - w);
		rc.setWidth(w);

		int h = set_eye_height;
		rc.moveTop(rc.top() + (rc.height() - h) / 2);
		rc.setHeight(h);

		a_area.rcVisibleButton = rc;
		rc_prev = rc;
	}

	//添加子track按钮
	if (a_item->canHaveChildTrack() && !is_show_value_only)
	{
		QRect rc = a_rc;
		int w = add_track_text_width;
		rc.moveLeft(rc_prev.left() - space_between_buttons - w);
		rc.setWidth(w);
		a_area.rcAddTrackButton = rc;
		rc_prev = rc;
	}

	//添加config按钮
	if (a_item->isConfigable() && !is_show_value_only)
	{
		QRect rc = a_rc;
		int w = config_width;
		rc.moveLeft(rc_prev.left() - space_between_buttons*2 - w);
		rc.setWidth(w);

		int h = config_height;
		rc.moveTop(rc.top() + (rc.height() - h) / 2);
		rc.setHeight(h);

		a_area.rcConfigButton = rc;
		rc_prev = rc;
	}

	//打点按钮
	if (a_item->canAddKeyFrame() && !is_show_value_only)
	{
		QRect rc = a_rc;
		int w = add_key_button_width;
		rc.moveLeft(rc_prev.left() - space_between_buttons * 2 - w);
		rc.setWidth(w);

		int h = w;
		rc.moveTop(rc.top() + (rc.height() - h) / 2);
		rc.setHeight(h);

		a_area.rcAddKeyFrameButton = rc;
		rc_prev = rc;
	}

	//添加字符串区域
	if (value_text_width > 0)
	{
		QRect rc = a_rc;
		rc.moveLeft(rc_prev.left() - space_between_buttons * 2 - value_text_width);
		rc.setWidth(value_text_width);

		a_area.rcValueText = rc;
		rc_prev = rc;
	}
}

void OvrTreeItemDelegate::drawTimeLineTitle(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	QStyleOptionViewItem opt = option;
	OvrTimeLineTreeItem* item = static_cast<OvrTimeLineTreeItem*>(index.internalPointer());

	//设置icon
	if (item->isActorItem())
	{
		if (!actor_icon->isNull())
		{
			opt.icon = *actor_icon;			
			opt.features |= QStyleOptionViewItem::HasDecoration;
		}
	}
	QStyledItemDelegate::paint(painter, opt, index);
	

	if (add_track_text_width == 0)
	{
		add_track_text_width = painter->fontMetrics().width(add_track_text);
	}

	value_text_width = 0;
	const QString& value_text = item->getValueText();
	if (!value_text.isEmpty())
	{
		value_text_width = painter->fontMetrics().width(value_text);
	}

	OvrTMLKeyButtonArea area;
	calcKeyButtonArea(opt.rect, item, area);

	QWidget* widget = static_cast<QWidget*>(painter->device());
	assert(widget != nullptr);
	QPoint mouse_pos = widget->mapFromGlobal(QCursor::pos());

	//绘制是否可见的按钮
	if (!area.rcVisibleButton.isEmpty())
	{
		painter->drawImage(area.rcVisibleButton, *eye_img_opened);
	}

	//绘制添加子轨道的按钮
	if (!area.rcAddTrackButton.isEmpty())
	{
		painter->save();
		painter->setPen(QColor("#C0C0C0"));
		painter->drawText(area.rcAddTrackButton, Qt::AlignCenter|Qt::AlignVCenter, add_track_text);
		painter->restore();
	}

	//绘制配置按钮
	if (!area.rcConfigButton.isEmpty())
	{
		assert(!config_img->isNull());
		painter->drawImage(area.rcConfigButton, *config_img);
	}

	//绘制打点按钮
	if (!area.rcAddKeyFrameButton.isEmpty())
	{
		bool is_hover = area.rcAddKeyFrameButton.contains(mouse_pos);
		painter->drawImage(area.rcAddKeyFrameButton, is_hover ? *add_key_button_img_sel : *add_key_button_img_nor);
	}

	//绘制float track的值
	if (!area.rcValueText.isEmpty())
	{
		painter->save();
		painter->setPen(QColor("#C0C0C0"));
		painter->drawText(area.rcValueText, Qt::AlignVCenter, item->getValueText());
		painter->restore();
	}
}

void OvrTreeItemDelegate::drawTimeLineValue(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	QStyledItemDelegate::paint(painter, option, index);

	painter->setRenderHint(QPainter::Antialiasing);
	painter->setClipRect(option.rect);

	painter->setViewport(getGlobalAxisX()->getAxisView()->getTMLXZeroMargin(), 0, option.rect.width(), option.rect.height());
		
	float wnd_port_x = getGlobalAxisView()->getWndPortX() - option.rect.left();
	float wnd_port_y = -option.rect.top();
	painter->setWindow(wnd_port_x, wnd_port_y, option.rect.width(), option.rect.height());

	drawItemValue(painter, option, index, wnd_port_x);
}


void OvrTreeItemDelegate::drawItemValue(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index, float a_wnd_port_x) const
{
	OvrTimeLineTreeItem* item = static_cast<OvrTimeLineTreeItem*>(index.internalPointer());
	assert(item != nullptr);

	if (item->isClipDataItem())
	{
		drawClipItem(painter, option, static_cast<OvrTMLClipItem*>(item), a_wnd_port_x);
	}
	else if (item->isPointDataItem())
	{
		drawPointItem(painter, option, static_cast<OvrTMLPointItem*>(item), a_wnd_port_x);
	}
	if (!item->isActorItem() && item->childCount() > 0 && !isExpand(index))
	{
		drawChildItems(painter, option, item, a_wnd_port_x);
	}
}

void OvrTreeItemDelegate::drawChildItems(QPainter *painter, const QStyleOptionViewItem &option, OvrTimeLineTreeItem* a_item, float a_wnd_port_x) const
{
	for (int i = 0; i < a_item->childCount(); ++i)
	{
		painter->save();
		OvrTimeLineTreeItem* child_item = a_item->child(i);
		if (child_item->isClipDataItem())
		{
			drawClipItem(painter, option, static_cast<OvrTMLClipItem*>(child_item), a_wnd_port_x);
		}
		else if (child_item->isPointDataItem())
		{
			drawPointItem(painter, option, static_cast<OvrTMLPointItem*>(child_item), a_wnd_port_x);
		}
		if (child_item->childCount() > 0)
		{
			drawChildItems(painter, option, child_item, a_wnd_port_x);
		}
		painter->restore();
	}
}

const int OvrTreeItemDelegate::kRowHeight = 27;

QSize OvrTreeItemDelegate::sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index) const
{
	QSize sz = QStyledItemDelegate::sizeHint(option, index);
	sz.setHeight(kRowHeight);
	return sz;
}

void OvrTreeItemDelegate::drawClip(QPainter* painter, int a_clip_begin, int a_clip_end, int a_top, int a_height, const QString& a_clip_name, bool is_selected) const
{
	float left = getGlobalAxisView()->getLPXForFrame(a_clip_begin);
	float right = getGlobalAxisView()->getLPXForFrame(a_clip_end);

	float width = right - left;
	if (width < 1)
	{
		width = 1.0f;
	}

	is_selected ? painter->setBrush(QColor("#B07F60B3")) : painter->setBrush(QColor("#607F60B3"));
	painter->setPen(QColor("#70997FBA"));
	painter->drawRect(left, a_top, width, a_height);

	if (!a_clip_name.isEmpty())
	{
		painter->setPen(QColor("#DED4E3"));
		painter->drawText(left, a_top, width, a_height, Qt::AlignCenter, a_clip_name);
	}
}

void OvrTreeItemDelegate::drawClipItem(QPainter *painter, const QStyleOptionViewItem &option, OvrTMLClipItem* clip_item, float a_wnd_port_x) const
{
	if (clip_item->getClipTrack()->GetClipMap().size() <= 0)
	{
		return;
	}

	float x_start = a_wnd_port_x;
	if (x_start < 0)
	{
		x_start = 0;
	}
	float x_end = x_start + option.rect.width();

	int frame_begin = getGlobalAxisView()->getFrameForLPX(x_start);
	int frame_end = getGlobalAxisView()->getFrameForLPX(x_end);

	const int border_margin = 1;
	int top = border_margin;
	int height = option.rect.height() - 2 * border_margin;

	int clip_begin, clip_end;
	for (auto clip : clip_item->getClipTrack()->GetClipMap())
	{
		clip_begin = clip.first;
		clip_end = clip_begin + clip.second->Length();
		if (clip_end < frame_begin)
		{
			continue;
		}
		if (clip_begin > frame_end)
		{
			break;
		}
		drawClip(painter, clip_begin, clip_end, top, height, QString::fromLocal8Bit(clip.second->file_path.GetStringConst()), clip_begin == clip_item->selectKey());
	}
}

bool OvrTreeItemDelegate::isExpand(const QModelIndex& index) const
{
	QObject* obj = parent();
	QTreeView* tree = dynamic_cast<QTreeView*>(obj);

	if (index.column() != 0)
	{
		const QModelIndex& index_0 = index.model()->index(index.row(), 0, index.parent());
		return tree->isExpanded(index_0);
	}
	return tree->isExpanded(index);
}

void OvrTreeItemDelegate::drawPointItem(QPainter *painter, const QStyleOptionViewItem &option, OvrTMLPointItem* item, float a_wnd_port_x) const
{
	float x_start = a_wnd_port_x;
	if (x_start < 0)
	{
		x_start = 0;
	}
	float x_end = x_start + option.rect.width();

	int frame_begin = getGlobalAxisView()->getFrameForLPX(x_start);
	int frame_end = getGlobalAxisView()->getFrameForLPX(x_end);
	
	QMap <ovr::uint64, char> points;
	if (item->GetPoints(points, frame_begin, frame_end) > 0)
	{
		drawPoint(painter, option, points);
	}
}

void OvrTreeItemDelegate::drawPoint(QPainter *painter, const QStyleOptionViewItem &option, const QMap<ovr::uint64, char>& keys) const
{
	assert(key_point_img != nullptr && !key_point_img->isNull());

	QRect pt_image_rc(0, 0, key_point_img->width(), key_point_img->height());
	pt_image_rc.moveTop((option.rect.height() - pt_image_rc.height()) / 2);

	int pos = 0, key = 0;
	for (auto it = keys.begin(); it != keys.end(); ++it)
	{
		key = it.key();
		pos = getGlobalAxisView()->getLPXForFrame(key);
		pt_image_rc.moveLeft(pos - pt_image_rc.width() / 2);

		it.value() > 0 ? painter->drawImage(pt_image_rc, *key_point_img_sel) : painter->drawImage(pt_image_rc, *key_point_img);
	}
}
