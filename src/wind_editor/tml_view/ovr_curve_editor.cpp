#include "Header.h"
#include "ovr_curve_editor.h"

#include "ovr_tml_axis.h"
#include "ovr_time_line_curve_view.h"
#include "ovr_timeline_tree_curve.h"

//////////////////////////////////////////////////////////////
// class OvrCurveEditor

OvrCurveEditor::OvrCurveEditor(QWidget *parent) :QWidget(parent)
{
	QGridLayout* g_layout = new QGridLayout;
	g_layout->setMargin(0);
	g_layout->setSpacing(1);

	{	//标题
		QLabel* w = new QLabel(QString::fromLocal8Bit("曲线编辑器"));
		QFont w_font = w->font();
		w_font.setPointSize(10);
		w->setFont(w_font);
		w->setAlignment(Qt::AlignCenter);
		w->setStyleSheet("color:rgb(180,180,180);background:#232323");
		g_layout->addWidget(w, 0, 0);
	}

	//时间轴
	time_line_axis = new OvrTMLAxis;
	time_line_axis->setStyleSheet("background:#2F2F2F;color:white");
	g_layout->addWidget(time_line_axis, 0, 1);

	//轨道选择窗口
	OvrTimeLineTreeCurve* tree = new OvrTimeLineTreeCurve;
	tree->setFixedWidth(300);
	g_layout->addWidget(tree, 1, 0);
	curve_tree = tree;

	//曲线编辑窗口
	curve_view = new OvrTimeLineCurveView();
	g_layout->addWidget(curve_view, 1, 1);

	setLayout(g_layout);

	connect(tree, &OvrTimeLineTreeCurve::onTrackSelected, curve_view, &OvrTimeLineCurveView::onTrackSelected, Qt::QueuedConnection);
	connect(tree, &OvrTimeLineTreeCurve::OnSwitchBack, curve_view, &OvrTimeLineCurveView::OnSwitchBack, Qt::QueuedConnection);
}

OvrTMLAxisView* OvrCurveEditor::getAxisView()
{
	return time_line_axis->getAxisView();
}