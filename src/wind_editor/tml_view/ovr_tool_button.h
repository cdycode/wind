#pragma once

#include <QLabel>

class OvrToolButton : public QLabel
{
	Q_OBJECT
public:
	explicit OvrToolButton(QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) :QLabel(parent, f) {}

	OvrToolButton* setIcons(const QString& a_icon_name, const QString& a_check_icon_name, const QString& a_path);

	void setCheck(bool a_flag);
	bool isChecked() { return is_checked; }

signals:
	void clicked(OvrToolButton* a_button);

protected:
	QPixmap* makeIcon(const QString& a_path, const QString& a_head, const QString& a_name, const QString& a_tail);

	virtual void enterEvent(QEvent *e) override;
	virtual void leaveEvent(QEvent *e) override;
	virtual void mousePressEvent(QMouseEvent *e) override;
	virtual void mouseReleaseEvent(QMouseEvent *e) override;

	QPixmap* nor_icon = nullptr;
	QPixmap* hi_icon  = nullptr;
	QPixmap* sel_icon = nullptr;

	QPixmap* check_nor_icon = nullptr;
	QPixmap* check_hi_icon = nullptr;
	QPixmap* check_sel_icon = nullptr;

	bool is_checked = false;
};

