#include "Header.h"
#include "ovr_time_line_time.h"

OVR_TIME_LINE_TIME::OVR_TIME_LINE_TIME(int a_hours, int a_minutes, int a_seconds, int a_frames, int a_frame_rate)
{
	time_hours = a_hours;
	time_minutes = a_minutes;
	time_seconds = a_seconds;
	time_frames = a_frames;
	frame_rate = a_frame_rate;
}

OVR_TIME_LINE_TIME::OVR_TIME_LINE_TIME(int a_frame_rate)
{
	frame_rate = a_frame_rate;
}

OVR_TIME_LINE_TIME& OVR_TIME_LINE_TIME::operator=(const OVR_TIME_LINE_TIME& a_time)
{
	if (this != &a_time) {
		time_hours = a_time.time_hours;
		time_minutes = a_time.time_minutes;
		time_seconds = a_time.time_seconds;
		time_frames = a_time.time_frames;
		frame_rate = a_time.frame_rate;
	}
	return *this;
}

int OVR_TIME_LINE_TIME::operator-(const OVR_TIME_LINE_TIME& a_time) const
{
	return totalFrames() - a_time.totalFrames();
}

void OVR_TIME_LINE_TIME::operator++(void)
{
	addFrames(1);
}

bool OVR_TIME_LINE_TIME::operator>(const OVR_TIME_LINE_TIME& a_time) const
{
	if (time_hours != a_time.time_hours)
	{
		return (time_hours > a_time.time_hours);
	}

	if (time_minutes != a_time.time_minutes)
	{
		return (time_minutes > a_time.time_minutes);
	}
	if (time_seconds != a_time.time_seconds)
	{
		return (time_seconds > a_time.time_seconds);
	}
	if (time_frames != a_time.time_frames)
	{
		return (time_frames > a_time.time_frames);
	}
	return false;
}

bool OVR_TIME_LINE_TIME::operator<(const OVR_TIME_LINE_TIME& a_time) const
{
	if (time_hours != a_time.time_hours)
	{
		return (time_hours < a_time.time_hours);
	}

	if (time_minutes != a_time.time_minutes)
	{
		return (time_minutes < a_time.time_minutes);
	}
	if (time_seconds != a_time.time_seconds)
	{
		return (time_seconds < a_time.time_seconds);
	}
	if (time_frames != a_time.time_frames)
	{
		return (time_frames < a_time.time_frames);
	}
	return false;
}

bool OVR_TIME_LINE_TIME::operator==(const OVR_TIME_LINE_TIME& a_time) const
{
	return (time_hours == a_time.time_hours) && (time_minutes == a_time.time_minutes) \
		&& (time_seconds == a_time.time_seconds) && (time_frames == a_time.time_frames);
}

bool OVR_TIME_LINE_TIME::operator!=(const OVR_TIME_LINE_TIME& a_time) const
{
	return (time_frames != a_time.time_frames) || (time_seconds != a_time.time_seconds) \
		|| (time_minutes != a_time.time_minutes) || (time_hours != a_time.time_hours);
}

bool OVR_TIME_LINE_TIME::operator<=(const OVR_TIME_LINE_TIME& a_time) const
{
	return *this < (a_time) || *this == (a_time);
}

bool OVR_TIME_LINE_TIME::operator>=(const OVR_TIME_LINE_TIME& a_time) const
{
	return *this > (a_time) || *this == (a_time);
}

bool OVR_TIME_LINE_TIME::addFrames(int a_frames)
{
	if (0 == a_frames)
	{
		return true;
	}
	if (a_frames > 0)
	{
		//帧增加
		time_frames += a_frames;
		if (time_frames < frame_rate)
		{
			return true;
		}

		//处理帧达到m_iFramesPerSecond的情况
		addSeconds(time_frames / frame_rate);
		time_frames %= frame_rate;
		return true;
	}

	//减少帧
	time_frames += a_frames;
	if (time_frames >= 0)
	{
		return true;
	}
	int iSeconds = (time_frames - (frame_rate - 1)) / frame_rate;
	if (addSeconds(iSeconds))
	{
		time_frames %= frame_rate;
		if (time_frames < 0)
		{
			time_frames += frame_rate;
		}
		return true;
	}
	//SetTime(0,0,0,0);
	return false;
}

bool OVR_TIME_LINE_TIME::addSeconds(int a_seconds)
{
	if (0 == a_seconds)
	{
		return true;
	}
	if (a_seconds > 0)
	{
		time_seconds += a_seconds;
		if (time_seconds < 60)
		{
			return true;
		}
		addMinutes(time_seconds / 60);
		time_seconds %= 60;
		return true;
	}

	//减少秒
	time_seconds += a_seconds;
	if (time_seconds >= 0)
	{
		return true;
	}
	int iMinutes = (time_seconds - 59) / 60;
	if (addMinutes(iMinutes))
	{
		time_seconds %= 60;
		if (time_seconds < 0)
		{
			time_seconds += 60;
		}
		return true;
	}
	//SetTime(0,0,0,0);
	return false;
}

bool OVR_TIME_LINE_TIME::addMinutes(int a_minutes)
{
	if (0 == a_minutes)
	{
		return true;
	}
	if (a_minutes > 0)
	{
		time_minutes += a_minutes;
		if (time_minutes < 60)
		{
			return true;
		}
		addHours(time_minutes / 60);
		time_minutes %= 60;
		return true;
	}

	//减少分
	time_minutes += a_minutes;
	if (time_minutes >= 0)
	{
		return true;
	}
	int iHours = (time_minutes - 59) / 60;
	if (addHours(iHours))
	{
		time_minutes %= 60;
		if (time_minutes < 0)
		{
			time_minutes += 60;
		}
		return true;
	}
	//SetTime(0,0,0);
	return false;
}

bool OVR_TIME_LINE_TIME::addHours(int a_hours)
{
	if (0 == a_hours)
	{
		return true;
	}
	time_hours += a_hours;
	if (time_hours >= 0)
	{
		return true;
	}
	reset();
	return false;
}

void OVR_TIME_LINE_TIME::setTime(int a_hours /*= 0*/, int a_minutes /*= 0*/, int a_seconds /*= 0*/, int a_frames /*= 0*/)
{
	time_hours = a_hours;
	time_minutes = a_minutes;
	time_seconds = a_seconds;
	time_frames = a_frames;
}

void OVR_TIME_LINE_TIME::updateFps(int fps)
{
	if (fps != frame_rate) {
		int frameCount = totalFrames();
		frame_rate = fps;
		addFrames(frameCount);
	}
}

bool OVR_TIME_LINE_TIME::isZeroTime(void) const
{
	return (0 >= time_hours) && (0 >= time_minutes) && (0 >= time_seconds) && (0 >= time_frames);
}

int OVR_TIME_LINE_TIME::totalFrames() const
{
	return totalSeconds()*frame_rate + time_frames;
}

int OVR_TIME_LINE_TIME::totalSeconds() const
{
	return totalMinutes() * 60 + time_seconds;
}

int OVR_TIME_LINE_TIME::totalMinutes() const
{
	return time_hours * 60 + time_minutes;
}

QString OVR_TIME_LINE_TIME::toStringExactToHour() const
{
	return QString::asprintf("%02d:00:00:00", time_hours);
}

QString OVR_TIME_LINE_TIME::toStringExactToMinute() const
{
	return QString::asprintf("%02d:%02d:00:00", time_hours, time_minutes);
}

QString OVR_TIME_LINE_TIME::toStringExactToSecond() const
{
	return QString::asprintf("%02d:%02d:%02d:00", time_hours, time_minutes, time_seconds);
}

QString OVR_TIME_LINE_TIME::toStringExactToFrame() const
{
	if (frame_rate < 100)
	{
		return QString::asprintf("%02d:%02d:%02d:%02d", time_hours, time_minutes, time_seconds, time_frames);
	}
	return QString::asprintf("%02d:%02d:%02d:%03d", time_hours, time_minutes, time_seconds, time_frames);
}

QString OVR_TIME_LINE_TIME::toString() const
{
	return toStringExactToFrame();
}

OVR_TIME_LINE_TIME OVR_TIME_LINE_TIME::timeRange(const OVR_TIME_LINE_TIME& t1, const OVR_TIME_LINE_TIME& t2)
{
	OVR_TIME_LINE_TIME t(t1.frameRate());
	t.addFrames(1 + abs(t1 - t2));
	return t;
}

void OVR_TIME_LINE_TIME::reset()
{
	time_hours = 0;
	time_minutes = 0;
	time_seconds = 0;
	time_frames = 0;
}
