#pragma once

#include <QWidget>

class OvrToolButton;

class OVRTMLTopBar : public QWidget, public OvrEventReceiver, public OvrEventSender
{
	Q_OBJECT
public:
	explicit OVRTMLTopBar(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());

	virtual QString senderName() const override { return "OVRTMLTopBar"; }
	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;
	virtual QString name() override { return "OVRTMLTopBar"; }

public slots:
	void currentIndexChanged(int index);
	void onToolBarButtonClicked(OvrToolButton* a_button);
	void onUpdatePointStatus(QString a_mode, bool a_is_devorce);
	void onPlayStatusChanged(QString a_status);

protected:
	virtual void paintEvent(QPaintEvent *event) override;

	void initUI();
	void onProjectOpen(bool, QString a_prj_dir);
	void onCurrentSceneChanged();

	OvrToolButton* AddToolButton(const QString& a_obj_name, const QString& a_tip, QLayout* a_layout = nullptr);

	void showCurveEditorButtons(bool show_editor);

	QComboBox* sequence_box = nullptr;
	QMap<QString, OvrToolButton*> buttons;
	QString tml_status = "pause";

	QHBoxLayout* curve_buttons_layout = nullptr;
};
