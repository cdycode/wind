﻿#include "Header.h"
#include "ovr_mark_view.h"
#include "ovr_time_line_tree_model.h"
#include "ovr_add_mark_dlg.h"
#include "ovr_tml_axis.h"

#include <QToolTip>

static ovr_core::OvrMarkTrack* getMarkTrack()
{
	ovr_core::OvrSequence* current_sequence = ovr_project::OvrProject::GetIns()->GetCurrentSequence();
	assert(current_sequence != nullptr);

	if (current_sequence == nullptr)
	{
		return nullptr;
	}

	return current_sequence->GetMarkInfo();
}

/////////////////////////////////////////////////////////////////////
// class OvrMarkView

OvrMarkView::OvrMarkView(QWidget* parent, Qt::WindowFlags f) :QWidget(parent, f)
{
	setMouseTracking(true);

	mark_menu = new OvrTMLMarkMenu(this);
	connect(mark_menu, &QMenu::triggered, this, &OvrMarkView::menuTriggered, Qt::QueuedConnection);
	connect(OvrTimeLineTreeModel::getInst(), &OvrTimeLineTreeModel::markAdded, this, &OvrMarkView::onMarkAdded, Qt::QueuedConnection);

	connect(getGlobalAxisView(), &OvrTMLAxisView::onWndPortChange, [this]() {
		update();
	});

	connect(getGlobalAxisView(), &OvrTMLAxisView::onScaleChange, [this](bool) {
		update();
	});
}

void OvrMarkView::reloadMark()
{
	bool tobe_updated = false;

	if (mark_items.size() > 0)
	{
		tobe_updated = true;
		mark_items.clear();
	}

	for (auto item : getMarkTrack()->GetKeyFrames())
	{
		mark_items[item.first] = QString::fromLocal8Bit(item.second.GetStringConst());
	}

	if (mark_items.size() > 0)
	{
		tobe_updated = true;
	}

	if (tobe_updated)
	{
		update();
	}

}

void OvrMarkView::onMarkAdded(ovr::uint64 a_frame)
{
	if (mark_items.contains(a_frame))
	{
		return;
	}

	OvrAddMarkDlg dlg;
	dlg.setMode(a_frame, "", &mark_items, QString::fromLocal8Bit("标记名"), true);

	if (dlg.exec() == 1)
	{
		select_key = a_frame;
		mark_items[a_frame] = dlg.getKeyName();
		getMarkTrack()->AddKeyFrame(a_frame, mark_items[a_frame].toLocal8Bit().constData());
		update();
	}
}

void OvrMarkView::menuTriggered(QAction *action)
{
	auto obj_name = action->objectName();

	if (obj_name == "rename_select")
	{
		renamMark();
	}
	else if (obj_name == "remove_select")
	{
		removeMark();
	}
}

void OvrMarkView::removeMark(bool)
{
	if (select_key != -1)
	{
		mark_items.remove(select_key);
		getMarkTrack()->RmvKeyFrame(select_key);

		select_key = -1;
		update();
	}
}

void OvrMarkView::paintEvent(QPaintEvent *e)
{
	ApplyWidgetStyleSheet(this);

	QPainter painter(this);
	getGlobalAxisView()->initAlignPainer(painter, this);

	float x_start = getGlobalAxisView()->getWndPortX();
	if (x_start < 0)
	{
		x_start = 0;
	}
	float x_end = x_start + width();

	int frame_begin = getGlobalAxisView()->getFrameForLPX(x_start);
	int frame_end = getGlobalAxisView()->getFrameForLPX(x_end);

	QMap <long int, char> keys;
	getFrameKeys(keys, frame_begin, frame_end);

	drawMarks(painter, keys);
}

void OvrMarkView::contextMenuEvent(QContextMenuEvent *event)
{
	not_clear_mark_on_focus_out = true;

	mark_menu->setFlag(setSelectKey(getMark(event->x())));
	mark_menu->move(mapToGlobal(event->pos()));
	mark_menu->show();
}

void OvrMarkView::mouseDoubleClickEvent(QMouseEvent *e)
{
	is_mouse_move = false;

	if (e->button() == Qt::LeftButton)
	{
		if (setSelectKey(getMark(e->x())))
		{
			qDebug() << "todo: config for mark " << QString::fromLocal8Bit(getMarkTrack()->GetKeyFrame(select_key).GetStringConst());
		}
	}

	QWidget::mouseDoubleClickEvent(e);
}

void OvrMarkView::mousePressEvent(QMouseEvent *e)
{
	is_mouse_move = false;
	is_drag = false;

	if (e->button() == Qt::LeftButton)
	{
		is_click_down = true;
		x_pt_click_down = e->x();
		int new_key = getMark(x_pt_click_down);
		if (is_mark_clicked = new_key != -1)
		{
			setSelectKey(new_key);
		}
	}
}

void OvrMarkView::mouseMoveEvent(QMouseEvent *e)
{
	if (!is_mouse_move)
	{
		is_mouse_move = true;
	}

	if (is_mark_clicked)
	{
		moveMark(x_pt_click_down, e->x());
	}
	//拖拽
	else if (is_click_down)
	{
		if (!is_drag)
		{
			is_drag = true;
			int old_x = mapToGlobal(QPoint(x_pt_click_down, 0)).x();
			emit onDragPress(old_x);
		}
		emit onDragMove(e->globalX());
	}
	//显示Mark的提示名
	else
	{
		setTooltipKey(getMark(e->x()));
	}
}

void OvrMarkView::mouseReleaseEvent(QMouseEvent *e)
{
	if (is_drag)
	{
		emit onDragRelease(e->globalX());
	}
	else if (is_click_down)
	{
		if (!is_mark_clicked)
		{
			setSelectKey(-1);
			emit mouseClick(e->globalX());
		}
	}

	is_drag = false;
	is_click_down = false;
	is_mouse_move = false;
	is_mark_clicked = false;
}

void OvrMarkView::getFrameKeys(QMap <long int, char>& keys, int start, int end) const
{
	int key = 0;
	int value = 0;
	for (auto it = mark_items.begin(); it != mark_items.end(); ++it)
	{
		key = it.key();

		if (key < start)
		{
			continue;
		}
		if (key > end)
		{
			break;
		}
		value = select_key == key ? 1 : 0;
		keys[key] = keys.contains(key) ? keys[key] + value : value;
	}
}

void OvrMarkView::drawMarks(QPainter& painter, const QMap<long int, char>& keys) const
{
	painter.setPen(Qt::NoPen);

	QRect rc(0, 0, kMarkItemWidth, kMarkItemHeight);
	rc.moveTop(height() - rc.height());

	int pos = 0, key = 0;

	QPoint points[3];
	painter.setRenderHint(QPainter::Antialiasing);

	for (auto it = keys.begin(); it != keys.end(); ++it)
	{
		key = it.key();
		pos = getGlobalAxisView()->getLPXForFrame(key);

		rc.moveLeft(pos - rc.width() / 2);
		if (it.value() > 0)
		{
			painter.setBrush(QColor("#FFCC6600"));
		}
		else
		{
			painter.setBrush(QColor("#60CC6600"));
		}

		getMarkDrawPoints(points, pos);

		painter.drawPolygon(points, 3);
	}
}

void OvrMarkView::getMarkDrawPoints(QPoint a_points[], int x) const
{
	a_points[0].setX(x - kMarkItemWidth / 2);
	a_points[0].setY(height());

	a_points[1].setX(x + kMarkItemWidth / 2);
	a_points[1].setY(height());

	a_points[2].setX(x);
	a_points[2].setY(height() - kMarkItemHeight);
}

bool OvrMarkView::setSelectKey(int a_key)
{
	if (select_key != a_key)
	{
		select_key = a_key;
		update();
	}
	return select_key != -1;
}

int OvrMarkView::getMark(int a_x)
{
	if (mark_items.size() == 0)
	{
		return -1;
	}

	//查找是否选中Mark
	int pos = a_x;
	int left_pos = pos - kMarkItemWidth / 2, right_pos = pos + kMarkItemWidth / 2;
	if (left_pos < 0)
	{
		left_pos = 0;
	}
	int frame_begin = getGlobalAxisView()->getFrameForDPX(left_pos);
	int frame_end = getGlobalAxisView()->getFrameForDPX(right_pos);

	int new_select_key = -1;
	for (int i = frame_begin; i < frame_end; ++i)
	{
		if (mark_items.contains(i))
		{
			new_select_key = i;
			drag_key = i;
			//break; not break, just to select the last mark
		}
	}

	return new_select_key;
}

void OvrMarkView::setTooltipKey(int a_key)
{
	if (tooltip_key != a_key)
	{
		//show tooltip
		if (a_key != -1)
		{
			int tip_x = getGlobalAxisView()->getDPXForFrame(a_key);
			tip_x += kMarkItemWidth / 2;
			int tip_y = height() - kMarkItemHeight / 2;
			QToolTip::showText(mapToGlobal(QPoint(tip_x, tip_y)), mark_items[a_key]);
		}
		//hide tooltip
		else
		{
			QToolTip::hideText();
		}
		tooltip_key = a_key;
	}
}

void OvrMarkView::moveMark(int a_old_x, int a_current_x)
{
	int dx = a_current_x - a_old_x;
	int frame_offset = round(getGlobalAxisView()->getFrameForLPX(abs(dx)));

	int new_key = dx > 0 ? drag_key + frame_offset : drag_key - frame_offset;
	if (new_key < 0)
	{
		new_key = 0;
	}

	assert(select_key != -1);

	if (select_key != new_key && !mark_items.contains(new_key))
	{
		mark_items[new_key] = mark_items[select_key];
		mark_items.remove(select_key);

		getMarkTrack()->AddKeyFrame(new_key, getMarkTrack()->GetKeyFrame(select_key));
		getMarkTrack()->RmvKeyFrame(select_key);

		select_key = new_key;

		update();
	}
}

void OvrMarkView::renamMark()
{
	assert(select_key != -1);

	not_clear_mark_on_focus_out = true;

	OvrAddMarkDlg dlg;
	dlg.setMode(select_key, mark_items[select_key], &mark_items, QString::fromLocal8Bit("标记名"), false);
	if (dlg.exec() == 1)
	{
		mark_items[select_key] = dlg.getKeyName();
		getMarkTrack()->AddKeyFrame(select_key, mark_items[select_key].toLocal8Bit().constData());
	}
}

void OvrMarkView::clearSelectedMark()
{
	if (not_clear_mark_on_focus_out)
	{
		not_clear_mark_on_focus_out = false;
		return;
	}

	if (select_key != -1)
	{
		select_key = -1;
		update();
	}
}

/////////////////////////////////////////////////////////////////////
// class OvrTMLMarkMenu

OvrTMLMarkMenu::OvrTMLMarkMenu(QWidget *parent) :QMenu(parent)
{
	setStyleSheet(GetMenuStyle());

	initContextMenu();
}

void OvrTMLMarkMenu::initContextMenu()
{
	action_rename = addAction(QString::fromLocal8Bit("重命名..."));
	action_rename->setObjectName("rename_select");

	action_removed = addAction(QString::fromLocal8Bit("删除"));
	action_removed->setObjectName("remove_select");
}

void OvrTMLMarkMenu::setFlag(bool a_enable)
{
	action_rename->setEnabled(a_enable);
	action_removed->setEnabled(a_enable);
}