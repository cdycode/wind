#pragma once

#include <QWidget>
#include <QScrollArea>
#include <wx_base/wx_vector2.h>
#include <wx_base/wx_color_value.h>

namespace ovr_core
{
	class OvrTrack;
	class OvrFloatTrack;
	class OvrCtlPoint;
	class OvrCurvePointManager;
}

class OvrCuveMenu;

class OvrTimeLineCurveView :public QWidget
{
	Q_OBJECT
public:
	explicit OvrTimeLineCurveView(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	~OvrTimeLineCurveView();

	void doOperation(QString a_op_name);

	bool isAutoFit() { return is_auto_fit; }
	void setAutoFit(bool a_flag);
	void setAutoFitHeight();

signals:
	void onUpdatePointStatus(QString a_mode, bool a_is_devorce);

public slots:
	void onTrackSelected(QString a_actor_id, ovr_core::OvrTrack* a_track);
	void OnSwitchBack();

protected:
	virtual void paintEvent(QPaintEvent *e) override;

	virtual void hideEvent(QHideEvent *event) override;

	virtual void mousePressEvent(QMouseEvent *event) override;
	virtual void mouseReleaseEvent(QMouseEvent *event) override;
	virtual void mouseMoveEvent(QMouseEvent *event) override;	
	virtual void wheelEvent(QWheelEvent *event) override;

	bool searchFloatTracksForGroup(ovr_core::OvrTrack* a_track, bool a_is_added_to_show_tracks);

	void onCurveLineChanged(ovr_core::OvrTrack* a_track);

	void drawAxisXAlignLine(QPainter& painter);
	void drawAxisY(QPainter& painter);
	void calcUnitBase(float& a_unit_base_space, float& a_unit_base_value, float& a_unit_num_to_draw);
	void drawAxisYUnit(QPainter& painter, const QString& a_value, float a_y);
	void drawAxisYSmallUnit(QPainter& painter, float a_y, float a_small_unit_space, int a_head_line);

	void drawCurve(QPainter& painter);
	void drawCurveLine(QPainter& painter, ovr_core::OvrCurvePointManager* a_pt_manager, const QString& a_tag);
	void drawCurvePoint(QPainter& painter, ovr_core::OvrCurvePointManager* a_pt_manager);
	void drawControlLine(QPainter& painter, int a_frame, const OvrVector2& a_pt_current, ovr_core::OvrCurvePointManager* a_pt_manager);
	void drawControlLine(QPainter& painter, const QColor& a_color, const OvrVector2& a_pt_from, const OvrVector2& a_pt_to);
	void drawBezier(QPainter& painter, int a_begin_frame, int a_end_frame, ovr_core::OvrCurvePointManager* a_pt_manager);

	void getControlPoints(int a_frame, OvrVector2& a_pt_left, OvrVector2& a_pt_right, bool is_user_control, ovr_core::OvrCurvePointManager* a_pt_manager);
	void getNormalLine(int a_frame, OvrVector2& a_pt_n, OvrVector2& a_pt_current, ovr_core::OvrCurvePointManager* a_pt_manager);

	OvrVector2 calcPointPos(const OvrVector2& a_pt_value);
	float calcPointPosX(float a_frame);
	float calcPointPosY(float a_value, int a_ratio);

	OvrVector2 calcPointValue(const OvrVector2& a_pt);
	float   calcPointValueX(float a_pos_x);
	float   calcPointValueY(float a_pos_y);

	bool selectPoint(int a_x, int a_y);
	bool selectCtlPoint(int a_x, int a_y);
	bool isDevicePointInLogicRect(int a_x, int a_y, OvrVector2 a_center);

	void showRightMenu(QPoint a_pt);
	ovr_core::OvrFloatTrack* isClickOnTrackPoint(const QPointF& a_pt, int& a_frame);
	void onAddPoint();
	void onRmvPoint();
	int getSelectPoint(OvrColorValue& a_color);

	void notifySelectPointChanage();
	void onTrackLinePointSelected();
	void updateCurrentCtlPoint(const OvrVector2& a_ctl_point, ovr_core::OvrCurvePointManager* a_pt_manager);

	bool fitWidth();
	bool fitHeight();
	void getXRanges(int& a_min_frame, int& a_max_frame);
	float getYRanges(float* a_min = nullptr, float* a_max = nullptr);

	bool changeWndPort(int a_wnd_port);

	void reset();
	void showTrackLine(bool a_is_show);
	void resetTrackLine();
	void updateTrackLine();

protected:
	float ratio = 500;

	bool is_auto_fit = false;

	bool is_selected_ctl_pt_left = false;
	bool is_selected_ctl_pt_right = false;

	QPointF pt_click_down;
	QPointF pt_click_center;
	bool is_ctl_point_pressed = false;
	bool is_data_point_pressed = false;
	bool is_data_point_moved = false;

	bool is_auto_center = true;
	int wnd_port_x = 0, wnd_port_y = 0;
	int tml_x_zero_margin = 0;

	bool is_drag = false;
	bool is_right_menu = false;
	QPoint drag_pt;
	int drag_wnd_port_y = 0;

	OvrCuveMenu* curve_menu = nullptr;

	QTransform cur_trans;

	std::map<ovr::uint64, OvrVector3> points_map;

	OvrString actor_id;
	ovr_core::OvrFloatTrack* cur_track = nullptr;
	ovr_core::OvrCurvePointManager* cur_pt_manager = nullptr;

	//ovr_core::OvrGroupTrack* group_track = nullptr;
	std::list<ovr_core::OvrFloatTrack*> show_tracks;
	QMap<QString, ovr_core::OvrFloatTrack*> xyz_tracks;
	bool has_track_line = false;
};

class OvrCuveMenu : public QMenu
{
	Q_OBJECT
public:
	explicit OvrCuveMenu(QWidget *parent = Q_NULLPTR);
	void setItemsEnable(bool a_can_rmv, bool a_can_add);
	void setRmvTarget(ovr_core::OvrFloatTrack* a_track, int a_frame) { track = a_track; frame = a_frame; }
	void setAddTarget(const QPoint& a_pt) { track = nullptr; pt_click = a_pt; }

	ovr_core::OvrFloatTrack* getTrack() { return track; }
	int getFrame() { return frame; }
	const QPoint& getPoint() { return pt_click; }

protected:
	QAction* rmv_action;
	QAction* add_action;

	ovr_core::OvrFloatTrack* track = nullptr;
	int frame = -1;
	QPoint pt_click;
};