#include "Header.h"
#include "ovr_volume_setting_dlg.h"

OvrVolumeSettingDlg::OvrVolumeSettingDlg(float volume, QWidget *parent)
	: QDialog(parent)
	, m_volume(volume)
{
	ui.setupUi(this);

	init_custom_ui();
	set_custom_style_sheet();
	preset_connects();
}

OvrVolumeSettingDlg::~OvrVolumeSettingDlg()
{
}

float OvrVolumeSettingDlg::get_volume()
{
	return m_volume;
}

void OvrVolumeSettingDlg::on_btn_ok_clicked()
{
	done(QDialog::Accepted);
}

void OvrVolumeSettingDlg::on_btn_cancel_clicked()
{
	done(QDialog::Rejected);
}

void OvrVolumeSettingDlg::on_update_display(int value)
{
	QString strText = QString::number(value);

	ui.volume_display->setText(strText);

	m_volume = display_to_volume(value);
}

void OvrVolumeSettingDlg::on_slider_pos()
{
	QString strText = ui.volume_display->text();

	bool bresult = false;
	float f_value = strText.toFloat(&bresult);

	f_value = f_value < 0 ? 0 : f_value > 100 ? 100 : f_value;

	int n_value = static_cast<int>(f_value);
	ui.volume_display->setText(QString::number(n_value));
	ui.slider_volume->setValue(n_value);

	m_volume = display_to_volume(n_value);
}

void OvrVolumeSettingDlg::init_custom_ui()
{
	setWindowFlags(windowFlags() | Qt::FramelessWindowHint);

	ui.title_bar->setUiFlags(TitleBar::TitleFlag::TF_BTN_CLOSE);
	QString strTitle = QString::fromLocal8Bit("��������");
	//ui.title_bar->setTitle(strTitle);

	installEventFilter(ui.title_bar);

	setWindowTitle(strTitle);

	ui.slider_volume->setRange(0, 100);
	ui.slider_volume->setSingleStep(1);

	QString reg_exp_filter = QString::fromLocal8Bit("^[+\\-]{0,1}\\d*\\.{0,1}\\d*$");
	QRegExp regExpPort = QRegExp(reg_exp_filter);
	ui.volume_display->setInputMethodHints(Qt::ImhDigitsOnly);
	ui.volume_display->setMaxLength(10);
	ui.volume_display->setMaximumWidth(50);
	ui.volume_display->setValidator(new QRegExpValidator(regExpPort));


	int display_vol = volume_to_display(m_volume);
	ui.slider_volume->setValue(display_vol);
	ui.volume_display->setText(QString::number(display_vol));

	ui.btn_ok->setDefault(true);
}

void OvrVolumeSettingDlg::set_custom_style_sheet()
{
	QString strDefaultStyle = QString::fromLocal8Bit(
		"QWidget:focus{ outline: none;}"
		"QWidget{background-color: rgb(48,48,48);color:rgb(204, 204, 204); font-family:\"΢���ź�\";}"
		"QLabel{font-size:14px;}"
		"QLineEdit{height: 26px; border-radius: 2px; background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); border-radius:2px; font-size:12px; selection-background-color: rgb(165, 165, 165); selection-color: rgb(32, 32, 32);}"
		"QPushButton{width: 138px; height: 38px; color: rgb(205, 205, 205); border: 1px solid rgb(30, 30, 30); border-radius: 4px; background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); }"
		"QPushButton:hover{background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(153, 153, 153), stop:1 rgb(109, 109, 109)); }"
		"QPushButton:pressed {background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:1 rgb(118, 118, 118), stop:0 rgb(87, 87, 87)); }"
		"QPushButton:disabled {background-color:rgb(56, 56, 56); color:rgb(76, 76, 76);  border: 1px solid rgb(76, 76, 76); }"

		"QProgressBar{border-radius:2px; background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); font-size:12px; text-align: right;}"
		"QProgressBar::chunk{background-color: #F69F11; width: 20px; }"

		"QMessageBox {color: rgb(204, 204, 204); background-color: rgba(48, 48, 48); border-radius: 2px; messagebox-text-interaction-flags: 5 }"
		"QMessageBox QPushButton {border: 1px solid rgb(30, 30, 30); border-radius: 2px; width: 70px; height: 25px; background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgba(118, 118, 118, 255), stop:1 rgba(87, 87, 87, 255));}"
	);
	this->setStyleSheet(strDefaultStyle);

	QString strTitleBarStyle = QString::fromLocal8Bit(
		"QWidget{background-color: rgb(48,48,48); color:rgb(204, 204, 204); font-family:\"΢���ź�\"; border:0px solid rgb(76, 76, 76);}"
		"QLabel{font:normal 16px; font-family:\"΢���ź�\";}"
	);
	ui.title_bar->setStyleSheet(strTitleBarStyle);

	////
	//QString strBtnSetStyle = QString::fromLocal8Bit(
	//	"QPushButton{background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(56, 56, 56), stop:1 rgb(50, 50, 50)); color: rgb(204, 204, 204); border: 1px solid rgb(76, 76, 76); border-radius: 2px;}"
	//	"QPushButton:hover{background-color:rgb(76, 76, 76);}"
	//	"QPushButton:pressed {background-color:rgb(56, 56, 56);}"
	//);
	//ui.btn_select_path->setStyleSheet(strBtnSetStyle);

	//
	QString strLineStyle = QString::fromLocal8Bit("background-color:rgb(59, 59, 59); border-top:1px solid rgb(38, 38, 38); border-bottom:1px solid rgb(48, 48, 48);");
	ui.line->setStyleSheet(strLineStyle);

}

void OvrVolumeSettingDlg::preset_connects()
{
	QObject::connect(ui.btn_ok, SIGNAL(clicked()), this, SLOT(on_btn_ok_clicked()));
	QObject::connect(ui.btn_cancel, SIGNAL(clicked()), this, SLOT(on_btn_cancel_clicked()));

	QObject::connect(ui.slider_volume, SIGNAL(valueChanged(int)), this, SLOT(on_update_display(int)));
	QObject::connect(ui.volume_display, SIGNAL(editingFinished()), this, SLOT(on_slider_pos()));
}

int OvrVolumeSettingDlg::volume_to_display(float vol, float min, float max)
{
	return vol / (max - min) * 100;
}

float OvrVolumeSettingDlg::display_to_volume(int dvol, int min, int max)
{
	return 1.*dvol/(max-min);
}
