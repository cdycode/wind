#pragma once

#include <QStyledItemDelegate>
#include <QSize>

class OvrLogicTimeLineEx;
class OvrTimeLineTreeItem;
class OvrTMLClipItem;
class OvrTMLPointItem;

struct OvrTMLKeyButtonArea
{
	QRect rcVisibleButton;
	QRect rcAddTrackButton;
	QRect rcConfigButton;
	QRect rcAddKeyFrameButton;
	QRect rcValueText;
};

class OvrTMLAxisView;

class OvrTreeItemDelegate : public QStyledItemDelegate
{
public:
    OvrTreeItemDelegate(QObject *parent = Q_NULLPTR);

	virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

	QSize sizeHint(const QStyleOptionViewItem & option,	const QModelIndex & index) const override;

	void calcKeyButtonArea(const QRect& a_rc, OvrTimeLineTreeItem* a_item, OvrTMLKeyButtonArea& a_area) const;

	static const int kRowHeight;

	void setShowValueOnly() { is_show_value_only = true; }
	void setShowHeadOnly() { is_show_head_only = true; }

protected:
	void drawTimeLineTitle(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
	void drawTimeLineValue(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
	void drawItemValue(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index, float a_wnd_port_x) const;
	void drawChildItems(QPainter *painter, const QStyleOptionViewItem &option, OvrTimeLineTreeItem* a_item, float a_wnd_port_x) const;

	void drawClipItem(QPainter *painter, const QStyleOptionViewItem &option, OvrTMLClipItem* clip_item, float a_wnd_port_x) const;
	void drawClip(QPainter* painter, int a_clip_begin, int a_clip_end, int a_top, int a_height, const QString& a_clip_name, bool is_selected) const;
	
	void drawPointItem(QPainter *painter, const QStyleOptionViewItem &option, OvrTMLPointItem* item, float a_wnd_port_x) const;
	void drawPoint(QPainter *painter, const QStyleOptionViewItem &option, const QMap<ovr::uint64, char>& keys) const;

	bool isExpand(const QModelIndex& index) const;

	QIcon* actor_icon;
	QIcon* ani_icon;
	QIcon* audio_icon;
	QString add_track_text;
	QString transparent_value_text;
	QImage* eye_img_opened = nullptr;
	QImage* eye_img_closed = nullptr;
	QImage* config_img = nullptr;

	QImage* add_key_button_img_nor = nullptr;
	QImage* add_key_button_img_sel = nullptr;
	bool is_show_value_only = false;
	bool is_show_head_only = false;

	QImage* key_point_img = nullptr;
	QImage* key_point_img_sel = nullptr;
};
