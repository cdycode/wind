#pragma once

#include <QTreeView>

class OvrTimeLineTreeItem;

class OvrTimeLineTreeCurve : public QTreeView, public OvrEventReceiver
{
	Q_OBJECT
public:
	OvrTimeLineTreeCurve(QWidget *parent = Q_NULLPTR);
	bool locateItem(QModelIndex a_index);

	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;
	virtual QString name() override { return "OvrTimeLineTreeCurve"; }

signals:
	void onTrackSelected(QString a_actor_id, ovr_core::OvrTrack* a_track);
	void OnSwitchBack();

public slots:
	void onPlayPosChanged(ovr::uint64 a_frame_index);

protected:
	virtual void currentChanged(const QModelIndex &current, const QModelIndex &previous) override;

	void updateItemIfNeeded();
	void updateItemIfNeeded(OvrTimeLineTreeItem* a_item, int a_value);

	ovr_engine::OvrActor* cur_actor = nullptr;
	ovr_core::OvrTrack* pos_track = nullptr;
	ovr_core::OvrTrack* float_track = nullptr;
};