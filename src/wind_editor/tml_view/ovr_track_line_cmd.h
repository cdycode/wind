#pragma once

#include <wx_project/command/ovr_command_manager.h>
#include <wx_project/ovr_scene_editor.h>
#include <wx_project/ovr_actor_track_line.h>

class OvrTrackLineCmd : public ovr_project::IOvrCommand
{
public:
	OvrTrackLineCmd(OvrString a_actor_id, std::vector<OvrVector3>* a_lines, std::vector<OvrVector3>* a_points, int a_pt_selected = -1, const OvrColorValue& a_color = OvrColorValue::White)
	{
		cmd_type = kCtSetData;
		actor_id = a_actor_id;
		lines = a_lines;
		points = a_points;
		select_pt = a_pt_selected;
		select_pt_color = a_color;
	}

	OvrTrackLineCmd(bool a_show)
	{
		cmd_type = kCTShow;
		is_show = a_show;
	}

	OvrTrackLineCmd(int a_pt_selected = -1, const OvrColorValue& a_color = OvrColorValue::White)
	{
		cmd_type = kCtChangeColor;
		select_pt = a_pt_selected;
		select_pt_color = a_color;
	}

	virtual bool Do() override
	{
		switch (cmd_type)
		{
		case OvrTrackLineCmd::kCTNone:
			break;
		case OvrTrackLineCmd::kCTShow:
			ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor()->GetActorTrackLine().show(is_show);
			break;
		case OvrTrackLineCmd::kCtSetData:
			ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor()->GetActorTrackLine().setTrackLine(actor_id, lines, points, select_pt, select_pt_color);
			break;
		case OvrTrackLineCmd::kCtChangeColor:
			ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor()->GetActorTrackLine().setSelectPointColor(select_pt, select_pt_color);
			break;
		default:
			assert(false);
			break;
		}
		return true;
	}

protected:
	std::vector<OvrVector3>* lines;
	std::vector<OvrVector3>* points;
	OvrString actor_id;
	bool is_show = false;
	int select_pt = -1;
	OvrColorValue select_pt_color;
	enum CmdType {kCTNone, kCTShow, kCtSetData, kCtChangeColor} cmd_type = kCTNone;
};