#pragma once

#include <QWidget>

class OvrMarkView;
class OvrTMLAxis;
class OvrTMLAxisView;

class OvrTMLAxisPane : public QWidget
{
	Q_OBJECT

public:
	explicit OvrTMLAxisPane(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());

	OvrMarkView* getMarkView() { return mark_view; }
	OvrTMLAxisView* getAxisView();

protected:
	virtual void focusOutEvent(QFocusEvent *event) override;
	virtual void keyPressEvent(QKeyEvent *event) override;

	virtual void wheelEvent(QWheelEvent * event)override;

	OvrMarkView* mark_view;
	OvrTMLAxis* time_line_axis;
};