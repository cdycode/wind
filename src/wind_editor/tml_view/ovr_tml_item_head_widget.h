#pragma once

#include <QWidget>

class OvrTMLItem;

class OvrTMLItemHeadWidget : public QWidget, public OvrEventSender
{
	Q_OBJECT

public:
	explicit OvrTMLItemHeadWidget(QWidget *parent = Q_NULLPTR);
	~OvrTMLItemHeadWidget();

	virtual QString senderName() const override { return "OvrTMLItemHeadWidget"; }

	void initTrackContext(const QString& a_title, ovr_core::OvrTrack* a_track, OvrTMLItem* a_item);

protected:
	virtual void paintEvent(QPaintEvent *event) override;
	virtual void contextMenuEvent(QContextMenuEvent *event) override;

	void onBarAction(QAction* action);
	void addChildTrack();
	void addKeyFrame();

	void initTitleColor(ovr_core::OvrTrack* a_track);

	QToolBar* fun_bar = nullptr;
	QLabel*	  title_label = nullptr;

	OvrTMLItem* bind_item = nullptr;
};