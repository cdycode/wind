#pragma once

#include <QTreeWidget>

class OvrTMLItem;

class OvrTMLTrackTree :	public QTreeWidget, public OvrEventReceiver
{
	Q_OBJECT

public:
	explicit OvrTMLTrackTree(QWidget *parent = Q_NULLPTR);

	virtual QString name() override { return "OvrTMLTrackTree"; }
	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;

protected:
	void initStyle();

	QMap<QString, OvrTMLItem*> actor_items;
};
