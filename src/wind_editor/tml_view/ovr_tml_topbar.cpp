#include "Header.h"
#include "ovr_tml_topbar.h"
#include "../project/ovr_player_listener.h"

#include "ovr_time_line_tree.h"
#include "ovr_time_line_tree_model.h"
#include "ovr_tool_button.h"

#include "ovr_curve_editor.h"
#include "ovr_time_line_curve_view.h"
#include "ovr_timeline_tree_curve.h"
#include "ovr_tml_axis.h"

#define kButtonSpace 20

static const char* ce_buttons[] = {
	"ce_fit_h", "ce_fit_w",
	"ce_bezier_auto", "ce_bezier_man",
	"ce_line" ,"ce_jump",
	"ce_bezier_devorce"
};

static const char* ce_status_buttons[] = {
	"ce_bezier_auto", "ce_bezier_man",
	"ce_line" ,"ce_jump"
};

////////////////////////////////////////////////////
// class OVRTMLTopBar

OVRTMLTopBar::OVRTMLTopBar(QWidget* parent, Qt::WindowFlags f) :QWidget(parent, f)
{
	initUI();

	QStringList e = { "onProjectOpen", "onCurrentSceneChanged" };

	registerEventListener(e);

	connect(OvrPlayerListener::getInst(), &OvrPlayerListener::onPlayStatusChanged, this, &OVRTMLTopBar::onPlayStatusChanged, Qt::QueuedConnection);
}

void OVRTMLTopBar::initUI()
{
	QHBoxLayout* h_layout = new QHBoxLayout;
	h_layout->setContentsMargins(0, 0,0,0);
	h_layout->setSpacing(0);
	setLayout(h_layout);

	//片段标题
	h_layout->addSpacing(20);	

	QLabel* label = new QLabel(QString::fromLocal8Bit("片段"));
	h_layout->addWidget(label);

	//片段选择对话框
	h_layout->addSpacing(10);
	QComboBox* box = new QComboBox;
	box->setView(new QListView());
	{
		box->setFixedSize(QSize(150, 24));
		connect(box, SIGNAL(currentIndexChanged(int)), this, SLOT(currentIndexChanged(int)));
		sequence_box = box;
	}
	h_layout->addWidget(box);

	//工具栏按钮
	h_layout->addSpacing(100);
	{
		AddToolButton("save", QString::fromLocal8Bit("保存..."))->setIcons("save", "", "time_line/");
		AddToolButton("saveas", QString::fromLocal8Bit("另存..."))->setIcons("saveas", "", "time_line/");

		h_layout->addSpacing(kButtonSpace);

		AddToolButton("undo", QString::fromLocal8Bit("撤销"))->setIcons("forward", "", "time_line/");
		AddToolButton("redo", QString::fromLocal8Bit("重做"))->setIcons("backward", "", "time_line/");

		h_layout->addSpacing(kButtonSpace);

		AddToolButton("config", QString::fromLocal8Bit("设置..."))->setIcons("set", "", "time_line/");
		h_layout->addSpacing(kButtonSpace);
		AddToolButton("mark", QString::fromLocal8Bit("添加标记点..."))->setIcons("mark", "", "time_line/");

		h_layout->addSpacing(kButtonSpace);

		AddToolButton("play", QString::fromLocal8Bit("播放"))->setIcons("play", "stop", "time_line/");
		AddToolButton("play_vr", QString::fromLocal8Bit("VR播放"))->setIcons("vrplay", "vrstop", "time_line/");
		AddToolButton("recplay", QString::fromLocal8Bit("录制动捕或表情"))->setIcons("recplay", "recstop", "time_line/");

		h_layout->addSpacing(kButtonSpace);
		{
			curve_buttons_layout = new QHBoxLayout;
			curve_buttons_layout->setSpacing(0);
			curve_buttons_layout->setMargin(0);
			
			AddToolButton("edit_curve", QString::fromLocal8Bit("曲线编辑器"), curve_buttons_layout)->setIcons("curve_edit", "curve_edited", "time_line/");
			curve_buttons_layout->addSpacing(5);
			AddToolButton("ce_fit_w", QString::fromLocal8Bit("适应宽度"), curve_buttons_layout)->setIcons("ce_fit_w", "", "time_line/")->hide();
			AddToolButton("ce_fit_h", QString::fromLocal8Bit("适应高度"), curve_buttons_layout)->setIcons("ce_fit_h", "", "time_line/")->hide();
			curve_buttons_layout->addSpacing(5);
			AddToolButton("ce_bezier_auto", QString::fromLocal8Bit("自动贝塞尔"), curve_buttons_layout)->setIcons("ce_bezier_auto", "ce_bezier_auto_s", "time_line/")->hide();
			AddToolButton("ce_bezier_man", QString::fromLocal8Bit("用户贝塞尔"), curve_buttons_layout)->setIcons("ce_bezier_man", "ce_bezier_man_s", "time_line/")->hide();
			AddToolButton("ce_line", QString::fromLocal8Bit("线性"), curve_buttons_layout)->setIcons("ce_line", "ce_line_s", "time_line/")->hide();
			AddToolButton("ce_jump", QString::fromLocal8Bit("阶跃"), curve_buttons_layout)->setIcons("ce_jump", "ce_jump_s", "time_line/")->hide();
			AddToolButton("ce_bezier_devorce", QString::fromLocal8Bit("分离控制"), curve_buttons_layout)->setIcons("ce_bezier_devorce", "ce_bezier_devorce_s", "time_line/")->hide();

			h_layout->addLayout(curve_buttons_layout);
		}
	}
}

 void OVRTMLTopBar::paintEvent(QPaintEvent *event)
 {
 	ApplyWidgetStyleSheet(this);

	 QWidget::paintEvent(event);
 }

void OVRTMLTopBar::currentIndexChanged(int index)
{
	qDebug() << "sequnce combox selected : " << index << ", " << sequence_box->itemText(index);
	notifyEvent("onCurrentSeqChanged", sequence_box->itemData(index).toString(), 0);
}

void OVRTMLTopBar::onProjectOpen(bool, QString)
{
	if (sequence_box->count() > 0)
	{
		sequence_box->clear();
	}
}

OvrToolButton* OVRTMLTopBar::AddToolButton(const QString& a_obj_name, const QString& a_tip, QLayout* a_layout)
{
	OvrToolButton* button = new OvrToolButton;
	button->setObjectName(a_obj_name);
	if (a_tip.length() > 0)
	{
		button->setToolTip(a_tip);
	}
	connect(button, &OvrToolButton::clicked, this, &OVRTMLTopBar::onToolBarButtonClicked, Qt::QueuedConnection);

	if (a_layout == nullptr)
	{
		a_layout = layout();
	}
	a_layout->addWidget(button);
	buttons[a_obj_name] = button;
	return button;
}

void OVRTMLTopBar::onCurrentSceneChanged()
{
	int select_index = -1;
	const std::list<OvrString>& id_list = ovr_project::OvrProject::GetIns()->GetCurrentScene()->GetSequenceNameList();
	
	{
		ovr_project::OvrAssetManager* asset_manager = ovr_project::OvrProject::GetIns()->GetAssetManager();

		const char* cur_seq_id = nullptr;
		ovr_core::OvrSequence* current_seq = ovr_project::OvrProject::GetIns()->GetCurrentSequence();
		if (current_seq != nullptr)
		{
			cur_seq_id = current_seq->GetUniqueName().GetStringConst();
		}

		int item_index = -1;
		for (auto seq_id : id_list)
		{
			++item_index;
			OvrString asset_name, asset_path;
			bool ret = asset_manager->GetSequenceFilePath(seq_id, asset_path);
			assert(ret);

			ret = asset_manager->GetAssetDisplayName(asset_path, asset_name);
			assert(ret);

			QString id = seq_id.GetStringConst();
			sequence_box->addItem(QString::fromLocal8Bit(asset_name.GetStringConst()), id);
			if (select_index == -1 && cur_seq_id != nullptr && id == cur_seq_id)
			{
				select_index = item_index;
			}
		}
	}

	//select first sequence
	if (select_index == -1 && id_list.size() > 0)
	{
		select_index = 0;
	}
	sequence_box->setCurrentIndex(0);
}


void OVRTMLTopBar::onToolBarButtonClicked(OvrToolButton* a_button)
{
	assert(a_button != nullptr);

	QString obj_name = a_button->objectName();

	if (obj_name == "play")
	{
		bool is_playing = a_button->isChecked();

		is_playing ? OvrTimeLineTreeModel::getInst()->pause() : OvrTimeLineTreeModel::getInst()->play(false);
		a_button->setCheck(!is_playing);
		buttons["play_vr"]->setEnabled(is_playing);
		buttons["recplay"]->setEnabled(is_playing);
	}
	else if (obj_name == "play_vr")
	{
		bool is_playing = a_button->isChecked();

		is_playing ? OvrTimeLineTreeModel::getInst()->pause() : OvrTimeLineTreeModel::getInst()->play(false, true);
		a_button->setCheck(!is_playing);
		buttons["play"]->setEnabled(is_playing);
		buttons["recplay"]->setEnabled(is_playing);
	}
	else if (obj_name == "recplay")
	{
		bool is_playing = a_button->isChecked();
		is_playing ? OvrTimeLineTreeModel::getInst()->pause() : OvrTimeLineTreeModel::getInst()->play(true);
		a_button->setCheck(!is_playing);
		buttons["play"]->setEnabled(is_playing);
		buttons["play_vr"]->setEnabled(is_playing);
	}
	else if (obj_name == "mark")
	{
		int frame = round(getGlobalAxisView()->getCurFrame());
		OvrTimeLineTreeModel::getInst()->addMark(frame);
	}
	else if (obj_name == "config")
	{
		OvrTimeLineTreeModel::getInst()->configSequcence();
	}
	else if (obj_name == "edit_curve")
	{
		bool is_show = !a_button->isChecked();
		showCurveEditorButtons(is_show);
		notifyEvent("onCurveEditor", obj_name, is_show);
	}
	else for (auto button_name : ce_buttons)
	{
		if (obj_name == button_name)
		{
			notifyEvent("onCurveEditor", obj_name, 0);
			break;
		}
	}
}

void OVRTMLTopBar::showCurveEditorButtons(bool show_editor)
{
	buttons["edit_curve"]->setCheck(show_editor);

	for (auto obj_name : ce_buttons)
	{
		buttons[obj_name]->setVisible(show_editor);
	}
}

void OVRTMLTopBar::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{
	if (a_event_name == "onProjectOpen")
	{
		onProjectOpen(a_param1.toBool(), a_param2.toString());
	}
	else if (a_event_name == "onCurrentSceneChanged")
	{
		onCurrentSceneChanged();
	}
}

void OVRTMLTopBar::onPlayStatusChanged(QString a_status)
{
	static const char* button_names[] = { "play_vr", "recplay", "play"};
	if (tml_status != a_status)
	{
		qDebug() << "onPlayStatusChanged " << a_status;
		tml_status = a_status;
		if (tml_status == "pause")
		{
			for (int i = 0; i < 3; ++i)
			{
				buttons[button_names[i]]->setCheck(false);
				buttons[button_names[i]]->setDisabled(false);
			}

			bool is_modified = false;
			if (OvrTimeLineTreeModel::getInst()->GetCaptureTrackCount(&is_modified) > 0 && is_modified)
			{
				OvrTimeLineTreeModel::getInst()->saveRecord();
			}
		}
	}
}

void OVRTMLTopBar::onUpdatePointStatus(QString a_mode, bool a_is_devorce)
{
	if (a_mode.isEmpty()) for (auto button : ce_status_buttons)
	{
		buttons[button]->setCheck(false);
		buttons["ce_bezier_devorce"]->setCheck(false);
	}
	else for (auto button : ce_status_buttons)
	{
		buttons[button]->setCheck(a_mode == button);
	}
	buttons["ce_bezier_devorce"]->setCheck(a_is_devorce);
}