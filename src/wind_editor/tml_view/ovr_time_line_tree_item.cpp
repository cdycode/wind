#include "Header.h"
#include "ovr_time_line_tree_item.h"
#include "ovr_actor_item.h"
#include "ovr_timelinel_item.h"

#include "ovr_tml_axis.h"

#include <QList>

static QMap<OvrTimeLineTreeItem*, int> g_update_with_time_itmes;
QList<OvrTimeLineTreeItem*> OvrTimeLineTreeItem::all_clip_list;

const QMap<OvrTimeLineTreeItem*, int>& OvrTimeLineTreeItem::getItemsUpdateWithTime()
{
	return g_update_with_time_itmes;
}

bool OvrTimeLineTreeItem::HasItemAcceptAsset(const QString& a_asset_path)
{
	for (auto item : all_clip_list)
	{
		if (item->isAcceptAsset(a_asset_path))
		{
			return true;
		}
	}
	return false;
}

OvrTimeLineTreeItem* CreateTMLTreeItem(const char* a_name, OvrTimeLineTreeItem* a_parent, ovr_core::OvrTrack* a_track, bool is_append)
{
	QString name = QString::fromLocal8Bit(a_name);
	if (a_track == nullptr)
	{
		return new OvrTimeLineTreeItem(name, a_parent);
	}

	OvrTimeLineTreeItem* item = nullptr;

	if (a_track->GetTrackType() == ovr_core::kTrackClip)
	{
		item = new OvrTMLClipItem(name, a_parent);
	}
	else if (a_track->GetTrackType() == ovr_core::kTrackPoint)
	{
		item = new OvrTMLPointItem(name, a_parent);
	}
	else
	{
		item = new OvrTimeLineTreeItem(name, a_parent);
	}

	item->setTrack(a_track);

	if (is_append)
	{
		a_parent->appendChild(item);
	}

	return item;
}

OvrTimeLineTreeItem* CreateTMLTreeItem(const OvrString& a_name, OvrTimeLineTreeItem* a_parent, ovr_core::OvrTrack* a_track, bool is_append)
{
	OvrTimeLineTreeItem* item =  CreateTMLTreeItem(a_name.GetStringConst(), a_parent, a_track, is_append);
	
	const std::list<ovr_core::OvrTrack*>& child_tracks = a_track->GetChildren();
	for (auto child_track : child_tracks)
	{
		CreateTMLTreeItem(child_track->GetDisplayName(), item, child_track, true);
	}
	return item;
}

OvrTimeLineTreeItem::OvrTimeLineTreeItem(const QString& a_title, OvrTimeLineTreeItem *a_parent_Item)
{
	title = a_title;
	parent_Item = a_parent_Item;
}

void OvrTimeLineTreeItem::setTrack(ovr_core::OvrTrack* a_track)
{ 
	item_track = a_track;

	if (isClipDataItem())
	{
		ovr_core::OvrClipInfo::Type clip_type = static_cast<ovr_core::OvrClipTrack*>(a_track)->GetClipType();
		if (clip_type == ovr_core::OvrClipInfo::kText)
		{
			can_add_key_frame = true;
		}
	}
	else if (isPointDataItem())
	{
		g_update_with_time_itmes[this] = kUpdateItemHead;
		can_add_key_frame = true;
	}
	else 
	{
		is_container_item = true;// a_track->GetTrackType() == ovr_core::kTrackContainer;
	}
}

ovr_core::OvrActorTrack* OvrTimeLineTreeItem::getActorTrack()
{
	OvrTimeLineTreeItem* item = this;
	while (item != nullptr)
	{
		if (item->getTrack() != nullptr && item->isActorItem())
		{
			return static_cast<ovr_core::OvrActorTrack*>(item->getTrack());
		}
		item = item->parent_Item;
	}
	return nullptr;
}

OvrTimeLineTreeItem::~OvrTimeLineTreeItem()
{
	if (g_update_with_time_itmes.contains(this))
	{
		g_update_with_time_itmes.remove(this);
	}

	clearChild();
}

void OvrTimeLineTreeItem::clearChild()
{
	qDeleteAll(child_Items);
	child_Items.clear();
}

void OvrTimeLineTreeItem::appendChild(OvrTimeLineTreeItem *child)
{
	assert(child->getTrack() != nullptr);

	child_Items.append(child);
}

int OvrTimeLineTreeItem::childCount() const
{
	return child_Items.count();
}

OvrTimeLineTreeItem *OvrTimeLineTreeItem::child(int a_row)
{
	assert(a_row >= 0 && a_row < child_Items.size());

    return child_Items.at(a_row);
}


int OvrTimeLineTreeItem::columnCount() const
{
    return 3;
}

QVariant OvrTimeLineTreeItem::data(int column) const
{
	if (column == 0)
	{
		return title;
	}
	return "";
}

OvrTimeLineTreeItem* OvrTimeLineTreeItem::parentItem()
{
    return parent_Item;
}

int OvrTimeLineTreeItem::getParents(QList<OvrTimeLineTreeItem*>& a_list)
{
	a_list.push_front(this);
	OvrTimeLineTreeItem* item = parent_Item;
	while (item != nullptr && item->getTrack() != nullptr)
	{
		a_list.push_front(item);
		item = item->parent_Item;
	}
	return a_list.size();
}

int OvrTimeLineTreeItem::row() const
{
	if (parent_Item != nullptr)
	{
		return parent_Item->child_Items.indexOf(const_cast<OvrTimeLineTreeItem*>(this));
	}

    return 0;
}

void OvrTimeLineTreeItem::removeChildItem(OvrTimeLineTreeItem* a_item)
{
	assert(a_item != nullptr);
	assert(child_Items.indexOf(a_item) != -1);

	child_Items.removeOne(a_item);
	delete a_item;
}

bool OvrTimeLineTreeItem::canHaveChildTrack()
{
	return item_track->HasSupportTrackList();
}

int OvrTimeLineTreeItem::key_point_range = 5;

void OvrTimeLineTreeItem::setKeyPointRange(int a_width)
{
	key_point_range = a_width;
}

int OvrTimeLineTreeItem::getKeyPointRange(int drag_pos, int& left_pos, int& right_pos)
{
	left_pos = drag_pos - key_point_range;
	right_pos = drag_pos + key_point_range;

	if (left_pos < 0)
	{
		left_pos = 0;
	}

	return key_point_range;
}

bool OvrTimeLineTreeItem::draging(int a_pos)
{
	int frame = getGlobalAxisView()->getFrameForGlobalDPX(a_pos);

	if (drag_mode == kDMMove)
	{
		return dragingMove(frame);
	}

	return dragingResize(frame);
}

bool OvrTimeLineTreeItem::cloneDragInfo(OvrTimeLineTreeItem* a_item)
{
	select_key = a_item->select_key;
	drag_pos = a_item->drag_pos;
	drag_key_head = a_item->drag_key_head;
	drag_mode = a_item->drag_mode;
	return true;
}

OvrTimeLineTreeItem* OvrTimeLineTreeItem::getActorItem()
{
	OvrTimeLineTreeItem* item = this;
	while (item != nullptr)
	{
		if (item->isActorItem())
		{
			return item;
		}
		item = item->parentItem();
	}
	return nullptr;
}

bool OvrTimeLineTreeItem::isNeedUpdateCol0WithTimeChange()
{
	if (item_track != nullptr)
	{
		//return item_track->GetTrackType() == ovr_core::kTrackFloat;;
	}
	return false;
}

bool OvrTimeLineTreeItem::isActorItem()
{
	return false;//item_track->GetTrackType() == ovr_core::kTrackActor;
}

bool OvrTimeLineTreeItem::isFaceCapture() 
{ 
	return false;//item_track->GetTrackType() == ovr_core::kTrackMorphCapture;
}

bool OvrTimeLineTreeItem::isMotionCapture() 
{
	return false;//item_track->GetTrackType() == ovr_core::kTrackSkeletalCapture;
}

bool OvrTimeLineTreeItem::isInteractive()
{
	return false;//item_track->GetTrackType() == ovr_core::kTrackInteractive;
}

bool OvrTimeLineTreeItem::isSpecialItem()
{
	auto track_type = item_track->GetTrackType();

	/*return track_type == ovr_core::kTrackEvent
		|| track_type == ovr_core::kTrackInteractive
		|| track_type == ovr_core::kTrackGroup
		|| track_type == ovr_core::kTrackBackgroundAudio;*/
	return false;
}