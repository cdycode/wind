#pragma once

#include <QHeaderView>

class OvrTMLAxisPane;
class OvrTMLTrackToolBar;

class OvrTimeLineHead :	public QHeaderView, public OvrEventReceiver
{
	Q_OBJECT
public:
	explicit OvrTimeLineHead(Qt::Orientation orientation, QWidget *parent = Q_NULLPTR);
	~OvrTimeLineHead();

	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;
	virtual QString name() override { return "OvrTimeLineHead"; }

public slots:
	void onSectionResized(int logicalIndex, int oldSize, int newSize);
	void onSeuenceqOpenend(int a_duration, int a_fps);

protected:
	void initStyle();
	void initUI();

protected:
	virtual void resizeEvent(QResizeEvent *event) override;
	virtual void paintSection(QPainter *painter, const QRect &rect, int logicalIndex) const override;

	bool is_size_adjusted = false;

	OvrTMLAxisPane* tml_group;
	OvrTMLTrackToolBar* track_toolbar;
};