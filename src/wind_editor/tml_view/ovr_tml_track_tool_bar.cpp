﻿#include "Header.h"
#include <wx_project/ovr_timeline_editor.h>
#include "ovr_tml_track_tool_bar.h"

#include "ovr_time_line_tree_model.h"


/////////////////////////////////////////////////////////////////////
// class OvrTrackToolBar

OvrTMLTrackToolBar::OvrTMLTrackToolBar(QWidget* parent, Qt::WindowFlags f) :QWidget(parent, f)
{
	setStyleSheet("background:#2F2F2F;color:white");

	QHBoxLayout* layout = new QHBoxLayout;
	layout->setContentsMargins(5, 0, 5, 0);
	layout->setSpacing(5);
	{
		QToolBar* bar = new QToolBar;
		bar->setFixedHeight(24);
		bar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
		{
			bar->setStyleSheet("background:#417505;color:#DDDDDD;border-radius: 2px;");
			QAction* action = bar->addAction(QIcon(getImagePath("icon_add.png")), QString::fromLocal8Bit("添加"));
			bar->widgetForAction(action)->setFixedWidth(70);
			action->setObjectName("add...");
			tml_menu = new OvrTMLAddMenu(this);
			action->setMenu(tml_menu);
			connect(tml_menu, &OvrTMLAddMenu::triggered, this, &OvrTMLTrackToolBar::actionTriggered, Qt::QueuedConnection);
			connect(bar, &QToolBar::actionTriggered, this, &OvrTMLTrackToolBar::actionTriggered, Qt::QueuedConnection);
		}
		layout->addWidget(bar);
	}
	QLineEdit* editor = new QLineEdit(this);
	QString line_edit_style_sheet = QString::fromLocal8Bit(
		"QLineEdit {border:1px solid rgb(34,34,34); background:rgb(36,36,36); color:rgb(204,204,204); selection-background-color: rgb(165, 165, 165); selection-color: rgb(32, 32, 32); border-radius: 2px;}"
	);
	editor->setStyleSheet(line_edit_style_sheet);
	editor->setPlaceholderText(QString::fromLocal8Bit("请输入关键字搜索"));

	QAction * action = new QAction(QIcon(getImagePath("icon_search_min.png")), "");
	editor->addAction(action, QLineEdit::LeadingPosition);
	editor->setMaxLength(100);
	editor->setFixedHeight(24);
	layout->addWidget(editor, 0);
	setLayout(layout);
}

void OvrTMLTrackToolBar::actionTriggered(QAction *action)
{
	QString name = action->objectName();
	if (name == "add...")
	{
		tml_menu->initContextMenu();
		QWidget* p = action->parentWidget();
		assert(p->inherits("QToolBar"));
		QToolBar* bar = static_cast<QToolBar*>(p);
		QWidget* w = bar->widgetForAction(action);
		QPoint pt = w->geometry().bottomLeft();
		pt.setY(bar->height() + 1);
		tml_menu->move(bar->mapToGlobal(pt));
		tml_menu->show();
	}
	else if (name == "add_actor_track")
	{
		notifyEvent("onAddActorTrack", action->data(), 0);
	}
	else if (name == "add_main_track")
	{
		notifyEvent("onAddMainTrack", action->property("type"), action->property("tag"));
	}
}

void OvrTMLTrackToolBar::paintEvent(QPaintEvent *e)
{
	ApplyWidgetStyleSheet(this);
}

/////////////////////////////////////////////////////////////////////
// class OvrTMLAddMenu

OvrTMLAddMenu::OvrTMLAddMenu(QWidget *parent) :QMenu(parent)
{
}

QAction* OvrTMLAddMenu::createMenuItem(const char* a_title, const char* a_name, const QString& a_icon_path)
{
	QAction* action = addAction(QString::fromLocal8Bit(a_title));
	action->setObjectName(a_name);

	if (!a_icon_path.isEmpty())
	{
		action->setIcon(QIcon(a_icon_path));
	}

	return action;
}

void OvrTMLAddMenu::initContextMenu()
{
	clear();

	QString style = "QMenu {background:rgb(37,37,38);color:white}"
		"\nQMenu::item:selected {background: rgba(100, 100, 100, 255)}"
		"\nQMenu::item:disabled {color:gray}";
	setStyleSheet(style);//144, 200, 246

						 //createMenuItem("文件夹", "add_folder", getImagePath("add_folder.png"))->setEnabled(false);

	const std::list<ovr_core::OvrTrackSupport> support_list = ovr_project::OvrProject::GetIns()->GetCurrentSequence()->GetSupportTrackList();

	for (auto track_support : support_list)
	{
		if (track_support.tag == "actor")
		{
			QMenu* sub_menu = addMenu(QIcon(getImagePath("add_object.png")), QString::fromLocal8Bit(track_support.name.GetStringConst()));
			addActorMenu(sub_menu, track_support.is_enable);
		}
		else
		{
			QAction* action = addAction(QString::fromLocal8Bit(track_support.name.GetStringConst()));
			action->setObjectName("add_main_track");
			action->setProperty("type", track_support.track_type);
			action->setProperty("tag", QString::fromLocal8Bit(track_support.tag.GetStringConst()));
			action->setEnabled(track_support.is_enable);
			addIcon(track_support.track_type, action);
		}
	}

}

void OvrTMLAddMenu::addIcon(int a_type, QAction* action)
{
	/*switch (a_type)
	{
	case ovr_core::kTrackBackgroundAudio:
	action->setIcon(QIcon(getImagePath("add_bgn_audio.png")));
	break;
	case ovr_core::kTrackInteractive:
	case ovr_core::kTrackEvent:
	action->setIcon(QIcon(getImagePath("add_object.png")));
	break;
	default:
	break;
	}*/
}

void OvrTMLAddMenu::addActorMenu(QMenu* a_sub_menu, bool a_is_enable)
{
	std::list<ovr_project::OvrActorName> actor_name;
	ovr_project::OvrProject::GetIns()->GetCurrentTimelineEditor()->GetAddActorList(actor_name);
	for (auto& item : actor_name)
	{
		QAction* action = a_sub_menu->addAction(QString::fromLocal8Bit(item.display_name.GetStringConst()));
		action->setObjectName("add_actor_track");
		action->setData(QString::fromLocal8Bit(item.unique_name.GetStringConst()));
	}
}