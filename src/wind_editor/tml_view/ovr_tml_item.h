#pragma once

#include <QTreeWidgetItem>

class OvrTMLItemHeadWidget;
class OvrTMLItemBodyWidget;

class OvrTMLItem :public QTreeWidgetItem
{
public:
	static OvrTMLItem* createMainItem(int a_type, const QString& a_tag, QTreeWidget* a_tree);
	static OvrTMLItem* createActorItem(const QString& a_actor_id, QTreeWidget* a_tree);

	OvrTMLItem(ovr_core::OvrTrack* a_track);
	~OvrTMLItem();

	ovr_core::OvrTrack* getTrack() { return track; }

	void init(const QString& display_name);
	void addChildItem(int a_type, const QString& a_tag);

	void remove();
	QString getName() { return item_name; }

protected:
	static OvrTMLItem* createTMLItem(const QString& display_name, ovr_core::OvrTrack* a_track, QTreeWidget* a_tree);

	QString item_name;
	ovr_core::OvrTrack* track = nullptr;

	OvrTMLItemHeadWidget* head_widget = nullptr;
	OvrTMLItemBodyWidget* body_widget = nullptr;
};