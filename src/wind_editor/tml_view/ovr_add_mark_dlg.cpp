#include "Header.h"

#include "ovr_add_mark_dlg.h"
#include "../utils_ui/title_bar.h"


OvrAddMarkDlg::OvrAddMarkDlg(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	//setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

	init_curstom_ui();
	set_custom_style_sheet();
	preset_connects();
}

OvrAddMarkDlg::~OvrAddMarkDlg()
{
}

void OvrAddMarkDlg::setMode(int a_key_pos, const QString& a_key_name, QMap<ovr::uint64, QString>* a_key_items, const QString& a_key_word, bool a_is_add_new)
{
	QString title = a_is_add_new ? QString::fromLocal8Bit("添加") : QString::fromLocal8Bit("重命名");
	title += a_key_word;
	setWindowTitle(title);

	ui.key_name_title->setText(a_key_word);
	key_word = a_key_word;

	key_pos = a_key_pos;
	ui.key_pos->setText(QString::number(a_key_pos));

	key_name = a_key_name;
	ui.key_name->setText(a_key_name);

	key_items = a_key_items;

	is_add_new = a_is_add_new;
}

void OvrAddMarkDlg::init_curstom_ui()
{
	setWindowFlags(windowFlags() | Qt::FramelessWindowHint);

	ui.title_bar->setUiFlags(TitleBar::TitleFlag::TF_BTN_CLOSE);
	//ui.title_bar->setTitle(QString::fromLocal8Bit("添加标记"));

	installEventFilter(ui.title_bar);
	//QString regPosExp = QString("\\d*");
	//QRegExp regExpPos = QRegExp(regPosExp);
	//QRegExpValidator *posRegExpValidator = new QRegExpValidator(regExpPos);
	//ui.key_pos->setInputMethodHints(Qt::ImhDigitsOnly);
	//ui.key_pos->setMaxLength(100);
	//ui.key_pos->setValidator(posRegExpValidator);

	QString regExp = QString("[^\\\\/*:|?\"\\s]*");
	QRegExp regExpPort = QRegExp(regExp);
	QRegExpValidator *portRegExpValidator = new QRegExpValidator(regExpPort);
	ui.key_name->setMaxLength(100);
	ui.key_name->setValidator(portRegExpValidator);

	ui.button_ok->setDefault(true);

}

void OvrAddMarkDlg::set_custom_style_sheet()
{
	QString strDlgStyle = QString::fromLocal8Bit(
		"QWidget:focus{ outline: none;}"
		"QWidget{background-color: rgb(48,48,48);color:rgb(204, 204, 204); font-family:\"微软雅黑\";}"
		"QLabel{font-size:14px;}"
		"QLineEdit{height: 26px; border-radius: 2px; background-color:rgb(62, 62, 62); border:1px solid rgb(76, 76, 76); border-radius:2px; font-size:12px; selection-background-color: rgb(165, 165, 165); selection-color: rgb(32, 32, 32);}"
		"QPushButton{width: 138px; height: 38px; color: rgb(205, 205, 205); border: 1px solid rgb(30, 30, 30); border-radius: 4px; background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(118, 118, 118), stop:1 rgb(87, 87, 87)); }"
		"QPushButton:hover{background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgb(153, 153, 153), stop:1 rgb(109, 109, 109)); }"
		"QPushButton:pressed {background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:1 rgb(118, 118, 118), stop:0 rgb(87, 87, 87)); }"
		"QPushButton:disabled {background-color:rgb(56, 56, 56); color:rgb(76, 76, 76);  border: 1px solid rgb(76, 76, 76); }"

		"QMessageBox {color: rgb(204, 204, 204); background-color: rgba(48, 48, 48); border-radius: 2px; messagebox-text-interaction-flags: 5 }"
		"QMessageBox QPushButton {border: 1px solid rgb(30, 30, 30); border-radius: 2px; width: 70px; height: 25px; background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgba(118, 118, 118, 255), stop:1 rgba(87, 87, 87, 255));}"
	);
	this->setStyleSheet(strDlgStyle);

	QString strTitleBarStyle = QString::fromLocal8Bit(
		"QWidget{background-color: rgb(48,48,48); color:rgb(204, 204, 204); font-family:\"微软雅黑\"; border:0px solid rgb(76, 76, 76);}"
		"QLabel{font:normal 16px; font-family:\"微软雅黑\";}"
	);
	ui.title_bar->setStyleSheet(strTitleBarStyle);

	//
	QString strLineStyle = QString::fromLocal8Bit("background-color:rgb(59, 59, 59); border-top:1px solid rgb(38, 38, 38); border-bottom:1px solid rgb(48, 48, 48);");
	ui.line->setStyleSheet(strLineStyle);
}

void OvrAddMarkDlg::preset_connects()
{
}

void OvrAddMarkDlg::on_button_ok_clicked()
{
	//标记名字不能为空
	if (ui.key_name->text().isEmpty())
	{
		QMessageBox::warning(this, this->windowTitle(), key_word + QString::fromLocal8Bit("不能为空！"));
		return;
	}

	//标记名字不能重名
	if (key_items != nullptr)
	{
		QList<ovr::uint64> keys = key_items->keys();
		for (auto key : keys)
		{
			if (key_items->value(key) == ui.key_name->text())
			{
				if (is_add_new)
				{
					QMessageBox::warning(this, this->windowTitle(), key_word + QString::fromLocal8Bit("名字不能重名！"));
				}
				else if (key_pos == key)
				{
					QMessageBox::warning(this, this->windowTitle(), QString::fromLocal8Bit("新的") + key_word + QString::fromLocal8Bit("名字未改变！"));
				}
				else
				{
					QMessageBox::warning(this, this->windowTitle(), key_word + QString::fromLocal8Bit("名字不能重名！"));
				}
				return;
			}
		}
	}

	key_name = ui.key_name->text();
	done(1);
}

void OvrAddMarkDlg::on_button_cancel_clicked()
{
	done(0);
}