#include "Header.h"
#include <wx_project/ovr_timeline_editor.h>
#include "ovr_tml_item.h"

#include "ovr_tml_item_head_widget.h"
#include "ovr_tml_item_body_widget.h"

OvrTMLItem* OvrTMLItem::createMainItem(int a_type, const QString& a_tag, QTreeWidget* a_tree)
{
	auto track = ovr_project::OvrProject::GetIns()->GetCurrentSequence()->AddTrack(ovr_core::OvrTrackType(a_type), a_tag.toLocal8Bit().constData());
	if (track != nullptr)
	{
		auto display_name = QString::fromLocal8Bit(track->GetDisplayName().GetStringConst());
		return createTMLItem(display_name, track, a_tree);
	}
	return nullptr;
}

OvrTMLItem* OvrTMLItem::createActorItem(const QString& a_actor_id, QTreeWidget* a_tree)
{
	auto track = ovr_project::OvrProject::GetIns()->GetCurrentTimelineEditor()->AddActorTrack(a_actor_id.toLocal8Bit().constData());
	if (track != nullptr)
	{
		auto display_name = QString::fromLocal8Bit(track->GetActor()->GetDisplayName().GetStringConst());
		return createTMLItem(display_name, track, a_tree);
	}
	return nullptr;
}

OvrTMLItem* OvrTMLItem::createTMLItem(const QString& display_name, ovr_core::OvrTrack* a_track, QTreeWidget* a_tree)
{
	auto item = new OvrTMLItem(a_track);
	a_tree->addTopLevelItem(item);

	item->init(display_name);

	a_tree->scrollToItem(item);
	a_tree->setCurrentItem(item);

	return item;
}

void OvrTMLItem::addChildItem(int a_type, const QString& a_tag)
{
	auto child_track = track->AddTrack(ovr_core::OvrTrackType(a_type), a_tag.toLocal8Bit().constData());
	if (child_track != nullptr)
	{
		auto child_item = new OvrTMLItem(child_track);
		addChild(child_item);

		auto display_name = QString::fromLocal8Bit(child_track->GetDisplayName().GetStringConst());
		child_item->init(display_name);

		treeWidget()->scrollToItem(child_item);
		treeWidget()->setCurrentItem(child_item);

		if (!treeWidget()->isItemExpanded(child_item))
		{
			treeWidget()->setItemExpanded(child_item, true);
		}
	}
}

OvrTMLItem::OvrTMLItem(ovr_core::OvrTrack* a_track):QTreeWidgetItem(QStringList({"", "", ""}), a_track->GetTrackType())
, track(a_track)
{
}

OvrTMLItem::~OvrTMLItem()
{
	//qDebug() << "~OvrTMLItem entered, " << item_name;
}

void OvrTMLItem::init(const QString& display_name)
{
	item_name = display_name;

	head_widget = new OvrTMLItemHeadWidget;
	head_widget->initTrackContext(display_name, track, this);

	body_widget = new OvrTMLItemBodyWidget;
	body_widget->initTrackContext(track, this);

	treeWidget()->setItemWidget(this, 0, head_widget);
	treeWidget()->setItemWidget(this, 1, body_widget);

	if (track->GetChildren().size() > 0)
	{
		for (auto child_track : track->GetChildren())
		{
			auto child_item = new OvrTMLItem(child_track);
			addChild(child_item);

			child_item->init(QString::fromLocal8Bit(child_track->GetDisplayName().GetStringConst()));
		}
	}
}

void OvrTMLItem::remove()
{
	auto parent_track = track->GetParent();
	bool is_ok = parent_track == nullptr ? 
		ovr_project::OvrProject::GetIns()->GetCurrentSequence()->RmvTrack(track) : parent_track->RmvTrack(track);
	
	if (is_ok)
	{
		delete this;
	}
}