#include "Header.h"
#include "ovr_tml_main_pane.h"

#include "ovr_tml_topbar.h"
#include "../utils_ui/ovr_dock_title_bar.h"

#include "ovr_tml_track_tool_bar.h"
#include "ovr_tml_axis.h"
#include "ovr_tml_axis_pane.h"
#include "ovr_tml_track_tree.h"
#include "ovr_tml_curve_editor.h"

// #include "ovr_time_line_tree.h"
// #include "ovr_time_line_tree_model.h"
// #include "ovr_curve_editor.h"
// #include "ovr_time_line_curve_view.h"
// #include "ovr_timeline_tree_curve.h"

#include "../utils_ui/size_dock_widget.h"


////////////////////////////////////////////////////
// class OvrTimelineBar

OvrTMLMainPane::OvrTMLMainPane(QWidget* parent, Qt::WindowFlags f)
	:QDockWidget(QString::fromLocal8Bit("时间线管理器"), parent, f)
{
	setObjectName(QStringLiteral("dockWidget_right"));
	setFeatures(QDockWidget::AllDockWidgetFeatures);
	setAllowedAreas(Qt::BottomDockWidgetArea);

	QSizePolicy size_policy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	size_policy.setHorizontalStretch(0);
	size_policy.setVerticalStretch(0);
	size_policy.setHeightForWidth(sizePolicy().hasHeightForWidth());
	setSizePolicy(size_policy);

	initUI();
	initStyle();

	QStringList e = { "onCurveEditor" };

	registerEventListener(e);
}

void OvrTMLMainPane::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{
	if (a_event_name == "onCurveEditor")
	{
		if (a_param1.toString() == "edit_curve")
		{
			showCurveEditor(a_param2.toBool());
		}
	}
}

OvrTMLMainPane::~OvrTMLMainPane()
{
}

void OvrTMLMainPane::initStyle()
{
	setStyleSheet("background:rgb(24, 24, 24)");
}

void OvrTMLMainPane::initUI()
{
	initBar();
	initContext();
}

void OvrTMLMainPane::initBar()
{
	auto dock_bar = new OVRDockTitleBar;
	dock_bar->setBarContext(new OVRTMLTopBar);

	setTitleBarWidget(dock_bar);
}

void OvrTMLMainPane::initContext()
{
	SizeDockWidget * dockWidgetContents = new SizeDockWidget(this);
	dockWidgetContents->updateSizeHint(QSize(-1, 200));

	g_layout = new QGridLayout(dockWidgetContents);
	g_layout->setContentsMargins(0,1,0,0);
	g_layout->setVerticalSpacing(1);
	g_layout->setHorizontalSpacing(2);
	g_layout->setColumnStretch(1, 100);

	//tool bar
	auto bar = new OvrTMLTrackToolBar;
	bar->setFixedWidth(285);
	g_layout->addWidget(bar, 0, 0);

	//timeline axis view
	g_layout->addWidget(new OvrTMLAxisPane, 0, 1);

	QHBoxLayout* h_layout = new QHBoxLayout;
	h_layout->setMargin(0);
	h_layout->setSpacing(0);

	//track tree
	tree = new OvrTMLTrackTree;
	tree->setColumnCount(2);
	tree->setColumnWidth(0, 285);
	tree->setMinimumWidth(285);
	tree->setHeaderHidden(true);

	h_layout->addWidget(tree);

	//curve view
	curve_editor = new OvrTMLCurveEditor;
	h_layout->addWidget(curve_editor, 100);

	curve_editor->hide();

	//cursor line
	auto axis = getGlobalAxisView();
	assert(axis != nullptr);
	tml_v_align_line = axis->createCursorLine(this);

	g_layout->addLayout(h_layout, 1, 0, 1, 2);

	setWidget(dockWidgetContents);
}

void OvrTMLMainPane::resizeEvent(QResizeEvent *event)
{
	assert(tml_v_align_line != nullptr);
	
	tml_v_align_line->resize({ 1,height() });

	QDockWidget::resizeEvent(event);
}

void OvrTMLMainPane::showCurveEditor(bool a_show)
{
	if (a_show)
	{
		tree->hideColumn(1);
		curve_editor->show();
	}
	else
	{
		curve_editor->hide();
		tree->showColumn(1);
	}
}