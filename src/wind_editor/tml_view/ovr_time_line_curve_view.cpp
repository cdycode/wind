#include "Header.h"
#include "ovr_time_line_curve_view.h"
#include "ovr_tml_axis.h"
#include "wx_base/wx_color_value.h"
#include <QPainter>
#include <QScrollBar>
#include <wx_core/track/ovr_float_track.h>
#include <wx_core/track/ovr_curve_point_manager.h>
#include <wx_project/ovr_render_scene.h>
#include "ovr_track_line_cmd.h"
#include "ovr_time_line_tree_model.h"

#include <wx_project/command/ovr_command_manager.h>

#define kMinUnitSpace  40
#define kSmallYLineLen 2
#define kCtlLineLength 60
#define kPointWH 7

#define kGrayLineColor  "#80494949"
#define kLightGrayLineColor  "#EE494949"
#define kBlackLineColor "#A0090909"
#define kValueTextColor "#696969"

#define kYMargin 10
#define kXMargin 10
#define kYMinSpace 50

#define kBezierNum 8

#define OVRPt2QtPt(a_pt) QPointF(a_pt._x, a_pt._y)
#define QtPt2OVRPt(a_pt) OvrVector2(a_pt.x(), a_pt.y())

//////////////////////////////////////////////////////
// class OvrTimeLineCurveView

OvrTimeLineCurveView::OvrTimeLineCurveView(QWidget* parent, Qt::WindowFlags f) :QWidget(parent, f)
{	
	setStyleSheet("color:red;background:#232323");

	curve_menu = new OvrCuveMenu;
	setMinimumHeight(kYMargin * 2 + kYMinSpace);

	connect(curve_menu, &QMenu::triggered, [this](QAction *action) {
		if (action->objectName() == "rmv_key")
		{
			onRmvPoint();
		}
		else if (action->objectName() == "add_key")
		{
			onAddPoint();
		}
	});

	connect(OvrTimeLineTreeModel::getInst(), &OvrTimeLineTreeModel::onSeuenceqOpenend, [this]() {
		if (isVisible())
		{
			if (is_auto_fit)
			{
				fitWidth();
			}
		}
	});

	connect(getGlobalAxisView(), &OvrTMLAxisView::onWndPortChange, [this]() {
		update();
	});

	connect(getGlobalAxisView(), &OvrTMLAxisView::onScaleChange, [this](bool) {
		update();
	});

}

OvrTimeLineCurveView::~OvrTimeLineCurveView()
{
	delete curve_menu;
	curve_menu = nullptr;
}

void OvrTimeLineCurveView::paintEvent(QPaintEvent *e)
{
	ApplyWidgetStyleSheet(this);

	QPainter painter(this);
	if (show_tracks.size() == 0 && is_auto_center)
	{
		is_auto_center = false;
		wnd_port_y = height() / 2;
	}
	getGlobalAxisView()->initAlignPainer(painter, this, wnd_port_y, true);

	painter.setRenderHint(QPainter::Antialiasing);

	cur_trans = painter.combinedTransform();

	drawAxisXAlignLine(painter);

	drawAxisY(painter);

	drawCurve(painter);
}

void OvrTimeLineCurveView::hideEvent(QHideEvent *event)
{
	if (has_track_line)
	{
		showTrackLine(false);
	}
	QWidget::hideEvent(event);
}

void OvrTimeLineCurveView::drawAxisXAlignLine(QPainter& painter)
{
	int y_head = wnd_port_y ;
	int y_end = wnd_port_y - height();

	//0点x
	painter.setPen(QColor(kGrayLineColor));
	painter.drawLine(0, y_head, 0, y_end);

	int x_max = getGlobalAxisView()->getLPXForFrame(getGlobalAxisView()->getFrameTotal());

	//中点
	int x_mid = x_max / 2;
	painter.drawLine(x_mid, y_head, x_mid, y_end);

	//尾
	painter.drawLine(x_max, y_head, x_max, y_end);
}

void OvrTimeLineCurveView::drawAxisY(QPainter& painter)
{
	float unit_base_space, unit_base_value, unit_num_to_draw;
	calcUnitBase(unit_base_space, unit_base_value, unit_num_to_draw);

	int int_unit_num_to_draw = unit_num_to_draw;
	if (!Float_Equal(unit_num_to_draw, int_unit_num_to_draw))
	{
		++int_unit_num_to_draw;
	}

	//从上到下绘制刻度线以及刻度值
	int unit_num = (int)(wnd_port_y / unit_base_space) + 1;
	float y_head = unit_num * unit_base_space;
	float y_end = wnd_port_y - height();

	for (float y = y_head; y > y_end; y -= unit_base_space)
	{
		const char* color = unit_num % 2 == 0? kGrayLineColor : kBlackLineColor;
		painter.setPen(QColor(color));

		if (unit_num % int_unit_num_to_draw == 0)
		{
			static char value_str[30] = { 0 };
			Float_Equal(unit_base_value, 0.001f) ? sprintf(value_str, "%.3f", unit_num * unit_base_value) : sprintf(value_str, "%.2f", unit_num * unit_base_value);
			drawAxisYUnit(painter, value_str, y);
			drawAxisYSmallUnit(painter, y, unit_base_space / 10, 1);
		}
		else
		{
			drawAxisYSmallUnit(painter, y, unit_base_space / 10, 0);
		}

		--unit_num;
	}
}

void OvrTimeLineCurveView::calcUnitBase(float& a_unit_base_space, float& a_unit_base_value, float& a_unit_num_to_draw)
{
	//计算最小单位刻度的值
	a_unit_base_value = 0.001f;
	float value_space1_per_10 = show_tracks.size() == 0 ? 1 : getYRanges() / 10;
	while (value_space1_per_10 > a_unit_base_value)
	{
		a_unit_base_value *= 10;
	}

	//计算最小单位刻度值对应的像素位置
	a_unit_base_space = calcPointPosY(a_unit_base_value, ratio);

	//单位刻度占据的像素空间最大不超过10倍kMinUnitSpace
	while (a_unit_base_space > 10 * kMinUnitSpace)
	{
		a_unit_base_space /= 10;
		a_unit_base_value /= 10;
	}

	assert(a_unit_base_space > 0);
	a_unit_num_to_draw = kMinUnitSpace / a_unit_base_space;
}

//绘制小刻度, o~9或者1~9
void OvrTimeLineCurveView::drawAxisYSmallUnit(QPainter& painter, float a_y, float a_small_unit_space, int a_head_line)
{
	if (a_small_unit_space >= 2)
	{
		painter.save();
		painter.setPen(QColor(kLightGrayLineColor));

		for (int i = a_head_line; i < 10; ++i)
		{
			int line_len = i == 0 ? kSmallYLineLen * 2 : kSmallYLineLen;
			float small_unit_y = a_y - i * a_small_unit_space;

			painter.drawLine(QPointF(wnd_port_x, small_unit_y), QPointF(wnd_port_x + line_len, small_unit_y));
			painter.drawLine(QPointF(wnd_port_x + width() - line_len, small_unit_y), QPointF(wnd_port_x + width(), small_unit_y));
		}
		painter.restore();
	}
}

//绘制大刻度
void OvrTimeLineCurveView::drawAxisYUnit(QPainter& painter, const QString& a_value, float a_y)
{
	//画线
	painter.drawLine(QPointF(0, a_y), QPointF(wnd_port_x + width(), a_y));

	//绘制刻度值
	QFontMetrics fm(painter.font());
	QSize str_size = fm.size(Qt::TextSingleLine, a_value);

	QRect rc(QPoint(0, a_y - str_size.height() / 2), str_size);
	rc = cur_trans.mapRect(rc);

	painter.save();
	painter.resetMatrix();
	painter.setPen(QColor(kValueTextColor));

	rc.moveLeft(2);
	painter.drawText(rc, a_value, Qt::AlignVCenter | Qt::AlignLeft);

	rc.moveLeft(width() - str_size.width() - 2);
	painter.drawText(rc, a_value, Qt::AlignVCenter | Qt::AlignLeft);

	painter.restore();
}

void OvrTimeLineCurveView::drawCurve(QPainter& painter)
{
	for (auto track : show_tracks)
	{
		auto pt_manager = track->getCurvePointManager();
		drawCurveLine(painter, pt_manager, track->GetTag().GetStringConst());
		drawCurvePoint(painter, pt_manager);
	}
}

//a_line_mode 0:线性变化, 1:阶跃跳变
void OvrTimeLineCurveView::drawCurveLine(QPainter& painter, ovr_core::OvrCurvePointManager* a_pt_manager, const QString& a_tag)
{
	static const char* sel_line_colors[] = { "#EEFF0000","#EE00FF00","#EE0000FF" };
	static const char* nor_line_colors[] = { "#BBFF0000","#BB00FF00","#BB0000FF" };
	static QMap<QString, int> color_maps = { {"X", 0}, { "Y", 1 }, { "Z", 2 } };
	
	int color_index = color_maps.contains(a_tag) ? color_maps[a_tag] : 0;
	auto line_color = cur_pt_manager == a_pt_manager ? sel_line_colors[color_index] : nor_line_colors[color_index];

	painter.setPen(QColor(line_color));
	painter.setBrush(Qt::NoBrush);

	bool is_first_point = true;
	OvrVector2 pt_prev, pt_current;
	int prev_frame, current_frame;

	auto& points = a_pt_manager->getPointsMap();
	for (auto item : points)
	{
		current_frame = item.first;
		pt_current = calcPointPos(item.second->cur_point);

		if (is_first_point)
		{
			is_first_point = false;
			pt_prev = OvrVector2(0, pt_current._y);
			QLineF line(pt_prev._x, pt_prev._y, pt_current._x, pt_current._y);
			painter.drawLine(line);
		}
		else
		{
			ovr_core::OvrCtlPoint::CTLMode point_mode = a_pt_manager->getPoint(prev_frame)->point_mode;

			//线性变化
			if (point_mode == ovr_core::OvrCtlPoint::kCMLine)
			{
				QLineF line(pt_prev._x, pt_prev._y, pt_current._x, pt_current._y);
				painter.drawLine(line);
			}
			//阶跃跳变
			else if (point_mode == ovr_core::OvrCtlPoint::kCMLJump)
			{
				QLineF line(pt_prev._x, pt_prev._y, pt_current._x, pt_prev._y);
				painter.drawLine(line);

				line.setLine(pt_current._x, pt_prev._y, pt_current._x, pt_current._y);
				painter.drawLine(line);
			}
			//贝塞尔曲线
			else
			{
				drawBezier(painter, prev_frame, current_frame, a_pt_manager);
			}
		}
		pt_prev = pt_current;
		prev_frame = current_frame;
	}

	if (!is_first_point)
	{
		pt_current = OvrVector2(wnd_port_x + width(), pt_prev._y);
		painter.drawLine(OVRPt2QtPt(pt_prev), OVRPt2QtPt(pt_current));
	}
}

void OvrTimeLineCurveView::drawBezier(QPainter& painter, int a_begin_frame, int a_end_frame, ovr_core::OvrCurvePointManager* a_pt_manager)
{
	int count = a_end_frame - a_begin_frame + 1;
	std::vector<float> values(count);
	OvrVector2 pt1, pt2;
	a_pt_manager->GetValues(a_begin_frame, a_end_frame, values);
	QPointF* points = new QPointF[count];
	for (int i = 0; i < count; ++i)
	{
		points[i].rx() = calcPointPosX(i + a_begin_frame);
		points[i].ry() = calcPointPosY(values[i], ratio);
	}
	painter.drawPolyline(points, count);
	delete [] points;
}

void OvrTimeLineCurveView::drawCurvePoint(QPainter& painter, ovr_core::OvrCurvePointManager* a_pt_manager)
{
	painter.setPen(Qt::NoPen);

	cur_pt_manager == a_pt_manager ? painter.setBrush(QColor("#FFB8B8B8")) : painter.setBrush(QColor("#BBB8B8B8"));

	OvrVector2 current_point;
	for (auto item : a_pt_manager->getPointsMap())
	{
		current_point = calcPointPos(item.second->cur_point);

		if (cur_pt_manager == a_pt_manager && a_pt_manager->GetSelectFrame() == item.first)
		{
			drawControlLine(painter, item.first, current_point, a_pt_manager);

			painter.save();
			painter.setBrush(QColor("#B8B800"));
			painter.drawEllipse(QPointF(current_point._x, current_point._y), kPointWH / 2, kPointWH / 2);
			painter.restore();
		}
		else
		{
			painter.drawEllipse(QPointF(current_point._x, current_point._y), kPointWH / 2, kPointWH / 2);
		}
	}
}

void OvrTimeLineCurveView::drawControlLine(QPainter& painter, int a_frame, const OvrVector2& a_pt_current, ovr_core::OvrCurvePointManager* a_pt_manager)
{
#define kCtlLineColorLight "#EE00AAAA"
#define kCtlLineColorDark "#9900AAAA"

	OvrVector2 ctl_pt_left, ctl_pt_right;
	getControlPoints(a_frame, ctl_pt_left, ctl_pt_right, true, a_pt_manager);

	painter.save();

	if (a_pt_manager->getPoint(a_frame)->is_move_both)
	{
		drawControlLine(painter, QColor(kCtlLineColorDark), a_pt_current, ctl_pt_left);
		drawControlLine(painter, QColor(kCtlLineColorDark), a_pt_current, ctl_pt_right);
	}
	else
	{
		drawControlLine(painter, QColor(kCtlLineColorDark), a_pt_current, ctl_pt_left);
		drawControlLine(painter, QColor(kCtlLineColorLight), a_pt_current, ctl_pt_right);
	}

	painter.restore();
}

void OvrTimeLineCurveView::drawControlLine(QPainter& painter, const QColor& a_color, const OvrVector2& a_pt_from, const OvrVector2& a_pt_to)
{
	QPen pen(a_color);
	pen.setWidth(2);
	painter.setPen(pen);

	painter.drawLine(OVRPt2QtPt(a_pt_from), OVRPt2QtPt(a_pt_to));

	painter.setPen(Qt::NoPen);
	painter.setBrush(QColor("#B8B800"));

	painter.drawEllipse(OVRPt2QtPt(a_pt_to), kPointWH / 2, kPointWH / 2);
}

OvrVector2 OvrTimeLineCurveView::calcPointPos(const OvrVector2& a_pt_value)
{
	OvrVector2 pos;
	pos._x = calcPointPosX(a_pt_value._x);
	pos._y = calcPointPosY(a_pt_value._y, ratio);
	return pos;
}

float OvrTimeLineCurveView::calcPointPosX(float a_frame)
{
	return getGlobalAxisView()->getLPXForFrame(a_frame);
}

float OvrTimeLineCurveView::calcPointPosY(float a_value, int a_ratio)
{
	return a_ratio * a_value / getGlobalAxisView()->getValuePerPixel();
}

OvrVector2 OvrTimeLineCurveView::calcPointValue(const OvrVector2& a_pt)
{
	OvrVector2 a_value;
	a_value._x = calcPointValueX(a_pt._x);
	a_value._y = calcPointValueY(a_pt._y);
	return a_value;
}

float OvrTimeLineCurveView::calcPointValueX(float a_pos_x)
{
	return getGlobalAxisView()->getFrameForLPX(a_pos_x);
}

float OvrTimeLineCurveView::calcPointValueY(float a_pos_y)
{
	return a_pos_y * getGlobalAxisView()->getValuePerPixel() / ratio;
}

void OvrTimeLineCurveView::getControlPoints(int a_frame, OvrVector2& a_pt_left, OvrVector2& a_pt_right, bool is_user_control, ovr_core::OvrCurvePointManager* a_pt_manager)
{
	OvrVector2 pt_n, pt_cur;

	getNormalLine(a_frame, pt_n, pt_cur, a_pt_manager);

	float line_len = is_user_control ? kCtlLineLength : pt_n.length() / kBezierNum;

	//获取默认的控制点
	a_pt_left = pt_cur - pt_n * (line_len / pt_n.length());
	a_pt_right = pt_cur + pt_n * (line_len / pt_n.length());

	//获取改变了的控制点
	ovr_core::OvrCtlPoint* ctl_point = a_pt_manager->getPoint(a_frame);
	assert(ctl_point != nullptr);

	if (ctl_point->is_left_valid)
	{
		OvrVector2 pt_n_left = pt_cur - calcPointPos(ctl_point->ctl_pt_left_value);
		a_pt_left = pt_cur - pt_n_left * (line_len / pt_n_left.length());
	}
	if (ctl_point->is_right_valid)
	{
		OvrVector2 pt_n_right = calcPointPos(ctl_point->ctl_pt_right_value) - pt_cur;
		a_pt_right = pt_cur + pt_n_right * (line_len / pt_n_right.length());
	}
}

void OvrTimeLineCurveView::getNormalLine(int a_frame, OvrVector2& a_pt_n, OvrVector2& a_pt_current, ovr_core::OvrCurvePointManager* a_pt_manager)
{
	OvrVector2 pt_prev, pt_next;
	a_pt_manager->getPoints(a_frame, a_pt_current, pt_prev, pt_next);

	a_pt_current = calcPointPos(a_pt_current);
	pt_prev = calcPointPos(pt_prev);
	pt_next = calcPointPos(pt_next);

	a_pt_n = pt_next - pt_prev;
}

void OvrTimeLineCurveView::reset()
{
	actor_id = "";

	//group_track = nullptr;
	show_tracks.clear();
	xyz_tracks.clear();

	cur_track = nullptr;
	cur_pt_manager = nullptr;

	has_track_line = false;

	is_auto_center = true;
}

void OvrTimeLineCurveView::OnSwitchBack()
{
	if (has_track_line)
	{
		for (auto float_track : xyz_tracks)
		{
			auto pt_manager = float_track->getCurvePointManager();
			if (pt_manager->isChanged())
			{
				updateTrackLine();
				return;
			}
		}
		onTrackLinePointSelected();
		showTrackLine(true);
	}
}

void OvrTimeLineCurveView::onTrackSelected(QString a_actor_id, ovr_core::OvrTrack* a_track)
{
	reset();
	actor_id = a_actor_id.toLocal8Bit().constData();

	if (a_track != nullptr || actor_id.GetStringSize() > 0)
	{
		if (!searchFloatTracksForGroup(a_track, true))
		{
			/*if (a_track->GetTrackType() == ovr_core::kTrackFloat)
			{
				cur_track = static_cast<ovr_core::OvrFloatTrack*>(a_track);
				show_tracks.push_back(cur_track);
				cur_pt_manager = cur_track->getCurvePointManager();
				searchFloatTracksForGroup(a_track->GetParent(), false);
			}*/
		}
	}

	if (is_auto_fit)
	{
		fitWidth();
	}
	fitHeight();

	update();

	has_track_line ? updateTrackLine() : resetTrackLine();
}

bool OvrTimeLineCurveView::searchFloatTracksForGroup(ovr_core::OvrTrack* a_track, bool a_is_added_to_show_tracks)
{
	/*if (a_track != nullptr && a_track->GetTrackType() == ovr_core::kTrackGroup)
	{
		bool is_pos_track = a_track->GetTag() == "position";
		group_track = static_cast<ovr_core::OvrGroupTrack*>(a_track);
		for (auto child_track : group_track->GetChildren())
		{
			if (child_track->GetTrackType() == ovr_core::kTrackFloat)
			{
				if (a_is_added_to_show_tracks)
				{
					show_tracks.push_back(static_cast<ovr_core::OvrFloatTrack*>(child_track));
				}
				if (is_pos_track)
				{
					auto tag = child_track->GetTag();
					if (tag == "X" || tag == "Y" || tag == "Z")
					{
						xyz_tracks[tag.GetStringConst()] = static_cast<ovr_core::OvrFloatTrack*>(child_track);
					}
				}
			}
		}
	}

	has_track_line = xyz_tracks.size() == 3;*/

	return show_tracks.size() > 0;
}

void OvrTimeLineCurveView::doOperation(QString a_op_name)
{
	if (a_op_name == "ce_fit_h")
	{
		if (fitHeight()) update();
	}
	else if (a_op_name == "ce_fit_w")
	{
		if (fitWidth()) update();
	}
	else if (cur_pt_manager != nullptr && cur_pt_manager->IsSelectedPoint())
	{
		static const char* op_names[] = { "ce_line", "ce_jump", "ce_bezier_auto" };

		bool is_updated = false;
		if (a_op_name == "ce_bezier_devorce")
		{
			is_updated = cur_pt_manager->makeDevorce(cur_pt_manager->GetSelectFrame());
		}
		else for (int i = 0; i < 3; ++i)
		{
			if (a_op_name == op_names[i])
			{
				is_updated = cur_pt_manager->setMode(cur_pt_manager->GetSelectFrame(), (ovr_core::OvrCtlPoint::CTLMode)i);
				break;
			}
		}

		if (is_updated)
		{
			update();
			onCurveLineChanged(cur_track);
			notifySelectPointChanage();
		}
	}
}

bool OvrTimeLineCurveView::fitWidth()
{
	int min_frame = 0, max_frame = getGlobalAxisView()->getFrameTotal();
	getXRanges(min_frame, max_frame);

	if (getGlobalAxisView()->makeSureVisible(min_frame, max_frame))
	{
		return true;
	}
	return false;
}

void OvrTimeLineCurveView::setAutoFit(bool a_flag)
{
	if (a_flag != is_auto_fit)
	{
		is_auto_fit = a_flag;
	}

	if (is_auto_fit && isVisible())
	{
		if (fitWidth() || fitHeight())
		{
			update();
		}
	}
}

void OvrTimeLineCurveView::setAutoFitHeight()
{
	if (fitHeight())
	{
		update();
	}
}

void OvrTimeLineCurveView::getXRanges(int& a_min_frame, int& a_max_frame)
{
	if (show_tracks.size() > 0)
	{
		bool is_first = true;
		for (auto track : show_tracks)
		{
			auto pt_manager = track->getCurvePointManager();
			if (is_first)
			{
				is_first = false;
				pt_manager->getXRange(a_min_frame, a_max_frame);
			}
			else
			{
				int tmp_min, tmp_max;
				if (pt_manager->getXRange(tmp_min, tmp_max))
				{
					if (tmp_min < a_min_frame) a_min_frame = tmp_min;
					if (tmp_max > a_max_frame) a_max_frame = tmp_max;
				}
			}
		}
	}
}

bool OvrTimeLineCurveView::fitHeight()
{
	int new_wnd_port_y = wnd_port_y;
	int new_ratio = ratio;

	if (show_tracks.size() == 0)
	{
		new_wnd_port_y = height() / 2;
	}
	else
	{
		int free_space = height() - kYMargin * 2;
		float min_v, max_v;
		float value_space = getYRanges(&min_v, &max_v);

		new_ratio = getGlobalAxisView()->getValuePerPixel() / (value_space / free_space);
		assert(new_ratio > 0);

		float mid_value = (max_v - min_v) / 2 + min_v;
		float y = calcPointPosY(mid_value, new_ratio);
		new_wnd_port_y = y + height() / 2;
	}

	if (new_wnd_port_y != wnd_port_y || new_ratio != ratio)
	{
		wnd_port_y = new_wnd_port_y;
		ratio = new_ratio;
		return true;
	}
	return false;
}

float OvrTimeLineCurveView::getYRanges(float* a_min, float* a_max)
{
	bool is_first = true;
	float min_v = 0, max_v = 0;
	for (auto track : show_tracks)
	{
		auto pt_manager = track->getCurvePointManager();
		if (is_first)
		{
			is_first = false;
			pt_manager->getYRange(&min_v, &max_v);
		}
		else
		{
			float tmp_min, tmp_max;
			pt_manager->getYRange(&tmp_min, &tmp_max);
			if (tmp_min < min_v) min_v = tmp_min;
			if (tmp_max > max_v) max_v = tmp_max;
		}
	}

	float value_space = max_v - min_v;
	if (value_space <= kFloatZero)
	{
		value_space = max_v <= kFloatZero ? value_space = 1 : value_space = max_v;
	}

	if (a_min != nullptr) *a_min = min_v;
	if (a_max != nullptr) *a_max = max_v;

	return value_space;
}

void OvrTimeLineCurveView::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::RightButton)
	{
		is_drag = true;
		is_right_menu = true;
		drag_pt = event->pos();
		drag_wnd_port_y = wnd_port_y;
		getGlobalAxisView()->dragPress(event->x(), 0);
	}
	else if (event->button() == Qt::LeftButton)
	{
		if (selectCtlPoint(event->x(), event->y()))
		{
			is_ctl_point_pressed = true;
		}
		else
		{
			if (selectPoint(event->x(), event->y()))
			{
				update();
				notifySelectPointChanage();
				onTrackLinePointSelected();
			}

			if (cur_pt_manager != nullptr && cur_pt_manager->GetSelectFrame() != -1)
			{
				is_data_point_pressed = true;
			}
		}
	}
}

void OvrTimeLineCurveView::onTrackLinePointSelected()
{
	if (has_track_line)
	{
		OvrColorValue c;
		int pt = getSelectPoint(c);
		ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new OvrTrackLineCmd(pt, c));
	}
}

bool OvrTimeLineCurveView::selectCtlPoint(int a_x, int a_y)
{
	if (cur_pt_manager != nullptr && cur_pt_manager->IsSelectedPoint())
	{
		is_selected_ctl_pt_left = is_selected_ctl_pt_right = false;

		OvrVector2 cur_ctl1, cur_ctl2;
		getControlPoints(cur_pt_manager->GetSelectFrame(), cur_ctl1, cur_ctl2, true, cur_pt_manager);

		if (isDevicePointInLogicRect(a_x, a_y, cur_ctl1))
		{
			is_selected_ctl_pt_left = true;
			pt_click_center = OVRPt2QtPt(cur_ctl1);
		}
		else if (isDevicePointInLogicRect(a_x, a_y, cur_ctl2))
		{
			is_selected_ctl_pt_right = true;
			pt_click_center = OVRPt2QtPt(cur_ctl2);
		}

		if (is_selected_ctl_pt_left || is_selected_ctl_pt_right)
		{
			pt_click_down.rx() = a_x;
			pt_click_down.ry() = a_y;
			pt_click_down = cur_trans.inverted().map(pt_click_down);
			return true;
		}
	}
	return false;
}


bool OvrTimeLineCurveView::selectPoint(int a_x, int a_y)
{
	bool to_be_updated = false;

	cur_pt_manager = nullptr;
	cur_track = nullptr;

	for (auto track : show_tracks)
	{
		int new_select_point_frame = -1;
		auto pt_manager = track->getCurvePointManager();
		for (auto item : pt_manager->getPointsMap())
		{
			if (isDevicePointInLogicRect(a_x, a_y, calcPointPos(item.second->cur_point)))
			{
				new_select_point_frame = item.first;
				break;
			}
		}
		to_be_updated = pt_manager->SetSelectFrame(new_select_point_frame) || to_be_updated;

		if (pt_manager->GetSelectFrame() != -1)
		{
			cur_pt_manager = pt_manager;
			cur_track = track;
			pt_click_down.rx() = a_x;
			pt_click_down.ry() = a_y;
			pt_click_down = cur_trans.inverted().map(pt_click_down);
			pt_click_center = OVRPt2QtPt(calcPointPos(cur_pt_manager->getPoint(pt_manager->GetSelectFrame())->cur_point));
		}
	}

	if (cur_track == nullptr && show_tracks.size() == 1)
	{
		cur_track = show_tracks.front();
		cur_pt_manager = cur_track->getCurvePointManager();
	}

	return to_be_updated;
}

void OvrTimeLineCurveView::notifySelectPointChanage()
{
	//未选中
	if (cur_pt_manager == nullptr || !cur_pt_manager->IsSelectedPoint())
	{
		emit onUpdatePointStatus("", false);
		return;
	}

	ovr_core::OvrCtlPoint* pt = cur_pt_manager->getPoint(cur_pt_manager->GetSelectFrame());
	assert(pt != nullptr);

	if (pt->isAutoBezier())
	{
		emit onUpdatePointStatus("ce_bezier_auto", !pt->is_move_both);
	}
	else if (pt->isUserBezier())
	{
		emit onUpdatePointStatus("ce_bezier_man", !pt->is_move_both);
	}
	else if (pt->isLine())
	{
		emit onUpdatePointStatus("ce_line", !pt->is_move_both);
	}
	else if (pt->isJump())
	{
		emit onUpdatePointStatus("ce_jump", !pt->is_move_both);
	}
	else
	{
		assert(false);
	}
}

bool OvrTimeLineCurveView::isDevicePointInLogicRect(int a_x, int a_y, OvrVector2 a_center)
{
	QPointF pt_hit = cur_trans.inverted().map(QPointF(a_x, a_y));

	int rc_wh = kPointWH + 2;
	QRectF point_rc(0, 0, rc_wh, rc_wh);

	point_rc.moveTopLeft(OVRPt2QtPt(a_center) - QPointF(rc_wh / 2, rc_wh / 2));

	return point_rc.contains(pt_hit);
}

void OvrTimeLineCurveView::mouseReleaseEvent(QMouseEvent *event)
{
	if (is_drag)
	{
		is_drag = false;
		getGlobalAxisView()->dragRelease(event->x(), 0);
		if (is_right_menu)
		{
			is_right_menu = false;
			showRightMenu(event->pos());
		}
	}

	if (is_ctl_point_pressed)
	{
		if (cur_pt_manager->isChanged())
		{
			onCurveLineChanged(cur_track);
		}
		is_ctl_point_pressed = false;
	}
	else if (is_data_point_pressed)
	{
		if (is_data_point_moved)
		{
			is_data_point_moved = false;
			onCurveLineChanged(cur_track);
		}
		is_data_point_pressed = false;
	}
}

void OvrTimeLineCurveView::showRightMenu(QPoint a_pt)
{
	//可以删除点吗
	bool can_rmv = false;
	int rmv_frame = -1;
	auto track = isClickOnTrackPoint(a_pt, rmv_frame);
	if (track != nullptr)
	{
		can_rmv = true;
		curve_menu->setRmvTarget(cur_track, rmv_frame);
	}

	//可以添加点吗
	bool can_add = false;
	if (!can_rmv && cur_pt_manager != nullptr)
	{
		can_add = true;
		curve_menu->setAddTarget(a_pt);
	}

	a_pt = mapToGlobal(a_pt);
	curve_menu->setItemsEnable(can_rmv, can_add);
	curve_menu->move(a_pt);
	curve_menu->show();
}

ovr_core::OvrFloatTrack* OvrTimeLineCurveView::isClickOnTrackPoint(const QPointF& a_pt, int& a_frame)
{
	for (auto track : show_tracks)
	{
		int new_select_point_frame = -1;
		auto pt_manager = track->getCurvePointManager();
		for (auto item : pt_manager->getPointsMap())
		{
			if (isDevicePointInLogicRect(a_pt.x(), a_pt.y(), calcPointPos(item.second->cur_point)))
			{
				a_frame = (int)item.first;
				return track;
			}
		}
	}
	a_frame = -1;
	return nullptr;
}

void OvrTimeLineCurveView::onAddPoint()
{
	QPoint pt_logic = cur_trans.inverted().map(curve_menu->getPoint());

	int new_frame = calcPointValueX(pt_logic.x());
	float new_value = calcPointValueY(pt_logic.y());
	cur_track->AddKeyFrame(new_frame, new_value);
	
	cur_pt_manager->SetSelectFrame(new_frame);
	notifySelectPointChanage();
	onCurveLineChanged(cur_track);
	update();
}

void OvrTimeLineCurveView::onRmvPoint()
{
	auto track = curve_menu->getTrack();
	auto frame = curve_menu->getFrame();
	bool pt_changed = track == cur_track && frame == cur_pt_manager->GetSelectFrame();
	if (track->RmvKeyFrame(frame))
	{
		update();
		if (pt_changed)
		{
			notifySelectPointChanage();
		}
		onCurveLineChanged(track);
	}
}

void OvrTimeLineCurveView::mouseMoveEvent(QMouseEvent *event)
{
	if (is_drag)
	{
		is_right_menu = false;
		int new_wnd_port_y = event->y() - drag_pt.ry() + drag_wnd_port_y;
		changeWndPort(new_wnd_port_y);
		getGlobalAxisView()->dragMove(event->x(), 0);
	}
	else if (is_ctl_point_pressed)
	{
		QPointF pt_move = cur_trans.inverted().map(event->localPos());
		QPointF user_change_ctl_pt = pt_move - (pt_click_down - pt_click_center);
		updateCurrentCtlPoint(QtPt2OVRPt(user_change_ctl_pt), cur_pt_manager);
	}
	else if (is_data_point_pressed)
	{
		QPointF pt_move = cur_trans.inverted().map(event->localPos());
		QPointF new_center_pt = pt_move - (pt_click_down - pt_click_center);

		int new_frame = calcPointValueX(new_center_pt.x());
		float new_value = calcPointValueY(new_center_pt.y());

		auto select_frame = cur_pt_manager->GetSelectFrame();

		//Y向移动
		float cur_value = 0;
		cur_track->GetKeyFrame(select_frame, cur_value);
		if (!Float_Equal(new_value, cur_value))
		{
			cur_track->AddKeyFrame(select_frame, new_value);
			is_data_point_moved = true;
		}
		//X向移动
		if (new_frame != select_frame)
		{
			if (cur_track->MoveKey(select_frame, new_frame))
			{
				is_data_point_moved = true;
			}
		}
		if (is_data_point_moved)
		{
			update();
		}
	}
}

void OvrTimeLineCurveView::updateCurrentCtlPoint(const OvrVector2& a_ctl_point, ovr_core::OvrCurvePointManager* a_pt_manager)
{
	assert(a_pt_manager->IsSelectedPoint());

	ovr_core::OvrCtlPoint* ctl_pt = a_pt_manager->getPoint(a_pt_manager->GetSelectFrame());

	OvrVector2 user_ctl_pt_left, user_ctl_pt_right;
	{
		OvrVector2 pt_current = calcPointPos(ctl_pt->cur_point);

		OvrVector2 pt_n = is_selected_ctl_pt_left ? pt_current - a_ctl_point : a_ctl_point - pt_current;
		pt_n *= kCtlLineLength / pt_n.length();

		user_ctl_pt_left = pt_current - pt_n;
		user_ctl_pt_right = pt_current + pt_n;
		if (pt_n._x < 0)
		{
			user_ctl_pt_left._x = user_ctl_pt_right._x = pt_current._x;

			int flag = a_ctl_point._y < pt_current._y ? -1 : 1;
			if (is_selected_ctl_pt_left)
			{
				user_ctl_pt_left._y = pt_current._y + flag * kCtlLineLength;
				user_ctl_pt_right._y = pt_current._y - flag * kCtlLineLength;
			}
			else
			{
				user_ctl_pt_left._y = pt_current._y - flag * kCtlLineLength;
				user_ctl_pt_right._y = pt_current._y + flag * kCtlLineLength;
			}
		}
	}

	OvrVector2 pt_left_value = calcPointValue(user_ctl_pt_left);
	OvrVector2 pt_right_value = calcPointValue(user_ctl_pt_right);

	if (a_pt_manager->updateCtlPointValue(a_pt_manager->GetSelectFrame(), pt_left_value, pt_right_value, is_selected_ctl_pt_left))
	{
		notifySelectPointChanage();
	}
	update();
}

void OvrTimeLineCurveView::wheelEvent(QWheelEvent *event)
{
	event->accept();

	QPoint pt = getGlobalAxisView()->mapFromGlobal(event->globalPos());
	getGlobalAxisView()->scaleTMLLine(pt.x(), event->delta() < 0);
}

bool OvrTimeLineCurveView::changeWndPort(int a_wnd_port)
{
	if (a_wnd_port != wnd_port_y)
	{
		wnd_port_y = a_wnd_port;
		update();
		return true;
	}
	return false;
}

void OvrTimeLineCurveView::onCurveLineChanged(ovr_core::OvrTrack* a_track)
{
	if (has_track_line)
	{
		updateTrackLine();
	}

	//重新seek更新Actor的float值
	auto player = ovr_project::OvrProject::GetIns()->GetPlayer();
	player->Seek(ovr_project::OvrProject::GetIns()->GetCurrentSequence()->GetCurrentFrame());
}

void OvrTimeLineCurveView::showTrackLine(bool a_is_show)
{
	//a_is_show ? qDebug("show TrackLine") : qDebug("hide TrackLine");

	if (ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor()->GetActorTrackLine().isShow() != a_is_show)
	{
		ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new OvrTrackLineCmd(a_is_show));
	}
}

void OvrTimeLineCurveView::resetTrackLine()
{
	if (ovr_project::OvrProject::GetIns()->GetCurrentSceneEditor()->GetActorTrackLine().checkValid())
	{
		ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new OvrTrackLineCmd("", nullptr, nullptr));
	}
}

void OvrTimeLineCurveView::updateTrackLine()
{
	assert(has_track_line);

	//确定轨迹线时间范围
	int min_x = -1, max_x = -1;
	for (auto float_track : xyz_tracks)
	{
		auto pt_manger = float_track->getCurvePointManager();
		pt_manger->resetChanged();
		int t1, t2;
		if (pt_manger->getXRange(t1, t2))
		{
			if (t1 < min_x || min_x == -1) min_x = t1;
			if (t2 > max_x) max_x = t2;
		}
	}

	//轨迹线至少有两个点
	if (min_x != max_x)
	{
		//生成轨迹线
		int count = max_x - min_x + 1;
		std::vector<OvrVector3>* lines = new std::vector<OvrVector3>(count);
		{
			//生成轨迹线的xyz点
			int data_pos = 0;
			for (auto float_track : xyz_tracks)
			{
				auto pt_manger = float_track->getCurvePointManager();
				pt_manger->initCurveLine(*lines, min_x, max_x, data_pos);
				++data_pos;
			}

			//转换到世界坐标
			auto cur_actor = ovr_project::OvrProject::GetIns()->GetCurrentScene()->GetActorByUniqueName(actor_id);
			assert(cur_actor != nullptr);
			auto& matrix = cur_actor->GetParent()->GetWorldTransform();
			for (auto& pt : *lines)
			{
				pt = matrix * pt;
			}
		}

		//生成轨迹关键点
		std::vector<OvrVector3>* points = nullptr;
		{
			points_map.clear();
			for (auto float_track : xyz_tracks)
			{
				auto pt_manger = float_track->getCurvePointManager();
				pt_manger->pickKeyPoint(*lines, points_map, min_x);
			}

			points = new std::vector<OvrVector3>(points_map.size());
			int pt_pos = 0;
			for (auto item : points_map)
			{
				(*points)[pt_pos++] = item.second;
			}
		}

		OvrColorValue pt_color;
		int select_point = getSelectPoint(pt_color);

		//显示新的轨迹线数据
		ovr_project::OvrProject::GetIns()->GetCommandManager()->PushCommand(new OvrTrackLineCmd(actor_id, lines, points, select_point, pt_color));
	}
}

int OvrTimeLineCurveView::getSelectPoint(OvrColorValue& a_color)
{
	static OvrColorValue pt_colors[] = { OvrColorValue(0.8,0,0), OvrColorValue(0,0.8,0), OvrColorValue(0,0,0.8) };
	static QMap<QString, int> color_maps = { { "X", 0 },{ "Y", 1 },{ "Z", 2 } };

	if (cur_track != nullptr)
	{
		auto tag = cur_track->GetTag().GetStringConst();
		if (color_maps.contains(tag))
		{
			a_color = pt_colors[color_maps[tag]];
		}
	}
	
	int frame = cur_pt_manager != nullptr ? cur_pt_manager->GetSelectFrame() : -1;
	if (frame != -1)
	{
		int pos = -1;
		for (auto item : points_map)
		{
			++pos;
			if (frame == item.first)
			{
				return pos;
			}
		}
	}
	return -1;
}

/////////////////////////////////////////////////////////
// class OvrCuveMenu

static 	const char* strMenuStyle =
"QMenu {background:rgb(39,39,39); color:rgb(205,205,205); selection-background-color: rgb(60,60,60); selection-color: rgb(205,205,205);}"
"QMenu::item:disabled {color:gray}";

OvrCuveMenu::OvrCuveMenu(QWidget *parent) : QMenu(parent)
{
	add_action = addAction(QString::fromLocal8Bit("在当前曲线添加关键点"));
	add_action->setObjectName("add_key");

	rmv_action = addAction(QString::fromLocal8Bit("删除当前关键点"));
	rmv_action->setObjectName("rmv_key");

	setStyleSheet(strMenuStyle);
}

void OvrCuveMenu::setItemsEnable(bool a_can_rmv, bool a_can_add)
{
	add_action->setEnabled(a_can_add);
	rmv_action->setEnabled(a_can_rmv);
}