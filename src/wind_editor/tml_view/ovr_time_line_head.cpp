﻿#include "Header.h"
#include "ovr_time_line_head.h"

#include "ovr_tml_track_tool_bar.h"
#include "ovr_tml_axis_pane.h"

#include "ovr_time_line_tree_model.h"

#include "ovr_mark_view.h"
#include "ovr_tml_axis.h"

/////////////////////////////////////////////////////////////////////
// class OvrTimeLineHead

void OvrTimeLineHead::initUI()
{
	track_toolbar = new OvrTMLTrackToolBar(this);
	tml_group = new OvrTMLAxisPane(this);
}

OvrTimeLineHead::OvrTimeLineHead(Qt::Orientation orientation, QWidget *parent) :QHeaderView(orientation, parent)
{
	initStyle();
	initUI();

	//更新游标, 缩放工具条
	connect(this, SIGNAL(sectionResized(int, int, int)), this, SLOT(onSectionResized(int, int, int)));

	//时间轴范围
	connect(OvrTimeLineTreeModel::getInst(), &OvrTimeLineTreeModel::onSeuenceqOpenend, this, &OvrTimeLineHead::onSeuenceqOpenend, Qt::QueuedConnection);
}

void OvrTimeLineHead::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{}

void OvrTimeLineHead::initStyle()
{
	setStretchLastSection(true);
}

OvrTimeLineHead::~OvrTimeLineHead()
{
}

void OvrTimeLineHead::paintSection(QPainter *painter, const QRect &rect, int logicalIndex) const
{
	painter->setBrush(QColor("#2F2F2F"));
	painter->setPen(Qt::NoPen);
	painter->drawRect(rect);

	if (logicalIndex == kTMLSectionLineIndex)
	{
		painter->setPen(QColor(0, 0, 0));
		painter->drawLine(rect.topLeft(), rect.bottomLeft());
	}
}

void OvrTimeLineHead::onSectionResized(int logicalIndex, int oldSize, int newSize)
{
	Q_UNUSED(oldSize);

	if (logicalIndex == kTMLSectionValueIndex)
	{
		tml_group->setGeometry(sectionPosition(kTMLSectionValueIndex), 0, newSize, height());
	}
	else if (logicalIndex == kTMLSectionHeadIndex)
	{
		QRect rc(0, 0, newSize - 5, rect().height());
		track_toolbar->setGeometry(rc);
	}
}

void OvrTimeLineHead::resizeEvent(QResizeEvent *event)
{
	QRect rc(0, 0, sectionSize(kTMLSectionHeadIndex) - 5, rect().height());
	track_toolbar->setGeometry(rc);
	if (!is_size_adjusted)
	{
		resizeSection(kTMLSectionLineIndex, kTMLSectionLineWidth);
		setSectionResizeMode(QHeaderView::Fixed);
		setSectionsMovable(false);
		resizeSection(kTMLSectionHeadIndex, 300);
		is_size_adjusted = true;
	}
	QHeaderView::resizeEvent(event);
}

void OvrTimeLineHead::onSeuenceqOpenend(int a_duration, int a_fps)
{
	assert(a_fps >= 20);
	assert(a_duration > 0);

	getGlobalAxisView()->setRange(a_duration, a_fps);
	getGlobalAxisView()->makeSureVisible();

	tml_group->getMarkView()->reloadMark();
}
