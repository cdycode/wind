#pragma once

#include <QObject>
#include <QList>
#include <QVariant>

#define kUpdateItemHead  1
#define kUpdateItemValue 2

class OvrTMLClipItem;

class OvrTimeLineTreeItem :public QObject
{
	Q_OBJECT
public:	
	OvrTimeLineTreeItem(const QString& a_title, OvrTimeLineTreeItem *a_parent_Item);
	virtual ~OvrTimeLineTreeItem();

	virtual bool isClipDataItem() { return false; }
	virtual bool isPointDataItem() { return false; }
	bool isContainerItem() { return is_container_item; }

	static const QMap<OvrTimeLineTreeItem*, int>& getItemsUpdateWithTime();
	static bool HasItemAcceptAsset(const QString& a_asset_path);

	void setTrack(ovr_core::OvrTrack* a_track);
	ovr_core::OvrTrack* getTrack() { return item_track; }
	ovr_core::OvrActorTrack* getActorTrack();

	virtual bool canHaveChildTrack();
	virtual bool canAddKeyFrame() { return can_add_key_frame; }

	bool isNeedUpdateCol0WithTimeChange();

	OvrTimeLineTreeItem* getActorItem();
	OvrTimeLineTreeItem* parentItem();
	int getParents(QList<OvrTimeLineTreeItem*>& a_list);

	int childCount() const;
	OvrTimeLineTreeItem* child(int a_row);
	void appendChild(OvrTimeLineTreeItem *child);
	const QList<OvrTimeLineTreeItem*>& getChildItems() { return child_Items; }
	virtual void removeChildItem(OvrTimeLineTreeItem* a_item);
	void clearChild();

	int columnCount() const;
	QVariant data(int column) const;
	int row() const;

	virtual bool contains(int a_frame) { return false; }

	static void setKeyPointRange(int a_width);
	virtual bool hitTest(int a_pos, bool is_for_resize = false) { return false; }
	bool draging(int a_pos);
	bool cloneDragInfo(OvrTimeLineTreeItem* a_item);

	int  selectKey() { return select_key; }
	void setSelectKey(int a_new_key) { select_key = a_new_key; }
	bool isSelected() { return select_key != -1; }
	void clearSelection() { select_key = -1; }
	virtual bool removeSelected() { return false; }
	virtual bool isResizeable() { return false; }
	virtual bool isSplitable() { return false; }

	virtual bool splitFromCurrentCusor() { return false; }

	bool isConfigable() { return is_config_able; }
	virtual const QString& getValueText() { return value_text; }

protected:
	int  getKeyPointRange(int drag_pos, int& left_pos, int& right_pos);
	virtual bool dragingResize(int a_frame) { return false; }
	virtual bool dragingMove(int a_frame) { return false; }
	virtual bool isAcceptAsset(const QString& a_asset_path) { return false; }

	QMap<long int, char> key_points;
	static int key_point_range;

	int select_key = -1;
	int drag_key_head = -1;
	int drag_key_tail = -1;
	int drag_pos = 0;

	enum DragMode{kDMMove, kDMHeadResize, kDMTailResize} drag_mode = kDMMove;

protected:	//new
	static QList<OvrTimeLineTreeItem*> all_clip_list;

	QString title;
	OvrTimeLineTreeItem* parent_Item = nullptr;
	QList<OvrTimeLineTreeItem*> child_Items;
	ovr_core::OvrTrack* item_track = nullptr;

	bool is_config_able = false;
	bool can_add_key_frame = false;
	bool is_container_item = false;

	QString value_text;

public:
	bool isActorItem();
	bool isSpecialItem();
	bool isFaceCapture();
	bool isMotionCapture();
	bool isInteractive();
};

class OvrTMLClipItem;

OvrTimeLineTreeItem* CreateTMLTreeItem(const char* a_name, OvrTimeLineTreeItem* a_parent, ovr_core::OvrTrack* a_track, bool is_append);
OvrTimeLineTreeItem* CreateTMLTreeItem(const OvrString& a_name, OvrTimeLineTreeItem* a_parent, ovr_core::OvrTrack* a_track, bool is_append);