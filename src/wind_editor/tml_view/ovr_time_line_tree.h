#pragma once

#include <QTreeView>
#include <QMenu>
#include <QPersistentModelIndex>

class OvrTimeLineTreeItem;
class QLabel;
class OVRActorItem;

class OvrTMLClipItem;

class OvrTreeItemDragObj : public QObject
{
	Q_OBJECT
public:
	Q_INVOKABLE explicit OvrTreeItemDragObj(QObject *parent = Q_NULLPTR) :QObject(parent) { reset(); }
	
	void reset();

	bool beginDrag(const QModelIndex& a_index, OvrTimeLineTreeItem* a_item, int a_pos, bool a_only_select = false);
	void draging(int a_pos, QMouseEvent *event, const QModelIndex& a_index_under_mouse);
	void endDrag(QMouseEvent *event);

	bool isDragingBegin() { return is_draging_begin; }
	void clickNothing();

	void setSelectItem(OvrTimeLineTreeItem* a_item) { drag_item = a_item; }

	bool split();
	bool removeSelecttion();
	void renameSelecttion();
	int  getSelectKey();

public slots:
	void onSetSelectItem(QModelIndex a_index, OvrTimeLineTreeItem* a_item, ovr::uint64 a_key);

protected:
	void update();
	void clearSelection();
	void moveClipTo(OvrTMLClipItem* a_other_item, const QModelIndex& a_other_item_index);

	OvrTimeLineTreeItem* drag_item = nullptr;
	QPersistentModelIndex drag_index;
	int drag_pos = -1;
	bool is_draging_begin = false;
	bool is_draging = false;
};

class OVRTMLAddTrackMenu;

class OvrTimeLineTree :	public QTreeView, public OvrEventReceiver
{
	Q_OBJECT
public:
	static OvrTimeLineTree* getInst();

	explicit OvrTimeLineTree(QWidget *parent = Q_NULLPTR);
	~OvrTimeLineTree();

	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;
	virtual QString name() override { return "OvrTimeLineTree"; }

	bool canBeDroped(const QPoint& a_ptGlobal, const QByteArray& a_file_path);

	void removeSelecttion();
	void renameSelecttion();
	void locateItem(QModelIndex a_index);

public slots:
	void onPlayPosChanged(ovr::uint64);
	void onAddTrackMenuSelected(QAction* a_action);

protected:
	virtual void mousePressEvent(QMouseEvent *event) override;
	virtual void mouseMoveEvent(QMouseEvent *event) override;
	virtual void mouseReleaseEvent(QMouseEvent *event) override;
	virtual void contextMenuEvent(QContextMenuEvent *event) override;
	virtual void keyPressEvent(QKeyEvent *event) override;
	virtual void focusOutEvent(QFocusEvent *event) override;
	virtual void mouseDoubleClickEvent(QMouseEvent *event) override;

	virtual void currentChanged(const QModelIndex &current, const QModelIndex &previous) override;

	virtual void dragEnterEvent(QDragEnterEvent *event) override;
	virtual void dragMoveEvent(QDragMoveEvent *event) override;
	virtual void dropEvent(QDropEvent *event) override;
	virtual void dragLeaveEvent(QDragLeaveEvent *event) override;

	void paintEvent(QPaintEvent *event) override;

	bool mousePressTreeLeft(QMouseEvent *event, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index);
	void mousePressTreeRight(QMouseEvent *event, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index);

	bool mouseMoveTreeLeft(QMouseEvent *event, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index);
	bool mouseMoveTreeRight(QMouseEvent *event, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index);

	bool viewportEvent(QEvent *event) override;

	void resizeEvent(QResizeEvent *event) override;
	void initAddTrackMenu(OvrTimeLineTreeItem* a_item);

	void configInterActive(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame);
	void configEvent(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame);

	void updateItemIfNeeded();
	void updateItemIfNeeded(OvrTimeLineTreeItem* a_item, int a_value);

	void initTreeStyle();

private:
	bool resetResizeCursor();

	QWidget* ovr_tml_v_line = nullptr;
	int tml_v_line_y_offset = 28;
	OvrTreeItemDragObj* drag_obj;
	OVRTMLAddTrackMenu* add_track_menu = nullptr;
	bool is_right_click = false;
	bool resize_cursor_set_for_clip = false;
};

class OvrTimeLineTreeItem;
class QTreeItemMenu :public QMenu
{
	Q_OBJECT
public:
	static bool showRightMenu(const QPoint a_pt, OvrTimeLineTreeItem* a_item, int a_key_frame, bool is_Column0 = false, QModelIndex a_index = QModelIndex());
	static bool showLeftMenu(const QPoint a_pt, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index);

public slots:
	void aboutToHide();
	void triggered(QAction *action);
	void leftTriggered(QAction *action);

protected:
	explicit QTreeItemMenu(QWidget *parent = Q_NULLPTR);
	~QTreeItemMenu();

	void initMenu();
	void initForEventActor();
	void initForInterActiveActor();
	void initForClip(ovr_core::OvrClipInfo::Type a_clip_type);
	void initForTextClip();
	void initForAudioClip();

	void addNewItem(bool a_enable = true);
	void addRemoveItem(bool a_enable = true);
	void addRenameItem();

	OvrTimeLineTreeItem* item = nullptr;
	QModelIndex item_index;
	bool is_Column0 = false;
	long key_frame = -1;
};

class OVRTMLAddTrackMenu :public QMenu
{
public:
	explicit OVRTMLAddTrackMenu(QWidget *parent = Q_NULLPTR);
	void initMenu(OvrTimeLineTreeItem* a_actor_item);
};
