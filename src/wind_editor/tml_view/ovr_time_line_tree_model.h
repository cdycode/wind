#pragma once

#include <QAbstractItemModel>
#include <QStandardItemModel>

namespace ovr_project
{
	class ProjectSence;
	class OvrSequence;
	class OvrSubTrack;
	class OvrActorSubCaptureTrack;
}
class OvrTimeLineTreeItem;
class OVRActorItem;

class OvrTimeLineTreeModel :public QAbstractItemModel, public OvrEventReceiver
{
	Q_OBJECT
public:
	static OvrTimeLineTreeModel* getInst();

	explicit OvrTimeLineTreeModel(QObject *parent = 0);
	~OvrTimeLineTreeModel();

	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;
	virtual QString name() override { return "OvrTimeLineTreeModel"; }

	bool openProject(const char* a_project_dir);

	//for reading
	QVariant data(const QModelIndex &index, int role) const override;
	Qt::ItemFlags flags(const QModelIndex &index) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
	QModelIndex parent(const QModelIndex &index) const override;
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;

	//for add/remove row/rows
	void play(bool a_record_flag, bool is_vr_mode = false);
	void pause();
	bool seek(int a_frame = -1);
	bool isRecord();
	bool isPause();
	void saveRecord();
	int GetCaptureTrackCount(bool* a_is_modified = nullptr, int* configured_count = nullptr);

	bool isSeuenceqValid()
	{
		return current_seq != nullptr;
	};

	bool addSpecialTrack(ovr_core::OvrTrackType a_type, const char* a_track_tag);

	bool addInterActiveClip(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame);
	bool renameInterActiveClip(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame);

	bool addEventFrame(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame);
	//bool addTransparentFrame(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame);
	bool addFloatFrame(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame);
	bool addTransformFrame(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame);
	bool addGroupFrame(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame);
	bool getTransFormValue(OvrTimeLineTreeItem* a_item, float& a_value);

	bool renameEventFrame(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame);

	bool cloneEvent(int a_frame, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index, QModelIndex& a_update_index, const QString& a_event);
	bool replaceEvent(int a_frame, OvrTimeLineTreeItem* a_item, const QString& a_event);

	bool cloneInteractive(int a_frame, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index, QModelIndex& a_update_index, const QString& a_name);
	bool replaceInteractive(int a_frame, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index, QModelIndex& a_update_index, const QString& a_name);

	void configSequcence();
	bool settingText(int a_frame, OvrTimeLineTreeItem* a_item);
	bool settingAudio(int a_frame, OvrTimeLineTreeItem* a_item);

	void addActorTrack(const QString& a_actor_id);
	void removeTrack(OvrTimeLineTreeItem* a_item, const QModelIndex& a_index);

	//add child clip data track
	QModelIndex addChildTrack(const QModelIndex& a_index, int a_track_type, const QString& a_sub_type);

	//add clip file for clip track
	bool addClipFile(const QModelIndex& a_index, const QByteArray& a_file_path, int a_pos);


	bool addKeyFrame(int a_frame, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index, QModelIndex& a_update_index);
	bool renameKeyFrame(int a_frame, OvrTimeLineTreeItem* a_item);
	void addMark(ovr::uint64 a_frame);

	void onOpenSequence(ovr_core::OvrSequence* a_new_sequence);

signals:
	void onSeuenceqOpenend(int a_duration, int a_fps);
	void markAdded(ovr::uint64 a_frame);
	void onSetSelectItem(QModelIndex a_index, OvrTimeLineTreeItem* a_item, ovr::uint64 a_key);


protected:
	void onProjectOpen(bool is_ok, QString a_prj_dir);
	void onCurrentSeqChanged(QString a_seq_id);
	void onRemoveTimeLineActor(QString a_actor_id);

	void setCurrentSeq(ovr_core::OvrSequence* a_seq);
	static OvrTimeLineTreeModel* g_model_inst;

private:
	OvrTimeLineTreeItem* rootItem;
	ovr_core::OvrSequence* current_seq = nullptr;
};