#include "Header.h"
#include "ovr_timelinel_item.h"
#include "ovr_tml_axis.h"
#include <wx_core/track/ovr_float_track.h>

#define kResizeClipSpace 5


OvrTMLPointItem::OvrTMLPointItem(const QString& a_title, OvrTimeLineTreeItem *a_parent_Item) :OvrTimeLineTreeItem(a_title, a_parent_Item)
{
}

template<class T>
bool OvrTMLPointItem::containsFun(int a_frame, const T& maps)
{
	return maps.find(a_frame) != maps.end();
}

bool OvrTMLPointItem::contains(int a_frame)
{
	/*switch (item_track->GetTrackType())
	{
	case ovr_core::kTrackEvent:
		return containsFun(a_frame, static_cast<ovr_core::OvrEventTrack*>(item_track)->GetAllKeyFrames());
		break;
	case ovr_core::kTrackTransform:
		return containsFun(a_frame, static_cast<ovr_core::OvrTransformTrack*>(item_track)->GetKeyFrameMap());
		break;
	case ovr_core::kTrackFloat:
		return containsFun(a_frame, static_cast<ovr_core::OvrFloatTrack*>(item_track)->GetKeyFrameMap());
		break;

	default:
		assert(false);
		break;
	}*/
	return false;
}

template<class T>
bool OvrTMLPointItem::hitTestFun(int a_pos, const T& maps)
{
	drag_mode = kDMMove;

	a_pos = getGlobalAxisView()->mapFromGlobal(QPoint(a_pos, 0)).x();
	int left_pos, right_pos;
	getKeyPointRange(a_pos, left_pos, right_pos);

	int center_frame = round(getGlobalAxisView()->getFrameForDPX(a_pos));
	int frame_begin = round(getGlobalAxisView()->getFrameForDPX(left_pos));
	int frame_end = round(getGlobalAxisView()->getFrameForDPX(right_pos));

	for (int i = frame_begin; i <= frame_end; ++i)
	{
		if (maps.find(i) != maps.end())
		{
			select_key = i;
			drag_key_head = i;
			drag_pos = center_frame;
			return true;
		}
	}
	return false;
}

bool OvrTMLPointItem::hitTest(int a_pos, bool)
{
	/*switch (item_track->GetTrackType())
	{
	case ovr_core::kTrackEvent:
		return hitTestFun(a_pos, static_cast<ovr_core::OvrEventTrack*>(item_track)->GetAllKeyFrames());
		break;
	case ovr_core::kTrackTransform:
		return hitTestFun(a_pos, static_cast<ovr_core::OvrTransformTrack*>(item_track)->GetKeyFrameMap());
		break;
	case ovr_core::kTrackFloat:
		return hitTestFun(a_pos, static_cast<ovr_core::OvrFloatTrack*>(item_track)->GetKeyFrameMap());
		break;
	default:
		assert(false);
		break;
	}*/
	return false;
}


bool OvrTMLPointItem::movePoint(ovr::uint64 a_from, ovr::uint64 a_to)
{
	/*switch (item_track->GetTrackType())
	{
	case ovr_core::kTrackEvent:
		return static_cast<ovr_core::OvrEventTrack*>(item_track)->MoveKey(a_from, a_to);
		break;
	case ovr_core::kTrackTransform:
		return static_cast<ovr_core::OvrTransformTrack*>(item_track)->MoveKey(a_from, a_to);
		break;
	case ovr_core::kTrackFloat:
		return static_cast<ovr_core::OvrFloatTrack*>(item_track)->MoveKey(a_from, a_to);
		break;
	default:
		assert(false);
		break;
	}*/
	return false;
}

bool OvrTMLPointItem::dragingMove(int a_frame)
{
	if (select_key >= 0)
	{
		int new_key = drag_key_head + a_frame - drag_pos;
		if (new_key < 0)
		{
			new_key = 0;
		}

		if (movePoint(select_key, new_key))
		{
			select_key = new_key;
			return true;
		}
	}
	return false;
}

bool OvrTMLPointItem::removeSelected()
{
	if (isSelected())
	{
		/*switch (item_track->GetTrackType())
		{
		case ovr_core::kTrackEvent:
			static_cast<ovr_core::OvrEventTrack*>(item_track)->RmvKeyFrame(select_key);
			break;
		case ovr_core::kTrackTransform:
			static_cast<ovr_core::OvrTransformTrack*>(item_track)->RmvKeyFrame(select_key);
			break;
		case ovr_core::kTrackFloat:
			static_cast<ovr_core::OvrFloatTrack*>(item_track)->RmvKeyFrame(select_key);
			break;
		default:
			assert(false);
			break;
		}*/
		select_key = -1;
		return true;
	}
	return false;
}

const QString& OvrTMLPointItem::getValueText()
{
	/*switch (item_track->GetTrackType())
	{
	case ovr_core::kTrackEvent:
		//static_cast<ovr_project::OvrEventTrack*>(item_track)->RmvKeyFrame(select_key);
		break;
	case ovr_core::kTrackTransform:
		//static_cast<ovr_project::OvrTransformTrack*>(item_track)->RmvKeyFrame(select_key);
		break;
	case ovr_core::kTrackFloat:
	{
		float v = static_cast<ovr_core::OvrFloatTrack*>(item_track)->GetCurrentValue();
		char buf[20] = { 0 };
		sprintf(buf, "%.2f", v);
		value_text = buf;
	}
		break;
	default:
		assert(false);
		break;
	}
	*/
	return value_text;
}

template<class T>
int OvrTMLPointItem::GetPointsFun(QMap<ovr::uint64, char>& a_points, ovr::uint64 a_frame_begin, ovr::uint64 a_frame_end, const T& maps)
{
	for (auto item : maps)
	{
		ovr::uint64 key = item.first;
		if (key < a_frame_begin)
		{
			continue;
		}
		if (key > a_frame_end)
		{
			break;
		}
		a_points[key] = select_key == key ? 1 : 0;
	}

	return a_points.size();
}

int OvrTMLPointItem::GetPoints(QMap<ovr::uint64, char>& a_points, ovr::uint64 a_frame_begin, ovr::uint64 a_frame_end)
{
	/*switch (item_track->GetTrackType())
	{
	case ovr_core::kTrackEvent:
		return GetPointsFun(a_points, a_frame_begin, a_frame_end, static_cast<ovr_core::OvrEventTrack*>(item_track)->GetAllKeyFrames());
		break;
	case ovr_core::kTrackTransform:
		return GetPointsFun(a_points, a_frame_begin, a_frame_end, static_cast<ovr_core::OvrTransformTrack*>(item_track)->GetKeyFrameMap());
		break;
	case ovr_core::kTrackFloat:
		return GetPointsFun(a_points, a_frame_begin, a_frame_end, static_cast<ovr_core::OvrFloatTrack*>(item_track)->GetKeyFrameMap());
		break;
	default:
		assert(false);
		break;
	}*/

	return 0;
}

OvrTMLClipItem::OvrTMLClipItem(const QString& a_title, OvrTimeLineTreeItem *a_parent_Item):OvrTimeLineTreeItem(a_title, a_parent_Item)
{
	assert(a_parent_Item != nullptr);

	all_clip_list.push_back(this);
}

OvrTMLClipItem::~OvrTMLClipItem()
{
	int item_index = all_clip_list.indexOf(this);
	assert(item_index != -1);
	if (item_index != -1)
	{
		all_clip_list.removeAt(item_index);
	}
}

bool OvrTMLClipItem::isResizeable()
{
	return !isFaceCapture() && !isMotionCapture();
}

ovr_core::OvrClipTrack* OvrTMLClipItem::getClipTrack()
{
	return static_cast<ovr_core::OvrClipTrack*>(item_track);
}

bool OvrTMLClipItem::contains(int a_frame)
{
	return getClipTrack()->Clip(a_frame) != nullptr;
}

bool OvrTMLClipItem::splitFromCurrentCusor()
{
	ovr_core::OvrClipTrack* clip_track = getClipTrack();

	if (select_key != -1 && isSplitable())
	{
		int frame = getGlobalAxisView()->getCurFrame();

		if (!contains(frame))
		{
			if (isInClip(select_key, frame))
			{
				int space = frame - select_key;
				int mid_pos = clip_track->Clip(select_key)->start_time + space;
				return splitClip(select_key, frame, mid_pos);
			}
		}
	}
	return false;
}

bool OvrTMLClipItem::isInClip(int a_key_frame, int a_frame)
{
	ovr_core::OvrClipInfo* clip = getClipTrack()->Clip(a_key_frame);
	if (clip != nullptr)
	{
		return a_frame >= a_key_frame  && a_frame < a_key_frame + clip->end_time - clip->start_time;
	}
	return false;
}

bool OvrTMLClipItem::splitClip(int a_from, int a_at, int a_mid_pos)
{
	ovr_core::OvrClipTrack* clip_track = getClipTrack();
	ovr_core::OvrClipInfo* clip = clip_track->Clip(a_from);

	if (clip_track->AddClip(a_at, clip->file_path, a_mid_pos, clip->end_time))
	{
		clip->end_time = a_mid_pos;
		return true;
	}
	return false;
}

bool OvrTMLClipItem::hitTest(int a_pos, bool is_for_resize)
{
	//不可改变大小
	if (is_for_resize && !isResizeable())
	{
		return false;
	}

	a_pos = getGlobalAxisView()->mapFromGlobal(QPoint(a_pos, 0)).x();
	int pos_frame = getGlobalAxisView()->getFrameForDPX(a_pos);

	OvrTMLClipItem* clip_item = static_cast<OvrTMLClipItem*>(this);
	for (auto item : clip_item->getClipTrack()->GetClipMap())
	{
		int frame_end = item.first + item.second->Length();
		if (pos_frame >= item.first && pos_frame <= frame_end)
		{
			drag_mode = calcDragMode(a_pos, item.first, frame_end);
			if (is_for_resize)
			{
				return drag_mode != kDMMove;
			}

			select_key = item.first;
			drag_key_head = item.first;
			auto clip = clip_item->getClipTrack()->Clip(select_key);
			drag_key_tail = drag_key_head + clip->Length();
			drag_pos = pos_frame;
			return true;
		}
	}
	return false;
}

OvrTimeLineTreeItem::DragMode OvrTMLClipItem::calcDragMode(int a_pos, int a_frame_head, int a_frame_end)
{
	int head_pos = getGlobalAxisView()->getDPXForFrame(a_frame_head);
	int tail_pos = getGlobalAxisView()->getDPXForFrame(a_frame_end);

	if (tail_pos - head_pos <= kResizeClipSpace)
	{
		return kDMMove;
	}
	else if (tail_pos - a_pos <= kResizeClipSpace)
	{
		return kDMTailResize;
	}
	else if (a_pos - head_pos <= kResizeClipSpace)
	{
		return kDMHeadResize;
	}

	return kDMMove;
}

bool OvrTMLClipItem::dragingMove(int a_frame)
{
	if (select_key >= 0)
	{
		int new_key = drag_key_head + a_frame - drag_pos;
		if (new_key < 0)
		{
			new_key = 0;
		}

		if (getClipTrack()->MoveClip(select_key, new_key))
		{
			select_key = new_key;
			return true;
		}
	}
	return false;
}

bool OvrTMLClipItem::dragingResize(int a_frame)
{
	if (!isResizeable())
	{
		return false;
	}

	int frame_space = a_frame - drag_pos;
	if (drag_mode == kDMTailResize)
	{
		int new_key_tail = drag_key_tail + frame_space;
		if (new_key_tail <= drag_key_head)
		{
			new_key_tail = drag_key_head + 1;
		}
		int new_space = new_key_tail - drag_key_head;
		auto clip = getClipTrack()->Clip(select_key);
		if (new_space != (clip->end_time - clip->start_time))
		{
			int new_end = clip->start_time + new_space;
			if (resizeClip(select_key, select_key, clip->start_time, new_end))
			{
				return true;
			}
		}
	}
	else
	{
		int new_key_head = drag_key_head + frame_space;
		if (new_key_head >= drag_key_tail)
		{
			new_key_head = drag_key_tail - 1;
		}
		else if (new_key_head < 0)
		{
			new_key_head = 0;
		}

		if (getClipTrack()->Clip(new_key_head) == nullptr)
		{
			if (new_key_head != select_key)
			{
				auto clip = getClipTrack()->Clip(select_key);
				int new_end = clip->start_time + drag_key_tail - new_key_head;
				if (resizeClip(select_key, new_key_head, clip->start_time, new_end))
				{
					select_key = new_key_head;
					return true;
				}
			}
		}
	}
	return false;
}

bool OvrTMLClipItem::resizeClip(int a_from, int a_to, int a_start, int a_end)
{
	ovr_core::OvrClipTrack* clip_track = getClipTrack();

	ovr_core::OvrClipInfo *clip = nullptr;

	if (a_from != a_to)
	{
		if (!clip_track->MoveClip(a_from, a_to))
		{
			return false;
		}
		clip = clip_track->Clip(a_to);
	}
	else
	{
		clip = clip_track->Clip(a_from);
	}

	clip->start_time = a_start;
	clip->end_time = a_end;

	return true;
}

bool OvrTMLClipItem::removeSelected()
{
	if (isSelected())
	{
		getClipTrack()->RmvClip(select_key);
		select_key = -1;
		return true;
	}
	return false;
}

//捕捉轨道暂不支持拆分
bool OvrTMLClipItem::isSplitable()
{
	return !isMotionCapture() && !isFaceCapture();
}

bool OvrTMLClipItem::dragSelectedClipin(OvrTMLClipItem* a_item_from)
{
	assert(a_item_from->select_key >= 0);

	ovr_core::OvrClipInfo* clip_info = a_item_from->getClipTrack()->Clip(a_item_from->select_key);
	if (getClipTrack()->CloneClip(a_item_from->select_key, clip_info))
	{
		cloneDragInfo(a_item_from);
		a_item_from->select_key = -1;
		a_item_from->getClipTrack()->RmvClip(select_key);
		return true;
	}
	return false;
}

bool OvrTMLClipItem::isAcceptAsset(const QString& a_asset_path)
{
	return getClipTrack()->IsSupportDragIn(a_asset_path.toLocal8Bit().constData());
}