#pragma once

#include <QHeaderView>
#include <QLabel>
#include <QMenu>

class OvrTMLMarkMenu;

class OvrMarkView :public QWidget
{
	Q_OBJECT
public:
	explicit OvrMarkView(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	void reloadMark();
	void removeMark(bool dummy = false);
	void clearSelectedMark();

signals:
	void onDragPress(int a_global_x);
	void onDragMove(int a_global_x);
	void onDragRelease(int a_global_x);
	void mouseClick(int a_global_x);

public slots:
	void onMarkAdded(ovr::uint64 a_frame);
	void menuTriggered(QAction *action);

protected:
	void paintEvent(QPaintEvent *e) override;
	virtual void contextMenuEvent(QContextMenuEvent *event) override;
	virtual void mouseDoubleClickEvent(QMouseEvent *e) override;

	virtual void mousePressEvent(QMouseEvent *e) override;
	virtual void mouseMoveEvent(QMouseEvent *e) override;
	virtual void mouseReleaseEvent(QMouseEvent *e) override;

	void drawMarks(QPainter& painter, const QMap<long int, char>& keys) const;
	void getFrameKeys(QMap <long int, char>& keys, int start, int end) const;
	void getMarkDrawPoints(QPoint a_points[], int x) const;

	void renamMark();
	void moveMark(int a_old_x, int a_current_x);

	int  getMark(int a_x);
	void setTooltipKey(int a_key);
	bool setSelectKey(int a_key);

	int select_key = -1;
	int tooltip_key = -1;
	int drag_key = -1;

	int  x_pt_click_down;
	bool is_click_down = false;
	bool is_mouse_move = false;
	bool is_mark_clicked = false;

	bool is_drag = false;

	const int kMarkItemWidth = 25;
	const int kMarkItemHeight = 18;

	QMap<ovr::uint64, QString> mark_items;

	bool not_clear_mark_on_focus_out = false;
	OvrTMLMarkMenu* mark_menu = nullptr;
};

class OvrTMLMarkMenu : public QMenu
{
	Q_OBJECT
public:
	explicit OvrTMLMarkMenu(QWidget *parent = Q_NULLPTR);
	~OvrTMLMarkMenu() {}

	void initContextMenu();
	void setFlag(bool a_enable);

protected:
	QAction *action_rename, *action_removed;
};
