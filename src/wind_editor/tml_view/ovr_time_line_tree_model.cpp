#include "Header.h"

#include "ovr_time_line_tree_model.h"
#include "ovr_actor_item.h"
#include "ovr_timelinel_item.h"
#include "ovr_tml_axis.h"

#include "ovr_time_line_tree_item.h"
#include "ovr_time_line_tree.h"
#include "ovr_add_mark_dlg.h"
#include "ovr_volume_setting_dlg.h"

#include "../dialog/ovr_save_record_dlg.h"
#include "../dialog/ovr_config_seg_range_dlg.h"
#include "../dialog/ovr_text_setting_dlg.h"

#include <wx_core/track/ovr_clip.h>
#include <wx_core/track/ovr_float_track.h>
#include <wx_project/ovr_render_scene.h>
#include <wx_project/ovr_timeline_editor.h>

#define kInteractiveDefaultLength 400

OvrTimeLineTreeModel* OvrTimeLineTreeModel::g_model_inst = nullptr;

OvrTimeLineTreeModel* OvrTimeLineTreeModel::getInst()
{
	if (g_model_inst == nullptr)
	{
		g_model_inst = new OvrTimeLineTreeModel;
	}
	return g_model_inst;
}

OvrTimeLineTreeModel::OvrTimeLineTreeModel(QObject *parent)
	:QAbstractItemModel(parent)
{
	rootItem = CreateTMLTreeItem("Track", nullptr, nullptr, false);

	QStringList e = { "onProjectOpen", "onOpenSequence", "onCurrentSeqChanged", "OnRmvActor" };

	registerEventListener(e);
}

OvrTimeLineTreeModel::~OvrTimeLineTreeModel()
{
	delete rootItem;
}

int OvrTimeLineTreeModel::columnCount(const QModelIndex &parent) const
{
	if (parent.isValid())
		return static_cast<OvrTimeLineTreeItem*>(parent.internalPointer())->columnCount();
	else
		return rootItem->columnCount();
}

QVariant OvrTimeLineTreeModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
	{
		return QVariant();
	}

	OvrTimeLineTreeItem *item = static_cast<OvrTimeLineTreeItem*>(index.internalPointer());

	if (role == Qt::DisplayRole)
	{
		return item->data(index.column());
	}

	if (role == Qt::ForegroundRole)
	{
		auto text_color = "#CCCCCC";
		if (index.column() == 0)
		{
			static const char* text_colors[] = { "#CC0000","#00CC00","#0000CC" };
			static QMap<QString, int> color_maps = { { "X", 0 },{ "Y", 1 },{ "Z", 2 } };
			auto tag = item->getTrack()->GetTag().GetStringConst();
			if (color_maps.contains(tag))
			{
				text_color = text_colors[color_maps[tag]];
			}
		}
		return QBrush(QColor(text_color));
	}

	return QVariant();
}

Qt::ItemFlags OvrTimeLineTreeModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return 0;

	return QAbstractItemModel::flags(index);
}

QVariant OvrTimeLineTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
	{
		return rootItem->data(section);
	}

	return QVariant();
}

QModelIndex OvrTimeLineTreeModel::index(int row, int column, const QModelIndex &parent) const
{
	if (!hasIndex(row, column, parent))
	{
		return QModelIndex();
	}

	OvrTimeLineTreeItem *parentItem = nullptr;

	if (!parent.isValid())
	{
		parentItem = rootItem;
	}
	else
	{
		parentItem = static_cast<OvrTimeLineTreeItem*>(parent.internalPointer());
	}

	OvrTimeLineTreeItem *childItem = parentItem->child(row);
	if (childItem)
	{
		return createIndex(row, column, childItem);
	}
	else
	{
		return QModelIndex();
	}
}

QModelIndex OvrTimeLineTreeModel::parent(const QModelIndex &index) const
{
	if (!index.isValid())
		return QModelIndex();

	OvrTimeLineTreeItem *childItem = static_cast<OvrTimeLineTreeItem*>(index.internalPointer());
	OvrTimeLineTreeItem *parentItem = childItem->parentItem();

	if (parentItem == rootItem)
		return QModelIndex();

	return createIndex(parentItem->row(), 0, parentItem);
}

int OvrTimeLineTreeModel::rowCount(const QModelIndex &parent) const
{
	OvrTimeLineTreeItem* parentItem = nullptr;
	if (parent.column() > 0)
	{
		return 0;
	}

	if (!parent.isValid())
	{
		parentItem = rootItem;
	}
	else
	{
		parentItem = static_cast<OvrTimeLineTreeItem*>(parent.internalPointer());
	}

	return parentItem->childCount();
}

bool OvrTimeLineTreeModel::openProject(const char* a_project_dir)
{
	qDebug() << "OvrTimeLineTreeModel::openProject: " << a_project_dir;
	return false;
}

void OvrTimeLineTreeModel::onProjectOpen(bool is_ok, QString a_prj_dir)
{
	beginResetModel();
	rootItem->clearChild();
	endResetModel();

	setCurrentSeq(nullptr);
}

void OvrTimeLineTreeModel::setCurrentSeq(ovr_core::OvrSequence* a_seq)
{
	if (current_seq == a_seq)
	{
		return;
	}
	current_seq = a_seq;
	if (a_seq == nullptr)
	{
		return;
	}

	assert(a_seq->GetDurtion() > 0);
	assert(a_seq->GetFPS() >= 20);

	emit onSeuenceqOpenend(a_seq->GetDurtion(), a_seq->GetFPS());
}

void OvrTimeLineTreeModel::configSequcence()
{
	int new_fps = 0, new_secs = 0;
	getGlobalAxisView()->getRange(new_secs, new_fps);

	OvrConfigSegRangeDlg dlg(new_secs, new_fps);
	if (dlg.exec() == 1)
	{
		if (new_fps != dlg.getFps() || new_secs != dlg.getSecs())
		{
			new_fps = dlg.getFps();
			new_secs = dlg.getSecs();

			assert(current_seq != nullptr);

			current_seq->SetFPS(new_fps);
			current_seq->SetDurtion(new_fps * new_secs);

			getGlobalAxisView()->setRange(new_secs, new_fps);
			emit onSeuenceqOpenend(current_seq->GetDurtion(), current_seq->GetFPS());
			OvrTimeLineTree::getInst()->update();
		}
	}
}

bool OvrTimeLineTreeModel::settingText(int a_frame, OvrTimeLineTreeItem* a_item)
{
	ovr_core::OvrClipTrack* clip_track = static_cast<ovr_core::OvrClipTrack*>(a_item->getTrack());
	ovr_core::OvrTextClip* clip_info = static_cast<ovr_core::OvrTextClip*>(clip_track->Clip(a_frame));
	OvrTextSettingDlg dlg;
	dlg.setValue(clip_info->GetText(), clip_info->GetShowTime());
	if (dlg.exec() == QDialog::Accepted)
	{
		clip_info->SetText(dlg.getValueText());
		clip_info->SetShowTime(dlg.getValueTime());
		return true;
	}
	return false;
}

//设置音频轨道上音频clip的音量大小, 通过右键菜单的方式调出设置窗口
bool OvrTimeLineTreeModel::settingAudio(int a_frame, OvrTimeLineTreeItem* a_item)
{
	ovr_core::OvrClipTrack* clip_track = static_cast<ovr_core::OvrClipTrack*>(a_item->getTrack());
	ovr_core::OvrAudioClip* clip_info = static_cast<ovr_core::OvrAudioClip*>(clip_track->Clip(a_frame));

	float v = clip_info->GetVolume();

	//setting UI
	//v = ...
	OvrVolumeSettingDlg volumeSettingDlg(v, OvrHelper::getInst()->getMainWindow());
	if (volumeSettingDlg.exec() == QDialog::Accepted) {

		float new_volume = volumeSettingDlg.get_volume();
		clip_info->SetVolume(new_volume);
	}

	return true;
}

void OvrTimeLineTreeModel::onCurrentSeqChanged(QString a_seq_id)
{
	if (a_seq_id.isEmpty())
	{
		setCurrentSeq(nullptr);
		return;
	}

	ovr_project::OvrProject* prj = ovr_project::OvrProject::GetIns();
	ovr_core::OvrSceneContext* scene = prj->GetCurrentScene();
	prj->OpenSequence(a_seq_id.toLocal8Bit().constData());
}

void OvrTimeLineTreeModel::saveRecord()
{
	if (isPause())
	{
		OVRSaveRecordDlg dlg;
		/*if (dlg.init(current_seq->GetActorTrackList()) > 0)
		{
			dlg.exec();
		}
		else*/
		{
			QMessageBox msgBox;
			msgBox.setText(QString::fromLocal8Bit("没有需要保存的捕捉轨道."));
			msgBox.exec();
		}
	}
}

void OvrTimeLineTreeModel::play(bool a_record_flag, bool is_vr_mode)
{
	if (!isSeuenceqValid())
	{
		return;
	}

	if (a_record_flag)
	{
		bool is_modified = false;
		int counfigure_count = 0;
		if (GetCaptureTrackCount(&is_modified, &counfigure_count) > 0)
		{
			//提示用户保存
			if (is_modified)
			{
				QMessageBox msgBox;
				msgBox.setText(QString::fromLocal8Bit("如果继续,已录制的数据会被清除."));
				msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
				if (msgBox.exec() == QMessageBox::Cancel)
				{
					return;
				}
			}
			else if (counfigure_count <= 0)
			{
				QMessageBox msgBox;
				msgBox.setText(QString::fromLocal8Bit("请为动作或表情捕捉轨道配置数据源."));
				msgBox.exec();
				return;
			}
		}
		else
		{
			QMessageBox msgBox;
			msgBox.setText(QString::fromLocal8Bit("录制前请先添加动作或表情捕捉轨道."));
			msgBox.exec();
			return;
		}
	}
	if (is_vr_mode)
	{
		//current_seq->SetVRMode(true);
	}
	auto player = ovr_project::OvrProject::GetIns()->GetPlayer();
	player->Play();
	player->SetRecord(a_record_flag);
}

bool OvrTimeLineTreeModel::isRecord()
{
	return current_seq != nullptr && current_seq->IsRecord();
}

bool OvrTimeLineTreeModel::isPause()
{
	if (current_seq != nullptr)
	{
		if (current_seq->GetCurrentStatus() == ovr_core::kRenderPause || current_seq->GetCurrentStatus() == ovr_core::kRenderSeek)
		{
			return true;
		}
	}
	return false;
}

int OvrTimeLineTreeModel::GetCaptureTrackCount(bool* a_is_modified, int* configured_count)
{
	int count = 0;
	if (configured_count != nullptr)
	{
		*configured_count = 0;
	}

	if (current_seq != nullptr)
	{
		/*for (auto actor_track : current_seq->GetActorTrackList())
		{
			for (auto child_track : actor_track.second->GetChildren())
			{
				if (child_track->GetTrackType() == ovr_core::kTrackSkeletalCapture)
				{
					++count;
					ovr_core::OvrMotionCaptureTrack* motion_track = static_cast<ovr_core::OvrMotionCaptureTrack*>(child_track);
					if (a_is_modified != nullptr && *a_is_modified == false)
					{
						*a_is_modified = motion_track->IsModified();
					}
					if (configured_count != nullptr && motion_track->GetDevice().GetStringSize() > 0)
					{
						*configured_count += 1;
					}
				}
				else if (child_track->GetTrackType() == ovr_core::kTrackMorphCapture)
				{
					++count;
					ovr_core::OvrMorphCaptureTrack* face_track = static_cast<ovr_core::OvrMorphCaptureTrack*>(child_track);
					if (a_is_modified != nullptr && *a_is_modified == false)
					{
						*a_is_modified = face_track->IsModified();
					}
					if (configured_count != nullptr && face_track->GetDevice().GetStringSize() > 0)
					{
						*configured_count += 1;
					}
				}
			}
		}*/
	}

	return count;
}

bool OvrTimeLineTreeModel::seek(int a_frame)
{
	if (!isSeuenceqValid())
	{
		return true;
	}
	if (current_seq->IsRecord())
	{
		return false;
	}
	if (a_frame == -1)
	{
		a_frame = current_seq->GetCurrentFrame();
	}
	ovr_project::OvrProject::GetIns()->GetPlayer()->Seek(a_frame);
	return true;
}

void OvrTimeLineTreeModel::pause()
{
	if (!isSeuenceqValid())
	{
		return;
	}
	qDebug() << "pause";

	ovr_project::OvrProject::GetIns()->GetPlayer()->Pause();
}

bool OvrTimeLineTreeModel::addSpecialTrack(ovr_core::OvrTrackType a_type, const char* a_track_tag)
{
	ovr_core::OvrTrack* track = (ovr_core::OvrTrack*)current_seq->AddTrack(a_type, a_track_tag);
	assert(track != nullptr);

	beginInsertRows(QModelIndex(), rowCount(), rowCount());
	CreateTMLTreeItem(track->GetDisplayName(), rootItem, track, true);
	endInsertRows();

	return true;
}


static ovr_engine::OvrActor* ActorID2Actor(const QString& a_actor_id)
{
	if (!a_actor_id.isEmpty());
	{
		ovr_core::OvrSceneContext* scene = ovr_project::OvrProject::GetIns()->GetCurrentScene();
		if (scene != nullptr)
		{
			return scene->GetActorByUniqueName(a_actor_id.toLocal8Bit().constData());
		}
	}
	return nullptr;
}

void OvrTimeLineTreeModel::addActorTrack(const QString& a_actor_id)
{
	auto actor = ActorID2Actor(a_actor_id);
	if (actor != nullptr)
	{
		auto actor_track = ovr_project::OvrProject::GetIns()->GetCurrentTimelineEditor()->AddActorTrack(a_actor_id.toLocal8Bit().constData());
		beginInsertRows(QModelIndex(), rowCount(), rowCount());
		CreateTMLTreeItem(actor->GetDisplayName(), rootItem, actor_track, true);
		endInsertRows();
	}
}

void OvrTimeLineTreeModel::removeTrack(OvrTimeLineTreeItem* a_item, const QModelIndex& a_index)
{
	assert(a_item != nullptr && a_index.isValid());

	OvrTimeLineTreeItem* parent_item = a_item->parentItem();

	//通过sequence删除独立轨道
	if (parent_item->getTrack() == nullptr)
	{
		beginRemoveRows(a_index.parent(), a_index.row(), a_index.row());
		current_seq->RmvTrack(a_item->getTrack());
		parent_item->removeChildItem(a_item);
		endRemoveRows();
	}
	else
	{
		beginRemoveRows(a_index.parent(), a_index.row(), a_index.row());
		parent_item->getTrack()->RmvTrack(a_item->getTrack());
		parent_item->removeChildItem(a_item);
		endRemoveRows();
	}
}

void OvrTimeLineTreeModel::onRemoveTimeLineActor(QString a_actor_id)
{
	auto target_track = ovr_project::OvrProject::GetIns()->GetCurrentTimelineEditor()->GetActorTrack(a_actor_id.toLocal8Bit().constData());
	if (target_track == nullptr)
	{
		return;
	}

	for (int i = 0; i < rowCount(); i++)
	{
		QModelIndex temp_index = index(i, 0);
		OvrTimeLineTreeItem* item = static_cast<OvrTimeLineTreeItem*>(temp_index.internalPointer());

		if (item->isContainerItem())
		{
			if (item->getTrack() == target_track)
			{
				beginRemoveRows(temp_index.parent(), temp_index.row(), temp_index.row());

				OvrTimeLineTreeItem* parent_item = item->parentItem();
				assert(parent_item != nullptr);

				bool removed_ok = current_seq->RmvTrack(target_track);
				assert(removed_ok);

				parent_item->removeChildItem(item);

				endRemoveRows();

				break;
			}
		}
	}

}

QModelIndex OvrTimeLineTreeModel::addChildTrack(const QModelIndex& a_index, int a_track_type, const QString& a_sub_type)
{
	OvrTimeLineTreeItem* item = static_cast<OvrTimeLineTreeItem*>(a_index.internalPointer());

	ovr_core::OvrTrack* parent_track = item->getTrack();

	ovr_core::OvrTrack* sub_track = parent_track->AddTrack((ovr_core::OvrTrackType)a_track_type, a_sub_type.toLocal8Bit().constData());
	assert(sub_track != nullptr);

	ovr_core::OvrTrack* dest_parent_track = sub_track->GetParent();
	assert(dest_parent_track != nullptr);

	if (dest_parent_track != parent_track)
	{
		for (int i = 0; i < item->childCount(); ++i)
		{
			auto child_item = item->child(i);
			if (dest_parent_track == child_item->getTrack())
			{
				QModelIndex child_index = index(i, 0, a_index);
				int row_pos = child_item->childCount();
				beginInsertRows(child_index, row_pos, row_pos);
				CreateTMLTreeItem(sub_track->GetDisplayName(), child_item, sub_track, true);
				endInsertRows();
				return child_index.child(row_pos, 0);
			}
		}
		assert(false);
		return QModelIndex();
	}

	int row_pos = item->childCount();
	beginInsertRows(a_index, row_pos, row_pos);
	OvrTimeLineTreeItem* new_item = CreateTMLTreeItem(sub_track->GetDisplayName(), item, sub_track, true);
	endInsertRows();

	return a_index.child(row_pos, 0);
}

bool OvrTimeLineTreeModel::addClipFile(const QModelIndex& a_index, const QByteArray& a_file_path, int a_pos)
{
	OvrTimeLineTreeItem* item = static_cast<OvrTimeLineTreeItem*>(a_index.internalPointer());
	ovr_core::OvrClipTrack* clip_track = static_cast<OvrTMLClipItem*>(item)->getClipTrack();
	ovr::uint64 duration = clip_track->GetAssetDuration(a_file_path.constData());

	int frame = round(getGlobalAxisView()->getFrameForGlobalDPX(a_pos));

	if (clip_track->AddClip(frame, a_file_path.constData(), 0, duration))
	{
		emit onSetSelectItem(a_index.sibling(a_index.row(), 2), item, frame);
		return true;
	}
	return false;
}

bool OvrTimeLineTreeModel::addKeyFrame(int a_frame, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index, QModelIndex& a_update_index)
{
	assert(a_index.column() == 0);

	bool is_added = false;
	float value = 0;

	/*switch (a_item->getTrack()->GetTrackType())
	{
	case ovr_core::kTrackGroup:
		if (!a_item->contains(a_frame))
		{
			is_added = addGroupFrame(a_item, a_frame);
			a_item->getTrack()->Update();
		}
		break;
	case ovr_core::kTrackFloat:
		if (!a_item->contains(a_frame))
		{
			if (getTransFormValue(a_item, value))
			{
				auto float_track = static_cast<ovr_core::OvrFloatTrack*>(a_item->getTrack());
				float_track->AddKeyFrame(a_frame, value);
				is_added = true;
				float_track->UpdateCurrentValue(a_frame);
			}
			else
			{
				is_added = addFloatFrame(a_item, a_frame);
			}
		}
		break;
	case ovr_core::kTrackEvent:
		is_added = addEventFrame(a_item, a_frame);
		break;
	case ovr_core::kTrackInteractive:
		is_added = addInterActiveClip(a_item, a_frame);
		break;
	case ovr_core::kTrackClip:
		is_added = static_cast<ovr_core::OvrClipTrack*>(a_item->getTrack())->AddClip(a_frame) != nullptr;
		break;
	default:
		qDebug() << "addKeyFrame do nothing for track: " << a_item->getTrack()->GetTrackType();
		break;
	}*/

	//选中当前添加的帧
	if (false)//is_added)
	{
		a_update_index = a_index.column() != 2 ? a_index.sibling(a_index.row(), 2) : a_index;
		emit onSetSelectItem(a_index.sibling(a_index.row(), 2), a_item, a_frame);
	}

	return is_added;
}

bool OvrTimeLineTreeModel::getTransFormValue(OvrTimeLineTreeItem* a_item, float& a_value)
{
	static QMap<QString, int> xyz_pos = { { "X", 0 },{ "Y", 1 },{ "Z", 2 } };
	static QMap<QString, int> rts_pos = { { "position", 0 },{ "rotation", 1 },{ "scale", 2 } };

	auto xyz_tag = a_item->getTrack()->GetTag().GetStringConst();
	auto rts_tag = a_item->getTrack()->GetParent()->GetTag().GetStringConst();
	if (xyz_pos.contains(xyz_tag) && rts_pos.contains(rts_tag))
	{
		OvrVector3 values;
		auto actor = a_item->getActorTrack()->GetActor();
		switch (rts_pos[rts_tag])
		{
		case 0:
			values = actor->GetPosition();
			break;
		case 1:
			actor->GetEulerAngle(values._x, values._y, values._z);
			break;
		case 2:
			values = actor->GetScale();
			break;
		default:
			assert(false);
			break;
		}
		a_value = values._m[xyz_pos[xyz_tag]];
		return true;
	}

	return false;
}

bool OvrTimeLineTreeModel::renameKeyFrame(int a_frame, OvrTimeLineTreeItem* a_item)
{
	/*switch (a_item->getTrack()->GetTrackType())
	{
	case ovr_core::kTrackInteractive:
		return renameInterActiveClip(a_item, a_frame);
	case ovr_core::kTrackEvent:
		return renameEventFrame(a_item, a_frame);
	default:
		qDebug() << "renameKeyFrame do nothing for track: " << a_item->getTrack()->GetTrackType();
		break;
	}*/
	return false;
}

bool OvrTimeLineTreeModel::cloneEvent(int a_frame, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index, QModelIndex& a_update_index, const QString& a_event)
{
	if (a_item->contains(a_frame))
	{
		return false;
	}
	assert(!a_event.isEmpty());

	ovr_core::OvrEventTrack* track = static_cast<ovr_core::OvrEventTrack*>(a_item->getTrack());
	track->AddKeyFrame(a_frame, a_event.toLocal8Bit().constData());

	a_update_index = a_index.sibling(a_index.row(), 2);
	emit onSetSelectItem(a_update_index, a_item, a_frame);

	return true;
}

bool OvrTimeLineTreeModel::replaceEvent(int a_frame, OvrTimeLineTreeItem* a_item, const QString& a_event)
{
	assert(!a_event.isEmpty());

	if (a_item->contains(a_frame))
	{
		ovr_core::OvrEventTrack* track = static_cast<ovr_core::OvrEventTrack*>(a_item->getTrack());
		track->AddKeyFrame(a_frame, a_event.toLocal8Bit().constData());
		return true;
	}
	return true;
}

bool OvrTimeLineTreeModel::cloneInteractive(int a_frame, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index, QModelIndex& a_update_index, const QString& a_name)
{
	assert(!a_name.isEmpty());

	ovr_core::OvrClipTrack* track = static_cast<OvrTMLClipItem*>(a_item)->getClipTrack();
	auto clip = track->Clip(a_frame);
	if (clip == nullptr)
	{
		int begin = 0, end = kInteractiveDefaultLength;
		track->AddClip(a_frame, a_name.toLocal8Bit().constData(), begin, end);
		a_update_index = a_index.sibling(a_index.row(), 2);
		emit onSetSelectItem(a_update_index, a_item, a_frame);
		return true;
	}

	return false;
}

bool OvrTimeLineTreeModel::replaceInteractive(int a_frame, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index, QModelIndex& a_update_index, const QString& a_name)
{
	assert(!a_name.isEmpty());

	if (a_item->contains(a_frame))
	{
		ovr_core::OvrClipTrack* track = static_cast<OvrTMLClipItem*>(a_item)->getClipTrack();
		track->Clip(a_frame)->file_path = a_name.toLocal8Bit().constData();
		a_update_index = parent(a_index).child(a_index.row(), 2);
		return true;
	}
	return false;
}

bool OvrTimeLineTreeModel::addEventFrame(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame)
{
	if (a_item->contains(a_frame))
	{
		return false;
	}

	ovr_core::OvrEventTrack* track = static_cast<ovr_core::OvrEventTrack*>(a_item->getTrack());

	QMap<ovr::uint64, QString> key_items;
	{
		for (auto item : track->GetAllKeyFrames())
		{
			key_items[item.first] = QString::fromLocal8Bit(item.second.GetStringConst());
		}
	}

	int number = track->GetEvents().size();
	QString new_name = QString::fromLocal8Bit("事件") + QString::number(number);
	while (track->IsHaveEvent(new_name.toLocal8Bit().constData()))
	{
		new_name = QString::fromLocal8Bit("事件") + QString::number(++number);
	}

	OvrAddMarkDlg dlg;
	dlg.setMode(a_frame, new_name, &key_items, QString::fromLocal8Bit("事件"), true);

	if (dlg.exec() == 1)
	{
		track->CreateEvent(dlg.getKeyName().toLocal8Bit().constData());
		track->AddKeyFrame(a_frame, dlg.getKeyName().toLocal8Bit().constData());
		return true;
	}
	return false;
}

bool OvrTimeLineTreeModel::addTransformFrame(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame)
{
#if 0
	//不判断是否已经存在key frame, 覆盖当前帧的数据
	ovr_core::OvrActorTrack* actor_track = a_item->getActorTrack();
	ovr_engine::OvrActor* actor = actor_track->GetActor();
	ovr_core::OvrTransformTrack* transfrom_track = static_cast<ovr_core::OvrTransformTrack*>(a_item->getTrack());
	transfrom_track->AddKeyFrame(a_frame, actor->GetTransform());
	return true;
#endif
	return false;
}

bool OvrTimeLineTreeModel::addGroupFrame(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame)
{
#if 0
	auto group_track = static_cast<ovr_core::OvrGroupTrack*>(a_item->getTrack());
	group_track->AddValue(a_frame);
	return true;
#endif
	return false;
}
/*
bool OvrTimeLineTreeModel::addTransparentFrame(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame)
{
if (a_item->contains(a_frame))
{
return false;
}

ovr_project::OvrFloatTrack* track = static_cast<ovr_project::OvrFloatTrack*>(a_item->getTrack());
assert(track != nullptr);

OvrAddMarkDlg dlg;
dlg.setMode(a_frame, "0.0", nullptr, QString::fromLocal8Bit("透明值"), true);
if (dlg.exec() == 1)
{
float value = dlg.getKeyName().toFloat();
if (value <= 0) value = 0;
if (value > 1)  value = 1;
track->AddKeyFrame(a_frame, value);
}
return true;
}
*/
bool OvrTimeLineTreeModel::addFloatFrame(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame)
{
#if 0
	ovr_core::OvrFloatTrack* track = static_cast<ovr_core::OvrFloatTrack*>(a_item->getTrack());
	assert(track != nullptr);

	OvrAddMarkDlg dlg;
	QString title = a_item->data(0).toString() + QString::fromLocal8Bit("值");
	dlg.setMode(a_frame, "0.0", nullptr, title, true);
	if (dlg.exec() == 1)
	{
		float value = dlg.getKeyName().toFloat();
		track->AddKeyFrame(a_frame, value);
		track->UpdateCurrentValue(a_frame);
		return true;
	}
#endif
	return false;
}

bool OvrTimeLineTreeModel::addInterActiveClip(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame)
{
	ovr_core::OvrInterActiveTrack* track = static_cast<ovr_core::OvrInterActiveTrack*>(a_item->getTrack());

	const std::map<ovr::uint64, ovr_core::OvrClipInfo*>& clips = track->GetClipMap();
	QMap<ovr::uint64, QString> key_items;
	{
		for (auto item : clips)
		{
			key_items[item.first] = QString::fromLocal8Bit(item.second->file_path.GetStringConst());
		}
	}

	int number = ovr_core::IOvrNodeManager::GetIns()->GetClusterFileList(ovr_core::IOvrNodeCluster::kArea).size();

	QString new_name = QString::fromLocal8Bit("交互区") + QString::number(number);
	while (true)
	{
		bool is_name_overlapped = false;
		for (auto item : key_items)
		{
			if (item == new_name)
			{
				is_name_overlapped = true;
				break;
			}
		}
		if (is_name_overlapped)
		{
			new_name = QString::fromLocal8Bit("交互区") + QString::number(++number);
		}
		else
		{
			break;
		}
	}

	OvrAddMarkDlg dlg;
	dlg.setMode(a_frame, new_name, &key_items, QString::fromLocal8Bit("交互区"), true);

	if (dlg.exec() == 1)
	{
		OvrString file_name(dlg.getKeyName().toLocal8Bit().constData());
		track->CreateCluster(file_name);

		int begin = 0, end = kInteractiveDefaultLength;
		track->AddClip(a_frame, file_name, begin, end);
		return true;
	}
	return false;
}

bool OvrTimeLineTreeModel::renameEventFrame(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame)
{
	if (!a_item->contains(a_frame))
	{
		return false;
	}

	ovr_core::OvrEventTrack* track = static_cast<ovr_core::OvrEventTrack*>(a_item->getTrack());

	QMap<ovr::uint64, QString> key_items;
	{
		const std::map<ovr::uint64, OvrString>& key_frames = track->GetAllKeyFrames();
		for (auto item : key_frames)
		{
			key_items[item.first] = QString::fromLocal8Bit(item.second.GetStringConst());
		}
	}

	OvrAddMarkDlg dlg;
	dlg.setMode(a_frame, QString::fromLocal8Bit(track->GetKeyFrame(a_frame).GetStringConst()),
		&key_items, QString::fromLocal8Bit("事件"), false);

	if (dlg.exec() == 1)
	{
		track->AddKeyFrame(a_frame, dlg.getKeyName().toLocal8Bit().constData());
		return true;
	}
	return false;
}

bool OvrTimeLineTreeModel::renameInterActiveClip(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame)
{
	if (!a_item->contains(a_frame))
	{
		return false;
	}

	QMap<ovr::uint64, QString> key_items;
	{
		static_cast<OvrTMLClipItem*>(a_item)->getClipTrack()->GetClipMap();
		for (auto item : static_cast<OvrTMLClipItem*>(a_item)->getClipTrack()->GetClipMap())
		{
			key_items[item.first] = QString::fromLocal8Bit(item.second->file_path.GetStringConst());
		}
	}

	OvrAddMarkDlg dlg;
	const char* file_path = static_cast<OvrTMLClipItem*>(a_item)->getClipTrack()->Clip(a_frame)->file_path.GetStringConst();
	dlg.setMode(a_frame, QString::fromLocal8Bit(file_path), &key_items, QString::fromLocal8Bit("交互区"), false);

	if (dlg.exec() == 1)
	{
		int begin = 0, end = 150;
		ovr_core::OvrInterActiveTrack* track = static_cast<ovr_core::OvrInterActiveTrack*>(a_item->getTrack());
		track->AddClip(a_frame, dlg.getKeyName().toLocal8Bit().constData(), begin, end);
		return true;
	}
	return false;
}
void OvrTimeLineTreeModel::addMark(ovr::uint64 a_frame)
{
	emit markAdded(a_frame);
}

void OvrTimeLineTreeModel::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{
	if (a_event_name == "onOpenSequence")
	{
		void* p = a_param1.value<void*>();
		auto a_new_sequence = static_cast<ovr_core::OvrSequence*>(p);
		onOpenSequence(a_new_sequence);
	}
	else if (a_event_name == "onProjectOpen")
	{
		onProjectOpen(a_param1.toBool(), a_param2.toString());
	}
	else if (a_event_name == "onCurrentSeqChanged")
	{
		onCurrentSeqChanged(a_param1.toString());
	}
	else if (a_event_name == "OnRmvActor")
	{
		onRemoveTimeLineActor(a_param1.toString());
	}
}

void OvrTimeLineTreeModel::onOpenSequence(ovr_core::OvrSequence* a_new_sequence)
{
	assert(a_new_sequence != nullptr);
	setCurrentSeq(a_new_sequence);

	beginResetModel();

	/*for (auto actor_track : a_new_sequence->GetActorTrackList())
	{
		ovr_engine::OvrActor* bind_actor = actor_track.second->GetActor();
		assert(bind_actor != nullptr);
		CreateTMLTreeItem(bind_actor->GetDisplayName(), rootItem, actor_track.second, true);
	}

	for (auto track : a_new_sequence->GetSpecialTrackList())
	{
		if (track->GetTrackType() == ovr_core::kTrackMark)
		{
			continue;
		}
		CreateTMLTreeItem(track->GetDisplayName(), rootItem, track, true);
	}*/

	endResetModel();
}