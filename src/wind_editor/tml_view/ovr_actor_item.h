#pragma once

#include "ovr_time_line_tree_item.h"


//容器轨道,可以添加,容纳, 容器轨道以及数据轨道
//容器轨道,能否添加,可以添加什么子轨道,由逻辑层的actor track决定
//容器轨道,分Actor容器, Group容器
class OvrTMLContainerItem : public OvrTimeLineTreeItem
{
public:
	void addContainer();
	void addPointTrack();
	void addClipTrack();

protected:
	ovr_core::OvrActorTrack* track = nullptr;
};