#include "Header.h"
#include "ovr_tml_curve_editor.h"

#include "ovr_tml_axis.h"


OvrTMLCurveEditor::OvrTMLCurveEditor(QWidget *parent, Qt::WindowFlags f) :OvrTMLCurveAxis(parent, f)
{
}

void OvrTMLCurveEditor::onDrawContext(QPainter& painter)
{
	return;

	painter.save();

	painter.setBrush(Qt::NoBrush);
	painter.setPen(QColor(0, 180, 0));

	painter.drawRect(getLPXForValue(600), getLPYForValue(-0.2), getLPXForValue(900), getLPYForValue(0.4));

	painter.restore();
}