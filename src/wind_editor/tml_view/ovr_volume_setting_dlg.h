#pragma once

#include <QDialog>
#include "ui_ovr_volume_setting_dlg.h"

class OvrVolumeSettingDlg : public QDialog
{
	Q_OBJECT

public:
	OvrVolumeSettingDlg(float volume, QWidget *parent = Q_NULLPTR);
	~OvrVolumeSettingDlg();

	float get_volume();

	private Q_SLOTS:;
	void on_btn_ok_clicked();
	void on_btn_cancel_clicked();

	void on_update_display(int);

	void on_slider_pos();

private:
	void init_custom_ui();
	void set_custom_style_sheet();
	void preset_connects();

	int volume_to_display(float vol, float min = 0, float max = 1);
	float display_to_volume(int dvol, int min = 0, int max = 100);
private:
	Ui::OvrVolumeSettingDlg ui;

	float m_volume;
};
