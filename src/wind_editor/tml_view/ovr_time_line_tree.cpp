#include "Header.h"
#include "ovr_time_line_tree.h"
#include "ovr_time_line_head.h"
#include "ovr_tree_item_delegate.h"
#include "ovr_time_line_tree_model.h"
#include "ovr_time_line_tree_item.h"
#include "ovr_timelinel_item.h"

#include "../project/ovr_player_listener.h"

#include "ovr_actor_item.h"

#include "../res_view/ovr_resource_tree_model.h"
#include "../res_view/ovr_resource_list_widget.h"

#include "../editor/ovr_obj_editor_wnd.h"
#include "../editor/ovr_obj_editor_cluster_manager.h"

#include "ovr_tml_axis.h"

#include <QPalette>

#include <wx_core/track/ovr_float_track.h>

////////////////////////////////////////////////
// class QTreeItemMenu
static 	const char* strMenuStyle =
	"QMenu {background:rgb(39,39,39); color:rgb(205,205,205); selection-background-color: rgb(60,60,60); selection-color: rgb(205,205,205);}"
	"QMenu::item:disabled {color:gray}";

bool QTreeItemMenu::showRightMenu(const QPoint a_pt, OvrTimeLineTreeItem* a_item, int a_key_frame, bool a_is_Column0, QModelIndex a_index)
{
	QTreeItemMenu * menu = new QTreeItemMenu;
	menu->is_Column0 = a_is_Column0;
	menu->item = a_item;
	menu->item_index = a_index;
	menu->key_frame = a_key_frame;

	menu->initMenu();

	if (menu != nullptr)
	{
		connect(menu, &QMenu::aboutToHide, menu, &QTreeItemMenu::aboutToHide, Qt::QueuedConnection);
		connect(menu, &QMenu::triggered, menu, &QTreeItemMenu::triggered, Qt::QueuedConnection);
		menu->move(a_pt);
		menu->show();

		return true;
	}
	return false;
}

void QTreeItemMenu::initMenu()
{
	/*switch (item->getTrack()->GetTrackType())
	{
	case ovr_core::kTrackEvent:
		initForEventActor();
		break;
	case ovr_core::kTrackInteractive:
		initForInterActiveActor();
		break;
	case ovr_core::kTrackMorphCapture:
	case ovr_core::kTrackSkeletalCapture:
		initForCaptureTrack();
		break;
	case ovr_core::kTrackClip:
		initForClip(static_cast<ovr_core::OvrClipTrack*>(item->getTrack())->GetClipType());
		break;
	default:
		addRemoveItem();
		break;
	}*/
}

void QTreeItemMenu::initForClip(ovr_core::OvrClipInfo::Type a_clip_type)
{
	switch (a_clip_type)
	{
	case ovr_core::OvrClipInfo::kInvalid:
		break;
	case ovr_core::OvrClipInfo::kSkeletalAnimation:
		break;
	case ovr_core::OvrClipInfo::kMorphAnimaition:
		break;
	case ovr_core::OvrClipInfo::kText:
		initForTextClip();
		break;
	case ovr_core::OvrClipInfo::kVideo:
		break;
	case ovr_core::OvrClipInfo::kAudio:
		initForAudioClip();
		break;
	default:
		break;
	}

	addRemoveItem();
}

void QTreeItemMenu::initForEventActor()
{
	addNewItem(is_Column0);
	addRemoveItem(!is_Column0);
	addSeparator();

	ovr_core::OvrEventTrack* track = static_cast<ovr_core::OvrEventTrack*>(item->getTrack());
	QMenu* sub_menu = addMenu(is_Column0 ? QString::fromLocal8Bit("选择事件") : QString::fromLocal8Bit("替换事件"));

	//if (track->GetEvents().size() > 0)
	{
		OvrString to_skip;
		if (!is_Column0)
		{
			to_skip = track->GetKeyFrame(key_frame);

			QString str; QTextStream stream(&str);
			stream << QString::fromLocal8Bit("替换事件 ") << "[" << QString::fromLocal8Bit(to_skip.GetStringConst()) << "]";
			sub_menu->setTitle(str);
		}

		for (auto item : track->GetEvents())
		{
			if (!is_Column0 && to_skip == item)
			{
				continue;
			}
			QAction* action = sub_menu->addAction(QString::fromLocal8Bit(item.GetStringConst()));
			action->setObjectName(is_Column0 ? "clone_event" : "replace_event");
		}

	}
	sub_menu->setEnabled(sub_menu->actions().size() > 0);
}

void QTreeItemMenu::initForInterActiveActor()
{
	addNewItem(is_Column0);
	addRemoveItem(!is_Column0);
	addSeparator();

	ovr_core::OvrClipTrack* track = static_cast<OvrTMLClipItem*>(item)->getClipTrack();
	QMenu* sub_menu = addMenu(is_Column0 ? QString::fromLocal8Bit("选择交互区") : QString::fromLocal8Bit("替换交互区"));

	{
		OvrString to_skip;
		if (!is_Column0)
		{
			if (track->Clip(key_frame) != nullptr)
			{
				to_skip = track->Clip(key_frame)->file_path;
			}

			QString str; QTextStream stream(&str);
			stream << QString::fromLocal8Bit("替换交互区 ") << "[" << QString::fromLocal8Bit(to_skip.GetStringConst()) << "]";
			sub_menu->setTitle(str);
		}

		auto areas = ovr_core::IOvrNodeManager::GetIns()->GetClusterFileList(ovr_core::IOvrNodeCluster::kArea);
		for (auto item : areas)
		{
			if (!is_Column0 && to_skip == item)
			{
				continue;
			}
			QAction* action = sub_menu->addAction(QString::fromLocal8Bit(item.GetStringConst()));
			action->setObjectName(is_Column0 ? "clone_interactive" : "replace_interactive");
		}
	}
	sub_menu->setEnabled(sub_menu->actions().size() > 0);
}

void QTreeItemMenu::initForTextClip()
{
	QAction* action = addAction(QString::fromLocal8Bit("设置..."));
	action->setObjectName("text_setting");
}

void QTreeItemMenu::initForAudioClip()
{
	QAction* action = addAction(QString::fromLocal8Bit("设置..."));
	action->setObjectName("audio_setting");
}

void QTreeItemMenu::addNewItem(bool a_enable)
{
	QAction* action = addAction(QString::fromLocal8Bit("新建..."));
	action->setObjectName("new_item");
	action->setEnabled(a_enable);
}

void QTreeItemMenu::addRemoveItem(bool a_enable)
{
	QAction* action = addAction(QString::fromLocal8Bit("删除"));
	action->setObjectName("remove_selection");
	action->setEnabled(a_enable);
}

void QTreeItemMenu::addRenameItem()
{
	QAction* action = addAction(QString::fromLocal8Bit("重命名"));
	action->setObjectName("rename_selection");
}

//删除轨道
bool QTreeItemMenu::showLeftMenu(const QPoint a_pt, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index)
{
	QTreeItemMenu* menu = nullptr;

	{
		menu = new QTreeItemMenu();
		menu->item = a_item;
		menu->item_index = a_index;
		QAction* action = menu->addAction(QString::fromLocal8Bit("删除 [") + a_item->data(0).toString() + "]");
		action->setObjectName("remove");

		//不允许删除transform的子轨道
		auto track = a_item->getTrack();
		if (track != nullptr)
		{
			auto tag = track->GetTag();
			if (tag == "X" || tag == "Y" || tag == "Z")
			{
				if (track->GetParent() != nullptr)
				{
					tag = track->GetParent()->GetTag();
				}
			}
			if (tag == "position" || tag == "rotation" || tag == "scale")
			{
				action->setEnabled(false);
			}
		}
	}

	if (menu != nullptr)
	{
		connect(menu, &QMenu::aboutToHide, menu, &QTreeItemMenu::aboutToHide, Qt::QueuedConnection);
		connect(menu, &QMenu::triggered, menu, &QTreeItemMenu::leftTriggered, Qt::QueuedConnection);
		menu->move(a_pt);
		menu->show();
		return true;
	}
	return false;
}

QTreeItemMenu::QTreeItemMenu(QWidget *parent)
	:QMenu(parent)
{
	setStyleSheet(strMenuStyle);
}

QTreeItemMenu::~QTreeItemMenu()
{
}

void QTreeItemMenu::aboutToHide()
{
	deleteLater();
}

void QTreeItemMenu::leftTriggered(QAction *action)
{
	qDebug() << action->objectName() << " " << item->data(0);

	if (action->objectName() == "remove")
	{
		OvrTimeLineTreeModel::getInst()->removeTrack(item, item_index);
	}
}

void QTreeItemMenu::triggered(QAction *action)
{
	assert(item != nullptr && key_frame >= 0);

	if (action->objectName() == "remove_selection")
	{
		OvrTimeLineTree::getInst()->removeSelecttion();
	}
	else if (action->objectName() == "text_setting")
	{
		if (OvrTimeLineTreeModel::getInst()->settingText(key_frame, item))
		{
			OvrTimeLineTree::getInst()->update(item_index);
		}
	}
	else if (action->objectName() == "audio_setting")
	{
		if (OvrTimeLineTreeModel::getInst()->settingAudio(key_frame, item))
		{
			OvrTimeLineTree::getInst()->update(item_index);
		}
	}
	else if (action->objectName() == "rename_selection")
	{
		OvrTimeLineTree::getInst()->renameSelecttion();
	}
	else if (action->objectName() == "new_item")
	{
		QModelIndex to_be_updated;
		if (OvrTimeLineTreeModel::getInst()->addKeyFrame(key_frame, item, item_index, to_be_updated))
		{
			OvrTimeLineTree::getInst()->update(to_be_updated);
		}
	}
	else  if (action->objectName() == "clone_event")
	{
		QModelIndex to_be_updated;
		if (OvrTimeLineTreeModel::getInst()->cloneEvent(key_frame, item, item_index, to_be_updated, action->text()))
		{
			OvrTimeLineTree::getInst()->update(to_be_updated);
		}
	}
	else  if (action->objectName() == "replace_event")
	{
		OvrTimeLineTreeModel::getInst()->replaceEvent(key_frame, item, action->text());
	}
	else  if (action->objectName() == "clone_interactive")
	{
		QModelIndex to_be_updated;
		if (OvrTimeLineTreeModel::getInst()->cloneInteractive(key_frame, item, item_index, to_be_updated, action->text()))
		{
			OvrTimeLineTree::getInst()->update(to_be_updated);
		}
	}
	else  if (action->objectName() == "replace_interactive")
	{
		QModelIndex to_be_updated;
		if (OvrTimeLineTreeModel::getInst()->replaceInteractive(key_frame, item, item_index, to_be_updated, action->text()))
		{
			OvrTimeLineTree::getInst()->update(to_be_updated);
		}
	}
	else
	{
		assert(false);
	}
}

/////////////////////////////////////////////////////////
// class OvrTreeItemDragObj

void OvrTreeItemDragObj::reset()
{
	drag_item = nullptr;
	drag_index = QModelIndex();
 	drag_pos = -1;
	is_draging_begin = false;
	is_draging = false;
}

void OvrTreeItemDragObj::clickNothing()
{
	if (drag_item != nullptr)
	{
		clearSelection();
		reset();
	}
}

bool OvrTreeItemDragObj::split()
{
	if (drag_item != nullptr && drag_item->splitFromCurrentCusor())
	{
		update();
		return true;
	}
	return false;
}

bool OvrTreeItemDragObj::removeSelecttion()
{
	if (drag_item != nullptr && drag_item->isSelected() && drag_item->removeSelected())
	{
		update();
		reset();
		return true;
	}
	return false;
}

void OvrTreeItemDragObj::renameSelecttion()
{
	assert(drag_item != nullptr);

	if (OvrTimeLineTreeModel::getInst()->renameKeyFrame(drag_item->selectKey(), drag_item))
	{
		update();
	}
}

int OvrTreeItemDragObj::getSelectKey()
{
	if (drag_item != nullptr)
	{
		return drag_item->selectKey();
	}
	return -1;
}

void OvrTreeItemDragObj::onSetSelectItem(QModelIndex a_index, OvrTimeLineTreeItem* a_item, ovr::uint64 a_key)
{ 
	reset();
	drag_item = a_item;
	drag_item->setSelectKey(a_key);
	drag_index = a_index;
}


static bool isExpand(const QModelIndex& index)
{
	if (index.column() != 0)
	{
		const QModelIndex& index_0 = index.model()->index(index.row(), 0, index.parent());
		return  OvrTimeLineTree::getInst()->isExpanded(index_0);
	}
	return OvrTimeLineTree::getInst()->isExpanded(index);
}

static bool isVisible(const QModelIndex& index)
{
	bool ret = OvrTimeLineTree::getInst()->isExpanded(index.parent());
	return ret;
}

void OvrTreeItemDragObj::clearSelection()
{
	if (drag_item != nullptr && drag_item->isSelected())
	{
		drag_item->clearSelection();
		update();
	}
}

bool OvrTreeItemDragObj::beginDrag(const QModelIndex& a_index, OvrTimeLineTreeItem* a_item, int a_pos, bool a_context_menu)
{
	if (drag_item != nullptr && drag_item != a_item)
	{
		clearSelection();
	}

	drag_item = a_item;
	drag_index = a_index;
	drag_pos = a_pos;

	//尝试选中
	if (drag_item->hitTest(drag_pos))
	{
		if (!a_context_menu)
		{
			is_draging_begin = true;
		}
		update();
		return true;
	}
	//清除以前选中
	else if (drag_item->isSelected())
	{
		drag_item->clearSelection();
		update();
	}
	reset();
	return false;
}

void OvrTreeItemDragObj::draging(int a_pos, QMouseEvent *event, const QModelIndex& a_index_under_mouse)
{
	if (!is_draging_begin || drag_item == nullptr)
	{
		return;
	}

	if (!is_draging)
	{
		is_draging = true;
	}

	if (drag_item->isClipDataItem())
	{
		//同父跨轨道拖拽
		if (a_index_under_mouse.isValid() && a_index_under_mouse.column() == 2)
		{
			OvrTimeLineTreeItem* item_under_mouse = static_cast<OvrTimeLineTreeItem*>(a_index_under_mouse.internalPointer());
			//同一个父的平级关系
			if (item_under_mouse->isClipDataItem() && item_under_mouse != drag_item && item_under_mouse->parentItem() == drag_item->parentItem())
			{
				moveClipTo(static_cast<OvrTMLClipItem*>(item_under_mouse), a_index_under_mouse);
			}
		}
	}

	//在当前轨道移动
	if (drag_item->draging(a_pos))
	{
		update();
	}
}

void OvrTreeItemDragObj::moveClipTo(OvrTMLClipItem* a_other_clip_item, const QModelIndex& a_other_item_index)
{
	OvrTMLClipItem* clip_item = static_cast<OvrTMLClipItem*>(drag_item);

	if (a_other_clip_item->dragSelectedClipin(clip_item))
	{
		OvrTimeLineTree::getInst()->update(a_other_item_index);
		OvrTimeLineTree::getInst()->update(drag_index);
		drag_item = a_other_clip_item;
		drag_index = a_other_item_index;
	}
}

void OvrTreeItemDragObj::endDrag(QMouseEvent *event)
{
	is_draging_begin = false;
	is_draging = false;
}

void OvrTreeItemDragObj::update()
{
	if (drag_item == nullptr)
	{
		return;
	}

	OvrTimeLineTree::getInst()->update(drag_index);
}

/////////////////////////////////////////////////////////
// class OvrTimeLineTree

static OvrTimeLineTree* tml_tree_inst = nullptr;

OvrTimeLineTree* OvrTimeLineTree::getInst()
{
	assert(tml_tree_inst != nullptr);
	return tml_tree_inst;
}

OvrTimeLineTree::OvrTimeLineTree(QWidget *parent):QTreeView(parent)
{
	tml_tree_inst = this;

	initTreeStyle();

	auto item_deltegate = new OvrTreeItemDelegate(this);
	setItemDelegate(item_deltegate);
	setModel(OvrTimeLineTreeModel::getInst());

	OvrTimeLineHead* head = new OvrTimeLineHead(Qt::Horizontal, this);
	setHeader(head);

	//游标线
	ovr_tml_v_line = getGlobalAxisView()->createCursorLine(this);
	ovr_tml_v_line->setGeometry(0, tml_v_line_y_offset, 1, 0);

	drag_obj = new OvrTreeItemDragObj(this);

	//选中列项
	connect(OvrTimeLineTreeModel::getInst(), &OvrTimeLineTreeModel::onSetSelectItem, drag_obj, &OvrTreeItemDragObj::onSetSelectItem, Qt::QueuedConnection);

	connect(getGlobalAxisView(), &OvrTMLAxisView::onWndPortChange, [this]() {
		viewport()->update();
	});

	connect(getGlobalAxisView(), &OvrTMLAxisView::onScaleChange, [this](bool) {
		viewport()->update();
	});
	connect(OvrPlayerListener::getInst(), &OvrPlayerListener::onPlayPosChanged, this, &OvrTimeLineTree::onPlayPosChanged, Qt::QueuedConnection);
}

void OvrTimeLineTree::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{}

void OvrTimeLineTree::initTreeStyle()
{
	setObjectName("OvrTimeLineTree");
	viewport()->setObjectName("OvrTimeLineTreeViewPort");

	setUniformRowHeights(true);
	setMouseTracking(true);
	setAcceptDrops(true);

	QByteArray open_node = getImagePath("icon_listclose_min.png").toLocal8Bit();
	QByteArray close_node = getImagePath("icon_listopen_min.png").toLocal8Bit();

	QString style_sheet = QString::asprintf(
		"QTreeView::branch:closed:has-children:!has-siblings,QTreeView::branch:closed:has-children:has-siblings {border-image:none;image:url(%s);}"
		"QTreeView::branch:open:has-children:!has-siblings,QTreeView::branch:open:has-children:has-siblings {border-image:none;image:url(%s);}"
		"QTreeView {border: 0px}"
		"QTreeView::item:hover {background-color: rgb(40, 40, 40);}"
		"QTreeView::item {border-bottom:1px solid rgba(151,151,151, 38); background-clip: border;}"
		"QTreeView::item:focus {selection-background-color: rgb(212, 212, 212);}"
		"QTreeView::item:selected {background-color: rgb(43, 43, 43);}"
		"QTreeView {background:rgb(25, 25, 25)}",
		close_node.constData(),
		open_node.constData()
	);

	setStyleSheet(style_sheet);

	QPalette pa = palette();
	pa.setColor(QPalette::Base, QColor("#252525"));
	pa.setColor(QPalette::AlternateBase, QColor("#222222"));
	setPalette(pa);
	setAlternatingRowColors(true);
}

OvrTimeLineTree::~OvrTimeLineTree()
{
}

void OvrTimeLineTree::resizeEvent(QResizeEvent *event)
{
	if (ovr_tml_v_line != nullptr)
	{
		ovr_tml_v_line->resize(1, height() - tml_v_line_y_offset);
	}
	QTreeView::resizeEvent(event);
}

void OvrTimeLineTree::onPlayPosChanged(ovr::uint64)
{
	if (isVisible())
	{
		updateItemIfNeeded();
	}
}

void OvrTimeLineTree::updateItemIfNeeded()
{
	auto& items = OvrTimeLineTreeItem::getItemsUpdateWithTime();
	for(auto it = items.begin(); it != items.end(); ++it)
	{
		OvrTimeLineTreeItem* item = it.key();
		int value = it.value();
		updateItemIfNeeded(item, value);
	}
}

void OvrTimeLineTree::updateItemIfNeeded(OvrTimeLineTreeItem* a_item, int a_value)
{
	QList<OvrTimeLineTreeItem*> parents;
	int count = a_item->getParents(parents);

	//获取item的index
	QModelIndex index;
	for (int i = 0; i < count; ++i)
	{
		OvrTimeLineTreeItem* item = parents[i];
		QModelIndex item_index = model()->index(item->row(), 0, index);
		if (item == a_item)
		{
			index = item_index;
			break;
		}
		else if (!isExpanded(item_index))
		{
			return;
		}
		index = item_index;
	}
	if (!index.isValid()) 
	{
		return;
	}

	//更新头
	if ((a_value & kUpdateItemHead) > 0)
	{
		update(index);
	}
	//更新数据列
	if ((a_value & kUpdateItemValue) > 0)
	{
		index = index.sibling(index.row(), 2);
		update(index);
	}
}

bool OvrTimeLineTree::mousePressTreeLeft(QMouseEvent *event, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index)
{
	OvrTMLKeyButtonArea area;
	static_cast<OvrTreeItemDelegate*>(itemDelegate())->calcKeyButtonArea(visualRect(a_index), a_item, area);

	if (area.rcAddTrackButton.contains(event->pos()))
	{
		initAddTrackMenu(a_item);
		QPoint pt = area.rcAddTrackButton.bottomRight();
		pt.setX(pt.x() + 5);
		add_track_menu->move(mapToGlobal(pt));
		add_track_menu->show();
	}
	//打点: 添加关键帧
	else if (area.rcAddKeyFrameButton.contains(event->pos()))
	{
		int frame = round(getGlobalAxisView()->getCurFrame());

		/*int track_type = a_item->getTrack()->GetTrackType();
		if (track_type == ovr_core::kTrackEvent || track_type == ovr_core::kTrackInteractive)
		{
			QTreeItemMenu::showRightMenu(mapToGlobal(event->pos()), a_item, frame, true, a_index);
		}
		else
		{
			QModelIndex to_be_updated;
			if (OvrTimeLineTreeModel::getInst()->addKeyFrame(frame, a_item, a_index, to_be_updated))
			{
				update(to_be_updated);
				if (track_type == ovr_core::kTrackFloat || track_type == ovr_core::kTrackGroup)
				{
					updateItemIfNeeded();
				}
			}
		}*/
	}
	else if (area.rcConfigButton.contains(event->pos()))
	{
		/*switch (a_item->getTrack()->GetTrackType())
		{
		default:
			break;
		}*/
	}
	return false;
}

void OvrTimeLineTree::onAddTrackMenuSelected(QAction* a_action)
{
	QModelIndex index = currentIndex();
	assert(index.isValid() && index.column() == kTMLSectionHeadIndex);

	OVRActorItem* actor_item = static_cast<OVRActorItem*>(index.internalPointer());
	assert(actor_item != nullptr);

	qDebug() << a_action->text() << " track will be added";
	assert(a_action->objectName() == "add_child_track");

	if (!isExpanded(index))
	{
		setExpanded(index, true);
	}

	int track_type = a_action->property("main_type").toInt();
	QString sub_type = a_action->property("sub_type").toString();
	QModelIndex added_index = OvrTimeLineTreeModel::getInst()->addChildTrack(index, track_type, sub_type);
	if (added_index.isValid())
	{
		if (added_index != index)
		{
			if (!isExpanded(added_index))
			{
				setExpanded(added_index, true);
			}
		}
	}
}

void OvrTimeLineTree::initAddTrackMenu(OvrTimeLineTreeItem* a_item)
{
	if (add_track_menu == nullptr)
	{
		add_track_menu = new OVRTMLAddTrackMenu(this);
		connect(add_track_menu, &QMenu::triggered, this, &OvrTimeLineTree::onAddTrackMenuSelected);
	}
	add_track_menu->initMenu(a_item);
}

void OvrTimeLineTree::mousePressTreeRight(QMouseEvent *event, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index)
{	
	drag_obj->beginDrag(a_index, a_item, event->globalX());
}

void OvrTimeLineTree::contextMenuEvent(QContextMenuEvent *event)
{
	const QModelIndex& index = indexAt(event->pos());
	if (index.isValid())
	{
		OvrTimeLineTreeItem* item = static_cast<OvrTimeLineTreeItem*>(index.internalPointer());

		if (index.column() == kTMLSectionValueIndex)
		{
			if (OvrTimeLineTreeModel::getInst()->isPause())
			{
				if (drag_obj->beginDrag(index, item, event->globalX(), true))
				{
					is_right_click = true;
					QTreeItemMenu::showRightMenu(event->globalPos(), item, drag_obj->getSelectKey(), false, index);
				}
			}
		}
		else if (index.column() == kTMLSectionHeadIndex)
		{
			if (OvrTimeLineTreeModel::getInst()->isPause())
			{
				
				QTreeItemMenu::showLeftMenu(event->globalPos(), item, index);
			}
		}
	}
}

void OvrTimeLineTree::paintEvent(QPaintEvent *event)
{
	QTreeView::paintEvent(event);

	QPainter painter(viewport());
	int x = header()->sectionPosition(kTMLSectionLineIndex);
	painter.setPen(QColor(5, 5, 5));
	painter.drawLine(x, 0, x, viewport()->height());
}

void OvrTimeLineTree::locateItem(QModelIndex a_index)
{
	if (a_index.isValid())
	{
		if (a_index.column() != 0)
		{
			a_index = a_index.sibling(a_index.row(), 0);
		}

		//collapseAll();
		auto item = static_cast<OvrTimeLineTreeItem*>(a_index.internalPointer());
		auto parent_index = a_index.parent();
		while (true)
		{
			if (parent_index.isValid() && !isExpanded(parent_index))
			{
				setExpanded(parent_index, true);
				parent_index = parent_index.parent();
				continue;
			}
			break;
		}

		scrollTo(a_index);

		if (a_index != currentIndex())
		{
			setCurrentIndex(a_index);
		}
	}
}

void OvrTimeLineTree::removeSelecttion()
{
	drag_obj->removeSelecttion();
}

void OvrTimeLineTree::renameSelecttion()
{
	is_right_click = true;
	drag_obj->renameSelecttion();
}

void OvrTimeLineTree::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_X)
	{
		drag_obj->split();
	}
	else if (event->key() == Qt::Key_Delete)
	{
		drag_obj->removeSelecttion();
	}
	QTreeView::keyPressEvent(event);
}

void OvrTimeLineTree::focusOutEvent(QFocusEvent *event)
{
	if (is_right_click)
	{
		is_right_click = false;
		QTreeView::focusOutEvent(event);
		return;
	}
	drag_obj->clickNothing();	
	QTreeView::focusOutEvent(event);
}

void OvrTimeLineTree::mousePressEvent(QMouseEvent *event)
{
	QTreeView::mousePressEvent(event);

	if (event->button() != Qt::LeftButton)
	{
		return;
	}

	const QModelIndex& index = indexAt(event->pos());
	if (index.isValid())
	{
		OvrTimeLineTreeItem* item = static_cast<OvrTimeLineTreeItem*>(index.internalPointer());

		if (index.column() == kTMLSectionHeadIndex)
		{
			mousePressTreeLeft(event, item, index);
		}
		else if (index.column() == kTMLSectionValueIndex)
		{
			mousePressTreeRight(event, item, index);
		}
	}
	else
	{
		//在右边区域单击吗?
		int x_start =  header()->sectionPosition(kTMLSectionValueIndex);
		QRect rc = viewport()->rect();
		rc.moveLeft(x_start);
		if (rc.contains(event->pos()))
		{
			drag_obj->clickNothing();
		}
	}
}

void OvrTimeLineTree::mouseDoubleClickEvent(QMouseEvent *event)
{
	const QModelIndex& index = indexAt(event->pos());
	if (index.isValid())
	{
		OvrTimeLineTreeItem* item = static_cast<OvrTimeLineTreeItem*>(index.internalPointer());
		if (index.column() == kTMLSectionValueIndex)
		{

			if (drag_obj->beginDrag(index, item, event->globalX(), true))
			{
				int frame = drag_obj->getSelectKey();
				int track_type = item->getTrack()->GetTrackType();
				/*if (track_type == ovr_core::kTrackEvent)
				{
					configEvent(item, frame);
				}
				else if (track_type == ovr_core::kTrackInteractive)
				{
					configInterActive(item, frame);
				}*/
			}
		}
	}
	QTreeView::mouseDoubleClickEvent(event);
}

void OvrTimeLineTree::currentChanged(const QModelIndex &current, const QModelIndex &previous)
{
	if (current.isValid())
	{
		auto item = static_cast<OvrTimeLineTreeItem*>(current.internalPointer());
	}

	QTreeView::currentChanged(current, previous);
}

void OvrTimeLineTree::configInterActive(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame)
{
	ovr_core::OvrClipInfo* clip_info = static_cast<OvrTMLClipItem*>(a_item)->getClipTrack()->Clip(a_frame);
	if (clip_info != nullptr)
	{
		ovr_core::OvrInterActiveTrack* track = static_cast<ovr_core::OvrInterActiveTrack*>(a_item->getTrack());
		OvrObjEditorClusterManager* obj_manager = CreateObjClusterManager(track);
		
		if (obj_manager->loadCluster(clip_info->file_path.GetStringConst()))
		{
			OvrObjEditorWnd* editor = OvrObjEditorWnd::createInst();
			editor->init(obj_manager);
			editor->show();
		}
		else
		{
			delete obj_manager;
		}
	}
}

void OvrTimeLineTree::configEvent(OvrTimeLineTreeItem* a_item, ovr::uint64 a_frame)
{
	ovr_core::OvrEventTrack* track = static_cast<ovr_core::OvrEventTrack*>(a_item->getTrack());
	OvrString event_name = track->GetKeyFrame(a_frame);
	assert(event_name.GetStringSize() > 0);
	qDebug() << "config event for " << QString::fromLocal8Bit(event_name.GetStringConst());

	OvrObjEditorClusterManager* obj_manager = CreateObjClusterManager(track);
	if (obj_manager->loadCluster(event_name.GetStringConst()))
	{
		OvrObjEditorWnd* editor = OvrObjEditorWnd::createInst();
		editor->init(obj_manager);
		editor->show();
	}
	else
	{
		delete obj_manager;
	}
}

void OvrTimeLineTree::mouseMoveEvent(QMouseEvent *event)
{
	QTreeView::mouseMoveEvent(event);

	if (drag_obj->isDragingBegin())
	{
		QModelIndex item_under_mouse = indexAt(event->pos());
		drag_obj->draging(event->globalX(), event, item_under_mouse);
	}
	else
	{
		const QModelIndex& index = indexAt(event->pos());
		if (index.isValid())
		{
			OvrTimeLineTreeItem* item = static_cast<OvrTimeLineTreeItem*>(index.internalPointer());
			if (index.column() == kTMLSectionHeadIndex)
			{
				mouseMoveTreeLeft(event, item, index);
			}
			else if (index.column() == kTMLSectionValueIndex)
			{
				mouseMoveTreeRight(event, item, index);
				return;
			}
		}
		resetResizeCursor();
	}
}

bool OvrTimeLineTree::mouseMoveTreeLeft(QMouseEvent *event, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index)
{
	static QRect last_invalid_rect;
	QRect current_invalid_rect;
	
	OvrTMLKeyButtonArea area;
	static_cast<OvrTreeItemDelegate*>(itemDelegate())->calcKeyButtonArea(visualRect(a_index), a_item, area);

	if (area.rcAddTrackButton.contains(event->pos()))
	{
		//current_invalid_rect = area.rcAddTrackButton;
	}
	else if (area.rcAddKeyFrameButton.contains(event->pos()))
	{
		current_invalid_rect = area.rcAddKeyFrameButton;
	}
	else if (area.rcConfigButton.contains(event->pos()))
	{
		//current_invalid_rect = area.rcConfigButton;
	}
	else if (area.rcVisibleButton.contains(event->pos()))
	{
		//current_invalid_rect = area.rcVisibleButton;
	}

	if (!current_invalid_rect.isEmpty())
	{
		if (current_invalid_rect != last_invalid_rect)
		{
			viewport()->update(current_invalid_rect);
			last_invalid_rect = current_invalid_rect;
		}
	}
	else if (!last_invalid_rect.isEmpty())
	{
		viewport()->update(last_invalid_rect);
		last_invalid_rect.setSize(QSize(0, 0));
	}
	return false;
}

bool OvrTimeLineTree::mouseMoveTreeRight(QMouseEvent *event, OvrTimeLineTreeItem* a_item, const QModelIndex& a_index)
{
	if (a_item->isResizeable())
	{
		if (a_item->hitTest(event->globalX(), true))
		{
			if (!resize_cursor_set_for_clip)
			{
				setCursor(QCursor(Qt::SizeHorCursor));
				resize_cursor_set_for_clip = true;
			}
			return true;
		}
	}
	resetResizeCursor();
	return false;
}

bool OvrTimeLineTree::resetResizeCursor()
{
	if (resize_cursor_set_for_clip)
	{
		resize_cursor_set_for_clip = false;
		unsetCursor();
		return true;
	}
	return false;
}

bool OvrTimeLineTree::viewportEvent(QEvent *event)
{
	if (event->type() == QEvent::Leave)
	{
		resetResizeCursor();
	}
	return QTreeView::viewportEvent(event);
}

void OvrTimeLineTree::mouseReleaseEvent(QMouseEvent *event)
{
	drag_obj->endDrag(event);

	QTreeView::mouseReleaseEvent(event);
}

bool OvrTimeLineTree::canBeDroped(const QPoint& a_ptGlobal, const QByteArray& a_file_path)
{
	QRect rc = rect();
	rc.moveTopLeft(mapToGlobal(rc.topLeft()));
	if (rc.contains(a_ptGlobal))
	{
		const QModelIndex& index = indexAt(viewport()->mapFromGlobal(a_ptGlobal));
		if (index.isValid() && index.column() == kTMLSectionValueIndex)
		{
			OvrTimeLineTreeItem* item = static_cast<OvrTimeLineTreeItem*>(index.internalPointer());
			if (item->isClipDataItem())
			{
				return static_cast<OvrTMLClipItem*>(item)->getClipTrack()->IsSupportDragIn(a_file_path.constData());
			}
		}
	}
	return false;
}

void OvrTimeLineTree::dragEnterEvent(QDragEnterEvent *event)
{
	QVariant item_type = event->mimeData()->property("drag_type");
	QVariant item_value = event->mimeData()->property("drag_value");
	if (item_type.isValid() && item_value.isValid())
	{
		if (item_type.toString() == "asset_drag")
		{
			QString asset_path = item_value.toString();
			if (OvrTimeLineTreeItem::HasItemAcceptAsset(asset_path))
			{
				event->accept();
				return;
			}
		}
	}
	event->ignore();
}

void OvrTimeLineTree::dragMoveEvent(QDragMoveEvent *event)
{
	QModelIndex item_index = indexAt(event->pos());
	if (item_index.isValid() && item_index.column() == 2)
	{
		auto item = static_cast<OvrTimeLineTreeItem*>(item_index.internalPointer());
		if (item->isClipDataItem())
		{
			auto clip_item = static_cast<OvrTMLClipItem*>(item);
			auto asset_path = event->mimeData()->property("drag_value").toString();
			if (clip_item->getClipTrack()->IsSupportDragIn(asset_path.toLocal8Bit().constData()))
			{
				event->accept();
				return;
			}
		}
	}
	event->ignore();
}

void OvrTimeLineTree::dropEvent(QDropEvent *event)
{
	QModelIndex item_index = indexAt(event->pos());
	auto asset_path = event->mimeData()->property("drag_value").toString();
	if (OvrTimeLineTreeModel::getInst()->addClipFile(item_index, asset_path.toLocal8Bit(), mapToGlobal(event->pos()).x()))
	{
		update(item_index);
		event->accept();
		return;
	}
	event->ignore();
}

void OvrTimeLineTree::dragLeaveEvent(QDragLeaveEvent *event)
{
	event->accept();
}

////////////////////////////////////////////////
// class OVRTMLAddTrackMenu

OVRTMLAddTrackMenu::OVRTMLAddTrackMenu(QWidget *parent) :QMenu(parent)
{
	QString style = "QMenu {background:rgb(37,37,38);color:white}"
		"\nQMenu::item:selected {background: rgba(100, 100, 100, 255)}"
		"\nQMenu::item:disabled {color:gray}";
	setStyleSheet(style);//144, 200, 246
}

template<class T>
bool CanbeTrackFace(void* a_actor_track)
{
	ovr_project::OvrActorTrack* actor_track = static_cast<ovr_project::OvrActorTrack*>(a_actor_track);
	T* mesh_actor = static_cast<T*>(actor_track->GetActor());
	ovr_engine::OvrMesh* mesh = mesh_actor->GetMesh();
	bool ret = mesh->HasPose();
	bool ret1 = mesh->GetPose(0) != nullptr;
	return ret || ret1;
}

void OVRTMLAddTrackMenu::initMenu(OvrTimeLineTreeItem* a_actor_item)
{
	clear();
	ovr_core::OvrActorTrack* actor_track = static_cast<ovr_core::OvrActorTrack*>(a_actor_item->getTrack());
	
	QAction* action = nullptr;
	for (auto track_support : actor_track->GetSupportTrackList())
	{
		action = addAction(QString::fromLocal8Bit(track_support.name.GetStringConst()));
		action->setObjectName("add_child_track");
		action->setProperty("main_type", track_support.track_type);
		action->setProperty("sub_type", QString::fromLocal8Bit(track_support.tag.GetStringConst()));
		action->setEnabled(track_support.is_enable);
	}
}