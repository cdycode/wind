#include "Header.h"
#include "ovr_tml_track_tree.h"

#include "ovr_tml_item.h"

OvrTMLTrackTree::OvrTMLTrackTree(QWidget *parent):QTreeWidget(parent)
{
	initStyle();

	QStringList e = { "onAddMainTrack", "onAddActorTrack", "OnRmvActor" };

	registerEventListener(e);
}

void OvrTMLTrackTree::initStyle()
{
	auto open_node = getImagePath("icon_listclose_min.png");
	auto close_node = getImagePath("icon_listopen_min.png");

	QString style_sheet = QString::asprintf(
		"QTreeView::branch:closed:has-children:!has-siblings,QTreeView::branch:closed:has-children:has-siblings {border-image:none;image:url(%s);}"
		"QTreeView::branch:open:has-children:!has-siblings,QTreeView::branch:open:has-children:has-siblings {border-image:none;image:url(%s);}"
		"QTreeView {border: 0px}"
		"QTreeView::item:hover {background-color: rgb(40, 40, 40);}"
		"QTreeView::item {border-bottom:1px solid rgba(151,151,151, 38); background-clip: border;}"
		"QTreeView::item:focus {selection-background-color: rgb(212, 212, 212);}"
		"QTreeView::item:selected {background-color: rgb(43, 43, 43);}"
		"QTreeView {background:rgb(25, 25, 25)}",
		close_node.toLocal8Bit().constData(),	open_node.toLocal8Bit().constData());
	setStyleSheet(style_sheet);

	QPalette pa = palette();
	pa.setColor(QPalette::Base, QColor("#252525"));
	pa.setColor(QPalette::AlternateBase, QColor("#222222"));
	setPalette(pa);
	setAlternatingRowColors(true);
}

void OvrTMLTrackTree::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{
	if (a_event_name == "onAddMainTrack")
	{
		OvrTMLItem::createMainItem(a_param1.toInt(), a_param2.toString(), this);
	}
	else if (a_event_name == "onAddActorTrack")
	{
		auto item = OvrTMLItem::createActorItem(a_param1.toString(), this);
		if (item != nullptr)
		{
			actor_items[a_param1.toString()] = item;
		}
	}
	else if (a_event_name == "OnRmvActor")
	{
		auto actor_id = a_param1.toString();
		if (actor_items.contains(actor_id))
		{
			actor_items[actor_id]->remove();
			actor_items.remove(actor_id);
		}
	}
}