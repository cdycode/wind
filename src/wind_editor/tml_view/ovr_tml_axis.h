#pragma once

#include <QWidget>
#include <QScrollArea>

struct TMLUnitLineInfo
{
	enum LineSize {
		kLSBase = 4, kLSLittle = 6, kLSSmall = 8, kLSMid = 12, kLSBig = 16
	};

	int big_unit, mid_unit, small_unit, little_unit;

	int unit_value_strig_width = 0;
	const int unit_value_string_margin = 30;

	bool is_draw_mid_line_value, is_draw_small_line_value, is_draw_little_line_value, is_draw_unit_line_value;

	void setUnitLineCount(int a_big_unit, int a_mid_unit, int a_small_unit, int a_little_unit);
	void initUnitDrawInfo(float a_pixel_per_unit);
	int  calcUnitLineInfo(int a_unit, bool& is_draw_value);
	int  getHideUnitValueCount();
};

class OvrTMLAxisCursor;
class OvrTMLCursorLine;
class OvrTMLAxisCursorValue;

//时间轴用于显示时间刻度的view
class OvrTMLAxisView :public QWidget, public OvrEventReceiver
{
	Q_OBJECT
public:
	explicit OvrTMLAxisView(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());

	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;
	virtual QString name() override { return "OvrTimeLineAxisView"; }

	OvrTMLCursorLine* createCursorLine(QWidget* a_parent);
	OvrTMLAxisCursorValue* createCursorValue(QWidget* a_parent);

	bool makeSureVisible(int a_frame_head = 0, int a_frame_end = -1);
	bool scaleTMLLine(int a_center_dpx, bool a_make_small);

	QString getTMLModeString();
	QString getTMLModeStringEx();

	float getWndPortX() { return tml_wnd_port; }
	float getValuePerPixel();

	float getLPXForFrame(float a_frame);
	float getDPXForFrame(float a_frame);
	float getGlobalDPXForFrame(float a_frame);
	float getLPXForDPX(float a_x);

	float getFrameForLPX(float a_x);
	float getFrameForDPX(float a_x);
	float getFrameForGlobalDPX(float a_x);
	float getCurFrame() { return cur_frame; }

	int	  getFrameTotal();
	int	  getFPS();

	void setRange(int a_secs, int a_fps);
	void getRange(int& a_secs, int& a_fps);

	int getTMLXZeroMargin() { return tml_x_zero_margin; }
	void initAlignPainer(QPainter& painter, QWidget* a_taget, int a_y = 0, bool a_is_reverse_y = false);

	bool changeWndPort(float a_wnd_port);


signals:
	void onTMLModeChange(QString a_mode);
	void onWndPortChange();
	void onScaleChange(bool make_small);
	void mouseClick();

public slots:
	void dragPress(int a_x, int a_y);
	void dragRelease(int a_x, int a_y);
	void dragMove(int a_x, int a_y);
	void seekToDPX(int a_x, bool can_be_round = false);
	void seekToFrame(float a_frame, bool can_be_round = false);
	void onPlayPosChanged(ovr::uint64 a_frame_index);

protected:
	virtual void paintEvent(QPaintEvent *e) override;
	virtual void showEvent(QShowEvent *event) override;
	virtual void resizeEvent(QResizeEvent *event) override;

	virtual void mousePressEvent(QMouseEvent *event) override;
	virtual void mouseMoveEvent(QMouseEvent *event) override;
	virtual void mouseReleaseEvent(QMouseEvent *event) override;

	virtual void wheelEvent(QWheelEvent *event) override;

	void initUnitDrawInfo();

	bool adjustAxisView(float a_axis_space, int a_frame_size = -1);
	int  nextMode(int a_mode);
	bool initUnitLineCount();
	void drawUnit(QPainter& painter);

	bool moveTMLLine(float a_wnd_port);
	void updateCursor();

	float getFrameNumber(float a_unit_num);

	float getUnitNumber(float a_frame, int a_tml_mode = 0);
	void  drawUnitValue(QPainter& painter, int a_frame_num, int x, int y);
	QString getFrameValueString(int a_frame_num, QList<int>* a_values = nullptr);

	bool setTMLMode(int a_mode);

	bool  changeCurFrame(float a_frame);

	void resetScaleCenter() { scale_center_dpx = -1; }
	int  getValueStringKeyIndex();

private:
	enum TMLMode {
		kTMLModeInvalid, 
		kTMLFrame, kTMLFrame_5, kTMLFrame_10,
		kTMLSecs, kTMLSecs_5, kTMLSecs_10, kTMLSecs_30,
		kTMLMin, kTMLMin_5, kTMLMin_10, kTMLMin_30,
		kTMLHour, kTMLModeMax
	} tml_mode = kTMLModeInvalid;

	bool is_mouse_pressed = false;
	float drag_x = 0;
	bool is_mouse_moved = false;

	float scale_center_dpx = -1;
	float scale_center_frame = -1;
	const int kMaxFrameUnitSpace = 24;

	float tml_wnd_port = 0;
	float tml_max_view_space = 0;
	float tml_cur_view_space = 0;
	float drag_tml_wnd_port = 0;
	QTransform cur_trans;

	float pixel_per_unit = 0;
	TMLUnitLineInfo tml_unit_line_info;

	int cursor_pos = 0;
	float cur_frame = 0;
	bool is_update_cursor = true;
	const int tml_x_zero_margin = 12;

	static int   share_total_secs;
	static int   share_total_frames;
	static int   share_fps;
	static float share_cur_frame;
	static float share_tml_wnd_port;
	static float share_pixel_per_unit;
	static TMLMode share_tml_mode;

	OvrTMLAxisCursor* cursor_wnd = nullptr;
	OvrTMLAxisCursorValue* cursor_value = nullptr;
	OvrTMLCursorLine* cursor_line = nullptr;
};

//时间轴
class OvrTMLAxis :public QWidget
{
	Q_OBJECT
public:
	explicit OvrTMLAxis(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());

	OvrTMLAxisView* getAxisView() { return axis_view; }

protected:
	void paintEvent(QPaintEvent *e) override;
	OvrTMLAxisView* axis_view = nullptr;
};

class OvrTMLAxisCursor :public QLabel
{
	Q_OBJECT
public:
	explicit OvrTMLAxisCursor(QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	void updatePos(int a_x, bool a_hide);

signals:
	void cursorDragedTo(int a_x, bool canbe_round = false);
	void timeLineScale(QPoint pt, int delta);

protected:
	virtual void mousePressEvent(QMouseEvent *e) override;
	virtual void mouseReleaseEvent(QMouseEvent *e) override;
	virtual void mouseMoveEvent(QMouseEvent *e) override;
	virtual void wheelEvent(QWheelEvent * event)override;

	int x_press_center_offset;
	bool is_draging = false;
};

class OvrTMLAxisCursorValue :public QLabel
{
	Q_OBJECT
public:
	explicit OvrTMLAxisCursorValue(QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());

	void updatePos(int a_global_x, const QString& a_value, bool a_hide);
	void setPos(bool a_is_top) { is_top = a_is_top; }

protected:
	bool is_top = true;
};

class OvrTMLCursorLine : public QWidget
{
	Q_OBJECT
public:
	explicit OvrTMLCursorLine(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());

	void updatePos(int a_global_x, int a_global_y, bool a_hide);

protected:
	virtual void paintEvent(QPaintEvent *event) override;
	virtual void wheelEvent(QWheelEvent *event) override;
};

void setGlobalAxisX(OvrTMLAxis* a_axis);
OvrTMLAxisView* getGlobalAxisView();
OvrTMLAxis* getGlobalAxisX();
