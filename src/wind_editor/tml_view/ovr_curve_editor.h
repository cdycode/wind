#pragma once

#include <QWidget>
#include <QMenu>
#include <QPersistentModelIndex>

class OvrTMLAxis;
class OvrTMLAxisView;
class OvrTimeLineCurveView;
class OvrTimeLineTreeCurve;

class OvrCurveEditor :public QWidget
{
public:
	OvrCurveEditor(QWidget *parent = Q_NULLPTR);

	OvrTimeLineCurveView* getCurveView() { return curve_view; }
	OvrTimeLineTreeCurve* getCurveTree() { return curve_tree; }
	OvrTMLAxisView* getAxisView();
protected:
	OvrTimeLineCurveView* curve_view;
	OvrTimeLineTreeCurve* curve_tree;
	OvrTMLAxis* time_line_axis;
};