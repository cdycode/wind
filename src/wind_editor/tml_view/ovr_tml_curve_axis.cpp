#include "Header.h"
#include "ovr_tml_curve_axis.h"

#include "ovr_tml_axis.h"

#define kGrayLineColor		 "#80494949"
#define kLightGrayLineColor  "#EE494949"
#define kZeroLineColor		 "#EE595959"
#define kBlackLineColor		 "#A0090909"
#define kValueTextColor		 "#696969"

#define kMinUnitSpace  40
#define kSmallYLineLen 2

#define kYMargin 10

OvrTMLCurveAxis::OvrTMLCurveAxis(QWidget *parent, Qt::WindowFlags f) :QWidget(parent, f)
{
	setStyleSheet("color:red;background:#232323");

	connect(getGlobalAxisView(), &OvrTMLAxisView::onWndPortChange, [this]() {
		update();
	});

	connect(getGlobalAxisView(), &OvrTMLAxisView::onScaleChange, [this](bool) {
		update();
	});

	QStringList e = { "onCurveEditor" };

	registerEventListener(e);
}

void OvrTMLCurveAxis::onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2)
{
	if (a_event_name == "onCurveEditor")
	{
		auto cmd = a_param1.toString();
		if (cmd == "ce_fit_w")
		{
			if (fitWidth())
			{
				update();
			}
		}
		else if (cmd == "ce_fit_h")
		{
			if (fitHeight())
			{
				update();
			}
		}
	}
}

void OvrTMLCurveAxis::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::RightButton)
	{
		is_right_pressed = true;
		pt_right_drag = event->pos();
		right_drag_wnd_port_y = wnd_port_y;
		right_drag_wnd_port_x = getGlobalAxisView()->getWndPortX();
	}
}

void OvrTMLCurveAxis::mouseMoveEvent(QMouseEvent *event)
{
	if (is_right_pressed)
	{
		int new_wnd_port_y = event->y() - pt_right_drag.y() + right_drag_wnd_port_y;
		int new_wnd_port_x = pt_right_drag.x() - event->x() + right_drag_wnd_port_x;

		if (getGlobalAxisView()->changeWndPort(new_wnd_port_x))
		{
			getGlobalAxisView()->update();
		}
		changeWndPort(new_wnd_port_y);
	}
}

void OvrTMLCurveAxis::mouseReleaseEvent(QMouseEvent *event)
{
	if (is_right_pressed)
	{
		is_right_pressed = false;
	}
}

void OvrTMLCurveAxis::wheelEvent(QWheelEvent *event)
{
	QPoint pt = getGlobalAxisView()->mapFromGlobal(event->globalPos());
	getGlobalAxisView()->scaleTMLLine(pt.x(), event->delta() < 0);
}

bool OvrTMLCurveAxis::changeWndPort(int a_wnd_port_y)
{
	if (a_wnd_port_y != wnd_port_y)
	{
		wnd_port_y = a_wnd_port_y;
		update();
		return true;
	}

	return false;
}

void OvrTMLCurveAxis::calcUnitBase(float& a_unit_base_space, float& a_unit_base_value, float& a_unit_num_to_draw)
{
	//计算最小单位刻度的值
	a_unit_base_value = 0.001f;
	float value_space1_per_10 = getYRanges() / 10;
	while (value_space1_per_10 > a_unit_base_value)
	{
		a_unit_base_value *= 10;
	}

	//计算最小单位刻度值对应的像素位置
	a_unit_base_space = getLPYForValue(a_unit_base_value);

	//单位刻度占据的像素空间最大不超过10倍kMinUnitSpace
	while (a_unit_base_space > 10 * kMinUnitSpace)
	{
		a_unit_base_space /= 10;
		a_unit_base_value /= 10;
	}

	assert(a_unit_base_space > 0);
	a_unit_num_to_draw = kMinUnitSpace / a_unit_base_space;
}

void OvrTMLCurveAxis::paintEvent(QPaintEvent* e)
{
	ApplyWidgetStyleSheet(this);

	QPainter painter(this);
	if (is_auto_center)
	{
		is_auto_center = false;
		wnd_port_y = height() / 2;
	}

	getGlobalAxisView()->initAlignPainer(painter, this, wnd_port_y, true);
	cur_trans = painter.combinedTransform();

	drawAxisY(painter);

	onDrawContext(painter);
}

void OvrTMLCurveAxis::drawAxisY(QPainter& painter)
{
	float unit_base_space, unit_base_value, unit_num_to_draw;
	calcUnitBase(unit_base_space, unit_base_value, unit_num_to_draw);

	int int_unit_num_to_draw = unit_num_to_draw;
	if (!Float_Equal(unit_num_to_draw, int_unit_num_to_draw))
	{
		++int_unit_num_to_draw;
	}

	//从上到下绘制刻度线以及刻度值
	int unit_num = (int)(wnd_port_y / unit_base_space) + 1;
	float y_head = unit_num * unit_base_space;
	float y_end = wnd_port_y - height();

	for (float y = y_head; y > y_end; y -= unit_base_space)
	{
		const char* color = unit_num % 2 == 0 ? kGrayLineColor : kBlackLineColor;
		if (unit_num % int_unit_num_to_draw == 0)
		{
			static char value_str[30] = { 0 };
			Float_Equal(unit_base_value, 0.001f) ? sprintf(value_str, "%.3f", unit_num * unit_base_value) : sprintf(value_str, "%.2f", unit_num * unit_base_value);
			QString format_value_str = value_str;
			if (unit_num >= 0)
			{
				format_value_str = " " + format_value_str;
			}
			if (Float_Equal(unit_num, 0))
			{
				painter.setPen(QColor(kZeroLineColor));
			}
			drawAxisYUnit(painter, format_value_str, y);	

			painter.setPen(QColor(color));
			drawAxisYSmallUnit(painter, y, unit_base_space / 10, 1);
		
		}
		else
		{
			painter.setPen(QColor(color));
			drawAxisYSmallUnit(painter, y, unit_base_space / 10, 0);
		}

		--unit_num;
	}
}

void OvrTMLCurveAxis::drawAxisYUnit(QPainter& painter, const QString& a_value, float a_y)
{
	//计算刻度线所在的设备坐标
	float a_dy = cur_trans.map(QPointF(0, a_y)).y();

	//计算刻度值所在的设备坐标
	QFontMetrics fm(painter.font());
	QSize str_size = fm.size(Qt::TextSingleLine, a_value);

	QRect rc(QPoint(0, a_y - str_size.height() / 2), str_size);
	rc = cur_trans.mapRect(rc);

	painter.save();
	painter.resetTransform();

	painter.drawLine(QPointF(0, a_dy), QPointF(width(), a_dy));

	painter.setPen(QColor(kValueTextColor));

	rc.moveLeft(2);
	painter.drawText(rc, a_value, Qt::AlignVCenter | Qt::AlignLeft);

	rc.moveLeft(width() - str_size.width() - 2);
	painter.drawText(rc, a_value, Qt::AlignVCenter | Qt::AlignLeft);

	painter.restore();
}

void OvrTMLCurveAxis::drawAxisYSmallUnit(QPainter& painter, float a_y, float a_small_unit_space, int a_head_line)
{
	if (a_small_unit_space >= 2)
	{
		a_y = cur_trans.map(QPoint(0.0f, a_y)).y();

		painter.save();
		painter.resetTransform();
		painter.setPen(QColor(kLightGrayLineColor));

		for (int i = a_head_line; i < 10; ++i)
		{
			int line_len = i == 0 ? kSmallYLineLen * 2 : kSmallYLineLen;
			float small_unit_y = a_y - i * a_small_unit_space;

			painter.drawLine(QPointF(0, small_unit_y), QPointF(line_len, small_unit_y));
			painter.drawLine(QPointF(width() - line_len, small_unit_y), QPointF(width(), small_unit_y));
		}
		painter.restore();
	}
}

void OvrTMLCurveAxis::getXRanges(int& a_min_frame, int& a_max_frame)
{
	a_min_frame = 0;
	a_max_frame = getGlobalAxisView()->getFrameTotal();
}

float OvrTMLCurveAxis::getYRanges(float* a_min, float* a_max)
{
	if (a_min != nullptr)
	{
		*a_min = 0;
	}

	if (a_max != nullptr)
	{
		*a_max = 1.0f;
	}

	return 1.0f;
}

float OvrTMLCurveAxis::getLPXForValue(float a_frame)
{
	return getGlobalAxisView()->getLPXForFrame(a_frame);
}

float OvrTMLCurveAxis::getValueForLPX(float a_lpx)
{
	return getGlobalAxisView()->getFrameForLPX(a_lpx);
}

float OvrTMLCurveAxis::getLPYForValue(float a_value, int a_ratio)
{
	return a_ratio * a_value / getGlobalAxisView()->getValuePerPixel();
}

float OvrTMLCurveAxis::getValueForLPY(float a_lpy)
{
	return a_lpy * getGlobalAxisView()->getValuePerPixel() / ratio;
}

OvrVector2 OvrTMLCurveAxis::getValueForLP(const OvrVector2& a_lpt)
{
	OvrVector2 a_value;

	a_value._x = getValueForLPX(a_lpt._x);
	a_value._y = getValueForLPY(a_lpt._y);

	return a_value;
}

OvrVector2 OvrTMLCurveAxis::getLPForValue(const OvrVector2& a_value)
{
	OvrVector2 lp;

	lp._x = getLPXForValue(a_value._x);
	lp._y = getLPYForValue(a_value._y);

	return lp;
}

bool OvrTMLCurveAxis::fitWidthHeight()
{
	bool flag = fitWidth();

	flag = fitHeight() || flag;

	return flag;
}

bool OvrTMLCurveAxis::fitWidth()
{
	int min_frame, max_frame;
	getXRanges(min_frame, max_frame);

	return getGlobalAxisView()->makeSureVisible(min_frame, max_frame);
}

bool OvrTMLCurveAxis::fitHeight()
{
	//计算纵向缩放比例
	float min_v, max_v;
	float value_space = getYRanges(&min_v, &max_v);
	int   context_y_space = height() - kYMargin * 2;

	float new_ratio = getGlobalAxisView()->getValuePerPixel() / (value_space / context_y_space);

	//计算偏移坐标以确保中间值在中间
	float mid_value = (max_v - min_v) / 2 + min_v;
	float y = getLPYForValue(mid_value, new_ratio);

	float new_wnd_port_y = y + height() / 2;

	if (!Float_Equal(new_wnd_port_y, wnd_port_y) || !Float_Equal(new_ratio, ratio))
	{
		wnd_port_y = new_wnd_port_y;
		ratio = new_ratio;
		return true;
	}

	return false;
}
