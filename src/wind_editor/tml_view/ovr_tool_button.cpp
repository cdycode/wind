#include "Header.h"
#include "ovr_tool_button.h"

OvrToolButton* OvrToolButton::setIcons(const QString& a_icon_name, const QString& a_check_icon_name, const QString& a_path)
{
	if (a_icon_name.length() > 0)
	{
		nor_icon = makeIcon(a_path, "icon_", a_icon_name, "_nor.png");
		hi_icon  = makeIcon(a_path, "icon_", a_icon_name, "_highlight.png");
		sel_icon = makeIcon(a_path, "icon_", a_icon_name, "_sel.png");
	}
	if (a_check_icon_name.length() > 0)
	{
		check_nor_icon = makeIcon(a_path, "icon_", a_check_icon_name, "_nor.png");
		check_hi_icon = makeIcon(a_path, "icon_", a_check_icon_name, "_highlight.png");
		check_sel_icon = makeIcon(a_path, "icon_", a_check_icon_name, "_sel.png");
	}

	if (nor_icon != nullptr)
	{
		setPixmap(*nor_icon);
	}

	return this;
}

QPixmap* OvrToolButton::makeIcon(const QString& a_path, const QString& a_head, const QString& a_name, const QString& a_tail)
{
	QString icon_path;
	QTextStream stream(&icon_path);
	stream << a_path << a_head << a_name << a_tail;

	QString file_path = getImagePath(icon_path.toLocal8Bit().constData());
	QFile file(file_path);
	if (file.exists())
	{
		return new QPixmap(file_path);
	}
	return nullptr;
}

void OvrToolButton::enterEvent(QEvent *e)
{
	if (isChecked())
	{
		if (check_hi_icon != nullptr)
		{
			setPixmap(*check_hi_icon);
		}
	}
	else if (!isChecked())
	{
		if (hi_icon != nullptr)
		{
			setPixmap(*hi_icon);
		}
	}

	QLabel::enterEvent(e);
}

void OvrToolButton::leaveEvent(QEvent *e)
{
	if (isChecked())
	{
		if (check_nor_icon != nullptr)
		{
			setPixmap(*check_nor_icon);
		}
	}
	else if (!isChecked())
	{
		if (nor_icon != nullptr)
		{
			setPixmap(*nor_icon);
		}
	}

	QLabel::leaveEvent(e);
}

void OvrToolButton::mousePressEvent(QMouseEvent *e)
{
	if (isChecked())
	{
		if (check_sel_icon != nullptr)
		{
			setPixmap(*check_sel_icon);
		}
	}
	else if (!isChecked())
	{
		if (sel_icon != nullptr)
		{
			setPixmap(*sel_icon);
		}
	}
	QLabel::mousePressEvent(e);
}

void OvrToolButton::mouseReleaseEvent(QMouseEvent *e)
{
	if (rect().contains(e->pos()))
	{
		emit clicked(this);
	}

	if (isChecked())
	{
		if (check_hi_icon != nullptr)
		{
			setPixmap(*check_hi_icon);
		}
		else if (check_nor_icon != nullptr)
		{
			setPixmap(*check_nor_icon);
		}
	}
	else if (!isChecked())
	{
		if (hi_icon != nullptr)
		{
			setPixmap(*hi_icon);
		}
		else if (nor_icon != nullptr)
		{
			setPixmap(*nor_icon);
		}
	}
	QLabel::mouseReleaseEvent(e);
}

void OvrToolButton::setCheck(bool a_flag)
{
	bool is_mouse_in = rect().contains(mapFromGlobal(QCursor::pos()));

	if (a_flag)
	{
		if (is_mouse_in && check_hi_icon != nullptr)
		{
			setPixmap(*check_hi_icon);
		}
		else if (check_nor_icon != nullptr)
		{
			setPixmap(*check_nor_icon);
		}
	}
	else
	{
		if (is_mouse_in && hi_icon != nullptr)
		{
			setPixmap(*hi_icon);
		}
		else if (nor_icon != nullptr)
		{
			setPixmap(*nor_icon);
		}
	}

	is_checked = a_flag;
}