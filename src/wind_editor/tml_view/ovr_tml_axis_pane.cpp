﻿#include "Header.h"
#include "ovr_tml_axis_pane.h"

#include "ovr_mark_view.h"
#include "ovr_tml_axis.h"

#include "ovr_time_line_tree_model.h"

/////////////////////////////////////////////////////////////////////
// class OvrTMLAxisPane

OvrTMLAxisPane::OvrTMLAxisPane(QWidget* parent, Qt::WindowFlags f) :QWidget(parent, f)
{
	setFocusPolicy(Qt::StrongFocus);
	setFixedHeight(48);

	QVBoxLayout* v_layout = new QVBoxLayout(this);
	v_layout->setMargin(0);
	v_layout->setSpacing(0);

	time_line_axis = new OvrTMLAxis();
	time_line_axis->setStyleSheet("background:#2F2F2F;color:white");
	setGlobalAxisX(time_line_axis);
	v_layout->addWidget(time_line_axis);

	mark_view = new OvrMarkView;
	mark_view->setStyleSheet("background:#2F2F2F;color:white");
	v_layout->addWidget(mark_view);

	time_line_axis->getAxisView()->createCursorValue(mark_view);

	connect(time_line_axis->getAxisView(), &OvrTMLAxisView::mouseClick, [this]() {
		mark_view->clearSelectedMark();
	});

	connect(mark_view, &OvrMarkView::onDragPress, [this](int a_x) {
		a_x = time_line_axis->getAxisView()->mapFromGlobal(QPoint(a_x, 0)).x();
		time_line_axis->getAxisView()->dragPress(a_x, 0);
	});
	connect(mark_view, &OvrMarkView::onDragMove, [this](int a_x) {
		a_x = time_line_axis->getAxisView()->mapFromGlobal(QPoint(a_x, 0)).x();
		time_line_axis->getAxisView()->dragMove(a_x, 0);
	});
	connect(mark_view, &OvrMarkView::onDragRelease, [this](int a_x) {
		a_x = time_line_axis->getAxisView()->mapFromGlobal(QPoint(a_x, 0)).x();
		time_line_axis->getAxisView()->dragRelease(a_x, 0);
	});
	connect(mark_view, &OvrMarkView::mouseClick, [this](int a_x) {
		a_x = time_line_axis->getAxisView()->mapFromGlobal(QPoint(a_x, 0)).x();
		time_line_axis->getAxisView()->seekToDPX(a_x, true);
	});
}

OvrTMLAxisView* OvrTMLAxisPane::getAxisView()
{
	return time_line_axis->getAxisView();
}

void OvrTMLAxisPane::focusOutEvent(QFocusEvent *event)
{
	mark_view->clearSelectedMark();
}

void OvrTMLAxisPane::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_Delete)
	{
		mark_view->removeMark();
		return;
	}
	else if (event->key() == Qt::Key_M)
	{
		int frame = round(getGlobalAxisView()->getCurFrame());
		OvrTimeLineTreeModel::getInst()->addMark(frame);
		return;
	}

	QWidget::keyPressEvent(event);
}

void OvrTMLAxisPane::wheelEvent(QWheelEvent *event)
{
	event->accept();

	auto view = time_line_axis->getAxisView();
	QPoint pt = view->mapFromGlobal(event->globalPos());

	view->scaleTMLLine(pt.x(), event->delta() < 0);
}