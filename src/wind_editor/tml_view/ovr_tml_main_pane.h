#pragma once

#include <QDockWidget>

class OvrTMLCursorLine;
class OvrTMLCurveEditor;

class OvrTMLMainPane : public QDockWidget, public OvrEventReceiver
{
	Q_OBJECT
public:
	explicit OvrTMLMainPane(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
	~OvrTMLMainPane();

	virtual QString name() override { return "OvrTMLMainPane"; }
	virtual void onEventArrive(const QString& a_event_name, const QVariant& a_param1, const QVariant& a_param2) override;

protected:
	virtual void resizeEvent(QResizeEvent *event) override;

	void initUI();
	void initStyle();

	void initBar();
	void initContext();

	void showCurveEditor(bool);

	QGridLayout* g_layout;
	QTreeWidget* tree;
	OvrTMLCurveEditor* curve_editor;

	OvrTMLCursorLine* tml_v_align_line = nullptr;
};