#include "ovr_bitmap_font_manager.h"
#include "ovr_bitmap_font_render_object.h"
#include "ovr_bitmap_font_texture.h"
//depends on lvr_scene only in android device,so we do not need to add shared lib depends info on windows
// see line 248 where we use it.
//#include "lvr_package_files.h"
#include "ovr_gl_gpu_program_manager.h"
#include "wx_engine/ovr_gpu_program.h"
#include "wx_engine/ovr_material.h"
#include "wx_engine/ovr_material_manager.h"
#include "ovr_gl_vertex_array_object.h"
#include "wx_engine/ovr_hardware_buffer_manager.h"
#include "ovr_gl_hardware_buffer.h"
#define  LVR_BITMAP_TEXTURE_WIDTH	2048
#define  LVR_BITMAP_TEXTURE_HEIGHT	2048

ovr::uint32 lvr_decode_next_char_advance02(const char** putf8Buffer)
{
	ovr::uint32  uc;
	char    c;

	// Security considerations:
	//
	// Changed, this is now only the case for DecodeNextChar:
	//  - If we hit a zero byte, we want to return 0 without stepping
	//    the buffer pointer past the 0. th
	//
	// If we hit an "overlong sequence"; i.e. a character encoded
	// in a longer multibyte string than is necessary, then we
	// need to discard the character.  This is so attackers can't
	// disguise dangerous characters or character sequences --
	// there is only one valid encoding for each character.
	//
	// If we decode characters { 0xD800 .. 0xDFFF } or { 0xFFFE,
	// 0xFFFF } then we ignore them; they are not valid in UTF-8.

	// This isn't actually an invalid character; it's a valid char that
	// looks like an inverted question mark.
#define INVALID_CHARE 0x0FFFD

#define FIRST_BYTEE(mask, shift)     \
	uc = (c & (mask)) << (shift);

#define NEXT_BYTEE(shift) \
	c = **putf8Buffer;   \
	if (c == 0) return 0; /* end of buffer, do not advance */   \
	if ((c & 0xC0) != 0x80) return INVALID_CHARE; /* standard check */  \
	(*putf8Buffer)++;    \
	uc |= (c & 0x3F) << shift;

	c = **putf8Buffer;
	(*putf8Buffer)++;
	if (c == 0)
		return 0;   // End of buffer.

	if ((c & 0x80) == 0) return (ovr::uint32)c; // Conventional 7-bit ASCII.

												// Multi-byte sequences.
	if ((c & 0xE0) == 0xC0)
	{
		// Two-byte sequence.
		FIRST_BYTEE(0x1F, 6);
		NEXT_BYTEE(0);
		if (uc < 0x80) return INVALID_CHARE;  // overlong
		return uc;
	}
	else if ((c & 0xF0) == 0xE0)
	{
		// Three-byte sequence.
		FIRST_BYTEE(0x0F, 12);
		NEXT_BYTEE(6);
		NEXT_BYTEE(0);
		if (uc < 0x800) return INVALID_CHARE; // overlong
											  // Not valid ISO 10646, but Flash requires these to work
											  // see AS3 test e15_5_3_2_3 for String.fromCharCode().charCodeAt(0)
											  // if (uc >= 0x0D800 && uc <= 0x0DFFF) return INVALID_CHAR;
											  // if (uc == 0x0FFFE || uc == 0x0FFFF) return INVALID_CHAR; // not valid ISO 10646
		return uc;
	}
	else if ((c & 0xF8) == 0xF0)
	{
		// Four-byte sequence.
		FIRST_BYTEE(0x07, 18);
		NEXT_BYTEE(12);
		NEXT_BYTEE(6);
		NEXT_BYTEE(0);
		if (uc < 0x010000) return INVALID_CHARE;  // overlong
		return uc;
	}
	else if ((c & 0xFC) == 0xF8)
	{
		// Five-byte sequence.
		FIRST_BYTEE(0x03, 24);
		NEXT_BYTEE(18);
		NEXT_BYTEE(12);
		NEXT_BYTEE(6);
		NEXT_BYTEE(0);
		if (uc < 0x0200000) return INVALID_CHARE; // overlong
		return uc;
	}
	else if ((c & 0xFE) == 0xFC)
	{
		// Six-byte sequence.
		FIRST_BYTEE(0x01, 30);
		NEXT_BYTEE(24);
		NEXT_BYTEE(18);
		NEXT_BYTEE(12);
		NEXT_BYTEE(6);
		NEXT_BYTEE(0);
		if (uc < 0x04000000) return INVALID_CHARE;    // overlong
		return uc;
	}
	else
	{
		// Invalid.
		return INVALID_CHARE;
	}
}

ovr::uint32 lvr_decode_next_char2(const char** putf8Buffer)
{
	unsigned int ch = lvr_decode_next_char_advance02(putf8Buffer);
	if (ch == 0)
		(*putf8Buffer)--;
	return ch;
}

using namespace  ovr_engine;
class OvrBitmapFontManagerImpl
	:public OvrBitmapFontManager
{
public:
	OvrBitmapFontManagerImpl()
		:_is_initiated(false)
		,_id_now(0)
		,_vbo(0)
		,_ibo(0)
		,_font(0)
		,_font_vertices(0)
		,_max_words(384)//do not init too big,just reasonable,and that's enough.
		,_render_words(0)
		, _font_texture_width(LVR_BITMAP_TEXTURE_WIDTH)
		, _font_texture_height(LVR_BITMAP_TEXTURE_HEIGHT)
		,_mem_font(0)
		,_defalut_align_method(ovr_e_font_align_middle)
		,vertex_data(nullptr)
		,index_data(nullptr)
	{}
	~OvrBitmapFontManagerImpl(){}
public:
	virtual bool	init(const char* a_font_path,bool a_from_package);
	virtual void	set_defalut_align_method(OvrEnumFontAlignMethod a_align_method);
	virtual bool	is_initiated();
	virtual void	release();
	virtual float get_advance_x(unsigned int char_code);
	virtual float get_vertical_space();
	virtual int add_render_string(const char* a_str, float a_scale, float a_max_width, const OvrVector3& a_pos, const OvrVector3& a_right, const OvrVector3& a_up, float a_line_spacing, float a_font_spacing);
	virtual int add_render_string(const char* a_str, float a_scale, const OvrVector3& a_pos, const OvrVector3& a_right, const OvrVector3& a_up, float a_line_spacing, float a_font_spacing);
	virtual void set_render_string_color(int a_id, ovr::uint8* a_color);
	virtual void set_render_string_size(int a_id,float a_sclae);
	virtual void set_warp_width(int a_id, float a_width);
	virtual void set_render_string_algin(int a_id,OvrEnumFontAlignMethod a_align_method);
	virtual void set_render_string_position(int a_id,OvrVector3 a_position);
	virtual void query_render_string_size(int a_id,float& ao_width,float& ao_heigh);
	virtual void query_render_text_buffer(int a_id, int& a_size, void* a_buffer);
	virtual void query_render_buffer_size(int a_id,int& a_size);
	virtual void update_render_string(int a_id,float a_pixel_per_meter,const OvrVector3& a_pos,const OvrVector3& a_right,const OvrVector3& a_up);
	virtual void remove_render_string(int a_id);
	virtual void submit_for_render(int a_id);
	virtual void update();
	virtual void draw(const OvrMatrix4& a_view_proj_mat);
	virtual ovr_engine::OvrBitmapFontTexture*  GetBitmapFontTexture() { return &_bitmap_font_texture; }
private:
	bool insert_to_ro(OvrBitmapFontRenderObject* a_bitmap_ro);
	float get_center_baseline_leave(int a_char_code);
private:
	bool				_is_initiated;
	int					_id_now;
	uint32_t			_vbo;
	//lvr_vertex_format*	_vf;
	uint32_t			_ibo;
	OvrFont*			_font;
	struct font_vertex
	{
		OvrVector3 pos;
		OvrVector2 texcoord;
		ovr::uint8 rgba[4];
	};
	font_vertex*		_font_vertices;
	int					_max_words;
	int					_render_words;

	int							_font_texture_width;
	int							_font_texture_height;
public:
	ovr_engine::OvrBitmapFontTexture		_bitmap_font_texture;
private:
	char*						_mem_font;
	OvrEnumFontAlignMethod	_defalut_align_method;
	float						_center_adjust_value;

	std::vector<int>	_ro_need_to_draw;
	//less than _id_now,but can be used,as the thing has been deleted.
	std::vector<int>	_place_can_use;
	std::map<int,OvrBitmapFontRenderObject*>	_font_lists;
	OvrMaterial*       use_mtl;
	OvrVertexData* vertex_data;
	OvrIndexData*  index_data;
};




int OvrBitmapFontManagerImpl::add_render_string(const char* a_str, float a_scale, const OvrVector3& a_pos, const OvrVector3& a_right, const OvrVector3& a_up, float a_line_spacing, float a_font_spacing)
{
	return add_render_string(a_str, a_scale, 100000.0f, a_pos, a_right, a_up,a_line_spacing,a_font_spacing);
}

int OvrBitmapFontManagerImpl::add_render_string(const char* a_str, float a_scale, float a_max_width, const OvrVector3& a_pos, const OvrVector3& a_right, const OvrVector3& a_up, float a_line_spacing,float a_font_spacing)
{
	OvrBitmapFontRenderObject* bro = new OvrBitmapFontRenderObject();
	char* p = (char*)a_str;
	float pixels_len = 0;
	float max_len = 0;//if there \n exist.
	int t_lines = 1;
	uint32_t t_last_code = 0;
	float t_max_width = a_max_width / (a_scale*LVR_FONT_METERS_PER_PIXEL*_font_texture_width);
	for (uint32_t charCode = lvr_decode_next_char2( (const char**)(&p) ); charCode != '\0';charCode = lvr_decode_next_char2( (const char**)(&p) ) )
	{
		if ('\n' == charCode || '\r' == charCode)
		{
			if ('\r' == t_last_code)
			{
				t_last_code = charCode;
				continue;
			}
			bro->_string_codes.push_back(0);
			pixels_len -= (32.0 / 2048.0)*a_font_spacing;
			max_len = lvr_max(pixels_len,max_len);
			pixels_len = 0;
			t_lines++;
			t_last_code = charCode;
			continue;
		}
		else if ('\t' == charCode)
		{
			charCode = ' ';
		}
		bro->_string_codes.push_back(charCode);
		OvrFontGlyphInfo * t_fgi = _bitmap_font_texture.get_font_glyph_info(charCode);
		if (NULL == t_fgi )
		{
			//LVR_LOG("CHAR_NOT_CONTAIN %d",charCode);
			OvrFontGlyph lfg = _font->get_font_glyph(charCode);
			t_fgi = _bitmap_font_texture.put_glyph(&lfg);
			//to do ..... should be called inside font impl.
			delete[] lfg.alpha_bits;
			lfg.alpha_bits = 0;
		}
		float tt = pixels_len;
		if (t_fgi)
		{
			pixels_len += t_fgi->advance_x + (32.0 / 2048.0)*a_font_spacing;
		}
		else
		{
			pixels_len += _bitmap_font_texture._half_word_advance_x + (32.0 / 2048.0)*a_font_spacing;
		}
		if (pixels_len > t_max_width)
		{	
			tt = pixels_len+(32.0 / 2048.0)*a_font_spacing;
			max_len = lvr_max(tt, max_len);
			if (t_fgi)
			{
				pixels_len = t_fgi->advance_x + (32.0 / 2048.0)*a_font_spacing;
			}
			else
			{
				pixels_len = _bitmap_font_texture._half_word_advance_x + (32.0 / 2048.0)*a_font_spacing;
			}
			t_lines++;
		
			//bro->_string_codes.push_back(0);
		}
		t_last_code = charCode;
		bro->_last_line_width = pixels_len;
	}
	max_len = lvr_max(pixels_len,max_len);
	bro->_postion = a_pos;
	bro->_right = a_right;
	bro->_up = a_up;
	bro->_scale_value = a_scale;
	bro->_word_num = bro->_string_codes.size();
	bro->_total_cross_value = max_len;
	bro->_align_method = _defalut_align_method;
	bro->_lines = t_lines;
	bro->_warp_width = a_max_width;
	bro->_line_spacing = a_line_spacing;
	bro->_font_spacing = a_font_spacing;
	int len = _place_can_use.size();
	int res = _id_now;
	if (len > 0)
	{
		res = _place_can_use[len-1];
		_place_can_use.pop_back();
	}
	else
	{
		++_id_now;
	}
	_font_lists[res] = bro;
	return res;
}

void OvrBitmapFontManagerImpl::update_render_string(int a_id,float a_scale,const OvrVector3& a_pos,const OvrVector3& a_right,const OvrVector3& a_up)
{
	std::map<int,OvrBitmapFontRenderObject*>::iterator it = _font_lists.find(a_id);
	if (it != _font_lists.end())
	{
		OvrBitmapFontRenderObject* t_bro = it->second;
		t_bro->_postion = a_pos;
		t_bro->_right = a_right;
		t_bro->_up = a_up;
		t_bro->_scale_value = a_scale;
	}
}

void OvrBitmapFontManagerImpl::remove_render_string(int a_id)
{
	for (std::vector<int>::iterator it0 = _ro_need_to_draw.begin();it0 != _ro_need_to_draw.end();it0++)
	{
		if (a_id == *it0)
		{
			_ro_need_to_draw.erase(it0);
			break;
		}
	}
	std::map<int,OvrBitmapFontRenderObject*>::iterator it = _font_lists.find(a_id);
	if (it != _font_lists.end())
	{
		delete it->second;
		_font_lists.erase(it);
		_place_can_use.push_back(a_id);
	}
}

void OvrBitmapFontManagerImpl::submit_for_render(int a_id)
{
	_ro_need_to_draw.push_back(a_id);
}

void OvrBitmapFontManagerImpl::update()
{
	_render_words = 0;
	std::vector<int>::iterator it = _ro_need_to_draw.begin();
	for (;it != _ro_need_to_draw.end();it++)
	{
		int id = *it;
		std::map<int,OvrBitmapFontRenderObject*>::iterator it_ro = _font_lists.find(id);
		if (it_ro != _font_lists.end())
		{
			insert_to_ro(it_ro->second);
		}
	}
	//glBindBuffer(GL_ARRAY_BUFFER,_vbo);
	uint32_t t_sz = _render_words*4*sizeof(font_vertex);
	//it is said that the gl hardware will perform better if you are using shared memory gpus
	//so if you change to gles3 libs,do remember to use the win style work.
//#ifdef LVR_OS_WIN32
	//void* mem = glMapBufferRange(GL_ARRAY_BUFFER,0,t_sz,GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
	//memcpy(mem,&_font_vertices[0],t_sz);
	//glUnmapBuffer(GL_ARRAY_BUFFER);
//#else
	//glBufferSubData(GL_ARRAY_BUFFER,0,t_sz,&_font_vertices[0]);
//#endif
	//glBindBuffer(GL_ARRAY_BUFFER,0);
	if (vertex_data)
	{
		vertex_data->vertex_buffer->WriteData(0, t_sz, &_font_vertices[0]);
	}
}

void OvrBitmapFontManagerImpl::draw(const OvrMatrix4& a_view_proj_mat)
{
	if (_render_words < 1)
	{
		return;
	}
	glDepthMask(GL_FALSE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDisable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//lvr_program* prog3 = lvr_shader_manager::get_shader_mgr()->get_shader_program("font_render_3d_simple");
	ovr_engine::OvrGpuProgram* prog = OvrGLGpuProgramManager::GetIns()->GetProgram("FontRender");

	//prog3->bind();
	prog->BindProgram();
	OvrGpuProgramParameters aa;
	OvrAutoParamSource src;
	src.mvp_matrix = a_view_proj_mat;
	ovr::uint16 shader_param_dirty = 0;
	shader_param_dirty  |= GPV_PER_OBJECT;

	use_mtl->UpdateAutoParameters(src, shader_param_dirty);
	prog->BindProgramParameters(use_mtl->GetProgramParameters(), shader_param_dirty);
	_bitmap_font_texture.bind(0);
	//prog3->set_uniform_matrix4fv(prog3->u_mvp_mat,(float*)&a_view_proj_mat[0]);
	//prog3->set_uniform4f(prog3->u_color,1,1,1,1);

	//prog->BindProgramParameters();

	//glBindBuffer(GL_ARRAY_BUFFER,_vbo);
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,_ibo);
//	_vf->bind();

	OvrGLVertexArrayObject* vao =
		static_cast<OvrGLVertexArrayObject*>(vertex_data->declaration);

	vao->Bind();
	bool updateVAO = vao->NeedUpdate();

	if (updateVAO)
	{
		vao->BindToGpu((OvrGLHardwareVertexBuffer*)vertex_data->vertex_buffer);
	}

	//glDrawElements(GL_TRIANGLES,_render_words*6, GL_UNSIGNED_INT,NULL);
	GLuint ibo = static_cast<OvrGLHardwareIndexBuffer*>(index_data->index_buffer)->GetBufferId();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	int aaa = _render_words * 6 - index_data->index_buffer->GetIndexCount();
	glDrawElements(GL_TRIANGLES,
		_render_words*6,
		GL_UNSIGNED_INT,
		0);
	//_vf->unbind();
	vao->UnBind();
	glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
	_ro_need_to_draw.clear();

	glDepthMask(GL_TRUE);
	//prog3->unbind();
	prog->UnBindProgram();
}

bool OvrBitmapFontManagerImpl::insert_to_ro(OvrBitmapFontRenderObject* a_bitmap_ro)
{
	const float size_scale_x = LVR_FONT_METERS_PER_PIXEL*a_bitmap_ro->_scale_value*_font_texture_width;
	const float size_scale_y = LVR_FONT_METERS_PER_PIXEL*a_bitmap_ro->_scale_value*_font_texture_height;
	//1.5 times array pitch
	const float font_height = ((float)_font->get_font_size()*(1.125 + a_bitmap_ro->_line_spacing))*LVR_FONT_METERS_PER_PIXEL*a_bitmap_ro->_scale_value;
	OvrVector3 t_right = a_bitmap_ro->_right;
	OvrVector3 t_up = a_bitmap_ro->_up;
	OvrVector3 t_normal = t_right.cross(t_up);
	t_normal.normalize();
	const OvrVector3 ct_pos_origin = a_bitmap_ro->_postion + t_normal*0.01f;
	OvrVector3 t_cur_pos = ct_pos_origin;
	const OvrVector3 t_line_increase = t_up*font_height*-1.0f;
	std::vector<ovr::uint16>& t_words = a_bitmap_ro->_string_codes;
	int word_num = a_bitmap_ro->_word_num;
	if (word_num + _render_words > _max_words)
	{
		//LVR_LOG("enlarge the value of max_words,it is too small now. we need at least %d while current is only %d",word_num + _render_words,_max_words);
		return false;//make sure we do not make a memory mistake.
	}
	int line_num = 0;
	ovr::uint8* t_color= a_bitmap_ro->_color;
	//while single line,do not need 1.125 line distance,when double line,this make sense
	OvrVector3  t_align_move = t_up * font_height*(float)(a_bitmap_ro->_lines - 1) *0.5f;
	switch(a_bitmap_ro->_align_method)
	{
	case ovr_e_font_align_left:
		break;
	case ovr_e_font_align_right:
		t_align_move += t_right*a_bitmap_ro->_total_cross_value*size_scale_x*-1.0f;
		break;
	case ovr_e_font_align_middle:
		t_align_move += t_right*a_bitmap_ro->_total_cross_value*size_scale_x*-0.5f;
		break;
	default:
		break;
	}
	float t_width_now = 0;
	std::vector<font_vertex>	tmp_vertices;
	tmp_vertices.reserve(word_num * 4);
	uint32_t  t_begin_pos = 0;
	float  t_width_len = a_bitmap_ro->_total_cross_value*size_scale_x;
	for(int i=0;i<word_num;++i)
	{
		const ovr::uint16 t_code = t_words[i];
		if (0 == t_code)//we make zero be a \n 
		{
			++line_num;
			t_cur_pos = ct_pos_origin + t_line_increase*(float)line_num;
			uint32_t t_pos_now = tmp_vertices.size();
			t_width_now -= (32.0 / 2048.0)*(a_bitmap_ro->_font_spacing)*size_scale_x;
			switch (a_bitmap_ro->_align_method)
			{
			case ovr_e_font_align_left:
				break;
			case ovr_e_font_align_right:
			{
				OvrVector3 t_pos_change = t_right*(t_width_len - t_width_now);
				for (uint32_t i = t_begin_pos; i < t_pos_now;++i)
				{
					tmp_vertices[i].pos += t_pos_change;
				}
			}
			break;
			case ovr_e_font_align_middle:
			{
				OvrVector3 t_pos_change = t_right*(t_width_len - t_width_now)*0.5;
				for (uint32_t i = t_begin_pos; i < t_pos_now; ++i)
				{
					tmp_vertices[i].pos += t_pos_change;
				}
			}
			break;
			default:
				break;
			}
			t_width_now = 0;
			t_begin_pos = t_pos_now;
			continue;
		}
		OvrFontGlyphInfo* t_fgi = _bitmap_font_texture.get_font_glyph_info(t_code);
		if (!t_fgi)
		{
			t_cur_pos += t_right*_bitmap_font_texture._half_word_advance_x*size_scale_x;
			t_width_now += _bitmap_font_texture._half_word_advance_x*size_scale_x;
			continue;
		}
		float t_cur_width = (t_fgi->advance_x + (32.0 / 2048.0)*(a_bitmap_ro->_font_spacing))*size_scale_x;
		t_width_now += t_cur_width;
		if (t_width_now >a_bitmap_ro->_warp_width)
		{
			++line_num;
			t_begin_pos = tmp_vertices.size();
			t_cur_pos = ct_pos_origin + t_line_increase*(float)line_num;
			t_width_now = t_cur_width;
		}

		font_vertex  ttt[4];
		font_vertex& A = ttt[0];
		font_vertex& B = ttt[1];
		font_vertex& C = ttt[2];
		font_vertex& D = ttt[3];
		OvrVector3 corner_pos = t_cur_pos + t_right*t_fgi->bearing_x*size_scale_x - t_up*(t_fgi->height - t_fgi->bearing_y + _center_adjust_value)*size_scale_y;
		corner_pos += t_align_move;//update in the near future.
		OvrVector3 right_vec = t_right*t_fgi->width*size_scale_x;
		OvrVector3 up_vec = t_up*t_fgi->height*size_scale_y;
		A.pos = corner_pos;
		A.texcoord = OvrVector2(t_fgi->x,t_fgi->y);
		
		B.pos = corner_pos + right_vec;
		B.texcoord = OvrVector2(t_fgi->x + t_fgi->width,t_fgi->y);

		C.pos = corner_pos + right_vec + up_vec;
		C.texcoord = OvrVector2(t_fgi->x+t_fgi->width,t_fgi->y+t_fgi->height);
		
		D.pos = corner_pos + up_vec;
		D.texcoord = OvrVector2(t_fgi->x,t_fgi->y+t_fgi->height);
		for (int i = 0; i < 4; ++i)
		{
			A.rgba[i] = t_color[i];
			B.rgba[i] = t_color[i];
			C.rgba[i] = t_color[i];
			D.rgba[i] = t_color[i];
		}
		//t_fgi->advance_x*_font_texture_width = width pixels,then multi meters per pixel to meters
		//and finnally multi scale to real size.
		const OvrVector3 right_leave = t_right*t_cur_width + t_right*size_scale_x*(32.0 / 2048.0)*(a_bitmap_ro->_font_spacing);
		t_cur_pos += right_leave;
		tmp_vertices.push_back(ttt[0]);
		tmp_vertices.push_back(ttt[1]);
		tmp_vertices.push_back(ttt[2]);
		tmp_vertices.push_back(ttt[3]);
	}
	uint32_t t_pos_now = tmp_vertices.size();
	t_width_now -= (32.0 / 2048.0)*(a_bitmap_ro->_font_spacing)*size_scale_x;
	switch (a_bitmap_ro->_align_method)
	{
	case ovr_e_font_align_left:
		break;
	case ovr_e_font_align_right:
	{
		OvrVector3 t_pos_change = t_right*(t_width_len - t_width_now);
		for (uint32_t i = t_begin_pos; i < t_pos_now; ++i)
		{
			tmp_vertices[i].pos += t_pos_change;
		}
	}
	break;
	case ovr_e_font_align_middle:
	{
		OvrVector3 t_pos_change = t_right*(t_width_len - t_width_now)*0.5;
		for (uint32_t i = t_begin_pos; i < t_pos_now; ++i)
		{
			tmp_vertices[i].pos += t_pos_change;
		}
	}
	break;
	default:
		break;
	}
	uint32_t t_w_num = tmp_vertices.size();
	for (uint32_t i = 0; i < t_w_num;++i)
	{
		uint32_t t_pos_id = _render_words * 4 + i % 4;
		_font_vertices[t_pos_id].pos = tmp_vertices[i].pos;
		for (int i = 0; i < 4; ++i)
		{
			(_font_vertices[t_pos_id]).rgba[i] = (tmp_vertices[i].rgba)[i];
		}
		//_font_vertices[t_pos_id].rgba = tmp_vertices[i].rgba;
		_font_vertices[t_pos_id].texcoord = tmp_vertices[i].texcoord;
		if (i%4 == 3)
		{
			++_render_words;
			if (_render_words >= _max_words)
			{
				return true;
			}
		}
	}
	return true;
}
extern char* g_chinese_simple_str;
//#include "chinese_simp_string.cpp"
bool OvrBitmapFontManagerImpl::init(const char* a_font_path,bool a_from_package)
{

	use_mtl = OvrMaterialManager::GetIns()->Create("BitmapFont_mtl");
	use_mtl->SetProgram("FontRender");

	OvrFontManager* font_mgr = OvrFontManager::get_font_manager();
	const char* font_name = "chinese";
	//depends on lvr_scene only in android device,so we do not need to add shared lib depends info on windows
	if (a_from_package)
	{
		//void* buf;
		//int len;
		////lvr_read_file_from_application_package(a_font_path,len,buf);
		//if(buf && font_mgr->add_font_from_mem((const char*)buf,len,(const char*)font_name))
		//{
		//	_mem_font = (char*)buf;
		//}
		//else
		//{
		//	LVR_LOG("pyerror,add font failed,make sure you know everything. %p",buf);
		//	if (buf)
		//	{
		//		LVR_LOG("pyerror,buf not exist");
		//		free(buf);
		//	}
		//}
	}
	else
	{
		if(!font_mgr->add_font_from_file((const char*)a_font_path,(const char*)font_name))
		{
			return false;
		}
	}
	_font = font_mgr->get_font((const char*)font_name);
	_font->set_font_size(LVR_FONT_SIZE);
	_font_texture_width = LVR_BITMAP_TEXTURE_WIDTH;
	_font_texture_height = LVR_BITMAP_TEXTURE_WIDTH;
	_bitmap_font_texture.init(_font_texture_width, _font_texture_height, LVR_FONT_SIZE);
	//////////////////////////////////////////////////////////////////////////
	
#ifdef LVR_OS_ANDROID
	//char* p = g_chinese_simple_str;
	char* p = "正在使用手机网络暂无 abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
#else
	//wchar_t * my_dd = L"啊阿埃挨哎唉哀皑癌蔼矮艾碍爱隘鞍氨安俺按暗岸胺案肮昂盎凹敖熬翱袄傲奥懊澳芭捌扒叭吧笆八疤巴拔跋靶把耙坝霸罢爸白柏百摆佰败拜稗斑班搬扳般颁板版扮拌伴瓣半办绊邦帮梆榜膀绑棒磅蚌镑傍谤苞胞包褒剥薄雹保堡饱宝抱报暴豹鲍爆杯碑悲卑北辈背贝钡倍狈备惫焙被奔苯本笨崩绷甭泵蹦迸逼鼻比鄙笔彼碧蓖蔽毕毙毖币庇痹闭敝弊必辟壁臂避陛鞭边编贬扁便变卞辨辩辫遍标彪膘表鳖憋别瘪彬斌濒滨宾摈兵冰柄丙秉饼炳病并玻菠播拨钵波博勃搏铂箔伯帛舶脖膊渤泊驳捕卜哺补埠不布步簿部怖擦猜裁材才财睬踩采彩菜蔡餐参蚕残惭惨灿苍舱仓沧藏操糙槽曹草厕策侧册测层蹭插叉茬茶查碴搽察岔差诧拆柴豺搀掺蝉馋谗缠铲产阐颤昌猖场尝常长偿肠厂敞畅唱倡超抄钞朝嘲潮巢吵炒车扯撤掣彻澈郴臣辰尘晨忱沉陈趁衬撑称城橙成呈乘程惩澄诚承逞骋秤吃痴持匙池迟弛驰耻齿侈尺赤翅斥炽充冲虫崇宠抽酬畴踌稠愁筹仇绸瞅丑臭初出橱厨躇锄雏滁除楚础储矗搐触处揣川穿椽传船喘串疮窗幢床闯创吹炊捶锤垂春椿醇唇淳纯蠢戳绰疵茨磁雌辞慈瓷词此刺赐次聪葱囱匆从丛凑粗醋簇促蹿篡窜摧崔催脆瘁粹淬翠村存寸磋撮搓措挫错搭达答瘩打大呆歹傣戴带殆代贷袋待逮怠耽担丹单郸掸胆旦氮但惮淡诞弹蛋当挡党荡档刀捣蹈倒岛祷导到稻悼道盗德得的蹬灯登等瞪凳邓堤低滴迪敌笛狄涤翟嫡抵底地蒂第帝弟递缔颠掂滇碘点典靛垫电佃甸店惦奠淀殿碉叼雕凋刁掉吊钓调跌爹碟蝶迭谍叠丁盯叮钉顶鼎锭定订丢东冬董懂动栋侗恫冻洞兜抖斗陡豆逗痘督都毒犊独读堵睹赌杜镀肚度渡妒端短锻段断缎堆兑队对墩吨蹲敦顿囤钝盾遁掇哆多夺垛躲朵跺舵剁惰堕蛾峨鹅俄额讹娥恶厄扼遏鄂饿恩而儿耳尔饵洱二贰发罚筏伐乏阀法珐藩帆番翻樊矾钒繁凡烦反返范贩犯饭泛坊芳方肪房防妨仿访纺放菲非啡飞肥匪诽吠肺废沸费芬酚吩氛分纷坟焚汾粉奋份忿愤粪丰封枫蜂峰锋风疯烽逢冯缝讽奉凤佛否夫敷肤孵扶拂辐幅氟符伏俘服浮涪福袱弗甫抚辅俯釜斧脯腑府腐赴副覆赋复傅付阜父腹负富讣附妇缚咐噶嘎该改概钙盖溉干甘杆柑竿肝赶感秆敢赣冈刚钢缸肛纲岗港杠篙皋高膏羔糕搞镐稿告哥歌搁戈鸽胳疙割革葛格蛤阁隔铬个各给根跟耕更庚羹埂耿梗工攻功恭龚供躬公宫弓巩汞拱贡共钩勾沟苟狗垢构购够辜菇咕箍估沽孤姑鼓古蛊骨谷股故顾固雇刮瓜剐寡挂褂乖拐怪棺关官冠观管馆罐惯灌贯光广逛瑰规圭硅归龟闺轨鬼诡癸桂柜跪贵刽辊滚棍锅郭国果裹过哈骸孩海氦亥害骇酣憨邯韩含涵寒函喊罕翰撼捍旱憾悍焊汗汉夯杭航壕嚎豪毫郝好耗号浩呵喝荷菏核禾和何合盒貉阂河涸赫褐鹤贺嘿黑痕很狠恨哼亨横衡恒轰哄烘虹鸿洪宏弘红喉侯猴吼厚候后呼乎忽瑚壶葫胡蝴狐糊湖弧虎唬护互沪户花哗华猾滑画划化话槐徊怀淮坏欢环桓还缓换患唤痪豢焕涣宦幻荒慌黄磺蝗簧皇凰惶煌晃幌恍谎灰挥辉徽恢蛔回毁悔慧卉惠晦贿秽会烩汇讳诲绘荤昏婚魂浑混豁活伙火获或惑霍货祸击圾基机畸稽积箕肌饥迹激讥鸡姬绩缉吉极棘辑籍集及急疾汲即嫉级挤几脊己蓟技冀季伎祭剂悸济寄寂计记既忌际妓继纪嘉枷夹佳家加荚颊贾甲钾假稼价架驾嫁歼监坚尖笺间煎兼肩艰奸缄茧检柬碱碱拣捡简俭剪减荐槛鉴践贱见键箭件健舰剑饯渐溅涧建僵姜将浆江疆蒋桨奖讲匠酱降蕉椒礁焦胶交郊浇骄娇嚼搅铰矫侥脚狡角饺缴绞剿教酵轿较叫窖揭接皆秸街阶截劫节桔杰捷睫竭洁结解姐戒藉芥界借介疥诫届巾筋斤金今津襟紧锦仅谨进靳晋禁近烬浸尽劲荆兢茎睛晶鲸京惊精粳经井警景颈静境敬镜径痉靖竟竞净炯窘揪究纠玖韭久灸九酒厩救旧臼舅咎就疚鞠拘狙疽居驹菊局咀矩举沮聚拒据巨具距踞锯俱句惧炬剧捐鹃娟倦眷卷绢撅攫抉掘倔爵觉决诀绝均菌钧军君峻俊竣浚郡骏喀咖卡咯开揩楷凯慨刊堪勘坎砍看康慷糠扛抗亢炕考拷烤靠坷苛柯棵磕颗科壳咳可渴克刻客课肯啃垦恳坑吭空恐孔控抠口扣寇枯哭窟苦酷库裤夸垮挎跨胯块筷侩快宽款匡筐狂框矿眶旷况亏盔岿窥葵奎魁傀馈愧溃坤昆捆困括扩廓阔垃拉喇蜡腊辣啦莱来赖蓝婪栏拦篮阑兰澜谰揽览懒缆烂滥琅榔狼廊郎朗浪捞劳牢老佬姥酪烙涝勒乐雷镭蕾磊累儡垒擂肋类泪棱楞冷厘梨犁黎篱狸离漓理李里鲤礼莉荔吏栗丽厉励砾历利僳例俐痢立粒沥隶力璃哩俩联莲连镰廉怜涟帘敛脸链恋炼练粮凉梁粱良两辆量晾亮谅撩聊僚疗燎寥辽潦了撂镣廖料列裂烈劣猎琳林磷霖临邻鳞淋凛赁吝拎玲菱零龄铃伶羚凌灵陵岭领另令溜琉榴硫馏留刘瘤流柳六龙聋咙笼窿隆垄拢陇楼娄搂篓漏陋芦卢颅庐炉掳卤虏鲁麓碌露路赂鹿潞禄录陆戮驴吕铝侣旅履屡缕虑氯律率滤绿峦挛孪滦卵乱掠略抡轮伦仑沦纶论萝螺罗逻锣箩骡裸落洛骆络妈麻玛码蚂马骂嘛吗埋买麦卖迈脉瞒馒蛮满蔓曼慢漫谩芒茫盲氓忙莽猫茅锚毛矛铆卯茂冒帽貌贸么玫枚梅酶霉煤没眉媒镁每美昧寐妹媚门闷们萌蒙檬盟锰猛梦孟眯醚靡糜迷谜弥米秘觅泌蜜密幂棉眠绵冕免勉娩缅面苗描瞄藐秒渺庙妙蔑灭民抿皿敏悯闽明螟鸣铭名命谬摸摹蘑模膜磨摩魔抹末莫墨默沫漠寞陌谋牟某拇牡亩姆母墓暮幕募慕木目睦牧穆拿哪呐钠那娜纳氖乃奶耐奈南男难囊挠脑恼闹淖呢馁内嫩能妮霓倪泥尼拟你匿腻逆溺蔫拈年碾撵捻念娘酿鸟尿捏聂孽啮镊镍涅您柠狞凝宁拧泞牛扭钮纽脓浓农弄奴努怒女暖虐疟挪懦糯诺哦欧鸥殴藕呕偶沤啪趴爬帕怕琶拍排牌徘湃派攀潘盘磐盼畔判叛乓庞旁耪胖抛咆刨炮袍跑泡呸胚培裴赔陪配佩沛喷盆砰抨烹澎彭蓬棚硼篷膨朋鹏捧碰坯砒霹批披劈琵毗啤脾疲皮匹痞僻屁譬篇偏片骗飘漂瓢票撇瞥拼频贫品聘乒坪苹萍平凭瓶评屏坡泼颇婆破魄迫粕剖扑铺仆莆葡菩蒲埔朴圃普浦谱曝瀑期欺栖戚妻七凄漆柒沏其棋奇歧畦崎脐齐旗祈祁骑起岂乞企启契砌器气迄弃汽泣讫掐洽牵扦钎铅千迁签仟谦乾黔钱钳前潜遣浅谴堑嵌欠歉枪呛腔羌墙蔷强抢橇锹敲悄桥瞧乔侨巧鞘撬翘峭俏窍切茄且怯窃钦侵亲秦琴勤芹擒禽寝沁青轻氢倾卿清擎晴氰情顷请庆琼穷秋丘邱球求囚酋泅趋区蛆曲躯屈驱渠取娶龋趣去圈颧权醛泉全痊拳犬券劝缺炔瘸却鹊榷确雀裙群然燃冉染瓤壤攘嚷让饶扰绕惹热壬仁人忍韧任认刃妊纫扔仍日戎茸蓉荣融熔溶容绒冗揉柔肉茹蠕儒孺如辱乳汝入褥软阮蕊瑞锐闰润若弱撒洒萨腮鳃塞赛三叁伞散桑嗓丧搔骚扫嫂瑟色涩森僧莎砂杀刹沙纱傻啥煞筛晒珊苫杉山删煽衫闪陕擅赡膳善汕扇缮墒伤商赏晌上尚裳梢捎稍烧芍勺韶少哨邵绍奢赊蛇舌舍赦摄射慑涉社设砷申呻伸身深娠绅神沈审婶甚肾慎渗声生甥牲升绳省盛剩胜圣师失狮施湿诗尸虱十石拾时什食蚀实识史矢使屎驶始式示士世柿事拭誓逝势是嗜噬适仕侍释饰氏市恃室视试收手首守寿授售受瘦兽蔬枢梳殊抒输叔舒淑疏书赎孰熟薯暑曙署蜀黍鼠属术述树束戍竖墅庶数漱恕刷耍摔衰甩帅栓拴霜双爽谁水睡税吮瞬顺舜说硕朔烁斯撕嘶思私司丝死肆寺嗣四伺似饲巳松耸怂颂送宋讼诵搜艘擞嗽苏酥俗素速粟僳塑溯宿诉肃酸蒜算虽隋随绥髓碎岁穗遂隧祟孙损笋蓑梭唆缩琐索锁所塌他它她塔獭挞蹋踏胎苔抬台泰酞太态汰坍摊贪瘫滩坛檀痰潭谭谈坦毯袒碳探叹炭汤塘搪堂棠膛唐糖倘躺淌趟烫掏涛滔绦萄桃逃淘陶讨套特藤腾疼誊梯剔踢锑提题蹄啼体替嚏惕涕剃屉天添填田甜恬舔腆挑条迢眺跳贴铁帖厅听烃汀廷停亭庭挺艇通桐酮瞳同铜彤童桶捅筒统痛偷投头透凸秃突图徒途涂屠土吐兔湍团推颓腿蜕褪退吞屯臀拖托脱鸵陀驮驼椭妥拓唾挖哇蛙洼娃瓦袜歪外豌弯湾玩顽丸烷完碗挽晚皖惋宛婉万腕汪王亡枉网往旺望忘妄威巍微危韦违桅围唯惟为潍维苇萎委伟伪尾纬未蔚味畏胃喂魏位渭谓尉慰卫瘟温蚊文闻纹吻稳紊问嗡翁瓮挝蜗涡窝我斡卧握沃巫呜钨乌污诬屋无芜梧吾吴毋武五捂午舞伍侮坞戊雾晤物勿务悟误昔熙析西硒矽晰嘻吸锡牺稀息希悉膝夕惜熄烯溪汐犀檄袭席习媳喜铣洗系隙戏细瞎虾匣霞辖暇峡侠狭下厦夏吓掀锨先仙鲜纤咸贤衔舷闲涎弦嫌显险现献县腺馅羡宪陷限线相厢镶香箱襄湘乡翔祥详想响享项巷橡像向象萧硝霄削哮嚣销消宵淆晓小孝校肖啸笑效楔些歇蝎鞋协挟携邪斜胁谐写械卸蟹懈泄泻谢屑薪芯锌欣辛新忻心信衅星腥猩惺兴刑型形邢行醒幸杏性姓兄凶胸匈汹雄熊休修羞朽嗅锈秀袖绣墟戌需虚嘘须徐许蓄酗叙旭序畜恤絮婿绪续轩喧宣悬旋玄选癣眩绚靴薛学穴雪血勋熏循旬询寻驯巡殉汛训讯逊迅压押鸦鸭呀丫芽牙蚜崖衙涯雅哑亚讶焉咽阉烟淹盐严研蜒岩延言颜阎炎沿奄掩眼衍演艳堰燕厌砚雁唁彦焰宴谚验殃央鸯秧杨扬佯疡羊洋阳氧仰痒养样漾邀腰妖瑶摇尧遥窑谣姚咬舀药要耀椰噎耶爷野冶也页掖业叶曳腋夜液一壹医揖铱依伊衣颐夷遗移仪胰疑沂宜姨彝椅蚁倚已乙矣以艺抑易邑屹亿役臆逸肄疫亦裔意毅忆义益溢诣议谊译异翼翌绎茵荫因殷音阴姻吟银淫寅饮尹引隐印英樱婴鹰应缨莹萤营荧蝇迎赢盈影颖硬映哟拥佣臃痈庸雍踊蛹咏泳涌永恿勇用幽优悠忧尤由邮铀犹油游酉有友右佑釉诱又幼迂淤于盂榆虞愚舆余俞逾鱼愉渝渔隅予娱雨与屿禹宇语羽玉域芋郁吁遇喻峪御愈欲狱育誉浴寓裕预豫驭鸳渊冤元垣袁原援辕园员圆猿源缘远苑愿怨院曰约越跃钥岳粤月悦阅耘云郧匀陨允运蕴酝晕韵孕匝砸杂栽哉灾宰载再在咱攒暂赞赃脏葬遭糟凿藻枣早澡蚤躁噪造皂灶燥责择则泽贼怎增憎曾赠扎喳渣札轧铡闸眨栅榨咋乍炸诈摘斋宅窄债寨瞻毡詹粘沾盏斩辗崭展蘸栈占战站湛绽樟章彰漳张掌涨杖丈帐账仗胀瘴障招昭找沼赵照罩兆肇召遮折哲蛰辙者锗蔗这浙珍斟真甄砧臻贞针侦枕疹诊震振镇阵蒸挣睁征狰争怔整拯正政帧症郑证芝枝支吱蜘知肢脂汁之织职直植殖执值侄址指止趾只旨纸志挚掷至致置帜峙制智秩稚质炙痔滞治窒中盅忠钟衷终种肿重仲众舟周州洲诌粥轴肘帚咒皱宙昼骤珠株蛛朱猪诸诛逐竹烛煮拄瞩嘱主著柱助蛀贮铸筑住注祝驻抓爪拽专砖转撰赚篆桩庄装妆撞壮状椎锥追赘坠缀谆准捉拙卓桌琢茁酌啄着灼浊兹咨资姿滋淄孜紫仔籽滓子自渍字鬃棕踪宗综总纵邹走奏揍租足卒族祖诅阻组钻纂嘴醉最罪尊遵昨左佐柞做作坐座abcdefghijklmnopqrstuvwxyz0123456789_ABCDEFGHIJKLMNOPQRSTUVWXYZ[]\\ / @!";
	//wchar_t * my_dd = L"正在使用手机网络暂无 abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	//char show_str[128];//"this is a good\n choice to go."
	//memset(show_str, 0, 64);
	/*lvr_FW2UTF8Convert(my_dd, wcslen(my_dd), show_str, 128);*/
	char* p = "正在使用手机网络暂无 abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";;
#endif
	//text preload is not needed currently,uncomment it when you need.
	for (ovr::uint32 charCode = lvr_decode_next_char2((const char**)(&p)); charCode != '\0'; charCode = lvr_decode_next_char2((const char**)(&p)))
	{	
		OvrFontGlyph lfg = _font->get_font_glyph(charCode);
		_bitmap_font_texture.put_glyph(&lfg);
		//to do ..... should be called inside font impl.
		delete[] lfg.alpha_bits;
		lfg.alpha_bits = 0;
	}
	//////////////////////////////////////////////////////////////////////////
	/*_vf = lvr_vertex_format::create(3,
		e_location_position,e_attrib_type_float3,
		e_location_uv0,e_attrib_type_float2,
		e_location_color,e_attrib_type_ubyte4);
	_vf->add_ref();*/
	vertex_data = new OvrVertexData;
	this->vertex_data->declaration->AddElement(0, VET_FLOAT3, VES_POSITION);
	this->vertex_data->declaration->AddElement(12, VET_FLOAT2, VES_UV0);
	this->vertex_data->declaration->AddElement(20, VET_UBYTE4, VES_COLOR);

	const int t_index_num = _max_words*6;
	uint32_t* t_indices = new uint32_t[t_index_num];
	int v = 0;
	for (int i=0;i<_max_words;++i)
	{
		t_indices[i * 6 + 0] = v + 0;
		t_indices[i * 6 + 1] = v + 1;
		t_indices[i * 6 + 2] = v + 2;
		t_indices[i * 6 + 3] = v + 0;
		t_indices[i * 6 + 4] = v + 2;
		t_indices[i * 6 + 5] = v + 3;
		v += 4;
	}
	//glGenBuffers(1,&_ibo);
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,_ibo);
	//glBufferData(GL_ELEMENT_ARRAY_BUFFER,t_index_num*sizeof(uint16_t),t_indices,GL_STATIC_DRAW);
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
	
	index_data = new OvrIndexData;
	index_data->index_buffer = OvrHardwareBufferManager::GetIns()->CreateIndexBuffer(t_index_num, t_indices);
	delete[] t_indices;



	_render_words = 0;

	const int vertices_num = _max_words*4;
	_font_vertices = new font_vertex[vertices_num];

	//glGenBuffers(1,&_vbo);
	//glBindBuffer(GL_ARRAY_BUFFER,_vbo);
	//glBufferData(GL_ARRAY_BUFFER,vertices_num*sizeof(font_vertex),_font_vertices,GL_DYNAMIC_DRAW);
	//glBindBuffer(GL_ARRAY_BUFFER,0);

	vertex_data->vertex_buffer = OvrHardwareBufferManager::GetIns()->CreateVertexBuffer(24, vertices_num, &_font_vertices[0],false);


	_vbo = ((OvrGLHardwareVertexBuffer*)vertex_data->vertex_buffer)->GetBufferId();
	_center_adjust_value = get_center_baseline_leave(22120);
	_is_initiated = true;
	return true;
}

bool OvrBitmapFontManagerImpl::is_initiated()
{
	return _is_initiated;
}


void OvrBitmapFontManagerImpl::release()
{
	/*if (_vf)
	{
		_vf->release();
		_vf = 0;
	}*/
	if (_font_vertices)
	{
		delete[] _font_vertices;
		_font_vertices = NULL;
	}
	std::map<int, OvrBitmapFontRenderObject*>::iterator it = _font_lists.begin();
	for (;it != _font_lists.end();it++)
	{
		delete it->second;
	}
	_font_lists.clear();
	OvrFontManager::get_font_manager()->release_all();
	_is_initiated = false;
    
    if(_mem_font){
        free(_mem_font);
    }
}

void OvrBitmapFontManagerImpl::set_render_string_color(int a_id, ovr::uint8* a_color)
{
	std::map<int,OvrBitmapFontRenderObject*>::iterator it = _font_lists.find(a_id);
	if (it != _font_lists.end())
	{
		OvrBitmapFontRenderObject* t_bro = it->second;
		for (int i = 0; i < 4; ++i)
		{
			t_bro->_color[i] = a_color[i];
		}
	}
}

void OvrBitmapFontManagerImpl::set_render_string_size(int a_id,float a_sclae)
{
	std::map<int,OvrBitmapFontRenderObject*>::iterator it = _font_lists.find(a_id);
	if (it != _font_lists.end())
	{
		OvrBitmapFontRenderObject* t_bro = it->second;
		t_bro->_scale_value = a_sclae;
	}
}

void OvrBitmapFontManagerImpl::set_warp_width(int a_id, float a_width)
{
	std::map<int, OvrBitmapFontRenderObject*>::iterator it = _font_lists.find(a_id);
	if (it != _font_lists.end())
	{
		OvrBitmapFontRenderObject* t_bro = it->second;
		t_bro->_warp_width = a_width;
	}
}

void OvrBitmapFontManagerImpl::set_render_string_position(int a_id,OvrVector3 a_position)
{
	std::map<int,OvrBitmapFontRenderObject*>::iterator iter = _font_lists.find(a_id);
	if (iter != _font_lists.end())
	{
		OvrBitmapFontRenderObject* t_bro = iter->second;
		t_bro->_postion = a_position;
	}
}

void OvrBitmapFontManagerImpl::set_render_string_algin(int a_id,OvrEnumFontAlignMethod a_align_method)
{
	std::map<int,OvrBitmapFontRenderObject*>::iterator iter = _font_lists.find(a_id);
	if (iter != _font_lists.end())
	{
		OvrBitmapFontRenderObject* t_bro = iter->second;
		t_bro->_align_method = a_align_method;
	}
}

void OvrBitmapFontManagerImpl::set_defalut_align_method(OvrEnumFontAlignMethod a_align_method)
{
	_defalut_align_method = a_align_method;
}

void OvrBitmapFontManagerImpl::query_render_string_size(int a_id,float& ao_width,float& ao_height)
{
	std::map<int,OvrBitmapFontRenderObject*>::iterator iter = _font_lists.find(a_id);
	if (iter != _font_lists.end())
	{
		OvrBitmapFontRenderObject* t_bitmap_ro = iter->second;
		const float size_scale_x = LVR_FONT_METERS_PER_PIXEL*t_bitmap_ro->_scale_value*_font_texture_width;
		ao_height = (float)_font->get_font_size()*((float)(t_bitmap_ro->_lines-1)*1.125f + 1.0f)*LVR_FONT_METERS_PER_PIXEL*t_bitmap_ro->_scale_value;
		ao_width = t_bitmap_ro->_total_cross_value*size_scale_x;
	}
	else
	{
		ao_width = 0;
		ao_height = 0;
	}
}

void OvrBitmapFontManagerImpl::query_render_text_buffer(int a_id, int& a_size, void* a_buffer)
{
	std::map<int, OvrBitmapFontRenderObject*>::iterator iter = _font_lists.find(a_id);
	if (iter != _font_lists.end())
	{
		OvrBitmapFontRenderObject* t_bitmap_ro = iter->second;
		int tmp_size = t_bitmap_ro->_word_num * 4 * sizeof(font_vertex);
		if (tmp_size != a_size)
		{
			return;
		}
		glBindBuffer(GL_ARRAY_BUFFER, _vbo);
		//uint32_t total_size = _render_words * 4 * sizeof(font_vertex);
		//it is said that the gl hardware will perform better if you are using shared memory gpus
		//so if you change to gles3 libs,do remember to use the win style work.
		//#ifdef LVR_OS_WIN32
		void* mem = glMapBufferRange(GL_ARRAY_BUFFER, t_bitmap_ro->_font_butter_offset, a_size, GL_MAP_READ_BIT);
		memcpy(a_buffer, mem, a_size);
		glUnmapBuffer(GL_ARRAY_BUFFER);
		//#else
		//glBufferSubData(GL_ARRAY_BUFFER, t_bitmap_ro->_font_butter_offset, total_size, a_buffer);
		//#endif
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	if (_font_lists.size() == 0)
	{
		a_size = 0;
	}
}

void OvrBitmapFontManagerImpl::query_render_buffer_size(int a_id, int& a_size)
{
	std::map<int, OvrBitmapFontRenderObject*>::iterator iter = _font_lists.find(a_id);
	if (iter != _font_lists.end())
	{
		OvrBitmapFontRenderObject* t_bitmap_ro = iter->second;
		a_size = t_bitmap_ro->_word_num * 4 * sizeof(font_vertex);
		//glBindBuffer(GL_ARRAY_BUFFER, _vbo);
		//uint32_t total_size = _render_words * 4 * sizeof(font_vertex);
		//it is said that the gl hardware will perform better if you are using shared memory gpus
		//so if you change to gles3 libs,do remember to use the win style work.
		//#ifdef LVR_OS_WIN32
		//void* mem = glMapBufferRange(GL_ARRAY_BUFFER, t_bitmap_ro->_font_butter_offset, a_size, GL_MAP_READ_BIT);
		//memcpy(a_buffer, mem, a_size);
		//glUnmapBuffer(GL_ARRAY_BUFFER);
		//#else
		//glBufferSubData(GL_ARRAY_BUFFER, t_bitmap_ro->_font_butter_offset, total_size, a_buffer);
		//#endif
		//glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	if (_font_lists.size() == 0)
	{
		a_size = 0;
	}
	return ;
}

float OvrBitmapFontManagerImpl::get_center_baseline_leave(int a_char_code)
{
	OvrFontGlyphInfo * t_fgi = _bitmap_font_texture.get_font_glyph_info(a_char_code);
	if (NULL == t_fgi )
	{
		OvrFontGlyph lfg = _font->get_font_glyph(a_char_code);
		t_fgi = _bitmap_font_texture.put_glyph(&lfg);
		//to do ..... should be called inside font impl.
		delete[] lfg.alpha_bits;
		lfg.alpha_bits = 0;
	}
	return t_fgi->bearing_y - t_fgi->height*0.5f;
}

float OvrBitmapFontManagerImpl::get_advance_x(unsigned int char_code)
{
	OvrFontGlyphInfo * t_fgi = NULL;
	if('\n' != char_code && ' ' != char_code )
	{
		t_fgi = _bitmap_font_texture.get_font_glyph_info(char_code);
		if (NULL == t_fgi )
		{
			OvrFontGlyph lfg = _font->get_font_glyph(char_code);
			t_fgi = _bitmap_font_texture.put_glyph(&lfg);
		}
	}
	float res;
	if (t_fgi)
	{
		res = t_fgi->advance_x;
	}
	else
	{
		res = _bitmap_font_texture._half_word_advance_x;
	}

	return res*LVR_FONT_METERS_PER_PIXEL*_font_texture_width;
}

float OvrBitmapFontManagerImpl::get_vertical_space()
{
	return ((float)_font->get_font_size()*1.125f)*LVR_FONT_METERS_PER_PIXEL;
}

static OvrBitmapFontManagerImpl* s_lbfm = 0;
OvrBitmapFontManager* OvrGetBitmapFontManager()
{
	if (0 == s_lbfm)
	{
		s_lbfm = new OvrBitmapFontManagerImpl();
	}
	return (OvrBitmapFontManager*)s_lbfm;
}

void OvrReleaseBitmapFontManager()
{
	if (s_lbfm)
	{
		delete s_lbfm;
		s_lbfm = 0;
	}
}


ovr_engine::OvrBitmapFontTexture* GetFontTex()
{
	if (s_lbfm)
	{
		return s_lbfm->GetBitmapFontTexture();
	}
	else
	{
		return nullptr;
	}
}
