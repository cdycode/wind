#include "wx_engine/ovr_text_component.h"
#include "wx_engine/ovr_render_system.h"
#include "wx_engine/ovr_scene.h"
#include "wx_engine/ovr_material_manager.h"
#include "wx_engine/ovr_material.h"
#include "wx_engine/ovr_vertex_index_data.h"
#include "wx_engine/ovr_texture_manager.h"
#include "wx_engine/ovr_render_queue.h"
#include "wx_engine/ovr_bounding_box.h"
#include "ovr_lvr_ui_text_manager.h"
#include "wx_engine/ovr_camera_component.h"
#include "wx_engine/ovr_material.h"
#include "wx_engine/ovr_texture.h"
#include "wx_engine/ovr_actor.h"
#include "wx_engine/ovr_hardware_buffer_manager.h"
#include "ovr_bitmap_font_manager.h"
//#include "lvr_bitmap_font_manager.h"



ovr_engine::OvrTextComponent::OvrTextComponent(OvrActor* a_actor)
	: OvrPrimitiveComponent(a_actor)
	, flont_spacing(0.0f)
	, line_spacing(0.0f)
	, color(1.0, 1.0, 1.0, 1.0)
	, align_method(eTextAlignLeft | eTextAlignMiddle)
	, align_horizontal(eTextAlignLeft)
	, align_verticle(eTextAlignMiddle)
	, x_margin(0)
	, y_margin(0)
	, x_background_size(1.0)
	, y_background_size(1.0)
	, text_scale(1)
	,render_text(nullptr)
	, wrie_bounding_box(nullptr)
	,transparent_value(1.0)
	,info(nullptr)
{
	info = new OvrTextInfo;
	text_mtl = OvrMaterialManager::GetIns()->Create(parent_actor->GetUniqueName() + "_text_render");
	text_mtl->SetProgram("FontRender");
	text_mtl->GetProgramParameters()->SetNameConstant("Texture0", GetFontTex());
	text_mtl->SetSceneBlend(SBF_SOURCE_ALPHA, SBF_ONE_MINUS_SOURCE_ALPHA);
	text_mtl->SetDepthCheckEnable(true);
	text_mtl->SetDepthWriteEnable(false);
	GenerateTextMesh(50);
	rendering_enabled = true;
	collision_enabled = true;

}

ovr_engine::OvrTextComponent::~OvrTextComponent()
{
	if (wrie_bounding_box)
	{
		delete wrie_bounding_box;
	}
	if (info)
	{
		delete info;
	}
}


void ovr_engine::OvrTextComponent::SetText(const OvrString& a_text)
{
	text = a_text;
}

OvrBox3f ovr_engine::OvrTextComponent::GetBoundingBox()
{
	return bounding_box;
}

void ovr_engine::OvrTextComponent::UpdateRenderQueue(OvrRenderQueue* a_queue)
{
	if (!rendering_enabled)
		return;

	IOvrUiTextManager* ui_text_mgr = parent_actor->GetScene()->GetTextManager();
	OvrQuaternion quat;
	OvrVector3 pos;
	GetWorldTransform().decompose(nullptr, &quat, &pos);
	pos._z += 0.001f;
	(*info).bitmap_font_id = -1;
	(*info).text = text;
	(*info).align_method = (ovr::uint32)align_method;
	(*info).color = color;
	color._w = transparent_value;

	(*info).background_x_scale = x_background_size;
	(*info).background_y_scale = y_background_size;

	(*info).font_spacing = flont_spacing;
	(*info).line_spacing = line_spacing;
	(*info).x_margin = x_margin;
	(*info).y_margin = y_margin;
	(*info).scale = text_scale;
	(*info).pos = pos;
	(*info).right = quat * OvrVector3::UNIT_X;
	(*info).up = quat * OvrVector3::UNIT_Y;
	ui_text_mgr->AddText((*info));

	if (render_text)
	{
		//a_queue->AddRenderable(render_text);
		if (parent_actor->GetShowBoundingBox())
		{
			if (!wrie_bounding_box)
				wrie_bounding_box = new OvrBoundingBox(parent_actor);
			wrie_bounding_box->SetupBoundingBox(bounding_box);
			a_queue->AddRenderable(wrie_bounding_box);
		}
	}

}

int ovr_engine::OvrTextComponent::Raycast(const OvrRay3f& a_ray, float& a_distance)
{
	return GetWorldBoundingBox().ray_interset(a_ray, a_distance);
}

void ovr_engine::OvrTextComponent::SetTextColor(const OvrVector4& a_color)
{
	color = a_color;
}


void ovr_engine::OvrTextComponent::SetTextMargin(const float& a_xMargin, const float& a_yMargin)
{
	x_margin = a_xMargin;
	y_margin = a_yMargin;
}

void ovr_engine::OvrTextComponent::SetAlignMethod(const OvrAlignMethod& a_align_horizontal, const OvrAlignMethod & a_align_vertical)
{
	align_method = a_align_horizontal | a_align_vertical;
	align_horizontal = a_align_horizontal;
	align_verticle = a_align_vertical;
}

void ovr_engine::OvrTextComponent::SetWarpWidth(const float& a_warp_width)
{
	flont_spacing = a_warp_width;
}

void ovr_engine::OvrTextComponent::SetLineWarp(const float& a_line_warp)
{
	line_spacing = a_line_warp;
}


void ovr_engine::OvrTextComponent::SetTextScale(const float& a_scale)
{
	text_scale = a_scale;
}

void ovr_engine::OvrTextComponent::SetBackgroundSize(const float x_size, const float y_size)
{
	x_background_size = x_size;
	y_background_size = y_size;
	parent_actor->SetScale(OvrVector3(x_background_size, y_background_size, 1.0));
	//GenerateBackBoardMesh(x_background_size, y_background_size);
}

void ovr_engine::OvrTextComponent::SetTransparentValue(const float a_transparent)
{
	transparent_value = a_transparent;
	color._w = transparent_value;
}

const OvrString& ovr_engine::OvrTextComponent::GetText() const
{
	return text;
}

const OvrVector4& ovr_engine::OvrTextComponent::GetTextColor() const
{
	return color;
}

void ovr_engine::OvrTextComponent::GetTextMargin(float& a_xMargin, float& a_yMargin)
{
	a_xMargin = x_margin;
	a_yMargin = y_margin;
}

const int ovr_engine::OvrTextComponent::GetAlignHorizontalMethod()const
{
	return align_horizontal;
}

const int ovr_engine::OvrTextComponent::GetAlignVerticleMethod()const
{
	return align_verticle;
}

const float ovr_engine::OvrTextComponent::GetWarpWidth()
{
	return flont_spacing;
}

const float ovr_engine::OvrTextComponent::GetLineWarp() const
{
	return line_spacing;
}

const float ovr_engine::OvrTextComponent::GetTextScale() const
{
	return text_scale;
}


void ovr_engine::OvrTextComponent::GetBackgroundSize(float& x_size, float& y_size)
{
	x_size = x_background_size;
	y_size = y_background_size;
}

float ovr_engine::OvrTextComponent::GetTransparentValue()
{
	return transparent_value;
}

ovr_engine::OvrMaterial* ovr_engine::OvrTextComponent::GetMaterial()
{
	return material;
}

void ovr_engine::OvrTextComponent::GenerateTextMesh(float max_num)
{
	struct  font_vertex
	{
		OvrVector3 pos;
		OvrVector2 texcoord;
		ovr::uint8 rgba[4];
	};

	if (render_text)
	{
		delete render_text;
	}

	font_vertex* text_vertex = new font_vertex[max_num];
	for (int i = 0; i < max_num; ++i)
	{
		text_vertex[i].pos = OvrVector3();
		text_vertex[i].texcoord = OvrVector2();
		for (int j = 0; j < 4; ++j)
		{
			(text_vertex[i].rgba)[j] = 255;
		}
	}
	render_text = new Text(this);
	render_text->vertex_data->declaration->AddElement(0, VET_FLOAT3, VES_POSITION);
	render_text->vertex_data->declaration->AddElement(12, VET_FLOAT2, VES_UV0);
	render_text->vertex_data->declaration->AddElement(20, VET_UBYTE4, VES_COLOR);
	render_text->vertex_data->vertex_buffer = OvrHardwareBufferManager::GetIns()->CreateVertexBuffer(24, max_num, &text_vertex[0]);
	delete[] text_vertex;

	const int index_num = max_num * 6;
	ovr::uint32 * t_indices = new ovr::uint32[index_num];
	int v = 0;
	for (int i = 0; i < max_num; ++i)
	{
		t_indices[i * 6 + 0] = v + 0;
		t_indices[i * 6 + 1] = v + 1;
		t_indices[i * 6 + 2] = v + 2;
		t_indices[i * 6 + 3] = v + 0;
		t_indices[i * 6 + 4] = v + 2;
		t_indices[i * 6 + 5] = v + 3;
		v += 4;
	}
	render_text->index_data->index_buffer = OvrHardwareBufferManager::GetIns()->CreateIndexBuffer(max_num*6, t_indices);
	delete[] t_indices;

	bounding_box.get_min() = OvrVector3(-x_background_size*0.25f, -y_background_size*0.25f, 0.0f);
	bounding_box.get_max() = OvrVector3(x_background_size*0.25f, y_background_size*0.25f, 0.0f);
}

ovr_engine::OvrTextComponent::Text::Text(OvrTextComponent* a_renderer)
{
	text_renderer = a_renderer;
	vertex_data = new OvrVertexData;
	index_data = new OvrIndexData;
}

ovr_engine::OvrTextComponent::Text::~Text()
{
	if (vertex_data)
	{
		delete vertex_data;
		vertex_data = nullptr;
	}
	if (index_data)
	{
		delete index_data;
		index_data = nullptr;
	}
}

OvrMatrix4 ovr_engine::OvrTextComponent::Text::GetWorldTransform() const
{
	return OvrMatrix4();
}

ovr_engine::OvrMaterial* ovr_engine::OvrTextComponent::Text::GetMaterial()
{
	return text_renderer->text_mtl;
}

void ovr_engine::OvrTextComponent::Text::PreRender(OvrScene* scene, OvrRenderSystem* driver)
{
	IOvrUiTextManager* ui_text_mgr = text_renderer->parent_actor->GetScene()->GetTextManager();
	ui_text_mgr->QueryTextId(text_renderer->info);
	int a_id = text_renderer->info->bitmap_font_id;
	if (a_id >= 0)
	{
		int a_size = -1;
		OvrGetBitmapFontManager()->query_render_buffer_size(a_id, a_size);
		if (a_size > 0)
		{
			char* buffer = new char[a_size];
			OvrGetBitmapFontManager()->query_render_text_buffer(a_id, a_size, buffer);
			vertex_data->vertex_buffer->WriteData(a_id, a_size, buffer);
			delete[]buffer;
		}
	}
	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(1.0f, -20.0f);
}

void ovr_engine::OvrTextComponent::Text::PostRender()
{
	glDisable(GL_POLYGON_OFFSET_FILL);
}

void ovr_engine::OvrTextComponent::Text::GetRenderOperation(OvrRenderOperation& a_op)
{
	a_op.use_indices = true;
	a_op.primitive_type = EPT_TRIANGLES;
	a_op.index_data = index_data;
	a_op.vertex_data = vertex_data;
}

float ovr_engine::OvrTextComponent::Text::GetSquaredViewDepth(const OvrCameraComponent* a_cam)
{
	OvrVector3 diff = text_renderer->GetParentActor()->GetWorldPosition() - a_cam->GetCameraPos();
	return diff.length();
}
