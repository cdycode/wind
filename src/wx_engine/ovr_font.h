#ifndef __ovr_font_h__
#define __ovr_font_h__

#include "ovr_font_configure.h"
#include "wx_base/wx_type.h"
//class lvr_multi_polygon2f;


typedef struct OvrAlphaText__
{
	ovr::uint8*	alpha_bits;
	ovr::uint32	width,height;
}OvrAlphaText;


class OvrFontGlyph
{
public:
	OvrFontGlyph()
		:alpha_bits(0)
		,width(0)
		,height(0)
		,advance_x(0)
		,advance_y(0)
		,bearing_x(0)
		,bearing_y(0)
	{}
	void release()
	{
		if (alpha_bits)
		{
			delete[] alpha_bits;
			alpha_bits = 0;
		}
	}
	~OvrFontGlyph()
	{
		
	}
public:
	ovr::int32		char_code;
	ovr::uint8*	alpha_bits;
	ovr::uint32	width,height;
	int			advance_x;
	int			advance_y;
	int			bearing_x;
	int			bearing_y;
};

class OvrFont
{
public:
	OvrFont(){}
	virtual ~OvrFont(){}
public:
	//str in utf8 code.
	virtual void set_font_size(ovr::uint32 a_font_size) = 0;
	virtual ovr::uint32 get_font_size() = 0;
	virtual OvrAlphaText	get_text_alpha(const ovr::uint16* a_str, ovr::uint32 a_len) = 0;
	virtual OvrFontGlyph  get_font_glyph(const ovr::uint16 a_char_code) = 0;
	//virtual bool get_font_outline(lvr_multi_polygon2f* ao_multi_poly,const uint16_t a_char_code) = 0;
};

#endif
