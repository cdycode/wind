﻿#include "wx_engine/ovr_ray_scene_query.h"
#include <algorithm>
#include "wx_engine/ovr_scene.h"
#include "wx_engine/ovr_primitive_component.h"
#include "wx_engine/ovr_actor.h"
using namespace ovr_engine;

OvrRaySceneQuery::OvrRaySceneQuery(OvrScene* a_scene)
{
	scene = a_scene;
}

ovr_engine::OvrDefaultRaySceneQuery::OvrDefaultRaySceneQuery(OvrScene* a_scene)
	:OvrRaySceneQuery(a_scene)
{

}

RayIntersetResults& ovr_engine::OvrDefaultRaySceneQuery::Execute(bool a_force /*= false*/)
{
	mResult.clear();
	OvrScene::ComponentCollectionMap::iterator collection_cur = scene->GetComponentCollectionMap().begin();
	OvrScene::ComponentCollectionMap::iterator collection_end = scene->GetComponentCollectionMap().end();

	while (collection_cur != collection_end)
	{
		OvrScene::ComponentCollection* components = collection_cur->second;
		OvrScene::ComponentCollection::iterator component_cur = components->begin();
		OvrScene::ComponentCollection::iterator component_end = components->end();
		for (; component_cur != component_end; component_cur++)
		{
			OvrPrimitiveComponent* com = dynamic_cast<OvrPrimitiveComponent*>(*component_cur);
			if (!com)
				break;

			if (!com->IsRealActive())
				continue;

			if (com->GetCollisionEnabled() || a_force)
			{
				float distance;
				if (com->Raycast(ray, distance))
				{
					OvrRayIntersetResult ret;
					ret.component = com;
					ret.distance = distance;
					mResult.push_back(ret);
				}
			}
		}
		collection_cur++;
	}

	if (sort_by_distance)
	{
		std::sort(mResult.begin(), mResult.end());
	}
	return mResult;
}