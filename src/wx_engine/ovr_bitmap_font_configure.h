#ifndef __ovr_bitmap_font_configure_h__
#define __ovr_bitmap_font_configure_h__

//#include "lvr_core_lib.h"
//#include "lvr_math_lib.h"
//#include "lvr_render_lib.h"
#include "ovr_font_lib.h"




enum OvrEnumFontAlignMethod
{
	ovr_e_font_align_left,
	ovr_e_font_align_middle,
	ovr_e_font_align_right
};

#endif
