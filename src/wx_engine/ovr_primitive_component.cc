#include "wx_engine/ovr_primitive_component.h"
#include "wx_engine/ovr_actor.h"
#include "wx_base/wx_box3f.h"
using namespace ovr_engine;
ovr_engine::OvrPrimitiveComponent::OvrPrimitiveComponent(OvrActor* a_actor)
	: OvrComponent(a_actor)
	, rendering_enabled(false)
	, collision_enabled(false)
{

}

ovr_engine::OvrPrimitiveComponent::~OvrPrimitiveComponent()
{
}

OvrBox3f ovr_engine::OvrPrimitiveComponent::GetWorldBoundingBox()
{
	OvrMatrix4 world_matrix = GetWorldTransform();
	world_aabb3 = GetBoundingBox();
	world_aabb3.transform_affine(world_matrix);
	return world_aabb3;
}

void ovr_engine::OvrPrimitiveComponent::SetCollisionEnabled(bool a_enable)
{
	collision_enabled = a_enable;
}

bool ovr_engine::OvrPrimitiveComponent::GetCollisionEnabled() const
{
	return collision_enabled;
}

void ovr_engine::OvrPrimitiveComponent::SetRenderingEnabled(bool a_enable)
{
	rendering_enabled = a_enable;
}

bool ovr_engine::OvrPrimitiveComponent::GetRenderingEnabled() const
{
	return rendering_enabled;
}
