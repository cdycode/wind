﻿#include <assert.h>
#include "wx_engine/ovr_gpu_program_constants.h"
#include "wx_engine/ovr_light_component.h"
#include "wx_engine/ovr_camera_component.h"
using namespace ovr_engine;

ovr_engine::OvrAutoConstant::OvrAutoConstant(OvrAutoConstantType a_type, ovr::uint16 a_mask, ovr::uint32 a_index)
	: paramType(a_type)
	, variability(a_mask)
	, physicalIndex(a_index)
{

}

bool ovr_engine::OvrConstant::IsFloat() const
{
	return OvrConstant::IsFloat(const_type);
}

bool ovr_engine::OvrConstant::IsInt() const
{
	return OvrConstant::IsInt(const_type);
}

bool ovr_engine::OvrConstant::IsSampler() const
{
	return IsSampler(const_type);
}

ovr::uint32 ovr_engine::OvrConstant::GetElementSize() const
{
	return GetElementSize(const_type);
}

ovr::uint32 ovr_engine::OvrConstant::GetElementSize(OvrGpuProgramConstantType ctype)
{
	switch (ctype)
	{
	case GCT_FLOAT1:
	case GCT_INT1:
	case GCT_SAMPLER2D:
	case GCT_SAMPLER_BUFFER:
	case GCT_SAMPLER2D_ARRAY:
	case GCT_SAMPLER_CUBE:
	case GCT_SAMPLER_CUBE_ARRAY:
		return 1;
	case GCT_VEC3:
		return 3;
	case GCT_VEC4:
	case GCT_INT4:
		return 4;
	case GCT_MAT3:
		return 9;
	case GCT_MAT4:
		return 16;
	default:
		return 1;
	};
}

bool ovr_engine::OvrConstant::IsSampler(OvrGpuProgramConstantType c)
{
	switch (c)
	{
	case GCT_SAMPLER2D:
	case GCT_SAMPLER_BUFFER:
	case GCT_SAMPLER2D_ARRAY:
	case GCT_SAMPLER_CUBE:
	case GCT_SAMPLER_CUBE_ARRAY:
		return true;
	default:
		return false;
	};
}

bool ovr_engine::OvrConstant::IsInt(OvrGpuProgramConstantType c)
{
	switch (c)
	{
	case GCT_INT1:
	case GCT_INT4:
		return true;
	default:
		return false;
	};
}

bool ovr_engine::OvrConstant::IsFloat(OvrGpuProgramConstantType c)
{
	switch (c)
	{
	case GCT_FLOAT1:
	case GCT_VEC3:
	case GCT_VEC4:
	case GCT_MAT3:
	case GCT_MAT4:
		return true;
	default:
		return false;
	};
}

ovr_engine::OvrAutoConstantDefinition::OvrAutoConstantDefinition(OvrAutoConstantType a_ac_type, OvrString a_name, 
	OvrGpuProgramConstantType a_type, ovr::uint16 a_variability)
	: ac_type(a_ac_type)
	, name(a_name)
	, const_type(a_type)
	, variability(a_variability)
{
}

OvrAutoConstantDefinition OvrGpuProgramConstants::auto_constant_dictionary[] = {
	OvrAutoConstantDefinition(ACT_MVP_MAT, "Mvpm", GCT_MAT4, GPV_PER_OBJECT),
	OvrAutoConstantDefinition(ACT_MODEL_MAT, "Modelm", GCT_MAT4, GPV_PER_OBJECT),
	OvrAutoConstantDefinition(ACT_VIEW_MAT, "Viewm", GCT_MAT4, GPV_GLOBAL),
	OvrAutoConstantDefinition(ACT_PROJ_MAT, "Projectionm", GCT_MAT4, GPV_GLOBAL),
	OvrAutoConstantDefinition(ACT_TEXM0_MAT, "Texm", GCT_MAT4, GPV_GLOBAL),
	OvrAutoConstantDefinition(ACT_TEXM1_MAT, "Texm2", GCT_MAT4, GPV_GLOBAL),
	OvrAutoConstantDefinition(ACT_JOINTS, "Joints", GCT_MAT4,GPV_PER_OBJECT),
	OvrAutoConstantDefinition(ACT_DIRECTION_LIGHT_COLOR, "DirectionLightColorIntensity", GCT_VEC4, GPV_LIGHTS),
	OvrAutoConstantDefinition(ACT_DIRECTION_LIGHT_DIR, "DirectionLightDir", GCT_VEC3, GPV_LIGHTS),
	OvrAutoConstantDefinition(ACT_DIRECTION_LIGHT_VP_MAT, "DirectionLightVPMat", GCT_MAT4, GPV_LIGHTS),
	OvrAutoConstantDefinition(ACT_ENV_LIGHT_COLOR, "EnvLightColorIntensity", GCT_VEC4, GPV_GLOBAL),
	OvrAutoConstantDefinition(ACT_EYE_POSTION, "EyePosition", GCT_VEC3, GPV_GLOBAL),
	OvrAutoConstantDefinition(ACT_POINT_LIGHT_COLOR, "PointLightColorIntensity", GCT_VEC4, GPV_LIGHTS),
	OvrAutoConstantDefinition(ACT_POINT_LIGHT_POS_RANGE, "PointLightPosRadius", GCT_VEC4, GPV_LIGHTS),
	OvrAutoConstantDefinition(ACT_POINT_LIGHT_RADIUS_FOR_SHADOW, "PointLightRange", GCT_FLOAT1, GPV_LIGHTS),
	OvrAutoConstantDefinition(ACT_LIGHT_NUM, "light_num", GCT_INT4, GPV_LIGHTS),
	OvrAutoConstantDefinition(ACT_SPOT_LIGHT_COLOR, "SpotLightColorIntensity", GCT_VEC4, GPV_LIGHTS),
	OvrAutoConstantDefinition(ACT_SPOT_LIGHT_POS_EXP, "SpotLightPosExponent", GCT_VEC4, GPV_LIGHTS),
	OvrAutoConstantDefinition(ACT_SPOT_LIGHT_DIR_CUT, "SpotLightDirCutOff", GCT_VEC4, GPV_LIGHTS),
	OvrAutoConstantDefinition(ACT_SPOT_LIGHT_VP_MAT, "SpotLightVPMat", GCT_MAT4, GPV_LIGHTS),
	OvrAutoConstantDefinition(ACT_SPOT_SHADOW_MAP, "Texture5", GCT_SAMPLER2D_ARRAY, GPV_LIGHTS),
	OvrAutoConstantDefinition(ACT_DIRECTION_SHADOW_MAP, "Texture6", GCT_SAMPLER2D, GPV_LIGHTS),
	OvrAutoConstantDefinition(ACT_POINT_SHADOW_MAP, "Texture7", GCT_SAMPLER_CUBE_ARRAY, GPV_LIGHTS),
};

ovr_engine::OvrGpuProgramConstants::OvrGpuProgramConstants()
	:float_size(0)
	,int_size(0)
	,tex_size(0)
{

}

ovr_engine::OvrGpuProgramConstants::~OvrGpuProgramConstants()
{

}

const OvrAutoConstantDefinition* ovr_engine::OvrGpuProgramConstants::FindAutoConstantDefinition(const OvrString& name)
{
	ovr::uint32 num = sizeof(auto_constant_dictionary) / sizeof(OvrAutoConstantDefinition);
	for (ovr::uint32 i = 0; i < num; i++)
	{
		if (name == auto_constant_dictionary[i].name)
		{
			return &auto_constant_dictionary[i];
		}
	}
	return nullptr;
}

ovr::uint32 ovr_engine::OvrGpuProgramConstants::GetAutoConstantCount()
{
	return auto_constant_list.size();
}

const OvrAutoConstant* ovr_engine::OvrGpuProgramConstants::GetAutoConstant(const OvrAutoConstantType& a_type) const
{
	for (ovr::uint32 i = 0; i < auto_constant_list.size(); i++)
	{
		if(auto_constant_list[i].paramType == a_type)
		{
			return &auto_constant_list[i];
		}
	}
	return nullptr;
}

const AutoConstantList* ovr_engine::OvrGpuProgramConstants::GetAutoConstantList() const
{
	return &auto_constant_list;
}

ovr::uint32 ovr_engine::OvrGpuProgramConstants::GetConstantCount()
{
	return constant_map.size();
}

const OvrConstant* ovr_engine::OvrGpuProgramConstants::AddConstant(OvrString a_name, OvrGpuProgramConstantType a_type, ovr::uint32 a_size)
{
	OvrConstant def;
	def.gpu_name = a_name;
	def.const_type = a_type;
	def.array_size = a_size;
	def.variability = GPV_GLOBAL;
	def.element_size = OvrConstant::GetElementSize(a_type);

	if (def.IsFloat())
	{
		def.physicalIndex = float_size;
		float_size += def.array_size * def.element_size;
		
	}
	else if (def.IsInt())
	{
		def.physicalIndex = int_size;
		int_size += def.array_size * def.element_size;
	}
	else if (def.IsSampler())
	{
		def.physicalIndex = tex_size;
		tex_size += def.array_size * def.element_size;
	}

	const OvrAutoConstantDefinition* auto_def = FindAutoConstantDefinition(a_name);
	if (auto_def)
	{
		assert(auto_def->const_type == def.const_type);
		auto_constant_list.push_back(OvrAutoConstant(
			auto_def->ac_type,
			auto_def->variability,
			def.physicalIndex
		));
		def.variability = auto_def->variability;
	}
	else
	{
		custom_constant_list.push_back(def);
	}
	constant_map[a_name] = def;
	return &constant_map[a_name];
}

const OvrAutoConstant* ovr_engine::OvrGpuProgramConstants::GetAutoConstant(ovr::uint32 a_index)
{
	if (a_index < auto_constant_list.size())
	{
		return &auto_constant_list[a_index];
	}
	return nullptr;
}

const OvrConstant* ovr_engine::OvrGpuProgramConstants::GetNameConstant(const OvrString& a_name)
{
	ConstantMap::const_iterator it = constant_map.find(a_name);
	if (it != constant_map.end())
	{
		return &(it->second);
	}
	return nullptr;
}

const ConstantList* ovr_engine::OvrGpuProgramConstants::GetCustomConstantList() const
{
	return &custom_constant_list;
}

void ovr_engine::OvrAutoParamSource::SetCurrentLightList(std::vector<OvrLightComponent*>& a_lights)
{
	for (ovr::uint32 i = 0; i < 4; i++)
	{
		light_nums[i] = 0;
	}

	for (ovr::uint32 i = 0; i < a_lights.size(); i++)
	{
		OvrLightComponent* light = a_lights[i];
		if (light->GetLightType() == eDirectionLight)
		{
			direction_light_color_intensity = light->GetDiffuseColor();
			direction_light_color_intensity[3] = light->GetIntensity();
			direction_light_dir = light->GetDirection();
			direction_light_dir.normalize();
			direction_light_vp_mat = light->GetViewMat() * light->GetProjMat();
			light_nums[0]++;
		}
		else if (light->GetLightType() == ePointLight)
		{
			int curr_point_light_index = light_nums[2];
			point_light_color_intensity[curr_point_light_index] = light->GetDiffuseColor();
			point_light_color_intensity[curr_point_light_index][3] = light->GetIntensity();
			point_light_position_radius[curr_point_light_index] = OvrVector4(light->GetPosition(), light->GetRange());
			light_nums[2]++;
		}
		else if (a_lights[i]->GetLightType() == eSpotLight)
		{
			int curr_spot_light_index = light_nums[1];
			spot_light_color_intensity[curr_spot_light_index] = light->GetDiffuseColor();
			spot_light_color_intensity[curr_spot_light_index][3] = light->GetIntensity();
			spot_light_position_exponent[curr_spot_light_index] = OvrVector4(light->GetPosition(), light->GetExp());
			spot_light_dir_cutoff[curr_spot_light_index] = OvrVector4(light->GetDirection(), light->GetFalloff());
			spot_light_vp_mat[curr_spot_light_index] = light->GetViewMat() * light->GetProjMat();
			light_nums[1] ++;
		}
		light_nums[3]++;
	}

}

void ovr_engine::OvrAutoParamSource::SetCurrentCamera(OvrCameraComponent* a_camera)
{
	if (a_camera)
	{
		view_matrix = a_camera->GetViewMatrix();
		projection_matrix = a_camera->GetProjectMatrix();
		eye_pos = a_camera->GetCameraPos();
	}
}
