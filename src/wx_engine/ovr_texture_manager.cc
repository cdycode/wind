#include "wx_engine/ovr_texture_manager.h"
#include "wx_engine/ovr_serializer_impl.h"
#include "wx_engine/ovr_media_texture.h"
#include "wx_engine/ovr_logger.h"
#include "lvr_texture_manager.h"
#include "wx_base/wx_log.h"
#include "ovr_gl_texture.h"
using namespace ovr_engine;
template<> OvrTextureManager* ovr_engine::OvrSingleton<OvrTextureManager>::s_instance = nullptr;

static uint8_t defaultTransparentTexture[4 * 4 * 4] =
{
	255,255,255,0, 255,255,255,0, 255,255,255,0,  255,255,255,0,
	255,255,255,0, 255,255,255,0, 255,255,255,0,  255,255,255,0,
	255,255,255,0, 255,255,255,0, 255,255,255,0,  255,255,255,0,
	255,255,255,0, 255,255,255,0, 255,255,255,0,  255,255,255,0
};

static uint8_t defaultBaseWhiteTexture[4 * 4 * 4] =
{
	255,255,255,255, 255,255,255,255, 255,255,255,255,  255,255,255,255,
	255,255,255,255, 255,255,255,255, 255,255,255,255,  255,255,255,255,
	255,255,255,255, 255,255,255,255, 255,255,255,255,  255,255,255,0,
	255,255,255,255, 255,255,255,255, 255,255,255,255,  255,255,255,0
};

ovr_engine::OvrTextureManager::OvrTextureManager()
	: base_white(nullptr)
	, base_transparent(nullptr)
{

}

ovr_engine::OvrTextureManager::~OvrTextureManager()
{
	UnInitialise();
}

bool ovr_engine::OvrTextureManager::Initialise()
{

	base_white = Create("BaseWhite", TEXTURE_TYPE_2D, 4, 4, PF_R8G8B8A8, 0, false);
	base_white->BlitFromMemory(defaultBaseWhiteTexture, 64);
	base_transparent = Create("BaseTransparent", TEXTURE_TYPE_2D, 4, 4, PF_R8G8B8A8, 0, false);
	base_white->BlitFromMemory(defaultTransparentTexture, 64);
	return true;
}

void ovr_engine::OvrTextureManager::UnInitialise()
{
	base_white = nullptr;
	base_transparent = nullptr;
	RemoveAll();
	lvr_texture_manager::release_texture_manager();
}

OvrTexture* ovr_engine::OvrTextureManager::Load(const OvrString& a_file_name)
{
	OvrLogD("ovrengine_debug","Load Texture %s",a_file_name.GetStringConst());
	OvrTexture* texture = (OvrTexture*)CreateOrRetrieve(a_file_name, false);
	texture->Load();
	return texture;
}


OvrTexture* ovr_engine::OvrTextureManager::Create(const OvrString& a_name, OvrTextureType a_type, int a_width, int a_height, OvrPixelFormat a_format, int a_depth, bool is_rtt /*= false*/)
{
	OvrGLTexture* gl_texture = new OvrGLTexture(a_name, true);
	gl_texture->SetTextureType(a_type);
	gl_texture->SetWidth(a_width);
	gl_texture->SetHeight(a_height);
	gl_texture->SetDepth(a_depth);
	gl_texture->SetFormat(a_format);
	gl_texture->SetRenderTarget(is_rtt);
	gl_texture->CreateInternalResources();
	return gl_texture;
}

OvrResource* ovr_engine::OvrTextureManager::CreateResource(OvrString a_name, bool is_manual)
{
	return new OvrGLTexture(a_name, is_manual);
}

OvrTexture* ovr_engine::OvrTextureManager::GetDefaultTexture()
{
	return base_white;
}

OvrTexture* ovr_engine::OvrTextureManager::GetDefaultTransparentTexture()
{
	return base_transparent;
}
