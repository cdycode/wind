#pragma once
#include <assert.h>
#include "ovr_gl_render_system.h"
#include "lvr_vertex_buffer.h"
#include "lvr_index_buffer.h"
#include "lvr_vertex_format.h"
#include "lvr_texturebuffer.h"
#include "lvr_shader_manager.h"
//#include "lvr_bitmap_font_manager.h"
//#include "lvr_bitmap_font_lib.h"
//#include "lvr_ui_manager.h"
#include "wx_engine/ovr_vertex_index_data.h"
#include "ovr_gl_hardware_buffer.h"
#include "ovr_gl_vertex_array_object.h"
#include "ovr_shader_string.h"
#include "wx_engine/ovr_logger.h"
#include "wx_engine/ovr_gpu_program.h"
#include "ovr_gl_define.h"
#include "ovr_gl_texture.h"
using namespace ovr_engine;
OvrGLRenderSystem::OvrGLRenderSystem()
{
}

ovr_engine::OvrGLRenderSystem::~OvrGLRenderSystem()
{
}

void ovr_engine::OvrGLRenderSystem::UnInitialise()
{
}

bool ovr_engine::OvrGLRenderSystem::EndScene()
{
	if (program)
		program->UnBindProgram();
	program = nullptr;
	return true;
}

void ovr_engine::OvrGLRenderSystem::SetViewPort(const Viewport& a_v)
{
	glViewport(a_v.x, a_v.y, a_v.x_size, a_v.y_size);
	OvrRenderSystem::SetViewPort(a_v);
}

void ovr_engine::OvrGLRenderSystem::BindGpuProgram(OvrGpuProgram* a_program)
{
	OvrRenderSystem::BindGpuProgram(a_program);
	if(program)
		program->BindProgram();
}

void ovr_engine::OvrGLRenderSystem::UnBindGpuProgram()
{
	if(program)
		program->UnBindProgram();
}

void ovr_engine::OvrGLRenderSystem::BindGpuProgramParameters(OvrGpuProgramParameters* params, ovr::uint16 mask)
{
	if (program)
		program->BindProgramParameters(params, mask);
}

void ovr_engine::OvrGLRenderSystem::SetSceneBlend(SceneBlendFactor a_src, SceneBlendFactor a_dest)
{
	if (a_src == SBF_ONE && a_dest == SBF_ZERO)
	{
		glDisable(GL_BLEND);
	}
	else
	{
		GLint srcfunc = GetBlendMode(a_src);
		GLint destfunc = GetBlendMode(a_dest);
		glBlendFunc(srcfunc, destfunc);
		glEnable(GL_BLEND);
	}
}

void ovr_engine::OvrGLRenderSystem::SetDepthCheckEnable(bool a_enable)
{
	if (a_enable)
		glEnable(GL_DEPTH_TEST);
	else
		glDisable(GL_DEPTH_TEST);
}

void ovr_engine::OvrGLRenderSystem::SetDepthWriteEnable(bool a_enable)
{
	glDepthMask(a_enable);
}

void ovr_engine::OvrGLRenderSystem::SetCullMode(CullingMode a_mode)
{
	if (a_mode == CULL_NONE)
	{
		glDisable(GL_CULL_FACE);
	}
	else
	{
		glEnable(GL_CULL_FACE);
		if (a_mode == CULL_FRONT)
		{
			glCullFace(GL_FRONT);
		}
		else
		{
			glCullFace(GL_BACK);
		}
	}
}

void ovr_engine::OvrGLRenderSystem::ClearBuffers(bool backBuffer, bool zBuffer, bool stencilBuffer, OvrColorValue color)
{

	GLbitfield mask = 0;
	if (backBuffer)
	{
		glClearColor(color[0], color[1], color[2], color[3]);
		mask |= GL_COLOR_BUFFER_BIT;
	}

	if (zBuffer)
	{
		glDepthMask(GL_TRUE);
		glClearDepthf(1.0f);
		glDepthFunc(GL_LEQUAL);
		mask |= GL_DEPTH_BUFFER_BIT;
	}

	if (stencilBuffer)
		mask |= GL_STENCIL_BUFFER_BIT;

	if (mask)
		glClear(mask);
}


GLint ovr_engine::OvrGLRenderSystem::GetBlendMode(SceneBlendFactor a_factor)
{
	switch (a_factor)
	{
	case SBF_ONE:
		return GL_ONE;
	case SBF_ZERO:
		return GL_ZERO;
	case SBF_DEST_COLOUR:
		return GL_DST_COLOR;
	case SBF_SOURCE_COLOUR:
		return GL_SRC_COLOR;
	case SBF_ONE_MINUS_DEST_COLOUR:
		return GL_ONE_MINUS_DST_COLOR;
	case SBF_ONE_MINUS_SOURCE_COLOUR:
		return GL_ONE_MINUS_SRC_COLOR;
	case SBF_DEST_ALPHA:
		return GL_DST_ALPHA;
	case SBF_SOURCE_ALPHA:
		return GL_SRC_ALPHA;
	case SBF_ONE_MINUS_DEST_ALPHA:
		return GL_ONE_MINUS_DST_ALPHA;
	case SBF_ONE_MINUS_SOURCE_ALPHA:
		return GL_ONE_MINUS_SRC_ALPHA;
	};
	// to keep compiler happy
	return GL_ONE;
}
void ovr_engine::OvrGLRenderSystem::Draw3DLine(const std::vector<OvrVector3>& a_vertices, const OvrMatrix4& a_view, const OvrMatrix4& a_proj, const float a_width, const OvrColorValue a_color, bool is_strip)
{

#ifdef OVR_OS_WIN32
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(&a_view[0]);
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(&a_proj[0]);
	glLineWidth(a_width);
	glDisable(GL_BLEND);
	if (is_strip)
		glBegin(GL_LINE_STRIP);
	else
		glBegin(GL_LINES);
	glColor4f(a_color[0], a_color[1], a_color[2], a_color[3]);
	for (ovr::uint32 i = 0; i < a_vertices.size(); i++)
	{
		glVertex3f(a_vertices[i]._x, a_vertices[i]._y, a_vertices[i]._z);
	}
	
	glEnd();
#endif
}

void ovr_engine::OvrGLRenderSystem::Draw3DPoint(const std::vector<OvrVector3>& a_vertices, const OvrMatrix4& a_view, const OvrMatrix4& a_proj, const float a_size, const OvrColorValue a_color /*= OvrColorValue(255, 255, 255, 255)*/)
{
#ifdef OVR_OS_WIN32
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(&a_view[0]);
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(&a_proj[0]);
	glPointSize(a_size);
	glDisable(GL_BLEND);
	
	glBegin(GL_POINTS);
	glColor4f(a_color[0], a_color[1], a_color[2], a_color[3]);
	for (ovr::uint32 i = 0; i < a_vertices.size(); i++)
	{
		glVertex3f(a_vertices[i]._x, a_vertices[i]._y, a_vertices[i]._z);
	}

	glEnd();
#endif
}
bool OvrGLRenderSystem::Initialise()
{
#if defined(OVR_OS_WIN32) 
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		OvrLogger::GetIns()->Log("glew init failed.", LL_ERROR);
		return false;
	}
	OvrLogger::GetIns()->Log(OvrString("glew version is ") + OvrString((char*)glewGetString(GLEW_VERSION)), LL_INFORMATION);
#endif

	return true;
}

bool OvrGLRenderSystem::BeginScene(bool a_backBuffer, bool a_zBuffer, OvrColorValue a_color, Viewport a_rect)
{
#if defined(OVR_OS_WIN32)
	glEnable(GL_MULTISAMPLE);
#endif
	SetViewPort(a_rect);
	ClearBuffers(a_backBuffer, a_zBuffer, false, a_color);
	return true;
}

void OvrGLRenderSystem::Render(OvrRenderOperation& a_op)
{

	OvrGLVertexArrayObject* vao = 
		static_cast<OvrGLVertexArrayObject*>(a_op.vertex_data->declaration);

	vao->Bind();
	bool updateVAO = vao->NeedUpdate();

	if (updateVAO)
	{
		vao->BindToGpu((OvrGLHardwareVertexBuffer*)a_op.vertex_data->vertex_buffer);
	}

	if (a_op.use_indices)
	{
		GLuint ibo = static_cast<OvrGLHardwareIndexBuffer*>(a_op.index_data->index_buffer)->GetBufferId();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	}

	GLint primType = GL_TRIANGLES;
	switch (a_op.primitive_type)
	{
	case EPT_TRIANGLES :
		primType = GL_TRIANGLES;
		break;
	case EPT_TRIANGLE_FAN:
		primType = GL_TRIANGLE_FAN;
		break;
	case EPT_TRIANGLE_STRIP :
		primType = GL_TRIANGLE_STRIP;
		break;
	case EPT_LINES :
		primType = GL_LINES;
		break;
	case EPT_LINE_STRIP :
		primType = GL_LINE_STRIP;
		break;
	case EPT_POINTS :
		primType = GL_POINTS;
		break;
	}

	if (a_op.use_indices)
	{
		glDrawElements(primType,
			a_op.index_data->index_buffer->GetIndexCount(),
			GL_UNSIGNED_INT,
			0);
	}
	else
	{
		glDrawArrays(primType,
			0,
			a_op.vertex_data->vertex_buffer->GetVertexCount());
	}
	vao->UnBind();
}