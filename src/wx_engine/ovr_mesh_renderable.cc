#include "wx_engine/ovr_mesh_renderable.h"
#include "wx_engine/ovr_skinned_mesh_component.h"
#include "wx_engine/ovr_static_mesh_component.h"
#include "wx_engine/ovr_actor.h"
#include "wx_engine/ovr_skeleton_instance.h"
#include "wx_engine/ovr_camera_component.h"
#include "wx_engine/ovr_pose.h"
#include "wx_engine/ovr_mesh_buffer.h"
#include "wx_engine/ovr_mesh.h"
#include "wx_engine/ovr_hardware_buffer_manager.h"
#include "wx_engine/ovr_vertex_index_data.h"
using namespace ovr_engine;
OvrMeshRenderable::OvrMeshRenderable(OvrMeshComponent* a_mesh_component, OvrMeshBuffer* a_mesh_buf, ovr::uint32 a_index)
	: mesh_component(a_mesh_component)
	, mesh_buffer(a_mesh_buf)
	, material_index(a_index)
{
	OvrMesh* mesh = mesh_buffer->GetParent();
	for (ovr::uint32 i = 0; i < mesh->GetPoseCount(); i++)
	{
		OvrString buf_name = mesh->GetPose(i)->GetTargetName();
		if (mesh->GetMeshBuffer(buf_name) == mesh_buffer)
		{
			pose_index.push_back(i);
			pose_weight.push_back(0);
		}
	}
}

OvrMeshRenderable::~OvrMeshRenderable()
{
}

OvrMatrix4 ovr_engine::OvrMeshRenderable::GetWorldTransform() const
{
	return mesh_component->GetWorldTransform();
}



float OvrMeshRenderable::GetSquaredViewDepth(const OvrCameraComponent* a_cam)
{
	OvrVector3 diff = mesh_component->GetParentActor()->GetWorldPosition() - a_cam->GetCameraPos();
	return diff.length();
}

void OvrMeshRenderable::UpdateAnimation(ovr_asset::OvrMorphFrame* a_frame)
{
	ovr::uint32 pose_num = pose_index.size();
	if (pose_num <= 0)
		return;

	std::vector<float> weight;
	weight.resize(pose_num);
	memset(&weight[0], 0, pose_num * sizeof(float));

	for (ovr::uint32 i = 0; i < a_frame->GetPoseNum(); i++)
	{
		ovr_asset::OvrPoseFrame* pose_frame = a_frame->GetPoseFrame(i);
		ovr::uint32 j = 0;
		for (; j < pose_index.size(); j++)
		{
			if (pose_frame->pose_index == pose_index[j])
			{
				break;
			}
		}
		if (j < pose_index.size())
		{
			weight[j] = pose_frame->pose_weight - pose_weight[j];
			pose_weight[j] = pose_frame->pose_weight;
		}
	}

	OvrVertexData* vertex_data = mesh_buffer->vertex_data;
	char* vbuf = (char*)(vertex_data->vertex_buffer->Lock(0));
	if (!vbuf)
	{
		return;
	}

	ovr::uint32 voffset = 0;
	ovr::uint32 vstride = vertex_data->vertex_buffer->GetVertexSize();
	for (ovr::uint32 i = 0; i < pose_index.size(); i++)
	{
		OvrPose* pose = mesh_buffer->GetParent()->GetPose(pose_index[i]);
		const std::map<ovr::uint32, OvrVector3>& vertex_offset_map = pose->GetVertexOffset();
		auto offset_cur = vertex_offset_map.begin();
		while (offset_cur != vertex_offset_map.end())
		{
			if (offset_cur->first < vertex_data->vertex_buffer->GetVertexCount())
			{
				OvrVector3* pos = (OvrVector3*)(vbuf + offset_cur->first * vstride + voffset);
				pos->_x += offset_cur->second[0] * weight[i];
				pos->_y += offset_cur->second[1] * weight[i];
				pos->_z += offset_cur->second[2] * weight[i];
			}
			offset_cur++;
		}
	}
	vertex_data->vertex_buffer->Unlock();
	return;
}

OvrMaterial* OvrMeshRenderable::GetMaterial()
{
	return mesh_component->GetMaterial(material_index);
}

bool OvrMeshRenderable::GetCastShadowEnable()
{
	return mesh_component->GetCastShadow();
}

void OvrMeshRenderable::SetMaterialIndex(const ovr::uint32& a_index)
{
	material_index = a_index;
}


void ovr_engine::OvrMeshRenderable::GetRenderOperation(OvrRenderOperation& a_op)
{
	a_op.index_data = mesh_buffer->index_data;
	a_op.vertex_data = mesh_buffer->vertex_data;
	a_op.primitive_type = mesh_buffer->GetPrimitiveType();
	a_op.use_indices = true;
}

OvrMatrix4* OvrMeshRenderable::GetSkeletonMatrix(ovr::uint32& a_num) const
{
	if (mesh_component->GetType() == kComponentSkinnedMesh)
	{
		OvrSkeletonInstance* skeletal = static_cast<OvrSkinnedMeshComponent*>(mesh_component)->GetSkeletonInstance();
		if (skeletal)
		{
			a_num = skeletal->GetJointNum();
			return skeletal->GetSkeletonMatrix();
		}
	}
	return nullptr;
}



