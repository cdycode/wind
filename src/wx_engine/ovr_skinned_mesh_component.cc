#include "wx_engine/ovr_skinned_mesh_component.h"
#include "wx_engine/ovr_skeleton_instance.h"
#include "wx_engine/ovr_mesh.h"
#include "wx_engine/ovr_logger.h"
#include "wx_engine/ovr_material.h"
#include "wx_engine/ovr_material_manager.h"
#include "wx_engine/ovr_bounding_box.h"
#include "wx_engine/ovr_actor.h"
using namespace ovr_engine;

ovr_engine::OvrSkinnedMeshComponent::OvrSkinnedMeshComponent(OvrActor* a_actor)
	:OvrMeshComponent(a_actor)
	,skeleton_instance(nullptr)
{

}

ovr_engine::OvrSkinnedMeshComponent::~OvrSkinnedMeshComponent()
{
	if (skeleton_instance)
		skeleton_instance->Drop();

	ResetRenderableList();
}

void ovr_engine::OvrSkinnedMeshComponent::Update()
{

}

OvrBox3f ovr_engine::OvrSkinnedMeshComponent::GetWorldBoundingBox()
{
	OvrMatrix4 world_matrix = GetWorldTransform();
	if (skeleton_instance)
	{
		OvrMatrix4 root_bone_matrix = *skeleton_instance->GetJointMatrix(0);
		world_matrix = root_bone_matrix * world_matrix;
	}
	world_aabb3 = GetBoundingBox();
	world_aabb3.transform_affine(world_matrix);
	return world_aabb3;
}

void ovr_engine::OvrSkinnedMeshComponent::SetMesh(ovr_engine::OvrMesh* a_mesh)
{
	if (!a_mesh)
		return;

	a_mesh->Grab();

	if (mesh)
		mesh->Drop();

	mesh = a_mesh;

	if (skeleton_instance)
		skeleton_instance->Drop();

	OvrSkeleton* skeleton = mesh->GetSkeleton();
	if (skeleton)
	{
		skeleton_instance = new OvrSkeletonInstance(skeleton);
		skeleton_instance->Grab();
	}

	BuildRenderableList();

	mesh_state_count = a_mesh->GetStateCount();
}

OvrSkeletonInstance* ovr_engine::OvrSkinnedMeshComponent::GetSkeletonInstance()
{
	return skeleton_instance;
}

bool ovr_engine::OvrSkinnedMeshComponent::UpdateAnimation(ovr_asset::OvrMotionFrame* a_frame)
{
	if (nullptr == a_frame || nullptr == skeleton_instance)
	{
		OvrLogger::GetIns()->Log("motion frame is null or mesh actor has not skeletal.", LL_WARNING);
		return false;
	}

	skeleton_instance->UpdateAnimation(a_frame);
	return true;
}