#include "wx_engine/ovr_image_component.h"
#include "wx_engine/ovr_render_system.h"
#include "wx_engine/ovr_scene.h"
#include "wx_engine/ovr_material_manager.h"
#include "wx_engine/ovr_material.h"
#include "wx_engine/ovr_vertex_index_data.h"
#include "wx_engine/ovr_texture_manager.h"
#include "wx_engine/ovr_render_queue.h"
#include "wx_engine/ovr_bounding_box.h"
#include "wx_engine/ovr_camera_component.h"
#include "wx_engine/ovr_material.h"
#include "wx_engine/ovr_texture.h"
#include "wx_engine/ovr_actor.h"
#include "wx_engine/ovr_hardware_buffer_manager.h"


ovr_engine::OvrImageComponent::OvrImageComponent(OvrActor* a_actor)
	: OvrPrimitiveComponent(a_actor)
	, x_background_size(1.0)
	, y_background_size(1.0)
	, transparent_value(1.0)
	, back_board(nullptr)
	, wrie_bounding_box(nullptr)
{
	material = OvrMaterialManager::GetIns()->Create(parent_actor->GetUniqueName());
	material->SetProgram("SingleTexWithTransparent");
	material->GetProgramParameters()->SetNameConstant("Texture0", OvrTextureManager::GetIns()->GetDefaultTexture());
	material->GetProgramParameters()->SetNameConstant("Transparent", 1.0);
	material->SetSceneBlend(SBF_SOURCE_ALPHA, SBF_ONE_MINUS_SOURCE_ALPHA);
	GenerateImageMesh(TEXT_BACKGROUND_X_SIZE, TEXT_BACKGROUND_Y_SIZE);
	rendering_enabled = true;
	collision_enabled = true;

}

ovr_engine::OvrImageComponent::~OvrImageComponent()
{
	if (wrie_bounding_box)
	{
		delete wrie_bounding_box;
	}
}
OvrBox3f ovr_engine::OvrImageComponent::GetBoundingBox()
{
	return bounding_box;
}

void ovr_engine::OvrImageComponent::UpdateRenderQueue(OvrRenderQueue* a_queue)
{
	if (!rendering_enabled)
		return;
	IOvrUiTextManager* ui_text_mgr = parent_actor->GetScene()->GetTextManager();
	OvrQuaternion quat;
	OvrVector3 pos;
	GetWorldTransform().decompose(nullptr, &quat, &pos);
	if (back_board)
	{
		a_queue->AddRenderable(back_board);
		if (parent_actor->GetShowBoundingBox())
		{
			if (!wrie_bounding_box)
				wrie_bounding_box = new OvrBoundingBox(parent_actor);
			wrie_bounding_box->SetupBoundingBox(bounding_box);
			a_queue->AddRenderable(wrie_bounding_box);
		}
	}
}

int ovr_engine::OvrImageComponent::Raycast(const OvrRay3f& a_ray, float& a_distance)
{
	return GetWorldBoundingBox().ray_interset(a_ray, a_distance);
}

void ovr_engine::OvrImageComponent::SetBackgroundTexture(OvrTexture* a_tex)
{
	back_board->GetMaterial()->GetProgramParameters()->GetTextureUnit(0)->SetTexture(a_tex);
}


void ovr_engine::OvrImageComponent::SetBackgroundSize(const float x_size, const float y_size)
{
	x_background_size = x_size;
	y_background_size = y_size;
	parent_actor->SetScale(OvrVector3(x_background_size, y_background_size, 1.0));
}


void ovr_engine::OvrImageComponent::SetTransparentValue(const float a_transparent)
{
	transparent_value = a_transparent;
	back_board->GetMaterial()->GetProgramParameters()->SetNameConstant("Transparent", transparent_value);
}


ovr_engine::OvrTexture* ovr_engine::OvrImageComponent::GetBackgroundTexture()
{
	return  back_board->GetMaterial()->GetProgramParameters()->GetTextureUnit(0)->GetTexture();
}

void ovr_engine::OvrImageComponent::GetBackgroundSize(float& x_size, float& y_size)
{
	x_size = x_background_size;
	y_size = y_background_size;
}

float ovr_engine::OvrImageComponent::GetTransparentValue()
{
	return transparent_value;
}

ovr_engine::OvrMaterial* ovr_engine::OvrImageComponent::GetMaterial()
{
	return material;
}

void ovr_engine::OvrImageComponent::GenerateImageMesh(float a_width, float a_height)
{
	struct Vertex
	{
		OvrVector3 pos;
		OvrVector2 uv0;
	};
	Vertex vertss[4];
	const float tz_value = 0.0f;
	const float t_w = a_width*0.5f;
	const float t_h = a_height*0.5f;
	vertss[0].pos = OvrVector3(-t_w, -t_h, tz_value);
	vertss[0].uv0 = OvrVector2(0.0f, 1.0f);
	vertss[1].pos = OvrVector3(t_w, -t_h, tz_value);
	vertss[1].uv0 = OvrVector2(1.0f, 1.0f);
	vertss[2].pos = OvrVector3(-t_w, t_h, tz_value);
	vertss[2].uv0 = OvrVector2(0.0f, 0.0f);
	vertss[3].pos = OvrVector3(t_w, t_h, tz_value);
	vertss[3].uv0 = OvrVector2(1.0f, 0.0f);

	if (back_board)
	{
		delete back_board;
	}
	back_board = new Image(this);
	back_board->vertex_data->declaration->AddElement(0, VET_FLOAT3, VES_POSITION);
	back_board->vertex_data->declaration->AddElement(12, VET_FLOAT2, VES_UV0);
	back_board->vertex_data->vertex_buffer = OvrHardwareBufferManager::GetIns()->CreateVertexBuffer(20, 4, &vertss[0]);
	//back_board->render_object->SetPrimitiveType(EPT_TRIANGLE_STRIP);
	bounding_box.get_min() = OvrVector3(-t_w, -t_h, tz_value);
	bounding_box.get_max() = OvrVector3(t_w, t_h, tz_value);
}

ovr_engine::OvrImageComponent::Image::Image(OvrImageComponent* a_renderer)
{
	text_renderer = a_renderer;
	vertex_data = new OvrVertexData;
}

ovr_engine::OvrImageComponent::Image::~Image()
{
	if (vertex_data)
	{
		delete vertex_data;
		vertex_data = nullptr;
	}
}

OvrMatrix4 ovr_engine::OvrImageComponent::Image::GetWorldTransform() const
{
	return text_renderer->GetWorldTransform();
}

ovr_engine::OvrMaterial* ovr_engine::OvrImageComponent::Image::GetMaterial()
{
	return text_renderer->material;
}

void ovr_engine::OvrImageComponent::Image::GetRenderOperation(OvrRenderOperation& a_op)
{
	a_op.use_indices = false;
	a_op.primitive_type = EPT_TRIANGLE_STRIP;
	a_op.vertex_data = vertex_data;
}

float ovr_engine::OvrImageComponent::Image::GetSquaredViewDepth(const OvrCameraComponent* a_cam)
{
	OvrVector3 diff = text_renderer->GetParentActor()->GetWorldPosition() - a_cam->GetCameraPos();
	return diff.length();
}
