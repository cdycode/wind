#include "wx_engine/ovr_particle_component.h"
#include "wx_engine/ovr_particle_system.h"
#include "wx_engine/ovr_material_manager.h"
#include "wx_engine/ovr_particle_system_manager.h"
#include "wx_engine/ovr_texture_manager.h"
#include "wx_engine/ovr_material.h"
#include "wx_base/wx_time.h"
#include "wx_engine/ovr_vertex_index_data.h"
#include <algorithm>
#include "ovr_gl_define.h"
#include "wx_engine/ovr_actor.h"
#include "wx_engine/ovr_hardware_buffer_manager.h"
#include "wx_engine/ovr_camera_component.h"
#include "wx_engine/ovr_scene.h"
#include "wx_engine/ovr_bounding_box.h"
#include "wx_engine/ovr_particle_emitter_box.h"
using namespace ovr_engine;
ovr_engine::OvrParticleComponent::ParticleElemnt::ParticleElemnt(OvrParticleComponent* a_renderer)
{
	this->particle_render = a_renderer;
	vertex_data = new OvrVertexData;
}

ovr_engine::OvrParticleComponent::ParticleElemnt::~ParticleElemnt()
{
	if (vertex_data)
	{
		delete vertex_data;
		vertex_data = nullptr;
	}
}

OvrMatrix4 ovr_engine::OvrParticleComponent::ParticleElemnt::GetWorldTransform() const
{
	return particle_render->GetWorldTransform();
}

OvrMaterial* ovr_engine::OvrParticleComponent::ParticleElemnt::GetMaterial()
{
	return particle_render->GetMaterial();
}

void ovr_engine::OvrParticleComponent::ParticleElemnt::PreRender(OvrScene* scene, OvrRenderSystem* driver)
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void ovr_engine::OvrParticleComponent::ParticleElemnt::PostRender()
{
	if(particle_render->GetParticleSystem())
	{
		//OvrRenderObjectManager::GetIns()->DestoryRenderObject(render_object);
		//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		//this->render_object->DrawInstanced(particle_actor->GetParticleSystem()->GetMaxPartcileNum());
	}
	else
	{
		//this->render_object->DrawInstanced(192);
	}
}

void ovr_engine::OvrParticleComponent::ParticleElemnt::GetRenderOperation(OvrRenderOperation& a_op)
{
	a_op.use_indices = false;
	a_op.primitive_type = EPT_TRIANGLES;
	a_op.vertex_data = vertex_data;
}

ovr_engine::OvrParticleComponent::OvrParticleComponent(OvrActor* a_actor) :
 OvrPrimitiveComponent(a_actor)
,particle_stop(false)
,particle_restart(false)
,particle_system(nullptr)
,particle_element(nullptr)
, wrie_bounding_box(nullptr)
, initialized(false)
{
	material_use = OvrMaterialManager::GetIns()->Create(parent_actor->GetUniqueName());
	material_use->SetProgram("ovrParticleRender");
	particle_element = new ParticleElemnt(this);
	particle_element->GetMaterial()->GetProgramParameters()->SetNameConstant("Texture0", OvrTextureManager::GetIns()->GetDefaultTexture());
	//particle_element->GetMaterial()->GetProgramParameters()->SetNameConstant("Texture0", OvrTextureManager::GetIns()->GetDefaultTexture());
	particle_element->GetMaterial()->GetProgramParameters()->SetNameConstant("Transparent", 1.0);
	particle_element->GetMaterial()->GetProgramParameters()->SetNameConstant("UniformColor", 1.0);
	particle_element->GetMaterial()->SetSceneBlend(SBF_SOURCE_ALPHA, SBF_ONE_MINUS_SOURCE_ALPHA);
	particle_element->GetMaterial()->SetDepthWriteEnable(false);
	particle_element->GetMaterial()->SetDepthCheckEnable(false);

	max_particle_num = 24;
	particle_pos_sizes.resize(max_particle_num);
	particle_rot_matrices.resize(max_particle_num);
	particle_color.resize(max_particle_num);
	GenerateParticleElement();

	particle_system = OvrParticleSystemManager::GetIns()->CreateParticleSystemByName(parent_actor->GetUniqueName() + "_particle");
	OvrParticleEmitterBox* box_emitter = (OvrParticleEmitterBox*)particle_system->GetEmitter();
	box_emitter->SetLife(3.0f, 3.0f);
	box_emitter->SetSpeed(0.2f, 0.2f);
	box_emitter->SetSize(1.0f, 1.0f);
	box_emitter->SetMainDir(OvrVector3(0.0f, -1.0f, 0.0f));
	box_emitter->SetPos(OvrVector3(0.0f));
	box_emitter->SetBoxSize(OvrVector3(0.5f, 0.1f, 0.2f));
}

ovr_engine::OvrParticleComponent::~OvrParticleComponent()
{
	if (wrie_bounding_box)
	{
		delete wrie_bounding_box;
	}

}

int ovr_engine::OvrParticleComponent::Raycast(const OvrRay3f& a_ray, float& a_distance)
{
	return GetWorldBoundingBox().ray_interset(a_ray, a_distance);
}

void ovr_engine::OvrParticleComponent::UpdateRenderQueue(OvrRenderQueue* a_queue)
{
	if (!parent_actor->IsVisible())
	{
		particle_pos_sizes.resize(max_particle_num);
		return;
	}

	if (particle_element)
	{
		a_queue->AddRenderable(particle_element);
		if (parent_actor->GetShowBoundingBox())
		{
			if (!wrie_bounding_box)
				wrie_bounding_box = new OvrBoundingBox(parent_actor);
			wrie_bounding_box->SetupBoundingBox(bounding_box);
			a_queue->AddRenderable(wrie_bounding_box);
		}
	}
	double a_time = OvrTime::GetSeconds();
	update((float)a_time);
}

static OvrQuaternion qq(OvrVector3::UNIT_Y, 0.01*OVR_PI);
void ovr_engine::OvrParticleComponent::update(float a_time)
{
	if (particle_stop)
	{
		return;
	}
	cull_result.clear();
	particle_num = 0;
	if (!particle_system)
	{
		return;
	}

		uint32_t t_pp_num;
		std::vector<OvrParticle>& too = particle_system->GetParticles(t_pp_num);
		if (particle_restart)
		{
			for (int p = 0; p < t_pp_num; ++p)
			{
				particle_system->GetEmitter()->EmitterParticle(&too[p]);
			}
			particle_restart = false;
		}
		particle_system->update(a_time);

		for (uint32_t i = 0; i < t_pp_num; ++i)
		{
			//judge if cull result return true
			cull_result.push_back(&too[i]);
			OvrMatrix4 m;
			m.from_quaternion(too[i].rotation_q);
			
			OvrQuaternion qq2(OvrVector3::UNIT_Z, 0.005*OVR_PI);
		    too[i].rotation_q = too[i].rotation_q*qq2;
			m.from_quaternion(too[i].rotation_q);

			particle_rot_matrices[particle_num] = m;
			particle_pos_sizes[particle_num] = OvrVector4(too[i].position, too[i].scale_size);
			particle_color[particle_num] = too[i].color;
			particle_num++;
		}
	//update;
	GenerateParticleElement();
	//material_use->GetProgramParameters()->SetNameConstant("u_pos_size", particle_pos_sizes[0], particle_system->GetMaxPartcileNum());
	//material_use->GetProgramParameters()->SetNameConstant("u_rotmat", particle_rot_matrices[0], particle_system->GetMaxPartcileNum());

}

void ovr_engine::OvrParticleComponent::SetParticleSystem(OvrParticleSystem* a_system)
{
	if (!a_system)
		return;
	particle_system = a_system;
	OvrVector3 emitter_pos = particle_system->GetEmitter()->GetPos();
	emitter_pos += parent_actor->GetPosition();
	particle_system->GetEmitter()->SetPos(OvrVector3(0));
}

void ovr_engine::OvrParticleComponent::RemoveParticlesystem()
{
	if(particle_system)
	{
		delete particle_system;
	}
	particle_system = nullptr;

	if(particle_element)
	{
		delete particle_element;
	}
	particle_element = nullptr;
}

void ovr_engine::OvrParticleComponent::SetMaxParticleNum(uint32_t a_max_particle_num)
{
	if( a_max_particle_num > 192)
	{
		max_particle_num = 192;
	}
	else
	{
		max_particle_num = a_max_particle_num;
	}
	particle_system->SetMaxParticleNum(max_particle_num);
	particle_pos_sizes.resize(max_particle_num);
	particle_rot_matrices.resize(max_particle_num);
	particle_color.resize(max_particle_num);
}

void ovr_engine::OvrParticleComponent::SetMaterial(OvrMaterial* a_material)
{
	material_use = a_material;
}

void ovr_engine::OvrParticleComponent::SetTransparent(float a_transparent)
{
	if (material_use)
		material_use->GetProgramParameters()->SetNameConstant("Transparent",a_transparent);
}

void ovr_engine::OvrParticleComponent::SetTexture(OvrTexture* a_tex)
{
	//particle_element->GetMaterial()->GetProgramParameters()->SetNameConstant("Texture0",a_tex);
	particle_element->GetMaterial()->SetMainTexture(a_tex);
}


void ovr_engine::OvrParticleComponent::SetParticlePause(bool a_pause)
{
	particle_stop = a_pause;
}

void ovr_engine::OvrParticleComponent::SetParticleRestart()
{
	particle_restart = true;
	particle_stop = false;
}

OvrBox3f ovr_engine::OvrParticleComponent::GetBoundingBox()
{
	return bounding_box;
}

void ovr_engine::OvrParticleComponent::GenerateParticleElement()
{
	float width = 0.2;
	float height = 0.2;
	struct particle_vertex {
		OvrVector3 pos;
		OvrVector2 uv0;
	};
	if(!initialized)
	{
		if (particle_element)
		{
			delete particle_element;
		}
		particle_element = new ParticleElemnt(this);
	}
		
	particle_vertex* vertss = new particle_vertex[6*max_particle_num];
	const float tz_value = 0.0f;
	const float t_w = width*0.5f;
	const float t_h = height*0.5f;
	for (int i = 0; i < max_particle_num; ++i)
	{
		OvrVector3 u_pos = OvrVector3(particle_pos_sizes[i]._x,
			particle_pos_sizes[i]._y, 
			particle_pos_sizes[i]._z);
		float u_size = particle_pos_sizes[i]._w;
		OvrMatrix4 u_rot;
		for (int j = 0; j < 16; ++j)
		{
			u_rot[j] = particle_rot_matrices[i][j];
		}
		//u_rot = lvr_matrix4f();
		u_rot.transpose();
		
		vertss[i * 6].pos = OvrVector3(-t_w, -t_h, tz_value);  //0
		vertss[i * 6].uv0 = OvrVector2(0.0f, 0.0f);
		//lvr_vector3f u_rot_offset = (u_rot.transform_point(lvr_vector3f(vertss[i * 4].pos)));

		//vertss[i * 4].pos = (u_pos + u_rot_offset* u_size);

	//	vertss[i * 4 + 1].pos = (u_pos + u_rot_offset2* u_size);

		vertss[i * 6 + 1].pos = OvrVector3(t_w, t_h, tz_value);  //2
		vertss[i * 6 + 1].uv0 = OvrVector2(1.0f, 1.0f);
		//lvr_vector3f u_rot_offset3 = (u_rot.transform_point(lvr_vector3f(vertss[i * 4 + 2].pos)));

		//vertss[i * 4 + 2].pos = (u_pos + u_rot_offset3* u_size);

		vertss[i * 6 + 2].pos = OvrVector3(-t_w, t_h, tz_value); //3
		vertss[i * 6 + 2].uv0 = OvrVector2(0.0f, 1.0f);
		//lvr_vector3f u_rot_offset4 = (u_rot.transform_point(lvr_vector3f(vertss[i * 4 + 3].pos)));

		//vertss[i * 4 + 3].pos = (u_pos + u_rot_offset4* u_size);


		vertss[i * 6 + 3].pos = OvrVector3(-t_w, -t_h, tz_value);  //0
		vertss[i * 6 + 3].uv0 = OvrVector2(0.0f, 0.0f);
		//lvr_vector3f u_rot_offset = (u_rot.transform_point(lvr_vector3f(vertss[i * 4].pos)));

		//vertss[i * 4].pos = (u_pos + u_rot_offset* u_size);

		vertss[i * 6 + 4].pos = OvrVector3(t_w, -t_h, tz_value);  //1
		vertss[i * 6 + 4].uv0 = OvrVector2(1.0f, 0.0f);
		//lvr_vector3f u_rot_offset2 = (u_rot.transform_point(lvr_vector3f(vertss[i * 4 + 1].pos)));


		vertss[i * 6 + 5].pos = OvrVector3(t_w, t_h, tz_value);  //2
		vertss[i * 6 + 5].uv0 = OvrVector2(1.0f, 1.0f);
		//lvr_vector3f u_rot_offset3 = (u_rot.transform_point(lvr_vector3f(vertss[i * 4 + 2].pos)));

		//vertss[i * 4 + 2].pos = (u_pos + u_rot_offset3* u_size);

		for (int m = 0; m < 6; ++m)
		{
			OvrVector3 u_rot_offset = u_rot.transform_point(vertss[i * 6 + m].pos);
			vertss[i * 6 + m].pos = u_pos + u_rot_offset*u_size;
		}

	}
	//vertss = vertss + 4;
	bounding_box.get_min() = OvrVector3(-t_w, -t_h, tz_value);
	bounding_box.get_max() = OvrVector3(t_w, t_h, tz_value);
	if (initialized)
	{
		particle_element->vertex_data->vertex_buffer->WriteData(0,max_particle_num * 6 *sizeof(particle_vertex),&vertss[0]);
	}
	else
	{
		particle_element->vertex_data->declaration->AddElement(0, VET_FLOAT3, VES_POSITION);
		particle_element->vertex_data->declaration->AddElement(12, VET_FLOAT2, VES_UV0);
		particle_element->vertex_data->vertex_buffer = OvrHardwareBufferManager::GetIns()->CreateVertexBuffer(20, max_particle_num * 6, &vertss[0]);
	}
		//particle_element->render_object = OvrRenderObjectManager::GetIns()->CreateRenderObject(kStaticVertex, 6*max_particle_num, vertss , 0, nullptr);
	//particle_element->render_object->SetPrimitiveType(EPT_TRIANGLES);
	delete[] vertss;
	initialized = true;
}

OvrParticleSystem*   ovr_engine::OvrParticleComponent::GetParticleSystem()
{
	return particle_system;
}

bool  SortByDepth(const OvrVector4& d1, const OvrVector4& d2)
{
	return (d1._z < d2._z);
}
void   ovr_engine::OvrParticleComponent::InsideDepthSortDescendingLess(ovr_engine::OvrCameraComponent* a_cam)
{
	//std::sort(particle_pos_sizes.begin(),particle_pos_sizes.end(),SortByDepth);
}

float OvrParticleComponent::ParticleElemnt::GetSquaredViewDepth(const OvrCameraComponent* a_cam)
{
	OvrVector3 diff = particle_render->GetParentActor()->GetWorldPosition() - a_cam->GetCameraPos();
	return diff.length();
}