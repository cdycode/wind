#include "wx_engine/ovr_particle_system_spring.h"
#include "lvr_primitive_sphere3.h"
#include "wx_engine/ovr_particle_emitter_point.h"
#include "wx_engine/ovr_particle_force_field.h"
#include "lvr_primitive_rect.h"
using namespace ovr_engine;
OvrParticleSystemSpring::OvrParticleSystemSpring()
{

}

OvrParticleSystemSpring::~OvrParticleSystemSpring()
{

}

void OvrParticleSystemSpring::Init()
{
// 	lvr_primitive_sphere3 t_sphere;
// 	t_sphere.set_pos(lvr_vector3f(0, 0, 0));
// 	t_sphere.set_radius(1.0f);
// 	t_sphere.update();
// 	_ro = lvr_create_standard_primitive_ro((lvr_primitive_data_base*)&t_sphere);

	/*lvr_primitive_rect t_rect;
	t_rect.set_uv_warp(1.0, 1.0);
	t_rect.set_pos(lvr_vector3f(0, 0, 0));
	t_rect.set_width(1.0f);
	t_rect.set_height(1.0f);
	t_rect.set_coord_axis(lvr_vector3f(1, 0, 0), lvr_vector3f(0, 1, 0));
	t_rect.update();
	_ro = lvr_create_standard_primitive_ro((lvr_primitive_data_base*)&t_rect);

	_material = new lvr_material;
	int w, h;
	_material->_diffuse_color.texture = lvr_texture_manager::get_texture_manager()->get_default_particle_texture();
	_material->_specular_color.texture = lvr_texture_manager::get_texture_manager()->get_defalut_texture();
	_material->_specular_color.color = lvr_vector4f(1.0, 1.0, 1.0, 1.0);
	_material->_prog = lvr_shader_manager::get_shader_mgr()->get_shader_program("ProgParticlePC_vertex_normal");
	_material->_shiness = 32.0f;
	OvrParticleEmitterPoint* t_emitter_box = new OvrParticleEmitterPoint;
	t_emitter_box->set_pos(lvr_vector3f(2, 0, 0));
	t_emitter_box->set_color(lvr_vector4f(1.0, 1.0, 1.0, 1.0));
	t_emitter_box->set_life(4.0f, 3.0f);
	t_emitter_box->set_speed(3.5f, 2.5f);
	t_emitter_box->set_size(0.3f, 0.2f);
	t_emitter_box->set_main_dir(lvr_vector3f(0, 1, 0));
	t_emitter_box->SetScatterAngle(0.15);

	OvrParticleSystem* tps = new OvrParticleSystem;
	tps->SetMaxParticleNum(192);
	tps->SetEmitter(t_emitter_box);

	OvrParticleForceField* tpff = new OvrParticleForceField;
	tpff->AddForce(lvr_vector3f(0, -1.0, 0), 1.0);
	tps->AddParticleContrller(tpff);
	AddParticleSystem(tps);

	SetMaxParticleNum(192);*/
}

void OvrParticleSystemSpring::UnInit()
{

}
