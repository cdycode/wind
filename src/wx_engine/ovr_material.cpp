#include "wx_engine/ovr_material.h"
#include "wx_engine/ovr_logger.h"
#include "wx_engine/ovr_serializer_impl.h"
#include "wx_engine/ovr_gpu_program_manager.h"
using namespace ovr_engine;
ovr_engine::OvrMaterial::OvrMaterial(OvrString a_name, bool a_is_manual)
	: OvrResource(a_name, a_is_manual)
	, is_depth_write(true)
	, is_depth_check(true)
	, shader(nullptr)
	, cull_mode(CULL_BACK)
	, params(nullptr)
	, src_factor(SBF_ONE)
	, dest_factor(SBF_ZERO)
	, is_skeleton_include(false)
	, main_texture_name("Texture0")
{

}

ovr_engine::OvrMaterial::~OvrMaterial()
{
	UnLoad();
}

void ovr_engine::OvrMaterial::SetProgram(const OvrString& a_shader_name)
{
	OvrGpuProgram* new_shader = OvrGpuProgramManager::GetIns()->GetProgram(a_shader_name);
	SetProgram(new_shader);	
}

void ovr_engine::OvrMaterial::SetProgram(OvrGpuProgram* a_shader)
{
	if (shader)
	{
		shader->DestoryParameters(params);
		shader = nullptr;
		params = nullptr;
	}

	if (a_shader)
	{
		shader = a_shader;
		params = shader->CreateParameters();
		const OvrAutoConstant* constant = shader->GetShaderConstants()->GetAutoConstant(ACT_JOINTS);
		if (constant)
			is_skeleton_include = true;
		else
			is_skeleton_include = false;
	}
}

OvrGpuProgram* ovr_engine::OvrMaterial::GetProgram()
{
	return shader;
}

void ovr_engine::OvrMaterial::SetSceneBlend(SceneBlendFactor a_src, SceneBlendFactor a_dest)
{
	src_factor = a_src;
	dest_factor = a_dest;
}

void ovr_engine::OvrMaterial::UpdateAutoParameters(const OvrAutoParamSource& src, ovr::uint16 mask)
{
	params->UpdateAutoParameters(src, mask);
}

bool ovr_engine::OvrMaterial::IsTransparent()
{
	if (src_factor == SBF_ONE && dest_factor == SBF_ZERO)
	{
		return false;
	}
	return true;
}

bool ovr_engine::OvrMaterial::SetMainTexture(OvrTexture* a_main_tex)
{
	if (params)
	{		
		params->SetNameConstant(main_texture_name, a_main_tex);
		return true;
	}
	return false;
}

OvrTexture* ovr_engine::OvrMaterial::GetMainTexture()
{
	if (params)
	{
		OvrTextureUnit* unit = params->GetTextureUnit(main_texture_name);
		if (unit->GetTexture())
		{
			return unit->GetTexture();
		}
	}
	return nullptr;
}

void ovr_engine::OvrMaterial::LoadImpl()
{
	OvrSerializerImpl::GetIns()->ImportMaterial(name, this);
}

void ovr_engine::OvrMaterial::UnLoadImpl()
{
	if (shader)
	{
		shader->DestoryParameters(params);
		shader = nullptr;
		params = nullptr;
	}
}
