﻿#include "wx_engine/ovr_gpu_program_constants.h"
#include "wx_engine/ovr_gpu_program_parameters.h"
#include "wx_engine/ovr_texture.h"
#include "wx_engine/ovr_logger.h"
using namespace ovr_engine;
ovr_engine::OvrGpuProgramParameters::~OvrGpuProgramParameters()
{
	Clear();
}

const OvrConstant* ovr_engine::OvrGpuProgramParameters::FindNameConstant(const OvrString& a_name)
{
	if(shader_constants)
		return shader_constants->GetNameConstant(a_name);
	return nullptr;
}

const OvrAutoConstantDefinition* ovr_engine::OvrGpuProgramParameters::FindAutoConstantDefinition(const OvrString& name)
{
	if (shader_constants)
		return shader_constants->FindAutoConstantDefinition(name);
	return nullptr;
}

ovr::uint32 ovr_engine::OvrGpuProgramParameters::GetAutoConstantCount()
{
	if (shader_constants)
		return shader_constants->GetAutoConstantCount();
	return 0;
}

const OvrAutoConstant* ovr_engine::OvrGpuProgramParameters::GetAutoConstant(ovr::uint32 a_index)
{
	if (shader_constants)
		return shader_constants->GetAutoConstant(a_index);
	return nullptr;
}

const OvrConstant* ovr_engine::OvrGpuProgramParameters::GetNameConstant(const OvrString& a_name)
{
	if (shader_constants)
		return shader_constants->GetNameConstant(a_name);
	return nullptr;
}

ovr::uint32 ovr_engine::OvrGpuProgramParameters::GetConstantCount()
{
	if (shader_constants)
		return shader_constants->GetAutoConstantCount();
	return 0;
}

const ConstantMap* ovr_engine::OvrGpuProgramParameters::GetConstantMap()
{
	if (shader_constants)
		return shader_constants->GetConstantMap();
	return nullptr;
}

const ConstantList* ovr_engine::OvrGpuProgramParameters::GetCustomConstantList()
{
	if (shader_constants)
		return shader_constants->GetCustomConstantList();
	return nullptr;
}

const AutoConstantList* ovr_engine::OvrGpuProgramParameters::GetAutoConstantList()
{
	if (shader_constants)
		return shader_constants->GetAutoConstantList();
	return nullptr;
}

void ovr_engine::OvrGpuProgramParameters::SetNameConstant(const OvrString& a_name, OvrTextureUnit& layer)
{
	const OvrConstant* def = FindNameConstant(a_name);
	if (def)
		WriteRawConstant(def->physicalIndex, &layer);
}
	
void ovr_engine::OvrGpuProgramParameters::SetNameConstant(const OvrString& a_name, OvrTexture* a_tex)
{
	const OvrConstant* def = FindNameConstant(a_name);
	if (def)
	{
		OvrTextureUnit unit(a_tex);
		WriteRawConstant(def->physicalIndex, &unit);
	}
}

void ovr_engine::OvrGpuProgramParameters::SetNameConstant(const OvrString& a_name, const OvrVector3& a_vec3, ovr::uint32 a_size)
{
	const OvrConstant* def = FindNameConstant(a_name);
	if (def)
		WriteRawConstant(def->physicalIndex, (const float*)&a_vec3, 3*a_size);
}

void ovr_engine::OvrGpuProgramParameters::SetNameConstant(const OvrString& a_name, const OvrMatrix4& a_mat4, ovr::uint32 a_size )
{
	const OvrConstant* def = FindNameConstant(a_name);
	if (def)
		WriteRawConstant(def->physicalIndex, (const float*)&a_mat4[0], 16 * a_size);
}

void ovr_engine::OvrGpuProgramParameters::SetNameConstant(const OvrString& a_name, const float& a_val)
{
	const OvrConstant* def = FindNameConstant(a_name);
	if (def)
		WriteRawConstant(def->physicalIndex, &a_val);
}

void ovr_engine::OvrGpuProgramParameters::SetNameConstant(const OvrString& a_name, const OvrVector4& a_vec4, ovr::uint32 a_size)
{
	const OvrConstant* def = FindNameConstant(a_name);
	if (def)
		WriteRawConstant(def->physicalIndex, (const float*)&a_vec4, 4*a_size);
}

void ovr_engine::OvrGpuProgramParameters::SetProgramConstants(OvrGpuProgramConstants* a_constants)
{
	shader_constants = a_constants;
	texture_constants.resize(a_constants->GetTextureLayerSize());
	float_constants.resize(a_constants->GetFloatSize());
	int_constants.resize(a_constants->GetIntSize());

}

float* ovr_engine::OvrGpuProgramParameters::GetFloatPointer(ovr::uint32 a_index)
{
	if (a_index < float_constants.size())
		return &float_constants[a_index];
	return nullptr;
}

int* ovr_engine::OvrGpuProgramParameters::GetIntPointer(ovr::uint32 a_index)
{
	if (a_index < int_constants.size())
		return &int_constants[a_index];
	return nullptr;
}

OvrTextureUnit* ovr_engine::OvrGpuProgramParameters::GetTextureUnit(ovr::uint32 a_index)
{
	if(a_index < texture_constants.size())
		return &texture_constants[a_index];
	return nullptr;
}

OvrTextureUnit* ovr_engine::OvrGpuProgramParameters::GetTextureUnit(const OvrString a_name)
{
	const OvrConstant* constant = GetNameConstant(a_name);
	if (constant)
	{
		if (constant->IsSampler())
		{
			return GetTextureUnit(constant->physicalIndex);
		}
	}
	return nullptr;
}

void ovr_engine::OvrGpuProgramParameters::UpdateAutoParameters(const OvrAutoParamSource& src, ovr::uint16 mask)
{
	for (ovr::uint32 i = 0; i < shader_constants->GetAutoConstantCount(); i++)
	{
		const OvrAutoConstant* entry = shader_constants->GetAutoConstant(i);
		if (entry->variability & mask)
		{
			OvrTextureUnit tex_unit;
			switch (entry->paramType)
			{
			case ACT_MVP_MAT:
				WriteRawConstant(entry->physicalIndex, (const float*)&src.mvp_matrix, 16);
				break;
			case ACT_MODEL_MAT:
				WriteRawConstant(entry->physicalIndex, (const float*)&src.world_matrix, 16);
				break;
			case ACT_VIEW_MAT:
				WriteRawConstant(entry->physicalIndex, (const float*)&src.view_matrix, 16);
				break;
			case ACT_PROJ_MAT:
				WriteRawConstant(entry->physicalIndex, (const float*)&src.projection_matrix, 16);
				break;
			case ACT_TEXM0_MAT:
				WriteRawConstant(entry->physicalIndex, (const float*)&src.tex_mat0, 16);
				break;
			case ACT_TEXM1_MAT:
				WriteRawConstant(entry->physicalIndex, (const float*)&src.tex_mat1, 16);
				break;
			case ACT_JOINTS:
				if (src.skeletal_matrix)
				{
					int num = src.skeletal_num > 100 ? 100 : src.skeletal_num;
					WriteRawConstant(entry->physicalIndex, (const float*)src.skeletal_matrix, num * 16);
				}
				break;
			case ACT_DIRECTION_LIGHT_COLOR:
				WriteRawConstant(entry->physicalIndex, (const float*)&src.direction_light_color_intensity, 4);
				break;
			case ACT_DIRECTION_LIGHT_DIR:
				WriteRawConstant(entry->physicalIndex, (const float*)&src.direction_light_dir, 3);
				break;
			case ACT_DIRECTION_LIGHT_VP_MAT:
				WriteRawConstant(entry->physicalIndex, (const float*)&src.direction_light_vp_mat, 16);
				break;
			case ACT_ENV_LIGHT_COLOR:
				WriteRawConstant(entry->physicalIndex, (const float*)&src.env_color_intensity, 4);
				break;
			case ACT_EYE_POSTION:
				WriteRawConstant(entry->physicalIndex, (const float*)&src.eye_pos, 3);
				break;
			case ACT_POINT_LIGHT_COLOR:
				WriteRawConstant(entry->physicalIndex, (const float*)&src.point_light_color_intensity, OVR_MAX_POINT_LIGHT_NUM*4);
				break;
			case ACT_POINT_LIGHT_POS_RANGE:
				WriteRawConstant(entry->physicalIndex, (const float*)&src.point_light_position_radius, OVR_MAX_POINT_LIGHT_NUM*4);
				break;
			case ACT_POINT_LIGHT_RADIUS_FOR_SHADOW:
				WriteRawConstant(entry->physicalIndex, (const float*)&src.point_light_radius_for_shadow, 1);
			    break;
			case ACT_LIGHT_NUM:
				WriteRawConstant(entry->physicalIndex, src.light_nums, 4);
				break;
			case ACT_SPOT_LIGHT_COLOR:
				WriteRawConstant(entry->physicalIndex, (const float*)src.spot_light_color_intensity, OVR_MAX_SPOT_LIGHT_NUM * 4);
				break;
			case ACT_SPOT_LIGHT_POS_EXP:
				WriteRawConstant(entry->physicalIndex, (const float*)src.spot_light_position_exponent, OVR_MAX_SPOT_LIGHT_NUM * 4);
				break;
			case ACT_SPOT_LIGHT_DIR_CUT:
				WriteRawConstant(entry->physicalIndex, (const float*)src.spot_light_dir_cutoff, OVR_MAX_SPOT_LIGHT_NUM * 4);
				break;
			case ACT_SPOT_LIGHT_VP_MAT:
				WriteRawConstant(entry->physicalIndex, (const float*)src.spot_light_vp_mat, OVR_MAX_SPOT_LIGHT_NUM * 16);
				break;
			case ACT_SPOT_SHADOW_MAP:
				tex_unit.SetTexture(src.spot_shadow_map);
				WriteRawConstant(entry->physicalIndex, &tex_unit);
				break;
			case ACT_POINT_SHADOW_MAP:
				tex_unit.SetTexture(src.point_shadow_map);
				WriteRawConstant(entry->physicalIndex, &tex_unit);
				break;
			case ACT_DIRECTION_SHADOW_MAP:
				tex_unit.SetTexture(src.direction_shadow_map);
				WriteRawConstant(entry->physicalIndex, &tex_unit);
				break;
			default: 
				break;
			}
		}
	}
}

void ovr_engine::OvrGpuProgramParameters::Clear()
{
	shader_constants = nullptr;
	texture_constants.clear();
	float_constants.clear();
	int_constants.clear();	
}

void ovr_engine::OvrGpuProgramParameters::WriteRawConstant(ovr::uint32 a_physicalIndex, OvrTextureUnit* a_tex, ovr::uint32 a_size)
{
	OvrTextureUnit* buf = &texture_constants[a_physicalIndex];
	for (ovr::uint32 i = 0; i < a_size; i++)
		buf[i] = a_tex[i];
}

void ovr_engine::OvrGpuProgramParameters::WriteRawConstant(ovr::uint32 a_physicalIndex, const float* a_float, ovr::uint32 a_size)
{
	float* buf = &float_constants[a_physicalIndex];
	memcpy(buf, a_float, a_size * sizeof(float));
}

void ovr_engine::OvrGpuProgramParameters::WriteRawConstant(ovr::uint32 a_physicalIndex, const int* a_int, ovr::uint32 a_size)
{
	int* buf = &int_constants[a_physicalIndex];
	memcpy(buf, a_int, a_size * sizeof(int));
}

