#ifndef __ovr_font_manager_h__
#define __ovr_font_manager_h__

#include "ovr_font_configure.h"

class OvrFont;
class OvrFontManager
{
public:
	OvrFontManager();
public:
	static OvrFontManager*	get_font_manager();
private:
	~OvrFontManager();
public:
	bool add_font_from_file(const char* a_file_path,const char* a_font_name);
	bool add_font_from_mem(const char* a_file_bytes,const int a_buf_len,const char* a_font_name);
	OvrFont*	get_font(const char* a_font_name);
	void		release_font(const char* a_font_name);
	void		release_all();
private:
	class impl;
	impl*	_this;
};

#endif
