#ifndef __ovr_font_impl_h__
#define __ovr_font_impl_h__

//#include "lvr_font_configure.h"

#include "ovr_kfont_gen.h"

#include "ovr_font.h"

// #include "ft2build.h"
// #include FT_FREETYPE_H
// #include FT_STROKER_H
// #include FT_BITMAP_H

//#include <map>
//////////////////////////////////////////////////////////////////////////
enum text_style
{
	regular  = 0,
	bold      = 1<<0,
	italic      = 1<<1,
	//underlined = 1<<2
};

struct rect
{
	rect()
	{
		xmin = ymin = 2000;
		xmax = ymax = -2000;
	}
	rect(float left_, float top_, float right_, float bottom_)
	{
		xmin = left_;
		xmax = right_;
		ymin = top_;
		ymax = bottom_;
	}

	void include(float x, float y)
	{
		xmin = xmin<x?xmin:x;
		xmax = xmax>x?xmax:x;
		ymin = ymin<y?ymin:y;
		ymax = ymax>y?ymax:y;
	}
	float get_width(){return (xmax-xmin+1);}
	float get_height(){return (ymax-ymin+1);}

	float xmin,xmax, ymin,ymax;
};

struct  span
{
	span():_x(0),_y(0),_width(0),_coverage(0){}
	span(int x_, int y_, int width_, int coverage_)
	{
		_x = x_;
		_y = y_;
		_width = width_;
		_coverage = coverage_;
	}

	int _x, _y, _width, _coverage;		
};
typedef std::vector<span> spans;

struct font_glyph
{
	font_glyph()
	{
		_rect.xmax = _rect.xmin = 0.f;
		_rect.ymax = _rect.ymin = 0.f;

		advance_x = advance_y = 0.f;
		bearing_x = bearing_y = 0.f;
	}
	void comput_rect()
	{
		for(unsigned int i = 0; i< font.size(); i++)
		{
			span sp = font[i];
			_rect.include((float)sp._x, (float)sp._y);
			_rect.include((float)sp._x+sp._width-1, (float)sp._y);
		}

		for(unsigned int i = 0; i< outline.size(); i++)
		{
			span sp = outline[i];
			_rect.include((float)sp._x, (float)sp._y);
			_rect.include((float)sp._x+sp._width-1, (float)sp._y);
		}
	}

	rect    _rect;
	float advance_x, advance_y, bearing_x, bearing_y;
	spans outline;
	spans font;

};
//typedef std::map<unsigned int, font_glyph*> page;//word code
//typedef std::map<unsigned int, page*> page_map;//character size
//////////////////////////////////////////////////////////////////////////
class OvrFontImpl : public OvrFont
{
public:
	OvrFontImpl();
	~OvrFontImpl();
public:
	virtual void set_font_size(uint32_t a_font_size);
	virtual uint32_t get_font_size();
	virtual OvrAlphaText	get_text_alpha(const uint16_t* a_str,uint32_t a_len);
	virtual OvrFontGlyph  get_font_glyph(const uint16_t a_char_code);
	//virtual bool get_font_outline(lvr_multi_polygon2f* ao_multi_poly,const uint16_t a_char_code);
public:
	bool load_from_file(const char* a_file_path);
	bool load_from_mem(const char* a_mem_file,const int buf_len);
	const char* get_font_family();
private:
	font_glyph* get_glyph(unsigned int word_code_, unsigned int character_size_, int style_);
	unsigned int get_line_spacing(unsigned int character_size_);
	int get_kerning(unsigned int pre_word_code_, unsigned int cur_word_code_,
		unsigned int character_size_);

	void clear();

	font_glyph* load_glyph(unsigned int word_code_, unsigned int character_size_, int style);
	bool set_current_size(unsigned int size_);
	///for italic, simply apply shear transform
	///with an angle of about 12 degrees
	void set_oblique();
	void render_spans(spans* spans_, FT_Outline* const outline_);
private:
	//page_map			_page_table;
	FT_Library			_library;
	FT_Face				_face;
	uint32_t			_font_size;
	OvrKfont			_kfont;
	bool				_use_kerning;
};

//each time the renderer calls us back we just push another span entry on our list
extern void raster_callback(const int y_, const int count_, const FT_Span* const spans_, void* const user);

#endif
