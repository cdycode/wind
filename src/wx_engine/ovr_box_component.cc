#include "wx_engine/ovr_box_component.h"
#include "wx_base/wx_box3f.h"
#include "wx_engine/ovr_actor.h"
using namespace ovr_engine;

ovr_engine::OvrBoxColliderComponent::OvrBoxColliderComponent(OvrActor* a_actor)
	:OvrShapeComponent(a_actor)
{
	wrie_bouding_box = new OvrBoundingBox(a_actor);
}

ovr_engine::OvrBoxColliderComponent::~OvrBoxColliderComponent()
{
	delete wrie_bouding_box;
}

const OvrVector3& ovr_engine::OvrBoxColliderComponent::GetCenter()
{
	return center;
}

const OvrVector3& ovr_engine::OvrBoxColliderComponent::GetSize()
{
	return size;
}

void ovr_engine::OvrBoxColliderComponent::SetCenter(const OvrVector3& a_center)
{
	center = a_center;
}

void ovr_engine::OvrBoxColliderComponent::SetSize(const OvrVector3& a_size)
{
	size = a_size;
}

int ovr_engine::OvrBoxColliderComponent::Raycast(const OvrRay3f& a_ray, float& a_distance)
{
	return GetWorldBoundingBox().ray_interset(a_ray, a_distance);
}


void ovr_engine::OvrBoxColliderComponent::UpdateRenderQueue(OvrRenderQueue* a_queue)
{
	if (rendering_enabled)
	{
		wrie_bouding_box->SetupBoundingBox(GetBoundingBox());
		a_queue->AddRenderable(wrie_bouding_box);
	}
}

OvrBox3f ovr_engine::OvrBoxColliderComponent::GetBoundingBox()
{
	OvrBox3f box;
	box.get_min() = OvrVector3(center[0] - size[0] / 2.0f, center[1] - size[1] / 2.0f, center[2] - size[2] / 2.0f);
	box.get_max() = OvrVector3(center[0] + size[0] / 2.0f, center[1] + size[1] / 2.0f, center[2] + size[2] / 2.0f);
	return box;
}
