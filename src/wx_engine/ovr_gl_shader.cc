﻿#include <assert.h>
#include <list>
#include "wx_engine/ovr_gpu_program.h"
#include "wx_engine/ovr_logger.h"
#include "lvr_shader_manager.h"
#include "wx_engine/ovr_serializer_impl.h"
#include "wx_engine/ovr_engine.h"
#include "lvr_shader_manager.h"
#include "ovr_shader_string.h"
#include "ovr_gl_shader.h"
#include "wx_engine/ovr_logger.h"
#include "lvr_program.h"
#include "ovr_gl_define.h"
#include "ovr_gl_texture.h"
using namespace ovr_engine;
ovr_engine::OvrGLShader::OvrGLShader()
	: shader_id(0)
	, source(nullptr)
{

}

ovr_engine::OvrGLShader::~OvrGLShader()
{
	Reset();
}

ovr_engine::OvrShaderType ovr_engine::OvrGLShader::GetType()
{
	return type;
}

const char* ovr_engine::OvrGLShader::GetSource()
{
	return source;
}

GLuint ovr_engine::OvrGLShader::GetShaderID()
{
	return shader_id;
}

void ovr_engine::OvrGLShader::SetSource(const char* a_shader_str)
{
	source = a_shader_str;
}

void ovr_engine::OvrGLShader::SetType(OvrShaderType a_type)
{
	type = a_type;
}

bool ovr_engine::OvrGLShader::Compile()
{
	switch (type)
	{
	case ST_VERTEX_TYPE:
		shader_id = glCreateShader(GL_VERTEX_SHADER);
		break;
	case ST_FRAGMENT_TYPE:
		shader_id = glCreateShader(GL_FRAGMENT_SHADER);
		break;
	case  ST_GEOMETRY_TYPE:
#if defined (OVR_OS_ANDROID)
		shader_id = 0;
#else
		shader_id = glCreateShader(GL_GEOMETRY_SHADER);
#endif
		break;
	case ST_COMPUTE_TYPE:
#if defined (OVR_OS_ANDROID)
		shader_id = 0;
#else
		shader_id = glCreateShader(GL_COMPUTE_SHADER);
#endif
		break;
	case ST_TESS_CONTROL_TYPE:
#if defined (LVR_OS_ANDROID) || defined( LVR_OS_IOS)
		shader_id = 0;
#else
		shader_id = glCreateShader(GL_TESS_CONTROL_SHADER);
#endif
		break;
	case ST_TESS_EVALUATION_TYPE:
#if defined (LVR_OS_ANDROID) || defined( LVR_OS_IOS)
		shader_id = 0;
#else
		shader_id = glCreateShader(GL_TESS_EVALUATION_SHADER);
#endif
		break;
	}

	if (shader_id == 0)
	{
		return false;
	}

	int sl = strlen(source);
	glShaderSource(shader_id, 1, (const char**)&source, &sl);
	glCompileShader(shader_id);
	int log_len = 0;
	GLint status;
	glGetShaderiv(shader_id, GL_COMPILE_STATUS, &status);
	if (!status)
	{
		GLint info_log_length;
		glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_log_length);
		char* shader_log = new char[info_log_length + 1];
		glGetShaderInfoLog(shader_id, info_log_length, &log_len, shader_log);
		OvrLogger::GetIns()->Log("shader compile failed.detail information are: ", LL_ERROR);
		if (log_len > 2)
		{
			OvrLogger::GetIns()->Log(shader_log, LL_ERROR);
		}
		else
		{
			OvrLogger::GetIns()->Log("unknown error.", LL_ERROR);
		}
		delete[] shader_log;
		return false;
	}
	return true;
}

void ovr_engine::OvrGLShader::Reset()
{

}