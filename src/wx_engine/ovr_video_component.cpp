#include "wx_engine/ovr_vertex_index_data.h"
#include "wx_engine/ovr_video_component.h"
#include "wx_engine/ovr_render_system.h"
#include "wx_engine/ovr_scene.h"
#include "wx_engine/ovr_media_texture.h"
#include "wx_engine/ovr_bounding_box.h"
#include "wx_engine/ovr_material_manager.h"
#include "wx_engine/ovr_texture_manager.h"
#include "wx_engine/ovr_hardware_buffer_manager.h"
#include "wx_engine/ovr_actor.h"
namespace ovr_engine
{
	bool OvrVideoComponent::GenerateTexMatInfo(MovieType a_format, OvrVector4 ao_texmat[2])
	{
		switch (a_format)
		{
		case e_movie_type_left_right_3d:
		case e_movie_type_pano_360_LR:
		case e_movie_type_pano_180_LR:
		case e_movie_type_left_right_3d_full:
		case e_movie_type_pano_cb_LR:
			ao_texmat[0] = OvrVector4(0.0f, 0.0f, 0.5f, 1.0f);
			ao_texmat[1] = OvrVector4(0.5f, 0.0f, 0.5f, 1.0f);
			break;
		case e_movie_type_right_left_3d:
		case e_movie_type_pano_360_RL:
		case e_movie_type_pano_180_RL:
		case e_movie_type_right_left_3d_full:
		case e_movie_type_pano_cb_RL:
			ao_texmat[1] = OvrVector4(0.0f, 0.0f, 0.5f, 1.0f);
			ao_texmat[0] = OvrVector4(0.5f, 0.0f, 0.5f, 1.0f);
			break;
		case e_movie_type_top_bottom_3d:
		case e_movie_type_pano_360_TD:
		case e_movie_type_pano_180_TD:
		case e_movie_type_top_bottom_3d_full:
		case e_movie_type_pano_cb_TD:
			ao_texmat[0] = OvrVector4(0.0f, 0.0f, 1.0f, 0.5f);
			ao_texmat[1] = OvrVector4(0.0f, 0.5f, 1.0f, 0.5f);
			break;
		case e_movie_type_bottom_top_3d:
		case e_movie_type_pano_360_DT:
		case e_movie_type_pano_180_DT:
		case e_movie_type_bottom_top_3d_full:
		case e_movie_type_pano_cb_DT:
			ao_texmat[1] = OvrVector4(0.0f, 0.0f, 1.0f, 0.5f);
			ao_texmat[0] = OvrVector4(0.0f, 0.5f, 1.0f, 0.5f);
			break;
		case e_movie_type_common_2d:
		case e_movie_type_common_2d_full:
		case e_movie_type_pano_360:
		case e_movie_type_pano_180:
		case e_movie_type_pano_cb:
		case e_movie_type_unknown:
		default:
			ao_texmat[0] = OvrVector4(0.0f, 0.0f, 1.0f, 1.0f);
			ao_texmat[1] = OvrVector4(0.0f, 0.0f, 1.0f, 1.0f);
			break;
		}
		return true;
	}

	OvrVideoComponent::OvrVideoComponent(OvrActor* a_actor)
		: OvrPrimitiveComponent(a_actor)
		, screen(nullptr)
		, wrie_bounding_box(nullptr)
	{
#ifdef OVR_OS_WIN32
		material = OvrMaterialManager::GetIns()->Create(GetParentActor()->GetUniqueName() + "inside_movie_texture");
		material->SetProgram("SingleTextureMovieNormal");

#else
		material = OvrMaterialManager::GetIns()->Create(GetParentActor()->GetUniqueName() + "inside_movie_texture_oes");
		material->SetShader("SingleTextureMovieOES");
#endif // OVR_OS_WIN32
		rendering_enabled = true;
		SetVideoType(Video360);
	}
	OvrVideoComponent::~OvrVideoComponent()
	{
		if (screen != nullptr)
		{
			delete screen;
		}
	}

	void OvrVideoComponent::SetVideoType(VideoType a_type)
	{
		video_type = a_type;
		if (video_type == Video2D)
		{
			movie_type = e_movie_type_common_2d;
		}
		is_build = false;
		Build();
	}

	void OvrVideoComponent::Update()
	{
		if (Video2D == video_type)
		{
			if (is_change_movie_type)
			{
				if (movie_type == e_movie_type_common_2d_full)
				{

					Screen* t_ro = GenerateRectMesh(1.0f, 1.0f);
					SetScreen(t_ro);

				}
				else if (movie_type == e_movie_type_common_2d)
				{
					Screen* t_ro = GenerateRectMesh(1.0f, 1.0f);
					SetScreen(t_ro);
				}
			}
		}
		is_change_movie_type = false;
	}

	void OvrVideoComponent::SetScreen(Screen* a_screen)
	{
		if (screen)
		{
			delete screen;
		}
		screen = a_screen;
		GenerateTexMatInfo(movie_type, texture_mat);
	}

	void OvrVideoComponent::SetMovieType(MovieType a_movie_type)
	{
		if (movie_type == a_movie_type)
		{
			return;
		}
		movie_type = a_movie_type;
		GenerateTexMatInfo(movie_type, texture_mat);

		is_change_movie_type = true;
	}

	ovr_engine::MovieType OvrVideoComponent::GetMovieType()
	{
		return movie_type;
	}

	ovr_engine::OvrMaterial* ovr_engine::OvrVideoComponent::GetMaterial()
	{
		return material;
	}



	void OvrVideoComponent::UpdateRenderQueue(OvrRenderQueue* a_queue)
	{
		if (!rendering_enabled)
			return;

		if (screen)
		{
			OvrTextureUnit util;
			OvrMatrix4 mat = util.GetTextureMatrix();
			ovr::uint32 idx = 0 % 2;
			mat[12] = texture_mat[idx][0];
			mat[13] = texture_mat[idx][1];
			mat[14] = texture_mat[idx][2];
			mat[15] = texture_mat[idx][3];
			util.SetTextureMatrix(mat);
			OvrTextureUnit* unit = material->GetProgramParameters()->GetTextureUnit("Texture0");
			unit->SetTextureMatrix(mat);
			a_queue->AddRenderable(screen);
			if (parent_actor->GetShowBoundingBox())
			{
				if (!wrie_bounding_box)
					wrie_bounding_box = new OvrBoundingBox(parent_actor);
				wrie_bounding_box->SetupBoundingBox(bounding_box);
				a_queue->AddRenderable(wrie_bounding_box);
			}
		}
	}

	void OvrVideoComponent::SetKeepMovieSize(bool a_keep_movie_size)
	{
		if (video_type != Video2D)
			return;

		if (a_keep_movie_size)
		{
			movie_type = e_movie_type_common_2d_full;
		}
		else
		{
			movie_type = e_movie_type_common_2d;
		}
	}

	bool OvrVideoComponent::Build()
	{
		if (!is_build)
		{
			switch (video_type)
			{
			case ovr_engine::Video360Box:
				SetScreen(GenerateCubeBoxMesh(1.0f));
				break;
			case ovr_engine::Video360:
				SetScreen(GenerateCubeSphereMesh(1.0f));
				break;
			case ovr_engine::Video180:
				SetScreen(GeneratePiMesh((float)OVR_PI, (float)(OVR_PI*0.8)));
				break;
			case ovr_engine::Video3D:
				SetScreen(GenerateRectMesh(1.0f, 1.0f));
				break;
			case ovr_engine::Video2D:
				SetScreen(GenerateRectMesh(1.0f, 1.0f));
				break;
			default:
				break;
			}

			if (movie_type == e_movie_type_unknown)
			{
				texture_mat[0] = OvrVector4(0.0f, 0.0f, 1.0f, 1.0f);
				texture_mat[1] = OvrVector4(0.0f, 0.0f, 1.0f, 1.0f);
			}
			is_build = true;
		}
		return true;
	}

	OvrBox3f OvrVideoComponent::GetBoundingBox()
	{
		return bounding_box;
	}

	OvrVideoComponent::Screen* OvrVideoComponent::GenerateRectMesh(float a_width, float a_height)
	{
		struct Vertex
		{
			OvrVector3 pos;
			OvrVector2 uv0;
		};
		Vertex vertss[4];
		const float tz_value = 0.0f;
		const float t_w = a_width*0.5f;
		const float t_h = a_height*0.5f;
		vertss[0].pos = OvrVector3(-t_w, -t_h, tz_value);
		vertss[0].uv0 = OvrVector2(0.0f, 1.0f);
		vertss[1].pos = OvrVector3(t_w, -t_h, tz_value);
		vertss[1].uv0 = OvrVector2(1.0f, 1.0f);
		vertss[2].pos = OvrVector3(-t_w, t_h, tz_value);
		vertss[2].uv0 = OvrVector2(0.0f, 0.0f);
		vertss[3].pos = OvrVector3(t_w, t_h, tz_value);
		vertss[3].uv0 = OvrVector2(1.0f, 0.0f);
		Screen* new_screen = new Screen(this);
		bounding_box.get_min() = OvrVector3(-t_w, -t_h, tz_value);
		bounding_box.get_max() = OvrVector3(t_w, t_h, tz_value);

		new_screen->vertex_data->declaration->AddElement(0, VET_FLOAT3, VES_POSITION);
		new_screen->vertex_data->declaration->AddElement(12, VET_FLOAT2, VES_UV0);
		new_screen->vertex_data->vertex_buffer = OvrHardwareBufferManager::GetIns()->CreateVertexBuffer(20, 4, vertss);

		//new_screen->render_object = OvrRenderObjectManager::GetIns()->CreateRenderObject(kStaticVertex, 4, vertss, 0, nullptr);
		//new_screen->render_object->SetPrimitiveType(EPT_TRIANGLE_STRIP);
		return new_screen;
	}

	OvrVideoComponent::Screen* OvrVideoComponent::GeneratePiMesh(float a_horizontal_angle, float a_view_angle)
	{
		const float uScale = 1.0f;
		const float vScale = 1.0f;
		const int horizontal = 128;
		const int vertical = 64;
		const float radius = 1.0f;
		const float c_pi = 3.1415926535898f;

		const int vertexCount = (horizontal + 1) * (vertical + 1);

		struct Vertex
		{
			OvrVector3 pos;
			OvrVector2 uv0;
		};
		Vertex* t_att = new Vertex[vertexCount];

		for (int y = 0; y <= vertical; y++)
		{
			float yf = (float)y / (float)vertical;
			const float lat = (yf - 0.5f) * a_view_angle;
			const float cosLat = cosf(lat);
			for (int x = 0; x <= horizontal; x++)
			{
				const float xf = (float)x / (float)horizontal;
				const float lon = c_pi + xf* a_horizontal_angle;
				const int index = y * (horizontal + 1) + x;

				t_att[index].pos[0] = radius * cosf(lon) * cosLat;
				t_att[index].pos[2] = radius * sinf(lon) * cosLat;
				t_att[index].pos[1] = radius * sinf(lat);

				// With a normal mapping, half the triangles degenerate at the poles,
				// which causes seams between every triangle.  It is better to make them
				// a fan, and only get one seam.
				if (y == 0 || y == vertical)
				{
					t_att[index].uv0[0] = 0.5f;
				}
				else
				{
					t_att[index].uv0[0] = xf * uScale;
				}
				t_att[index].uv0[1] = (1.0f - yf) * vScale;
			}
		}

		ovr::uint32 *indices = new ovr::uint32[horizontal * vertical * 6];

		int index = 0;
		for (int x = 0; x < horizontal; x++)
		{
			for (int y = 0; y < vertical; y++)
			{
				indices[index + 0] = y * (horizontal + 1) + x;
				indices[index + 1] = y * (horizontal + 1) + x + 1;
				indices[index + 2] = (y + 1) * (horizontal + 1) + x;
				indices[index + 3] = (y + 1) * (horizontal + 1) + x;
				indices[index + 4] = y * (horizontal + 1) + x + 1;
				indices[index + 5] = (y + 1) * (horizontal + 1) + x + 1;
				index += 6;
			}
		}

		Screen* new_screen = new Screen(this);
		bounding_box.get_min() = OvrVector3(-radius, -radius, -radius);
		bounding_box.get_max() = OvrVector3(radius, radius, radius);
		new_screen->vertex_data->declaration->AddElement(0, VET_FLOAT3, VES_POSITION);
		new_screen->vertex_data->declaration->AddElement(12, VET_FLOAT2, VES_UV0);
		new_screen->vertex_data->vertex_buffer = OvrHardwareBufferManager::GetIns()->CreateVertexBuffer(20, vertexCount, t_att);
		new_screen->index_data->index_buffer = OvrHardwareBufferManager::GetIns()->CreateIndexBuffer(horizontal*vertical * 6, indices);
		delete[] indices;
		delete[] t_att;
		return new_screen;
	}

	OvrVideoComponent::Screen* OvrVideoComponent::GenerateCubeBoxMesh(float a_radius)
	{
		struct Vertex
		{
			OvrVector3 pos;
			OvrVector2 uv0;
		};
		Vertex t_att[24];
		float	t_min = -a_radius, t_max = a_radius;
		float   delta_v0 = 1.0f / 3.0f;
		float	delta_v1 = 2.0f / 3.0f;
		//all keep cw.012  023,if we need ccw just change indices.
		//since we look inside,inside view it is ccw.
		//    1    2
		//		/
		//    0    3
		float zmin = t_min;
		float zmax = t_max;
		//left,xmin
		t_att[0].pos = OvrVector3(t_min, t_min, zmax);
		t_att[0].uv0 = OvrVector2(delta_v0, 0.5f);
		t_att[1].pos = OvrVector3(t_min, t_max, zmax);
		t_att[1].uv0 = OvrVector2(delta_v0, 0.0f);
		t_att[2].pos = OvrVector3(t_min, t_max, zmin);
		t_att[2].uv0 = OvrVector2(delta_v1, 0.0f);
		t_att[3].pos = OvrVector3(t_min, t_min, zmin);
		t_att[3].uv0 = OvrVector2(delta_v1, 0.5f);
		//front zmax
		t_att[4].pos = OvrVector3(t_max, t_min, zmax);
		t_att[4].uv0 = OvrVector2(delta_v1, 1.0f);
		t_att[5].pos = OvrVector3(t_max, t_max, zmax);
		t_att[5].uv0 = OvrVector2(delta_v1, 0.5f);
		t_att[6].pos = OvrVector3(t_min, t_max, zmax);
		t_att[6].uv0 = OvrVector2(1.0f, 0.5f);
		t_att[7].pos = OvrVector3(t_min, t_min, zmax);
		t_att[7].uv0 = OvrVector2(1.0f, 1.0f);
		//right xmax
		t_att[8].pos = OvrVector3(t_max, t_min, zmin);
		t_att[8].uv0 = OvrVector2(0.0f, 0.5f);
		t_att[9].pos = OvrVector3(t_max, t_max, zmin);
		t_att[9].uv0 = OvrVector2(0.0f, 0.0f);
		t_att[10].pos = OvrVector3(t_max, t_max, zmax);
		t_att[10].uv0 = OvrVector2(delta_v0, 0.0f);
		t_att[11].pos = OvrVector3(t_max, t_min, zmax);
		t_att[11].uv0 = OvrVector2(delta_v0, 0.5f);
		//back zmin
		t_att[12].pos = OvrVector3(t_min, t_min, zmin);
		t_att[12].uv0 = OvrVector2(delta_v0, 1.0f);
		t_att[13].pos = OvrVector3(t_min, t_max, zmin);
		t_att[13].uv0 = OvrVector2(delta_v0, 0.5f);
		t_att[14].pos = OvrVector3(t_max, t_max, zmin);
		t_att[14].uv0 = OvrVector2(delta_v1, 0.5f);
		t_att[15].pos = OvrVector3(t_max, t_min, zmin);
		t_att[15].uv0 = OvrVector2(delta_v1, 1.0f);
		//top ymax
		t_att[16].pos = OvrVector3(t_min, t_max, zmin);
		t_att[16].uv0 = OvrVector2(delta_v1, 0.5f);
		t_att[17].pos = OvrVector3(t_min, t_max, zmax);
		t_att[17].uv0 = OvrVector2(delta_v1, 0.0f);
		t_att[18].pos = OvrVector3(t_max, t_max, zmax);
		t_att[18].uv0 = OvrVector2(1.0f, 0.0f);
		t_att[19].pos = OvrVector3(t_max, t_max, zmin);
		t_att[19].uv0 = OvrVector2(1.0f, 0.5f);
		//bottom ymin
		t_att[20].pos = OvrVector3(t_min, t_min, zmax);
		t_att[20].uv0 = OvrVector2(0.0f, 1.0f);
		t_att[21].pos = OvrVector3(t_min, t_min, zmin);
		t_att[21].uv0 = OvrVector2(0.0f, 0.5f);
		t_att[22].pos = OvrVector3(t_max, t_min, zmin);
		t_att[22].uv0 = OvrVector2(delta_v0, 0.5f);
		t_att[23].pos = OvrVector3(t_max, t_min, zmax);
		t_att[23].uv0 = OvrVector2(delta_v0, 1.0f);

		//flipv
		// 	for (int i = 0; i < 24;++i)
		// 	{
		// 		t_att[i].uv0[1] = 1.0 - t_att[i].uv0[1];
		// 	}

		ovr::uint32 indices[36];
		for (int i = 0; i < 6; ++i)
		{
			int tpi = 4 * i;
			int tii = 6 * i;
			indices[tii] = tpi + 0;
			indices[tii + 1] = tpi + 1;
			indices[tii + 2] = tpi + 2;
			indices[tii + 3] = tpi + 0;
			indices[tii + 4] = tpi + 2;
			indices[tii + 5] = tpi + 3;
		}
		Screen* new_screen = new Screen(this);
		bounding_box.get_min() = OvrVector3(t_min, t_min, zmin);
		bounding_box.get_max() = OvrVector3(t_max, t_max, zmax);
		new_screen->vertex_data->declaration->AddElement(0, VET_FLOAT3, VES_POSITION);
		new_screen->vertex_data->declaration->AddElement(12, VET_FLOAT2, VES_UV0);
		new_screen->vertex_data->vertex_buffer = OvrHardwareBufferManager::GetIns()->CreateVertexBuffer(20, 24, t_att);
		new_screen->index_data->index_buffer = OvrHardwareBufferManager::GetIns()->CreateIndexBuffer(36, indices);
		return new_screen;
	}

	OvrVideoComponent::Screen* OvrVideoComponent::GenerateCubeSphereMesh(float a_radius)
	{
		const float uScale = 1.0f;
		const float vScale = 1.0f;
		const int poleVertical = 3;
		const int uniformVertical = 64;
		const int horizontal = 128;
		const int vertical = uniformVertical + poleVertical * 2;
		const float radius = a_radius;
		const float c_pi = 3.1415926535898f;

		const int vertexCount = (horizontal + 1) * (vertical + 1);

		struct Vertex
		{
			OvrVector3 pos;
			OvrVector2 uv0;
		};
		Vertex* t_att = new Vertex[vertexCount];

		for (int y = 0; y <= vertical; y++)
		{
			float yf;
			if (y <= poleVertical)
			{
				yf = (float)y / (poleVertical + 1) / uniformVertical;
			}
			else if (y >= vertical - poleVertical)
			{
				yf = (float)(uniformVertical - 1 + ((float)(y - (vertical - poleVertical - 1)) / (poleVertical + 1))) / uniformVertical;
			}
			else
			{
				yf = (float)(y - poleVertical) / uniformVertical;
			}
			const float lat = (yf - 0.5f) * c_pi;
			const float cosLat = cosf(lat);
			for (int x = 0; x <= horizontal; x++)
			{
				const float xf = (float)x / (float)horizontal;
				const float lon = (0.25f + xf) * c_pi * 2.0f;
				const int index = y * (horizontal + 1) + x;

				if (x == horizontal)
				{
					// Make sure that the wrap seam is EXACTLY the same
					// xyz so there is no chance of pixel cracks.
					t_att[index].pos = t_att[y * (horizontal + 1) + 0].pos;
				}
				else
				{
					t_att[index].pos[0] = radius * cosf(lon) * cosLat;
					t_att[index].pos[2] = radius * sinf(lon) * cosLat;
					t_att[index].pos[1] = radius * sinf(lat);
				}

				// With a normal mapping, half the triangles degenerate at the poles,
				// which causes seams between every triangle.  It is better to make them
				// a fan, and only get one seam.
				if (y == 0 || y == vertical)
				{
					t_att[index].uv0[0] = 0.5f;
				}
				else
				{
					t_att[index].uv0[0] = xf * uScale;
				}
				t_att[index].uv0[1] = (1.0f - yf) * vScale;
			}
		}

		ovr::uint32 *indices = new ovr::uint32[horizontal * vertical * 6];

		int index = 0;
		for (int x = 0; x < horizontal; x++)
		{
			for (int y = 0; y < vertical; y++)
			{
				indices[index + 0] = y * (horizontal + 1) + x;
				indices[index + 1] = y * (horizontal + 1) + x + 1;
				indices[index + 2] = (y + 1) * (horizontal + 1) + x;
				indices[index + 3] = (y + 1) * (horizontal + 1) + x;
				indices[index + 4] = y * (horizontal + 1) + x + 1;
				indices[index + 5] = (y + 1) * (horizontal + 1) + x + 1;
				index += 6;
			}
		}
		Screen* new_screen = new Screen(this);
		new_screen->vertex_data->declaration->AddElement(0, VET_FLOAT3, VES_POSITION);
		new_screen->vertex_data->declaration->AddElement(12, VET_FLOAT2, VES_UV0);
		new_screen->vertex_data->vertex_buffer = OvrHardwareBufferManager::GetIns()->CreateVertexBuffer(20, vertexCount, t_att);
		new_screen->index_data->index_buffer = OvrHardwareBufferManager::GetIns()->CreateIndexBuffer(horizontal * vertical * 6, indices);

		bounding_box.get_min() = OvrVector3(-radius, -radius, -radius);
		bounding_box.get_max() = OvrVector3(radius, radius, radius);
		delete[] indices;
		delete[] t_att;
		return new_screen;
	}
	 
	int OvrVideoComponent::Raycast(const OvrRay3f& a_ray, float& a_distance)
	{
		return GetWorldBoundingBox().ray_interset(a_ray, a_distance);
	}

	OvrVideoComponent::Screen::Screen(OvrVideoComponent* a_renderer)
		: video_renderer(a_renderer)
	{
		vertex_data = new OvrVertexData();
		index_data = new OvrIndexData;
	}

	OvrVideoComponent::Screen::~Screen()
	{
		delete vertex_data;
		delete index_data;
	}

	OvrMatrix4 OvrVideoComponent::Screen::GetWorldTransform() const
	{
		return video_renderer->GetWorldTransform();
	}

	ovr_engine::OvrMaterial* OvrVideoComponent::Screen::GetMaterial()
	{
		return video_renderer->GetMaterial();
	}

	void OvrVideoComponent::Screen::GetRenderOperation(OvrRenderOperation& a_op)
	{
		a_op.index_data = index_data;
		if(!index_data->index_buffer)
			a_op.use_indices = false;
		else
			a_op.use_indices = true;
		a_op.vertex_data = vertex_data;
		a_op.src_renderable = this;	
		a_op.primitive_type = EPT_TRIANGLE_STRIP;
	}

}
