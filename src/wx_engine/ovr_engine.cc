#include "headers.h"
#include "wx_engine/ovr_engine.h"
#include "wx_engine/ovr_scene.h"
#include "wx_engine/ovr_actor_axis.h"
#include "wx_engine/ovr_axis_system.h"
#include "ovr_shader_string.h"
#include "ovr_gl_render_system.h"
#include "wx_engine/ovr_logger.h"
#include "wx_engine/ovr_gpu_program_manager.h"
#include "wx_engine/ovr_skeleton_manager.h"
#include "wx_engine/ovr_material_manager.h"
#include "wx_engine/ovr_texture_manager.h"
#include "wx_engine/ovr_mesh_manager.h"
#include "ovr_lvr_ui_text_manager.h"
#include "wx_engine/ovr_particle_system_manager.h"
#include "wx_base/wx_log.h"
#include "wx_engine/ovr_camera_component.h"
#include "wx_engine/ovr_dynamic_lib_manager.h"
#include "ovr_gl_hardware_buffer_manager.h"
#include "ovr_gl_gpu_program_manager.h"
#include "wx_engine/ovr_serializer_impl.h"
using namespace ovr_engine;
OvrEngine* OvrEngine::s_ins = nullptr;
ovr_engine::OvrEngine::OvrEngine()
	: is_inited(false)
	, window_width(0)
	, window_height(0)
{
	render_system = new OvrGLRenderSystem();
	ui_text_mgr = new OvrLvrUiTextManager();
	mesh_manager = new OvrMeshManager();
	material_manager = new OvrMaterialManager();
	texture_manager = new OvrTextureManager();
	skeleton_manager = new OvrSkeletonManager();
	particle_manager = new OvrParticleSystemManager();
	serialize = new OvrSerializerImpl();
	logger = new OvrLogger();
	hw_manager = new OvrGLHardwareBufferManager();
	program_manager = new OvrGLGpuProgramManager();
}

ovr_engine::OvrEngine::~OvrEngine()
{
	if (is_inited)
		Uninit();

	delete render_system;
	delete ui_text_mgr;
	delete mesh_manager;
	delete material_manager;
	delete texture_manager;
	delete skeleton_manager;
	delete particle_manager;
	delete serialize;
	delete logger;
	delete hw_manager;
	delete program_manager;
}

OvrEngine* ovr_engine::OvrEngine::GetIns()
{
	if (nullptr != s_ins)
	{
		return s_ins;
	}
	s_ins = new OvrEngine;
	return s_ins;
}

void ovr_engine::OvrEngine::DelIns()
{
	if (nullptr != s_ins)
	{
		delete s_ins;
		s_ins = nullptr;
	}
}

void ovr_engine::OvrEngine::SetResourceLocation(const OvrString& a_path)
{
	resource_path = a_path;
}

const OvrString& ovr_engine::OvrEngine::GetResourceLocation()
{
	return resource_path;
}

bool ovr_engine::OvrEngine::Init()
{
	if (is_inited)
	{
		return true;
	}
	OvrLogger::GetIns()->Initialise("ovrlog.txt");

	ovr_asset::OvrArchiveAssetManager* manager = ovr_asset::OvrArchiveAssetManager::GetIns();
	manager->SetRootDir(resource_path.GetStringConst());

	
	render_system->Initialise();
	program_manager->Initialise();

#ifdef OVR_OS_WIN32
	ui_text_mgr->Initialise("./system/engine/font/DroidSansFallback.ttf", false);
#elif defined(OVR_OS_ANDROID)
	ui_text_mgr->Initialise("assets/DroidSansFallback.ttf", true);
#endif
	mesh_manager->Initialise();
	texture_manager->Initialise();
	material_manager->Initialise();
	particle_manager->Initialise();
	is_inited = true;
	return true;
}

void ovr_engine::OvrEngine::Uninit()
{

	for (ovr::uint32 i = 0; i < scene_list.size(); i++)
	{
		delete scene_list[i];
	}
	scene_list.clear();


	render_system->UnInitialise();
	ui_text_mgr->UnInitialise();
	mesh_manager->UnInitialise();
	material_manager->UnInitialise();
	program_manager->UnInitialise();
	texture_manager->UnInitialise();
	particle_manager->UnInitialise();
	logger->UnInitialise();
	is_inited = false;
}


void ovr_engine::OvrEngine::Reshape(int a_new_width, int a_new_height)
{
	window_width = a_new_width;
	window_height = a_new_height;
	glViewport(0, 0, window_width, window_height);
	LVR_LOG("reshape to %d %d", window_width, window_height);
}

OvrScene* ovr_engine::OvrEngine::GetScene(const OvrString& a_scene_name)
{
	SceneListIterator it = scene_list.begin();
	for (; it != scene_list.end(); it++)
	{
		if ((*it)->GetUniqueName() == a_scene_name)
		{
			return *it;
		}
	}
	return nullptr;
}

void ovr_engine::OvrEngine::LoadPlugin(const OvrString& a_plugin_name)
{

	typedef void* (*DLLStartFunc)();
	OvrDynLib* dyn = OvrDynLibManager::GetIns()->Load(a_plugin_name);
	if (dyn)
	{
		DLLStartFunc start_func = (DLLStartFunc)dyn->GetSymbol("DllStartPlugin");
		if (start_func)
		{
			(*start_func)();
		}
	}
}

void ovr_engine::OvrEngine::UnLoadPlugin(const OvrString& a_plugin_name)
{
	typedef void* (*DLLStopFunc)();
	OvrDynLib* dyn = OvrDynLibManager::GetIns()->Load(a_plugin_name);
	if (dyn)
	{
		DLLStopFunc stop_func = (DLLStopFunc)dyn->GetSymbol("DllStopPlugin");
		if (stop_func)
		{
			(*stop_func)();
		}
		OvrDynLibManager::GetIns()->Unload(dyn);
	}
}

void ovr_engine::OvrEngine::InstallPlugin(OvrPlugin* plugin)
{
	plugin->Install();
	plugins.push_back(plugin);
}

void ovr_engine::OvrEngine::UnInstallPlugin(OvrPlugin* plugin)
{
	plugin->UnInstall();
	std::vector<OvrPlugin*>::iterator it = plugins.begin();
	for (; it != plugins.end(); it++)
	{
		if (*it == plugin)
		{
			plugins.erase(it);
			break;
		}
	}
}

OvrScene* ovr_engine::OvrEngine::CreateScene()
{
	OvrScene* new_scene = new OvrScene(render_system, ui_text_mgr);
	scene_list.push_back(new_scene);
	return new_scene;
}

void ovr_engine::OvrEngine::DestoryScene(OvrScene*& a_scene)
{

	SceneListIterator it = scene_list.begin();
	for (; it != scene_list.end(); it++)
	{
		if (*it == a_scene)
		{
			scene_list.erase(it);
			delete a_scene;
			a_scene = nullptr;
			return;
		}
	}
}

ovr::uint32 ovr_engine::OvrEngine::GetSceneCount()
{
	return scene_list.size();
}
