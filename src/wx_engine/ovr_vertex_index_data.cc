#include "wx_engine/ovr_vertex_index_data.h"
#include "wx_engine/ovr_hardware_buffer_manager.h"
using namespace ovr_engine;

ovr_engine::OvrVertexDeclaration::OvrVertexDeclaration()
{
}

ovr_engine::OvrVertexDeclaration::~OvrVertexDeclaration()
{
	RemoveAllElements();
}

void ovr_engine::OvrVertexDeclaration::AddElement(ovr::uint32 a_offset, OvrVertexElementType a_type, OvrVertexElementSemantic a_semantic)
{
	OvrVertexElement ele;
	ele.offset = a_offset;
	ele.semantic = a_semantic;
	ele.type = a_type;
	elements.push_back(ele);
	NotifyChanged();
}

void ovr_engine::OvrVertexDeclaration::InsertElement(ovr::uint32 a_pos, ovr::uint32 a_offset, OvrVertexElementType a_type, OvrVertexElementSemantic a_semantic)
{
	if (a_pos >= elements.size())
	{
		AddElement(a_offset, a_type, a_semantic);
		return;
	}
		

	OvrVertexElement ele;
	ele.offset = a_offset;
	ele.semantic = a_semantic;
	ele.type = a_type;

	VertexElementList::iterator it = elements.begin();
	it += a_pos;

	elements.insert(it, ele);
	NotifyChanged();
}

void ovr_engine::OvrVertexDeclaration::RemoveAllElements(void)
{
	elements.clear();
	NotifyChanged();
}

void ovr_engine::OvrVertexDeclaration::RemoveElement(ovr::uint16 a_elem_index)
{
	if (a_elem_index < elements.size())
	{
		VertexElementList::iterator it = elements.begin();
		it += a_elem_index;
		elements.erase(it);
		NotifyChanged();
	}
}

void ovr_engine::OvrVertexDeclaration::ModifyElement(ovr::uint16 a_elem_index, ovr::uint32 offset, OvrVertexElementType a_type, OvrVertexElementSemantic a_semantic)
{
	if (a_elem_index < elements.size())
	{
		elements[a_elem_index].offset = offset;
		elements[a_elem_index].semantic = a_semantic;
		elements[a_elem_index].type = a_type;
		NotifyChanged();
	}
}

const OvrVertexElement* ovr_engine::OvrVertexDeclaration::FindElementBySemantic(OvrVertexElementSemantic a_sem) const
{
	VertexElementList::const_iterator ei, eiend;
	eiend = elements.end();
	for (ei = elements.begin(); ei != eiend; ++ei)
	{
		if (ei->semantic == a_sem)
		{
			return &(*ei);
		}
	}

	return nullptr;
}

ovr::uint32 ovr_engine::OvrVertexDeclaration::GetElementCount()
{
	return elements.size();
}

const OvrVertexElement* ovr_engine::OvrVertexDeclaration::GetElement(ovr::uint16 index) const
{
	if (index < elements.size())
	{
		return &elements[index];
	}
	return nullptr;
}

void ovr_engine::OvrVertexDeclaration::RemoveElement(OvrVertexElementSemantic& a_semantic)
{
	VertexElementList::iterator ei, eiend;
	eiend = elements.end();
	for (ei = elements.begin(); ei != eiend; ++ei)
	{
		if (ei->semantic == a_semantic)
		{
			elements.erase(ei);
			NotifyChanged();
			break;
		}
	}
}

ovr_engine::OvrVertexData::OvrVertexData()
{
	manager = OvrHardwareBufferManager::GetIns();
	vertex_buffer = nullptr;
	declaration = manager->CreateVertexDeclaration();
}

ovr_engine::OvrVertexData::~OvrVertexData()
{
	manager->DestroyVertexDeclaration(declaration);
}

ovr_engine::OvrIndexData::OvrIndexData()
	:index_buffer(nullptr)
{

}
