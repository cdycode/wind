#include "wx_engine/ovr_static_mesh_component.h"
#include "wx_engine/ovr_mesh.h"
#include "wx_engine/ovr_logger.h"
#include "wx_engine/ovr_material.h"
#include "wx_engine/ovr_material_manager.h"
#include "wx_engine/ovr_bounding_box.h"
#include "wx_engine/ovr_actor.h"
using namespace ovr_engine;
OvrStaticMeshComponent::OvrStaticMeshComponent(OvrActor* a_actor)
	:OvrMeshComponent(a_actor)
{

}

OvrStaticMeshComponent::~OvrStaticMeshComponent()
{
	ResetRenderableList();
}

void OvrStaticMeshComponent::Update()
{

}

void OvrStaticMeshComponent::SetMesh(ovr_engine::OvrMesh* a_mesh)
{
	if (!a_mesh)
		return;

	a_mesh->Grab();

	if (mesh)
		mesh->Drop();

	mesh = a_mesh;

	BuildRenderableList();

	mesh_state_count = a_mesh->GetStateCount();
}