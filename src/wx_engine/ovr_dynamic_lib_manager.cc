﻿#include "wx_engine/ovr_dynamic_lib_manager.h"
using namespace ovr_engine;
template<> OvrDynLibManager* ovr_engine::OvrSingleton<OvrDynLibManager>::s_instance = nullptr;
OvrDynLib* ovr_engine::OvrDynLibManager::Load(const OvrString& a_name)
{
	OvrDynLib* dyn = (OvrDynLib*)GetByName(a_name);
	if(!dyn)
	{
		dyn = new OvrDynLib(a_name);
		dyn->Load();
	}
	return dyn;
}

void ovr_engine::OvrDynLibManager::Unload(OvrDynLib* a_lib)
{
	std::map<OvrString, OvrDynLib*>::iterator it = dynlib_map.find(a_lib->GetName());
	if (it != dynlib_map.end())
	{
		it->second->UnLoad();
		delete it->second;
		dynlib_map.erase(it);
	}
}

OvrDynLib* ovr_engine::OvrDynLibManager::GetByName(const OvrString& a_name)
{
	std::map<OvrString, OvrDynLib*>::iterator it = dynlib_map.find(a_name);
	if (it != dynlib_map.end())
	{
		return it->second;
	}
	return nullptr;
}