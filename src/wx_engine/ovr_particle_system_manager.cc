#include "wx_engine/ovr_particle_system_manager.h"
#include "wx_engine/ovr_particle_system_snow.h"
#include "wx_engine/ovr_particle_system_fire.h"
#include "wx_engine/ovr_particle_system_spring.h"
#include "wx_engine/ovr_particle_system.h"
#include "wx_engine/ovr_particle_emitter_box.h"
#include "wx_engine/ovr_particle_emitter_point.h"
#include "wx_engine/ovr_particle_emitter_circle.h"
#include "wx_engine/ovr_particle_emitter_line.h"
#include "wx_engine/ovr_particle_emitter_sphere.h"
#include "wx_engine/ovr_particle_emitter_mesh.h"
#include "wx_engine/ovr_particle_force_field.h"
#include "wx_engine/ovr_particle_controller_bezier.h"
using namespace ovr_engine;
ovr_engine::OvrParticleSystemManager::OvrParticleSystemManager()
	:max_partcile_num(24)
{

}


ovr_engine::OvrParticleSystemManager::~OvrParticleSystemManager()
{

}

bool ovr_engine::OvrParticleSystemManager::Initialise()
{
	//InitInsideSystems();
	//feather_emitter->SetScatterAngle(30);
	//feather_emitter->SetInitRotateQ(OvrQuaternion(0.707,0.707,0,0));
	//feather_emitter->SetInitAxisRad(OvrVector3::UNIT_X,0.2*OVR_PI);
	feather_sys = new OvrParticleSystem;
	feather_sys->SetMaxParticleNum(24);
	OvrParticleEmitterBox* feather_emitter = (OvrParticleEmitterBox*)feather_sys->GetEmitter();
	feather_emitter->SetLife(3.0f, 3.0f);
	feather_emitter->SetSpeed(0.2f, 0.2f);
	feather_emitter->SetSize(1.0f, 1.0f);
	feather_emitter->SetMainDir(OvrVector3(0.0f, -1.0f, 0.0f));
	feather_emitter->SetPos(OvrVector3(0.0f));
	feather_emitter->SetBoxSize(OvrVector3(0.5f, 0.1f, 0.2f));
	OvrParticleForceField* tpff = new OvrParticleForceField;
	tpff->set_controller_type( OvrParticleController::control_type::e_control_rotation);
	tpff->AddForce(OvrVector3(0.0f, 0.0f, 0.0f), 0.07f);
	//tpff->AddForce(OvrVector3(1,0,0),0.1);
	feather_sys->AddParticleContrller(tpff);
	//particle_systems[feather_sys] = feather_sys;
	return true;
}



void ovr_engine::OvrParticleSystemManager::UnInitialise()
{
	std::map<OvrString, OvrParticleSystem*>::iterator cur = particle_systems.begin();
	std::map<OvrString, OvrParticleSystem*>::iterator end = particle_systems.end();
	while (cur != end)
	{
		OvrString a = cur->first;
		delete cur->second;
		cur++;
	}
	particle_systems.clear();

	if (feather_sys)
	{
		delete feather_sys;
	}
	feather_sys = nullptr;
}

OvrParticleSystem* ovr_engine::OvrParticleSystemManager::CreateParticleSystemByName(const OvrString& a_name, const OvrString& a_type /*= "OvrParticleEmitterBox"*/)
{
	OvrParticleSystem* new_system = new OvrParticleSystem;
	new_system->SetMaxParticleNum(max_partcile_num);
	//new_system->ChangeEmitter(a_type);
	particle_systems[a_name] = new_system;
	return new_system;
}





OvrParticleSystem* ovr_engine::OvrParticleSystemManager::Load(const OvrString& a_file_name)
{
	return nullptr;
}


OvrParticleSystem* ovr_engine::OvrParticleSystemManager::GetFeatherparticleSystem()
{
	return feather_sys;
}

void ovr_engine::OvrParticleSystemManager::InitInsideSystems()
{
	OvrParticleEmitter*  tpeb = REFLEX_CLASS(OvrParticleEmitterBox);
	//tpeb->SetLife(10.0f, 5.0f);
	//tpeb->SetSpeed(0.5f, 0.3f);
	//tpeb->SetSize(0.2f, 0.1f);
	//tpeb->SetMainDir(OvrVector3(0.0f, 1.0f, 0.0f));
	//tpeb->SetPos(OvrVector3(0.0f));
	//tpeb->SetBoxSize(OvrVector3(3.0f, 0.5f, 3.0f));
	OvrParticleSystem* default_sys = new OvrParticleSystem;
	default_sys->SetMaxParticleNum(max_partcile_num);
	particle_systems["default_box_emitter"] = default_sys;

	tpeb = REFLEX_CLASS(OvrParticleEmitterLine);
	default_sys = new OvrParticleSystem;
	default_sys->SetMaxParticleNum(max_partcile_num);
	particle_systems["default_line_emitter"] = default_sys;

	tpeb = REFLEX_CLASS(OvrParticleEmitterPoint);
	default_sys = new OvrParticleSystem;
	default_sys->SetMaxParticleNum(max_partcile_num);
	particle_systems["default_point_emitter"] = default_sys;

	tpeb = REFLEX_CLASS(OvrParticleEmitterCircle);
	default_sys = new OvrParticleSystem;
	default_sys->SetMaxParticleNum(max_partcile_num);
	particle_systems["default_circle_emitter"] = default_sys;

	tpeb = REFLEX_CLASS(OvrParticleEmitterSphere);
	default_sys = new OvrParticleSystem;
	default_sys->SetMaxParticleNum(max_partcile_num);
	particle_systems["default_sphere_emitter"] = default_sys;

	tpeb = REFLEX_CLASS(OvrParticleEmitterMesh);
	default_sys = new OvrParticleSystem;
	default_sys->SetMaxParticleNum(max_partcile_num);
	particle_systems["default_mesh_emitter"] = default_sys;
}




