#include <cmath>
#include "wx_engine/ovr_particle_system.h"
#include "wx_engine/ovr_particle_emitter_box.h"
#include "wx_engine/ovr_particle_emitter_line.h"
#include "wx_engine/ovr_particle_emitter_circle.h"
#include "wx_engine/ovr_particle_emitter_sphere.h"
#include "wx_engine/ovr_particle_emitter_mesh.h"
#include "wx_engine/ovr_particle_emitter_point.h"
using namespace ovr_engine;
OvrParticleSystem::OvrParticleSystem()
	:controller_num(0), current_particle_num(0), camera_ptr(NULL)
{
	emitter = REFLEX_CLASS(OvrParticleEmitterBox);
}

OvrParticleSystem::~OvrParticleSystem()
{
	if (emitter)
	{
		delete emitter;
	}
	emitter = nullptr;

	std::vector<OvrParticleController*>::iterator it = controllers.begin();
	for (; it != controllers.end(); ++it)
	{
		delete *it;
	}
	controllers.clear();
}

void OvrParticleSystem::update(float a_time)
{
	float t_delta_time = a_time - last_time;
	if (fabs(t_delta_time)>1.0f)
	{
		t_delta_time = 0.016f;
	}
	if (current_particle_num<max_particle_num)
	{
		//emitter->EmitterParticle(&particles[current_particle_num]);
		current_particle_num++;
	}
	for (int i = 0; i < current_particle_num;++i)
	{
		particles[i].life_now += t_delta_time;
		if (particles[i].life_now >= particles[i].life_total)
		{
			emitter->EmitterParticle(&particles[i]);
		}
		for (int j = 0; j < controller_num;++j)
		{
			controllers[j]->update(&particles[i], t_delta_time);
		}
		particles[i].position += particles[i].cur_v*t_delta_time;
		particles[i].color[3] = 1.0-particles[i].life_now / particles[i].life_total;
	}
	last_time = a_time;
}

void OvrParticleSystem::AddParticleContrller(OvrParticleController* a_controller)
{
	controllers.push_back(a_controller);
	controller_num = controllers.size();
}



void OvrParticleSystem::SetMaxParticleNum(uint32_t a_particle_num)
{
	max_particle_num = a_particle_num;
	particles.resize(max_particle_num);
	current_particle_num = current_particle_num > max_particle_num ?  max_particle_num : current_particle_num;
}

void OvrParticleSystem::SetFaceCamera(OvrCamera* a_cam_ptr)
{
	camera_ptr = a_cam_ptr;
}

std::vector<OvrParticle>& OvrParticleSystem::GetParticles(uint32_t& ao_particle_num)
{
	ao_particle_num = current_particle_num;
	return particles;
}

OvrParticleEmitter* ovr_engine::OvrParticleSystem::GetEmitter()
{
	return emitter;
}

void ovr_engine::OvrParticleSystem::ChangeEmitter(const OvrString& a_emitter)
{
	if (emitter)
	{
		delete emitter;
	}
	emitter = (OvrParticleEmitter*)OvrParticleEmitter::CreateObject(a_emitter);
}

const int ovr_engine::OvrParticleSystem::GetMaxPartcileNum()
{
	return max_particle_num;
}
