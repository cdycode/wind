#include <cmath>
#include "wx_engine/ovr_particle_emitter_line.h"
#include "wx_engine/ovr_particle_class_info.h"

using namespace ovr_engine;
using namespace ovr;

REGISTER_CLASS(OvrParticleEmitterLine)
OvrParticleEmitterLine::OvrParticleEmitterLine()
	:OvrParticleEmitter()
{

}

ovr_engine::OvrParticleEmitterLine::OvrParticleEmitterLine(const OvrVector3& a_begin_pt, const OvrVector3& a_end_pt)
	:OvrParticleEmitter()
	,begin_pt(a_begin_pt)
	,end_pt(a_end_pt)
{

}

void ovr_engine::OvrParticleEmitterLine::SetBeginPt(const OvrVector3& a_begin_pt)
{
	begin_pt = a_begin_pt;
}

void ovr_engine::OvrParticleEmitterLine::SetEndPt(const OvrVector3& a_end_pt)
{
	end_pt = a_end_pt;
}

const OvrVector3& ovr_engine::OvrParticleEmitterLine::GetBeginPt() const
{
	return begin_pt;
}

const OvrVector3& ovr_engine::OvrParticleEmitterLine::GetEndPt() const
{
	return end_pt;
}

OvrParticleEmitterLine::~OvrParticleEmitterLine()
{

}

void OvrParticleEmitterLine::EmitterParticle(OvrParticle* ao_particle)
{
	OvrParticle& t_p = *ao_particle;
	float t_w = cos(rad);
	float t_h = sin(rad);
	OvrVector3 init_dir = OvrVector3(OvrRandom(-t_w, t_w), OvrRandom(t_h, 1.0f), OvrRandom(-t_w, t_w)).get_normalize();
	t_p.cur_v = init_dir*OvrRandom(min_speed, max_speed);
	float t_angle = OvrRandom(0.0f, 1.0f);
	t_p.position = OvrLerp(begin_pt, end_pt, t_angle);
	t_p.life_total = OvrRandom(min_life, max_life);
	t_p.scale_size = OvrRandom(min_size, max_size);
}
