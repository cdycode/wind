#include "wx_engine/ovr_shape_component.h"
#include "wx_engine/ovr_actor.h"
using namespace ovr_engine;

ovr_engine::OvrShapeComponent::OvrShapeComponent(OvrActor* a_actor)
	:OvrPrimitiveComponent(a_actor)
{
	collision_enabled = true;
}

ovr_engine::OvrShapeComponent::~OvrShapeComponent()
{

}
