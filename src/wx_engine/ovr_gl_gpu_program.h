#pragma once
#include <vector>
#include <map>
#include "wx_base/wx_string.h"
#include "wx_engine/ovr_gpu_program.h"
#include "ovr_gl_define.h"
namespace ovr_engine
{	
	class OvrGpuProgramManager;
	class OvrGLShader;
	class OvrGLGpuProgramUtil
	{
	public:
		static OvrString GetUniformName(const char* a_name);
		static int	GetTextureLayerUsage(const OvrString& a_tex_name);
		static OvrGpuProgramConstantType GetProgramConstantType(int a_type);
	};
	class OvrGLGpuProgram : public OvrGpuProgram
	{
	public:
		struct UniformReference
		{
			ovr::uint32 location;
			OvrString   name;
			int			extra;
			const OvrConstant* constant;
		};
	public:
		OvrGLGpuProgram(OvrString a_name);
		virtual ~OvrGLGpuProgram();
		virtual OvrString GetName() override;
		virtual void SetShaders(OvrGLShader* a_vs, OvrGLShader* a_fs);
		virtual void Reset();
		virtual void BindProgram() override;
		virtual void UnBindProgram() override;
		virtual void BindProgramParameters(OvrGpuProgramParameters* params, ovr::uint16 mask) override;
		virtual void SetTextureUnit(ovr::uint32 a_slot, OvrTextureUnit* a_tex_unit);
	protected:
		void    CompileAndLink();
		GLuint	GetGLTextureFliter(const OvrTextureFilter& a_filter);
		GLuint	GetGLTextureAddressMode(const OvrTextureAddressMode& a_mode);
	protected:
		std::vector<UniformReference> uniform_set;
		OvrGLShader* vs;
		OvrGLShader* ps;
		GLuint       program_id;
		GLenum      texture_types[8];

	};
}