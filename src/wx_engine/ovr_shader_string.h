#pragma once;
namespace ovr_engine 
{
	extern const char* vs_standard_with_light;
	extern const char* skinned_vs_standard_with_light;
	extern const char* pointvs;
	extern const char* directvs;
	extern const char* gc_skinned_ani_vs;
	extern const char* g_SingleTextureVertexShaderSrcMovTexcoord;
	extern const char* ovr_singleTex_with_transparent_vs;

	extern const char* ps_standard_with_light;
	extern const char* psTest_transparent;
	extern const char* pointps;	
	extern const char* directps;
	extern const char* g_SingleTextureFragmentShaderSrcWithColor;
	extern const char* g_SingleTextureFragmentShaderSrcExternalOES;
	extern const char* ovr_singleTex_with_transparent_ps;
	extern const char* ovr_particle_vs;
	extern const char* ovr_particle_ps;

	extern const char* ovr_skybox_vs;
	extern const char* ovr_skybox_ps;

	extern const char* vs_standard_with_normal_tex;
	extern const char* ps_standard_with_normal_tex;

	extern const char* vs_glass_str;
	extern const char* ps_galss_str;

	extern const char* ps_common;

	extern const char* vertex_color_vs;
	extern const char* vertex_color_ps;


	extern const char* single_texture_vs;
	extern const char* single_texture_ps;

	extern const char* simple_vs;
	extern const char* simple_ps;

	extern const char* font_vs;
	extern const char* font_ps;

	extern const char* axis_world_vs;
	extern const char* axis_world_ps;

}