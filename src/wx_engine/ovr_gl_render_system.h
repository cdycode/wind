#pragma once
#include <map>
#include "wx_engine/ovr_render_system.h"
#include "ovr_gl_define.h"
namespace ovr_engine
{
	class OvrGLRenderSystem : public OvrRenderSystem
	{		
	public:

		OvrGLRenderSystem();
		virtual ~OvrGLRenderSystem();
		virtual bool Initialise() override;
		virtual void UnInitialise() override;
		virtual bool BeginScene(bool a_backBuffer, bool a_zBuffer, OvrColorValue a_color, Viewport a_rect) override;
		virtual bool EndScene() override;
		virtual void Render(OvrRenderOperation& a_op) override;
		virtual void SetViewPort(const Viewport& a_v) override;
		virtual void BindGpuProgram(OvrGpuProgram* a_program);
		virtual void UnBindGpuProgram();
		virtual void BindGpuProgramParameters(OvrGpuProgramParameters* params, ovr::uint16 mask);
		virtual void SetSceneBlend(SceneBlendFactor a_src, SceneBlendFactor a_dest);
		virtual void SetDepthCheckEnable(bool a_enable);
		virtual void SetDepthWriteEnable(bool a_enable);
		virtual void SetCullMode(CullingMode a_mode);
		virtual void ClearBuffers(bool backBuffer, bool zBuffer, bool stencilBuffer, OvrColorValue color) override;
		GLint GetBlendMode(SceneBlendFactor a_factor);
		virtual void Draw3DLine(const std::vector<OvrVector3>& a_vertices, const OvrMatrix4& a_view, const OvrMatrix4& a_proj, const float a_width,
			const OvrColorValue a_color = OvrColorValue(255, 255, 255, 255), bool is_strip = true) override;
		virtual void Draw3DPoint(const std::vector<OvrVector3>& a_vertices, const OvrMatrix4& a_view, const OvrMatrix4& a_proj, const float a_size,
			const OvrColorValue a_color = OvrColorValue(255, 255, 255, 255)) override;
	protected:
		GLenum      texture_types[8];
	};
}
