#include <cmath>
#include "wx_engine/ovr_particle_emitter_point.h"
#include "wx_engine/ovr_particle_class_info.h"
using namespace ovr_engine;
using namespace  ovr;
REGISTER_CLASS(OvrParticleEmitterPoint)
OvrParticleEmitterPoint::OvrParticleEmitterPoint()
	:OvrParticleEmitter()
{

}

OvrParticleEmitterPoint::~OvrParticleEmitterPoint()
{

}

void OvrParticleEmitterPoint::EmitterParticle(OvrParticle* ao_particle)
{
	ao_particle->position = pos;
	ao_particle->direction = main_dir;
	float t_max_vaule = tan(rad);
	OvrVector3 v0(main_dir[1], main_dir[2], main_dir[0]);
	OvrVector3 t_axis0 = v0.cross(main_dir);
	t_axis0.normalize();
	OvrVector3 t_axis1 = t_axis0.cross(main_dir);
	t_axis1.normalize();
	OvrVector3 t_rand_circle(OvrRandom(-1.0f, 1.0f)*t_max_vaule, 1.0, OvrRandom(-1.0f, 1.0f)*t_max_vaule);
	t_rand_circle.normalize();
	OvrVector3 t_target_dir = t_axis0*t_rand_circle[0] + main_dir*t_rand_circle[1] + t_axis1*t_rand_circle[2];
	ao_particle->cur_v = t_target_dir*OvrRandom(min_speed, max_speed);
	ao_particle->life_now = 0;
	ao_particle->life_total = OvrRandom(min_life, max_life);
	ao_particle->offset_pos = OvrVector3(0, 0, 0);
	ao_particle->scale_size = OvrRandom(min_size, max_size);
	ao_particle->weight = 1.0;
	ao_particle->color = color;
}

void OvrParticleEmitterPoint::SetScatterAngle(float a_angle)
{
	rad = a_angle;
}

void ovr_engine::OvrParticleEmitterPoint::SetPointPos(const OvrVector3& a_pos)
{
	pos = a_pos;
}

const OvrVector3& ovr_engine::OvrParticleEmitterPoint::GetPointPos() const
{
	return pos;
}
