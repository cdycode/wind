#include "wx_engine/ovr_scene.h"
#include "wx_engine/ovr_actor.h"
//#include "lvr_math_configure.h"
#include "wx_engine/ovr_mesh_component.h"
#include "wx_engine/ovr_light_component.h"
#include "wx_engine/ovr_camera_component.h"
#include "wx_engine/ovr_box_component.h"
namespace ovr_engine
{
	OvrActor::OvrActor(OvrScene* a_scene)
		: is_need_update(true)
		, parent(nullptr)
		, scene(a_scene)
		, is_visable(true)
		, show_bounding_box(false)
		, is_static(false)
	{
	}

	OvrActor::~OvrActor()
	{
		RemoveAllComponents();
	}

	OvrScene* OvrActor::GetScene()
	{
		return scene;
	}

	void OvrActor::SetUniqueName(const char* a_name)
	{
		OvrString old_name = GetUniqueName();
		std::map<OvrString, OvrActor*>& actor_map = scene->actor_map;
		std::map<OvrString, OvrActor*>::iterator it = actor_map.find(old_name);
		actor_map.erase(it);
		actor_map[a_name] = this;
		unique_name = a_name;
	}

	void OvrActor::Update(bool a_force_update)
	{
		bool updated = a_force_update || is_need_update;
		if (updated)
		{
			transform.GetMatrix(local_matrix);
			if (parent)
				world_matrix = local_matrix * parent->GetWorldTransform();
			else
				world_matrix = local_matrix;
			is_need_update = false;
		}

		for (ovr::uint32 i = 0; i < components.size(); i++)
		{
			if(components[i]->IsActive())
				components[i]->Update();
		}

		UpdateChild(updated);
	}

	void OvrActor::Yaw(float a_angle, OvrTransformSpace a_space)
	{
		Rotate(OvrVector3::UNIT_Y, a_angle, a_space);
	}

	void OvrActor::Roll(float a_angle, OvrTransformSpace a_space)
	{
		Rotate(OvrVector3::UNIT_Z, a_angle, a_space);
	}

	void OvrActor::Pitch(float a_angle, OvrTransformSpace a_space)
	{
		Rotate(OvrVector3::UNIT_X, a_angle, a_space);
	}

	void OvrActor::Rotate(const OvrVector3& a_axis, const float a_angle, OvrTransformSpace a_space)
	{
		OvrQuaternion q(a_axis, a_angle);
		Rotate(q, a_space);
	}

	void OvrActor::Rotate(const OvrQuaternion a_quat, OvrTransformSpace a_space)
	{
		switch (a_space)
		{
		case kSpaceParent:
			SetQuaternion(transform.quaternion * a_quat);
			break;
		case kSpaceLocal:
			SetQuaternion(a_quat * transform.quaternion);
			break;
		}
	}

	//���ص��ǽǶ�
	void OvrActor::GetEulerAngle(float& a_pitch, float& a_yaw, float& a_roll)
	{
		transform.quaternion.get_euler_angles(a_pitch, a_yaw, a_roll);
	}

	OvrQuaternion OvrActor::GetWorldQuaternion()
	{
		OvrQuaternion quat;
		world_matrix.decompose(nullptr, &quat, nullptr);
		return quat;
	}

	OvrVector3 OvrActor::GetWorldPosition()
	{
		OvrVector3 pos;
		world_matrix.decompose(nullptr, nullptr, &pos);
		return pos;
	}

	void OvrActor::SetTransform(const OvrTransform& a_transform)
	{
		transform = a_transform;
		is_need_update = true;
	}

	void OvrActor::SetPosition(const OvrVector3& a_position)
	{ 
		transform.position = a_position;
		is_need_update = true;
	}

	void OvrActor::SetScale(const OvrVector3& a_scale)
	{ 
		transform.scale = a_scale;
		is_need_update = true;
	}

	void OvrActor::SetQuaternion(const OvrQuaternion& a_quaternion) 
	{ 
		transform.quaternion = a_quaternion;
		is_need_update = true;
	}

	void OvrActor::ResetTransform()
	{
		transform.position = OvrVector3(0.0f, 0.0f, 0.0f);
		transform.scale = OvrVector3(1.0f, 1.0f, 1.0f);
		transform.quaternion = OvrQuaternion();
	}

	void OvrActor::SetEulerAngle(float a_pitch, float a_yaw, float a_roll)
	{
		transform.quaternion = OvrQuaternion::make_euler_quat((float)(a_pitch / 180.0f*OVR_PI), (float)(a_yaw / 180.0f*OVR_PI), (float)(a_roll / 180.0f*OVR_PI));
		is_need_update = true;
	}

	bool OvrActor::MoveForwardBack(const float a_distance)
	{
		OvrVector3 right = (GetQuaternion()*OvrVector3::UNIT_Y).cross(GetQuaternion()*(OvrVector3::UNIT_Z));
		OvrVector3 t_moving = (GetQuaternion()*OvrVector3::UNIT_Y).cross(right);
		t_moving.normalize();
		SetPosition(transform.position - t_moving*a_distance);
		return true;
	}

	bool OvrActor::MoveLeftRight(const float a_distance)
	{
		OvrVector3 t_moving = (GetQuaternion()*OvrVector3::UNIT_Y).cross(GetQuaternion()*(OvrVector3::UNIT_Z));

		t_moving.normalize();
		SetPosition(transform.position + t_moving*a_distance);
		return true;
	}

	void OvrActor::SetParent(OvrActor* a_actor)
	{
		parent = a_actor;
		is_need_update = true;
	}

	void OvrActor::UpdateChild(bool a_force_update)
	{
		for (ovr::uint32 i = 0; i < child_list.size(); i++)
		{
			child_list[i]->Update(a_force_update);
		}
	}

	OvrActor* OvrActor::GetChild(ovr::uint16 a_index)
	{
		if (a_index < child_list.size())
		{
			std::vector<OvrActor*>::iterator it = child_list.begin();
			it += a_index;
			return *it;
		}
		return nullptr;
	}

	OvrActor* OvrActor::GetChild(const OvrString& a_name)
	{
		
		for (ovr::uint32 i = 0; i < child_list.size(); i++)
		{
			if (child_list[i]->GetDisplayName() == a_name)
			{
				return child_list[i];
			}
		}
		return nullptr;
	}

	ovr::uint32 OvrActor::GetChildCount()
	{
		return child_list.size();
	}


	bool OvrActor::AddChild(OvrActor* a_actor, bool a_world_pos_stays)
	{
		if (a_world_pos_stays)
		{
			OvrMatrix4 world_matrix_inv;
			world_matrix_inv = world_matrix;
			world_matrix_inv.inverse();
		
			OvrQuaternion world_quat_inv;
			world_matrix_inv.decompose(nullptr, &world_quat_inv, nullptr);
			a_actor->SetPosition(world_matrix_inv.transform_point(a_actor->GetWorldPosition()));
			a_actor->SetQuaternion(a_actor->GetWorldQuaternion() * world_quat_inv);
		}	

		if (a_actor->GetParent())
		{
			a_actor->GetParent()->RemoveChild(a_actor);
		}
		child_list.push_back(a_actor);
		a_actor->SetParent(this);
		return true;
	}

	OvrActor* OvrActor::CreateChildActor()
	{
		OvrActor* new_actor = scene->CreateActor();
		AddChild(new_actor);
		return new_actor;
	}

	OvrActor* OvrActor::RemoveChild(ovr::uint16 a_index)
	{
		if (a_index < child_list.size())
		{
			std::vector<OvrActor*>::iterator it = child_list.begin();
			it += a_index;
			OvrActor* ret = *it;
			child_list.erase(it);
			ret->SetParent(nullptr);
			return ret;
		}
		return nullptr;
	}

	OvrActor* OvrActor::RemoveChild(OvrActor* a_child)
	{
		if (a_child)
		{
			std::vector<OvrActor*>::iterator it = child_list.begin();
			for (; it != child_list.end(); it++)
			{
				if (*it == a_child)
				{
					child_list.erase(it);
					a_child->SetParent(nullptr);
					break;
				}
			}
		}
		return a_child;
	}

	OvrActor* OvrActor::RemoveChild(const OvrString& a_name)
	{
		OvrActor* ret = nullptr;
		std::vector<OvrActor*>::iterator it = child_list.begin();
		for (; it != child_list.end(); it++)
		{
			if ((*it)->GetDisplayName() == a_name)
			{
				ret = *it;
				child_list.erase(it);
				ret->SetParent(nullptr);
			}
		}
		return ret;
	}

	void OvrActor::RemoveAllChild()
	{
		std::vector<OvrActor*>::iterator it = child_list.begin();
		for (; it != child_list.end(); it++)
		{
			(*it)->SetParent(nullptr);
		}
		child_list.clear();
	}

	void OvrActor::FindVisibleObjects(OvrRenderQueue* a_queue)
	{
		if (!IsVisible())
			return;

		std::vector<OvrPrimitiveComponent*> primitives = GetComponents<OvrPrimitiveComponent>();
		for(ovr::uint32 i = 0; i < primitives.size(); i++)
		{
			if(primitives[i]->IsActive())
				primitives[i]->UpdateRenderQueue(a_queue);
		}

		ChildList::iterator it = child_list.begin();
		for (; it != child_list.end(); ++it)
		{
			(*it)->FindVisibleObjects(a_queue);
		}
	}

	bool OvrActor::HasParent()
	{
		return nullptr != parent;
	}

	void OvrActor::RemoveComponent(OvrComponent* a_component)
	{
		std::vector<OvrComponent*>::iterator it = components.begin();
		for (; it != components.end(); it++)
		{
			if (*it == a_component)
			{
				scene->NotifyComponentDestroyed(*it);
				delete *it;
				components.erase(it);
				break;
			}
		}
		return;
	}

	void OvrActor::RemoveAllComponents()
	{
		std::vector<OvrComponent*>::iterator it = components.begin();
		while (it != components.end())
		{
			scene->NotifyComponentDestroyed(*it);
			delete *it;			
			it++;
		}
		components.clear();
	}

	
}

