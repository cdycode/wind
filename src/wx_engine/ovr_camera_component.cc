#include "wx_engine/ovr_camera_component.h"
#include "lvr_plane3.h"
#include "wx_engine/ovr_actor.h"
using namespace ovr_engine;

ovr_engine::OvrCameraComponent::OvrCameraComponent(OvrActor* a_actor)
	:OvrComponent(a_actor)
	, field_view(90.0f*3.1415926535898f / 180.0f)
	, aspect_ratio(1)
	, near_dist(0.01f)
	, far_dist(2000.0f)
	, render_path(kForward)
{

}

ovr_engine::OvrCameraComponent::~OvrCameraComponent()
{

}

void ovr_engine::OvrCameraComponent::Update()
{
	camera_position = parent_actor->GetWorldPosition();
	OvrQuaternion tt = parent_actor->GetWorldQuaternion();
	BuildCameraLookAtMatrixLH(camera_position, tt*(-OvrVector3::UNIT_Z), tt*(OvrVector3::UNIT_Y));
}


void ovr_engine::OvrCameraComponent::BuildCameraLookAtMatrixLH(const OvrVector3& a_position, const OvrVector3& a_target, const OvrVector3& a_up)
{
	view_matrix = OvrMatrix4::make_view_matrix(a_position, a_position + a_target, a_up);
}

void ovr_engine::OvrCameraComponent::BuildProjectionMatrixPerspectiveFovLH(float a_field_view_rad, float a_aspect_ratio, float a_near, float a_far)
{
	project_matrix = OvrMatrix4::make_proj_matrix_rh(a_field_view_rad, a_aspect_ratio, a_near, a_far);
}

void ovr_engine::OvrCameraComponent::SetFieldView(float a_field_view_rad)
{
	field_view = a_field_view_rad;
	BuildProjectionMatrixPerspectiveFovLH(field_view, aspect_ratio, near_dist, far_dist);
}

void ovr_engine::OvrCameraComponent::SetAspectRatio(float a_aspect_ratio)
{
	aspect_ratio = a_aspect_ratio;
	BuildProjectionMatrixPerspectiveFovLH(field_view, aspect_ratio, near_dist, far_dist);
}

void ovr_engine::OvrCameraComponent::SetNearClip(float a_near)
{
	near_dist = a_near;
	BuildProjectionMatrixPerspectiveFovLH(field_view, aspect_ratio, near_dist, far_dist);
}

void ovr_engine::OvrCameraComponent::SetFarClip(float a_far)
{
	far_dist = a_far;
	BuildProjectionMatrixPerspectiveFovLH(field_view, aspect_ratio, near_dist, far_dist);
}

void ovr_engine::OvrCameraComponent::SetPerspective(float a_field_view_rad, float a_aspect_ratio, float a_near, float a_far)
{
	field_view = a_field_view_rad;
	aspect_ratio = a_aspect_ratio;
	near_dist = a_near;
	far_dist = a_far;
	BuildProjectionMatrixPerspectiveFovLH(a_field_view_rad, a_aspect_ratio, a_near, a_far);
}

OvrRay3f ovr_engine::OvrCameraComponent::GeneratePickRay(float a_x, float a_y)
{
	OvrRay3f tray;
	OvrVector3 near_point;
	Unproject(a_x, a_y, 0, near_point);
	OvrVector3 far_point;
	Unproject(a_x, a_y, 1, far_point);
	tray.SetOrigin(near_point);
	OvrVector3 direction = far_point - near_point;
	direction.normalize();
	tray.SetDir(direction);
	return tray;
}

void ovr_engine::OvrCameraComponent::GetPlacePosition(float a_mouse_x, float a_mouse_y, OvrVector3& a_position)
{
	OvrRay3f ray = this->GeneratePickRay(a_mouse_x, a_mouse_y);

	lvr_plane3f plane(lvr_vector3f(0.0f, 1.0f, 0.0f), 0.0f);
	float distance = 0.0f;
	plane.intersect(distance, lvr_vector3f(ray.GetOrigin()[0], ray.GetOrigin()[1], ray.GetOrigin()[2]), lvr_vector3f(ray.GetDir()[0], ray.GetDir()[1], ray.GetDir()[2]));

	a_position = ray.GetOrigin() + ray.GetDir()*distance;
}

void OvrCameraComponent::Unproject(float a_x, float a_y, float a_depth, OvrVector3& a_dst)
{
	OvrVector4 tmp_point(a_x*2.0f - 1.0f, a_y*2.0f - 1.0f, a_depth*2.0f - 1.0f, 1.0f);
	OvrMatrix4 view_proj_mat = GetViewMatrix()*GetProjectMatrix();
	OvrMatrix4 inv_mat;
	view_proj_mat.get_inverse(inv_mat);
	tmp_point = inv_mat*tmp_point;
	float t_inv3 = 1.0f / tmp_point[3];
	a_dst = OvrVector3(tmp_point[0] * t_inv3, tmp_point[1] * t_inv3, tmp_point[2] * t_inv3);

}
