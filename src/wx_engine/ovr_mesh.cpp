#include "wx_engine/ovr_mesh.h"
#include "wx_engine/ovr_mesh_buffer.h"
#include "wx_engine/ovr_material.h"
#include "wx_engine/ovr_skeleton_manager.h"
#include <wx_asset_manager/ovr_archive_asset_manager.h>
#include <wx_asset_manager/ovr_archive_pose.h>
#include "wx_engine/ovr_logger.h"
#include "wx_engine/ovr_pose.h"
#include "wx_engine/ovr_serializer_impl.h"
using namespace ovr_engine;
ovr_engine::OvrMesh::OvrMesh(OvrString a_name, bool a_is_manual)
	: OvrResource(a_name, a_is_manual)
	, skeleton(nullptr)
{
	mesh_buffer_list.clear();
	pose_list.clear();
}

OvrMesh::~OvrMesh()
{
	UnLoad();
}

OvrString OvrMesh::GetMaterialName(ovr::uint32 a_index)
{
	if (a_index >= mesh_buffer_list.size())
	{
		return "";
	}

	return mesh_buffer_list[a_index]->GetMaterialName();
}

void ovr_engine::OvrMesh::SetMaterialName(OvrString a_material_name)
{
	for (ovr::uint32 i = 0; i < mesh_buffer_list.size(); i++)
	{
		mesh_buffer_list[i]->SetMaterialName(a_material_name);
	}
}

void ovr_engine::OvrMesh::SetMaterialName(ovr::uint32 a_index, const OvrString& a_material_name)
{
	if (a_index >= mesh_buffer_list.size())
	{
		return;
	}

	mesh_buffer_list[a_index]->SetMaterialName(a_material_name);
}

OvrMeshBuffer* ovr_engine::OvrMesh::GetMeshBuffer(ovr::uint32 a_index)
{
	if (a_index >= mesh_buffer_list.size())
	{
		return nullptr;
	}

	return mesh_buffer_list[a_index];
}

OvrMeshBuffer* ovr_engine::OvrMesh::GetMeshBuffer(OvrString a_name)
{
	std::map<OvrString, ovr::uint32>::iterator iter = name_buffer_map.find(a_name);
	if (iter !=  name_buffer_map.end())
	{
		return mesh_buffer_list[iter->second];
	}
	return nullptr;
}

ovr::uint32 OvrMesh::GetMeshBufferIndex(const OvrString& a_name) const
{
	NameBufferMap::const_iterator it = name_buffer_map.find(a_name);
	if (it == name_buffer_map.end())
	{	
		OvrLogger::GetIns()->Log(OvrString("No Mesh Buffer named ") + name + OvrString(" found."), LL_ERROR);
	}
	return it->second;
}

ovr::uint32 ovr_engine::OvrMesh::GetMeshBufferCount()
{
	return mesh_buffer_list.size();
}

void ovr_engine::OvrMesh::SetSkeletonName(OvrString a_name)
{
	skeleton = OvrSkeletonManager::GetIns()->Load(a_name);
	skeleton->Grab();
}

void ovr_engine::OvrMesh::SetPoseName(OvrString a_name)
{
	pose_list.clear();
	pose_name = a_name;
}

bool ovr_engine::OvrMesh::HasSkeleton()
{
	return (nullptr != skeleton)? true: false;
}

OvrSkeleton* ovr_engine::OvrMesh::GetSkeleton()
{
	return skeleton;
}

bool ovr_engine::OvrMesh::HasPose()
{
	return 0 != pose_list.size();
}

OvrPose* ovr_engine::OvrMesh::GetPose(ovr::uint32 a_index)
{
	if (a_index < pose_list.size())
	{
		return pose_list[a_index];
	}
	return nullptr;
}

const OvrMesh::PoseList& ovr_engine::OvrMesh::GetPoseList()
{
	return pose_list;
}

ovr::uint32 ovr_engine::OvrMesh::GetPoseCount()
{
	return pose_list.size();
}

OvrPose* ovr_engine::OvrMesh::CreatePose(ovr::uint32 handle, OvrString a_name)
{
	OvrPose* pose = new OvrPose(handle, a_name);
	pose_list.push_back(pose);
	return pose;
}



OvrPose* ovr_engine::OvrMesh::CreatePose(OvrString a_target_name, OvrString a_name)
{
	OvrPose* pose = new OvrPose(a_target_name, a_name);
	pose_list.push_back(pose);
	return pose;
}

OvrMeshBuffer* ovr_engine::OvrMesh::CreateMeshBuffer(OvrString a_name)
{
	OvrMeshBuffer* buf = CreateMeshBuffer();
	name_buffer_map[a_name] = mesh_buffer_list.size() - 1;
	buf->Grab();
	return buf;

}

OvrMeshBuffer* ovr_engine::OvrMesh::CreateMeshBuffer()
{
	OvrMeshBuffer* buf = new OvrMeshBuffer(this);
	mesh_buffer_list.push_back(buf);
	return buf;
}

void ovr_engine::OvrMesh::LoadImpl()
{
	OvrSerializerImpl::GetIns()->ImportMesh(name, this);
}

void ovr_engine::OvrMesh::UnLoadImpl()
{
	for (ovr::uint32 i = 0; i < mesh_buffer_list.size(); i++)
	{
		mesh_buffer_list[i]->Drop();
	}
	mesh_buffer_list.clear();

	if (skeleton)
	{
		skeleton->Drop();
		skeleton = nullptr;
	}

	pose_list.clear();
}
