#include "wx_engine/ovr_mesh_component.h"
#include "wx_engine/ovr_mesh.h"
#include "wx_engine/ovr_bounding_box.h"
#include "wx_engine/ovr_material.h"
#include "wx_engine/ovr_material_manager.h"
#include "wx_engine/ovr_logger.h"
#include "wx_engine/ovr_bounding_box.h"
#include "wx_engine/ovr_actor.h"
#include "wx_engine/ovr_mesh_renderable.h"
#include "wx_engine/ovr_bounding_box.h"
using namespace ovr_engine;

ovr_engine::OvrMeshComponent::OvrMeshComponent(OvrActor* a_actor)
	: OvrPrimitiveComponent(a_actor)
	, mesh(nullptr)
	, wire_bounding_box(nullptr)
	, mesh_state_count(0)
{
	rendering_enabled = true;
	is_shadow_cast = true;
	priority_id = OVR_RENDERABLE_DEFAULT_PRIORITY;
}

ovr_engine::OvrMeshComponent::~OvrMeshComponent()
{
	if (mesh)
	{
		mesh->Drop();
	}
	if (wire_bounding_box)
	{
		delete wire_bounding_box;
	}
}

void ovr_engine::OvrMeshComponent::UpdateRenderQueue(OvrRenderQueue* a_queue)
{
	if (!rendering_enabled)
		return;

	if (mesh->GetStateCount() != mesh_state_count)
	{
		SetMesh(mesh);
	}

	for (ovr::uint32 i = 0; i < renderable_list.size(); i++)
	{
		a_queue->AddRenderable(renderable_list[i], priority_id);
	}

	if (parent_actor->GetShowBoundingBox())
	{
		if (!wire_bounding_box)
			wire_bounding_box = new OvrBoundingBox(parent_actor);
		wire_bounding_box->SetupBoundingBox(GetBoundingBox());
		a_queue->AddRenderable(wire_bounding_box);
	}
}

OvrMesh* ovr_engine::OvrMeshComponent::GetMesh() const
{
	return mesh;
}


bool ovr_engine::OvrMeshComponent::HasPose()
{
	return mesh->HasPose();
}

OvrBox3f ovr_engine::OvrMeshComponent::GetBoundingBox()
{
	static OvrBox3f box;
	if(mesh)
		return mesh->GetBounds();
	return box;
}

void ovr_engine::OvrMeshComponent::SetMaterialCount(ovr::uint32 a_num)
{
	ovr::uint32 num = material_list.size();
	if (a_num == num)
	{
		return;
	}

	if (a_num < num)
	{
		ovr::uint32 count = num - a_num;
		ovr::uint32 index = num - 1;
		while (count > 0)
		{
			OvrMaterial* material = material_list[index];
			if (material)
			{
				material->Drop();
			}
			index--;
			count--;
		}
	}
	material_list.resize(a_num);
}

void ovr_engine::OvrMeshComponent::SetMaterial(ovr::uint32 a_index, OvrMaterial* a_material)
{
	OvrMaterial* material = GetMaterial(a_index);
	if (material)
	{
		material->Drop();
		if (a_material)
			a_material->Grab();
		material_list[a_index] = a_material;
	}	
}

OvrMaterial* ovr_engine::OvrMeshComponent::GetMaterial(ovr::uint32 a_index)
{
	if (a_index < GetMaterialCount())
	{
		return material_list[a_index];
	}
	return nullptr;
}

ovr::uint32 ovr_engine::OvrMeshComponent::GetMaterialCount()
{
	return material_list.size();
}

void ovr_engine::OvrMeshComponent::SetRenderQueueGroupPriority(ovr::uint16 a_priority)
{
	priority_id = a_priority;
}


bool ovr_engine::OvrMeshComponent::UpdateAnimation(ovr_asset::OvrMorphFrame* a_frame)
{
	if (nullptr == a_frame)
	{
		OvrLogger::GetIns()->Log("motion frame is null.", LL_WARNING);
		return false;
	}

	for (ovr::uint32 i = 0; i < renderable_list.size(); i++)
	{
		renderable_list[i]->UpdateAnimation(a_frame);
	}
	return true;
}

void ovr_engine::OvrMeshComponent::BuildRenderableList()
{
	ResetRenderableList();

	for (ovr::uint32 i = 0; i < mesh->GetMeshBufferCount(); i++)
	{
		OvrMeshBuffer* mb = mesh->GetMeshBuffer(i);
		OvrMaterial* material = OvrMaterialManager::GetIns()->GetDefaultMaterial();
		if (mb->IsMaterialSet())
		{
			material = OvrMaterialManager::GetIns()->Load(mb->GetMaterialName());
		}
		ovr::uint32 material_index = AddMaterial(material);
		OvrMeshRenderable* rnd = new OvrMeshRenderable(this, mb, material_index);
		renderable_list.push_back(rnd);
	}
}

void ovr_engine::OvrMeshComponent::ResetRenderableList()
{
	for (ovr::uint32 i = 0; i < renderable_list.size(); i++)
	{
		delete renderable_list[i];
	}

	for (ovr::uint32 i = 0; i < material_list.size(); i++)
	{
		material_list[i]->Drop();
	}
	renderable_list.clear();
	material_list.clear();
}

ovr::uint32 ovr_engine::OvrMeshComponent::AddMaterial(OvrMaterial* a_material)
{
	ovr::uint32 i = 0;
	for (; i < material_list.size(); i++)
	{
		if (material_list[i] == a_material)
		{
			break;
		}
	}
	if (i == material_list.size())
	{
		a_material->Grab();
		material_list.push_back(a_material);
	}
	return i;
}

int ovr_engine::OvrMeshComponent::Raycast(const OvrRay3f& a_ray, float& a_distance)
{
	return GetWorldBoundingBox().ray_interset(a_ray, a_distance);
}