﻿#include <vector>
#include "headers.h"
#include "wx_engine/ovr_scene.h"
#include "wx_engine/ovr_render_queue.h"
#include "wx_engine/i_ovr_ui_text_manager.h"
#include "wx_engine/ovr_ray_scene_query.h"
#include "wx_engine/ovr_particle_component.h"
#include "wx_engine/ovr_sky_box.h"
#include "wx_engine/ovr_light_component.h"
#include "wx_engine/ovr_camera_component.h"
#include "wx_engine/ovr_render_texture.h"
#include "wx_engine/ovr_material.h"
#include "wx_engine/ovr_render_system.h"
namespace ovr_engine 
{
	static const OvrVector3 g_look[6] = {
		OvrVector3(1.0f, 0.0f, 0.0f),
		OvrVector3(-1.0f, 0.0f, 0.0f),
		OvrVector3(0.0f, 1.0f, 0.0f),
		OvrVector3(0.0f, -1.0f, 0.0f),
		OvrVector3(0.0f, 0.0f, 1.0f),
		OvrVector3(0.0f, 0.0f, -1.0f) };
	static const OvrVector3 g_up[6] = {
		OvrVector3(0.0f, -1.0f, 0.0f),
		OvrVector3(0.0f, -1.0f, 0.0f),
		OvrVector3(0.0f, 0.0f, 1.0f),
		OvrVector3(0.0f, 0.0f, -1.0f),
		OvrVector3(0.0f, -1.0f, 0.0f),
		OvrVector3(0.0f, -1.0f, 0.0f) };

	OvrScene::OvrScene(OvrRenderSystem* a_rs, IOvrUiTextManager* a_ui_mgr)
		: render_system(a_rs)
		, ui_text_mgr(a_ui_mgr)
		, main_camera(nullptr)
		, enable_shadow(false)
		, render_queue(nullptr)
		, collector_visitor(nullptr)
		, is_prepare_shadow_tex(false)
		, cur_shadow_caster_light(nullptr)
		, sky_box(nullptr)
	{
		UniqueNameCreater::CreateSenceUniqueName(unique_name);
		root_actor = new OvrActor(this);
	}

	void OvrScene::PrepareRenderQueue()
	{
		if (!render_queue)
		{
			render_queue = new OvrRenderQueue;
			collector_visitor = new SceneRenderableCollectorVistor;
			
		}
		render_queue->Clear();
		collector_visitor->target_scene = this;
	}

	void OvrScene::EnsureTextureShadow()
	{
		if (is_prepare_shadow_tex)
			return;

		OvrString shadow_mtls[4] = { "Ps","SPs","Ds","SDs" };
		OvrString shadow_mtl_shaders[4] = { "ShadowCasterPoint" ,"ShadowCasterPointSkin","ShadowCasterDirection","ShadowCasterDirectionSkin" };
		shadow_caster_material.resize(4);
		for (int i = 0; i < 4; ++i)
		{
			shadow_caster_material[i] = OvrMaterialManager::GetIns()->Create(shadow_mtls[i]);
			shadow_caster_material[i]->SetProgram(shadow_mtl_shaders[i]);
		}
		int resolution = 1024;

		shadow_tex = OvrTextureManager::GetIns()->Create(
			GetUniqueName() + OvrString("inside_tex_shadow"),
			TEXTURE_TYPE_2D, resolution, resolution, PF_DEPTH24,0, true);
		spot_tex_array = OvrTextureManager::GetIns()->Create(
			GetUniqueName() + OvrString("inside_tex_array"), 
			TEXTURE_TYPE_2D_ARRAY, resolution, resolution,PF_DEPTH24, 32, true);
		point_tex_array = OvrTextureManager::GetIns()->Create(
			GetUniqueName() + OvrString("inside_tex_cube_array"), 
			TEXTURE_TYPE_CUBE_ARRAY, 512, 512, PF_R8G8B8A8,32, true);
		is_prepare_shadow_tex = true;
	}

	OvrScene::~OvrScene()
	{
		for (auto actor : actor_map)
		{
			delete actor.second;
		}
		actor_map.clear();
		delete root_actor;
	}

	OvrMaterial* OvrScene::DeriveShadowCasterMaterial(OvrMaterial* a_material)
	{
		OvrMaterial* ret = nullptr;
		if (cur_shadow_caster_light->GetLightType() == ePointLight)
		{
			if (a_material->IsSkeletonInclude())
				ret = shadow_caster_material[1];
			else
				ret = shadow_caster_material[0];
		}
		else
		{
			if (a_material->IsSkeletonInclude())
				ret = shadow_caster_material[3];
			else
				ret = shadow_caster_material[2];
		}
		return ret;
	}

	bool OvrScene::ValidateRenderableForRendering(OvrMaterial* a_material, OvrRenderable* a_rnd)
	{
		if (!a_material || !a_rnd)
			return false;

		if (cur_shadow_caster_light)
		{
			if (!a_rnd->GetCastShadowEnable())
			{
				return false;
			}
		}
		return true;
	}

	void OvrScene::FindVisableObjects()
	{
		GetRoot()->FindVisibleObjects(render_queue);
	}

	void OvrScene::RenderObjects(const OvrRenderableCollector* a_collector, OrganisationMode a_om)
	{
		a_collector->AcceptVisitor(collector_visitor, a_om);
	}

	OvrMaterial* OvrScene::SetMaterial(OvrMaterial* a_material, bool a_shadowDerivation)
	{
		render_system->UnBindGpuProgram();

		OvrMaterial* material = a_material;
		if (a_shadowDerivation && cur_shadow_caster_light)
		{
			material = DeriveShadowCasterMaterial(a_material);
		}

		render_system->BindGpuProgram(material->GetProgram());
		shader_param_dirty = GPV_ALL;

		OvrGpuProgramParameters* shader_params = material->GetProgramParameters();
		if (shader_params->GetTextureUnit(0))
			auto_params.tex_mat0 = shader_params->GetTextureUnit(0)->GetTextureMatrix();
		shader_param_dirty |= GPV_GLOBAL;

		render_system->SetSceneBlend(material->GetSourceBlendFactor(), material->GetDestBlendFactor());
		render_system->SetDepthCheckEnable(material->GetDepthCheckEnable());
		render_system->SetDepthWriteEnable(material->GetDepthWriteEnable());
		render_system->SetCullMode(material->GetCullMode());
		return material;
	}

	void OvrScene::RenderSingleObject(OvrRenderable* a_rnd, OvrMaterial* a_material)
	{
		auto_params.world_matrix = a_rnd->GetWorldTransform();
		auto_params.mvp_matrix = auto_params.world_matrix * auto_params.view_matrix * auto_params.projection_matrix;
		auto_params.skeletal_matrix = a_rnd->GetSkeletonMatrix(auto_params.skeletal_num);
		shader_param_dirty |= GPV_PER_OBJECT;

		a_material->UpdateAutoParameters(auto_params, shader_param_dirty);
		render_system->BindGpuProgramParameters(a_material->GetProgramParameters(), shader_param_dirty);
		shader_param_dirty = 0;
		a_rnd->PreRender(this, render_system);
		OvrRenderOperation op;
		a_rnd->GetRenderOperation(op);
		render_system->Render(op);
		a_rnd->PostRender();

	}

	void OvrScene::RenderVisibleObjects()
	{		
		if (sky_box)
		{
			render_queue->AddRenderable(sky_box);
		}
		RenderBasicQueueObjects();
	}

	void OvrScene::RenderVisibleObjectsShadowMap(std::vector<OvrLightComponent*> a_lights)
	{
		if (a_lights.size() <= 0)
			return;

		OvrRenderSystem::Viewport save_view_port = render_system->GetViewPort();

		std::vector<OvrLightComponent*> point_lights;
		std::vector<OvrLightComponent*> spot_lights;
		OvrLightComponent* direction_light = nullptr;
		for (ovr::uint32 i = 0; i < a_lights.size(); ++i)
		{
			if(!a_lights[i]->IsCastShadow())
				continue;

			switch (a_lights[i]->GetLightType())
			{
			case ePointLight:
				point_lights.push_back(a_lights[i]);
				break;
			case eSpotLight:
				spot_lights.push_back(a_lights[i]);
				break;
			case eDirectionLight:
				direction_light = a_lights[i];
			default:
				break;
			}
		}

		if (spot_lights.size() > 0)
		{
			OvrRenderTexture* rtt = spot_tex_array->GetRenderTarget();
			rtt->BindRTT();
			for (ovr::uint32 l = 0; l < spot_lights.size(); ++l)
			{
				cur_shadow_caster_light = spot_lights[l];
				auto_params.view_matrix = spot_lights[l]->GetViewMat();
				auto_params.projection_matrix = spot_lights[l]->GetProjMat();
				rtt->Bind(l, 0);
				render_system->ClearBuffers(true, true, false, OvrColorValue(1.0f));
				RenderTextureShadowCasterQueueObjects();
				rtt->UnBind();
			}
			rtt->UnBindRTT();
		}


		if (point_lights.size() > 0)
		{
			OvrRenderTexture* rtt = point_tex_array->GetRenderTarget();
			auto_params.projection_matrix = OvrMatrix4::make_proj_matrix_rh((float)3.1415926535898f * 0.5f, 1.0f, 0.01f, 100.0f);
			rtt->BindRTT();
			for (ovr::uint32 l = 0; l < point_lights.size(); ++l)
			{
				cur_shadow_caster_light = point_lights[l];
				OvrVector3 tPos = point_lights[l]->GetPosition();
				auto_params.eye_pos = tPos;
				auto_params.point_light_radius_for_shadow = point_lights[l]->GetRange();
				for (int i = 0; i < 6; ++i)
				{					
					auto_params.view_matrix = OvrMatrix4::make_view_matrix(tPos, tPos + g_look[i], g_up[i]);
					rtt->Bind(l, i);
					render_system->ClearBuffers(true, true, false, OvrColorValue(1.0f));
					RenderTextureShadowCasterQueueObjects();
					rtt->UnBind();
				}
			}
			rtt->UnBindRTT();
		}

		if (direction_light)
		{
			OvrRenderTexture* rtt = shadow_tex->GetRenderTarget();
			cur_shadow_caster_light = direction_light;
			auto_params.view_matrix = direction_light->GetViewMat();
			auto_params.projection_matrix = direction_light->GetProjMat();
			rtt->BindRTT();
			render_system->ClearBuffers(true, true, false, OvrColorValue(1.0f));
			RenderTextureShadowCasterQueueObjects();
			rtt->UnBindRTT();
		}

		cur_shadow_caster_light = nullptr;
		render_system->SetViewPort(save_view_port);
		OvrCameraComponent* camera = main_camera->GetComponent<OvrCameraComponent>();
		auto_params.SetCurrentCamera(camera);
	}

	void OvrScene::RenderTextureShadowCasterQueueObjects()
	{
		OvrRenderQueue::RenderPriorityGroupMap queue_map = render_queue->GetQueuePriorityGroupMap();
		OvrRenderQueue::RenderPriorityGroupMap::iterator queue_it = queue_map.begin();
		while (queue_it != queue_map.end())
		{
			RenderPriorityGroup* group = queue_it->second;
			RenderObjects(group->GetSolids(), OM_PASS_GROUP);
			queue_it++;
		}
	}

	void OvrScene::RenderSceneToTexture()
	{

	}

	void OvrScene::RenderBasicQueueObjects()
	{
		OvrCameraComponent* camera = main_camera->GetComponent<OvrCameraComponent>();
		OvrRenderQueue::RenderPriorityGroupMap queue_map = render_queue->GetQueuePriorityGroupMap();
		OvrRenderQueue::RenderPriorityGroupMap::iterator queue_it = queue_map.begin();
		while (queue_it != queue_map.end())
		{
			RenderPriorityGroup* group = queue_it->second;
			group->GetTransparents()->Sort(camera);
			RenderObjects(group->GetSolids(), OM_PASS_GROUP);
			RenderObjects(group->GetTransparents(), OM_SORT_DESCENDING);
			queue_it++;
		}
	}

	OvrActor* OvrScene::GetActor(const OvrString& a_unique_name)
	{
		auto actor = actor_map.find(a_unique_name.GetStringConst());
		if (actor != actor_map.end())
		{
			return actor->second;
		}
		return nullptr;
	}

	void OvrScene::SetActiveCamera(OvrActor* a_camera)
	{
		if (nullptr != a_camera)
		{
			if (a_camera->GetScene() == this )
			{
				if (a_camera->GetComponent<OvrCameraComponent>())
				{
					main_camera = a_camera;
					return;
				}					
			}
		}
	}

	OvrActor* OvrScene::GetRoot()
	{
		return root_actor;
	}

	OvrActor* OvrScene::CreateActor(OvrVector3 a_position /*= OvrVector3::ZERO*/, OvrQuaternion a_quat /*= OvrQuaternion::IDENTITY*/, OvrVector3 a_scale /*= OvrVector3::UNIT_SCALE*/)
	{
		OvrActor* actor = new OvrActor(this);
		actor->SetPosition(a_position);
		actor->SetQuaternion(a_quat);
		actor->SetScale(a_scale);
		actor_map[actor->GetUniqueName()] = actor;
		root_actor->AddChild(actor);
		return actor;
	}

	bool OvrScene::DestoryActor(OvrActor* a_actor)
	{
		if (nullptr == a_actor)
		{
			return false;
		}

		auto actor_it = actor_map.find(a_actor->GetUniqueName().GetStringConst());
		if (actor_it != actor_map.end())
		{
			OvrActor* actor = actor_it->second;
			if (nullptr != actor->GetParent())
			{
				actor->GetParent()->RemoveChild(actor);
			}

			for (ovr::uint32 i = 0; i < actor->GetChildCount(); i++)
			{
				DestoryActor(a_actor->GetChild(i));
			}
			actor_map.erase(actor_it);		
			delete actor;
			return true;
		}
		return false;
	}

	void OvrScene::UpdateScene()
	{
		GetRoot()->Update();
	}

	void OvrScene::DrawScene()
	{		
		if (!render_system || !main_camera)
			return;

		OvrCameraComponent* camera = main_camera->GetComponent<OvrCameraComponent>();
		if (!camera)
			return;

		PrepareRenderQueue();
		FindVisableObjects();

		std::vector<OvrLightComponent*> lights;
		ComponentCollection* collection = GetComponentCollection(kComponentLight);
		ComponentCollection::iterator it = collection->begin();
		for (; it != collection->end(); it++)
		{
			OvrLightComponent* light = (OvrLightComponent*)*it;
			if (light->GetParentActor()->IsVisible() && light->IsActive())
			{
				lights.push_back(light);
			}
		}

		auto_params.SetCurrentCamera(camera);
		auto_params.SetCurrentLightList(lights);
		auto_params.env_color_intensity = ambient_color;

		EnsureTextureShadow();
		auto_params.direction_shadow_map = shadow_tex;
		auto_params.point_shadow_map = point_tex_array;
		auto_params.spot_shadow_map = spot_tex_array;


		ui_text_mgr->Update(camera->GetCameraPos(), main_camera->GetWorldQuaternion()*(-OvrVector3::UNIT_Z), 0);

		if (camera->GetRenderPath() == kForward)
		{
			RenderVisibleObjectsShadowMap(lights);
			RenderVisibleObjects();
		}

		ui_text_mgr->Draw(camera->GetViewProjectMatrix());
		ui_text_mgr->RemoveAllText();	
	}

	OvrRaySceneQuery* OvrScene::CreateRaySceneQuery()
	{
		OvrDefaultRaySceneQuery* q = new OvrDefaultRaySceneQuery(this);
		return q;
	}

	void OvrScene::DestorySceneQuery(OvrRaySceneQuery* a_query)
	{
		delete a_query;
	}

	void OvrScene::SetSkyBox(OvrMaterial* a_material, float a_distance)
	{
		if (!sky_box)
			sky_box = new OvrSkyBox();
		sky_box->SetSize(a_distance);
		sky_box->SetMaterial(a_material);
	}

	void OvrScene::NotifyComponentCreated(OvrComponent* a_component)
	{
		if (a_component)
		{
			ComponentCollection* collection = GetComponentCollection(a_component->GetType());
			collection->push_back(a_component);
		}
		return;
	}

	void OvrScene::NotifyComponentDestroyed(OvrComponent* a_component)
	{
		ComponentCollection* collection = GetComponentCollection(a_component->GetType());
		ComponentCollection::iterator it = collection->begin();
		for (; it != collection->end(); it++)
		{
			if (*it == a_component)
			{
				collection->erase(it);
				break;
			}
		}
	}

	OvrScene::ComponentCollection* OvrScene::GetComponentCollection(int a_type)
	{
		ComponentCollectionMap::iterator it = component_collection_map.find(a_type);
		if (it != component_collection_map.end())
		{
			return it->second;
		}
		else
		{
			ComponentCollection* new_collection = new ComponentCollection;
			component_collection_map[a_type] = new_collection;
			return new_collection;
		}
	}

	void OvrScene::SetAmbientColor(const OvrVector4& a_ambient)
	{
		ambient_color = a_ambient;
	}

	void OvrScene::SceneRenderableCollectorVistor::visit(OvrMaterial* material)
	{
		use_material = target_scene->SetMaterial(material);
	}

	void OvrScene::SceneRenderableCollectorVistor::visit(OvrRenderable* a_rnd)
	{		
		if(target_scene->ValidateRenderableForRendering(use_material, a_rnd))
			target_scene->RenderSingleObject(a_rnd, use_material);
	}

	void OvrScene::SceneRenderableCollectorVistor::visit(RenderablePass* a_rnd_pass)
	{
		if (target_scene->ValidateRenderableForRendering(a_rnd_pass->material, a_rnd_pass->renderable))
		{
			use_material = target_scene->SetMaterial(a_rnd_pass->material);
			target_scene->RenderSingleObject(a_rnd_pass->renderable, use_material);
		}
	}

}


