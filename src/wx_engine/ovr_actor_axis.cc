﻿#include "headers.h"
#include <wx_engine/ovr_actor_axis.h>
#include "wx_engine/ovr_engine.h"
#include "wx_engine/ovr_actor.h"
#include "wx_engine/ovr_camera_component.h"
namespace ovr_engine 
{
	OvrActorAxis* sg_actor_axis = nullptr;
	OvrActorAxis* OvrActorAxis::GetIns()
	{
		if (nullptr != sg_actor_axis)
		{
			return sg_actor_axis;
		}
		sg_actor_axis = new OvrActorAxis;
		return sg_actor_axis;
	}
	void OvrActorAxis::DelIns()
	{
		if (nullptr != sg_actor_axis)
		{
			sg_actor_axis->Uninit();
			delete sg_actor_axis;
			sg_actor_axis = nullptr;
		}
	}

	OvrActorAxis::OvrActorAxis() 
		:axis_type(kActorAxisInvalid)
		, current_axis(nullptr)
		, transform_axis(nullptr)
		, rotation_axis(nullptr)
		, scale_axis(nullptr)
	{
	}
	OvrActorAxis::~OvrActorAxis() 
	{
		Uninit();
	}

	bool OvrActorAxis::Init() 
	{
		if (nullptr == transform_axis)
		{
			transform_axis = new lvr_axis_common;
			transform_axis->init_with_gl();
		}
		if (nullptr == rotation_axis)
		{
			rotation_axis = new lvr_axis_rotate;
			rotation_axis->init_with_gl();
		}
		if (nullptr == scale_axis)
		{
			scale_axis = new lvr_axis_scale;
			scale_axis->init_with_gl();
		}
		current_axis = nullptr;
		return true;
	}
	void OvrActorAxis::Uninit() 
	{
		if (nullptr != transform_axis)
		{
			delete transform_axis;
			transform_axis = nullptr;
		}
		if (nullptr != rotation_axis)
		{
			delete rotation_axis;
			rotation_axis = nullptr;
		}
		if (nullptr != scale_axis)
		{
			delete scale_axis;
			scale_axis = nullptr;
		}
		current_axis = nullptr;
	}
	void OvrActorAxis::SetType(ActorAxisType a_new_type)
	{ 
		//先将旧的置为不可见
		if (nullptr != current_axis)
		{
			current_axis->set_visible(false);
		}

		axis_type = a_new_type; 
		if (kActorAxisTransform == axis_type)
		{
			current_axis = transform_axis;
		}
		else if (kActorAxisRotate == axis_type)
		{
			current_axis = rotation_axis;
		}
		else if (kActorAxisScale == axis_type)
		{
			current_axis = scale_axis;
		}
		else
		{
			current_axis = nullptr;
		}
		if (nullptr != current_axis)
		{
			current_axis->set_visible(true);
		}
	}

	void OvrActorAxis::SetTransform(const OvrVector3& a_position, const OvrQuaternion& a_quaternion) 
	{
		position = a_position;
		if (nullptr != current_axis)
		{
			transform_axis->set_position(lvr_vector3f(a_position[0], a_position[1], a_position[2]));
			transform_axis->set_orientation(lvr_quaternionf(a_quaternion[1], a_quaternion[2], a_quaternion[3], a_quaternion[0]));
			rotation_axis->set_position(lvr_vector3f(a_position[0], a_position[1], a_position[2]));
			rotation_axis->set_orientation(lvr_quaternionf(a_quaternion[1], a_quaternion[2], a_quaternion[3], a_quaternion[0]));
			scale_axis->set_position(lvr_vector3f(a_position[0], a_position[1], a_position[2]));
			scale_axis->set_orientation(lvr_quaternionf(a_quaternion[1], a_quaternion[2], a_quaternion[3], a_quaternion[0]));
		}
		
	}
	void OvrActorAxis::MouseMove(OvrActor* a_camera, float a_x, float a_y)
	{
		if (nullptr == current_axis)
		{
			return;
		}
		if (!is_pressed)
		{
			OvrCameraComponent* component = a_camera->GetComponent<OvrCameraComponent>();
			OvrRay3f ray = component->GeneratePickRay(a_x, a_y);
			//OvrRay3f ray = IOvrEngine::GetIns()->GetRoamCamera()->GeneratePickRay(a_x, a_y);
			OvrVector3 origin = ray.GetOrigin();
			OvrVector3 direction = ray.GetDir();
			int axis_info = current_axis->test_ray(lvr_vector3f(origin[0], origin[1], origin[2]), lvr_vector3f(direction[0], direction[1], direction[2]));
			current_axis->lock_operate_axis(axis_info);
			return;
		}

		//先只处理移动情况
		//if (kActorAxisRotate != axis_type)
		{
			OvrCameraComponent* component = a_camera->GetComponent<OvrCameraComponent>();
			OvrRay3f ray = component->GeneratePickRay(a_x, a_y);
			float distance = 0;
			if (!help_plane.Intersect(distance, ray.GetOrigin(), ray.GetDir()))
			{
				return;
			}
			OvrVector3 current_pos = (ray.GetOrigin() + ray.GetDir()*distance) - start_pos;

			if (kActorAxisTransform == axis_type)
			{
				SetActorNewPos(current_pos);
			}
			else if (kActorAxisScale == axis_type)
			{
				SetActorNewScale(current_pos);
			}
			else 
			{
				//SetActorNewRotate(a_x - pressed_x, a_y - pressed_y);
				SetActorNewRotate(ray.GetOrigin(), ray.GetDir(), a_x - pressed_x, a_y - pressed_y);
			}
		}
	
	}

	ActorAxisName OvrActorAxis::GetAxisName()
	{
		if (nullptr == current_axis)
		{
			return kAxisNameInvalid;
		}
		ActorAxisName axis_name = kAxisNameInvalid;
		int axis_type = current_axis->get_lock_axis();
		if (lvr_axis_base::e_axis_x == axis_type)
		{
			axis_name = kAxisNameX;
		}
		else if (lvr_axis_base::e_axis_y == axis_type)
		{
			axis_name = kAxisNameY;
		}
		else if (lvr_axis_base::e_axis_z == axis_type)
		{
			axis_name = kAxisNameZ;
		}
		
		return axis_name;
	}

	void OvrActorAxis::BeginPressed(OvrActor* a_camera, OvrActor* a_actor, float a_x, float a_y)
	{
		is_pressed = true;
		pressed_x = a_x;
		pressed_y = a_y;
		selected_actor = a_actor;
		if(selected_actor)
		{
			help_sphere.SetPos(selected_actor->GetPosition());

		}
		help_sphere.SetRadius(1.0f);
		world_transform = OvrTransform();
		world_transform_matrix = OvrMatrix4();
		OvrActor* parent = a_actor->GetParent();
		if (parent)
		{
			world_transform_matrix = parent->GetWorldTransform();
			world_transform_matrix.decompose(&world_transform.scale, &world_transform.quaternion, &world_transform.position);
		}

		default_transform = a_actor->GetTransform();
		OvrCameraComponent* component = a_camera->GetComponent<OvrCameraComponent>();
		GeneratePane(a_camera);

		//OvrRay3f ray = IOvrEngine::GetIns()->GetRoamCamera()->GeneratePickRay(a_x, a_y);
		OvrRay3f ray = component->GeneratePickRay(a_x, a_y);
		

		float distance = 0;
		if (help_plane.Intersect(distance, ray.GetOrigin(), ray.GetDir()))
		{
			start_pos = ray.GetOrigin() + ray.GetDir()*distance;
		}
		else 
		{
			int a = 0;
		}
		if(help_sphere.Intersect(distance,ray.GetOrigin(),ray.GetDir()))
		{
			start_pos = ray.GetOrigin() + ray.GetDir()*distance;
		}
	}

	void OvrActorAxis::EndPressed() 
	{
		is_pressed = false;
		selected_actor = nullptr;
	}

	void OvrActorAxis::Update(OvrCameraComponent* a_camera)
	{
		if (nullptr != current_axis)
		{
			const OvrVector3& position = a_camera->GetCameraPos();
			//const OvrVector3& position = IOvrEngine::GetIns()->GetRoamCamera()->GetCameraPos();
			current_axis->adjust_size(lvr_vector3f(position[0], position[1], position[2]));
		}
	}

	void OvrActorAxis::Draw(const OvrMatrix4& a_vp)
	{
		if (nullptr == current_axis)
		{
			return;
		}
		lvr_matrix4f view_project;
		for (int i = 0; i < 16; i++)
		{
			view_project[i] = a_vp[i];
		}
		glDisable(GL_DEPTH_TEST);
		current_axis->draw(view_project);
	}

	void OvrActorAxis::GeneratePane(OvrActor* a_camera)
	{
		const OvrVector3& look_dir = a_camera->GetWorldQuaternion()*(-OvrVector3::UNIT_Z);
		//const OvrVector3& look_dir = IOvrEngine::GetIns()->GetRoamCamera()->GetDirection();

		ActorAxisName axis_name = GetAxisName();
		if (kAxisNameX == axis_name)
		{
			//help_plane
			float value1 = look_dir * OvrVector3::UNIT_Y;
			float value2 = look_dir * OvrVector3::UNIT_Z;
			if (fabs(value1) > fabs(value2))
			{
				help_plane.SetNormal(OvrVector3::UNIT_Y);
			}
			else
			{
				help_plane.SetNormal(OvrVector3::UNIT_Z);
			}
		}
		else if (kAxisNameY == axis_name)
		{
			float value1 = look_dir * OvrVector3::UNIT_X;
			float value2 = look_dir * OvrVector3::UNIT_Z;
			if (fabs(value1) > fabs(value2))
			{
				help_plane.SetNormal(OvrVector3::UNIT_X);
			}
			else
			{
				help_plane.SetNormal(OvrVector3::UNIT_Z);
			}
		}
		else if (kAxisNameZ == axis_name)
		{
			float value1 = look_dir * OvrVector3::UNIT_X;
			float value2 = look_dir * OvrVector3::UNIT_Y;
			if (fabs(value1) > fabs(value2))
			{
				help_plane.SetNormal(OvrVector3::UNIT_X);
			}
			else
			{
				help_plane.SetNormal(OvrVector3::UNIT_Y);
			}
		}
		float length = help_plane.GetNormal() * world_transform_matrix.transform_point(default_transform.position);
		help_plane.SetDistanceToOrigin(length);
	}

	void OvrActorAxis::SetActorNewPos(const OvrVector3& a_move_pos)
	{
		OvrVector3 new_pos_local;
		OvrVector3 new_pos_world;
		ActorAxisName axis_name = GetAxisName();
		
		OvrVector3 local_pos(default_transform.position);
		OvrMatrix4 world_transform_matrix_inverse = world_transform_matrix;
		world_transform_matrix_inverse.inverse();
		if (kAxisNameX == axis_name) 
		{	
			new_pos_world = world_transform_matrix.transform_point(local_pos) + OvrVector3(a_move_pos[0], 0.0f, 0.0f);
			new_pos_local = world_transform_matrix_inverse.transform_point(new_pos_world);
			selected_actor->SetPosition(new_pos_local);
			transform_axis->set_position(lvr_vector3f(new_pos_world[0], new_pos_world[1], new_pos_world[2]));
			rotation_axis->set_position(lvr_vector3f(new_pos_world[0], new_pos_world[1], new_pos_world[2]));
			scale_axis->set_position(lvr_vector3f(new_pos_world[0], new_pos_world[1], new_pos_world[2]));
		}
		else if (kAxisNameY == axis_name)
		{
			new_pos_world = world_transform_matrix.transform_point(local_pos) + OvrVector3(0.0f, a_move_pos[1], 0.0f);
			new_pos_local = world_transform_matrix_inverse.transform_point(new_pos_world);
			selected_actor->SetPosition(new_pos_local);
			transform_axis->set_position(lvr_vector3f(new_pos_world[0], new_pos_world[1], new_pos_world[2]));
			rotation_axis->set_position(lvr_vector3f(new_pos_world[0], new_pos_world[1], new_pos_world[2]));
			scale_axis->set_position(lvr_vector3f(new_pos_world[0], new_pos_world[1], new_pos_world[2]));
		}
		else if (kAxisNameZ == axis_name)
		{
			new_pos_world = world_transform_matrix.transform_point(local_pos) + OvrVector3(0.0f, 0.0f, a_move_pos[2]);
			new_pos_local = world_transform_matrix_inverse.transform_point(new_pos_world);
			selected_actor->SetPosition(new_pos_local);
			transform_axis->set_position(lvr_vector3f(new_pos_world[0], new_pos_world[1], new_pos_world[2]));
			rotation_axis->set_position(lvr_vector3f(new_pos_world[0], new_pos_world[1], new_pos_world[2]));
			scale_axis->set_position(lvr_vector3f(new_pos_world[0], new_pos_world[1], new_pos_world[2]));
		}
	}

	void OvrActorAxis::SetActorNewScale(const OvrVector3& a_move_pos)
	{
		OvrVector3 new_scale;
		ActorAxisName axis_name = GetAxisName();
		if (kAxisNameX == axis_name)
		{
			new_scale = default_transform.scale + OvrVector3(a_move_pos[0], 0.0f, 0.0f);
			selected_actor->SetScale(new_scale);
		}
		else if (kAxisNameY == axis_name)
		{
			new_scale = default_transform.scale + OvrVector3(0.0f, 0.0f, a_move_pos[1]);
			selected_actor->SetScale(new_scale);
		}
		else if (kAxisNameZ == axis_name)
		{
			new_scale = default_transform.scale + OvrVector3(0.0f,  a_move_pos[2],0.0f);
			selected_actor->SetScale(new_scale);
		}	
	}

	void OvrActorAxis::SetActorNewRotate(const OvrVector3& a_ray_origin, const OvrVector3& a_ray_dir,float a_move_x,float a_move_y)
	{
		OvrQuaternion new_quaternion_local;
		OvrQuaternion new_quaternion_world;
		ActorAxisName axis_name = GetAxisName();
		//float new_move = a_move_pos.length() - start_pos.length();


		if(is_pressed)
		{
			float t_dist = 1e30;
			if(help_sphere.Intersect(t_dist,a_ray_origin,a_ray_dir))
			{
				OvrVector3 last_hit_point = a_ray_origin + a_ray_dir*t_dist;
				last_hit_point.normalize();
				start_pos.normalize();
				last_hit_point._y = last_hit_point._z = 0;
				last_hit_point._y = last_hit_point._z = 0;

				OvrMatrix4 rot_mat = OvrMatrix4::make_rotate_from_to_matrix(last_hit_point, start_pos);
				OvrQuaternion q;

				rot_mat.decompose(nullptr,&q,nullptr);
				
				if (kAxisNameX == axis_name)
				{
					//selected_actor->SetQuaternion(q);
					//rotation_axis->set_orientation(lvr_quaternionf(q[1], q[2], q[3], q[0]));
					//selected_actor->SetQuaternion(default_transform.quaternion*q);
				}
			}
		}

		
		//float a_move_y = 0;
		//float a_move_x = 0;
		if (kAxisNameX == axis_name)
		{
			new_quaternion_local = OvrQuaternion::make_euler_quat(-a_move_y, 0.0f, 0.0f) * default_transform.quaternion;
			new_quaternion_world = new_quaternion_local * world_transform.quaternion;
			selected_actor->SetQuaternion(new_quaternion_local);
			//rotation_axis->set_orientation(lvr_quaternionf(new_quaternion_world[1], new_quaternion_world[2], new_quaternion_world[3], new_quaternion_world[0]));
			//scale_axis->set_orientation(lvr_quaternionf(new_quaternion_world[1], new_quaternion_world[2], new_quaternion_world[3], new_quaternion_world[0]));
		}
		else if (kAxisNameY == axis_name)
		{
			new_quaternion_local = OvrQuaternion::make_euler_quat(0.0f, a_move_y,0.0f ) * default_transform.quaternion;
			new_quaternion_world = new_quaternion_local * world_transform.quaternion;
			selected_actor->SetQuaternion(new_quaternion_local);
			//rotation_axis->set_orientation(lvr_quaternionf(new_quaternion_world[1], new_quaternion_world[2], new_quaternion_world[3], new_quaternion_world[0]));
			//scale_axis->set_orientation(lvr_quaternionf(new_quaternion_world[1], new_quaternion_world[2], new_quaternion_world[3], new_quaternion_world[0]));
		}
		else if (kAxisNameZ == axis_name)
		{
			new_quaternion_local = OvrQuaternion::make_euler_quat(0.0f, 0.0f, -a_move_x) * default_transform.quaternion;
			new_quaternion_world = new_quaternion_local * world_transform.quaternion;
			selected_actor->SetQuaternion(new_quaternion_local);
			//rotation_axis->set_orientation(lvr_quaternionf(new_quaternion_world[1], new_quaternion_world[2], new_quaternion_world[3], new_quaternion_world[0]));
			//scale_axis->set_orientation(lvr_quaternionf(new_quaternion_world[1], new_quaternion_world[2], new_quaternion_world[3], new_quaternion_world[0]));
		}	
	}
}
