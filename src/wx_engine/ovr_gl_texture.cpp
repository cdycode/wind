#include "ovr_gl_texture.h"
#include "ovr_gl_fbo_render_texture.h"
#include "wx_engine/ovr_logger.h"
#include "wx_engine/ovr_serializer_impl.h"
using namespace ovr_engine;
GLenum ovr_engine::OvrGLPixelUtil::GetGLOriginFormat(OvrPixelFormat a_format)
{
	switch (a_format)
	{
	case ovr_engine::PF_R8G8B8A8:
		return GL_RGBA;
	case ovr_engine::PF_R8G8B8:
		return GL_RGB;
	case ovr_engine::PF_DEPTH16:
	case ovr_engine::PF_DEPTH24:
		return GL_DEPTH_COMPONENT;
	default:
		return 0;
	}
}

GLenum ovr_engine::OvrGLPixelUtil::GetGLOriginDataType(OvrPixelFormat a_format)
{
	switch (a_format)
	{
	case ovr_engine::PF_R8G8B8A8:
	case ovr_engine::PF_R8G8B8:
		return GL_UNSIGNED_BYTE;
	case ovr_engine::PF_DEPTH16:
		return GL_UNSIGNED_SHORT;
	case ovr_engine::PF_DEPTH24:
		return GL_UNSIGNED_INT;
	default:
		return 0;
	}
}

GLenum ovr_engine::OvrGLPixelUtil::GetGLInternalFormat(OvrPixelFormat a_format, bool a_hwGamma /*= false*/)
{
	switch (a_format)
	{
	case ovr_engine::PF_R8G8B8A8:
		if (a_hwGamma)
			return GL_SRGB8_ALPHA8;
		else
			return GL_RGBA8;
	case ovr_engine::PF_R8G8B8:
		if (a_hwGamma)
			return GL_SRGB8;
		else
			return GL_RGB8;
	case ovr_engine::PF_DEPTH16:
		return GL_DEPTH_COMPONENT16;
	case ovr_engine::PF_DEPTH24:
		return GL_DEPTH_COMPONENT24;
	default:
		return GL_NONE;
	}
}

GLenum ovr_engine::OvrGLPixelUtil::GetClosestGLInternalFormat(OvrPixelFormat a_format, bool a_hwGamma /*= false*/)
{
	GLenum format = GetGLInternalFormat(a_format, a_hwGamma);
	if (format == GL_NONE)
	{
		if (a_hwGamma)
			return GL_SRGB8;
		else
			return GL_RGBA8;
	}
	else
		return format;
}

ovr_engine::OvrPixelFormat ovr_engine::OvrGLPixelUtil::GetClosestOVRFormat(GLenum fmt)
{
	switch (fmt)
	{
	case GL_DEPTH_COMPONENT16:
		return PF_DEPTH16;
	case GL_DEPTH_COMPONENT:
	case GL_DEPTH_COMPONENT24:
		return PF_DEPTH24;
	case GL_RGBA8:
	case GL_RGBA:
		return PF_R8G8B8A8;
	case GL_RGB8:
	case GL_SRGB8:
	case GL_RGB:
		return PF_R8G8B8;
	default:
		return PF_R8G8B8;
	}
	 
}

ovr::uint32 ovr_engine::OvrGLPixelUtil::GetMemorySize(OvrPixelFormat a_format, ovr::uint32 width, ovr::uint32 height)
{
	switch (a_format)
	{
	case ovr_engine::PF_DEPTH16:
		return 2 * width * height;
	case ovr_engine::PF_R8G8B8:
	case ovr_engine::PF_DEPTH24:
		return 3 * width * height;
	case ovr_engine::PF_R8G8B8A8:
		return 4 * width * height;
	default:
		return 0;
	}
}


ovr_engine::OvrImage ovr_engine::OvrGLPixelUtil::GetOvrImageInfo(const lvr_image_info& a_info)
{
	OvrImage info;
	info.data = a_info.data;
	info.data_size = a_info.data_size;
	info.data_type = a_info.data_type;
	info.format = a_info.format;
	info.gl_format = a_info.gl_format;
	info.gl_internal_format = a_info.gl_internal_format;
	info.height = a_info.height;
	info.width = a_info.width;
	info.use_srgb = a_info.use_srgb;
	memcpy(info.name, a_info.name, 256);
	info.mipcount = a_info.mipcount;
	info.is_cube_tex = a_info.is_cube_tex;
	info.image_size_stored = a_info.image_size_stored;
	return info;
}

ovr_engine::OvrGLTexture::OvrGLTexture(OvrString a_name, bool a_is_manual)
	: OvrTexture(a_name, a_is_manual)
	, tex_id(0)
{

}

ovr_engine::OvrGLTexture::~OvrGLTexture()
{
	if (load_state == LOADSTATE_LOADED)
	{
		UnLoad();
	}
	else
	{
		FreeInternalResources();
	}
}

void ovr_engine::OvrGLTexture::CreateInternalResourcesImpl()
{
	glGenTextures(1, &tex_id);
	glBindTexture(GetGLTextureTarget(), tex_id);


	glTexParameteri(GetGLTextureTarget(), GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GetGLTextureTarget(), GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GetGLTextureTarget(), GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GetGLTextureTarget(), GL_TEXTURE_MAG_FILTER, GL_NEAREST);


	GLenum gl_internalformat = OvrGLPixelUtil::GetClosestGLInternalFormat(format, false);
	GLenum gl_format = OvrGLPixelUtil::GetGLOriginFormat(format);
	GLenum gl_datatype = OvrGLPixelUtil::GetGLOriginDataType(format);

	switch (type)
	{
	case ovr_engine::TEXTURE_TYPE_2D:
		glTexImage2D(GL_TEXTURE_2D, 0, gl_internalformat,
			width, height, 0,
			gl_format, gl_datatype, 0);
		break;
	case ovr_engine::TEXTURE_TYPE_2D_ARRAY:
		glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, gl_internalformat,
			width, height, depth, 0,
			gl_format, gl_datatype, 0);
		break;
	case ovr_engine::TEXTURE_TYPE_CUBE:
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		for (int face = 0; face < 6; face++) {
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, 0, gl_internalformat,
				width, height, 0,
				gl_format, gl_datatype, 0);
		}
		break;
	case ovr_engine::TEXTURE_TYPE_CUBE_ARRAY:
		glTexParameteri(GL_TEXTURE_CUBE_MAP_ARRAY, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glTexImage3D(GL_TEXTURE_CUBE_MAP_ARRAY, 0, gl_internalformat,
			width, height, depth * 6, 0,
			gl_format, GL_UNSIGNED_BYTE, 0);
		break;
	default:
		break;
	}

	if (is_render_target)
	{
		render_target = new OvrGLFBORenderTexture(this);
	}

	glBindTexture(GetGLTextureTarget(), 0);
}

void ovr_engine::OvrGLTexture::FreeInternalResourcesImpl()
{
	if (tex_id)
	{
		glDeleteTextures(1, &tex_id);
		tex_id = 0;
	}
	if (render_target)
	{
		delete render_target;
		render_target = nullptr;
	}
}

GLuint ovr_engine::OvrGLTexture::GetTextureId()
{
	return tex_id;
}

void ovr_engine::OvrGLTexture::BlitFromMemory(void* a_data, ovr::uint32 a_size)
{
	if (!is_internal_created) 
		return;

	ovr::uint32 size = OvrGLPixelUtil::GetMemorySize(format, width, height);
	if (size != a_size)
	{
		return;
	}

	GLenum gl_internalformat = OvrGLPixelUtil::GetClosestGLInternalFormat(format, false);
	GLenum gl_format = OvrGLPixelUtil::GetGLOriginFormat(format);
	GLenum gl_datatype = OvrGLPixelUtil::GetGLOriginDataType(format);

	glBindTexture(GetGLTextureTarget(), tex_id);
	switch (type)
	{
	case ovr_engine::TEXTURE_TYPE_2D:
		glTexImage2D(GL_TEXTURE_2D, 0, gl_internalformat,
			width, height, 0,
			gl_format, gl_datatype, a_data);
		break;
	case ovr_engine::TEXTURE_TYPE_2D_ARRAY:
		glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, gl_internalformat,
			width, height, depth, 0,
			gl_format, gl_datatype, a_data);
		break;
	case ovr_engine::TEXTURE_TYPE_CUBE:
		for (int face = 0; face < 6; face++) {
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, 0, gl_internalformat,
				width, height, 0,
				gl_format, gl_datatype, a_data);
		}
		break;
	case ovr_engine::TEXTURE_TYPE_CUBE_ARRAY:
		glTexImage3D(GL_TEXTURE_CUBE_MAP_ARRAY, 0, gl_internalformat,
			width, height, depth * 6, 0,
			gl_format, GL_UNSIGNED_BYTE, a_data);
		break;
	default:
		break;
	}
	glBindTexture(GetGLTextureTarget(), 0);
}

bool ovr_engine::OvrGLTexture::LoadFromImage(const OvrImage& a_image)
{
	if (load_state != LOADSTATE_UNLOADED) return false;

	type = TEXTURE_TYPE_2D;
	render_target = false;

	format = OvrGLPixelUtil::GetClosestOVRFormat(a_image.gl_format);
	width = a_image.width;
	height = a_image.height;

	CreateInternalResources();

	const unsigned char * level = (const unsigned char*)a_image.data;
	const unsigned char * endOfBuffer = level + a_image.data_size;
	ovr::uint32 size = OvrGLPixelUtil::GetMemorySize(format, width, height);

	if (a_image.image_size_stored)
	{
		int32_t mipSize = size;
		mipSize = *(const size_t *)level;
		level += 4;
	}

	BlitFromMemory((void*)level, size);
	return true;
}

GLenum ovr_engine::OvrGLTexture::GetGLTextureTarget()
{
	switch (type)
	{
	case TEXTURE_TYPE_2D:
		return GL_TEXTURE_2D;
	case TEXTURE_TYPE_2D_ARRAY:
		return GL_TEXTURE_2D_ARRAY;
	case TEXTURE_TYPE_CUBE:
		return GL_TEXTURE_CUBE_MAP;
	case TEXTURE_TYPE_CUBE_ARRAY:
		return GL_TEXTURE_CUBE_MAP_ARRAY;
	default:
		return 0;
	};
}

void ovr_engine::OvrGLTexture::LoadImpl()
{
	OvrImage info;
	if (OvrSerializerImpl::GetIns()->GetImageInfo(name, info))
	{
		LoadFromImage(info);
	}
}

void ovr_engine::OvrGLTexture::UnLoadImpl()
{
	FreeInternalResources();
}