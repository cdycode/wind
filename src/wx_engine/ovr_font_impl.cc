#include "ovr_font_impl.h"

#define py_use_new_method
OvrFontImpl::OvrFontImpl()
	:_library(0),_face(0),_font_size(24),_use_kerning(false)
{

}

OvrFontImpl::~OvrFontImpl()
{
	if(_face)
	{
		FT_Done_Face((FT_Face)_face);
	}
	if(_library)
	{
		FT_Done_FreeType((FT_Library)_library);
	}
}

void OvrFontImpl::set_font_size(uint32_t a_font_size)
{
	_font_size = a_font_size;
}

uint32_t OvrFontImpl::get_font_size()
{
	return _font_size;
}

OvrAlphaText OvrFontImpl::get_text_alpha(const uint16_t* a_str,uint32_t a_len)
{
	OvrAlphaText t_l_a_t;
	t_l_a_t.width = 0;
	t_l_a_t.height = 0;
	t_l_a_t.alpha_bits = 0;
	return t_l_a_t;
}

int to_scale4(unsigned int& n)
{
	int res = n%4;
	if (res != 0)
	{
		n = n/4;
		n = n*4+4;
		return 4-res;
	}
	return 0;
}

OvrFontGlyph OvrFontImpl::get_font_glyph(const uint16_t a_char_code)
{
	OvrFontGlyph lfg;
	lfg.char_code = a_char_code;
	font_glyph* glyph = load_glyph(a_char_code,_font_size,0);
	uint32_t wg = glyph->_rect.get_width();
	uint32_t hg = glyph->_rect.get_height();

	//new method 00
#ifdef py_use_new_method
	std::vector<uint8_t>	t_bits;
	font_info fi{ (uint16_t)0, (uint16_t)0, (uint16_t)0, (uint16_t)0, (uint16_t)0, (uint16_t)0, (uint32_t)-1 };
	_kfont.generate_kfont(a_char_code, t_bits, fi);
	//wg = fi.width;
	//hg = fi.height;
	if (fi.width > 0)
	{
		wg = 32;
		hg = 32;
	}
#endif
	to_scale4(wg);
	int yadd = to_scale4(hg);
	lfg.alpha_bits = new uint8_t[wg*hg];
	memset(lfg.alpha_bits,0,wg*hg*sizeof(uint8_t));
#ifdef py_use_new_method
	if (t_bits.size() > 0)
	{
		for (uint32_t i = 0; i < hg; ++i)
		{
			memcpy(lfg.alpha_bits + wg*(31-i), &t_bits[32 * i], 32/*fi.width*/);
		}
		lfg.width = wg;
		lfg.height = hg;
		lfg.advance_x = fi.advance_x;
		lfg.advance_y = fi.advance_y;
		lfg.bearing_x = fi.left;
		lfg.bearing_y = 32 - fi.top + yadd;
	}
	else
#endif
	{
		int min_y = glyph->_rect.ymin;
		int min_x = glyph->_rect.xmin;
		for (spans::iterator iter = glyph->font.begin(); iter != glyph->font.end(); iter++)
		{
			for (int w = 0; w < iter->_width; w++)
			{
				int index1 = (iter->_y - min_y) *wg + (iter->_x - min_x) + w;
				unsigned char& dst = lfg.alpha_bits[index1];
				dst = iter->_coverage;
			}
		}
		lfg.width = wg;
		lfg.height = hg;
		lfg.advance_x = glyph->advance_x;
		lfg.advance_y = glyph->advance_y;
		lfg.bearing_x = glyph->bearing_x;
		lfg.bearing_y = glyph->bearing_y + yadd;
	}
	//to do ...... this should called other place,should we?
	if (glyph)
	{
		delete glyph;
	}
	return lfg;
}

bool OvrFontImpl::load_from_file(const char* a_file_path)
{
	if(_face)
	{
		FT_Done_Face((FT_Face)_face);
	}
	if(_library)
	{
		FT_Done_FreeType((FT_Library)_library);
	}
	clear();

	unsigned int error;
	///initialize the library
	FT_Library lib;
	error = FT_Init_FreeType(&lib);
	if(error){
		printf("FT_Init_FreeType failed!");
		return false;
	}
	_library = lib;

	//load a font
	FT_Face face;
	error = FT_New_Face(lib, (const char*)a_file_path, 0, &face);
	if(error == FT_Err_Unknown_File_Format){
		printf("the font file could be opened and read, but it appears that its font format is unsupported!");
		return false;
	}
	else if(error){
		printf("another error code means that the font file could not be opened or read, or simply that it is broken!");
		return false;
	}
	error = FT_Select_Charmap(face, FT_ENCODING_UNICODE);
	if(error){
		printf("failed to set the Unicode character set!");
		FT_Done_Face(face);
		return false;
	}
	_face = face;
	_use_kerning = (FT_HAS_KERNING(face)>0)?true:false;
	//_init_in_mem = false;
	_kfont.set_font_info(_library, _face);
	return true;
}

bool OvrFontImpl::load_from_mem(const char* a_mem_file,const int buf_len)
{
	if(_face)
	{
		FT_Done_Face((FT_Face)_face);
	}
	if(_library)
	{
		FT_Done_FreeType((FT_Library)_library);
	}
	clear();

	unsigned int error;
	///initialize the library
	FT_Library lib;
	error = FT_Init_FreeType(&lib);
	if(error){
		printf("FT_Init_FreeType failed!");
		return false;
	}
	_library = lib;

	//load a font
	FT_Face face;
	error = FT_New_Memory_Face(lib, (FT_Byte*)a_mem_file,buf_len, 0, &face);
	if(error == FT_Err_Unknown_File_Format){
		printf("the font file could be opened and read, but it appears that its font format is unsupported!");
		return false;
	}
	else if(error){
		printf("another error code means that the font file could not be opened or read, or simply that it is broken!");
		return false;
	}
	error = FT_Select_Charmap(face, FT_ENCODING_UNICODE);
	if(error){
		printf("failed to set the Unicode character set!");
		FT_Done_Face(face);
		return false;
	}
	_face = face;
	_use_kerning = (FT_HAS_KERNING(face)>0)?true:false;
	//_init_in_mem = true;
	_kfont.set_font_info(_library, _face);
	return true;
}

const char* OvrFontImpl::get_font_family()
{
	FT_Face face = reinterpret_cast<FT_Face>(_face);
	return (face->family_name);
}


//if cached outside,we can do this directly.
font_glyph* OvrFontImpl::get_glyph(unsigned int word_code_, unsigned int character_size_, int style_)
{
	//page& font_page_ = _page_table[character_size_];
	//unsigned int key = (((style_&bold)?1:0)<<31)| (((style_&italic)?1:0)<<30)|word_code_;
	//page::iterator iter = font_page_.find(key);
	//if(iter != font_page_.end())
	//{
	//	return iter->second;
	//}

	//load new glyph
	font_glyph* glyph = load_glyph(word_code_, character_size_, style_);
	//font_page_.insert(std::make_pair(key, glyph));
	return glyph;
}

unsigned int OvrFontImpl::get_line_spacing(unsigned int character_size_)
{
	FT_Face face = static_cast<FT_Face>(_face);

	if (face && set_current_size(character_size_))
	{
		return (face->size->metrics.height >> 6);
	}

	return 0;
}

int OvrFontImpl::get_kerning(unsigned int pre_word_code_, unsigned int cur_word_code_, unsigned int character_size_)
{
	// Special case where first or second is 0 (null character)
	if (pre_word_code_ == 0 || cur_word_code_ == 0)
		return 0;

	FT_Face face = static_cast<FT_Face>(_face);

	if (face && _use_kerning && set_current_size(character_size_))
	{
		// Convert the characters to indices
		FT_UInt index1 = FT_Get_Char_Index(face, pre_word_code_);
		FT_UInt index2 = FT_Get_Char_Index(face, cur_word_code_);

		// Get the kerning vector
		FT_Vector kerning;
		FT_Get_Kerning(face, index1, index2, FT_KERNING_DEFAULT, &kerning);

		// Return the X advance
		return kerning.x >> 6;
	}

	return 0;
}

void OvrFontImpl::clear()
{
	//page_map tmp;
	//tmp.swap(_page_table);
}

font_glyph* OvrFontImpl::load_glyph(unsigned int word_code_, unsigned int character_size_, int style)
{
	font_glyph* glyph = 0;

	FT_Face face = static_cast<FT_Face>(_face);
	FT_Library lib_ = static_cast<FT_Library>(_library);
	if(!set_current_size(character_size_))
	{
		return glyph;
	}

	if(FT_Load_Char(face, word_code_, FT_LOAD_TARGET_NORMAL|FT_LOAD_FORCE_AUTOHINT|FT_LOAD_NO_BITMAP) != 0){
		printf("FT_Load_Char failed!");
		return glyph;
	}

	if(face->glyph->format == FT_GLYPH_FORMAT_OUTLINE )
	{
		glyph = new font_glyph();
		if(style & italic)
		{
			set_oblique();
		}
		if(style & bold)
		{
			FT_Pos weight = 1<<6;
			FT_Outline_Embolden(&face->glyph->outline, weight);
		}
		//render regular font
		render_spans(&(glyph->font), &face->glyph->outline);

		FT_Stroker stroker;
		double outline_width =character_size_*0.025;
		FT_Stroker_New(lib_, &stroker);
		FT_Stroker_Set(stroker, (int)(outline_width*64),
			FT_STROKER_LINECAP_ROUND,
			FT_STROKER_LINEJOIN_ROUND,
			0);

		FT_Glyph glyph_des;
		if(FT_Get_Glyph(face->glyph, &glyph_des) != 0){
			printf("FT_Get_Glyph failed!");
			return glyph;
		}

		FT_Glyph_StrokeBorder(&glyph_des, stroker, 0,1);
		if(glyph_des->format == FT_GLYPH_FORMAT_OUTLINE)
		{
			//render outline
			FT_Outline* outline_gly = &reinterpret_cast<FT_OutlineGlyph>(glyph_des)->outline;
			render_spans(&(glyph->outline), outline_gly);
		}

		glyph->advance_x = (float)(face->glyph->advance.x>>6);
		glyph->advance_y = (float)(face->glyph->advance.y>>6);
		glyph->bearing_x = (float)(face->glyph->metrics.horiBearingX>>6);
		glyph->bearing_y = (float)(face->glyph->metrics.horiBearingY>>6);
		glyph->comput_rect();

		FT_Stroker_Done(stroker);
		FT_Done_Glyph(glyph_des);
	}
	else
	{
		printf("the font don't support outline!");
	}

	return glyph;
}

bool OvrFontImpl::set_current_size(unsigned int size_)
{
	if(_face == NULL)
		return false;

	FT_Face face = static_cast<FT_Face>(_face);
	FT_UShort cur_size = face->size->metrics.x_ppem;
	if(cur_size != size_)
	{
		FT_Set_Pixel_Sizes((FT_Face)_face, size_, size_);

		//FT_Set_Char_Size((FT_Face)_face, 0,16*64,96,96);
	}
	return true;
}

void OvrFontImpl::set_oblique()
{
	FT_Face face = reinterpret_cast<FT_Face>(_face);
	FT_Matrix    transform;
	FT_Outline*  outline = &face->glyph->outline;
	///for italic, simply apply shear transform
	///with an angle of about 12 degrees
	transform.xx = 0x10000L;
	transform.yx = 0x00000L;

	transform.xy = 0x0366AL;
	transform.yy = 0x10000L;

	FT_Outline_Transform( outline, &transform );
}

void OvrFontImpl::render_spans(spans* spans_, FT_Outline* const outline_)
{
	FT_Library lib_ = static_cast<FT_Library>(_library);
	FT_Raster_Params params;
	memset(&params, 0, sizeof(params));
	params.flags = FT_RASTER_FLAG_AA | FT_RASTER_FLAG_DIRECT;
	params.gray_spans = raster_callback;
	params.user = spans_;

	FT_Outline_Render(lib_, outline_, &params);
}

void raster_callback(const int y_, const int count_, const FT_Span* const spans_, void* const user)
{
	spans *sptr = (spans *)user;
	for (int i = 0; i < count_; ++i)
		sptr->push_back(span(spans_[i].x, y_, spans_[i].len, spans_[i].coverage));
}
