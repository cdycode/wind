#include "wx_engine/ovr_pose.h"
using namespace ovr_engine;
ovr_engine::OvrPose::OvrPose(ovr::uint32 a_handle, OvrString a_name)
	:handle(a_handle)
	, name(a_name)
{

}

ovr_engine::OvrPose::OvrPose(OvrString a_target_name, OvrString a_name)
	: handle(0)
	, name(a_name)
	, target_name(a_target_name)
{

}

void ovr_engine::OvrPose::AddVertex(ovr::uint32 a_index, OvrVector3 a_offset)
{
	vertex_offset_map[a_index] = a_offset;
}

