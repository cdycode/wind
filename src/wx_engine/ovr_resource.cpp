#include "wx_engine/ovr_resource.h"
using namespace ovr_engine;

ovr_engine::OvrResource::~OvrResource()
{
}

void ovr_engine::OvrResource::UnLoad()
{
	if (LOADSTATE_LOADED != load_state)
		return;

	UnLoadImpl();
	load_state = LOADSTATE_UNLOADED;
}

void ovr_engine::OvrResource::Load()
{
	if (LOADSTATE_UNLOADED != load_state)
		return;

	if(!is_manual)
		LoadImpl();

	load_state = LOADSTATE_LOADED;
	DirtyState();
}

bool ovr_engine::OvrResource::IsLoaded()
{
	if (LOADSTATE_LOADED == load_state)
		return true;
	return false;
}

bool ovr_engine::OvrResource::isManuallyLoaded() const
{
	return is_manual;
}

void ovr_engine::OvrResource::Reload()
{	
	UnLoad();
	Load();
}

void ovr_engine::OvrResource::DirtyState()
{
	state_count++;
}

ovr_engine::OvrResource::OvrResource(OvrString a_name, bool a_is_manual)
	: name(a_name)
	, is_manual(a_is_manual)
	, load_state(LOADSTATE_UNLOADED)
	, state_count(0)
{
	if (is_manual)
		load_state = LOADSTATE_LOADED;
}
