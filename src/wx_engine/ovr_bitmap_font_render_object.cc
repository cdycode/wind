#include "ovr_bitmap_font_render_object.h"


OvrBitmapFontRenderObject::OvrBitmapFontRenderObject()
	:_word_num(0)
	,_postion(0,0,0)
	,_right(1,0,0)
	,_up(0,1,0)
	,_scale_value(1)
	,_warp_width(100.0)
	, _line_spacing(0.0f)
	, _font_spacing(0.0f)
	, _total_cross_value(0.0f)
	, _lines(0)
	,_last_line_width(0.0f)
	,_font_butter_offset(0)
{
	for (int i = 0; i < 4; ++i)
	{
		_color[i] = 255;
	}
}

OvrBitmapFontRenderObject::~OvrBitmapFontRenderObject()
{

}
