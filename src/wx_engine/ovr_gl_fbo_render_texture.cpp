#include "ovr_gl_fbo_render_texture.h"
#include "ovr_gl_texture.h"
#include "wx_engine/ovr_logger.h"
#include "wx_engine/ovr_serializer_impl.h"
using namespace ovr_engine;
ovr_engine::OvrGLFBORenderTexture::OvrGLFBORenderTexture(OvrGLTexture* a_texture)
	: texture(a_texture)
	, fbo_id(0)
	, save_fbo_id(0)
	, depth_buffer(0)
{  
	glGetIntegerv(GL_FRAMEBUFFER_BINDING, &save_fbo_id);
	glGenFramebuffers(1, &fbo_id);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo_id);

	OvrPixelFormat format = texture->GetFormat();
	if (format == PF_DEPTH16 || format == PF_DEPTH24)
	{
		glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texture->GetTextureId(), 0, 0);
	} 
	else
	{
		glGenRenderbuffers(1, &depth_buffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depth_buffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, texture->GetWidth(), texture->GetHeight());
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
		glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture->GetTextureId(), 0, 0);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depth_buffer);
	}


	GLenum result = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (result == GL_FRAMEBUFFER_COMPLETE) 
	{
		OvrLogger::GetIns()->Log("Framebuffer is complete.\n", LL_INFORMATION);
	}
	else 
	{
		OvrLogger::GetIns()->Log("Framebuffer is not complete.\n", LL_INFORMATION);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, save_fbo_id);
}

ovr_engine::OvrGLFBORenderTexture::~OvrGLFBORenderTexture()
{
	if (fbo_id)
	{
		glDeleteFramebuffers(1, &fbo_id);
		fbo_id = 0;
	}
	if (depth_buffer)
	{
		glDeleteRenderbuffers(1, &depth_buffer);
		depth_buffer = 0;
	}
}

void ovr_engine::OvrGLFBORenderTexture::BindRTT()
{
	glGetIntegerv(GL_FRAMEBUFFER_BINDING, &save_fbo_id);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo_id);
}

void ovr_engine::OvrGLFBORenderTexture::Bind(int a_depth, int a_face)
{
	GLenum attachment = GL_COLOR_ATTACHMENT0;
	if (texture->GetFormat() == PF_DEPTH16 || texture->GetFormat() == PF_DEPTH24)
	{
		GLenum attachment = GL_DEPTH_ATTACHMENT;
	}
	glViewport(0, 0, texture->GetWidth(), texture->GetHeight());
	glFramebufferTextureLayer(GL_FRAMEBUFFER, attachment, texture->GetTextureId(), 0, a_depth * texture->GetFaceNum() + a_face);
}

void ovr_engine::OvrGLFBORenderTexture::UnBind()
{
	glFlush();
}

void ovr_engine::OvrGLFBORenderTexture::UnBindRTT()
{
	glBindFramebuffer(GL_FRAMEBUFFER, save_fbo_id);
}
