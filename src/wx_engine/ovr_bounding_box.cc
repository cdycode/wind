﻿#include "headers.h"
#include "wx_engine/ovr_bounding_box.h"
#include "wx_engine/ovr_material_manager.h"
#include "wx_engine/ovr_vertex_index_data.h"
#include "wx_engine/ovr_hardware_buffer_manager.h"
#include "wx_engine/ovr_material.h"
namespace ovr_engine
{

	OvrBoundingBox::OvrBoundingBox(OvrActor* a_actor)
		: parent_actor(a_actor)
		, vertex_data(nullptr)
		, index_data(nullptr)

	{

	}

	OvrBoundingBox::~OvrBoundingBox()
	{
		Reset();
	}

	OvrMatrix4 OvrBoundingBox::GetWorldTransform() const
	{
		return parent_actor->GetWorldTransform();
	}

	OvrMaterial* OvrBoundingBox::GetMaterial()
	{
		OvrMaterial* material = (OvrMaterial*)OvrMaterialManager::GetIns()->GetByName("DebugBoundingBox");
		if (!material)
		{
			material = OvrMaterialManager::GetIns()->Create("DebugBoundingBox");
			material->SetProgram("VertexColor");
		}
		return material;
	}

	void OvrBoundingBox::SetupBoundingBox(const OvrBox3f& a_box)
	{
		if ((bounding_box.get_max() == a_box.get_max())
			&& (bounding_box.get_min() == a_box.get_min()))
		{
			return;
		}
		Reset();
		BuildBoundingBoxMesh(a_box);
		bounding_box = a_box;
	}

	void OvrBoundingBox::GetRenderOperation(OvrRenderOperation& a_op)
	{
		a_op.use_indices = true;
		a_op.primitive_type = EPT_LINES;
		a_op.index_data = index_data;
		a_op.vertex_data = vertex_data;
	}

	void OvrBoundingBox::BuildBoundingBoxMesh(const OvrBox3f& a_box)
	{

		const OvrVector3& box_min = a_box.get_min();
		const OvrVector3& box_max = a_box.get_max();
		struct Vertex
		{
			OvrVector3 pos;
			ovr::uint8 color[4];
		};
		int a = sizeof(Vertex);
		Vertex box_vertex[8];
		box_vertex[0].pos = OvrVector3(box_min[0], box_min[1], box_min[2]);	//0
		box_vertex[1].pos = OvrVector3(box_min[0], box_max[1], box_min[2]);	//1
		box_vertex[2].pos = OvrVector3(box_max[0], box_min[1], box_min[2]);	//2
		box_vertex[3].pos = OvrVector3(box_max[0], box_max[1], box_min[2]);	//3
		box_vertex[4].pos = OvrVector3(box_min[0], box_min[1], box_max[2]);	//4
		box_vertex[5].pos = OvrVector3(box_min[0], box_max[1], box_max[2]);	//5
		box_vertex[6].pos = OvrVector3(box_max[0], box_min[1], box_max[2]);	//6
		box_vertex[7].pos = OvrVector3(box_max[0], box_max[1], box_max[2]);	//7
		for (int i = 0; i < 8; i++)
		{
			box_vertex[i].color[0] = 255;
			box_vertex[i].color[1] = 0;
			box_vertex[i].color[2] = 0;
			box_vertex[i].color[3] = 255;
		}

		ovr::uint32 ids[24] = { 0,1,1,3,3,2,2,0, 4,5,5,7,7,6,6,4, 1,5,7,3,6,2,4,0 };

		OvrHardwareBufferManager* mgr = OvrHardwareBufferManager::GetIns();
		vertex_data = new OvrVertexData();
		vertex_data->declaration->AddElement(0, VET_FLOAT3, VES_POSITION);
		vertex_data->declaration->AddElement(12, VET_UBYTE4, VES_COLOR);
		vertex_data->vertex_buffer = mgr->CreateVertexBuffer(16, 8, &box_vertex[0]);
	
		index_data = new OvrIndexData();
		index_data->index_buffer = mgr->CreateIndexBuffer(24, ids);

	}

	void OvrBoundingBox::Reset()
	{
		if (vertex_data)
		{
			delete vertex_data;
			vertex_data = nullptr;
		}
		if (index_data)
		{
			delete index_data;
			index_data = nullptr;
		}
		bounding_box.get_min() = OvrVector3::ZERO;
		bounding_box.get_max() = OvrVector3::ZERO;
	}

}