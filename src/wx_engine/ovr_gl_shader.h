#pragma once
#include "ovr_gl_define.h"
namespace ovr_engine
{	
	enum OvrShaderType
	{
		ST_VERTEX_TYPE,
		ST_FRAGMENT_TYPE,
		ST_GEOMETRY_TYPE,
		ST_COMPUTE_TYPE,
		ST_TESS_CONTROL_TYPE,
		ST_TESS_EVALUATION_TYPE
	};

	class OvrGLShader
	{
	public:
		OvrGLShader();
		virtual ~OvrGLShader();
		OvrShaderType	GetType();
		const char*		GetSource();
		GLuint			GetShaderID();
		void            SetSource(const char* a_shader_str);
		void            SetType(OvrShaderType a_type);
		void			Reset();
		bool            Compile();
	protected:
		GLuint        shader_id;
		OvrShaderType type;
		const char*   source;
	};
}