#include "ovr_bitmap_font_texture.h"
#include "ovr_gl_define.h"
#include <vector>
//can change to some work like deal with 2d rect inserts with different size.
class space_control
{
public:
	//modify the position of font glyph.
	space_control(int total_width,int total_height)
		:_total_width(total_width),_total_height(total_height)
		,_fix_height(0),_total_lines(0)
		,_xscale(1.0/total_width),_yscale(1.0/total_height)
	{

	}

	void set_fix_height(int fix_height)
	{

		_fix_height = fix_height+4;
		if (_fix_height %4 != 0)
		{
			_fix_height = ((_fix_height>>2) + 1)<<2;
		}
		_total_lines = _total_height/_fix_height;
		_left_widths.resize(_total_lines,_total_width);
	}

	bool insert_glyph(OvrFontGlyphInfo* glyph)
	{
		for (int i=0;i<_total_lines;++i)
		{
			if (_left_widths[i] >= glyph->width)
			{
				glyph->x = (float)(_total_width - _left_widths[i]);
				glyph->y = (float)(i*_fix_height);
				_left_widths[i] -= (int)glyph->width;
				if (glyph->height > _fix_height)
				{
					//LVR_LOG("warning.this can be break our design");
				}
				break;
			}
		}
		return true;
	}
public:
	int	_total_width;
	int _total_height;
	unsigned int _fix_height;
	int _total_lines;
	double _xscale;
	double _yscale;
	std::vector<int>	_left_widths;
};

using namespace ovr_engine;
void OvrBitmapFontTexture::init(int a_w,int a_h,int a_char_size)
{
	release();
	glGenTextures(1,&tex_id);
	glBindTexture(GL_TEXTURE_2D, tex_id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	unsigned char* uc = new unsigned char[a_w*a_h];
	memset(uc,0,a_w*a_h*sizeof(char));
	glTexImage2D(GL_TEXTURE_2D,0,GL_ALPHA,a_w,a_h,0,GL_ALPHA,GL_UNSIGNED_BYTE,uc);
	delete[] uc;
	_width = a_w;
	_height = a_h;
	_sc = new space_control(_width,_height);
	_sc->set_fix_height(a_char_size);
	_half_word_advance_x = (float)((float)(_char_size>>1)/(float)_width);
}

void OvrBitmapFontTexture::bind(int a_slot)
{
	glActiveTexture(GL_TEXTURE0+a_slot);
	glBindTexture(GL_TEXTURE_2D, tex_id);
}
//should make a default info here.so we can do our work.let the first place be a blank
OvrFontGlyphInfo* OvrBitmapFontTexture::get_font_glyph_info(int32_t a_char_code)
{
	std::map<int32_t,OvrFontGlyphInfo*>::iterator it = _page.find(a_char_code);
	if (it != _page.end())
	{
		return it->second;
	}
	return NULL;
}

OvrFontGlyphInfo* OvrBitmapFontTexture::put_glyph(OvrFontGlyph* a_font_glyph)
{
	if (!_sc)
	{
		return NULL;
	}
	OvrFontGlyphInfo* lfgi = new OvrFontGlyphInfo;
	lfgi->width = (float)a_font_glyph->width;
	lfgi->height = (float)a_font_glyph->height;
	lfgi->advance_x = (float)a_font_glyph->advance_x;
	lfgi->advance_y = (float)a_font_glyph->advance_y;
	lfgi->bearing_x = (float)a_font_glyph->bearing_x;
	lfgi->bearing_y = (float)a_font_glyph->bearing_y;
	lfgi->char_code = a_font_glyph->char_code;
	_sc->insert_glyph(lfgi);//modify x,y inside
	glBindTexture(GL_TEXTURE_2D, tex_id);
	glTexSubImage2D(GL_TEXTURE_2D,0,(int)lfgi->x,(int)lfgi->y,(int)lfgi->width,(int)lfgi->height,GL_ALPHA,GL_UNSIGNED_BYTE,a_font_glyph->alpha_bits);
	//_tex->update_rect(lfgi->x,lfgi->y,lfgi->width,lfgi->height,(const char*)(a_font_glyph->alpha_bits));
	const float xs = 1.0f / _width;
	const float ys = 1.0f / _height;
	lfgi->x *= xs;
	lfgi->y *= ys;
	lfgi->width *= xs;
	lfgi->height *= ys;
	lfgi->advance_x *= xs;
	lfgi->advance_y *= ys;
	lfgi->bearing_x *= xs;
	lfgi->bearing_y *= ys;
	_page[lfgi->char_code] = lfgi;
	return lfgi;
}

void OvrBitmapFontTexture::release()
{
	if (_sc)
	{
		delete _sc;
		_sc = 0;
	}
	if (tex_id)
	{
		glDeleteTextures(1,&tex_id);
		tex_id = 0;
	}
	std::map<int32_t,OvrFontGlyphInfo*>::iterator it = _page.begin();
	for (;it != _page.end();it++)
	{
		if (it->second)
		{
			delete it->second;
			it->second = 0;
		}
	}
	std::map<int32_t,OvrFontGlyphInfo*>  tmp;
	_page.swap(tmp);
}

OvrBitmapFontTexture::OvrBitmapFontTexture()
	:OvrGLTexture("BitmapFontTex",false),
	_width(0),
	_height(0),
	_char_size(24),
	_sc(0)
{

}

OvrBitmapFontTexture::~OvrBitmapFontTexture()
{
	release();
}
