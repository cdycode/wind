#include "headers.h"
#include <wx_engine/ovr_object.h>
#include "wx_base/wx_unique_name.h"
namespace ovr_engine 
{
	OvrObject::OvrObject() 
		:display_name("RenderObject")
	{
		UniqueNameCreater::CreateActorUniqueName(unique_name);
	}
	OvrObject::~OvrObject() 
	{
	}

	void OvrObject::SetUniqueName(const char* a_name)
	{
		unique_name = a_name;
	}

}