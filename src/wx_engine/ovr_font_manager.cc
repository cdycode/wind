#include "ovr_font_manager.h"
#include <map>
#include <string>
#include "ovr_font_impl.h"

class OvrFontManager::impl
{
public:
	impl():_cur_font(0){}
	void release_font(const char* a_font_name)
	{
		std::map<std::string,OvrFont*>::iterator t_it = _fonts.find(std::string((const char*)a_font_name));
		if (t_it != _fonts.end())
		{
			delete t_it->second;
			_fonts.erase(t_it);
		}
	}
	void release_all()
	{
		std::map<std::string,OvrFont*>::iterator t_it = _fonts.begin();
		while(t_it != _fonts.end())
		{
			delete t_it->second;
			t_it++;
		}
		_fonts.clear();
	}
	~impl()
	{
		release_all();
	}
public:
	OvrFont* _cur_font;
	std::map<std::string,OvrFont*>	_fonts;
};
OvrFontManager*	OvrFontManager::get_font_manager()
{
	static OvrFontManager s_lfm;
	return &s_lfm;
}

OvrFontManager::OvrFontManager()
{
	_this = new impl;
}

OvrFontManager::~OvrFontManager()
{
	_this->release_all();
	delete _this;
}

bool OvrFontManager::add_font_from_file(const char* a_file_path,const char* a_font_name)
{
	std::map<std::string, OvrFont*>::iterator it = _this->_fonts.find(std::string((const char*)a_font_name));
	OvrFontImpl* lf = new OvrFontImpl();
	if (lf->load_from_file(a_file_path))
	{
		if (it != _this->_fonts.end())
		{
			delete it->second;
		}
		_this->_fonts[std::string((const char*)a_font_name)] = lf;
		return true;
	}
	else
	{
		delete lf;
		return false;
	}
}

bool OvrFontManager::add_font_from_mem(const char* a_file_bytes,const int a_buf_len,const char* a_font_name)
{
	OvrFontImpl* lf = new OvrFontImpl();
	if (lf->load_from_mem(a_file_bytes,a_buf_len))
	{
		_this->_fonts[std::string((const char*)a_font_name)] = lf;
		return true;
	}
	else
	{
		delete lf;
		return false;
	}
}

OvrFont* OvrFontManager::get_font(const char* a_font_name)
{
	std::map<std::string,OvrFont*>::iterator t_it = _this->_fonts.find(std::string((const char*)a_font_name));
	if (t_it != _this->_fonts.end())
	{
		return t_it->second;
	}
	return 0;
}

void OvrFontManager::release_font(const char* a_font_name)
{
	_this->release_font(a_font_name);
}

void OvrFontManager::release_all()
{
	_this->release_all();
}
