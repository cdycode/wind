#ifndef _OVR_UI_TEXT_H_
#define _OVR_UI_TEXT_H_
#include "wx_engine/i_ovr_ui_text_manager.h"
//class lvr_ui_manager;
//class lvr_ui_menu;
//class lvr_ui_text;

namespace ovr_engine
{
	class OvrLvrUiTextManager : public IOvrUiTextManager
	{
	public:
		OvrLvrUiTextManager();
		~OvrLvrUiTextManager();
		void  Initialise(const OvrString& font_path, bool a_inside);
		void  UnInitialise();
		void  AddText(const OvrTextInfo& a_text);
		void  RemoveAllText();
		void  Update(const OvrVector3& a_pos, const OvrVector3& a_dir, double a_time);
		void  Draw(const OvrMatrix4& a_vp);
		virtual void  QueryTextId(OvrTextInfo* a_info);
	private:
		int  ProcessText(int a_text);
	protected:
		//lvr_ui_manager*     ui_mgr;
		//lvr_ui_menu*        ui_menu;
		std::vector<OvrTextInfo>   texts;
	};
}
#endif