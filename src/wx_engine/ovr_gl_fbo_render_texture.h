#pragma once
#include "ovr_gl_define.h"
#include "wx_engine/ovr_texture.h"
#include "wx_engine/ovr_render_texture.h"
namespace ovr_engine
{
	class OvrGLTexture;
	class OvrGLFBORenderTexture : public OvrRenderTexture
	{
	public:
		OvrGLFBORenderTexture(OvrGLTexture* a_texture);
		virtual ~OvrGLFBORenderTexture();
		virtual void BindRTT();
		virtual void Bind(int a_depth, int a_face);
		virtual void UnBind();
		virtual void UnBindRTT();
	protected:
		GLuint fbo_id;
		GLint save_fbo_id;
		GLuint depth_buffer;
		OvrGLTexture* texture;
	};
}