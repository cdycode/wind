#include "wx_engine/ovr_texture_unit.h"
#include "wx_engine/ovr_texture.h"
using namespace ovr_engine;
ovr_engine::OvrTextureUnit& ovr_engine::OvrTextureUnit::operator=(const OvrTextureUnit& other)
{
	SetTexture(other.texture);
	texture_matrix = other.texture_matrix;
	address_mode = other.address_mode;
	border_color = other.border_color;
	filter = other.filter;
	return *this;
}

ovr_engine::OvrTextureUnit::OvrTextureUnit()
	: texture(nullptr)
	, param_changed(true)
{
	address_mode.u = TEXTURE_ADDRESSMODE_CLAMP;
	address_mode.v = TEXTURE_ADDRESSMODE_CLAMP;
	address_mode.w = TEXTURE_ADDRESSMODE_CLAMP;
	filter = TEXTURE_FILTER_BILINEAR;
	border_color = OvrColorValue(0.0f);
}

ovr_engine::OvrTextureUnit::~OvrTextureUnit()
{
	if (texture)
		texture->Drop();
}

ovr_engine::OvrTextureUnit::OvrTextureUnit(OvrTexture* a_texture_ptr)
	: texture(nullptr)
{
	SetTexture(a_texture_ptr);
	address_mode.u = TEXTURE_ADDRESSMODE_CLAMP;
	address_mode.v = TEXTURE_ADDRESSMODE_CLAMP;
	address_mode.w = TEXTURE_ADDRESSMODE_CLAMP;
	filter = TEXTURE_FILTER_BILINEAR;
	border_color = OvrColorValue(0.0f);
}

void ovr_engine::OvrTextureUnit::SetTextureTilingAndOffset(float a_tx, float a_ty, float a_ox, float a_oy)
{
	OVR_ASSERT(a_tx >= 0 && a_ty >= 0);
	texture_matrix = OvrMatrix4::make_scale_matrix(OvrVector3(a_tx, a_ty, 1.0f)) * OvrMatrix4::make_translation_matrix(OvrVector3(a_ox, a_oy, 0.0f));
}

void ovr_engine::OvrTextureUnit::SetTextureMatrix(const OvrMatrix4& a_mat)
{
	texture_matrix = a_mat;
}

void ovr_engine::OvrTextureUnit::SetTexture(OvrTexture* texture_ptr)
{
	if (texture)
	{
		texture->Drop();
	}
	if (texture_ptr)
	{
		texture_ptr->Grab();
	}
	texture = texture_ptr;
	SetDirty();
}

void ovr_engine::OvrTextureUnit::SetAddressingMode(OvrTextureAddressMode a_u, OvrTextureAddressMode a_v, OvrTextureAddressMode a_w)
{
	address_mode.u = a_u;
	address_mode.v = a_v;
	address_mode.w = a_w;
	SetDirty();
}

void ovr_engine::OvrTextureUnit::SetBorderingColor(const OvrColorValue& a_color)
{
	border_color = a_color;
	SetDirty();
}

void ovr_engine::OvrTextureUnit::SetFilter(const OvrTextureFilter& a_filter)
{
	filter = a_filter;
	SetDirty();
}

void ovr_engine::OvrTextureUnit::SetDirty(bool a_dirty)
{
	param_changed = a_dirty;
}

void ovr_engine::OvrTextureUnit::SetAddressingMode(const OvrUVWAddressingMode& a_mode)
{
	address_mode = a_mode;
	SetDirty();
}


ovr_engine::OvrUVWAddressingMode::OvrUVWAddressingMode()
	: u(TEXTURE_ADDRESSMODE_CLAMP)
	, v(TEXTURE_ADDRESSMODE_CLAMP)
	, w(TEXTURE_ADDRESSMODE_CLAMP)
{

}

ovr_engine::OvrUVWAddressingMode::OvrUVWAddressingMode(OvrTextureAddressMode a_u, OvrTextureAddressMode a_v, OvrTextureAddressMode a_w)
	: u(a_u)
	, v(a_v)
	, w(a_w)
{

}

ovr_engine::OvrUVWAddressingMode::OvrUVWAddressingMode(OvrTextureAddressMode a_mode)
	: u(a_mode)
	, v(a_mode)
	, w(a_mode)
{

}
