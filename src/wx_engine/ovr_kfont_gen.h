#ifndef __ovr_kfont_gen_h__
#define __ovr_kfont_gen_h__

//#include "lvr_font_configure.h"

#include "freetype/ft2build.h"
#include "wx_base/wx_type.h"
#include <vector>
#include FT_FREETYPE_H
#include FT_STROKER_H
#include FT_BITMAP_H

struct font_info
{
	ovr::uint16 advance_x;
	ovr::uint16 advance_y;

	ovr::int16 top;
	ovr::int16 left;
	ovr::uint16 width;
	ovr::uint16 height;

	ovr::uint32 dist_index;
};

class OvrKfont
{
public:
	OvrKfont();
	~OvrKfont();
public:
	void set_font_info(FT_Library a_lib, FT_Face a_face);
	bool generate_kfont(wchar_t a_wchar, std::vector<uint8_t>& lzma_dist, font_info& char_info);
	void get_font_dist_info(ovr::uint32 char_size, ovr::uint32 validata_chars, font_info& char_info,
		float* char_dist_data, float& min_value, float& max_value);
private:
	void compute_distance(wchar_t a_wchar, font_info& char_info, std::vector<float>& char_dist_data, float& min_value, float& max_value, int num_threads, std::vector<uint8_t> const & ttf, int start_code, int end_code, ovr::uint32 internal_char_size, ovr::uint32 char_size);
private:
	FT_Library	ft_lib;
	FT_Face		ft_face;
	ovr::uint32	_internal_char_size;
};

#endif
