﻿#include "wx_engine/ovr_render_queue.h"
#include "wx_engine/ovr_actor.h"
#include "wx_engine/ovr_material.h"
#include <algorithm>
#include <math.h>
#include <cmath>
using namespace ovr_engine;
void OvrRenderableCollector::Clear()
{
	grouped.clear();
	sorted_descending.clear();
}

void OvrRenderableCollector::AddRenderable(OvrMaterial* material, OvrRenderable* rnd)
{
	if (organisation_mode & OM_SORT_DESCENDING)
	{
		sorted_descending.push_back(RenderablePass(rnd, material));
	}

	if (organisation_mode & OM_PASS_GROUP)
	{
		PassGroupRenderableMap::iterator it = grouped.find(material);
		if (it != grouped.end())
		{
			it->second.push_back(rnd);
			return;
		}
		RenderableList rnds;
		rnds.push_back(rnd);
		grouped[material] = rnds;
	}
	
}

ovr_engine::OvrRenderableCollector::OvrRenderableCollector()
	: organisation_mode(0)
{
	grouped.clear();
}

void ovr_engine::OvrRenderableCollector::AddOrganisationMode(OrganisationMode a_om)
{
	organisation_mode |= a_om;
}

void ovr_engine::OvrRenderableCollector::ResetOrganisationMode(OrganisationMode a_om)
{
	organisation_mode = 0;
}

void ovr_engine::OvrRenderableCollector::Sort(OvrCameraComponent* a_cam)
{
	if (organisation_mode & OM_SORT_DESCENDING)
	{	
		std::stable_sort(
			sorted_descending.begin(), sorted_descending.end(),
			DepthSortDescendingLess(a_cam));
	}

}

void ovr_engine::OvrRenderableCollector::AcceptVisitor(OvrRenderableCollectorVistor* a_vistor, OrganisationMode a_om) const
{
	switch (a_om)
	{
	case OM_PASS_GROUP:
		AcceptVisitorGrouped(a_vistor);
		break;
	case OM_SORT_DESCENDING:
		AcceptVisitorDescending(a_vistor);
		break;
	}
}

void ovr_engine::OvrRenderableCollector::AcceptVisitorGrouped(OvrRenderableCollectorVistor* a_vistor)const
{
	PassGroupRenderableMap::const_iterator it = grouped.begin();
	for (; it != grouped.end(); it++)
	{
		a_vistor->visit(it->first);
		for (ovr::uint32 i = 0; i < it->second.size(); i++)
		{
			a_vistor->visit(it->second.at(i));
		}
	}
}

void ovr_engine::OvrRenderableCollector::AcceptVisitorDescending(OvrRenderableCollectorVistor* a_vistor)const
{
	for (ovr::uint32 i = 0; i < sorted_descending.size(); i++)
	{
		a_vistor->visit((RenderablePass*)(&sorted_descending[i]));
	}
}

void OvrRenderQueue::Clear()
{
	std::map<ovr::uint16, RenderPriorityGroup*>::iterator it = groups.begin();
	for (; it != groups.end(); it++)
	{
		it->second->Clear();
	}
	groups.clear();
}


ovr_engine::OvrRenderQueue::OvrRenderQueue()
	:default_renderable_priority(OVR_RENDERABLE_DEFAULT_PRIORITY)
{
}

void ovr_engine::OvrRenderQueue::AddRenderable(OvrRenderable* rnd)
{
	AddRenderable(rnd, default_renderable_priority);
}

RenderPriorityGroup* ovr_engine::OvrRenderQueue::GetRenderPriorityGroup(ovr::uint16 a_id)
{
	std::map<ovr::uint16, RenderPriorityGroup*>::iterator it = groups.find(a_id);
	if (it != groups.end())
		return it->second;
	return nullptr;
}

OvrRenderQueue::RenderPriorityGroupMap& ovr_engine::OvrRenderQueue::GetQueuePriorityGroupMap()
{
	return groups;
}

void ovr_engine::OvrRenderQueue::AddRenderable(OvrRenderable* rnd, ovr::uint16 a_priority)
{
	RenderPriorityGroup* group = GetRenderPriorityGroup(a_priority);
	if (group)
	{
		group->AddRenderable(rnd);
	}
	else
	{
		RenderPriorityGroup* new_group = new RenderPriorityGroup(this);
		new_group->AddRenderable(rnd);
		groups[a_priority] = new_group;
	}
}

bool ovr_engine::OvrRenderableCollector::DepthSortDescendingLess::operator()(const RenderablePass& a, const RenderablePass& b) const
{
	float adepth = a.renderable->GetSquaredViewDepth(camera);
	float bdepth = b.renderable->GetSquaredViewDepth(camera);

	float tolerance = std::numeric_limits<float>::epsilon();
	if (std::abs(bdepth - adepth) <= tolerance)
	{
		return a.material < b.material;
	}
	else
	{
		return (adepth > bdepth);
	}
}

ovr_engine::RenderPriorityGroup::RenderPriorityGroup(OvrRenderQueue* a_parent)
	:parent(a_parent)
{
	opaques.AddOrganisationMode(OM_PASS_GROUP);
	transparents.AddOrganisationMode(OM_SORT_DESCENDING);
	transparents.AddOrganisationMode(OM_PASS_GROUP);
}

void ovr_engine::RenderPriorityGroup::AddRenderable(OvrRenderable* a_rend)
{

	OvrMaterial* material = a_rend->GetMaterial();
	if (!material)
		return;

	if (material->IsTransparent())
	{
		AddTransparentRenderable(material, a_rend);
	}
	else
	{
		AddSolidRenderable(material, a_rend);
	}
}

void ovr_engine::RenderPriorityGroup::AddOrganisationMode(OrganisationMode a_om)
{
	opaques.AddOrganisationMode(a_om);
	transparents.AddOrganisationMode(a_om);
}

void ovr_engine::RenderPriorityGroup::AddSolidRenderable(OvrMaterial* a_material, OvrRenderable* a_rend)
{
	opaques.AddRenderable(a_material, a_rend);
}

void ovr_engine::RenderPriorityGroup::AddTransparentRenderable(OvrMaterial* a_material, OvrRenderable* a_rend)
{
	transparents.AddRenderable(a_material, a_rend);
}

void ovr_engine::RenderPriorityGroup::Clear()
{
	opaques.Clear();
	transparents.Clear();
}
