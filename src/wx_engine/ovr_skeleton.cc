#include "wx_engine/ovr_skeleton.h"
#include "wx_engine/ovr_serializer_impl.h"
using namespace ovr_engine;

void ovr_engine::OvrJoint::SetLocalMatrix(const OvrMatrix4& a_matrix)
{
	local_matrix = a_matrix;
}

void ovr_engine::OvrJoint::UpdateWorldMatrix()
{
	if (nullptr == parent)
	{
		*final_matrix = local_matrix;
	}
	else
	{
		*final_matrix = local_matrix * (*parent->final_matrix);
	}

	//�ݹ鴦���ӹ���
	for (auto child : child_list)
	{
		child->UpdateWorldMatrix();
	}
}

void ovr_engine::OvrJoint::UpdateFinalMatrix()
{
	*final_matrix = vertex_to_local_matrix * (*final_matrix);

	//�ݹ鴦���ӹ���
	for (auto child : child_list)
	{
		child->UpdateFinalMatrix();
	}
}

ovr_engine::OvrSkeleton::OvrSkeleton(OvrString a_name, bool a_is_manual)
	: OvrResource(a_name, a_is_manual)
	, skeleton_matrix(nullptr)
{

}

ovr_engine::OvrSkeleton::~OvrSkeleton()
{
	UnLoad();
}

void ovr_engine::OvrSkeleton::LoadImpl()
{
	OvrSerializerImpl::GetIns()->ImportSkeleton(name, this);
}

void ovr_engine::OvrSkeleton::UnLoadImpl()
{
	if (nullptr != skeleton_matrix)
	{
		delete[] skeleton_matrix;
		skeleton_matrix = nullptr;
	}

	joint_array.clear();
}

OvrJoint* ovr_engine::OvrSkeleton::GetJoint(ovr::uint32 a_index)
{
	if (a_index < joint_array.size())
	{
		return &joint_array[a_index];
	}
	return nullptr;
}

ovr::uint32 ovr_engine::OvrSkeleton::GetJointNum()
{
	return joint_array.size();
}

void ovr_engine::OvrSkeleton::SetJointNum(ovr::uint32 a_index)
{
	joint_array.resize(a_index);
	if (nullptr != skeleton_matrix)
	{
		delete skeleton_matrix;
		skeleton_matrix = nullptr;
	}
	skeleton_matrix = new OvrMatrix4[a_index];

	for (ovr::uint32 i = 0; i < joint_array.size(); i++)
	{
		joint_array[i].final_matrix = &(skeleton_matrix[i]);
	}
}

void ovr_engine::OvrSkeleton::UpdateMatrix()
{
	if (0 == joint_array.size())
	{
		return;
	}
	joint_array[0].UpdateWorldMatrix();
	joint_array[0].UpdateFinalMatrix();
}

OvrMatrix4* ovr_engine::OvrSkeleton::GetSkeletonMatrix()
{
	return skeleton_matrix;
}

OvrMatrix4* ovr_engine::OvrSkeleton::GetJointMatrix(ovr::uint32 a_joint_index)
{ 
	if (a_joint_index >= joint_array.size())
	{
		return nullptr;
	}
	return &skeleton_matrix[a_joint_index];
}