﻿#pragma once
#include "wx_engine/ovr_gpu_program_manager.h"
namespace ovr_engine
{
	class OvrGLGpuProgram;
	class OvrGLGpuProgramManager : public OvrGpuProgramManager
	{
	public:
		virtual void Initialise() override;
		virtual void UnInitialise()override;
		OvrGLGpuProgram* AddProgramFromString(OvrString a_name, const char* a_vs_str, const char* a_fs_str);
	};
}