﻿#include "wx_engine/ovr_renderable.h"
using namespace ovr_engine;

ovr_engine::RenderablePass::RenderablePass(OvrRenderable* a_rnd, OvrMaterial* a_material)
	: renderable(a_rnd)
	, material(a_material)
{

}

ovr_engine::RenderablePass::~RenderablePass()
{

}

OvrMatrix4* ovr_engine::OvrRenderable::GetSkeletonMatrix(ovr::uint32& a_num) const
{
	return nullptr;
}

float* ovr_engine::OvrRenderable::GetPoseWeights(ovr::uint32& a_num) const
{
	return nullptr;
}

bool ovr_engine::OvrRenderable::GetCastShadowEnable()
{
	return false;
}

float ovr_engine::OvrRenderable::GetSquaredViewDepth(const OvrCameraComponent* a_cam)
{
	return 0.0f;
}
