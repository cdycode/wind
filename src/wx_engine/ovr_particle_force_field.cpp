#include "wx_engine/ovr_particle_force_field.h"
using namespace ovr_engine;
OvrParticleForceField::OvrParticleForceField()
	:force_num(0)
{

}

OvrParticleForceField::~OvrParticleForceField()
{

}

void OvrParticleForceField::AddForce(OvrVector3 a_dir, float a_strength)
{
	force_direction.push_back(a_dir);
	force_strength.push_back(a_strength);
	force_num = force_direction.size();
}

bool OvrParticleForceField::update(OvrParticle* ao_particle, float a_delta_time)
{
	for (uint32_t i = 0; i < force_num;++i)
	{
		float v_change = force_strength[i] / ao_particle->weight*a_delta_time;
		ao_particle->cur_v += force_direction[i]*v_change;
	}
	return true;
}
