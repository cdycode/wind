﻿#include "wx_engine/ovr_hardware_buffer_manager.h"
#include "wx_engine/ovr_hardware_buffer.h"
#include "wx_base/wx_type.h"
#include "wx_engine/ovr_engine_define.h"
namespace ovr_engine
{
	class OvrHardwareVertexBuffer;
	class OvrHardwareIndexBuffer;
	class OvrVertexDeclaration;
	class OVR_ENGINE_API OvrGLHardwareBufferManager : public OvrHardwareBufferManager
	{
	public:
		OvrGLHardwareBufferManager();
		virtual OvrVertexDeclaration* CreateVertexDeclaration() override;
		virtual OvrHardwareVertexBuffer* CreateVertexBuffer(ovr::uint32 a_vertex_size, ovr::uint32 a_vcount, void* a_vdata, bool a_static_draw = true)override;
		virtual OvrHardwareIndexBuffer* CreateIndexBuffer(ovr::uint32 a_icount, void* a_idata)override;
	};
}