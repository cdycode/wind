#include "ovr_gl_vertex_array_object.h"
#include "ovr_gl_hardware_buffer.h"
using namespace ovr_engine;
int OvrGLVertexArrayObject::s_type_size[VET_COUNT] =
{
	0,
	4,  // FLOAT1
	8,  // FLOAT2
	12, // FLOAT3
	16, // FLOAT4
	2,  // HALF1
	4,  // HALF2
	6,  // HALF3
	8,  // HALF4
	4,  // UBYTE4
	2,  // SHORT1
	4,  // SHORT2
	8,   // SHORT4
	4,  // INT1
	8,  // INT2
	12, // INT3
	16 // INT4
};
int OvrGLVertexArrayObject::s_type_channel[VET_COUNT] =
{
	0,
	1,  // FLOAT1
	2,  // FLOAT2
	3, // FLOAT3
	4, // FLOAT4
	1,  // HALF1
	2,  // HALF2
	3,  // HALF3
	4,  // HALF4
	4,  // UBYTE4
	1,  // SHORT1
	2,  // SHORT2
	4,   // SHORT4
	1,	//int1
	2,	//int2
	3,	//int3
	4	//int4
};
int OvrGLVertexArrayObject::s_type_type[VET_COUNT] =
{
	0,
	GL_FLOAT,  // FLOAT1
	GL_FLOAT,  // FLOAT2
	GL_FLOAT, // FLOAT3
	GL_FLOAT, // FLOAT4
	GL_HALF_FLOAT,  // HALF1
	GL_HALF_FLOAT,  // HALF2
	GL_HALF_FLOAT,  // HALF3
	GL_HALF_FLOAT,  // HALF4
	GL_UNSIGNED_BYTE,  // UBYTE4
	GL_UNSIGNED_SHORT,  // SHORT1
	GL_UNSIGNED_SHORT,  // SHORT2
	GL_UNSIGNED_SHORT,   // SHORT4
	GL_INT,				//INT1
	GL_INT,				//INT2
	GL_INT,				//INT3
	GL_INT				//INT4
};

ovr_engine::OvrGLVertexArrayObject::OvrGLVertexArrayObject()
	:vao(0)
	,need_update(true)
{

}

ovr_engine::OvrGLVertexArrayObject::~OvrGLVertexArrayObject()
{

}

void ovr_engine::OvrGLVertexArrayObject::Bind()
{
	if (vao == 0)
	{
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		NotifyChanged();
		return;
	}
	
	glBindVertexArray(vao);
}

void ovr_engine::OvrGLVertexArrayObject::UnBind()
{
	glBindVertexArray(0);
}

void ovr_engine::OvrGLVertexArrayObject::BindToGpu(OvrGLHardwareVertexBuffer* a_vertex_buf)
{
	glBindBuffer(GL_ARRAY_BUFFER, a_vertex_buf->GetBufferId());

	for (int i = 0; i < elements.size(); ++i)
	{
		const OvrVertexElement& t_e = elements[i];
		glEnableVertexAttribArray(t_e.semantic);
		glVertexAttribPointer(t_e.semantic, s_type_channel[t_e.type], s_type_type[t_e.type], t_e.semantic == VES_COLOR ? GL_TRUE : GL_FALSE, a_vertex_buf->GetVertexSize(), (void*)(t_e.offset));
	}

	need_update = false;
}

void ovr_engine::OvrGLVertexArrayObject::NotifyChanged()
{
	need_update = true;
}

