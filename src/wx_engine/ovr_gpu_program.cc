﻿#include "wx_engine/ovr_gpu_program.h"
using namespace ovr_engine;

ovr_engine::OvrGpuProgram::OvrGpuProgram(OvrString a_name)
	: name(a_name)
	, constants(nullptr)
{

}

OvrGpuProgramParameters* ovr_engine::OvrGpuProgram::CreateParameters()
{
	if (constants)
	{
		OvrGpuProgramParameters* params = new OvrGpuProgramParameters();
		params->SetProgramConstants(constants);
		return params;
	}
	return nullptr;
}

void ovr_engine::OvrGpuProgram::DestoryParameters(OvrGpuProgramParameters* params)
{
	if (params)
		delete params;
}

const OvrGpuProgramConstants* ovr_engine::OvrGpuProgram::GetShaderConstants()
{
	return constants;
}
