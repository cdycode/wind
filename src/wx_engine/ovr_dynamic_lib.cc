﻿#include "wx_engine/ovr_dynamic_lib.h"
using namespace ovr_engine;

ovr_engine::OvrDynLib::OvrDynLib(OvrString a_name)
	: name(a_name)
	, handle(0)
{

}

ovr_engine::OvrDynLib::~OvrDynLib()
{
	UnLoad();
}

void ovr_engine::OvrDynLib::Load()
{
#if defined(OVR_OS_WIN32)
	OvrString dll_name = name + OvrString(".dll");
#else
	OvrString dll_name = name + OvrString(".so");
#endif
	handle = DYNLIB_LOAD(dll_name.GetStringConst());
	if (nullptr == handle)
	{
		OvrLogger::GetIns()->Log(OvrString("Cannot find library ") + name, LL_WARNING);
	}
	return;	
}

void ovr_engine::OvrDynLib::UnLoad()
{
	DYNLIB_UNLOAD(handle);
	handle = 0;
}

void* ovr_engine::OvrDynLib::GetSymbol(const OvrString& a_name)
{
	if (handle)
	{
		return DYNLIB_GETSYM(handle, a_name.GetStringConst());
	}
	OvrLogger::GetIns()->Log(OvrString("Cannot find symbol dllStartPlugin in library ") + a_name, LL_WARNING);
	return nullptr;
}
