﻿#include "ovr_gl_gpu_program_manager.h"
#include "ovr_gl_gpu_program.h"
#include "ovr_gl_shader.h"
#include "ovr_shader_string.h"
using namespace ovr_engine;
void ovr_engine::OvrGLGpuProgramManager::Initialise()
{
	AddProgramFromString("ShadowCasterDirection", directvs, directps);
	AddProgramFromString("ShadowCasterDirectionSkin", gc_skinned_ani_vs, directps);
	AddProgramFromString("ShadowCasterPoint", pointvs, pointps);
	AddProgramFromString("ShadowCasterPointSkin", gc_skinned_ani_vs, pointps);
	AddProgramFromString("VertexColor", vertex_color_vs, vertex_color_ps);
	AddProgramFromString("StandardWithNM", vs_standard_with_normal_tex, ps_standard_with_normal_tex);
	AddProgramFromString("StandardSkinnedWithNM", skinned_vs_standard_with_light, ps_standard_with_normal_tex);
	AddProgramFromString("Standard", vs_standard_with_light, ps_standard_with_light);
	AddProgramFromString("StandardNoLight", single_texture_vs, single_texture_ps);
	AddProgramFromString("SingleTextureMovieNormal", g_SingleTextureVertexShaderSrcMovTexcoord, g_SingleTextureFragmentShaderSrcWithColor);
	AddProgramFromString("SingleTextureMovieOES", g_SingleTextureVertexShaderSrcMovTexcoord, g_SingleTextureFragmentShaderSrcWithColor);
	AddProgramFromString("StandardSkinned", skinned_vs_standard_with_light, ps_standard_with_light);
	AddProgramFromString("StandardTransparent", vs_standard_with_light, psTest_transparent);
	AddProgramFromString("SkyBox", ovr_skybox_vs, ovr_skybox_ps);
	AddProgramFromString("SingleTexWithTransparent", ovr_singleTex_with_transparent_vs, ovr_singleTex_with_transparent_ps);
	AddProgramFromString("ovrParticleRender", ovr_particle_vs, ovr_singleTex_with_transparent_ps);
	AddProgramFromString("GlassShader", vs_glass_str, ps_galss_str);
	AddProgramFromString("SimpleRender", simple_vs, simple_ps);
	AddProgramFromString("FontRender", font_vs, font_ps);
	AddProgramFromString("AxisWorldRender", axis_world_vs, axis_world_ps);

	program_list.push_back("StandardWithNM");
	program_list.push_back("StandardSkinnedWithNM");
	program_list.push_back("Standard");
	program_list.push_back("StandardNoLight");
	program_list.push_back("SingleTextureMovieNormal");
	program_list.push_back("SingleTextureMovieOES");
	program_list.push_back("StandardSkinned");
	program_list.push_back("StandardTransparent");
	program_list.push_back("SkyBox");
	program_list.push_back("SingleTexWithTransparent");
	program_list.push_back("ovrParticleRender");
	program_list.push_back("GlassShader");
}

void ovr_engine::OvrGLGpuProgramManager::UnInitialise()
{
	std::vector<OvrString> name_list;
	std::map<OvrString, OvrGpuProgram*>::iterator cur = program_map.begin();
	std::map<OvrString, OvrGpuProgram*>::iterator end = program_map.end();
	while (cur != end)
	{
		OvrString a = cur->first;
		delete cur->second;
		cur++;
	}
	program_map.clear();
}

OvrGLGpuProgram* ovr_engine::OvrGLGpuProgramManager::AddProgramFromString(OvrString a_name, const char* a_vs_str, const char* a_fs_str)
{
	OvrGLShader* vs = new OvrGLShader;
	vs->SetSource(a_vs_str);
	vs->SetType(ST_VERTEX_TYPE);
	OvrGLShader* ps = new OvrGLShader;
	ps->SetSource(a_fs_str);
	ps->SetType(ST_FRAGMENT_TYPE);
	OvrGLGpuProgram* program = new OvrGLGpuProgram(a_name);
	program->SetShaders(vs, ps);
	program_map[a_name] = program;
	return program;
}
