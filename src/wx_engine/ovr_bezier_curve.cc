#include "wx_engine/ovr_bezier_curve.h"
//#include "ovr_logger.h"
using namespace  ovr_engine;
using namespace  ovr;
ovr_engine::OvrBezierCurve::OvrBezierCurve()
{

}

ovr_engine::OvrBezierCurve::~OvrBezierCurve()
{

}

bool ovr_engine::OvrBezierCurve::AddPoint(const OvrCtrlPoint& a_point)
{
	switch (a_point.ctrl_point_type)
	{
	case OvrCtrlPoint::e_cubicPoint:
		if (points_count < 1)
		{
			return false;
		}
		if (OvrCtrlPoint::e_quadricPoint == ctrl_points[points_count - 1].ctrl_point_type)
		{
			return false;
		}
		if (points_count > 2 && OvrCtrlPoint::e_cubicPoint == ctrl_points[points_count - 1].ctrl_point_type
			&& OvrCtrlPoint::e_point != ctrl_points[points_count - 2].ctrl_point_type)
		{
			return false;
		}
		break;
	case OvrCtrlPoint::e_quadricPoint:
		if (points_count < 1 || OvrCtrlPoint::e_point != ctrl_points[points_count - 1].ctrl_point_type)
		{
			return false;
		}
		break;
	case OvrCtrlPoint::e_point:
		segment_begin_pt_ids.push_back(points_count);
		segment_count++;
		break;
	default:
		break;
	}
	ctrl_points.push_back(a_point);
	points_count++;

	return true;
}

bool ovr_engine::OvrBezierCurve::GetPoint(int a_segment_id, float a_pos, OvrVector3& ao_point)
{

	if (a_segment_id >= segment_count || segment_begin_pt_ids[a_segment_id] >= points_count - 1)
	{
		//OvrLogger::GetIns->Log("Out Of Range", OvrLogLevel::LL_WARNING);
		return  false;
	}
	int id = segment_begin_pt_ids[a_segment_id];
	switch (ctrl_points[id + 1].ctrl_point_type)
	{
	case OvrCtrlPoint::e_cubicPoint:
		OvrBezier3(ao_point, ctrl_points[id].pos
			, ctrl_points[id + 1].pos
			, ctrl_points[id + 2].pos
			, ctrl_points[id + 3].pos
			, a_pos);
		break;
	case OvrCtrlPoint::e_quadricPoint:
		OvrBezier2(ao_point, ctrl_points[id].pos
			, ctrl_points[id + 1].pos
			, ctrl_points[id + 2].pos
			, a_pos);
		break;
	case OvrCtrlPoint::e_point:
		ao_point = OvrLerp(ctrl_points[id].pos, ctrl_points[id + 1].pos, a_pos);
		break;
	default:
		break;
	}
	return true;
}

bool ovr_engine::OvrBezierCurve::GetTangent(int segment_id, float a_pos, OvrVector3& ao_tangent)
{
	if (segment_id >= segment_count)
	{
		//LVR_LOG("Out Of Range");
		return  false;
	}
	int id = segment_begin_pt_ids[segment_id];
	switch (ctrl_points[id + 1].ctrl_point_type)
	{
	case OvrCtrlPoint::e_cubicPoint:
		OvrBezierDir3(ao_tangent, ctrl_points[id].pos
			, ctrl_points[id + 1].pos
			, ctrl_points[id + 2].pos
			, ctrl_points[id + 3].pos
			, a_pos);
		break;
	case OvrCtrlPoint::e_quadricPoint:
		OvrBezierDir2(ao_tangent, ctrl_points[id].pos
			, ctrl_points[id + 1].pos
			, ctrl_points[id + 2].pos
			, a_pos);
		break;
	case OvrCtrlPoint::e_point:
		ao_tangent = ctrl_points[id].pos - ctrl_points[id + 1].pos;
		break;
	default:
		break;
	}
}

bool ovr_engine::OvrBezierCurve::GetPointTanget(int segment_id, float a_pos, OvrVector3& ao_point, OvrVector3& ao_tangent)
{

	if (segment_id >= segment_count)
	{
		//LVR_LOG("Out Of Range");
		return  false;
	}
	int id = segment_begin_pt_ids[segment_id];
	switch (ctrl_points[id + 1].ctrl_point_type)
	{
	case OvrCtrlPoint::e_cubicPoint:
		OvrBezier3(ao_point, ctrl_points[id].pos
			, ctrl_points[id + 1].pos
			, ctrl_points[id + 2].pos
			, ctrl_points[id + 3].pos
			, a_pos);
		OvrBezierDir3(ao_tangent, ctrl_points[id].pos
			, ctrl_points[id + 1].pos
			, ctrl_points[id + 2].pos
			, ctrl_points[id + 3].pos
			, a_pos);
		break;
	case OvrCtrlPoint::e_quadricPoint:
		OvrBezier2(ao_point, ctrl_points[id].pos
			, ctrl_points[id + 1].pos
			, ctrl_points[id + 2].pos
			, a_pos);
		OvrBezierDir2(ao_tangent, ctrl_points[id].pos
			, ctrl_points[id + 1].pos
			, ctrl_points[id + 2].pos
			, a_pos);
		break;
	case OvrCtrlPoint::e_point:
		ao_point = OvrLerp(ctrl_points[id].pos, ctrl_points[id + 1].pos, a_pos);
		ao_tangent = ctrl_points[id].pos - ctrl_points[id + 1].pos;
		break;
	default:
		break;
	}
}

bool ovr_engine::OvrBezierCurve::CheckCurveState()
{
	for (int i = 0; i < points_count; i++)
	{
		OvrCtrlPoint& tp = ctrl_points[i];
		switch (tp.ctrl_point_type)
		{
		case OvrCtrlPoint::e_cubicPoint:
			break;
		case OvrCtrlPoint::e_quadricPoint:
			break;
		case OvrCtrlPoint::e_point:
			break;
		default:
			return false;
			break;
		}
	}
}
