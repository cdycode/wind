#ifndef OVR_ENGINE_HEADERS_H_
#define OVR_ENGINE_HEADERS_H_

#ifdef LVR_OS_WIN32
#define lvr_render_IMPORT
#endif
#include "lvr_render_lib.h"
#ifdef LVR_OS_WIN32
#define lvr_bitmap_font_IMPORT
#endif

//#include "lvr_bitmap_font_lib.h"
#include "lvr_distortion_api.h"
#include "lvr_gaze_cursor.h"
#include "lvr_scene_manager.h"
#include "lvr_axis_system.h"
#include "lvr_log_math_object.h"
//#include "lvr_browser_lib.h"
#include <lvr_primitive_axis.h>
#include <lvr_vertex_format.h>
#include <lvr_vertex_buffer.h>
#include <lvr_index_buffer.h>
#include <lvr_render_object.h>
#include <lvr_package_files.h>
#include <lvr_ktx.h>
#include <lvr_texture2d.h>
#include <lvr_matrix4.h>
#include "lvr_axis_common.h"
#include "lvr_axis_rotate.h"
#include "lvr_axis_scale.h"
#include "lvr_primitive_rect.h"
#include "lvr_scene_util.h"

//#include <wx_skeletal_animation/ovr_skeleton_animation_controller.h>
//#include <wx_skeletal_animation/ovr_skeletal_animation.h>

#include <wx_base/wx_box3f.h>

//#define test_use_distortion

//#include "glfw/glfw3.h"


#include <wx_asset_manager/ovr_archive_asset_manager.h>
#include <wx_asset_manager/ovr_archive_mesh.h>
#include <wx_asset_manager/ovr_archive_skeleton.h>
#include <wx_asset_manager/ovr_archive_skeletal_animation.h>
#include <wx_asset_manager/ovr_archive_material.h>
#include <wx_asset_manager/ovr_archive_texture.h>
#include <wx_asset_manager/ovr_archive_pose.h>
#include <wx_asset_manager/ovr_motion_frame.h>


#include <wx_motion/ovr_skeletal_animation_sequence.h>

#include <wx_engine/ovr_object.h>
#include <wx_engine/ovr_actor.h>
#include "wx_engine/ovr_engine.h"
#include "wx_engine/ovr_video_component.h"
#include "wx_engine/ovr_engine.h"
#include "wx_engine/ovr_actor_axis.h"
#include "wx_engine/ovr_material_manager.h"
#include "wx_engine/ovr_texture_manager.h"
#include "wx_engine/ovr_camera_component.h"
#include "wx_engine/ovr_light_component.h"
#include "wx_engine/ovr_mesh_component.h"
#include "wx_engine/ovr_box_component.h"
#endif
