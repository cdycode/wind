#pragma once
#include "wx_base/wx_type.h"
#if defined(OVR_OS_WIN32) 
#include "gl/glew.h"
#else
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES3/gl3.h>
#include <GLES3/gl3ext.h>
#define GL_SAMPLER_CUBE_MAP_ARRAY 0x900C
#define GL_SAMPLER_BUFFER 0x8DC2
#define GL_READ_WRITE 0x88BA
#endif