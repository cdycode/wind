#ifndef __ovr_bitmap_font_render_object_h__
#define __ovr_bitmap_font_render_object_h__

#include "ovr_bitmap_font_configure.h"
#include "wx_base/wx_vector3.h"
#include <vector>
class OvrBitmapFontRenderObject
{
public:
	OvrBitmapFontRenderObject();
	~OvrBitmapFontRenderObject();
public:
	int				_word_num;
	OvrVector3	_postion;
	OvrVector3	_right;
	OvrVector3	_up;
	float			_scale_value;
	float			_warp_width;
	float           _line_spacing;
	float           _font_spacing;
	ovr::uint8	    _color[4];
	std::vector<ovr::uint16>	_string_codes;
	OvrEnumFontAlignMethod _align_method;
	float			_total_cross_value;
	int				_lines;
	float			_last_line_width;
	ovr::uint32        _font_butter_offset;
};

#endif
