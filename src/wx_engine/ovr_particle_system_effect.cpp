#include "wx_engine/ovr_particle_system_effect.h"
#include "lvr_render_object.h"
using namespace ovr_engine;
OvrParticleSystemEffect::OvrParticleSystemEffect()
{
	_max_particle_num = 192;
	_particle_num = 0;
}

OvrParticleSystemEffect::~OvrParticleSystemEffect()
{

}

void OvrParticleSystemEffect::SetMaxParticleNum(uint32_t a_max_particle_num)
{
	_max_particle_num = a_max_particle_num;
	_particle_pos_sizes.resize(_max_particle_num);
	_particle_rot_matrices.resize(_max_particle_num);
	_particle_color.resize(_max_particle_num);
}

void OvrParticleSystemEffect::SetParticleRenderObject(lvr_render_object* a_ro)
{
	_ro = a_ro;
}

OvrMaterial* OvrParticleSystemEffect::GetMaterial()
{
	return _material;
}

void OvrParticleSystemEffect::SetMaterial(OvrMaterial* a_material)
{
	_material = a_material;
}

void OvrParticleSystemEffect::AddParticleSystem(OvrParticleSystem* a_particle_sys_part)
{
	_particle_system_group[a_particle_sys_part] = a_particle_sys_part;
}

void OvrParticleSystemEffect::RemoveParticleSystem(OvrParticleSystem* a_particle_sys_part)
{
	std::map<OvrParticleSystem*, OvrParticleSystem*>::iterator it = _particle_system_group.find(a_particle_sys_part);
	if (it != _particle_system_group.end())
	{
		_particle_system_group.erase(it);
	}
}

void OvrParticleSystemEffect::cull(OvrCamera* a_camera)
{
	_cull_result.clear();
	_particle_num = 0;
	std::map<OvrParticleSystem*, OvrParticleSystem*>::iterator it = _particle_system_group.begin();
	for (; it != _particle_system_group.end(); it++)
	{
		uint32_t t_pp_num;
		std::vector<OvrParticle>& too = it->second->GetParticles(t_pp_num);
		for (uint32_t i = 0; i < t_pp_num;++i)
		{
			//judge if cull result return true
			_cull_result.push_back(&too[i]);
			lvr_matrix3f m;
			//m.from_quaternion(too[i].rotation_q);
			_particle_rot_matrices[_particle_num] = m;
			_particle_pos_sizes[_particle_num] = OvrVector4(too[i].position, too[i].scale_size);
			_particle_color[_particle_num] = too[i].color;
			_particle_num++;
		}
	}
}

void OvrParticleSystemEffect::update(OvrCamera* a_camera, float a_time)
{
	std::map<OvrParticleSystem*, OvrParticleSystem*>::iterator it = _particle_system_group.begin();
	for (; it != _particle_system_group.end(); it++)
	{
		it->first->update(a_time);
	}
}

void OvrParticleSystemEffect::RenderShadow(OvrNecessaryRenderData* a_data, bool a_cube_case)
{
	/*uint32_t tnum = _particle_num;
	_material->bind();
	lvr_program* t_prog = a_cube_case?_material->_shadow_cast_prog_cube:_material->_shadow_cast_prog;
	t_prog->set_uniform4fv(t_prog->u_mvp_mat, &_particle_pos_sizes[0][0]);
	t_prog->set_uniform_matrix3fv(t_prog->u_model_mat, &_particle_rot_matrices[0][0], tnum);
	_ro->draw_instanced(tnum);
	_material->unbind();*/
}

void OvrParticleSystemEffect::draw(OvrNecessaryRenderData* a_data)
{/*
	uint32_t tnum = _particle_num;
	if (tnum < 1)
	{
		return;
	}
	_material->bind();
	lvr_program* t_prog = _material->_prog;
	t_prog->set_uniform4fv(t_prog->u_color, &_particle_color[0][0],tnum);
	t_prog->set_uniform_matrix4fv(t_prog->u_mvp_mat, &a_data->view_proj_matrix[0]);
	t_prog->set_uniform4fv("u_pos_size", &_particle_pos_sizes[0][0],tnum);
	t_prog->set_uniform_matrix3fv("u_rotmat", &_particle_rot_matrices[0][0], tnum);
	_ro->draw_instanced(tnum);
	_material->unbind();*/
}

