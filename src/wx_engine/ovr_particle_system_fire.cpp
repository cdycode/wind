#include "wx_engine/ovr_particle_system_fire.h"
#include "lvr_primitive_rect.h"
#include "wx_engine/ovr_particle_emitter_box.h"
using namespace ovr_engine;
OvrParticleSystemFire::OvrParticleSystemFire()
{

}

OvrParticleSystemFire::~OvrParticleSystemFire()
{

}

void OvrParticleSystemFire::update(OvrCamera* a_camera, float a_time)
{
	_test_emitter->SetPos(OvrVector3(cos(a_time), 0, sin(a_time))*3.0f);
	OvrParticleSystemEffect::update(a_camera, a_time);
}

void OvrParticleSystemFire::Init()
{
	lvr_primitive_rect t_rect;
	t_rect.set_uv_warp(1.0, 1.0);
	t_rect.set_pos(lvr_vector3f(0, 0, 0));
	t_rect.set_width(1.0f);
	t_rect.set_height(1.0f);
	t_rect.set_coord_axis(lvr_vector3f(1, 0, 0), lvr_vector3f(0, 1, 0));
	t_rect.update();
	/*_ro = lvr_create_standard_primitive_ro((lvr_primitive_data_base*)&t_rect);

	_material = new lvr_material;
	int w, h;
	_material->_diffuse_color.texture = lvr_texture_manager::get_texture_manager()->get_default_particle_texture();
	_material->_specular_color.texture = lvr_texture_manager::get_texture_manager()->get_defalut_texture();
	_material->_specular_color.color = lvr_vector4f(1.0, 0.0, 0.0, 1.0);
	_material->_prog = lvr_shader_manager::get_shader_mgr()->get_shader_program("ProgParticlePC_vertex_normal");
	_material->_shiness = 32.0f;
	OvrParticleEmitterBox* t_emitter_box = new OvrParticleEmitterBox;
	t_emitter_box->set_life(6.0f, 4.0f);
	t_emitter_box->set_speed(0.25f, 0.16f);
	t_emitter_box->set_size(0.6f, 0.35f);
	t_emitter_box->set_color(lvr_vector4f(1.0, 1.0, 0.0, 1.0));
	t_emitter_box->set_main_dir(lvr_vector3f(0, 1, 0));
	t_emitter_box->SetBoxSize(lvr_vector3f(0.25, 0.0, 0.25));
*/
	//_test_emitter = t_emitter_box;

	OvrParticleSystem* tps = new OvrParticleSystem;
	tps->SetMaxParticleNum(192);
	//tps->SetEmitter(t_emitter_box);
	AddParticleSystem(tps);

	SetMaxParticleNum(192);
}

void OvrParticleSystemFire::UnInit()
{

}
