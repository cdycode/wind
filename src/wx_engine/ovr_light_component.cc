#include "wx_engine/ovr_light_component.h"
#include "wx_engine/ovr_actor.h"
#include "wx_engine/ovr_logger.h"
using namespace ovr_engine;

ovr_engine::OvrLightComponent::OvrLightComponent(OvrActor* a_actor)
	:OvrComponent(a_actor)
	, diffuse_color(1.0f, 1.0f, 1.0f, 1.0f)
	, ambient_color(1.0f, 1.0f, 1.0f, 1.0f)
	, specular_color(1.0f, 1.0f, 1.0f, 1.0f)
	, attenuation(0.0f, 0.0f, 0.0f)
	, light_position(0.0f, 0.0f, 0.0f)
	, direction(0.0f, 1.0f, 0.0f)
	, range(2000)
	, falloff(30.0f)
	, theta(0.0f)
	, phi(0.0f)
	, intensity(0.6f)
	, cast_shadow(false)
	, proj_mat(0.0f)
	, view_mat(0.0f)
	, light_type(ePointLight)
{
	
}

ovr_engine::OvrLightComponent::~OvrLightComponent()
{

}

void ovr_engine::OvrLightComponent::Update()
{
	DoLightRec();
}

void ovr_engine::OvrLightComponent::SetLightType(OvrLightType a_light_type)
{
	light_type = a_light_type;
}

ovr_engine::OvrLightType ovr_engine::OvrLightComponent::GetLightType()
{
	return light_type;
}

void ovr_engine::OvrLightComponent::EnableCastShadow(bool a_shadow /*= true*/)
{
	cast_shadow = a_shadow;
}

bool ovr_engine::OvrLightComponent::IsCastShadow()
{
	return cast_shadow;
}
void ovr_engine::OvrLightComponent::SetDiffuseColor(const OvrVector4& a_diffuse)
{
	diffuse_color = a_diffuse;
}

void ovr_engine::OvrLightComponent::SetAmbientColor(const OvrVector4& a_ambient)
{
	ambient_color = a_ambient;
}

void ovr_engine::OvrLightComponent::SetSpecularColor(const OvrVector4& a_spec)
{
	specular_color = a_spec;
}

void ovr_engine::OvrLightComponent::SetAttenuation(const OvrVector3& a_attenu)
{
	attenuation = a_attenu;
}

void ovr_engine::OvrLightComponent::SetRange(const float a_range)
{
	range = a_range;
	if (a_range > 100)
	{
		OvrLogger::GetIns()->Log("Point light radius is bigger than 100.\n", LL_WARNING);
	}
}

void ovr_engine::OvrLightComponent::SetFalloff(const float a_falloff)
{
	falloff = a_falloff;
}

void ovr_engine::OvrLightComponent::SetTheta(const float a_theta)
{
	theta = a_theta;
}

void ovr_engine::OvrLightComponent::SetPhi(const float a_phi)
{
	phi = a_phi;
}

void ovr_engine::OvrLightComponent::SetIntensity(const float a_intensity)
{
	intensity = a_intensity;
}


void ovr_engine::OvrLightComponent::SetExp(float a_exp)
{
	exp = a_exp;
}

const OvrVector4& ovr_engine::OvrLightComponent::GetAmbientColor() const
{
	return ambient_color;
}

const OvrVector4& ovr_engine::OvrLightComponent::GetDiffuseColor() const
{
	return diffuse_color;
}

const OvrVector4& ovr_engine::OvrLightComponent::GetSpecularColor() const
{
	return specular_color;
}

const OvrVector3& ovr_engine::OvrLightComponent::GetPosition() const
{
	return light_position;
}

const OvrVector3& ovr_engine::OvrLightComponent::GetDirection() const
{
	return direction;
}

const OvrVector3& ovr_engine::OvrLightComponent::GetAttenuation() const
{
	return attenuation;
}

float ovr_engine::OvrLightComponent::GetRange() const
{
	return range;
}

float ovr_engine::OvrLightComponent::GetFalloff() const
{
	return falloff;
}

float ovr_engine::OvrLightComponent::GetTheta() const
{
	return theta;
}

float ovr_engine::OvrLightComponent::GetPhi() const
{
	return phi;
}

float ovr_engine::OvrLightComponent::GetIntensity() const
{
	return intensity;
}


float ovr_engine::OvrLightComponent::GetExp() const
{
	return exp;
}

const OvrMatrix4&	ovr_engine::OvrLightComponent::GetProjMat() const
{
	return proj_mat;
}

const OvrMatrix4&	ovr_engine::OvrLightComponent::GetViewMat() const
{
	return view_mat;
}

void ovr_engine::OvrLightComponent::DoLightRec()
{
	OvrQuaternion quat;
	parent_actor->GetWorldTransform().decompose(nullptr, &quat, &light_position);
	direction = quat * OvrVector3::UNIT_Z;
	OvrVector3 up = OvrVector3::UNIT_Y;

	if (fabs(up * direction) > 0.9)
	{
		up = OvrVector3::UNIT_X;
	}

	switch (light_type)
	{
	case ovr_engine::eDirectionLight:
		proj_mat = OvrMatrix4::make_ortho_matrix(45, 45, -400, 400.0);
		view_mat = OvrMatrix4::make_view_matrix(light_position, light_position + direction, up);
		break;
	case ovr_engine::eSpotLight:
		proj_mat = OvrMatrix4::make_proj_matrix_rh(1.5f, 1.0f, 0.1f, 1000.0f);
		view_mat = OvrMatrix4::make_view_matrix(light_position, light_position + direction, up);
		break;
	default:
		break;
	}
}
