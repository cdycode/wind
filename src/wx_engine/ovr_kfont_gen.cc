#include "ovr_kfont_gen.h"
#include "wx_base/wx_vector2.h"


float const SQRT2 = 1.414213f;
ovr::uint32 const NUM_CHARS = 65536;
#define LVR_PI	3.1415926535898
#define LVR_TWO_PI (LVR_PI*2.0)
#define LVR_HALF_PI (LVR_PI*0.5)
#define LVR_QUARTER_PI (LVR_PI*0.25)
#define LVR_TOLERANCE			0.00000000001
#define LVR_RAD_TO_DEG(rad)	((rad)*180.0/LVR_PI)
#define LVR_DEG_TO_RAD(deg)	((deg)*LVR_PI/180.0)

#define LVR_MIN(a,b) ((a)<(b)?(a):(b))
#define LVR_MAX(a,b) ((a)>(b)?(a):(b))

#define  lvr_min(a,b) ((a)<(b)?(a):(b))
#define  lvr_max(a,b) ((a)>(b)?(a):(b))

template <typename T>
inline T
lvr_sign(T const & x)
{
	return x < T(0) ? T(-1) : (x > T(0) ? T(1) : T(0));
}


#if defined(LVR_OS_WIN32)
#  ifdef LVR_CPU_X86
#    if defined(__cplusplus_cli)
#      define LVR_DEBUG_BREAK   do { __debugbreak(); } while(0)
#    elif defined(LVR_CC_GNU)
#      define LVR_DEBUG_BREAK   do { LVR_ASM("int $3\n\t"); } while(0)
#    else
#      define LVR_DEBUG_BREAK   do { LVR_ASM int 3 } while (0)
#    endif
#  else
#    define LVR_DEBUG_BREAK     do { __debugbreak(); } while(0)
#  endif
// Android specific debugging support
#elif defined(LVR_OS_ANDROID)
#  define LVR_DEBUG_BREAK       do { __builtin_trap(); } while(0)
// Unix specific debugging support
#elif defined(LVR_CPU_X86) || defined(LVR_CPU_X86_64)
#  define LVR_DEBUG_BREAK       do { LVR_ASM("int $3\n\t"); } while(0)
#else
#  define LVR_DEBUG_BREAK       do { *((int *) 0) = 1; } while(0)
#endif

#ifndef LVR_ASSERT
#if defined(_DEBUG) || defined(LVR_BUILD_DEBUG)
#define LVR_ASSERT(p)  do { if (!(p))  { LVR_DEBUG_BREAK; } } while(0)
#else
#define LVR_ASSERT(a)  ((void)0)
#endif
#endif

template <typename T>
T lvr_clip(const T& n, const T& lower, const T& upper) {
	return lvr_max(lower, lvr_min(n, upper));
}

template<class T>
void lvr_swap(T& a, T& b)
{
	T tmp = b;
	b = a;
	a = tmp;
}

class  lvr_vector2i
{
public:
	lvr_vector2i() :a(0), b(0) {}
	lvr_vector2i(int a_a,int a_b) :a(a_a), b(a_b) {}
	~lvr_vector2i() {}
public:
	int& operator[](int a_id) { return a_id == 0 ?  a : b; }
	const int& operator[](int a_id) const { return a_id == 0 ? a : b; }
	int a, b;
};

float EdgeDistance(OvrVector2 const & grad, float val)
{
	float df;
	if ((0 == grad[0]) || (0 == grad[1]))
	{
		df = 0.5f - val;
	}
	else
	{
		OvrVector2 n_grad(grad);
		n_grad.normalize();
		if (n_grad[0] < 0)
		{
			n_grad[0] = -n_grad[0];
		}
		if (n_grad[1] < 0)
		{
			n_grad[1] = -n_grad[1];
		}
		if (n_grad[0] < n_grad[1])
		{
			lvr_swap(n_grad[0], n_grad[1]);
		}

		float v1 = 0.5f * n_grad[1] / n_grad[0];
		if (val < v1)
		{
			df = 0.5f * (n_grad[0] + n_grad[1]) - sqrt(2 * n_grad[0] * n_grad[1] * val);
		}
		else if (val < 1 - v1)
		{
			df = (0.5f - val) * n_grad[0];
		}
		else
		{
			df = -0.5f * (n_grad[0] + n_grad[1]) + sqrt(2 * n_grad[0] * n_grad[1] * (1 - val));
		}
	}
	return df;
}

float AADist(std::vector<float> const & img, std::vector<OvrVector2> const & grad,
	int width, int offset_addr, lvr_vector2i const & offset_dist_xy, OvrVector2 const & new_dist)
{
	int closest = offset_addr - offset_dist_xy[1] * width - offset_dist_xy[0]; // Index to the edge pixel pointed to from c
	float val = img[closest];
	if (0 == val)
	{
		return 1e10f;
	}

	float di = new_dist.length();
	float df;
	if (0 == di)
	{
		df = EdgeDistance(grad[closest], val);
	}
	else
	{
		df = EdgeDistance(new_dist, val);
	}
	return di + df;
}

bool UpdateDistance(int x, int y, int dx, int dy, std::vector<float> const & img, int width,
	std::vector<OvrVector2> const & grad, std::vector<lvr_vector2i>& dist_xy, std::vector<float>& dist)
{
	float const EPSILON = 1e-3f;

	bool changed = false;
	int addr = y * width + x;
	float old_dist = dist[addr];
	if (old_dist > 0)
	{
		int offset_addr = (y + dy) * width + (x + dx);
		lvr_vector2i new_dist_xy;
		new_dist_xy[0] = dist_xy[offset_addr][0] - dx;
		new_dist_xy[0] = dist_xy[offset_addr][1] - dy;

		//= dist_xy[offset_addr] - lvr_vector2i(dx, dy);
		float new_dist = AADist(img, grad, width, offset_addr, dist_xy[offset_addr], OvrVector2(new_dist_xy[0], new_dist_xy[1]));
		if (new_dist < old_dist - EPSILON)
		{
			dist_xy[addr] = new_dist_xy;
			dist[addr] = new_dist;
			changed = true;
		}
	}

	return changed;
}


void AAEuclideanDistance(std::vector<float> const & img, std::vector<OvrVector2> const & grad,
	int width, int height, std::vector<float>& dist)
{
	std::vector<lvr_vector2i> dist_xy(img.size(), lvr_vector2i(0, 0));

	for (size_t i = 0; i < img.size(); ++i)
	{
		if (img[i] <= 0)
		{
			dist[i] = 1e10f;
		}
		else if (img[i] < 1)
		{
			dist[i] = EdgeDistance(grad[i], img[i]);
		}
		else
		{
			dist[i] = 0;
		}
	}

	bool changed;
	do
	{
		changed = false;

		for (int y = 1; y < height; ++y)
		{
			// Scan right, propagate distances from above & left
			for (int x = 0; x < width; ++x)
			{
				if (x > 0)
				{
					changed |= UpdateDistance(x, y, -1, +0, img, width, grad, dist_xy, dist);
					changed |= UpdateDistance(x, y, -1, -1, img, width, grad, dist_xy, dist);
				}
				changed |= UpdateDistance(x, y, +0, -1, img, width, grad, dist_xy, dist);
				if (x < width - 1)
				{
					changed |= UpdateDistance(x, y, +1, -1, img, width, grad, dist_xy, dist);
				}
			}

			// Scan left, propagate distance from right
			for (int x = width - 2; x >= 0; --x)
			{
				changed |= UpdateDistance(x, y, +1, +0, img, width, grad, dist_xy, dist);
			}
		}

		for (int y = height - 2; y >= 0; --y)
		{
			// Scan left, propagate distances from below & right
			for (int x = width - 1; x >= 0; --x)
			{
				if (x < width - 1)
				{
					changed |= UpdateDistance(x, y, +1, +0, img, width, grad, dist_xy, dist);
					changed |= UpdateDistance(x, y, +1, +1, img, width, grad, dist_xy, dist);
				}
				changed |= UpdateDistance(x, y, +0, +1, img, width, grad, dist_xy, dist);
				if (x > 0)
				{
					changed |= UpdateDistance(x, y, -1, +1, img, width, grad, dist_xy, dist);
				}
			}

			// Scan right, propagate distance from left
			for (int x = 1; x < width; ++x)
			{
				changed |= UpdateDistance(x, y, -1, +0, img, width, grad, dist_xy, dist);
			}
		}
	} while (changed);
}
void ComputeGradient(std::vector<float> const & img_2x, int w, int h, std::vector<OvrVector2>& grad)
{
	LVR_ASSERT(img_2x.size() == static_cast<size_t>(w * h * 4));
	LVR_ASSERT(grad.size() == static_cast<size_t>(w * h));

	std::vector<OvrVector2> grad_2x(w * h * 4, OvrVector2(0, 0));
	for (int y = 1; y < h * 2 - 1; ++y)
	{
		for (int x = 1; x < w * 2 - 1; ++x)
		{
			int addr = y * w * 2 + x;
			if ((img_2x[addr] > 0) && (img_2x[addr] < 1))
			{
				float s = -img_2x[addr - w * 2 - 1] - img_2x[addr + w * 2 - 1] + img_2x[addr - w * 2 + 1] + img_2x[addr + w * 2 + 1];
				grad_2x[addr] = OvrVector2(s - SQRT2 * (img_2x[addr - 1] - img_2x[addr + 1]),
					s - SQRT2 * (img_2x[addr - w * 2] - img_2x[addr + w * 2]));
				if (grad_2x[addr].length() > LVR_TOLERANCE)
				{
					grad_2x[addr].normalize();
				}
			}
		}
	}

	for (int y = 0; y < h; ++y)
	{
		for (int x = 0; x < w; ++x)
		{
			grad[y * w + x] = (grad_2x[(y * 2 + 0) * w * 2 + (x * 2 + 0)]
				+ grad_2x[(y * 2 + 0) * w * 2 + (x * 2 + 1)]
				+ grad_2x[(y * 2 + 1) * w * 2 + (x * 2 + 0)]
				+ grad_2x[(y * 2 + 1) * w * 2 + (x * 2 + 1)]) * 0.25f;
		}
	}
}

struct raster_user_struct
{
	FT_BBox bbox;
	int buf_width;
	int buf_height;
	ovr::uint32 _internal_char_size;
	bool non_empty;
	uint8_t* char_bitmap;
};

void RasterCallback(int y, int count, FT_Span const * const spans, void* const user)
{
	raster_user_struct* sptr = static_cast<raster_user_struct*>(user);

	int const y0 = sptr->buf_height - 1 - (y - sptr->bbox.yMin);
	if (y0 >= 0)
	{
		for (int i = 0; i < count; ++i)
		{
			if (spans[i].coverage > 127)
			{
				sptr->non_empty = true;
				int const x0 = spans[i].x - sptr->bbox.xMin;
				int const x_end = x0 + spans[i].len;
				int const x_align_8 = ((x0 + 7) & ~0x7);
				int x = lvr_max(0, x0);
				sptr->char_bitmap[(y0 * sptr->_internal_char_size + x) / 8] |= static_cast<uint8_t>(~(0xFF >> (x_align_8 - x)));
				if (x_align_8 < x_end)
				{
					x = x_align_8;
				}
				else
				{
					sptr->char_bitmap[(y0 * sptr->_internal_char_size + x) / 8] &= static_cast<uint8_t>((1UL << (x_end - (x & ~0x7))) - 1);
					x = x_end;
				}
				for (; x < (x_end & ~0x7); x += 8)
				{
					sptr->char_bitmap[(y0 * sptr->_internal_char_size + x) / 8] = 0xFF;
				}
				if (x < x_end)
				{
					sptr->char_bitmap[(y0 * sptr->_internal_char_size + x) / 8] = static_cast<uint8_t>((1UL << (x_end - x)) - 1);
				}
			}
		}
	}
}

void OvrKfont::get_font_dist_info(ovr::uint32 char_size, ovr::uint32 validate_chars, font_info& char_info,
	float* char_dist_data,float& min_value, float& max_value)
{
	FT_GlyphSlot ft_slot = ft_face->glyph;
	const float scale = 1.0 / sqrt(char_size*char_size * 2);
	std::vector<uint8_t> char_bitmap(_internal_char_size / 8 * _internal_char_size);
	std::vector<float> aa_char_bitmap_2x(char_size * char_size * 4);
	std::vector<float> aa_char_bitmap(char_size * char_size);
	std::vector<OvrVector2> grad(char_size * char_size);
	std::vector<float> outside(char_size * char_size);
	std::vector<float> inside(char_size * char_size);

	raster_user_struct raster_user;
	raster_user._internal_char_size = _internal_char_size;
	raster_user.char_bitmap = &char_bitmap[0];

	FT_Raster_Params params;
	memset(&params, 0, sizeof(params));
	params.flags = FT_RASTER_FLAG_AA | FT_RASTER_FLAG_DIRECT;
	params.gray_spans = RasterCallback;
	params.user = &raster_user;
	//const ovr::uint32& ch = validate_chars;

	memset(&char_bitmap[0], 0, char_bitmap.size());

	FT_UInt gindex = FT_Get_Char_Index(ft_face, validate_chars);
	FT_Load_Glyph(ft_face, gindex, FT_LOAD_NO_BITMAP);
	char_info.advance_x = static_cast<ovr::uint16>(ft_slot->advance.x / 64.0f / _internal_char_size * char_size);
	char_info.advance_y = static_cast<ovr::uint16>(ft_slot->advance.y / 64.0f / _internal_char_size * char_size);

	FT_BBox& bbox = raster_user.bbox;
	FT_Outline_Get_CBox(&ft_slot->outline, &bbox);
	bbox.xMin = lvr_sign(bbox.xMin) * (abs(bbox.xMin) + 63) / 64;
	bbox.xMax = lvr_sign(bbox.xMax) * (abs(bbox.xMax) + 63) / 64;
	bbox.yMin = lvr_sign(bbox.yMin) * (abs(bbox.yMin) + 63) / 64;
	bbox.yMax = lvr_sign(bbox.yMax) * (abs(bbox.yMax) + 63) / 64;

	int const buf_width = lvr_min(static_cast<int>(bbox.xMax - bbox.xMin), static_cast<int>(_internal_char_size));
	int const buf_height = lvr_min(static_cast<int>(bbox.yMax - bbox.yMin), static_cast<int>(_internal_char_size));
	raster_user.buf_width = buf_width;
	raster_user.buf_height = buf_height;
	raster_user.non_empty = false;

	FT_Outline_Render(ft_lib, &ft_slot->outline, &params);

	if (raster_user.non_empty)
	{
		float const x_offset = (_internal_char_size - buf_width) / 2.0f;
		float const y_offset = (_internal_char_size - buf_height) / 2.0f;

		char_info.left = static_cast<int16_t>((bbox.xMin - x_offset) / _internal_char_size * char_size + 0.5f);
		char_info.top = static_cast<int16_t>((3 / 4.0f - (bbox.yMax + y_offset) / _internal_char_size) * char_size + 0.5f);
		char_info.width = static_cast<ovr::uint16>(lvr_min(1.0f, (buf_width + x_offset) / _internal_char_size) * char_size + 0.5f);
		char_info.height = static_cast<ovr::uint16>(lvr_min(1.0f, (buf_height + y_offset) / _internal_char_size) * char_size + 0.5f);

		memset(&aa_char_bitmap_2x[0], 0, sizeof(aa_char_bitmap_2x[0]) * aa_char_bitmap_2x.size());
		memset(&grad[0], 0, sizeof(grad[0]) * grad.size());
		memset(&outside[0], 0, sizeof(outside[0]) * outside.size());
		memset(&inside[0], 0, sizeof(outside[0]) * outside.size());

		{
			float const fblock = static_cast<float>(_internal_char_size) / ((char_size - 2) * 2);
			ovr::uint32 const block = static_cast<ovr::uint32>(fblock + 0.5f);
			ovr::uint32 const block_sq = block * block;
			for (ovr::uint32 y = 2; y < char_size * 2 - 2; ++y)
			{
				for (ovr::uint32 x = 2; x < char_size * 2 - 2; ++x)
				{
					// TODO: 重写模板类型转换
					OvrVector2 temp = OvrVector2(x + 0.5f, y + 0.5f) * fblock -
						OvrVector2(static_cast<float>(x_offset), static_cast<float>(y_offset));
					lvr_vector2i const map_xy = lvr_vector2i(static_cast<int>(temp[0]), static_cast<int>(temp[1]));
					uint64_t aa64 = 0;
					for (ovr::uint32 dy = 0; dy < block; ++dy)
					{
						for (ovr::uint32 dx = 0; dx < block; ++dx)
						{
							if ( (ovr::uint32)(map_xy[0] + dx) < _internal_char_size
								&& (ovr::uint32)(map_xy[1] + dy) < _internal_char_size )
							{
								aa64 += ((char_bitmap[((map_xy[1] + dy) * _internal_char_size + (map_xy[0] + dx)) / 8] >> ((map_xy[0] + dx) & 0x7)) & 0x1) != 0 ? 1 : 0;
							}
						}
					}

					aa_char_bitmap_2x[y * char_size * 2 + x] = static_cast<float>(aa64) / block_sq;
				}
			}

			for (ovr::uint32 y = 0; y < char_size; ++y)
			{
				for (ovr::uint32 x = 0; x < char_size; ++x)
				{
					aa_char_bitmap[y * char_size + x] = (aa_char_bitmap_2x[(y * 2 + 0) * char_size * 2 + (x * 2 + 0)]
						+ aa_char_bitmap_2x[(y * 2 + 0) * char_size * 2 + (x * 2 + 1)]
						+ aa_char_bitmap_2x[(y * 2 + 1) * char_size * 2 + (x * 2 + 0)]
						+ aa_char_bitmap_2x[(y * 2 + 1) * char_size * 2 + (x * 2 + 1)]) * 0.25f;
				}
			}
		}

		ComputeGradient(aa_char_bitmap_2x, char_size, char_size, grad);

		AAEuclideanDistance(aa_char_bitmap, grad, char_size, char_size, outside);

		for (size_t i = 0; i < grad.size(); ++i)
		{
			aa_char_bitmap[i] = 1 - aa_char_bitmap[i];
			grad[i] = grad[i]*-1.0f;
		}

		AAEuclideanDistance(aa_char_bitmap, grad, char_size, char_size, inside);

		for (ovr::uint32 i = 0; i < outside.size(); ++i)
		{
			if (inside[i] < 0)
			{
				inside[i] = 0;
			}
			if (outside[i] < 0)
			{
				outside[i] = 0;
			}

			float value = (inside[i] - outside[i]) * scale;

			char_dist_data[char_info.dist_index + i] = value;
			// 用于后面缩放到 0-1
			min_value = lvr_min(min_value, value);
			max_value = lvr_max(max_value, value);
		}
	}
	else
	{
		char_info.dist_index = static_cast<ovr::uint32>(-1);
	}
}

void OvrKfont::compute_distance(wchar_t a_wchar, font_info& char_info, std::vector<float>& char_dist_data,
	float& min_value, float& max_value,
	int num_threads, std::vector<uint8_t> const & ttf, int start_code, int end_code,
	ovr::uint32 internal_char_size, ovr::uint32 char_size)
{

	//int32_t cur_num_char = 0;

	FT_Error error = FT_Set_Pixel_Sizes(ft_face, 0, internal_char_size);
	error = FT_Select_Charmap(ft_face, FT_ENCODING_UNICODE);
	if (error != 0)
	{
	//	LVR_LOG("set font info failed");
		return;
	}

	ovr::uint32 mapping = FT_Get_Char_Index(ft_face, a_wchar);
	if (mapping <= 0)
	{
		//LVR_LOG("char do not exist in this font file face %p char %d",ft_face,a_wchar);
		return;
	}
	if (static_cast<ovr::uint32>(-1) == char_info.dist_index)
	{
		char_info.dist_index = static_cast<ovr::uint32>(char_dist_data.size());
		char_dist_data.resize(char_dist_data.size() + char_size * char_size);
	}
	float max_values = -1;
	float min_values = 1;

	get_font_dist_info(char_size, a_wchar, char_info, char_dist_data.data(), min_values, max_values);
	min_value = lvr_min(min_value, min_values);
	max_value = lvr_max(max_value, max_values);

	if (abs(max_value - min_value) < 2.0f / 65536)
	{
		max_value = min_value + 2.0f / 65536;
	}
}
struct quantizer_chars_param
{
	float min_value;
	float inv_scale;
	float frscale;
	float fbase;
	std::pair<int32_t, int32_t> const * char_index;
	font_info const * char_info;
	float const * char_dist_data;
	ovr::uint32 char_size_sq;
	ovr::uint32 s;
	ovr::uint32 e;
};

void quantizer_chars(std::vector<uint8_t>& lzma_dist, quantizer_chars_param const & param)
{
	lzma_dist.clear();

	//float const min_value = param.min_value;
	//float const inv_scale = param.inv_scale;
	//float const frscale = param.frscale;
	//float const fbase = param.fbase;

	// 预计算的值，更改字库需要调整
	//float const min_value = -0.573248029f;
	//float const inv_scale = 292.579285f;
	//float const frscale = 0.00342796875f;
	//float const fbase = -0.573211670f;

	//float const min_value = param.min_value;
	//float const inv_scale = param.inv_scale;
	float const min_value = -0.612457275;
	float const inv_scale = 312.156464;

	std::vector<uint8_t> uint8_dist(param.char_size_sq); // 未压缩 distance 数据
	std::vector<uint8_t> char_lzma_dist;
	for (ovr::uint32 i = param.s; i < param.e; ++i)
	{
		int const ch = param.char_index[i].first;
		float const * dist = &param.char_dist_data[param.char_info[ch].dist_index];
		for (size_t j = 0; j < param.char_size_sq; ++j)
		{
			uint8_dist[j] = static_cast<uint8_t>(lvr_clip(static_cast<int>((dist[j] - min_value) * inv_scale + 0.5f), 0, 255));

			//float const d = dist[j] - (uint8_dist[j] * frscale + fbase);
			//mse += d * d;
		}

		lzma_dist.insert(lzma_dist.end(), uint8_dist.begin(), uint8_dist.end());
	}
}
void quantizer(std::vector<uint8_t>& lzma_dist, ovr::uint32 non_empty_chars,
	int num_threads, std::pair<int32_t, int32_t> const * char_index,
	font_info const * char_info, float const * char_dist_data,
	ovr::uint32 char_size_sq, float min_value, float max_value,
	int16_t& base, int16_t& scale)
{
	float fscale = max_value - min_value;
	base = static_cast<int16_t>(min_value * 32768 + 0.5f);
	scale = static_cast<int16_t>((fscale - 1) * 32768 + 0.5f);
	float inv_scale = 255 / fscale;

	float const frscale = (scale / 32768.0f + 1) / 255.0f;
	float const fbase = base / 32768.0f;

	{
		//ovr::uint32 n = (non_empty_chars + num_threads - 1) / num_threads;
		ovr::uint32 s = 0;//i * n;
		ovr::uint32 e = non_empty_chars; // lvr_min(s + n, non_empty_chars);

		quantizer_chars_param param;
		param.min_value = min_value;
		param.inv_scale = inv_scale;
		param.frscale = frscale;
		param.fbase = fbase;
		param.char_index = char_index;
		param.char_info = char_info;
		param.char_dist_data = char_dist_data;
		param.char_size_sq = char_size_sq;
		param.s = s;
		param.e = e;

		quantizer_chars(lzma_dist, param);
	}
}

bool OvrKfont::generate_kfont(wchar_t a_wchar, std::vector<uint8_t>& lzma_dist,font_info& char_info)
{
	std::vector<std::pair<int32_t, int32_t>> char_index;
	std::vector<float> char_dist_data;

	float min_value = 0;
	float max_value = 0;
	std::vector<uint8_t> ttf;
	compute_distance(a_wchar, char_info, char_dist_data, min_value, max_value, 1, ttf, 0, 65535, 32 * 4, 32);

	min_value = -0.612500012;
	max_value = 0.204398051;
	// char index
	char_index.clear();
	ovr::uint32 non_empty_chars = 0;

	if (char_info.dist_index != static_cast<ovr::uint32>(-1))
	{
		char_index.push_back(std::make_pair(static_cast<int32_t>(0), non_empty_chars));
		++non_empty_chars;
	}

	//-18909, -4082
	int16_t base, scale;
	if (!char_index.empty())
	{
		quantizer(lzma_dist, non_empty_chars, 8, &char_index[0], &char_info,
			&char_dist_data[0], // validate_chars * 32 * 32
			32 * 32, min_value, max_value, base, scale);
		return true;
	}
	return false;
}

void OvrKfont::set_font_info(FT_Library a_lib, FT_Face a_face)
{
	ft_lib = a_lib;
	ft_face = a_face;
}

OvrKfont::OvrKfont()
	:_internal_char_size(128)
{

}

OvrKfont::~OvrKfont()
{

}

