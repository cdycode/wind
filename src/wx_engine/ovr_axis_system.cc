﻿#include "headers.h"
#include "wx_engine/ovr_axis_system.h"
#include "wx_engine/ovr_engine.h"
namespace ovr_engine 
{
	OvrAxisSystem::OvrAxisSystem()
		:axis_system(nullptr)
	{

	}
	OvrAxisSystem::~OvrAxisSystem()
	{
		Uninit();
	}
	void OvrAxisSystem::Init()
	{
		Uninit();

		axis_system = new lvr_render_object;

		lvr_axis t_axis;
		t_axis.set_up(16, 16, 0.05f, 0.8f, 1.5f, size);
		t_axis.update();
		const lvr_primitive_data_base::vertex_fromat* t_vd = t_axis.get_data();

		lvr_vertex_format* t_vf = lvr_vertex_format::create(2,
			e_location_position, e_attrib_type_float3,
			e_location_color, e_attrib_type_ubyte4);
		struct my_vertex
		{
			lvr_vector3f pos;
			lvr_vector4uc color;
		};
		uint32_t  t_vertex_num = t_axis.get_vertex_num();
		my_vertex* t_my_data = new my_vertex[t_vertex_num * 3];

		//lvr_matrix4f rot_to_x = lvr_matrix4f::make_rotation_matrix_around_axis(lvr_vector3f(0,0,1),LVR_HALF_PI);
		//lvr_vector3f x_x = rot_to_x * y_x; 
		///lvr_vector3f x_y = rot_to_x * y_y;
		//lvr_vector3f x_z = rot_to_x * y_z;
		//0,-1,0		1,0,0		0,0,1  ok,not bad
		lvr_vector3f x_x(0, -1, 0);
		lvr_vector3f x_y(1, 0, 0);
		lvr_vector3f x_z(0, 0, 1);
		for (uint32_t i = 0; i < t_vertex_num; ++i)
		{
			my_vertex& t_v = t_my_data[i];
			const lvr_vector3f& old_v = t_vd[i].pos;
			t_v.pos = x_x*old_v[0] + x_y*old_v[1] + x_z*old_v[2];
			t_v.color = lvr_vector4uc(255, 0, 0, 255);
		}
		for (uint32_t i = 0; i < t_vertex_num; ++i)
		{
			my_vertex& t_v = t_my_data[t_vertex_num + i];
			const lvr_vector3f& old_v = t_vd[i].pos;
			t_v.pos = lvr_vector3f(old_v[0], old_v[1], old_v[2]);
			t_v.color = lvr_vector4uc(0, 255, 0, 255);
		}
		//lvr_matrix4f rot_to_z = lvr_matrix4f::make_rotation_matrix_around_axis(lvr_vector3f(1,0,0),LVR_HALF_PI);
		//lvr_vector3f z_x = rot_to_z * y_x; 
		//lvr_vector3f z_y = rot_to_z * y_y;
		//lvr_vector3f z_z = rot_to_z * y_z;
		//1,0,0		0,0,-1		0,1,0
		lvr_vector3f z_x(1, 0, 0);
		lvr_vector3f z_y(0, 0, 1);
		lvr_vector3f z_z(0, -1, 0);
		for (uint32_t i = 0; i < t_vertex_num; ++i)
		{
			my_vertex& t_v = t_my_data[t_vertex_num * 2 + i];
			const lvr_vector3f& old_v = t_vd[i].pos;
			t_v.pos = z_x*old_v[0] + z_y*old_v[1] + z_z*old_v[2];
			t_v.color = lvr_vector4uc(0, 0, 255, 255);
		}

		lvr_vertex_buffer* t_vb = new lvr_vertex_buffer;
		t_vb->set_vertex_buffer(t_vf, (const int8_t*)t_my_data, t_vertex_num * 3 * sizeof(my_vertex));

		delete[] t_my_data;

		uint32_t  t_tri_num = t_axis.get_tri_num();
		const uint16_t * t_ids = t_axis.get_tri_index();
		const uint32_t per_index_num = t_tri_num * 3;
		uint16_t* t_myindexs = new uint16_t[per_index_num * 3];
		for (int j = 0; j < 3; ++j)
		{
			const int add_id = j*t_vertex_num;
			const int t_d_id = j*per_index_num;
			for (uint32_t i = 0; i < per_index_num; ++i)
			{
				t_myindexs[t_d_id + i] = t_ids[i] + add_id;
			}
		}
		lvr_index_buffer* t_ib = new lvr_index_buffer;
		t_ib->set_index_buffer((const int8_t*)t_myindexs, per_index_num * 3, sizeof(uint16_t), e_primitive_triangle_list);

		delete[] t_myindexs;
		axis_system->set_up(t_vb, t_ib, t_vf);
	}

	void OvrAxisSystem::Uninit()
	{
		if (nullptr != axis_system)
		{
			axis_system->release_res();
			delete axis_system;
			axis_system = nullptr;
		}
	}

	void OvrAxisSystem::Draw(OvrCameraComponent* a_camera)
	{
		OvrMatrix4 v = a_camera->GetViewMatrix();
		OvrMatrix4 p = a_camera->GetProjectMatrix();
		lvr_vector3f lvr_pos(-0.01f, -0.0065f,-0.01f);
		lvr_matrix4f lvr_v;
		lvr_matrix4f lvr_p;
		for (int i = 0; i < 16; i++)
		{
			lvr_v[i] = v[i];
		}
		lvr_v[12] = 0.0f;
		lvr_v[13] = 0.0f;
		lvr_v[14] = 0.0f;
		lvr_v[15] = 1.0f;

		for (int i = 0; i < 16; i++)
		{
			lvr_p[i] = p[i];
		}
		lvr_matrix4f mvp = lvr_v * lvr_matrix4f::make_translation_matrix(lvr_pos) * lvr_p;

		glEnable(GL_DEPTH_TEST);
		lvr_program* axis_prog = lvr_shader_manager::get_shader_mgr()->get_shader_program("ProgVertexColor");
		axis_prog->bind();
		
		axis_prog->set_uniform_matrix4fv(axis_prog->u_mvp_mat, &mvp[0]);
		axis_system->draw();

		axis_prog->unbind();
	}

	OvrAxisSystemWorld::OvrAxisSystemWorld()
	{
		size = 0.001f;
	}
	OvrAxisSystemWorld::~OvrAxisSystemWorld()
	{

	}

	void OvrAxisSystemWorld::Update(OvrCameraComponent* a_camera)
	{
		//OvrVector3 camera_pos = IOvrEngine::GetIns()->GetRoamCamera()->GetCameraPos();
		//OvrMatrix4 view_mat = IOvrEngine::GetIns()->GetRoamCamera()->GetViewMatrix();
		OvrVector3 camera_pos = a_camera->GetCameraPos();
		OvrMatrix4 view_mat = a_camera->GetViewMatrix();


		const OvrVector3 view_look(-view_mat(0, 2), -view_mat(1, 2), -view_mat(2, 2));
		const OvrVector3 view_up(view_mat(0, 1), view_mat(1, 1), view_mat(2, 1));
		const OvrVector3 view_right(view_mat(0, 0), view_mat(1, 0), view_mat(2, 0));
		//_axis_pos = viewPos + view_right*-0.7f + view_up*-0.45f + view_look*0.5f;
		axis_pos = camera_pos + view_right*-0.12f + view_up*-0.065f + view_look*0.1f;
	}

}
