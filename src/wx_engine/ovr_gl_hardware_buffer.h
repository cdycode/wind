#include "wx_engine/ovr_hardware_buffer.h"
#include "ovr_gl_define.h"
namespace ovr_engine
{

	class OvrGLHardwareVertexBuffer : public OvrHardwareVertexBuffer
	{
	public:
		OvrGLHardwareVertexBuffer(ovr::uint32 a_vertex_size, ovr::uint32 a_vcount, void* a_vdata, bool a_static_draw = true);
		virtual ~OvrGLHardwareVertexBuffer();
		GLuint GetBufferId() { return id; }
		virtual void* Lock(unsigned int a_mode) override;
		virtual void  Unlock() override;
		virtual void  WriteData(int a_offset, int a_update_data_len, const void* a_datas) override;
	protected:
		GLuint id;
	};

	class OvrGLHardwareIndexBuffer : public OvrHardwareIndexBuffer
	{
	public:
		OvrGLHardwareIndexBuffer(ovr::uint32 a_icount, void* a_idata);
		virtual ~OvrGLHardwareIndexBuffer();
		GLuint GetBufferId() { return id; }
		virtual void* Lock(unsigned int a_mode) override;
		virtual void  Unlock() override;
		virtual void  WriteData(int a_offset, int a_update_data_len, const void* a_datas) override;

	protected:
		GLuint id;

	};

}