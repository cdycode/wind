﻿#include "wx_engine/ovr_hardware_buffer.h"
using namespace ovr_engine;
ovr_engine::OvrHardwareBuffer::OvrHardwareBuffer()
	: size_in_byte(0)
{
}

ovr::uint32 ovr_engine::OvrHardwareBuffer::GetSize()
{
	return size_in_byte;
}

ovr_engine::OvrHardwareVertexBuffer::OvrHardwareVertexBuffer(ovr::uint32 a_vertex_size, ovr::uint32 a_vcount)
	: vertex_size(a_vertex_size)
	, vertex_num(a_vcount)
{
	size_in_byte = a_vertex_size * a_vcount;
}

ovr_engine::OvrHardwareIndexBuffer::OvrHardwareIndexBuffer(ovr::uint32 a_icount)
	: index_count(a_icount)
{
	size_in_byte = 4 * a_icount;
}

