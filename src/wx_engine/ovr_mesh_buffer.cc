#include "wx_engine/ovr_mesh_buffer.h"
using namespace ovr_engine;

const OvrString& ovr_engine::OvrMeshBuffer::GetMaterialName()
{
	return material_name;
}

void ovr_engine::OvrMeshBuffer::SetMaterialName(const OvrString& a_material_name)
{
	material_name = a_material_name;
	is_material_set = true;
}

bool ovr_engine::OvrMeshBuffer::IsMaterialSet()
{
	return is_material_set;
}

void ovr_engine::OvrMeshBuffer::SetPrimitiveType(OvrPrimitiveType a_type)
{
	primitive_type = a_type;
}



ovr_engine::OvrMeshBuffer::OvrMeshBuffer(OvrMesh* a_mesh)
	:mesh(a_mesh)
	, is_material_set(false)
{
}

ovr_engine::OvrMeshBuffer::~OvrMeshBuffer()
{
}