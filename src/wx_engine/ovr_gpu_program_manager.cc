﻿#include "wx_engine/ovr_gpu_program_manager.h"
#include "ovr_gl_shader.h"
#include "ovr_gl_gpu_program.h"
#include "ovr_shader_string.h"
using namespace ovr_engine;
template<> OvrGpuProgramManager* ovr_engine::OvrSingleton<OvrGpuProgramManager>::s_instance = nullptr;

std::vector<OvrString> ovr_engine::OvrGpuProgramManager::GetAllProgramNames()
{
	return program_list;
}

OvrGpuProgram* ovr_engine::OvrGpuProgramManager::GetProgram(OvrString a_shader_name)
{
	std::map<OvrString, OvrGpuProgram*>::iterator it = program_map.find(a_shader_name);
	if (it != program_map.end())
	{
		return it->second;
	}
	return nullptr;
}

