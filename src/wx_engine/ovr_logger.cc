#include "wx_engine/ovr_logger.h"
using namespace ovr_engine;
template<> OvrLogger* ovr_engine::OvrSingleton<OvrLogger>::s_instance = nullptr;

ovr_engine::OvrLogger::OvrLogger()
{

}

ovr_engine::OvrLogger::~OvrLogger()
{
	UnInitialise();
}

void ovr_engine::OvrLogger::Initialise(OvrString a_file_name)
{
	out_file.open(a_file_name.GetStringConst());
}

void ovr_engine::OvrLogger::UnInitialise()
{
	out_file.close();
}

OvrString ovr_engine::OvrLogger::GetLevelString(OvrLogLevel a_level)
{
	OvrString levelstr = "UnKnown";
	switch (a_level)
	{
	case ovr_engine::LL_INFORMATION:
		levelstr = "Information";
		break;
	case ovr_engine::LL_WARNING:
		levelstr = "Warning";
		break;
	case ovr_engine::LL_ERROR:
		levelstr = "Error";
		break;
	default:
		break;
	}
	return levelstr;
}

void ovr_engine::OvrLogger::Log(OvrString a_msg, OvrLogLevel a_level)
{
	out_file << GetLevelString(a_level).GetStringConst() << " : " << a_msg.GetStringConst() << endl;
}
