#pragma once
#include "wx_engine/ovr_texture.h"
#include "ovr_gl_define.h"
#include "lvr_data_types.h"
namespace ovr_engine
{

	class OvrGLPixelUtil
	{
	public:
		static GLenum GetGLOriginFormat(OvrPixelFormat a_format);
		static GLenum GetGLOriginDataType(OvrPixelFormat a_format);
		static GLenum GetGLInternalFormat(OvrPixelFormat a_format, bool a_hwGamma = false);
		static GLenum GetClosestGLInternalFormat(OvrPixelFormat a_format, bool a_hwGamma = false);
		static OvrPixelFormat GetClosestOVRFormat(GLenum fmt);
		static ovr::uint32 GetMemorySize(OvrPixelFormat a_format, ovr::uint32 width, ovr::uint32 height);
		static OvrImage GetOvrImageInfo(const lvr_image_info& a_info);

	};

	class OvrGLFBORenderTexture;
	class OvrGLTexture : public OvrTexture
	{
	public:
		OvrGLTexture(OvrString a_name, bool a_is_manual);
		virtual ~OvrGLTexture();
		virtual bool LoadFromImage(const OvrImage& a_image);
		GLenum GetGLTextureTarget();
		GLuint GetTextureId();
		virtual void BlitFromMemory(void* a_data, ovr::uint32 a_size);

	protected:
		virtual void LoadImpl();
		virtual void UnLoadImpl();
		virtual void CreateInternalResourcesImpl();
		virtual void FreeInternalResourcesImpl();
	protected:
		GLuint tex_id;
	};
}