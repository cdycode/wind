#include <math.h>
#include "wx_engine/ovr_particle_emitter_circle.h"
#include "wx_engine/ovr_particle_class_info.h"

using namespace ovr_engine;
using namespace  ovr;
REGISTER_CLASS(OvrParticleEmitterCircle)

OvrParticleEmitterCircle::OvrParticleEmitterCircle()
	:OvrParticleEmitter()
	, radius(0)
{

}

OvrParticleEmitterCircle::~OvrParticleEmitterCircle()
{

}

void OvrParticleEmitterCircle::EmitterParticle(OvrParticle* ao_particle)
{
	OvrParticle& t_p = *ao_particle;
	float t_w = sinf(rad);
	float t_h = cosf(rad);
	OvrVector3 init_dir = OvrVector3(OvrRandom(-t_w, t_w), OvrRandom(t_h, 1.0f), OvrRandom(-t_w, t_w)).get_normalize();
	t_p.cur_v = init_dir*OvrRandom(min_speed, max_speed);
	float t_angle = OvrRandom(0.0f, (float)2*OVR_PI);
	t_p.position = OvrVector3(radius*cosf(t_angle), 0, radius*sinf(t_angle));
	t_p.life_total = OvrRandom(min_life, max_life);
	t_p.scale_size = OvrRandom(min_size, max_size);
}
