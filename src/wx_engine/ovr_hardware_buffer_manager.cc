﻿#include "wx_engine/ovr_hardware_buffer_manager.h"
#include "wx_engine/ovr_vertex_index_data.h"
using namespace ovr_engine;
template<> OvrHardwareBufferManager* ovr_engine::OvrSingleton<OvrHardwareBufferManager>::s_instance = nullptr;
ovr_engine::OvrHardwareBufferManager::OvrHardwareBufferManager()
{
	
}

void ovr_engine::OvrHardwareBufferManager::DestroyVertexDeclaration(OvrVertexDeclaration* a_ro)
{
	delete a_ro;
}
