#include "wx_engine/ovr_resource_manager.h"
#include "wx_engine/ovr_resource.h"
using namespace ovr_engine;

ovr_engine::OvrResourceManager::OvrResourceManager()
{

}

ovr_engine::OvrResourceManager::~OvrResourceManager()
{
	RemoveAll();
}

OvrResource* ovr_engine::OvrResourceManager::GetByName(const OvrString& a_file_name)
{
	std::map<OvrString, OvrResource*>::iterator it = resource_map.find(a_file_name);
	if (it != resource_map.end())
	{
		return it->second;
	}
	return nullptr;
}

void ovr_engine::OvrResourceManager::Remove(const OvrString& a_name)
{
	std::map<OvrString, OvrResource*>::iterator it = resource_map.find(a_name);
	if (it != resource_map.end())
	{
		it->second->Drop();
		resource_map.erase(it);
	}
}

void ovr_engine::OvrResourceManager::RemoveAll()
{
	std::map<OvrString, OvrResource*>::iterator it = resource_map.begin();
	while (it != resource_map.end())
	{
		it->second->Drop();
		it++;
	}
	resource_map.clear();
}

OvrResource* ovr_engine::OvrResourceManager::CreateOrRetrieve(OvrString a_name, bool is_manual)
{
	OvrResource* new_resource = GetByName(a_name);
	if (!new_resource)
	{
		new_resource = CreateResource(a_name, is_manual);
		resource_map[a_name] = new_resource;
	}
	return new_resource;
}

void ovr_engine::OvrResourceManager::Remove(OvrResource* a_mesh)
{
	std::map<OvrString, OvrResource*>::iterator it = resource_map.begin();
	while (it != resource_map.end())
	{
		if (it->second == a_mesh)
		{
			it->second->Drop();
			resource_map.erase(it);
			break;
		}
		it++;
	}
}
