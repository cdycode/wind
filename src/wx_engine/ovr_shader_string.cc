#include "ovr_shader_string.h"
#include "wx_engine/ovr_engine_define.h"
namespace ovr_engine
{
#define  HIGHP
#define  MEDIUMP
#define LOWP

#ifndef MAX_POINTLIGHT_NUM
#define MAX_POINTLIGHT_NUM " 32 "
#endif

#ifndef MAX_SPOTLIGHT_NUM
#define MAX_SPOTLIGHT_NUM " 32 "
#endif

#ifndef LVR_OS_WIN32
#define  LVR_OS_WIN32
#endif
#define light_shading_codes_1122 "float DecodeFloatRGBA(vec4 rgba)\
				{return dot(rgba, vec4(1.0, 1.0 / 255.0, 1.0 / 65025.0, 1.0 / 16581375.0));}\n"\
"float rcp(float x){return 1.0/x;}"\
"float Square( float x ){return x*x;}\
	vec2 Square(vec2 x) { return x*x; }\
	vec3 Square(vec3 x) { return x*x; }\
	vec4 Square(vec4 x) { return x*x; }"\
	"vec3 Diffuse_OrenNayar(vec3 DiffuseColor, float Roughness, float NoV, float NoL, float VoH)\
{\
	float a = Roughness * Roughness;\
	float s = a;\
	float s2 = s * s;\
	float VoL = 2 * VoH * VoH - 1;\
	float Cosri = VoL - NoV * NoL;\
	float C1 = 1 - 0.5 * s2 / (s2 + 0.33);\
	float C2 = 0.45 * s2 / (s2 + 0.09) * Cosri * (Cosri >= 0 ? rcp(max(NoL, NoV)) : 1);\
	return DiffuseColor / 3.1415926535898 * (C1 + C2) * (1 + Roughness * 0.5);\
}\n"\
"vec3 F_Fresnel(vec3 SpecularColor, float VoH)\
{\
	vec3 SpecularColorSqrt = sqrt(clamp(vec3(0, 0, 0), vec3(0.99, 0.99, 0.99), SpecularColor));\
	vec3 n = (1 + SpecularColorSqrt) / (1 - SpecularColorSqrt);\
	vec3 g = sqrt(n*n + VoH*VoH - 1);\
	return 0.5 * Square((g - VoH) / (g + VoH)) * (1 + Square(((g + VoH)*VoH - 1) / ((g - VoH)*VoH + 1)));\
}\n"\
	"float normal_shadow_2d(sampler2D shadow_map,vec4 a_coord){"\
				"vec3 uvz = a_coord.xyz/a_coord.w;"\
				"vec3 final_cd = (uvz+1.0)*0.5;"\
				"const float elps = 0.0001;"\
				"return texture2D(shadow_map, final_cd.xy).r+elps < final_cd.z?0.0:1.0;}"\
		"float normal_shadow_cube(samplerCube shadow_map,vec4 a_dir_len,float light_range){"\
				"float real_z = a_dir_len.w / light_range;"\
				"float ref_z = DecodeFloatRGBA(textureCube(shadow_map, a_dir_len.xyz));"\
				"return real_z <= ref_z + 0.001 ? 1.0:0.0;}"\
		"vec3 FogShading(vec3 pos,vec3 origin_color,vec3 fog_color,vec2 fog_distance)"\
			"{"\
				"float dis = length(pos);\n"\
				"float t_fogfactor = clamp((dis - fog_distance.x)/fog_distance.y,0.0,1.0);\n"\
				"return mix(origin_color,fog_color,t_fogfactor);"\
			"}\n"\
		"vec3 light_shading(vec3 n,vec3 s,vec3 v,vec3 kd,vec3 ks,vec3 ka,vec2 shininess_light_object,vec3 light_color,float light_intensity)"\
			"{"\
		"		vec3 MatSpecularIntensityv = (texture2D( Texture3, oTexCoord0 )).xyz;\n"\
	    HIGHP " vec3 h = reflect( -s, n );"\
		HIGHP " vec3 Is = ks*pow(max(dot(h, v), 0.0), 8.0)*MatSpecularIntensityv;"\
		HIGHP " vec3 Id = kd*max(dot(s, n), 0.0);"\
		HIGHP " vec3 Ia = ka;"\
			   "return light_color*(Is + Id)*light_intensity;"\
			 "}"\
	"vec3 light_shading0(vec3 n,vec3 s0,vec3 v,vec3 kd,vec3 ks,vec3 ka,vec2 shininess_light_object,vec3 light_color,float light_intensity)"\
			"{"\
        HIGHP " vec3 s = normalize(s0);\n "\
		HIGHP "	vec3 MatSpecularIntensityv = (texture2D( Texture3, oTexCoord0 )).xyz;\n"\
	    HIGHP " vec3 h = normalize(s + v);"\
		HIGHP " float voh = dot(v,h);"\
		HIGHP " float nov = dot(n,v);"\
		HIGHP " float nos = dot(n,s);"\
		HIGHP " vec3 Is = F_Fresnel(MatSpecularIntensityv,voh) * 0.1;"\
		HIGHP " vec3 Id = Diffuse_OrenNayar(kd.xyz,0.05,nov,nos,voh);"\
		HIGHP " vec3 Ia = ka;"\
			   "return light_color*(Is + Id)*light_intensity;"\
			 "}"\
		"vec3 spot_light_shading(vec3 n,vec3 s,vec3 v,vec3 direction,float exponent,float cut_off,vec3 kd,vec3 ks,vec3 ka,vec2 shininess_level_matIntensity,vec3 light_color,float light_intensity)"\
			"{"\
		HIGHP "float angle = acos(dot(-s, direction));"\
		HIGHP "float cutoff = clamp(cut_off, 0.0, 3.14);"\
		HIGHP "vec3 Id,Is;"\
				"if (angle < cutoff)"\
				"{"\
					"float spot_factor = pow(dot(-s, direction), exponent);"\
			  HIGHP " vec3 h = reflect( -s, n );"\
					"Id = spot_factor*kd*max(dot(s, n), 0.0);"\
					"Is = spot_factor*ks*(pow(max(dot(h, n), 0.0), shininess_level_matIntensity.x)*shininess_level_matIntensity.y);"\
				"}"\
				"else"\
				"{\n"\
					"Id = vec3(0.0);"\
					"Is = vec3(0.0);"\
				"}\n"\
			  HIGHP " vec3 Ia = ka;"\
					"return (Is + Ia + Id)*light_color*light_intensity;"\
			 "}\n"\
	    "vec3 decode_normal(vec2 n)\
			{\
				vec3 normal;\
				normal.z = dot(n, n) * 2 - 1;\
				normal.xy = normalize(n) * sqrt(1 - normal.z * normal.z);\
				return normal;\
			}"\
		"vec2 encode_normal(vec3 normal)\n"\
			"{\n"\
				"return normalize(normal.xy) * sqrt(normal.z * 0.5 + 0.5);\n"\
			"}\n"\
			"const vec2 c_delta_around[8] = vec2[](\
			vec2(-1, 1), vec2(1, -1), vec2(-1, -1), vec2(1, 1),\
			vec2(-1, 0), vec2(1, 0), vec2(0, -1), vec2(0, 1)); "
	

	const char* vs_standard_with_normal_tex = "precision highp float;"
		"attribute " HIGHP " vec4 Position;\n"
		"attribute " HIGHP " vec3 Normal;\n"
		"attribute " HIGHP " vec3 Tangent;\n"
		"attribute " HIGHP " vec3 Binormal;\n"
		"attribute " HIGHP " vec2 TexCoord;\n"
		"uniform " HIGHP " vec4 uOffsetRes;\n"//use it when needed
		"uniform " HIGHP " mat4 Mvpm;\n"
		"uniform " HIGHP " mat4 Modelm;\n"
		"varying " MEDIUMP " vec2 oTexCoord0;\n"
		"varying  " HIGHP " vec3 v_world_position;\n"
		"varying  " HIGHP " vec3 v_world_tangent;\n"
		"varying  " HIGHP " vec3 v_world_binormal;\n"
		"uniform " HIGHP " mat4 Texm;\n"
		"uniform " HIGHP " mat4 Texm2;\n"
		"varying  " HIGHP " vec3 v_world_normal;\n"
		"void main()\n"
		"{\n"
		" " HIGHP " vec3 t_pos =Position.xyz;"
		"   gl_Position = Mvpm * vec4(t_pos,1.0 );\n"
		"   v_world_position = (Modelm * vec4(t_pos, 1.0)).xyz;\n"
		"   v_world_binormal = (Modelm * vec4(Binormal, 0.0)).xyz;\n"
		"   v_world_tangent = (Modelm * vec4(Tangent, 0.0)).xyz;\n"
		"   vec3 w_n = (Modelm * vec4(Normal, 0.0)).xyz;\n"
		"   v_world_normal = cross( v_world_tangent, v_world_binormal);\n"
		"if(dot(v_world_normal, w_n) < 0)\n"
		"{ v_world_normal = -v_world_normal;}\n"
		//"   v_world_normal =(Modelm * vec4(Normal, 0.0)).xyz ;\n"
		"	oTexCoord0 =vec2( Texm * vec4(TexCoord,1,1) );\n"
		"}\n";

	const char* ps_standard_with_normal_tex = 
#ifdef LVR_OS_WIN32
		"#version 400 core\n"
		"#extension GL_NV_shadow_samplers_cube:enable\n"
		"#extension GL_EXT_texture_array:enable\n"
		"#extension GL_ARB_texture_cube_map_array:enable\n"
		"#extension GL_EXT_gpu_shader4_1:enable\n"
#endif
		"uniform  sampler2D Texture0;\n" //Diffuse map
		"uniform  sampler2D Texture1;\n"  //emissive
		"uniform  sampler2D Texture2;\n" //Normal map  
		"uniform  sampler2D Texture3;\n"   //Specular shiness map
										   //"uniform  sampler2D Texture4;\n" // Directional light shadow map
		"uniform  sampler2DArray Texture5;\n" //spot light shadow map 0
		"uniform  sampler2D Texture6;\n" // Directional light shadow map
		"uniform  samplerCubeArray Texture7;\n" //point light shadow map
		"uniform " HIGHP " vec4 uOffsetRes;\n"//use it when needed
		"uniform" LOWP " vec3 uFogColor;\n"
		"uniform" HIGHP " vec2 uFogParm;\n"//near distance,far distance minus near
		"uniform " HIGHP " vec3 EyePosition;\n"
		"uniform " HIGHP " vec4 DirectionLightColorIntensity;\n"
		"uniform " HIGHP " vec3 DirectionLightDir;\n"
		"uniform " HIGHP " mat4 DirectionLightVPMat;\n"
		"uniform " HIGHP " vec4 EnvLightColorIntensity;\n"
		"uniform " HIGHP " vec4 PointLightColorIntensity[32];\n"
		"uniform " HIGHP " vec4 PointLightPosRadius[32];\n"
		"uniform " HIGHP " vec4 SpotLightColorIntensity[32];\n"
		"uniform " HIGHP " vec4 SpotLightPosExponent[32];\n"
		"uniform " HIGHP " vec4 SpotLightDirCutOff[32];\n"
		"uniform " HIGHP " mat4 SpotLightVPMat[32];\n"
		"uniform " HIGHP " float uShininess;\n"
		"uniform " HIGHP " ivec4 light_num;\n"//directional light num,spotlight num,point light num
		"uniform " HIGHP "  vec4 DiffuseColor ;\n"   //Specular shiness map
		"varying  " HIGHP " vec3 v_world_position;\n"
		"varying  " HIGHP " vec3 v_world_tangent;\n"
		"varying  " HIGHP " vec3 v_world_binormal;\n"
		"varying  " HIGHP " vec3 v_world_normal;\n"
		"varying " MEDIUMP " vec2 oTexCoord0;\n"
		"varying  " HIGHP " vec4 v_temp;\n"

		//"varying  " HIGHP " vec4 oColor;\n"
		light_shading_codes_1122
		"float normal_shadow_2darray(sampler2DArray shadow_map,vec4 a_coord,int layerid){"\
		   "vec3 uvz = a_coord.xyz/a_coord.w;"\
		   "vec3 final_cd = (uvz+1.0)*0.5;"\
		   "const float elps = 0.001;"\
		   "return texture2DArray(shadow_map, vec3(final_cd.xy,layerid)).r+elps < final_cd.z?0.0:1.0;}"\
		"float normal_shadow_cube_array(samplerCubeArray shadow_map,vec4 a_dir_len,float light_range,int layerid){"\
		   "float real_z = a_dir_len.w / light_range;"\
		   "float ref_z = DecodeFloatRGBA(texture(shadow_map, vec4(a_dir_len.xyz,layerid)));"\
		   "return real_z <= ref_z + 0.001 ? 1.0:0.0;}"
		"void main()\n"
		"{\n"
		"   " MEDIUMP "vec4 kd = texture2D(Texture0,oTexCoord0);\n"
		"   " HIGHP " vec3 normal = texture2D( Texture2, oTexCoord0 ).xyz * 2.0 - 1.0;\n"
		"   " HIGHP " vec3 n = normal.x * v_world_tangent + normal.y * v_world_binormal + normal.z * v_world_normal;\n"
		//"     vec3  n = v_world_normal;"
		"   " HIGHP " vec3 v = normalize( EyePosition - v_world_position);\n"
		"   " MEDIUMP " vec3 ks = kd.xyz;\n"
		"   " MEDIUMP " vec2 shininess2 = vec2(1.0,texture2D( Texture3, oTexCoord0 ).x);\n"
		"   " MEDIUMP " vec3 ka = kd.xyz*EnvLightColorIntensity.xyz*EnvLightColorIntensity.w;\n"//to do ... ka should cal alone
		"vec3 light_effect_color =  EnvLightColorIntensity.xyz*kd.xyz*EnvLightColorIntensity.w;\n"

		//directional light
		//"for (int i = 0; i < light_num.x; i++){"
		"if (light_num.x > 0){"
		"   " HIGHP "vec4 shadow_coord = DirectionLightVPMat * vec4(v_world_position,1.0);"
		"   " MEDIUMP "float shadow = normal_shadow_2d(Texture6, shadow_coord);"
		"   light_effect_color += light_shading(n,-DirectionLightDir,v,kd.xyz,ks.xyz,ka,shininess2,DirectionLightColorIntensity.xyz,DirectionLightColorIntensity.w)*shadow;\n"
		//"    light_effect_color = vec3(shadow,shadow,shadow);\n"
			"}"

			//spotlight
			"for (int j = 0; j < light_num.y; j++){"
			"   " HIGHP "vec3 dist = SpotLightPosExponent[j].xyz - v_world_position;"
			"   " HIGHP "vec3 pt_s = normalize(dist);"
			"   " HIGHP "vec4 shadow_coord = SpotLightVPMat[j] * vec4(v_world_position,1.0);"
			"   " MEDIUMP "float shadow = normal_shadow_2darray(Texture5, shadow_coord,j);"
			"   " MEDIUMP " vec3 t_color = spot_light_shading(n,pt_s,v,SpotLightDirCutOff[j].xyz,SpotLightPosExponent[j].w,\
			  SpotLightDirCutOff[j].w,kd.xyz,ks.xyz,ka,shininess2,SpotLightColorIntensity[j].xyz,SpotLightColorIntensity[j].w);\n"
		"light_effect_color += t_color*shadow; }"
			//point light 
			"for (int k = 0; k < light_num.z; k++){"
			"   " HIGHP "vec3 dist = PointLightPosRadius[k].xyz - v_world_position;"
			"   " HIGHP " float real_intensity = PointLightColorIntensity[k].w* cos(clamp(length(dist)/PointLightPosRadius[k].w,0.0,1.0)*1.57);"
			"   " HIGHP "float dist_len = length(dist);"
			"   " HIGHP "vec3 pt_s = normalize(dist);"
			"   " MEDIUMP "float shadow = normal_shadow_cube_array(Texture7, vec4(pt_s*(-1),dist_len),PointLightPosRadius[k].w,k);"
			"   " MEDIUMP " vec3 t_color = light_shading(n,pt_s,v,kd.xyz,ks.xyz,ka,shininess2,PointLightColorIntensity[k].xyz,real_intensity);\n"
			"light_effect_color += t_color * shadow; }\n"

			"   gl_FragColor = vec4(FogShading(v_world_position-EyePosition,light_effect_color,uFogColor,uFogParm),kd.a);\n"
		    "   vec4 ke = texture2D(Texture1,oTexCoord0);\n"
		"   gl_FragColor = vec4((light_effect_color + ke.xyz) * DiffuseColor.xyz, DiffuseColor.w*kd.w);\n"
			//"   gl_FragColor = vec4(kd.xyz,1.0);\n"

		"}\n";

		const char* skinned_vs_standard_with_light =
			 "precision highp float;"
			"attribute " HIGHP " vec4 Position;\n"
			"attribute " HIGHP " vec3 Normal;\n"
			"attribute " HIGHP " vec3 Tangent;\n"
			"attribute " HIGHP " vec3 Binormal;\n"
			"attribute " HIGHP " vec2 TexCoord;\n"
			"attribute " HIGHP " vec4 JointWeights;\n"
			"attribute " HIGHP " vec4 JointIndices;\n"
			"uniform " HIGHP " mat4 Mvpm;\n"
			"uniform " HIGHP " mat4 Texm;\n"
			"uniform " HIGHP " mat4 Modelm;\n"
			"uniform " HIGHP " mat4 Joints[48];\n"
			"varying " MEDIUMP " vec2 oTexCoord0;\n"
			"varying  " HIGHP " vec3 v_world_position;\n"
			"varying  " HIGHP " vec3 v_world_tangent;\n"
			"varying  " HIGHP " vec3 v_world_binormal;\n"
			"varying  " HIGHP " vec3 v_world_normal;\n"
			"vec3 multiply( mat4 m, vec3 v )\n"
			"{\n"
			"   return vec3(\n"
			"      m[0].x * v.x + m[1].x * v.y + m[2].x * v.z,\n"
			"      m[0].y * v.x + m[1].y * v.y + m[2].y * v.z,\n"
			"      m[0].z * v.x + m[1].z * v.y + m[2].z * v.z );\n"
			"}\n"
			"void main()\n"
			"{\n"
			" " HIGHP " vec3 t_pos = ((Joints[int(JointIndices.x)] * vec4(Position.xyz,1.0))*JointWeights.x + (Joints[int(JointIndices.y)]* vec4(Position.xyz,1.0))*(1.0-JointWeights.x)).xyz;\n"
			" " HIGHP " vec3 t_binormal = multiply( Joints[int(JointIndices.x)], Binormal)*JointWeights.x + multiply(Joints[int(JointIndices.y)], Binormal)*(1.0-JointWeights.x) ;\n"
			" " HIGHP " vec3 t_tangent = multiply( Joints[int(JointIndices.x)], Tangent)*JointWeights.x + multiply(Joints[int(JointIndices.y)], Tangent)*(1.0-JointWeights.x);\n"
			"   gl_Position = Mvpm * vec4(t_pos,1.0 );\n"
			"   v_world_position = (Modelm * vec4(t_pos, 1.0)).xyz;\n"
			"   v_world_binormal = (Modelm * vec4(t_binormal, 0.0)).xyz;\n"
			"   v_world_tangent = (Modelm * vec4(t_tangent, 0.0)).xyz;\n"
			"   v_world_normal = cross(v_world_tangent, v_world_binormal);\n"
			//"   v_world_normal = (Modelm * vec4(Normal, 0.0)).xyz;\n"
			"	oTexCoord0 =vec2( Texm * vec4(TexCoord,1,1) );\n"
			"}\n";

#define util_string "vec4 EncodeFloatRGBA(float v)\
	        {\
				vec4 enc = vec4(1.0, 255.0, 65025.0, 16581375.0) * v;\
				enc = frac(enc);\
				enc -= enc.yzww * vec4(1.0 / 255.0, 1.0 / 255.0, 1.0 / 255.0, 0);\
				return enc;\
			}"\
					"float DecodeFloatRGBA(vec4 rgba)\
			{\
				return dot(rgba, vec4(1.0, 1.0 / 255.0, 1.0 / 65025.0, 1.0 / 16581375.0));\
			}"

		const char* pointvs = "uniform " HIGHP " mat4 Mvpm;\n"
			"uniform " HIGHP " mat4 Modelm;\n"
			"attribute " HIGHP " vec4 Position;\n"
			"attribute " HIGHP " vec3 Normal;\n"
			"attribute " HIGHP " vec2 TexCoord;\n"
			"varying " HIGHP " vec2 oTexCoord0;\n"
			"varying " HIGHP " vec3 oNormal;\n"
			"varying  " HIGHP " vec3 v_world_position;\n"
			"void main()\n"
			"{\n"
			"   gl_Position = Mvpm * Position;\n"
			"   v_world_position = (Modelm *  vec4(Position.xyz, 1.0)).xyz;\n"
			//"   gl_Position = Mvpm * vec4(v_world_position.xyz, 1.0);\n"
			"   oTexCoord0 = TexCoord;\n"
			"	oNormal = (Modelm*vec4(Normal,0.0)).xyz; "
			"}\n";

		const char* pointps =
			"uniform " HIGHP " vec3 EyePosition;\n"
			"uniform " HIGHP " float PointLightRange;\n"
			"varying  " HIGHP " vec3 v_world_position;\n"
			util_string
			"void main()\n"
			"{\n"
			"float t_len = length(v_world_position - EyePosition);"
			"   gl_FragColor = EncodeFloatRGBA( clamp(t_len /PointLightRange,0.0,0.95));\n"
			//"   gl_FragColor =vec4(1.0,0.0,0.0,1.0);\n"
			"}\n";


		const char* gc_skinned_ani_vs = "precision highp float;"
			"attribute " HIGHP " vec4 Position;\n"
			"attribute " HIGHP " vec3 Tangent;\n"
			"attribute " HIGHP " vec3 Binormal;\n"
			"attribute " HIGHP " vec2 TexCoord;\n"
			"attribute " HIGHP " vec4 JointWeights;\n"
			"attribute " HIGHP " vec4 JointIndices;\n"
			"uniform " HIGHP " mat4 Mvpm;\n"
			"uniform " HIGHP " mat4 Texm;\n"
			"uniform " HIGHP " mat4 Modelm;\n"
			"uniform " HIGHP " mat4 Joints[48];\n"
			"varying " MEDIUMP " vec2 oTexCoord0;\n"
			"varying  " HIGHP " vec3 v_world_position;\n"
			"varying  " HIGHP " vec3 v_world_tangent;\n"
			"varying  " HIGHP " vec3 v_world_binormal;\n"
			"varying  " HIGHP " vec3 v_world_normal;\n"
			"vec3 multiply( mat4 m, vec3 v )\n"
			"{\n"
			"   return vec3(\n"
			"      m[0].x * v.x + m[1].x * v.y + m[2].x * v.z,\n"
			"      m[0].y * v.x + m[1].y * v.y + m[2].y * v.z,\n"
			"      m[0].z * v.x + m[1].z * v.y + m[2].z * v.z );\n"
			"}\n"
			"void main()\n"
			"{\n"
			" " HIGHP " vec3 t_pos = ((Joints[int(JointIndices.x)] * vec4(Position.xyz,1.0))*JointWeights.x + (Joints[int(JointIndices.y)]* vec4(Position.xyz,1.0))*(1.0-JointWeights.x)).xyz;\n"
			" " HIGHP " vec3 t_binormal = multiply( Joints[int(JointIndices.x)], Binormal)*JointWeights.x + multiply(Joints[int(JointIndices.y)], Binormal)*(1.0-JointWeights.x) ;\n"
			" " HIGHP " vec3 t_tangent = multiply( Joints[int(JointIndices.x)], Tangent)*JointWeights.x + multiply(Joints[int(JointIndices.y)], Tangent)*(1.0-JointWeights.x);\n"
			"   gl_Position = Mvpm * vec4(t_pos,1.0 );\n"
			"   v_world_position = (Modelm * vec4(t_pos, 1.0)).xyz;\n"
			"   v_world_binormal = (Modelm * vec4(t_binormal, 0.0)).xyz;\n"
			"   v_world_tangent = (Modelm * vec4(t_tangent, 0.0)).xyz;\n"
			"   v_world_normal = cross(v_world_tangent, v_world_binormal);\n"
			//"   oTexCoord0 = vec2(TexCoord.x,1.0-TexCoord.y);\n"
			"	oTexCoord0 =vec2( Texm * vec4(TexCoord,1,1) );\n"
			"}\n";


		const char* directvs = "uniform " HIGHP " mat4 Mvpm;\n"
			"uniform " HIGHP " mat4 Modelm;\n"
			"attribute " HIGHP " vec4 Position;\n"
			"attribute " HIGHP " vec2 TexCoord;\n"
			"attribute " HIGHP " vec3 Normal;\n"
			"varying  " HIGHP " vec2 oTexCoord;\n"
			"void main()\n"
			"{\n"
			"	vec4 world_pos = Modelm * vec4(Position.xyz, 1.0);"
			"   gl_Position = Mvpm * vec4(Position.xyz, 1.0);\n"
			"	oTexCoord = TexCoord; \n"
			"}\n";

		const char* directps =
			"varying  " HIGHP " vec2 oTexCoord;\n"
			"void main()\n"
			"{\n"
			"   gl_FragColor =  vec4(0);\n"
			"}\n";

		const char* psTest_transparent = "#version 400 core\n"
#ifdef LVR_OS_WIN32
			"#extension GL_NV_shadow_samplers_cube:enable\n"
			"#extension GL_EXT_texture_array:enable\n"
			"#extension GL_ARB_texture_cube_map_array:enable\n"
			"#extension GL_EXT_gpu_shader4_1:enable\n"
#endif
			"uniform  sampler2D Texture0;\n" //Diffuse map
			"uniform  sampler2D Texture1;\n"  //emissive
			"uniform  sampler2D Texture2;\n" //Normal map  
			"uniform  sampler2D Texture3;\n"   //Specular shiness map
											   //"uniform  sampler2D Texture4;\n" // Directional light shadow map
			"uniform  sampler2DArray Texture5;\n" //spot light shadow map 0
			"uniform  sampler2D Texture6;\n" // Directional light shadow map
			"uniform  samplerCubeArray Texture7;\n" //point light shadow map
			"uniform " HIGHP " vec4 uOffsetRes;\n"//use it when needed
			"uniform" LOWP " vec3 uFogColor;\n"
			"uniform" HIGHP " vec2 uFogParm;\n"//near distance,far distance minus near
			"uniform " HIGHP " vec3 EyePosition;\n"
			"uniform " HIGHP " vec4 DirectionLightColorIntensity;\n"
			"uniform " HIGHP " vec3 DirectionLightDir;\n"
			"uniform " HIGHP " mat4 DirectionLightVPMat;\n"
			"uniform " HIGHP " vec4 EnvLightColorIntensity;\n"
			"uniform " HIGHP " vec4 PointLightColorIntensity[32];\n"
			"uniform " HIGHP " vec4 PointLightPosRadius[32];\n"
			"uniform " HIGHP " vec4 SpotLightColorIntensity[32];\n"
			"uniform " HIGHP " vec4 SpotLightPosExponent[32];\n"
			"uniform " HIGHP " vec4 SpotLightDirCutOff[32];\n"
			"uniform " HIGHP " mat4 SpotLightVPMat[32];\n"
			"uniform " HIGHP " float uShininess;\n"
			"uniform " HIGHP " ivec4 light_num;\n"//directional light num,spotlight num,point light num
			"uniform " HIGHP " float Transparent;\n"
			"varying  " HIGHP " vec3 v_world_position;\n"
			//"varying  " HIGHP " vec3 v_world_tangent;\n"
			//"varying  " HIGHP " vec3 v_world_binormal;\n"
			"varying  " HIGHP " vec3 v_world_normal;\n"
			"varying " MEDIUMP " vec2 oTexCoord0;\n"
			//"varying  " HIGHP " vec4 oColor;\n"
			light_shading_codes_1122
			"float normal_shadow_2darray(sampler2DArray shadow_map,vec4 a_coord,int layerid){"\
			"vec3 uvz = a_coord.xyz/a_coord.w;"\
			"vec3 final_cd = (uvz+1.0)*0.5;"\
			"const float elps = 0.0001;"\
			"return texture2DArray(shadow_map, vec3(final_cd.xy,layerid)).r+elps < final_cd.z?0.0:1.0;}"\
			"float normal_shadow_cube_array(samplerCubeArray shadow_map,vec4 a_dir_len,float light_range,int layerid){"\
			"float real_z = a_dir_len.w / light_range;"\
			"float ref_z = DecodeFloatRGBA(texture(shadow_map, vec4(a_dir_len.xyz,layerid)));"\
			"return real_z <= ref_z + 0.001 ? 1.0:0.0;}"
			"void main()\n"
			"{\n"
			"   " MEDIUMP " vec4 kd = texture2D( Texture0, oTexCoord0 );\n"
			"   " HIGHP " vec3 normal = texture2D( Texture2, oTexCoord0 ).xyz * 2.0 - 1.0;\n"
			//"   " HIGHP " vec3 n = normal.x * v_world_tangent + normal.y * v_world_binormal + normal.z * v_world_normal;\n"
			"  vec3  n = v_world_normal;"
			"   " HIGHP " vec3 v = normalize( EyePosition - v_world_position);\n"
			"   " MEDIUMP " vec3 ks = kd.xyz;\n"
			"   " MEDIUMP " vec2 shininess2 = vec2(1.0,texture2D( Texture3, oTexCoord0 ).x);\n"
			"   " MEDIUMP " vec3 ka = kd.xyz*EnvLightColorIntensity.xyz*EnvLightColorIntensity.w;\n"//to do ... ka should cal alone
			"vec3 light_effect_color = EnvLightColorIntensity.xyz*kd.xyz*EnvLightColorIntensity.w;\n"

			//directional light
			//"for (int i = 0; i < light_num.x; i++){"
			"if (light_num.x > 0){"
			"   " HIGHP "vec4 shadow_coord = DirectionLightVPMat * vec4(v_world_position,1.0);"
			"   " MEDIUMP "float shadow = normal_shadow_2d(Texture6, shadow_coord);"
			"   light_effect_color += light_shading(n,-DirectionLightDir,v,kd.xyz,ks.xyz,ka,shininess2,DirectionLightColorIntensity.xyz,DirectionLightColorIntensity.w)*shadow;\n"
			//"    light_effect_color = vec3(shadow,shadow,shadow);\n"
			"}"

			//spotlight
			"for (int j = 0; j < light_num.y; j++){"
			"   " HIGHP "vec3 dist = SpotLightPosExponent[j].xyz - v_world_position;"
			"   " HIGHP "vec3 pt_s = normalize(dist);"
			"   " HIGHP "vec4 shadow_coord = SpotLightVPMat[j] * vec4(v_world_position,1.0);"
			"   " MEDIUMP "float shadow = normal_shadow_2darray(Texture5, shadow_coord,j);"
			"   " MEDIUMP " vec3 t_color = spot_light_shading(n,pt_s,v,SpotLightDirCutOff[j].xyz,SpotLightPosExponent[j].w,\
			  SpotLightDirCutOff[j].w,kd.xyz,ks.xyz,ka,shininess2,SpotLightColorIntensity[j].xyz,SpotLightColorIntensity[j].w);\n"
			"light_effect_color += t_color*shadow; }"
			//point light 
			"for (int k = 0; k < light_num.z; k++){"
			"   " HIGHP "vec3 dist = PointLightPosRadius[k].xyz - v_world_position;"
			"   " HIGHP " float real_intensity = PointLightColorIntensity[k].w* cos(clamp(length(dist)/PointLightPosRadius[k].w,0.0,1.0)*1.57);"
			"   " HIGHP "float dist_len = length(dist);"
			"   " HIGHP "vec3 pt_s = normalize(dist);"
			"   " MEDIUMP "float shadow = normal_shadow_cube_array(Texture7, vec4(pt_s*(-1),dist_len),PointLightPosRadius[k].w,k);"
			"   " MEDIUMP " vec3 t_color = light_shading(n,pt_s,v,kd.xyz,ks.xyz,ka,shininess2,PointLightColorIntensity[k].xyz,real_intensity);\n"
			"light_effect_color += t_color * shadow; }\n"

			"   gl_FragColor = vec4(FogShading(v_world_position-EyePosition,light_effect_color,uFogColor,uFogParm),kd.a);\n"
			"   gl_FragColor = vec4(light_effect_color, kd.w * Transparent);\n"
			//"   gl_FragColor = vec4(kd.xyz,1.0);\n"

			"}\n";

	const char* g_SingleTextureVertexShaderSrcMovTexcoord = 
		"uniform " HIGHP " mat4 Mvpm;\n"
		"uniform " HIGHP " mat4 Texm;\n"
		"attribute " HIGHP " vec4 Position;\n"
		"attribute " HIGHP " vec2 TexCoord;\n"
		"varying " HIGHP " vec2 oTexCoord0;\n"
		"void main()\n"
		"{\n"
		"   gl_Position = Mvpm * vec4(Position.xyz,1.0);\n"
		"	oTexCoord0 =vec2( Texm * vec4(TexCoord,1,1) );\n"
		"}\n";
	const char* g_SingleTextureFragmentShaderSrcWithColor =
		"uniform sampler2D Texture0;\n"
		"varying " HIGHP " vec2 oTexCoord0;\n"
		"void main()\n"
		"{\n"
		//"   gl_FragColor = texture2D( Texture0, oTexCoord0 );\n"
		//"	gl_FragColor = vec4(1.0,oTexCoord0,1.0);\n"
		" " HIGHP "   vec4 c = texture2D( Texture0, oTexCoord0 );\n"
		"	gl_FragColor = vec4(c.rgb,c.a);\n"
		//"	gl_FragColor = vec4(0.0, 0.0, 1.0, 1.0);\n"
		"}\n";

	const char* ps_common = 
		"uniform sampler2D Texture0;\n"
		"varying  vec2 oTexCoord0;\n"
		"void main()\n"
		"{\n"
		"   vec4 c = texture2D( Texture0, oTexCoord0 );\n"
		"	gl_FragColor = vec4(c.rgb,1.0);\n"
		"}\n";

	const char* g_SingleTextureFragmentShaderSrcExternalOES =
#ifdef LVR_OS_ANDROID
		"#extension GL_OES_EGL_image_external : require\n"
		"uniform samplerExternalOES Texture0;\n"
#else
		"uniform sampler2D Texture0;\n"
#endif
		"varying " HIGHP " vec2 oTexCoord0;\n"
		"void main()\n"
		"{\n"
		"   gl_FragColor = texture2D( Texture0, oTexCoord0 );\n"
		//"	gl_FragColor = vec4(1.0,oTexCoord0,1.0);\n"
		"}\n";

	const char* ovr_singleTex_with_transparent_vs = 
		"uniform " HIGHP " mat4 Mvpm;\n"
		"uniform " HIGHP " mat4 Texm;\n"
		"attribute " HIGHP " vec4 Position;\n"
		"attribute " HIGHP " vec2 TexCoord;\n"
		"varying " HIGHP " vec2 oTexCoord0;\n"
		"void main()\n"
		"{\n"
		"   gl_Position = Mvpm * vec4(Position.xyz,1.0);\n"
		"	oTexCoord0 =vec2( Texm * vec4(TexCoord,1,1) );\n"
		"}\n";

	const char* ovr_singleTex_with_transparent_ps = 
		"uniform sampler2D Texture0;\n"
		"uniform " HIGHP " float Transparent;\n"
		"uniform " HIGHP " float UniformColor;\n"
		"varying " HIGHP " vec2 oTexCoord0;\n"
		"void main()\n"
		"{\n"
		//"   gl_FragColor = texture2D( Texture0, oTexCoord0 );\n"
		//"	gl_FragColor = vec4(1.0,oTexCoord0,1.0);\n"
		" " HIGHP "   vec4 c = texture2D( Texture0, oTexCoord0 );\n"
		"	gl_FragColor = vec4(c.rgb*UniformColor,c.a*Transparent);\n"
		//"	gl_FragColor = vec4(1.0,0.0,0.0,1.0);\n"
		"}\n";

	const char* ovr_particle_vs =
		"attribute vec3 Position;\n"
		"attribute vec2 TexCoord;\n"
		"uniform " HIGHP " mat4 Mvpm;\n"
		"varying  " HIGHP " vec3 v_world_position;\n"
		"varying  " HIGHP " vec2 oTexCoord0;\n"
		"void main()\n"
		"{\n"
		"   v_world_position = Position;\n"
		"   oTexCoord0 = TexCoord;\n"
		"   gl_Position = Mvpm* vec4(v_world_position, 1.0);\n"
		"} ";


		const char* ovr_particle_ps = "#version 400 core\n"
#ifdef LVR_OS_WIN32
			"#extension GL_NV_shadow_samplers_cube:enable\n"
			"#extension GL_EXT_texture_array:enable\n"
			"#extension GL_ARB_texture_cube_map_array:enable\n"
			"#extension GL_EXT_gpu_shader4_1:enable\n"
#endif
			"uniform  sampler2D Texture0;\n" //Diffuse map
			"uniform  sampler2D Texture3;\n"   //Specular shiness map
											   //"uniform  sampler2D Texture4;\n" // Directional light shadow map
			"uniform  sampler2DArray Texture5;\n" //spot light shadow map 0
			"uniform  sampler2D Texture6;\n" // Directional light shadow map
			"uniform  samplerCubeArray Texture7;\n" //point light shadow map
			"uniform " HIGHP " vec4 uOffsetRes;\n"//use it when needed
			"uniform" LOWP " vec3 uFogColor;\n"
			"uniform" HIGHP " vec2 uFogParm;\n"//near distance,far distance minus near
			"uniform " HIGHP " vec3 EyePosition;\n"
			"uniform " HIGHP " vec4 DirectionLightColorIntensity;\n"
			"uniform " HIGHP " vec3 DirectionLightDir;\n"
			"uniform " HIGHP " mat4 DirectionLightVPMat;\n"
			"uniform " HIGHP " vec4 EnvLightColorIntensity;\n"
			"uniform " HIGHP " vec4 PointLightColorIntensity[" MAX_POINTLIGHT_NUM "];\n"
			"uniform " HIGHP " vec4 PointLightPosRadius[" MAX_POINTLIGHT_NUM "];\n"
			"uniform " HIGHP " vec4 SpotLightColorIntensity[" MAX_SPOTLIGHT_NUM "];\n"
			"uniform " HIGHP " vec4 SpotLightPosExponent[" MAX_SPOTLIGHT_NUM "];\n"
			"uniform " HIGHP " vec4 SpotLightDirCutOff[" MAX_SPOTLIGHT_NUM "];\n"
			"uniform " HIGHP " mat4 SpotLightVPMat[" MAX_SPOTLIGHT_NUM "];\n"
			"uniform " HIGHP " float uShininess;\n"
			"uniform " HIGHP " float Transparent;\n"
			"uniform " HIGHP " ivec4 light_num;\n"//directional light num,spotlight num,point light num
			"varying  " HIGHP " vec3 v_world_position;\n"
			"varying  " HIGHP " vec3 v_world_tangent;\n"
			"varying  " HIGHP " vec3 v_world_binormal;\n"
			"varying  " HIGHP " vec3 v_world_normal;\n"
			"varying " MEDIUMP " vec2 oTexCoord0;\n"
			"varying  " LOWP " vec4 oColor;\n"
			light_shading_codes_1122
			"float normal_shadow_2darray(sampler2DArray shadow_map,vec4 a_coord,int layerid){"\
			"vec3 uvz = a_coord.xyz/a_coord.w;"\
			"vec3 final_cd = (uvz+1.0)*0.5;"\
			"const float elps = 0.0001;"\
			"return texture2DArray(shadow_map, vec3(final_cd.xy,layerid)).r+elps < final_cd.z?0.0:1.0;}"\
			"float normal_shadow_cube_array(samplerCubeArray shadow_map,vec4 a_dir_len,float light_range,int layerid){"\
			"float real_z = a_dir_len.w / light_range;"\
			"float ref_z = DecodeFloatRGBA(texture(shadow_map, vec4(a_dir_len.xyz,layerid)));"\
			"return real_z <= ref_z + 0.001 ? 1.0:0.0;}"
			"void main()\n"
			"{\n"
			"   " MEDIUMP " vec4 kd = texture2D( Texture0, oTexCoord0 )*oColor;\n"
			"   " HIGHP " vec3 n = v_world_normal;"
			"   " HIGHP " vec3 v = normalize( EyePosition - v_world_position);\n"
			"   " MEDIUMP " vec3 ks = kd.xyz;\n"
			"   " MEDIUMP " vec2 shininess2 = vec2(uShininess,texture2D( Texture3, oTexCoord0 ).x);\n"
			"   " MEDIUMP " vec3 ka = kd.xyz*EnvLightColorIntensity.xyz*EnvLightColorIntensity.w;\n"//to do ... ka should cal alone
			"vec3 light_effect_color = EnvLightColorIntensity.xyz*kd.xyz*EnvLightColorIntensity.w;\n"

			//directional light
			//"for (int i = 0; i < light_num.x; i++){"
			"if (light_num.x > 0){"
			"   " HIGHP "vec4 shadow_coord = DirectionLightVPMat * vec4(v_world_position,1.0);"
			"   " MEDIUMP "float shadow = normal_shadow_2d(Texture6, shadow_coord);"
			"   light_effect_color += light_shading(n,-DirectionLightDir,v,kd.xyz,ks.xyz,ka,shininess2,DirectionLightColorIntensity.xyz,DirectionLightColorIntensity.w);\n"
			"}"

			//spotlight
			"for (int j = 0; j < light_num.y; j++){"
			"   " HIGHP "vec3 dist = SpotLightPosExponent[j].xyz - v_world_position;"
			"   " HIGHP "vec3 pt_s = normalize(dist);"
			"   " HIGHP "vec4 shadow_coord = SpotLightVPMat[j] * vec4(v_world_position,1.0);"
			"   " MEDIUMP "float shadow = normal_shadow_2darray(Texture5, shadow_coord,j);"
			"   " MEDIUMP " vec3 t_color = spot_light_shading(n,pt_s,v,SpotLightDirCutOff[j].xyz,SpotLightPosExponent[j].w,\
			  SpotLightDirCutOff[j].w,kd.xyz,ks.xyz,ka,shininess2,SpotLightColorIntensity[j].xyz,SpotLightColorIntensity[j].w);\n"
			"light_effect_color += t_color; }"
			//point light 
			"for (int k = 0; k < light_num.z; k++){"
			"   " HIGHP "vec3 dist = PointLightPosRadius[k].xyz - v_world_position;"
			"   " HIGHP " float real_intensity = PointLightColorIntensity[k].w* cos(clamp(length(dist)/PointLightPosRadius[k].w,0.0,1.0)*1.57);"
			"   " HIGHP "float dist_len = length(dist);"
			"   " HIGHP "vec3 pt_s = normalize(dist);"
			"   " MEDIUMP "float shadow = normal_shadow_cube_array(Texture7, vec4(-pt_s,dist_len),PointLightPosRadius[k].w,k);"
			"   " MEDIUMP " vec3 t_color = light_shading(n,pt_s,v,kd.xyz,ks.xyz,ka,shininess2,PointLightColorIntensity[k].xyz,real_intensity);\n"
			"light_effect_color += t_color; }\n"

			"   gl_FragColor = vec4(FogShading(v_world_position-EyePosition,light_effect_color,uFogColor,uFogParm),kd.a);\n"
			"   gl_FragColor = vec4(light_effect_color,Transparent*kd.w);\n"
		"}\n";

		const char* ovr_skybox_vs = "#version 400 core\n"
			"attribute " HIGHP " vec3 Position;\n"
			"uniform " HIGHP " mat4 Mvpm;\n"
			"varying  " HIGHP " vec3 oTexCoord0;\n"
			"void main()\n"
			"{\n"
				"vec4 WVP_Pos = Mvpm * vec4(Position, 1.0);\n"
				"gl_Position = WVP_Pos.xyww;\n"
				"oTexCoord0 = Position;\n"
				//"oTexCoord0 = vec3(1.0, 0.0, 1.0);\n"
			"}\n";

		const char* ovr_skybox_ps = "#version 400 core\n"
			"varying  " HIGHP " vec3 oTexCoord0;\n"
			"uniform samplerCube Texture0;\n"
			"void main()\n"
			"{\n"
				"gl_FragColor = texture(Texture0, oTexCoord0);"
			//"gl_FragColor = vec4(1.0f, 0.0f, 1.0f, 1.0f);"
			"}\n";

		const char* vs_standard_with_light = "precision highp float;"
			"attribute " HIGHP " vec4 Position;\n"
			"attribute " HIGHP " vec3 Normal;\n"
			"attribute " HIGHP " vec3 Tangent;\n"
			"attribute " HIGHP " vec3 Binormal;\n"
			"attribute " HIGHP " vec2 TexCoord;\n"
			"uniform " HIGHP " vec4 uOffsetRes;\n"//use it when needed
			"uniform " HIGHP " mat4 Mvpm;\n"
			"uniform " HIGHP " mat4 Modelm;\n"
			"varying " MEDIUMP " vec2 oTexCoord0;\n"
			"varying  " HIGHP " vec3 v_world_position;\n"
			//"varying  " HIGHP " vec3 v_world_tangent;\n"
			//"varying  " HIGHP " vec3 v_world_binormal;\n"
			"uniform " HIGHP " mat4 Texm;\n"
			"uniform " HIGHP " mat4 Texm2;\n"
			"varying  " HIGHP " vec3 v_world_normal;\n"
			"void main()\n"
			"{\n"
			" " HIGHP " vec3 t_pos =Position.xyz;"
			"   gl_Position = Mvpm * vec4(t_pos,1.0 );\n"
			"   v_world_position = (Modelm * vec4(t_pos, 1.0)).xyz;\n"
			//"   v_world_binormal = (Modelm * vec4(Binormal, 0.0)).xyz;\n"
			//"   v_world_tangent = (Modelm * vec4(Tangent, 0.0)).xyz;\n"
			//"   v_world_normal = cross(Tangent, Binormal);\n"
			"   v_world_normal =(Modelm * vec4(Normal, 0.0)).xyz ;\n"
			"	oTexCoord0 =vec2( Texm * vec4(TexCoord,1,1) );\n"
			"}\n";


		const char* ps_standard_with_light = "#version 400 core\n"
#ifdef LVR_OS_WIN32
			"#extension GL_NV_shadow_samplers_cube:enable\n"
			"#extension GL_EXT_texture_array:enable\n"
			"#extension GL_ARB_texture_cube_map_array:enable\n"
			"#extension GL_EXT_gpu_shader4_1:enable\n"
#endif
			"uniform  sampler2D Texture0;\n" //Diffuse map
			"uniform  sampler2D Texture1;\n"  //emissive
			"uniform  sampler2D Texture2;\n" //Normal map  
			"uniform  sampler2D Texture3;\n"   //Specular shiness map
											   //"uniform  sampler2D Texture4;\n" // Directional light shadow map
			"uniform  sampler2DArray Texture5;\n" //spot light shadow map 0
			"uniform  sampler2D Texture6;\n" // Directional light shadow map
			"uniform  samplerCubeArray Texture7;\n" //point light shadow map
			"uniform " HIGHP " vec4 uOffsetRes;\n"//use it when needed
			"uniform" LOWP " vec3 uFogColor;\n"
			"uniform" HIGHP " vec2 uFogParm;\n"//near distance,far distance minus near
			"uniform " HIGHP " vec3 EyePosition;\n"
			"uniform " HIGHP " vec4 DirectionLightColorIntensity;\n"
			"uniform " HIGHP " vec3 DirectionLightDir;\n"
			"uniform " HIGHP " mat4 DirectionLightVPMat;\n"
			"uniform " HIGHP " vec4 EnvLightColorIntensity;\n"
			"uniform " HIGHP " vec4 PointLightColorIntensity[32];\n"
			"uniform " HIGHP " vec4 PointLightPosRadius[32];\n"
			"uniform " HIGHP " vec4 SpotLightColorIntensity[32];\n"
			"uniform " HIGHP " vec4 SpotLightPosExponent[32];\n"
			"uniform " HIGHP " vec4 SpotLightDirCutOff[32];\n"
			"uniform " HIGHP " mat4 SpotLightVPMat[32];\n"
			"uniform " HIGHP " float uShininess;\n"
			"uniform " HIGHP " ivec4 light_num;\n"//directional light num,spotlight num,point light num
			"varying  " HIGHP " vec3 v_world_position;\n"
			//"varying  " HIGHP " vec3 v_world_tangent;\n"
			//"varying  " HIGHP " vec3 v_world_binormal;\n"
			"varying  " HIGHP " vec3 v_world_normal;\n"
			"varying " MEDIUMP " vec2 oTexCoord0;\n"
			"varying  " HIGHP " vec4 v_temp;\n"

			//"varying  " HIGHP " vec4 oColor;\n"
			light_shading_codes_1122
			"float normal_shadow_2darray(sampler2DArray shadow_map,vec4 a_coord,int layerid){"\
			"vec3 uvz = a_coord.xyz/a_coord.w;"\
			"vec3 final_cd = (uvz+1.0)*0.5;"\
			"const float elps = 0.001;"\
			"return texture2DArray(shadow_map, vec3(final_cd.xy,layerid)).r+elps < final_cd.z?0.0:1.0;}"\
			"float normal_shadow_cube_array(samplerCubeArray shadow_map,vec4 a_dir_len,float light_range,int layerid){"\
			"float real_z = a_dir_len.w / light_range;"\
			"float ref_z = DecodeFloatRGBA(texture(shadow_map, vec4(a_dir_len.xyz,layerid)));"\
			"return real_z <= ref_z + 0.001 ? 1.0:0.0;}"
			"void main()\n"
			"{\n"
			"   " MEDIUMP "vec4 kd = texture2D(Texture0,oTexCoord0);\n"
			"   " HIGHP " vec3 normal = texture2D( Texture2, oTexCoord0 ).xyz * 2.0 - 1.0;\n"
			//"   " HIGHP " vec3 n = normal.x * v_world_tangent + normal.y * v_world_binormal + normal.z * v_world_normal;\n"
			"     vec3  n = v_world_normal;"
			"   " HIGHP " vec3 v = normalize( EyePosition - v_world_position);\n"
			"   " MEDIUMP " vec3 ks = kd.xyz;\n"
			"   " MEDIUMP " vec2 shininess2 = vec2(1.0,texture2D( Texture3, oTexCoord0 ).x);\n"
			"   " MEDIUMP " vec3 ka = kd.xyz*EnvLightColorIntensity.xyz*EnvLightColorIntensity.w;\n"//to do ... ka should cal alone
			"vec3 light_effect_color =  EnvLightColorIntensity.xyz*kd.xyz*EnvLightColorIntensity.w;\n"

			//directional light
			//"for (int i = 0; i < light_num.x; i++){"
			"if (light_num.x > 0){"
			"   " HIGHP "vec4 shadow_coord = DirectionLightVPMat * vec4(v_world_position,1.0);"
			"   " MEDIUMP "float shadow = normal_shadow_2d(Texture6, shadow_coord);"
			"   light_effect_color += light_shading(n,-DirectionLightDir,v,kd.xyz,ks.xyz,ka,shininess2,DirectionLightColorIntensity.xyz,DirectionLightColorIntensity.w)*shadow;\n"
			//"    light_effect_color = vec3(shadow,shadow,shadow);\n"
			"}"

			//spotlight
			"for (int j = 0; j < light_num.y; j++){"
			"   " HIGHP "vec3 dist = SpotLightPosExponent[j].xyz - v_world_position;"
			"   " HIGHP "vec3 pt_s = normalize(dist);"
			"   " HIGHP "vec4 shadow_coord = SpotLightVPMat[j] * vec4(v_world_position,1.0);"
			"   " MEDIUMP "float shadow = normal_shadow_2darray(Texture5, shadow_coord,j);"
			"   " MEDIUMP " vec3 t_color = spot_light_shading(n,pt_s,v,SpotLightDirCutOff[j].xyz,SpotLightPosExponent[j].w,\
			  SpotLightDirCutOff[j].w,kd.xyz,ks.xyz,ka,shininess2,SpotLightColorIntensity[j].xyz,SpotLightColorIntensity[j].w);\n"
			"light_effect_color += t_color*shadow; }"
			//point light 
			"for (int k = 0; k < light_num.z; k++){"
			"   " HIGHP "vec3 dist = PointLightPosRadius[k].xyz - v_world_position;"
			"   " HIGHP " float real_intensity = PointLightColorIntensity[k].w* cos(clamp(length(dist)/PointLightPosRadius[k].w,0.0,1.0)*1.57);"
			"   " HIGHP "float dist_len = length(dist);"
			"   " HIGHP "vec3 pt_s = normalize(dist);"
			"   " MEDIUMP "float shadow = normal_shadow_cube_array(Texture7, vec4(pt_s*(-1),dist_len),PointLightPosRadius[k].w,k);"
			"   " MEDIUMP " vec3 t_color = light_shading(n,pt_s,v,kd.xyz,ks.xyz,ka,shininess2,PointLightColorIntensity[k].xyz,real_intensity);\n"
			"light_effect_color += t_color * shadow; }\n"

			//"   gl_FragColor = vec4(FogShading(v_world_position-EyePosition,light_effect_color,uFogColor,uFogParm),kd.a);\n"
			"   vec4 ke = texture2D(Texture1,oTexCoord0);\n"
			"   gl_FragColor = vec4(light_effect_color + ke.xyz + ka,kd.w);\n"
			//"   gl_FragColor = vec4(1.0);\n"

			"}\n";
			
		const char* vs_glass_str = "";
		const char* ps_galss_str = "";

		const char * vertex_color_vs =
			"uniform " HIGHP " mat4 Mvpm;\n"
			"attribute " HIGHP " vec4 Position;\n"
			"attribute " LOWP " vec4 VertexColor;\n"
			"varying " LOWP " vec4 oColor;\n"
			"void main()\n"
			"{\n"
			"   gl_Position = Mvpm * vec4(Position.xyz,1.0);\n"
			"   oColor = VertexColor;\n"
			"}\n";

		const char * vertex_color_ps =
			"varying " LOWP " vec4 oColor;\n"
			"void main()\n"
			"{\n"
			"   gl_FragColor = oColor;\n"
			"}\n";

		const char * single_texture_vs =
			"uniform " HIGHP " mat4 Mvpm;\n"
			"attribute " HIGHP " vec4 Position;\n"
			"attribute " HIGHP " vec2 TexCoord;\n"
			"varying " HIGHP " vec2 oTexCoord0;\n"
			"void main()\n"
			"{\n"
			"   gl_Position = Mvpm * vec4(Position.xyz,1.0);\n"
			"   oTexCoord0 = TexCoord;\n"
			"}\n";


		const char * single_texture_ps =
			"uniform sampler2D Texture0;\n"
			"uniform " HIGHP " float UniformColor;\n"
			"varying " HIGHP " vec2 oTexCoord0;\n"
			"void main()\n"
			"{\n"
			//"   gl_FragColor = texture2D( Texture0, oTexCoord0 );\n"
			//"	gl_FragColor = vec4(1.0,oTexCoord0,1.0);\n"
			" " HIGHP "   vec4 c = texture2D( Texture0, oTexCoord0 );\n"
			"	gl_FragColor = vec4(c.rgb*UniformColor,c.a);\n"
			"}\n";

		const char * simple_vs =
			"uniform " HIGHP " mat4 Mvpm;\n"
			"attribute " HIGHP " vec4 Position;\n"
			"void main()\n"
			"{\n"
			"   gl_Position = Mvpm * Position;\n"
			"}\n";
		const char * simple_ps =
			"uniform " LOWP " vec4 UniformColor;\n"//use it as alpha only.
			"void main()\n"
			"{\n"
			"   gl_FragColor = UniformColor;"
			"}\n";


		const char * font_vs =
			"uniform " HIGHP "mat4 Mvpm;\n"
			"attribute vec4 Position;\n"
			"attribute vec2 TexCoord;\n"
			"attribute vec4 VertexColor;\n"
			"varying  " HIGHP " vec2 oTexCoord0;\n"
			"varying  " LOWP " vec4 oColor;\n"
			"void main()\n"
			"{\n"
			"   gl_Position = Mvpm*Position;\n"
			"   oTexCoord0 = TexCoord;\n"	// need to flip Y
			"	oColor = VertexColor;\n"
			"}\n";

		const char * font_ps = 
			"uniform sampler2D Texture0;\n"
			"varying " HIGHP " vec2 oTexCoord0;\n"
			"varying " LOWP " vec4 oColor;\n"
			"void main()\n"
			"{\n"
			"	gl_FragColor = oColor;\n"
			"	" MEDIUMP "float distance = texture2D(Texture0, oTexCoord0).a;\n"
			//"	gl_FragColor.a *= distance * 28.013672 - 17.465820;\n"
			"	gl_FragColor.a *= (distance * 26.1416016 - 18.5986328);\n"
			//"	gl_FragColor.a = 1.0;\n"
			"}\n";

		const char * axis_world_vs =
			"uniform " HIGHP " mat4 Modelm;\n"
			"uniform " HIGHP " mat4 Viewm;\n"
			"uniform " HIGHP " mat4 Projectionm;\n"
			"attribute " HIGHP " vec4 Position;\n"
			"void main()\n"
			"{\n"
			"	mat4 mm = mat4(1.0f);\n"
			"   mm[3][0] = -0.01f;\n"
			"   mm[3][1] = -0.0065f;\n"
			"   mm[3][2] = -0.01f;\n"
			"   mm[3][3] = 1.0f;\n"

			"	mat4 vm = Viewm;\n"
			"	vm[3][0] = 0.0f;\n"
			"	vm[3][1] = 0.0f;\n"
			"	vm[3][2] = 0.0f;\n"
			"	vm[3][3] = 1.0f;\n"

			"   mat4 mvp = Projectionm * mm * vm * Modelm;\n"
			"   gl_Position = mvp * Position;\n"
			"}\n";
		const char * axis_world_ps =
			"uniform " LOWP " vec4 UniformColor;\n"//use it as alpha only.
			"void main()\n"
			"{\n"
			"   gl_FragColor = UniformColor;"
			"}\n";
}