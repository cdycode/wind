﻿#include <assert.h>
#include <list>
#include "wx_engine/ovr_gpu_program.h"
#include "wx_engine/ovr_logger.h"
#include "wx_engine/ovr_serializer_impl.h"
#include "wx_engine/ovr_engine.h"
#include "ovr_shader_string.h"
#include "ovr_gl_gpu_program.h"
#include "ovr_gl_define.h"
#include "ovr_gl_texture.h"
#include "ovr_gl_shader.h"
#include "wx_engine/ovr_vertex_index_data.h"
using namespace ovr_engine;

ovr_engine::OvrGLGpuProgram::OvrGLGpuProgram(OvrString a_name)
	: OvrGpuProgram(a_name)
	, program_id(0)
	, vs(nullptr)
	, ps(nullptr)
{
	for (ovr::uint32 i = 0; i < 8; i++)
	{
		texture_types[i] = GL_TEXTURE_2D;
	}
}

ovr_engine::OvrGLGpuProgram::~OvrGLGpuProgram()
{
	Reset();
}


OvrString ovr_engine::OvrGLGpuProgram::GetName()
{
	return name;
}

void ovr_engine::OvrGLGpuProgram::SetShaders(OvrGLShader* a_vs, OvrGLShader* a_fs)
{
	vs = a_vs;
	ps = a_fs;
	CompileAndLink();
}

void ovr_engine::OvrGLGpuProgram::Reset()
{

	if (vs)
	{
		if (program_id > 0)
		{
			glDetachShader(program_id, vs->GetShaderID());
		}
		delete vs;
		vs = nullptr;
	}
	if (ps)
	{
		if (program_id > 0)
		{
			glDetachShader(program_id, ps->GetShaderID());
		}
		delete ps;
		ps = nullptr;
	}
	
	if (program_id > 0)
	{
		glDeleteProgram(program_id);
		program_id = 0;
	}
}

void ovr_engine::OvrGLGpuProgram::BindProgram()
{
	glUseProgram(program_id);
}

void ovr_engine::OvrGLGpuProgram::UnBindProgram()
{
	glUseProgram(0);
}

void ovr_engine::OvrGLGpuProgram::BindProgramParameters(OvrGpuProgramParameters* params, ovr::uint16 mask)
{
	for (ovr::uint32 i = 0; i < uniform_set.size(); i++)
	{
		const OvrConstant* constant = uniform_set[i].constant;
		ovr::uint32 loc = uniform_set[i].location;
		int	tex_loc = uniform_set[i].extra;
		if (constant->variability & mask)
		{
			switch (constant->const_type)
			{
			case GCT_FLOAT1:
			{
				const float* float_ptr = params->GetFloatPointer(constant->physicalIndex);
				glUniform1fv(loc, constant->array_size, float_ptr);
			}
			break;
			case GCT_INT1:
			{
				const int* int_ptr = params->GetIntPointer(constant->physicalIndex);
				glUniform1iv(loc, constant->array_size, int_ptr);
			}
			break;
			case GCT_VEC3:
			{
				const float* float_ptr = params->GetFloatPointer(constant->physicalIndex);
				glUniform3fv(loc, constant->array_size, float_ptr);
			}
			break;
			case GCT_VEC4:
			{
				const float* float_ptr = params->GetFloatPointer(constant->physicalIndex);
				glUniform4fv(loc, constant->array_size, float_ptr);
			}
			break;
			case GCT_MAT3:
			{
				const float* float_ptr = params->GetFloatPointer(constant->physicalIndex);
				glUniformMatrix3fv(loc, constant->array_size, false, float_ptr);
			}
			break;
			case GCT_MAT4:
			{
				const float* float_ptr = params->GetFloatPointer(constant->physicalIndex);
				glUniformMatrix4fv(loc, constant->array_size, false, float_ptr);
			}
			break;
			case GCT_INT4:
			{
				const int* int_ptr = params->GetIntPointer(constant->physicalIndex);
				glUniform4iv(loc, constant->array_size, int_ptr);
			}
			break;
			case GCT_SAMPLER2D:
			case GCT_SAMPLER_BUFFER:
			case GCT_SAMPLER2D_ARRAY:
			case GCT_SAMPLER_CUBE:
			case GCT_SAMPLER_CUBE_ARRAY:
			{
				OvrTextureUnit* unit = params->GetTextureUnit(constant->physicalIndex);
				SetTextureUnit(tex_loc, unit);
			}
			break;
			default:
				break;
			}
		}
	}
}

void ovr_engine::OvrGLGpuProgram::SetTextureUnit(ovr::uint32 a_slot, OvrTextureUnit* a_tex_unit)
{
	OvrGLTexture* gl_texture = (OvrGLTexture*)(a_tex_unit->GetTexture());
	OvrUVWAddressingMode mode = a_tex_unit->GetAddressingMode();
	OvrTextureFilter filter = a_tex_unit->GetFilter();
	glActiveTexture(GL_TEXTURE0 + a_slot);
	if (gl_texture)
	{
		glBindTexture(gl_texture->GetGLTextureTarget(), gl_texture->GetTextureId());
		texture_types[a_slot] = gl_texture->GetGLTextureTarget();
		if (a_tex_unit->IsDirty())
		{
			glTexParameteri(gl_texture->GetGLTextureTarget(), GL_TEXTURE_WRAP_S, GetGLTextureAddressMode(mode.u));
			glTexParameteri(gl_texture->GetGLTextureTarget(), GL_TEXTURE_WRAP_T, GetGLTextureAddressMode(mode.v));
			glTexParameteri(gl_texture->GetGLTextureTarget(), GL_TEXTURE_WRAP_R, GetGLTextureAddressMode(mode.w));
			glTexParameteri(gl_texture->GetGLTextureTarget(), GL_TEXTURE_MIN_FILTER, GetGLTextureFliter(filter));
			glTexParameteri(gl_texture->GetGLTextureTarget(), GL_TEXTURE_MAG_FILTER, GetGLTextureFliter(filter));
			a_tex_unit->SetDirty(false);
		}
	}
	else
	{
		glBindTexture(texture_types[a_slot], 0);
	}
}

void ovr_engine::OvrGLGpuProgram::CompileAndLink()
{
	if (!vs->Compile())
		return;
	if (!ps->Compile())
		return;

	program_id = glCreateProgram();

	if (vs)
	{
		glAttachShader(program_id, vs->GetShaderID());
	}
	if (ps)
	{
		glAttachShader(program_id, ps->GetShaderID());
	}
	
	glBindAttribLocation(program_id, VES_POSITION, "Position");
	glBindAttribLocation(program_id, VES_NORMAL, "Normal");
	glBindAttribLocation(program_id, VES_TANGENT, "Tangent");
	glBindAttribLocation(program_id, VES_BINORMAL, "Binormal");
	glBindAttribLocation(program_id, VES_COLOR, "VertexColor");
	glBindAttribLocation(program_id, VES_UV0, "TexCoord");
	glBindAttribLocation(program_id, VES_UV1, "TexCoord1");
	glBindAttribLocation(program_id, VES_JOINT_WEIGHTS, "JointWeights");
	glBindAttribLocation(program_id, VES_JOINT_INDICES, "JointIndices");

	glLinkProgram(program_id);

	if (vs)
		glDeleteShader(vs->GetShaderID());
	if (ps)
		glDeleteShader(ps->GetShaderID());

	int status = 0;
	glGetProgramiv(program_id, GL_LINK_STATUS, &status);
	if (!status)
	{
		int info_len = 0;
		glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &info_len);
		char* program_log = new char[info_len + 1];
		int log_len = 0;
		glGetProgramInfoLog(program_id, info_len, &log_len, program_log);
		OvrLogger::GetIns()->Log("program link failed.detail information are:\n", LL_ERROR);
		if (log_len > 2)
		{
			OvrLogger::GetIns()->Log(program_log, LL_ERROR);
		}
		else
		{
			OvrLogger::GetIns()->Log("unknown error.\n", LL_ERROR);
		}
		glDeleteProgram(program_id);
		program_id = 0;
		return;
	}
		
	glUseProgram(program_id);
	for (int i = 0; i < 8; i++)
	{
		char name[32];
		sprintf(name, "Texture%i", i);
		const int uTex = glGetUniformLocation(program_id, name);
		if (uTex != -1)
		{
			glUniform1i(uTex, i);
		}
	}
	glUseProgram(0);

	constants = new OvrGpuProgramConstants();
	GLint u_count = 0;
	GLint u_name_maxlen = 0;
	glGetProgramiv(program_id, GL_ACTIVE_UNIFORMS, &u_count);
	glGetProgramiv(program_id, GL_ACTIVE_UNIFORM_MAX_LENGTH, &u_name_maxlen);
	char* u_name = new char[++u_name_maxlen];
	for (ovr::uint32 i = 0; i < (ovr::uint32)u_count; i++)
	{
		memset(u_name, 0, u_name_maxlen);
		GLint u_size;
		GLenum u_type;
		glGetActiveUniform(program_id, i, u_name_maxlen, 0, &u_size, &u_type, reinterpret_cast<GLchar*>(u_name));
		OvrGpuProgramConstantType shader_type = OvrGLGpuProgramUtil::GetProgramConstantType(u_type);
		OvrString shader_name = OvrGLGpuProgramUtil::GetUniformName(u_name);
		int shader_extra = OvrGLGpuProgramUtil::GetTextureLayerUsage(shader_name);
		int shader_location = glGetUniformLocation(program_id, u_name);
		const OvrConstant* shader_constant = constants->AddConstant(shader_name, shader_type, u_size);
		UniformReference u_ref;
		u_ref.location = shader_location;
		u_ref.name = shader_name;
		u_ref.constant = shader_constant;
		u_ref.extra = shader_extra;
		uniform_set.push_back(u_ref);
	}
	delete[] u_name;
}

GLuint ovr_engine::OvrGLGpuProgram::GetGLTextureFliter(const OvrTextureFilter& a_filter)
{
	switch (a_filter)
	{
	case TEXTURE_FILTER_NEAREST:
		return GL_NEAREST;
	case TEXTURE_FILTER_BILINEAR:
		return GL_LINEAR;
	default:
		return GL_LINEAR;
	}
}

GLuint ovr_engine::OvrGLGpuProgram::GetGLTextureAddressMode(const OvrTextureAddressMode& a_mode)
{
	switch (a_mode)
	{
	case TEXTURE_ADDRESSMODE_WARP:
		return GL_REPEAT;
	case TEXTURE_ADDRESSMODE_CLAMP:
		return GL_CLAMP_TO_EDGE;
	case TEXTURE_ADDRESSMODE_MIRROR:
		return GL_MIRRORED_REPEAT;
	case TEXTURE_ADDRESSMODE_BORDER:
		return GL_CLAMP_TO_BORDER;
	default:
		return GL_CLAMP_TO_EDGE;
	}
}

OvrString ovr_engine::OvrGLGpuProgramUtil::GetUniformName(const char* a_name)
{
	OvrString name(a_name);
	std::list<OvrString> name_list;
	name.Split('[', name_list);
	return name_list.front();
}

int ovr_engine::OvrGLGpuProgramUtil::GetTextureLayerUsage(const OvrString& a_tex_name)
{
	if (a_tex_name == "Texture0")
		return 0;
	else if (a_tex_name == "Texture1")
		return 1;
	else if (a_tex_name == "Texture2")
		return 2;
	else if (a_tex_name == "Texture3")
		return 3;
	else if (a_tex_name == "Texture4")
		return 4;
	else if (a_tex_name == "Texture5")
		return 5;
	else if (a_tex_name == "Texture6")
		return 6;
	else if (a_tex_name == "Texture7")
		return 7;
	return -1;
}

ovr_engine::OvrGpuProgramConstantType ovr_engine::OvrGLGpuProgramUtil::GetProgramConstantType(int a_type)
{
	OvrGpuProgramConstantType type = GCT_FLOAT1;
	switch (a_type)
	{
	case GL_FLOAT:
		type = GCT_FLOAT1;
		break;
	case GL_FLOAT_VEC3:
		type = GCT_VEC3;
		break;
	case GL_FLOAT_VEC4:
		type = GCT_VEC4;
		break;
	case GL_FLOAT_MAT3:
		type = GCT_MAT3;
		break;
	case GL_FLOAT_MAT4:
		type = GCT_MAT4;
		break;
	case GL_INT_VEC4:
		type = GCT_INT4;
		break;
	case GL_SAMPLER_2D:
		type = GCT_SAMPLER2D;
		break;
	case GL_SAMPLER_2D_ARRAY:
		type = GCT_SAMPLER2D_ARRAY;
		break;
	case GL_SAMPLER_CUBE:
		type = GCT_SAMPLER_CUBE;
		break;
	case GL_SAMPLER_CUBE_MAP_ARRAY:
		type = GCT_SAMPLER_CUBE_ARRAY;
		break;
	case GL_SAMPLER_BUFFER:
		type = GCT_SAMPLER_BUFFER;
		break;
	case GL_INT:
		type = GCT_INT1;
		break;
	default:
		break;
	}
	return type;
}
