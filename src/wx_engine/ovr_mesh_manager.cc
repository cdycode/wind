#include "wx_engine/ovr_mesh_manager.h"
#include "wx_engine/ovr_serializer_impl.h"
#include "wx_engine/ovr_vertex_index_data.h"
#include "wx_engine/ovr_logger.h"
#include "wx_engine/ovr_hardware_buffer_manager.h"
using namespace ovr_engine;
template<> OvrMeshManager* ovr_engine::OvrSingleton<OvrMeshManager>::s_instance = nullptr;

ovr_engine::OvrMeshManager::~OvrMeshManager()
{
	UnInitialise();
}

bool ovr_engine::OvrMeshManager::Initialise()
{
	return true;
}

void ovr_engine::OvrMeshManager::UnInitialise()
{
	RemoveAll();
}

OvrMesh* ovr_engine::OvrMeshManager::Load(const OvrString& a_file_name)
{
	OvrMesh* mesh = (OvrMesh*)CreateOrRetrieve(a_file_name, false);
	mesh->Load();
	return mesh;
}

OvrMesh* ovr_engine::OvrMeshManager::Create(const OvrString& a_name)
{
	OvrMesh* mesh = (OvrMesh*)CreateOrRetrieve(a_name, true);
	return mesh;
}

OvrMesh* ovr_engine::OvrMeshManager::CreateCube(OvrString a_name, float a_size)
{
	OvrMesh* new_mesh = Create(a_name);
	MeshBuildParams param;
	param.type = kCube;
	param.size = a_size;
	LoadManualCube(new_mesh, param);
	return new_mesh;
}

OvrMesh* ovr_engine::OvrMeshManager::CreatePlane(OvrString a_name, float a_size)
{
	OvrMesh* new_mesh = Create(a_name);
	MeshBuildParams param;
	param.type = kPlane;
	param.size = a_size;
	LoadManualPlane(new_mesh, param);
	return new_mesh;
}

OvrMesh* ovr_engine::OvrMeshManager::CreateCylinder(OvrString a_name, float scale, float r, float h, int n)
{
	OvrMesh* new_mesh = Create(a_name);
	MeshBuildParams param;
	param.type = kCylinder;
	param.radius = r;
	param.height = h;
	param.stacks = n;
	param.scale = scale;
	LoadManualCylinder(new_mesh, param);
	return new_mesh;
}

OvrMesh* ovr_engine::OvrMeshManager::CreateSphere(OvrString a_name, float a_radius, ovr::uint32 a_slices, ovr::uint32 a_stacks)
{
	OvrMesh* new_mesh = Create(a_name);
	MeshBuildParams param;
	param.type = kSphere;
	param.radius = a_radius;
	param.slices = a_slices;
	param.stacks = a_stacks;
	LoadManualSphere(new_mesh, param);
	return new_mesh;
}

OvrResource* ovr_engine::OvrMeshManager::CreateResource(OvrString a_name, bool is_manual)
{
	return new OvrMesh(a_name, is_manual);
}

void ovr_engine::OvrMeshManager::LoadManualCube(OvrMesh* a_mesh, const MeshBuildParams& param)
{
	OvrMeshBuffer* buf = a_mesh->CreateMeshBuffer();
	ovr::uint32 triangle_num = 12;
	ovr::uint32 vertex_num = 8;
	TempMeshVertex* vertex_buffers = new TempMeshVertex[vertex_num];
	OvrVector3 t_p[2];
	OvrVector3 pos(0.0f, 0.0f, 0.0f);
	t_p[0] = OvrVector3(param.size*-0.5f, param.size*-0.5f, param.size*-0.5f);
	t_p[1] = OvrVector3(param.size*0.5f, param.size*0.5f, param.size*0.5f);

	for (int i = 0; i < 2; ++i)
	{
		for (int j = 0; j < 2; ++j)
		{
			for (int k = 0; k < 2; ++k)
			{
				int t_id = i * 4 + j * 2 + k;
				OvrVector3& t_v = vertex_buffers[t_id].pos;
				OvrVector3& t_n = vertex_buffers[t_id].normal;
				OvrVector2& t_t = vertex_buffers[t_id].uv;
				t_v = OvrVector3(t_p[i][0], t_p[j][1], t_p[k][2]) + pos;
				t_n = t_v;
				t_n.normalize();
				t_t = OvrVector2(float(i), float(j));
			}
		}
	}

	ovr::uint32* indice = new ovr::uint32[3 * triangle_num];
	OvrTriangleImpl* t_tris = (OvrTriangleImpl*)indice;
	t_tris[0] = OvrTriangleImpl(0, 1, 2);
	t_tris[1] = OvrTriangleImpl(1, 3, 2);

	t_tris[2] = OvrTriangleImpl(0, 5, 1);
	t_tris[3] = OvrTriangleImpl(0, 4, 5);

	t_tris[4] = OvrTriangleImpl(1, 5, 3);
	t_tris[5] = OvrTriangleImpl(5, 7, 3);

	t_tris[6] = OvrTriangleImpl(5, 4, 7);
	t_tris[7] = OvrTriangleImpl(4, 6, 7);

	t_tris[8] = OvrTriangleImpl(4, 0, 6);
	t_tris[9] = OvrTriangleImpl(0, 2, 6);

	t_tris[10] = OvrTriangleImpl(3, 7, 2);
	t_tris[11] = OvrTriangleImpl(7, 6, 2);

	OvrBox3f box(OvrVector3(param.size*-0.5f, param.size*-0.5f, param.size*-0.5f), OvrVector3(param.size*0.5f, param.size*0.5f, param.size*0.5f));

	buf->vertex_data = new OvrVertexData();
	buf->index_data = new OvrIndexData();
	buf->vertex_data->declaration->AddElement(0, VET_FLOAT3, VES_POSITION);
	buf->vertex_data->declaration->AddElement(12, VET_FLOAT3, VES_NORMAL);
	buf->vertex_data->declaration->AddElement(24, VET_FLOAT2, VES_UV0);
	buf->vertex_data->vertex_buffer = OvrHardwareBufferManager::GetIns()->CreateVertexBuffer(32, vertex_num, vertex_buffers);
	buf->index_data->index_buffer = OvrHardwareBufferManager::GetIns()->CreateIndexBuffer(3 * triangle_num, indice);
	a_mesh->SetBounds(box);
}

void ovr_engine::OvrMeshManager::LoadManualPlane(OvrMesh* a_mesh, const MeshBuildParams& param)
{
	OvrMeshBuffer* buf = a_mesh->CreateMeshBuffer();

	ovr::uint32 triangle_num = 2;
	ovr::uint32 vertex_num = 4;
	TempMeshVertex* vertex_buffers = new TempMeshVertex[vertex_num];
	OvrVector3 pos(0.0f, 0.0f, 0.0f);
	TempMeshVertex temp;
	temp.pos = OvrVector3(-param.size / 2, 0.0f, param.size / 2);
	temp.normal = OvrVector3(0.0f, 1.0f, 0.0f);
	temp.uv = OvrVector2(0.0f, 0.0f);
	vertex_buffers[0] = temp;
	temp.pos = OvrVector3(param.size / 2, 0.0f, param.size / 2);
	temp.uv = OvrVector2(1.0f, 0.0f);
	vertex_buffers[1] = temp;
	temp.pos = OvrVector3(param.size / 2, 0.0f, -param.size / 2);
	temp.uv = OvrVector2(1.0f, 1.0f);
	vertex_buffers[2] = temp;
	temp.pos = OvrVector3(-param.size / 2, 0.0f, -param.size / 2);
	temp.uv = OvrVector2(0.0f, 1.0f);
	vertex_buffers[3] = temp;

	ovr::uint32* indice = new ovr::uint32[3 * triangle_num];
	OvrTriangleImpl* t_tris = (OvrTriangleImpl*)indice;
	t_tris[0] = OvrTriangleImpl(0, 1, 2);
	t_tris[1] = OvrTriangleImpl(0, 2, 3);

	OvrBox3f box(OvrVector3(param.size*-0.5f, 0, param.size*-0.5f), OvrVector3(param.size*0.5f, 0, param.size*0.5f));
	buf->vertex_data = new OvrVertexData();
	buf->index_data = new OvrIndexData();
	buf->vertex_data->declaration->AddElement(0, VET_FLOAT3, VES_POSITION);
	buf->vertex_data->declaration->AddElement(12, VET_FLOAT3, VES_NORMAL);
	buf->vertex_data->declaration->AddElement(24, VET_FLOAT2, VES_UV0);
	buf->vertex_data->vertex_buffer = OvrHardwareBufferManager::GetIns()->CreateVertexBuffer(32, vertex_num, vertex_buffers);
	buf->index_data->index_buffer = OvrHardwareBufferManager::GetIns()->CreateIndexBuffer(3 * triangle_num, indice);
	a_mesh->SetBounds(box);

	delete[] vertex_buffers;
	delete[] indice;
}

void ovr_engine::OvrMeshManager::LoadManualCylinder(OvrMesh* a_mesh, const MeshBuildParams& param)
{
	OvrMeshBuffer* buf = a_mesh->CreateMeshBuffer();

	ovr::uint32 vertex_num = param.stacks * 2 + 2;
	float originOffsetY = -param.height * 0.5f;

	TempMeshVertex* vertex_buffers = new TempMeshVertex[vertex_num];
	//�������㸳ֵ
	vertex_buffers[0].pos[0] = 0.0f;
	vertex_buffers[0].pos[1] = param.height + originOffsetY;
	vertex_buffers[0].pos[2] = 0.0f;
	vertex_buffers[0].normal = OvrVector3(0.0f, 1.0f, 0.0f);
	// ���׶��㸳ֵ
	vertex_buffers[vertex_num - 1].pos[0] = 0.0f;
	vertex_buffers[vertex_num - 1].pos[1] = originOffsetY;
	vertex_buffers[vertex_num - 1].pos[2] = 0.0f;
	vertex_buffers[vertex_num - 1].normal = OvrVector3(0.0f, -1.0f, 0.0f);

	float angleXZ;
	float posX, posZ;
	for (ovr::uint32 i = 0; i < param.stacks; i++)
	{
		angleXZ = (float)(TWO_PI * i / param.stacks);
		posX = sinf(angleXZ);
		posZ = cosf(angleXZ);

		vertex_buffers[i + 1].pos[0] = param.radius * posX;
		vertex_buffers[i + 1].pos[1] = param.height + originOffsetY;
		vertex_buffers[i + 1].pos[2] = param.radius * posZ;
		OvrVector3 normal = OvrVector3(posX, 0, posZ);
		//normal.normalize();
		//normal += lvr_vector3f(0.0f, 1.0f, 0.0f);
		normal.normalize();
		vertex_buffers[i + 1].normal = normal;
		vertex_buffers[i + 1 + param.stacks].pos[0] = param.radius * posX;
		vertex_buffers[i + 1 + param.stacks].pos[1] = originOffsetY;
		vertex_buffers[i + 1 + param.stacks].pos[2] = param.radius * posZ;
		normal = OvrVector3(param.radius * posX, 0, param.radius * posZ);
		normal.normalize();
		//normal += lvr_vector3f(0.0f, -1.0f, 0.0f);
		//normal.normalize();
		vertex_buffers[i + 1 + param.stacks].normal = normal;
	}

	ovr::uint32 numTriangles = param.stacks * 4;
	ovr::uint32* indice = new ovr::uint32[3 * numTriangles];
	OvrTriangleImpl* t_tris = (OvrTriangleImpl*)indice;
	ovr::uint32 nOffset = 0;
	for (ovr::uint32 i = 0; i < param.stacks; i++)
	{
		nOffset = i * 4;
		t_tris[nOffset].A = 0;
		t_tris[nOffset].B = 1 + i;
		t_tris[nOffset].C = 1 + (i + 1) % param.stacks;

		nOffset += 1;
		t_tris[nOffset].A = vertex_num - 1;
		t_tris[nOffset].B = 1 + param.stacks + (i + 1) % param.stacks;
		t_tris[nOffset].C = 1 + param.stacks + i;

		nOffset += 1;
		t_tris[nOffset].A = 1 + i;
		t_tris[nOffset].B = 1 + param.stacks + i;
		t_tris[nOffset].C = 1 + (i + 1) % param.stacks;

		nOffset += 1;
		t_tris[nOffset].A = 1 + (i + 1) % param.stacks;
		t_tris[nOffset].B = 1 + param.stacks + i;
		t_tris[nOffset].C = 1 + param.stacks + (i + 1) % param.stacks;
	}

	OvrBox3f box(OvrVector3(-param.radius, -param.height * 0.5f, -param.radius), OvrVector3(param.radius, param.height * 0.5f, param.radius));
	buf->vertex_data = new OvrVertexData();
	buf->index_data = new OvrIndexData();
	buf->vertex_data->declaration->AddElement(0, VET_FLOAT3, VES_POSITION);
	buf->vertex_data->declaration->AddElement(12, VET_FLOAT3, VES_NORMAL);
	buf->vertex_data->declaration->AddElement(24, VET_FLOAT2, VES_UV0);
	buf->vertex_data->vertex_buffer = OvrHardwareBufferManager::GetIns()->CreateVertexBuffer(32, vertex_num, vertex_buffers);
	buf->index_data->index_buffer = OvrHardwareBufferManager::GetIns()->CreateIndexBuffer(3 * numTriangles, indice);
	a_mesh->SetBounds(box);
	delete[] vertex_buffers;
	delete[] indice;
}

void ovr_engine::OvrMeshManager::LoadManualSphere(OvrMesh* a_mesh, const MeshBuildParams& param)
{
	OvrMeshBuffer* buf = a_mesh->CreateMeshBuffer();
	const int poleVertical = 3;
	const int uniformVertical = param.slices;
	const int horizontal = param.stacks;
	const int vertical = uniformVertical + poleVertical * 2;

	const int vertexCount = (horizontal + 1) * (vertical + 1);

	TempMeshVertex* vertex_buffers = new TempMeshVertex[vertexCount];
	OvrVector3 pos(0.0f, 0.0f, 0.0f);
	for (int y = 0; y <= vertical; y++)
	{
		float yf;
		if (y <= poleVertical)
		{
			yf = (float)y / (poleVertical + 1) / uniformVertical;
		}
		else if (y >= vertical - poleVertical)
		{
			yf = (float)(uniformVertical - 1 + ((float)(y - (vertical - poleVertical - 1)) / (poleVertical + 1))) / uniformVertical;
		}
		else
		{
			yf = (float)(y - poleVertical) / uniformVertical;
		}
		const float lat = (float)((yf - 0.5f) * PI);
		const float cosLat = cosf(lat);
		for (int x = 0; x <= horizontal; x++)
		{
			const float xf = (float)x / (float)horizontal;
			const float lon = (float)((0.5f + xf) * TWO_PI);
			const int index = y * (horizontal + 1) + x;

			if (x == horizontal)
			{
				// Make sure that the wrap seam is EXACTLY the same
				// xyz so there is no chance of pixel cracks.
				vertex_buffers[index].pos = vertex_buffers[y * (horizontal + 1) + 0].pos;
				vertex_buffers[index].normal = vertex_buffers[y * (horizontal + 1) + 0].normal;
			}
			else
			{
				vertex_buffers[index].normal[0] = cosf(lon) * cosLat;
				vertex_buffers[index].normal[2] = sinf(lon) * cosLat;
				vertex_buffers[index].normal[1] = sinf(lat);
				vertex_buffers[index].pos = vertex_buffers[index].normal * param.radius + pos;
			}

			// With a normal mapping, half the triangles degenerate at the poles,
			// which causes seams between every triangle.  It is better to make them
			// a fan, and only get one seam.
			if (y == 0 || y == vertical)
			{
				vertex_buffers[index].uv[0] = 0.5f;
			}
			else
			{
				vertex_buffers[index].uv[0] = xf;
			}
			vertex_buffers[index].uv[1] = (1.0f - yf);
		}
	}

	ovr::uint32 triangle_num = horizontal * vertical * 2;
	ovr::uint32* indice = new ovr::uint32[3 * triangle_num];
	OvrTriangleImpl* t_tris = (OvrTriangleImpl*)indice;
	int index = 0;
	for (int x = 0; x < horizontal; x++)
	{
		for (int y = 0; y < vertical; y++)
		{
			t_tris[index].A = y * (horizontal + 1) + x;
			t_tris[index].C = y * (horizontal + 1) + x + 1;
			t_tris[index].B = (y + 1) * (horizontal + 1) + x;
			t_tris[index + 1].A = (y + 1) * (horizontal + 1) + x;
			t_tris[index + 1].C = y * (horizontal + 1) + x + 1;
			t_tris[index + 1].B = (y + 1) * (horizontal + 1) + x + 1;
			index += 2;
		}
	}

	OvrBox3f box(OvrVector3(-param.radius, -param.radius, -param.radius), OvrVector3(param.radius, param.radius, param.radius));
	buf->vertex_data = new OvrVertexData();
	buf->index_data = new OvrIndexData();
	buf->vertex_data->declaration->AddElement(0, VET_FLOAT3, VES_POSITION);
	buf->vertex_data->declaration->AddElement(12, VET_FLOAT3, VES_NORMAL);
	buf->vertex_data->declaration->AddElement(24, VET_FLOAT2, VES_UV0);
	buf->vertex_data->vertex_buffer = OvrHardwareBufferManager::GetIns()->CreateVertexBuffer(32, vertexCount, vertex_buffers);
	buf->index_data->index_buffer = OvrHardwareBufferManager::GetIns()->CreateIndexBuffer(3 * triangle_num, indice);
	a_mesh->SetBounds(box);

	delete[] vertex_buffers;
	delete[] indice;
}