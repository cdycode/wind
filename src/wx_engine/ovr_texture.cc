#include "wx_engine/ovr_texture.h"
using namespace ovr_engine;
ovr_engine::OvrTexture::OvrTexture(OvrString a_name, bool a_is_manual)
	: OvrResource(a_name, a_is_manual)
	, is_render_target(false)
	, render_target(nullptr)
	, is_internal_created(false)
	, width(0)
	, height(0)
	, msaa(0)
	, depth(0)
	, mip_map(1)
	, type(TEXTURE_TYPE_2D)
{
}

ovr_engine::OvrTexture::~OvrTexture()
{
}


ovr::uint32 ovr_engine::OvrTexture::GetFaceNum()
{
	if (type == TEXTURE_TYPE_CUBE || type == TEXTURE_TYPE_CUBE_ARRAY)
		return 6;
	return 1;
}

void ovr_engine::OvrTexture::CreateInternalResources()
{
	if (!is_internal_created)
	{
		CreateInternalResourcesImpl();
		is_internal_created = true;
	}
}

void ovr_engine::OvrTexture::FreeInternalResources()
{
	if (is_internal_created)
	{
		FreeInternalResourcesImpl();
		is_internal_created = false;
	}
}

