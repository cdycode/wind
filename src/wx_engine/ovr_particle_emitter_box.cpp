#include "wx_engine/ovr_particle_emitter_box.h"
#include "wx_base/wx_matrix4f.h"
#include <cmath>
#include <random>
#include <algorithm>
#include <numeric>
#include "wx_engine/ovr_particle_class_info.h"

using namespace  ovr;
using namespace ovr_engine;
REGISTER_CLASS(OvrParticleEmitterBox)
OvrParticleEmitterBox::OvrParticleEmitterBox()
	:OvrParticleEmitter()
{

}

OvrParticleEmitterBox::~OvrParticleEmitterBox()
{

}

void OvrParticleEmitterBox::SetBoxSize(OvrVector3 a_size)
{
	box_size = a_size;
}

static std::default_random_engine engine(5);
static std::uniform_int_distribution<unsigned> u(0, 100);
void OvrParticleEmitterBox::EmitterParticle(OvrParticle* ao_particle)
{
	ao_particle->position =pos + OvrVector3(OvrRandom(-box_size[0], box_size[0]), OvrRandom(-box_size[1], box_size[1]), OvrRandom(-box_size[2], box_size[2]));
	ao_particle->direction = main_dir + OvrVector3(OvrRandom(-1.0f,1.0f),0.0f,OvrRandom(-1.0f,1.0f));
	ao_particle->cur_v = main_dir*OvrRandom(min_speed, max_speed);
	ao_particle->life_now = 0.0f;
	ao_particle->life_total = OvrRandom(min_life,max_life);
	ao_particle->offset_pos = OvrVector3(0.0f, 0.0f, 0.f);
	ao_particle->scale_size = OvrRandom(min_size,max_size);
	ao_particle->weight = 1.0f;
	ao_particle->color = color;

	
	float random_rad = (float)u(engine) / 100;
	OvrMatrix4 tmpM = OvrMatrix4::make_rotation_matrix_around_axis(OvrVector3::UNIT_Z, random_rad*OVR_PI);
	tmpM.decompose(nullptr, &ao_particle->rotation_q ,nullptr);
	
}
