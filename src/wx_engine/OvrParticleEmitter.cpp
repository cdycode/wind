#include "wx_engine/OvrParticleEmitter.h"
#include <map>
#include "wx_engine/ovr_particle_class_info.h"
using namespace ovr_engine;
static std::map<OvrString, ClassInfo*> *m_classInfoMap = NULL;
OvrParticleEmitter::OvrParticleEmitter()
	:pos(0.0,0.0,0.0)
	,rad(0)
	,max_life(0.0f)
	,min_life(0.0f)
	,max_size(0.0f)
	,min_size(0.0f)
	,max_speed(0.0f)
	,min_speed(0.0f)
	,face_camera(false)
	, main_dir(OvrVector3(0.0f,1.0f,0.0f))
{

}

OvrParticleEmitter::~OvrParticleEmitter()
{

}

void OvrParticleEmitter::SetInitRotateQ(const OvrQuaternion& a_q)
{
	init_rot = a_q;
}

void OvrParticleEmitter::SetMainDir(const OvrVector3& a_dir)
{
	main_dir = a_dir;
}

void OvrParticleEmitter::SetInitAxisRad(const OvrVector3& a_axis, float a_rad)
{
	axis = a_axis;
	rad = a_rad;
}

void OvrParticleEmitter::SetColor(OvrVector4 a_color)
{
	color = a_color;
}

void OvrParticleEmitter::SetSize(float a_max, float a_min)
{
	max_size = a_max;
	min_size = a_min;
}

void OvrParticleEmitter::SetSpeed(float a_max, float a_min)
{
	max_speed = a_max;
	min_speed = a_min;
}

void OvrParticleEmitter::SetLife(float a_max, float a_min)
{
	max_life = a_max;
	min_life = a_min;
}

void OvrParticleEmitter::SetPos(const OvrVector3& a_pos)
{
	pos = a_pos;
}

ovr_engine::OvrParticleEmitter::OvrParticleEmitter(float a_max_size, float a_min_size, float a_max_speed, float a_min_speed, float a_max_life, float a_min_life)
	:max_size(a_max_size)
	,min_size(a_min_size)
	,max_speed(a_max_speed)
	,min_speed(a_min_speed)
	,max_life(a_max_life)
	,min_life(a_min_life)
	, face_camera(false)
	, main_dir(OvrVector3(0.0f, 1.0f, 0.0f))
{

}

void ovr_engine::OvrParticleEmitter::SetFaceCamera(bool if_face_camera)
{
	face_camera = if_face_camera;
}

OvrVector3 const ovr_engine::OvrParticleEmitter::GetPos()
{
	return pos;
}

bool ovr_engine::OvrParticleEmitter::Register(ClassInfo* pCInfo)
{

	if (!m_classInfoMap)
	{
		m_classInfoMap = new std::map<OvrString, ClassInfo*>();
	}
	if (!pCInfo)
	{
		return false;
	}
	if (m_classInfoMap->end() == m_classInfoMap->find(pCInfo->m_className))
	{
		m_classInfoMap->insert(std::map<OvrString, ClassInfo*>::value_type(pCInfo->m_className, pCInfo));
	}
	return true;
}

OvrParticleEmitter* ovr_engine::OvrParticleEmitter::CreateObject(const OvrString& className)
{
	std::map<OvrString, ClassInfo*>::const_iterator c_iter = m_classInfoMap->find(className);
	if (m_classInfoMap->end() != c_iter)
	{
		return c_iter->second->CreateObject();
	}
	return nullptr;
}

