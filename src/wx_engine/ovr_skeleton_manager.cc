#include "wx_engine/ovr_skeleton_manager.h"
#include "wx_engine/ovr_skeleton.h"
#include "wx_engine/ovr_serializer_impl.h"
using namespace ovr_engine;
template<> OvrSkeletonManager* ovr_engine::OvrSingleton<OvrSkeletonManager>::s_instance = nullptr;

ovr_engine::OvrSkeletonManager::OvrSkeletonManager()
{

}

ovr_engine::OvrSkeletonManager::~OvrSkeletonManager()
{
	UnInitialise();
}

bool ovr_engine::OvrSkeletonManager::Initialise()
{
	return true;
}

void ovr_engine::OvrSkeletonManager::UnInitialise()
{
	RemoveAll();
}

OvrSkeleton* ovr_engine::OvrSkeletonManager::Load(OvrString a_file_name)
{
	OvrSkeleton* skeleton = (OvrSkeleton*)CreateOrRetrieve(a_file_name, false);
	skeleton->Load();
	return skeleton;
}

OvrResource* ovr_engine::OvrSkeletonManager::CreateResource(OvrString a_name, bool is_manual)
{
	return new OvrSkeleton(a_name, is_manual);
}
