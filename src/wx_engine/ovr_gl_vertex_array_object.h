#include "wx_engine/ovr_vertex_index_data.h"
#include "ovr_gl_define.h"
namespace ovr_engine
{
	class OvrGLHardwareVertexBuffer;
	class OvrGLVertexArrayObject : public OvrVertexDeclaration
	{
	public:
		OvrGLVertexArrayObject();
		virtual ~OvrGLVertexArrayObject();
		void		 Bind();
		void         UnBind();
		void         BindToGpu(OvrGLHardwareVertexBuffer* a_vertex_buf);
		bool		 NeedUpdate() { return need_update; }
		virtual void NotifyChanged();

	protected:
		GLuint vao;
		static int s_type_size[VET_COUNT];
		static int s_type_channel[VET_COUNT];
		static int s_type_type[VET_COUNT];
		bool need_update;
	};

}