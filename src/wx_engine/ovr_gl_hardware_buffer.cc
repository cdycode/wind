#include "ovr_gl_hardware_buffer.h"
#include "ovr_gl_define.h"
using namespace ovr_engine;

ovr_engine::OvrGLHardwareVertexBuffer::OvrGLHardwareVertexBuffer(ovr::uint32 a_vertex_size, ovr::uint32 a_vcount, void* a_vdata, bool a_static_draw )
	:OvrHardwareVertexBuffer(a_vertex_size, a_vcount)
{
	glGenBuffers(1, &id);
	glBindBuffer(GL_ARRAY_BUFFER, id);
	unsigned int memsize = a_vertex_size * a_vcount;
	glBufferData(GL_ARRAY_BUFFER, memsize, a_vdata, a_static_draw ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

ovr_engine::OvrGLHardwareVertexBuffer::~OvrGLHardwareVertexBuffer()
{

}

void* ovr_engine::OvrGLHardwareVertexBuffer::Lock(unsigned int a_mode)
{
	glBindBuffer(GL_ARRAY_BUFFER, id);
	void* mem = glMapBuffer(GL_ARRAY_BUFFER, GL_READ_WRITE);//GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT
	return mem;
}

void ovr_engine::OvrGLHardwareVertexBuffer::Unlock()
{
	glUnmapBuffer(GL_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void ovr_engine::OvrGLHardwareVertexBuffer::WriteData(int a_offset, int a_update_data_len, const void* a_datas)
{
	glBindBuffer(GL_ARRAY_BUFFER, id);
	glBufferSubData(GL_ARRAY_BUFFER, a_offset, a_update_data_len, a_datas);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

ovr_engine::OvrGLHardwareIndexBuffer::OvrGLHardwareIndexBuffer(ovr::uint32 a_icount, void* a_idata)
	:OvrHardwareIndexBuffer(a_icount)
{
	glGenBuffers(1, &id);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id);
	unsigned int memsize = 4 * a_icount;
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, memsize, a_idata, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

ovr_engine::OvrGLHardwareIndexBuffer::~OvrGLHardwareIndexBuffer()
{

}

void* ovr_engine::OvrGLHardwareIndexBuffer::Lock(unsigned int a_mode)
{
	return nullptr;
}

void ovr_engine::OvrGLHardwareIndexBuffer::Unlock()
{

}

void ovr_engine::OvrGLHardwareIndexBuffer::WriteData(int a_offset, int a_update_data_len, const void* a_datas)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id);
	glBufferSubData(id, a_offset, a_update_data_len, a_datas);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
