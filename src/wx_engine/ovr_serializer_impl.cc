﻿#include "rapidjson/stringbuffer.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/document.h"
#include <vector>
#include <lvr_ktx.h>
#include <limits.h>
#include "headers.h"
#include <wx_engine/ovr_serializer_impl.h>
#include "wx_base/wx_small_file.h"
#include "wx_engine/ovr_vertex_index_data.h"
#include "wx_engine/ovr_logger.h"
#include "lvr_shader_manager.h"
#include "wx_engine/ovr_texture_manager.h"
#include "wx_asset_manager/ovr_archive_material.h"
#include "wx_engine/ovr_pose.h"
#include "wx_base/wx_log.h"
#include "ovr_gl_texture.h"
#include "wx_engine/ovr_hardware_buffer_manager.h"
#include "wx_engine/ovr_mesh.h"
#include "wx_engine/ovr_gpu_program_parameters.h"
using namespace ovr_engine;
template<> OvrSerializerImpl* ovr_engine::OvrSingleton<OvrSerializerImpl>::s_instance = nullptr;

ovr_engine::OvrSerializerImpl::OvrSerializerImpl()
{
	manager = ovr_asset::OvrArchiveAssetManager::GetIns();
}

ovr_engine::OvrSerializerImpl::~OvrSerializerImpl()
{

}

bool ovr_engine::OvrSerializerImpl::ImportMesh(OvrString a_file_name, OvrMesh* dest)
{
	bool hasSkeleton = false;
	ovr_asset::OvrArchiveMesh* mesh_asset = (ovr_asset::OvrArchiveMesh*)manager->LoadAsset(a_file_name.GetStringConst(), true);
	if (!mesh_asset)
	{
		OvrLogger::GetIns()->Log(a_file_name + " can not found.", LL_ERROR);
		return false;
	}

	if (mesh_asset->GetSkeletonPath().GetStringSize() > 0)
	{
		hasSkeleton = true;
		dest->SetSkeletonName(mesh_asset->GetSkeletonPath());
	}

	if (mesh_asset->GetPosePath().GetStringSize() > 0)
	{
		dest->SetPoseName(mesh_asset->GetPosePath());
		ovr_asset::OvrArchivePoseContainer* pose_asset = (ovr_asset::OvrArchivePoseContainer*)manager->LoadAsset(mesh_asset->GetPosePath(), true);
		for (ovr::uint32 i = 0; i < pose_asset->GetPoseNum(); i++)
		{
			ovr_asset::OvrPose& asset_pose = pose_asset->GetPose(i);
			OvrPose* pose = dest->CreatePose(asset_pose.surface_name, asset_pose.name);
			auto offset_cur = asset_pose.vertex_offset_map.begin();
			while (offset_cur != asset_pose.vertex_offset_map.end())
			{
				pose->AddVertex(offset_cur->first, offset_cur->second);
				offset_cur++;
			}
		}
	}

	OvrBox3f box;
	box.get_min() = OvrVector3(FLT_MAX, FLT_MAX, FLT_MAX);
	box.get_max() = OvrVector3(FLT_MIN, FLT_MIN, FLT_MIN);
	for (ovr::uint32 s_index = 0; s_index < mesh_asset->GetSurfaceNum(); s_index++)
	{
		ovr_asset::OvrArchiveSurface* surface_asset = mesh_asset->GetSurface(s_index);
		OvrMeshBuffer* buf = dest->CreateMeshBuffer(surface_asset->GetName());
		if (surface_asset->GetMaterialNum() <= 0)
		{
			buf->SetMaterialName("BaseWhite");
		}
		else
		{
			OvrString material_name = surface_asset->GetMaterialPath(0);
			buf->SetMaterialName(material_name);
		}

		ovr::uint32 triganle_num = surface_asset->GetTriangleNum();
		ovr::uint32* indice = new ovr::uint32[triganle_num * 3];
		OvrTriangleImpl* triangles = (OvrTriangleImpl*)indice;
		for (ovr::uint32 i = 0; i < triganle_num; i++)
		{
			OvrArray3i& current_triangle = surface_asset->GetTriangle(i);
			triangles[i].A = current_triangle[0];
			triangles[i].B = current_triangle[1];
			triangles[i].C = current_triangle[2];
		}
		ovr::uint32 v_num = surface_asset->GetVertexNum();		
		if (hasSkeleton)
		{
			SkeletalMeshVertex* vextices = new SkeletalMeshVertex[v_num];
			for (ovr::uint32 v_index = 0; v_index < v_num; v_index++)
			{
				ovr_asset::OvrVertex vertex_asset = surface_asset->GetVertex(v_index);
				vextices[v_index].pos = OvrVector3(vertex_asset.position[0], vertex_asset.position[1], vertex_asset.position[2]);
				vextices[v_index].uv0[1] = 1.0f - vertex_asset.uv0[1];
				vextices[v_index].uv0[0] = vertex_asset.uv0[0];
				vextices[v_index].normal = OvrVector3(vertex_asset.normal[0], vertex_asset.normal[1], vertex_asset.normal[2]);
				vextices[v_index].tangent = OvrVector3(vertex_asset.tangent[0], vertex_asset.tangent[1], vertex_asset.tangent[2]);
				vextices[v_index].binormal = OvrVector3(vertex_asset.binormal[0], vertex_asset.binormal[1], vertex_asset.binormal[2]);
				vextices[v_index].indices[0] = vertex_asset.bone_index[0];
				vextices[v_index].indices[1] = vertex_asset.bone_index[1];
				vextices[v_index].indices[2] = vertex_asset.bone_index[2];
				vextices[v_index].indices[3] = vertex_asset.bone_index[3];				
				vextices[v_index].weights = OvrVector4(vertex_asset.bone_weights[0], vertex_asset.bone_weights[1], vertex_asset.bone_weights[2], vertex_asset.bone_weights[3]);
				box.expand(vertex_asset.position);
			}

			buf->vertex_data = new OvrVertexData();
			buf->index_data = new OvrIndexData();
			buf->vertex_data->declaration->AddElement(0, VET_FLOAT3, VES_POSITION);
			buf->vertex_data->declaration->AddElement(12, VET_FLOAT2, VES_UV0);
			buf->vertex_data->declaration->AddElement(20, VET_FLOAT3, VES_NORMAL);
			buf->vertex_data->declaration->AddElement(32, VET_FLOAT3, VES_TANGENT);
			buf->vertex_data->declaration->AddElement(44, VET_FLOAT3, VES_BINORMAL);
			buf->vertex_data->declaration->AddElement(56, VET_FLOAT4, VES_JOINT_WEIGHTS);
			buf->vertex_data->declaration->AddElement(72, VET_UBYTE4, VES_JOINT_INDICES);
			buf->vertex_data->vertex_buffer = OvrHardwareBufferManager::GetIns()->CreateVertexBuffer(76, v_num, vextices);
			buf->index_data->index_buffer = OvrHardwareBufferManager::GetIns()->CreateIndexBuffer(triganle_num * 3, indice);
			delete[] vextices;
		}
		else
		{
			StaticMeshVertex* vextices = new StaticMeshVertex[v_num];
			for (ovr::uint32 v_index = 0; v_index < v_num; v_index++)
			{
				ovr_asset::OvrVertex vertex_asset = surface_asset->GetVertex(v_index);
				vextices[v_index].pos = OvrVector3(vertex_asset.position[0], vertex_asset.position[1], vertex_asset.position[2]);
				vextices[v_index].uv0[1] = 1.0f - vertex_asset.uv0[1];
				vextices[v_index].uv0[0] = vertex_asset.uv0[0];
				vextices[v_index].normal = OvrVector3(vertex_asset.normal[0], vertex_asset.normal[1], vertex_asset.normal[2]);
				vextices[v_index].tangent = OvrVector3(vertex_asset.tangent[0], vertex_asset.tangent[1], vertex_asset.tangent[2]);
				vextices[v_index].binormal = OvrVector3(vertex_asset.binormal[0], vertex_asset.binormal[1], vertex_asset.binormal[2]);
				vextices[v_index].color[0] = vertex_asset.color[0];
				vextices[v_index].color[1] = vertex_asset.color[1];
				vextices[v_index].color[2] = vertex_asset.color[2];
				vextices[v_index].color[3] = vertex_asset.color[3];
				box.expand(vertex_asset.position);
			}	
			buf->vertex_data = new OvrVertexData();
			buf->index_data = new OvrIndexData();
			buf->vertex_data->declaration->AddElement(0, VET_FLOAT3, VES_POSITION);
			buf->vertex_data->declaration->AddElement(12, VET_FLOAT2, VES_UV0);
			buf->vertex_data->declaration->AddElement(20, VET_FLOAT3, VES_NORMAL);
			buf->vertex_data->declaration->AddElement(32, VET_FLOAT3, VES_TANGENT);
			buf->vertex_data->declaration->AddElement(44, VET_FLOAT3, VES_BINORMAL);
			buf->vertex_data->declaration->AddElement(56, VET_UBYTE4, VES_COLOR);
			buf->vertex_data->vertex_buffer = OvrHardwareBufferManager::GetIns()->CreateVertexBuffer(60, v_num, vextices);
			buf->index_data->index_buffer = OvrHardwareBufferManager::GetIns()->CreateIndexBuffer(triganle_num * 3, indice);
			delete[] vextices;
		}
		delete[] indice;
	}
	dest->SetBounds(box);
	return true;
}

bool ovr_engine::OvrSerializerImpl::ImportSkeleton(OvrString a_file_name, OvrSkeleton* dest)
{
	ovr_asset::OvrArchiveSkeleton* skeleton_asset = (ovr_asset::OvrArchiveSkeleton*)manager->LoadAsset(a_file_name.GetStringConst(), true);
	if (!skeleton_asset)
	{
		OvrLogger::GetIns()->Log(a_file_name + " can not found.", LL_ERROR);
		return false;
	}
	ovr::uint32 bone_num = skeleton_asset->GetJointNum();
	dest->SetJointNum(bone_num);
	for (ovr::uint32 i = 0; i < bone_num; i++)
	{
		//skeleton_animation_array 与a_skeleton中数组一一对应
		ovr_asset::OvrArchiveJoint& joint_asset = skeleton_asset->GetJoint(i);
		dest->GetJoint(i)->name = joint_asset.name;
		dest->GetJoint(i)->bind_pose = joint_asset.default_pose;
		dest->GetJoint(i)->vertex_to_local_matrix = joint_asset.bone_to_mesh_mat;
		dest->GetJoint(i)->local_to_parent_matrix = joint_asset.parent_transform;
		dest->GetJoint(i)->index = i;
		//构建父子关系
		if (joint_asset.parent_index >= 0)
		{
			dest->GetJoint(i)->parent = dest->GetJoint(joint_asset.parent_index);
			dest->GetJoint(i)->parent->child_list.push_back(dest->GetJoint(i));
		}
	}
	return true;
}

bool ovr_engine::OvrSerializerImpl::GetImageInfo(OvrString a_file_name, OvrImage& a_info)
{
	ovr_asset::OvrArchiveTexture* tex_asset = (ovr_asset::OvrArchiveTexture*)manager->LoadAsset(a_file_name.GetStringConst(), true);
	if (!tex_asset)
	{
		OvrLogger::GetIns()->Log(a_file_name + " can not found.", LL_ERROR);
		return false;
	}

	lvr_image_info l_image_info;
	if (!lvr_ktx::load_from_mem((const int8_t*)tex_asset->GetDisplayName().GetStringConst(), tex_asset->GetBuffer(), tex_asset->GetDataSize(), false, false, l_image_info))
	{
		OvrLogger::GetIns()->Log(a_file_name + " Ktx load form memory faild.", LL_ERROR);
		return false;
	}
	a_info = OvrGLPixelUtil::GetOvrImageInfo(l_image_info);
	return true;
}

bool ovr_engine::OvrSerializerImpl::ImportMaterial(OvrString a_file_name, OvrMaterial* dest)
{
	ovr_asset::OvrArchiveMaterial* mat_asset = (ovr_asset::OvrArchiveMaterial*)manager->LoadAsset(a_file_name.GetStringConst(), true);
	if (!mat_asset)
	{
		OvrLogger::GetIns()->Log(a_file_name + " can not found.", LL_ERROR);
		return false;
	}
	OvrLogD("ovrengine_debug", "Load material %s ", a_file_name.GetStringConst());
	dest->SetProgram(mat_asset->GetShaderName());
	if (mat_asset->IsSetSceneBlend())
	{
		int src_str = mat_asset->GetSceneBlendSrc();
		int dest_str = mat_asset->GetSceneBlendDest();
		dest->SetSceneBlend((ovr_engine::SceneBlendFactor)src_str, (ovr_engine::SceneBlendFactor)dest_str);
	}
	
	dest->SetDepthWriteEnable(mat_asset->IsDepthWrite());
	dest->SetDepthCheckEnable(mat_asset->IsDepthCheck());
	
	if (mat_asset->IsCullMode())
	{
		int cullmode = mat_asset->GetCullMode();
		
		dest->SetCullMode((ovr_engine::CullingMode)cullmode);
	}

	OvrGpuProgram* shader = dest->GetProgram();
	if (nullptr == shader)
	{
		return false;
	}
	const ovr_engine::OvrGpuProgramConstants* shader_constants = shader->GetShaderConstants();
	if (nullptr == shader_constants)
	{
		return false;
	}
	const ovr_engine::ConstantList* constant_list = shader_constants->GetCustomConstantList();

	//遍历参数列表

	for (ovr::uint32 index = 0; index < constant_list->size(); index++)
	{
		const ovr_engine::OvrConstant& constant = (*constant_list)[index];

		if (constant.const_type == GCT_VEC4)
		{
			OvrVector4 constvalue;
			if (mat_asset->GetVector4ByName(constant.gpu_name, constvalue))
				dest->GetProgramParameters()->SetNameConstant(constant.gpu_name, constvalue);
			else
			{
				//设置初值
				if (constant.gpu_name == "DiffuseColor")
				{
					dest->GetProgramParameters()->SetNameConstant(constant.gpu_name, OvrVector4(1, 1, 1, 1));
					mat_asset->SetConstantFloat4("DiffuseColor", OvrVector4(1, 1, 1, 1));
				}
			}
		}
		else if (constant.const_type == GCT_SAMPLER2D)
		{
			OvrTextureUnit util;
			OvrString texturePath;
			if (mat_asset->GetSample2DByName(constant.gpu_name, texturePath))
			{
				if (texturePath != "null")
					util.SetTexture(OvrTextureManager::GetIns()->Load(texturePath));
			}
			dest->GetProgramParameters()->SetNameConstant(constant.gpu_name, util);
		}
		else if (constant.const_type == GCT_FLOAT1)
		{
			float constvalue;
			
			if (mat_asset->GetFloatByName(constant.gpu_name, constvalue))
				dest->GetProgramParameters()->SetNameConstant(constant.gpu_name, constvalue);
			else
			{
				//设置初值
			}
		}
	}

	manager->DestoryAsset(mat_asset);
	mat_asset = NULL;

	return true;
}

bool ovr_engine::OvrSerializerImpl::ExportMaterial(OvrMaterial* dest)
{
	return ExportMaterial(dest->GetName(), dest);
}

bool ovr_engine::OvrSerializerImpl::ExportMaterial(const OvrString& a_file_name, OvrMaterial* src)
{
	ovr_asset::OvrArchiveMaterial* mat_asset = (ovr_asset::OvrArchiveMaterial*)manager->CreateAsset(ovr_asset::kAssetMaterial,a_file_name.GetStringConst());
	OvrGpuProgram* shader = src->GetProgram();
	
	if (!mat_asset)
	{
		OvrLogger::GetIns()->Log(a_file_name + " can not found.", LL_ERROR);
		return false;
	}
	if (shader != NULL)
		mat_asset->SetShaderName(shader->GetName());

	if (!src->GetDepthCheckEnable())
		mat_asset->SetDepthCheck(false);
	if (!src->GetDepthWriteEnable())
		mat_asset->SetDepthWrite(false);

	if (src->GetDestBlendFactor() != SBF_ZERO || src->GetSourceBlendFactor() != SBF_ONE)
	{
		mat_asset->SetSceneBlend(src->GetSourceBlendFactor(), src->GetDestBlendFactor());
	}
		
	mat_asset->SetCullMode(src->GetCullMode());

	const ConstantList* constant_list = src->GetProgramParameters()->GetCustomConstantList();
	ConstantList::const_iterator cur = constant_list->begin();
	ConstantList::const_iterator end = constant_list->end();

	for (; cur != end; cur++)
	{
		switch (cur->const_type)
		{
			case	GCT_FLOAT1:
			{
				const float* float_ptr = src->GetProgramParameters()->GetFloatPointer(cur->physicalIndex);
				mat_asset->SetConstantFloat(cur->gpu_name, float_ptr[0]);
				break;
			}
			
			case	GCT_SAMPLER2D:
			{
				OvrTextureUnit* unit = src->GetProgramParameters()->GetTextureUnit(cur->physicalIndex);
				if (unit->GetTexture())
					mat_asset->SetConstantSample2D(cur->gpu_name, unit->GetTexture()->GetName());
				else
					mat_asset->SetConstantSample2D(cur->gpu_name, "null");
				break;
			}
		}
	}

	bool bsuccess = mat_asset->Save();
	
	manager->DestoryAsset(mat_asset);
	mat_asset = NULL;
	return bsuccess;
}

OvrString ovr_engine::OvrSerializerImpl::GetFullPath(const OvrString& path)
{
	OvrString root_dir = manager->GetRootDir();
	return root_dir + path;
}

ovr_engine::SceneBlendFactor ovr_engine::OvrSerializerImpl::GetSceneBlendFactor(const OvrString& str)
{
	if (str == "one")
		return SBF_ONE;
	else if (str == "zero")
		return SBF_ZERO;
	else if (str == "dest_colour")
		return SBF_DEST_COLOUR;
	else if (str == "src_colour")
		return SBF_SOURCE_COLOUR;
	else if (str == "one_minus_dest_colour")
		return SBF_ONE_MINUS_DEST_COLOUR;
	else if (str == "one_minus_src_colour")
		return SBF_ONE_MINUS_SOURCE_COLOUR;
	else if (str == "dest_alpha")
		return SBF_DEST_ALPHA;
	else if (str == "src_alpha")
		return SBF_SOURCE_ALPHA;
	else if (str == "one_minus_dest_alpha")
		return SBF_ONE_MINUS_DEST_ALPHA;
	else if (str == "one_minus_src_alpha")
		return SBF_ONE_MINUS_SOURCE_ALPHA;
	else
		return SBF_ONE;
}

OvrString ovr_engine::OvrSerializerImpl::GetSceneBlendString(const SceneBlendFactor& fac)
{
	switch (fac)
	{
	case	SBF_ONE:
		return OvrString("one");
	case	SBF_ZERO:
		return OvrString("zero");
	case	SBF_DEST_COLOUR:
		return OvrString("dest_colour");
	case	SBF_SOURCE_COLOUR:
		return OvrString("dest_colour");
	case	SBF_ONE_MINUS_DEST_COLOUR:
		return OvrString("one_minus_dest_colour");
	case	SBF_ONE_MINUS_SOURCE_COLOUR:
		return OvrString("one_minus_src_colour");
	case	SBF_DEST_ALPHA:
		return OvrString("dest_alpha");
	case	SBF_SOURCE_ALPHA:
		return OvrString("src_alpha");
	case	SBF_ONE_MINUS_DEST_ALPHA:
		return OvrString("one_minus_dest_alpha");
	case	SBF_ONE_MINUS_SOURCE_ALPHA:
		return OvrString("one_minus_src_alpha");
	default:
		return OvrString("one");
	}
}

