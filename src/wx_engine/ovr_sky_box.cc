﻿#include "wx_engine/ovr_sky_box.h"
#include "wx_engine/ovr_vertex_index_data.h"
#include "wx_engine/ovr_hardware_buffer_manager.h"
using namespace ovr_engine;

ovr_engine::OvrSkyBox::OvrSkyBox()
	: material(nullptr)
	, size(0)
{

}

ovr_engine::OvrSkyBox::OvrSkyBox(float a_size)
	: material(nullptr)
	, size(0)
{
	SetSize(a_size);
}

ovr_engine::OvrSkyBox::~OvrSkyBox()
{
}

void ovr_engine::OvrSkyBox::GetRenderOperation(OvrRenderOperation& a_op)
{

}

void ovr_engine::OvrSkyBox::SetSize(float a_size)
{
	if (size == a_size)
		return;

	struct Vertex
	{
		OvrVector3 pos;
	};
	ovr::uint32 triangle_num = 12;
	ovr::uint32 vertex_num = 24;
	Vertex* vertex_buffers = new Vertex[vertex_num];
	//front plane
	vertex_buffers[0].pos = OvrVector3(-a_size, -a_size, a_size);
	vertex_buffers[1].pos = OvrVector3(a_size, -a_size, a_size);
	vertex_buffers[2].pos = OvrVector3(a_size, a_size, a_size);
	vertex_buffers[3].pos = OvrVector3(-a_size, a_size, a_size);

	//back plane
	vertex_buffers[4].pos = OvrVector3(-a_size, -a_size, -a_size);
	vertex_buffers[5].pos = OvrVector3(a_size, -a_size, -a_size);
	vertex_buffers[6].pos = OvrVector3(a_size, a_size, -a_size);
	vertex_buffers[7].pos = OvrVector3(-a_size, a_size, -a_size);

	//left plane
	vertex_buffers[8].pos = OvrVector3(-a_size, -a_size, a_size);
	vertex_buffers[9].pos = OvrVector3(-a_size, -a_size, -a_size);
	vertex_buffers[10].pos = OvrVector3(-a_size, a_size, -a_size);
	vertex_buffers[11].pos = OvrVector3(-a_size, a_size, a_size);

	//right plane
	vertex_buffers[12].pos = OvrVector3(a_size, -a_size, a_size);
	vertex_buffers[13].pos = OvrVector3(a_size, -a_size, -a_size);
	vertex_buffers[14].pos = OvrVector3(a_size, a_size, -a_size);
	vertex_buffers[15].pos = OvrVector3(a_size, a_size, a_size);

	//top plane
	vertex_buffers[16].pos = OvrVector3(-a_size, a_size, a_size);
	vertex_buffers[17].pos = OvrVector3(a_size, a_size, a_size);
	vertex_buffers[18].pos = OvrVector3(a_size, a_size, -a_size);
	vertex_buffers[19].pos = OvrVector3(-a_size, a_size, -a_size);

	//buttom plane
	vertex_buffers[20].pos = OvrVector3(-a_size, -a_size, a_size);
	vertex_buffers[21].pos = OvrVector3(a_size, -a_size, a_size);
	vertex_buffers[22].pos = OvrVector3(a_size, -a_size, -a_size);
	vertex_buffers[23].pos = OvrVector3(-a_size, -a_size, -a_size);

	ovr::uint32* indice = new ovr::uint32[3 * triangle_num];
	OvrTriangleImpl* t_tris = (OvrTriangleImpl*)indice;
	t_tris[0] = OvrTriangleImpl(0, 2, 1);
	t_tris[1] = OvrTriangleImpl(0, 3, 2);

	t_tris[2] = OvrTriangleImpl(4, 5, 6);
	t_tris[3] = OvrTriangleImpl(4, 6, 7);

	t_tris[4] = OvrTriangleImpl(8, 9, 11);
	t_tris[5] = OvrTriangleImpl(9, 10, 11);

	t_tris[6] = OvrTriangleImpl(12, 15, 13);
	t_tris[7] = OvrTriangleImpl(15, 14, 13);

	t_tris[8] = OvrTriangleImpl(16, 19, 17);
	t_tris[9] = OvrTriangleImpl(17, 19, 18);

	t_tris[10] = OvrTriangleImpl(20, 21, 23);
	t_tris[11] = OvrTriangleImpl(21, 22, 23);

	vertex_data->declaration->AddElement(0, VET_FLOAT3, VES_POSITION);
	vertex_data->vertex_buffer = OvrHardwareBufferManager::GetIns()->CreateVertexBuffer(12, vertex_num, vertex_buffers);
	index_data->index_buffer = OvrHardwareBufferManager::GetIns()->CreateIndexBuffer(triangle_num * 3, indice);
	
	size = a_size;

	delete[] indice;
	delete[] vertex_buffers;
}
