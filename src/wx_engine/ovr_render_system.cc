#include "wx_engine/ovr_render_system.h"
using namespace ovr_engine;

ovr_engine::OvrRenderSystem::OvrRenderSystem()
	:program(nullptr)
{

}




ovr_engine::OvrRenderSystem::~OvrRenderSystem()
{

}

void ovr_engine::OvrRenderSystem::BindGpuProgram(OvrGpuProgram* a_program)
{
	program = a_program;
}

void ovr_engine::OvrRenderSystem::SetViewPort(const Viewport& a_v)
{
	view_port = a_v;
}

const OvrRenderSystem::Viewport& ovr_engine::OvrRenderSystem::GetViewPort()
{
	return view_port;
}
