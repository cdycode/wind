#include <math.h>
#include "wx_engine/ovr_particle_emitter_sphere.h"
#include "wx_engine/ovr_particle_class_info.h"
using namespace ovr_engine;
using namespace ovr;

REGISTER_CLASS(OvrParticleEmitterSphere)
OvrParticleEmitterSphere::OvrParticleEmitterSphere()
	:OvrParticleEmitter()
	,radius(0)
{

}

ovr_engine::OvrParticleEmitterSphere::OvrParticleEmitterSphere(float a_radius)
	:OvrParticleEmitter()
	,radius(a_radius)
{

}

OvrParticleEmitterSphere::~OvrParticleEmitterSphere()
{

}

void OvrParticleEmitterSphere::EmitterParticle(OvrParticle* ao_particle)
{
	OvrParticle& t_p = *ao_particle;
	float t_w = cosf(rad);
	float t_h = sinf(rad);
	OvrVector3 init_dir = OvrVector3(OvrRandom(-t_w, t_w), OvrRandom(t_h, 1.0f), OvrRandom(-t_w, t_w)).get_normalize();
	t_p.cur_v = init_dir*OvrRandom(min_speed, max_speed);
	t_p.position = OvrVector3(OvrRandom(-radius, radius), 0, OvrRandom(-radius, radius));
	t_p.life_total = OvrRandom(min_life, max_life);
	t_p.scale_size = OvrRandom(min_size, max_size);
}
