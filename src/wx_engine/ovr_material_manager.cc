#include "wx_engine/ovr_material_manager.h"
#include "wx_engine/ovr_serializer_impl.h"
#include "wx_engine/ovr_material.h"
#include "wx_engine/ovr_texture_manager.h"
#include "wx_base/wx_log.h"
using namespace ovr_engine;
template<> OvrMaterialManager* ovr_engine::OvrSingleton<OvrMaterialManager>::s_instance = nullptr;
OvrMaterial* ovr_engine::OvrMaterialManager::Load(const OvrString& a_file_name)
{
	OvrMaterial* material = (OvrMaterial*)CreateOrRetrieve(a_file_name, false);
	material->Load();
	return material;
}

OvrMaterial* ovr_engine::OvrMaterialManager::Create(const OvrString& a_name)
{
	OvrMaterial* material = (OvrMaterial*)CreateOrRetrieve(a_name, true);
	return material;
}

OvrMaterial* ovr_engine::OvrMaterialManager::GetDefaultMaterial(bool is_light /*= true*/)
{
	if (is_light)
	{
		return (OvrMaterial*)GetByName("BaseWhite");
	}
	return (OvrMaterial*)GetByName("BaseWhiteNoLight");
}

OvrResource* ovr_engine::OvrMaterialManager::CreateResource(OvrString a_name, bool is_manual)
{
	return new OvrMaterial(a_name, is_manual);
}

bool ovr_engine::OvrMaterialManager::Initialise()
{
	OvrMaterial* mat = Create("BaseWhite");
	mat->SetProgram("Standard");
	mat->SetMainTexture(OvrTextureManager::GetIns()->GetDefaultTexture());


	mat = Create("BaseWhiteNoLight");
	mat->SetProgram("StandardNoLight");
	mat->SetMainTexture(OvrTextureManager::GetIns()->GetDefaultTexture());
	return true;
}

void ovr_engine::OvrMaterialManager::UnInitialise()
{
	RemoveAll();
}

ovr_engine::OvrMaterialManager::OvrMaterialManager()
{

}

ovr_engine::OvrMaterialManager::~OvrMaterialManager()
{
	UnInitialise();
}