#include "wx_engine/ovr_media_texture.h"
#include "ovr_gl_define.h"
namespace ovr_engine {

	OvrGLMediaTexture::OvrGLMediaTexture(OvrString a_name)
		:OvrTexture(a_name, true)
	{
		texture_id = 0;
	}

	OvrGLMediaTexture::~OvrGLMediaTexture() 
	{
		FreeInternalResources();
	}

	void OvrGLMediaTexture::CreateInternalResources()
	{
		if (is_internal_created)
			return;

		glGenTextures(1, &texture_id);
		glBindTexture(GL_TEXTURE_2D, texture_id);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		return;
	}
	void OvrGLMediaTexture::FreeInternalResources() 
	{
		if (texture_id != GL_INVALID_VALUE)
		{
			glDeleteTextures(1, &texture_id);
			texture_id = GL_INVALID_VALUE;
		}
	}

	void OvrGLMediaTexture::UpdateTexture(ovr::int8* a_raw_data)
	{
		//LOGI("================= %llu %d  %d", need_time, tRawFrame.m_iBufferSize, tRawFrame.m_iDataSize);
		glBindTexture(GL_TEXTURE_2D, texture_id);

		//glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, mVideoWidth, mVideoHeight, glInternalFormat, GL_UNSIGNED_BYTE, tRawFrame.m_ucBuffer);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, a_raw_data);

		glBindTexture(GL_TEXTURE_2D, 0);
	}

	unsigned int OvrGLMediaTexture::GetTextureId()
	{
		return texture_id;
	}
}//end namespace