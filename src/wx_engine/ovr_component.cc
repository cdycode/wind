#include "wx_engine/ovr_component.h"
#include "wx_engine/ovr_actor.h"
#include "wx_engine/ovr_material.h"
using namespace ovr_engine;

ovr_engine::OvrComponent::OvrComponent(OvrActor* a_actor)
	: parent_actor(a_actor)
	, is_active(true)
{

}

ovr_engine::OvrComponent::~OvrComponent()
{

}

OvrActor* ovr_engine::OvrComponent::GetParentActor()
{
	return parent_actor;
}

void ovr_engine::OvrComponent::SetActive(bool a_active)
{
	is_active = a_active;
}

bool ovr_engine::OvrComponent::IsRealActive()
{
	return is_active && parent_actor->IsVisible();
}

OvrMatrix4 ovr_engine::OvrComponent::GetWorldTransform()
{
	return parent_actor->GetWorldTransform();
}

