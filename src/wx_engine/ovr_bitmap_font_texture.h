#ifndef __ovr_bitmap_font_texture_h__
#define __ovr_bitmap_font_texture_h__

#include "ovr_bitmap_font_configure.h"
#include "ovr_gl_texture.h"

#include <map>
class space_control;
class OvrFontGlyphInfo
{
public:
	OvrFontGlyphInfo()
		:char_code(0),
		x(0.0f),
		y(0.0f),
		width(0.0f),
		height(0.0f),
		advance_x(0.0f),
		advance_y(0.0f),
		bearing_x(0.0f),
		bearing_y(0.0f)
	{}
public:
	int32_t		char_code;
	float		x;
	float		y;
	float		width;
	float		height;
	float		advance_x;
	float		advance_y;
	float		bearing_x;
	float		bearing_y;
};
namespace ovr_engine {
	class OvrBitmapFontTexture :public OvrGLTexture
	{
	public:
		OvrBitmapFontTexture();
		~OvrBitmapFontTexture();
	public:
		void init(int a_w, int a_h, int a_char_size);
		void bind(int);
		OvrFontGlyphInfo*	get_font_glyph_info(int32_t a_char_code);
		OvrFontGlyphInfo*	put_glyph(OvrFontGlyph* a_font_glyph);
		void release();
	public:
		std::map<int32_t, OvrFontGlyphInfo*>  _page;
		int				_width, _height;
		int				_char_size;
		space_control*	_sc;
		float			_half_word_advance_x;
	};
}
#endif
