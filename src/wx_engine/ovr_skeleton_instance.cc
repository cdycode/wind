#include "wx_engine/ovr_skeleton_instance.h"
using namespace ovr_engine;

ovr_engine::OvrSkeletonInstance::OvrSkeletonInstance(OvrSkeleton* a_src)
	: OvrSkeleton(a_src->GetName(), a_src->isManuallyLoaded())
	, src_skeleton(a_src)
{
	a_src->Grab();

	name = a_src->GetName();
	ovr::uint32 num = a_src->GetJointNum();
	if (num <= 0)
		return;

	skeleton_matrix = new OvrMatrix4[num];
	memcpy(skeleton_matrix, a_src->GetSkeletonMatrix(), num * sizeof(OvrMatrix4));

	joint_array.resize(num);
	for (ovr::uint32 i = 0; i < num; i++)
	{
		OvrJoint* src_joint = a_src->GetJoint(i);
		joint_array[i].name = src_joint->name;
		joint_array[i].index = src_joint->index;
		joint_array[i].vertex_to_local_matrix = src_joint->vertex_to_local_matrix;
		joint_array[i].local_to_parent_matrix = src_joint->local_to_parent_matrix;
		joint_array[i].bind_pose = src_joint->bind_pose;
		joint_array[i].local_matrix = src_joint->local_matrix;
		joint_array[i].final_matrix = &skeleton_matrix[i];
		joint_array[i].parent = nullptr;
		if (src_joint->parent)
		{
			joint_array[i].parent = &joint_array[src_joint->parent->index];
		}

		std::vector<OvrJoint*>& src_child_list = src_joint->child_list;
		for (ovr::uint32 j = 0; j < src_child_list.size(); j++)
		{
			joint_array[i].child_list.push_back(&joint_array[src_child_list[j]->index]);
		}
	}

}

ovr_engine::OvrSkeletonInstance::~OvrSkeletonInstance()
{
	src_skeleton->Drop();
}

void ovr_engine::OvrSkeletonInstance::UpdateAnimation(ovr_asset::OvrMotionFrame* a_frame)
{
	for (ovr::uint32 i = 0; i < a_frame->GetJointNum(); i++)
	{
		ovr_asset::OvrJointFrame* joint_frame = a_frame->GetJointAnimationFrame(i);
		OvrJoint* joint = GetJoint(joint_frame->joint_index);
		if (nullptr != joint)
		{
			OvrMatrix4 local_matrix;
			if (!joint_frame->is_materix)
			{
				OvrMatrix4 pos_matrix, rot_matrix;
				rot_matrix.from_quaternion(joint_frame->joint_quaternion);
				pos_matrix = OvrMatrix4::make_translation_matrix(joint_frame->joint_positon);
				local_matrix = rot_matrix * pos_matrix * joint->local_to_parent_matrix;
			}
			else
			{
				local_matrix = joint_frame->joint_materix;
			}		
			joint->SetLocalMatrix(local_matrix);
		}
	}
	UpdateMatrix();
}
