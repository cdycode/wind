﻿#include "ovr_gl_hardware_buffer_manager.h"
#include "ovr_gl_vertex_array_object.h"
#include "ovr_gl_hardware_buffer.h"
using namespace ovr_engine;

ovr_engine::OvrGLHardwareBufferManager::OvrGLHardwareBufferManager()
	:OvrHardwareBufferManager()
{

}

OvrVertexDeclaration* ovr_engine::OvrGLHardwareBufferManager::CreateVertexDeclaration()
{
	return new OvrGLVertexArrayObject();
}

OvrHardwareVertexBuffer* ovr_engine::OvrGLHardwareBufferManager::CreateVertexBuffer(ovr::uint32 a_vertex_size, ovr::uint32 a_vcount, void* a_vdata,  bool a_static_draw)
{
	return new OvrGLHardwareVertexBuffer(a_vertex_size, a_vcount, a_vdata,a_static_draw);
}

OvrHardwareIndexBuffer* ovr_engine::OvrGLHardwareBufferManager::CreateIndexBuffer(ovr::uint32 a_icount, void* a_idata)
{
	return new OvrGLHardwareIndexBuffer(a_icount, a_idata);

}
