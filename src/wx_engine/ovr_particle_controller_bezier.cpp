#include "wx_engine/ovr_particle_controller_bezier.h"
using namespace ovr_engine;
using namespace  ovr;
OvrTimeLine::OvrTimeLine()
{

}

OvrTimeLine::~OvrTimeLine()
{

}

bool OvrTimeLine::GetTimePos(float time_now, int& ao_segment, float& ao_pos)
{
	for (int i = 1; i < segment_mark_num; ++i)
	{
		if (time_now <= time_marks[i])
		{
			ao_segment = i - 1;
			ao_pos = (time_marks[i] - time_now) / (time_marks[i] - time_marks[i - 1]);
			Ovrclamp(ao_pos, 0.0f, 1.0f);
			return true;
		}
	}
	return false;
}

bool OvrTimeLine::GetTimePosWithLastSegment(float time_now, int& ao_segment, float& ao_pos)
{
	for (int i = ao_segment + 1; i < segment_mark_num; ++i)
	{
		if (time_now <= time_marks[i])
		{
			ao_segment = i - 1;
			ao_pos = (time_marks[i] - time_now) / (time_marks[i] - time_marks[i - 1]);
			Ovrclamp(ao_pos, 0.0f, 1.0f);
			return true;
		}
	}
	return false;
}

void OvrTimeLine::AddTimeMark(float time)
{
	time_marks.push_back(time);
	segment_mark_num++;
}

void OvrTimeLine::ModifyTimeMark(int id, float time)
{
	if (id < segment_mark_num)
	{
		time_marks[id] = time;
	}
}

OvrParticleControllerBezier::OvrParticleControllerBezier()
{

}

OvrParticleControllerBezier::~OvrParticleControllerBezier()
{

}

bool OvrParticleControllerBezier::update(OvrParticle* a_particle, float a_delta_time)
{
	int t_segment = 0;
	float t_pos = 0.0;
	if (_time_line.GetTimePos(a_particle->life_now, t_segment, t_pos))
	{
		switch (type)
		{
		case OvrParticleController::e_control_pos:
			if (_bezier_curve.GetPointTanget(t_segment, t_pos, a_particle->position, a_particle->direction))
			{
				a_particle->position += a_particle->offset_pos;
				return true;
			}
			break;
		case OvrParticleController::e_control_rotation:
			break;
		case OvrParticleController::e_control_scale:
			break;
		case OvrParticleController::e_control_color:
			break;
		case OvrParticleController::e_control_any:
			break;
		default:
			break;
		}
	}
	return false;
}

