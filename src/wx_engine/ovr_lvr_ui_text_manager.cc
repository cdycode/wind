#include "ovr_lvr_ui_text_manager.h"
//#include "lvr_bitmap_font_manager.h"
//#include "lvr_bitmap_font_lib.h"
//#include "lvr_ui_manager.h"
//#include "lvr_shader_manager.h"
#include "wx_engine/ovr_logger.h"
#include "wx_engine/ovr_text_component.h"
#include "ovr_bitmap_font_manager.h"
void warp_text(std::string& ao_res, const char* a_text, float a_font_spaing, float a_scale, float a_line_to_warp, float a_warp_width)
{
	static float s_single_word_width = 0;
	if (s_single_word_width < 0.001)
	{
		s_single_word_width = OvrGetBitmapFontManager()->get_advance_x(22120);
	}

	char* p = (char*)a_text;
	float pixels_len = 0;
	int t_cur_line = 1;
	bool t_need_to_deal = false;
	float real_single_word_width = (s_single_word_width + a_font_spaing)* a_scale;
	ovr::uint32 t_last_code = 0;
	char* deal_pos = p;
	for (ovr::uint32 charCode = lvr_decode_next_char2((const char**)(&p)); charCode != '\0'; charCode = lvr_decode_next_char2((const char**)(&p)))
	{
		if ('\n' == charCode || '\r' == charCode)
		{
			if ('\r' == t_last_code)
			{
				t_last_code = charCode;
				continue;
			}
			t_last_code = charCode;
			if (t_cur_line == a_line_to_warp && pixels_len < a_warp_width - real_single_word_width*3.0f)
			{
				deal_pos = p - 1;
				t_need_to_deal = true;
			}
			pixels_len = 0;
			t_cur_line++;
			continue;
		}
		float t_add_len = real_single_word_width;
		if (charCode < 255)
		{
			t_add_len *= 0.5f;
		}
		pixels_len += t_add_len;
		if (t_cur_line > a_line_to_warp)
		{
			t_need_to_deal = true;
		}
		if (t_cur_line == a_line_to_warp)
		{
			if (pixels_len < a_warp_width - real_single_word_width*3.0f)
			{
				deal_pos = p;
			}
		}
		if (pixels_len > a_warp_width)//for multi lines case
		{
			t_cur_line++;
			if (t_cur_line > a_line_to_warp)
			{
				t_need_to_deal = true;
			}
			pixels_len = t_add_len;
		}
		t_last_code = charCode;
	}
	if (t_need_to_deal)
	{
		int tpos = deal_pos - a_text;
		char* tmpstr = new char[tpos + 4];
		memcpy(tmpstr, a_text, tpos);
		tmpstr[tpos] = '.';
		tmpstr[tpos + 1] = '.';
		tmpstr[tpos + 2] = '.';
		tmpstr[tpos + 3] = '\0';
		ao_res = tmpstr;
		delete[] tmpstr;
	}
	else
	{
		ao_res = a_text;
	}
}
using namespace ovr_engine;
OvrLvrUiTextManager::OvrLvrUiTextManager() 
{

}

OvrLvrUiTextManager::~OvrLvrUiTextManager()
{

}


void OvrLvrUiTextManager::Initialise(const OvrString& font_path, bool a_inside)
{
	//lvr_get_bitmap_font_manager()->init(font_path.GetStringConst(), a_inside);
	OvrGetBitmapFontManager()->init(font_path.GetStringConst(), a_inside);
	//lvr_shader_manager::get_shader_mgr()->init_inside_programs();
	//  shader manager has been initialized;
	//ui_mgr = new lvr_ui_manager;
	//ui_mgr->init(128);
	//ui_menu = new lvr_ui_menu;
	//ui_mgr->add_menu(ui_menu);
}

void OvrLvrUiTextManager::UnInitialise()
{
	/*if (ui_mgr)
	{
		ui_mgr->uninit();
		delete ui_mgr;
		ui_mgr = nullptr;
	}*/
}

void OvrLvrUiTextManager::AddText(const OvrTextInfo& a_text)
{
	texts.push_back(a_text);
}

void OvrLvrUiTextManager::RemoveAllText()
{
	for (uint32_t i = 0; i < texts.size(); ++i)
	{
		OvrGetBitmapFontManager()->remove_render_string(i);
	}
	texts.clear();
	//ui_menu->release_all();
}

void OvrLvrUiTextManager::Update(const OvrVector3& a_pos, const OvrVector3& a_dir, double a_time)
{
	int  text_count = texts.size();
	for (int i = 0; i < text_count; ++i)
	{
		OvrTextInfo tmpInfo = texts[i];
		unsigned int real_align = 1;
		if (tmpInfo.align_method == 1) // left
		{
			real_align = 0;
		}
		else if (tmpInfo.align_method == 2) //middle
		{
			real_align = 1;
		}
		else if (tmpInfo.align_method == 4)//up
		{
			real_align = 2;
		}
		else if (tmpInfo.align_method == (OvrAlignMethod::eTextAlignLeft | OvrAlignMethod::eTextAlignUp))
		{
			real_align = 0;
		}
		else if (tmpInfo.align_method == (OvrAlignMethod::eTextAlignLeft | OvrAlignMethod::eTextAlignMiddle))
		{
			real_align = 0;
		}
		else if (tmpInfo.align_method == (OvrAlignMethod::eTextAlignLeft | OvrAlignMethod::eTextAlignDown))
		{
			real_align = 0;
		}
		else if (tmpInfo.align_method == (OvrAlignMethod::eTextAlignMiddle | OvrAlignMethod::eTextAlignUp))
		{
			real_align = 1;
		}
		else if (tmpInfo.align_method == (OvrAlignMethod::eTextAlignMiddle | OvrAlignMethod::eTextAlignMiddle))
		{
			real_align = 1;
		}
		else if (tmpInfo.align_method == (OvrAlignMethod::eTextAlignMiddle | OvrAlignMethod::eTextAlignDown))
		{
			real_align = 1;
		}
		else if (tmpInfo.align_method == (OvrAlignMethod::eTextAlignRight | OvrAlignMethod::eTextAlignUp))
		{
			real_align = 2;
		}
		else if (tmpInfo.align_method == (OvrAlignMethod::eTextAlignRight | OvrAlignMethod::eTextAlignMiddle))
		{
			real_align = 2;
		}
		else if (tmpInfo.align_method == (OvrAlignMethod::eTextAlignRight | OvrAlignMethod::eTextAlignDown))
		{
			real_align = 2;
		}

		//ui_text->set_text_align((lvr_enum_font_align_method)real_align);

		//ui_text->set_position(lvr_vector3f(tmpInfo.pos._x, tmpInfo.pos._y, tmpInfo.pos._z));
		//ui_text->set_text(tmpInfo.text.GetStringConst());
		//fix the real pos with x_margin and y_margin;
		float text_width = -1;
		float text_height = -1;
		//int ui_id = ui_text->get_text_id();
	//	lvr_get_bitmap_font_manager()->query_render_string_size(ui_id, text_width, text_height);
		if (text_width > TEXT_BACKGROUND_X_SIZE || text_height > TEXT_BACKGROUND_Y_SIZE)
		{
			OvrLogger::GetIns()->Log("The background size doesn't match the text.\n",ovr_engine::LL_WARNING);
		}

		int a_id = ProcessText(i);
		if (a_id >= 0)
		{
			OvrGetBitmapFontManager()->query_render_string_size(a_id, text_width, text_height);
		}
		
		texts[i].bitmap_font_id = a_id;
		switch (tmpInfo.align_method)
		{
		case eTextAlignLeft:
			tmpInfo.pos += tmpInfo.right * TEXT_BACKGROUND_X_SIZE * tmpInfo.background_x_scale * 0.5 *-1;
			tmpInfo.pos += tmpInfo.right*tmpInfo.x_margin;
			//tmpInfo.pos += tmpInfo.up*tmpInfo.y_margin*-1;
			break;
		case  eTextAlignMiddle:
			tmpInfo.pos;
			break;
		case eTextAlignRight:
			tmpInfo.pos += tmpInfo.right * TEXT_BACKGROUND_X_SIZE * tmpInfo.background_x_scale * 0.5f ;
			tmpInfo.pos += tmpInfo.right*tmpInfo.x_margin*-1.0f;
			//tmpInfo.pos += tmpInfo.up*tmpInfo.y_margin*-1;
			break;
		case eTextAlignUp:
			tmpInfo.pos += tmpInfo.up *(TEXT_BACKGROUND_Y_SIZE *tmpInfo.background_y_scale * 0.5f - text_height * 0.5f);
			//tmpInfo.pos += tmpInfo.right*tmpInfo.x_margin;
			tmpInfo.pos += tmpInfo.up*tmpInfo.y_margin*-1;
			break;
		case eTextAlignDown:
			tmpInfo.pos += tmpInfo.up *(-TEXT_BACKGROUND_Y_SIZE *tmpInfo.background_y_scale * 0.5f + text_height * 0.5f);
			//tmpInfo.pos += tmpInfo.right*tmpInfo.x_margin;
			tmpInfo.pos += tmpInfo.up*tmpInfo.y_margin * 1;
			break;
		case eTextAlignLeft | eTextAlignUp:
			tmpInfo.pos += tmpInfo.right * TEXT_BACKGROUND_X_SIZE * tmpInfo.background_x_scale * 0.5f *-1.0f;
			tmpInfo.pos += tmpInfo.up *(TEXT_BACKGROUND_Y_SIZE *tmpInfo.background_y_scale * 0.5f - text_height * 0.5f);
			tmpInfo.pos += tmpInfo.right*tmpInfo.x_margin;
			tmpInfo.pos += tmpInfo.up*-tmpInfo.y_margin;
			break;
		case eTextAlignMiddle | eTextAlignUp:
			tmpInfo.pos += tmpInfo.up *(TEXT_BACKGROUND_Y_SIZE *tmpInfo.background_y_scale * 0.5f - text_height * 0.5f);
			//tmpInfo.pos += tmpInfo.right*tmpInfo.x_margin;
			tmpInfo.pos += tmpInfo.up*-tmpInfo.y_margin;
			break;
		case eTextAlignRight | eTextAlignUp:
			tmpInfo.pos += tmpInfo.right * TEXT_BACKGROUND_X_SIZE * tmpInfo.background_x_scale * 0.5f * 1.0f;
			tmpInfo.pos += tmpInfo.up *(TEXT_BACKGROUND_Y_SIZE *tmpInfo.background_y_scale * 0.5f - text_height * 0.5f);
			tmpInfo.pos += tmpInfo.right*-tmpInfo.x_margin;
			tmpInfo.pos += tmpInfo.up*-tmpInfo.y_margin;
			break;
		case eTextAlignLeft | eTextAlignDown:
			tmpInfo.pos += tmpInfo.right * TEXT_BACKGROUND_X_SIZE * tmpInfo.background_x_scale * 0.5f *-1.0f;
			tmpInfo.pos += tmpInfo.up *(-TEXT_BACKGROUND_Y_SIZE *tmpInfo.background_y_scale * 0.5f + text_height * 0.5f);
			tmpInfo.pos += tmpInfo.right*tmpInfo.x_margin;
			tmpInfo.pos += tmpInfo.up*tmpInfo.y_margin;
			break;
		case eTextAlignMiddle | eTextAlignDown:
			tmpInfo.pos += tmpInfo.up *(-TEXT_BACKGROUND_Y_SIZE *tmpInfo.background_y_scale * 0.5f + text_height * 0.5f);
			//tmpInfo.pos += tmpInfo.right*-tmpInfo.x_margin;
			tmpInfo.pos += tmpInfo.up*tmpInfo.y_margin;
			break;
		case eTextAlignRight | eTextAlignDown:
			tmpInfo.pos += tmpInfo.right * TEXT_BACKGROUND_X_SIZE * tmpInfo.background_x_scale * 0.5f * 1.0f;
			tmpInfo.pos += tmpInfo.up *(-TEXT_BACKGROUND_Y_SIZE *tmpInfo.background_y_scale * 0.5f + text_height * 0.5f);
			tmpInfo.pos += tmpInfo.right*-tmpInfo.x_margin;
			tmpInfo.pos += tmpInfo.up*tmpInfo.y_margin;
			break;
		case eTextAlignLeft | eTextAlignMiddle:
			tmpInfo.pos += tmpInfo.right * TEXT_BACKGROUND_X_SIZE * tmpInfo.background_x_scale * 0.5f *-1.0f;
			tmpInfo.pos += tmpInfo.right*tmpInfo.x_margin;
			//tmpInfo.pos += tmpInfo.up*-tmpInfo.y_margin;
			break;
		case eTextAlignRight | eTextAlignMiddle:
			tmpInfo.pos += tmpInfo.right * TEXT_BACKGROUND_X_SIZE * tmpInfo.background_x_scale * 0.5f * 1.0f;
			tmpInfo.pos += tmpInfo.right*-tmpInfo.x_margin;
			//tmpInfo.pos += tmpInfo.up*-tmpInfo.y_margin;
			break;
		default:
			break;
		}

		//ui_text->set_position(lvr_vector3f(tmpInfo.pos._x, tmpInfo.pos._y, tmpInfo.pos._z));
	//	ui_text->set_text(tmpInfo.text.GetStringConst());

		ovr::uint8 colors[4];
		for (int j = 0; j < 4; ++j)
		{
			colors[j] = (ovr::uint8 )(tmpInfo.color[j] * 255);
		}
		OvrGetBitmapFontManager()->set_render_string_position(a_id,tmpInfo.pos);
		OvrGetBitmapFontManager()->set_render_string_color(a_id,colors);
		OvrGetBitmapFontManager()->set_render_string_algin(a_id, (OvrEnumFontAlignMethod)real_align);
		OvrGetBitmapFontManager()->set_warp_width(a_id, TEXT_BACKGROUND_X_SIZE - 0.1f);
		OvrGetBitmapFontManager()->submit_for_render(a_id);
		//ui_text->update();
		//ui_menu->add_text(ui_text);
	}
	//char* new_buffer = new char[1000];
	//if (ui_mgr)
	{
		//double cTime = lvr_time::get_seconds();
		//ui_mgr->update(lvr_vector3f(a_pos._x, a_pos._y, a_pos._z), lvr_vector3f(a_dir._x, a_dir._y, a_dir._z), cTime);
		
		//int a_size;
		//lvr_get_bitmap_font_manager()->query_render_text_buffer(1, a_size, new_buffer);
		//delete[] new_buffer;
	}
	OvrGetBitmapFontManager()->update();
}

void OvrLvrUiTextManager::Draw(const OvrMatrix4& a_vp)
{
	glDisable(GL_CULL_FACE);
	//glEnable(GL_CULL_FACE);
	/*glClearColor(0.1, 0.1, 0.1, 0);
	glClearDepth(1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);*/
	//lvr_matrix4f tmpMat;
	for (int i = 0; i < 16; ++i)
	{
		//tmpMat[i] = a_vp[i];
	}
	//if (ui_mgr)
	{
		//ui_mgr->draw(tmpMat);
	}
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(1.0f, -20.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	OvrGetBitmapFontManager()->draw(a_vp);

}

void ovr_engine::OvrLvrUiTextManager::QueryTextId(OvrTextInfo* a_info)
{
	for (int i = 0; i < texts.size(); ++i)
	{
		bool a_bool = (a_info->pos == texts[i].pos) && (a_info->text == texts[i].text);
		if (a_bool)
		{
			a_info->bitmap_font_id = texts[i].bitmap_font_id;
		}
	}
}

int ovr_engine::OvrLvrUiTextManager::ProcessText(int a_text)
{
	OvrTextInfo a_info =  texts[a_text];
	float scale = a_info.scale;
	OvrVector3 position = a_info.pos;
	OvrVector3 right_vec = a_info.right;
	OvrVector3 up_vec = a_info.up;
	float line_spacing = a_info.line_spacing;
	float font_spacing = a_info.font_spacing;
	std::string a_string;
	warp_text(a_string, (a_info.text.GetStringConst()), font_spacing, scale, 10, TEXT_BACKGROUND_X_SIZE - 0.1f);
	return OvrGetBitmapFontManager()->add_render_string(a_string.c_str(),scale,position,right_vec,up_vec,line_spacing,font_spacing);
}

