﻿#include <wx_plugin/wx_i_av_media.h>
#include <wx_plugin/wx_plugin_manager.h>

namespace wx_plugin
{
	IOvrAVMedia* IOvrAVMedia::Load() 
	{
		return dynamic_cast<IOvrAVMedia*>(wxPluginManager::GetIns()->Load("wxAVMedia"));
	}

	void IOvrAVMedia::Unload() 
	{
		wxPluginManager::GetIns()->Unload("wxAVMedia");
	}
}