﻿#include <wx_plugin/wx_i_sound.h>
#include <wx_plugin/wx_plugin_manager.h>

namespace wx_plugin 
{
	wxISoundManager* wxISoundManager::Load() 
	{
		return dynamic_cast<wxISoundManager*>(wxPluginManager::GetIns()->Load("wxFmod"));
	}

	void wxISoundManager::Unload() 
	{
		wxPluginManager::GetIns()->Unload("wxFmod");
	}
}
