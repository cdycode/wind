#include <wx_plugin/wx_i_archive_project.h>
#include <wx_plugin/wx_plugin_manager.h>

namespace wx_plugin 
{
	wxIArchiveProject* wxIArchiveProject::Load()
	{
		return dynamic_cast<wxIArchiveProject*>(wxPluginManager::GetIns()->Load("wxArchiveProjectPB"));
	}

	void wxIArchiveProject::Unload()
	{
		wxPluginManager::GetIns()->Unload("wxArchiveProjectPB");
	}
}