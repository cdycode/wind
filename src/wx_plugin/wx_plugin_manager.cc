﻿#include <wx_base/wx_dll_loader.h>
#include <wx_plugin/wx_i_av_media.h>
#include <wx_plugin/wx_plugin_manager.h>
#include <wx_plugin/wx_i_sound.h>
#include <wx_plugin/wx_i_archive_project.h>

namespace wx_plugin
{
	//OvrVisualNode的初始化和反初始化函数
	typedef wxIPlugin* (*StartPluginFunc)();
	typedef void (*StopPluginFunc)();

	static wxPluginManager* sg_plugin_manager = nullptr;
	wxPluginManager* wxPluginManager::GetIns() 
	{
		if (nullptr != sg_plugin_manager)
		{
			return sg_plugin_manager;
		}
		sg_plugin_manager = new wxPluginManager;
		return sg_plugin_manager;
	}

	void wxPluginManager::DelIns() 
	{
		if (nullptr != sg_plugin_manager)
		{
			delete sg_plugin_manager;
			sg_plugin_manager = nullptr;
		}
	}

	wxPluginManager::wxPluginManager() 
	{
	}
	wxPluginManager::~wxPluginManager() 
	{
	}

	void wxPluginManager::LoadAll()
	{
		IOvrAVMedia::Load();
		wxISoundManager::Load();
		wxIArchiveProject::Load();
	}

	void wxPluginManager::UnloadAll()
	{
		for (auto it = dll_loader_map.begin(); it != dll_loader_map.end(); it++)
		{
			UnloadDll(it->second);
		}
		dll_loader_map.clear();
	}

	wxIPlugin*	wxPluginManager::Load(const OvrString& a_plugin_name) 
	{
		auto it = dll_loader_map.find(a_plugin_name);
		if (it == dll_loader_map.end())
		{
			//还未加载
			return LoadDll(a_plugin_name);
		}
		
		//已经加载
		return it->second.plugin_ins;
	}

	void wxPluginManager::Unload(const OvrString& a_plugin_name) 
	{
		auto it = dll_loader_map.find(a_plugin_name);
		if (it == dll_loader_map.end()) 
		{
			return;
		}
		UnloadDll(it->second);
		dll_loader_map.erase(it);
	}


	wxIPlugin* wxPluginManager::LoadDll(const OvrString& a_plugin_name)
	{
		OvrDllLoader*	loader = new OvrDllLoader;
		if (nullptr == loader)
		{
			return nullptr;
		}
		OvrString dll_name = a_plugin_name;
#ifdef _WIN32
#	ifdef _DEBUG
		dll_name.AppendString("_x86_d.dll");
#	else
		dll_name.AppendString("_x86.dll");
#	endif
#else
		dll_name = "lib" + a_plugin_name + ".so";
#endif // _WIN32

		if (loader->Load(dll_name)) 
		{
			StartPluginFunc start_func = (StartPluginFunc)loader->GetFunctionAddress("StartPlugin");
			if (nullptr != start_func)
			{
				//调用初始化函数
				wxIPlugin* plugin_ins = (*start_func)();
				if (nullptr != plugin_ins)
				{
					plugin_ins->Init();
					dll_loader_map.insert(std::make_pair(a_plugin_name, wxPluginInfo(loader, plugin_ins)));
					return plugin_ins;
				}
			}
		}

		delete loader;
		return nullptr;
	}

	void wxPluginManager::UnloadDll(wxPluginInfo& a_plugin_info)
	{
		if (nullptr != a_plugin_info.plugin_ins)
		{
			a_plugin_info.plugin_ins->Uninit();
			a_plugin_info.plugin_ins = nullptr;
		}
		if (nullptr != a_plugin_info.dll_loader)
		{
			//调用反初始化函数
			StopPluginFunc stop_func = (StopPluginFunc)a_plugin_info.dll_loader->GetFunctionAddress("StopPlugin");
			if (nullptr != stop_func)
			{
				(*stop_func)();
			}
			//调用卸载函数
			a_plugin_info.dll_loader->Unload();
			delete a_plugin_info.dll_loader;
			a_plugin_info.dll_loader = nullptr;
		}
	}
}