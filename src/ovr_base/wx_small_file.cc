﻿#include "wx_small_file.h"
#include "wx_file_tools.h"
#include "wx_zip.h"
bool OvrSmallFile::Open(const OvrString& a_file_path) 
{
	Close();
	
	FILE* file_handle = fopen(a_file_path.GetStringConst(), "rb");
	if (nullptr == file_handle)
	{
		return false;
	}

	fseek(file_handle, 0, SEEK_END);
	file_size = ftell(file_handle);
	fseek(file_handle, 0, SEEK_SET);

	if (file_size == 0)
	{
		return true;
	}

	file_buffer = new char[file_size+1];
	if (nullptr == file_buffer)
	{
		return false;
	}
	memset(file_buffer, 0, file_size + 1);
	fread(file_buffer, sizeof(char), file_size, file_handle);
	file_buffer[file_size] = '\0';	//兼容需要  小文件一般用来存放字符串 故此添加此结尾

	return true;
}

void OvrSmallFile::Close() 
{
	if (nullptr != file_handle)
	{
		fclose(file_handle);
		file_handle = nullptr;
	}

	if (nullptr != file_buffer)
	{
		delete []file_buffer;
		file_buffer = nullptr;
	}
}

//创建文件
bool OvrSmallFile::WriteFile(const OvrString& a_file_path, const char* a_data, ovr::uint32 a_data_size) 
{
	//写文件
	FILE* new_file = OvrFileTools::CreateFileX(a_file_path.GetStringConst());

	if (nullptr == new_file)
	{
		return false;
	}
	fwrite(a_data, 1, a_data_size, new_file);
	fclose(new_file);
	return true;
}
//zip
bool OvrSmallFile::Open(const OvrString& a_zip_path, const OvrString& a_file_path_inzip)
{
	OvrZip zip;
	zip.OpenZip(a_zip_path.GetStringConst());
	bool bresult = zip.ReadFileFromZip(a_file_path_inzip.GetStringConst());
	if (!bresult)
		return false;
	file_size = zip.GetFileSize();
	file_buffer = new char[file_size+1];
	memcpy(file_buffer, zip.GetBuff(), file_size);
	file_buffer[file_size] = '\0';
	zip.CloseZip();
	return true;
}
bool OvrSmallFile::ExtractZip(const OvrString& a_zip_path, const OvrString& a_extract_path)
{
	OvrZip zip;
	zip.OpenZip(a_zip_path.GetStringConst());
	zip.ExtractZip(a_extract_path.GetStringConst());
	zip.CloseZip();
	return true;
}