#include "wx_color_value.h"

const OvrColorValue OvrColorValue::ZERO = OvrColorValue(0.0, 0.0, 0.0, 0.0);
const OvrColorValue OvrColorValue::Black = OvrColorValue(0.0, 0.0, 0.0);
const OvrColorValue OvrColorValue::White = OvrColorValue(1.0, 1.0, 1.0);
const OvrColorValue OvrColorValue::Red = OvrColorValue(1.0, 0.0, 0.0);
const OvrColorValue OvrColorValue::Green = OvrColorValue(0.0, 1.0, 0.0);
const OvrColorValue OvrColorValue::Blue = OvrColorValue(0.0, 0.0, 1.0);

bool OvrColorValue::operator==(const OvrColorValue& rhs) const
{
	return (r == rhs.r &&
		g == rhs.g &&
		b == rhs.b &&
		a == rhs.a);
}
//---------------------------------------------------------------------
bool OvrColorValue::operator!=(const OvrColorValue& rhs) const
{
	return !(*this == rhs);
}