﻿#include "wx_vector2.h"
#include <math.h>




OvrVector2::OvrVector2() :_x(0), _y(0)
{
}

OvrVector2::OvrVector2(float a_v) : _x(a_v), _y(a_v)
{
}

OvrVector2::OvrVector2(float a_x, float a_y) : _x(a_x), _y(a_y)
{
}


OvrVector2::~OvrVector2()
{
}


float& OvrVector2::operator[](int a_id)
{
	return _m[a_id];
}

const float& OvrVector2::operator[](int a_id) const
{
	return _m[a_id];
}

OvrVector2 OvrVector2::operator+(const OvrVector2& a_v2) const
{
	return OvrVector2(_x + a_v2[0], _y + a_v2[1]);
}

OvrVector2 OvrVector2::operator-(const OvrVector2& a_v2) const
{
	return OvrVector2(_x - a_v2[0], _y - a_v2[1]);
}

OvrVector2 OvrVector2::operator-() const
{
	return OvrVector2(-_x, -_y);
}

float OvrVector2::operator*(const OvrVector2& a_v2) const
{
	return _x*a_v2[0] + _y*a_v2[1];
}

OvrVector2 OvrVector2::operator*(float a_scale) const
{
	return OvrVector2(_x*a_scale, _y*a_scale);
}

OvrVector2 OvrVector2::operator/(float a_scale) const
{
	return OvrVector2(_x / a_scale, _y / a_scale);
}

void OvrVector2::operator+=(const OvrVector2& a_v2)
{
	_x += a_v2[0];
	_y += a_v2[1];
}

void OvrVector2::operator-=(const OvrVector2& a_v3)
{
	_x -= a_v3[0];
	_y -= a_v3[1];
}

void OvrVector2::operator*=(float a_scale)
{
	_x *= a_scale;
	_y *= a_scale;
}

void OvrVector2::operator/=(float a_scale)
{
	_x /= a_scale;
	_y /= a_scale;
}

bool OvrVector2::operator == (const OvrVector2& a_v2)const
{
	return _x == a_v2._x && _y == a_v2._y;
}

bool OvrVector2::operator<(const OvrVector2& a_v2) const
{
	if (_x < a_v2[0])
	{
		return true;
	}
	else if (_x == a_v2[0])
	{
		if (_y < a_v2[1])
		{
			return true;
		}
	}
	return false;
}

float OvrVector2::length() const
{
	return (float)sqrt(length_sq());
}

float OvrVector2::length_sq() const
{
	return _x*_x + _y*_y;
}

float OvrVector2::normalize()
{
	float t_l = length();
	*this /= t_l;
	return t_l;
}

OvrVector2 OvrVector2::get_normalize() const
{
	float t_l = length();
	return OvrVector2(_x / t_l, _y / t_l);
}

OvrVector2 OvrVector2::multi(const OvrVector2& a_v2) const
{
	return OvrVector2(_m[0] * a_v2[0], _m[1] * a_v2[1]);
}

bool OvrVector2::is_nan() const
{
	return _x != _x || _y != _y;
}



