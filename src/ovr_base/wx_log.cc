﻿#include "wx_log.h"
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#ifndef _WIN32
#include <android/log.h>
#endif

void OvrLog::Format(int a_level, const char* a_tag, const char * pFmt, ...)
{
#ifndef _WIN32
	//把%I64d替换成%lld
	const char *pPtr1 = pFmt;
	char szFormat[512] = { 0 };

	const char *pPtr2 = strstr(pPtr1, "%I64d");
	if (pPtr2 != NULL)
	{
		int nCopyLen = 0;
		while (pPtr2 != NULL)
		{
			memcpy(szFormat + nCopyLen, pPtr1, pPtr2 - pPtr1);
			nCopyLen += (pPtr2 - pPtr1);

			strcpy(szFormat + nCopyLen, "%lld");
			nCopyLen += strlen("%lld");

			pPtr1 = pPtr2 + strlen("%I64d");
			pPtr2 = strstr(pPtr1, "%I64d");
		}
		strcpy(szFormat + nCopyLen, pPtr1);
		pPtr1 = szFormat;
	}

	char temp_buffer[1024];
	va_list argptr;

	va_start(argptr, pPtr1);
	vsprintf(temp_buffer, pPtr1, argptr);
	va_end(argptr);

	if(a_level == OvrLog::kDebug)
    {
        __android_log_print(ANDROID_LOG_DEBUG,a_tag,"%s", temp_buffer);
    }
    else if (a_level == OvrLog::kInfo)
    {
        __android_log_print(ANDROID_LOG_INFO,a_tag,"%s", temp_buffer);
    }
    else if (a_level == OvrLog::kError)
    {
        __android_log_print(ANDROID_LOG_ERROR,a_tag,"%s", temp_buffer);
    }
#else

	char temp_buffer[1024];

	va_list argptr;
	
	//把可变参数序列化到strPrintBuff中
	va_start(argptr, pFmt);
	vsprintf(temp_buffer, pFmt, argptr);
	va_end(argptr);

	printf(temp_buffer);
#endif
}

