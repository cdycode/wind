﻿#include "wx_transform.h"

void OvrTransform::operator = (const OvrTransform& a_transform)
{
	position = a_transform.position;
	scale = a_transform.scale;
	quaternion = a_transform.quaternion;
}

void OvrTransform::GetMatrix(OvrMatrix4& a_matrix)const
{
	OvrMatrix4 rotate_matrix;
	rotate_matrix.from_quaternion(quaternion);
	a_matrix = OvrMatrix4::make_scale_matrix(scale) * rotate_matrix * OvrMatrix4::make_translation_matrix(position);
}
