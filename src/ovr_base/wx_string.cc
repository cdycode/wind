﻿
#include "wx_string.h"
#if defined(OVR_OS_WIN32)
#   include <windows.h>
#endif

#include <libiconv/iconv.h>

const OvrString	OvrString::empty = "";

void ConvertString(const OvrString& a_from_code, const OvrString& a_to_code, const OvrString& a_from_string, OvrString& a_to_string) 
{
	bool is_to_wchar = (a_to_code == "utf-16le");
	bool is_to_utf8 = (a_to_code == "utf-8");

	iconv_t cd = iconv_open(a_to_code.GetStringConst(), a_from_code.GetStringConst());

	//输入字符串信息
	char* in_string = (char*)a_from_string.GetStringConst();
	size_t in_len = a_from_string.GetStringSize();

	//输出字符串信息
	size_t out_buffer_len = in_len + 8;	//估算值
	if (is_to_wchar)
	{
		out_buffer_len = in_len * 2 + 4;
	}
	else if (is_to_utf8)
	{
		out_buffer_len = in_len * 4 + 2;	//utf8 还可变长字节，常用汉字占3个字节  不常用汉字占4个字节
	}
	char* out_buffer = new char[out_buffer_len];
	memset(out_buffer, 0, out_buffer_len);
	char* out_string = out_buffer;
	size_t out_len = out_buffer_len;

	if (iconv(cd, &in_string, &in_len, &out_string, &out_len) != -1)
	{
		//in_string 还未转换的地址 一般是末尾 in_len 剩余未转换的字节数
		//out_string 剩余的地址  out_len 剩余的长度
		a_to_string.SetString(out_buffer, out_buffer_len - out_len);
	}
	iconv_close(cd);
	delete []out_buffer;
}

void OvrString::GbkToUtf8(const OvrString& a_in_gbk, OvrString& a_out_utf8) 
{
	ConvertString("gb2312", "utf-8", a_in_gbk, a_out_utf8);
}

void OvrString::Utf8ToGbk(const OvrString& a_in_utf8, OvrString& a_out_gbk) 
{
	ConvertString("utf-8", "gb2312", a_in_utf8, a_out_gbk);
}

void OvrString::GbkToUnicode(const OvrString& a_in_gbk, std::wstring& a_out_unicode) 
{
	OvrString out_string;
	ConvertString("gb2312", "utf-16le", a_in_gbk, out_string);	//windows为小端 如果遇到大端的机器 可能需要设置为utf-16be
	a_out_unicode.append((const wchar_t*)out_string.GetStringConst(), out_string.GetStringSize()/2);
}
void OvrString::UnicodeToGbk(const std::wstring& a_in_unicode, OvrString& a_out_gbk)
{
	OvrString in_string;
	in_string.SetString((const char*)a_in_unicode.c_str(), a_in_unicode.size() * 2);	//最后一个是0字符
	ConvertString("utf-16le", "gb2312", in_string, a_out_gbk);
}

void OvrString::UnicodeToUtf8(const std::wstring& a_in_unicode, OvrString& a_out_utf8) 
{
	OvrString in_string;
	in_string.SetString((const char*)a_in_unicode.c_str(), a_in_unicode.size() * 2);	//最后一个是0字符
	ConvertString("utf-16le", "utf-8", in_string, a_out_utf8);
}

void OvrString::Utf8ToUnicode(const OvrString& a_in_utf8, std::wstring& a_out_unicode) 
{
	OvrString out_string;
	ConvertString("utf-8", "utf-16le", a_in_utf8, out_string);	//windows为小端 如果遇到大端的机器 可能需要设置为utf-16be
	a_out_unicode.append((const wchar_t*)out_string.GetStringConst(), out_string.GetStringSize()/2);
}

OvrString::OvrString()
{
	Resize(1);
	string_buffer[0] = '\0';
}

OvrString::OvrString(const char* a_string)
{
	SetString(a_string);
}

OvrString::OvrString(const char* a_string, ovr::uint32 a_string_size) 
{
	SetString(a_string, a_string_size);
}

OvrString::OvrString(const OvrString& a_string)
{
	SetString(a_string.GetStringConst());
}

OvrString::OvrString(ovr::uint32 a_buffer_size)
{
	Resize(a_buffer_size);
	memset(string_buffer, 0, buffer_length);
}

OvrString::~OvrString()
{
	if (nullptr != string_buffer)
	{
		delete[]string_buffer;
		string_buffer = nullptr;
	}
}

OvrString& OvrString::operator = (const char* a_src_string)
{
	SetString(a_src_string);
	return *this;
}

OvrString& OvrString::operator = (const OvrString& a_src_string)
{
	SetString(a_src_string.GetStringConst());
	return *this;
}

bool OvrString::operator == (const char* a_string)const
{
	return (0 == strcmp(string_buffer, a_string));
}

bool OvrString::operator == (const OvrString& a_string)const 
{
	return (0 == strcmp(string_buffer, a_string.GetStringConst()));
}

bool OvrString::operator != (const char* a_string)const 
{
	return (0 != strcmp(string_buffer, a_string));
}

bool OvrString::operator != (const OvrString& a_string)const 
{
	return (0 != strcmp(string_buffer, a_string.GetStringConst()));
}

char& OvrString::operator [] (ovr::uint32 a_index)
{
	OVR_ASSERT(a_index <= string_size);
	return string_buffer[a_index];
}

bool OvrString::operator += (const char* a_append_string) 
{
	return AppendString(a_append_string);
}
bool OvrString::operator += (const OvrString& a_append_string) 
{
	return AppendString(a_append_string.GetStringConst());
}

OvrString OvrString::operator + (const OvrString& a_string) const
{
	OvrString new_string(*this);
	new_string += a_string;
	return new_string;
}

bool OvrString::operator < (const OvrString& a_string)const 
{
	return (strcmp(string_buffer, a_string.string_buffer) < 0);
}

ovr::int32	OvrString::FindLastCharPos(char a_char)const
{
	ovr::int32 find_pos = -1;
	for (ovr::int32 i = string_size-1; i >= 0; i--)
	{
		if (string_buffer[i] == a_char)
		{
			find_pos = i;
			break;
		}
	}
	return find_pos;
}

OvrString OvrString::SubString(ovr::uint32 a_from_pos, ovr::uint32 a_size)const
{
	if (a_from_pos > string_size)
	{
		return empty;
	}
	if (a_from_pos+a_size > string_size)
	{
		a_size = string_size - a_from_pos;
	}
	if (a_size == 0)
	{
		return empty;
	}

	OvrString sub_string(&(string_buffer[a_from_pos]));

	sub_string[a_size] = '\0';
	return sub_string;
}

ovr::uint32	OvrString::Split(char a_char, std::list<OvrString>& a_list)const 
{
	ovr::uint32 from_pos = 0;
	for (ovr::uint32 i = 0; i < string_size; i++)
	{
		if (string_buffer[i] == a_char)
		{
			//避免出现第一个或者连续两个是分割字符的情况
			if (i > from_pos)
			{
				a_list.push_back(OvrString(string_buffer + from_pos, i - from_pos));
			}
			from_pos = i +1;
		}
	}
	if (from_pos < string_size)
	{
		a_list.push_back(OvrString(string_buffer +from_pos, string_size - from_pos));
	}
	
	return (ovr::uint32)a_list.size();
}


bool OvrString::SetString(const char* a_src_string, ovr::uint32 len)
{
	if (nullptr != a_src_string)
	{
		if (Resize(len + 1))
		{
			memcpy(string_buffer, a_src_string, len);
			string_size = len;
			string_buffer[string_size] = '\0';
			return true;
		}
	}
	return false;
}

bool OvrString::SetString(const char* a_src_string)
{
	if (nullptr != a_src_string)
	{
		ovr::uint32 src_size = (ovr::uint32)strlen(a_src_string);
		return SetString(a_src_string, src_size);
	}
	return false;
}

bool OvrString::AppendChar(const char c)
{
	return AppendString(&c, 1);
}

bool OvrString::AppendString(const char* a_string)
{
	if (nullptr == a_string)
	{
		return false;
	}

	return AppendString(a_string, (ovr::uint32)strlen(a_string));
}

bool OvrString::AppendString(const char* a_string, ovr::uint32 len)
{
	if (nullptr == a_string)
	{
		return false;
	}

	if (string_size == 0)
	{
		return SetString(a_string, len);
	}

	ovr::uint32 new_size = len + string_size;

	if (new_size >= buffer_length)
	{
		char* new_buffer = new char[new_size + 1];
		if (nullptr == new_buffer)
		{
			return false;
		}
		memcpy(new_buffer, string_buffer, string_size);
		memcpy(new_buffer + string_size, a_string, len);
		new_buffer[new_size] = '\0';
		delete[]string_buffer;
		string_buffer = new_buffer;
	}
	else
	{
		memcpy(string_buffer + string_size, a_string, len);
		string_buffer[new_size] = '\0';
	}

	string_size = new_size;

	return true;
}

void OvrString::ToUtf8(OvrString& a_utf8)const 
{
	GbkToUtf8(*this, a_utf8);
}

void OvrString::FromUtf8(const char* a_utf8) 
{
	Utf8ToGbk(a_utf8, *this);
}

//会丢失原数据
bool OvrString::Resize(ovr::uint32 a_new_size)
{
	if (a_new_size <= buffer_length)
	{
		return true;
	}
	if (nullptr != string_buffer)
	{
		delete[]string_buffer;
		string_buffer = nullptr;
		string_size = 0;
		buffer_length = 0;
	}
	string_buffer = new char[a_new_size];
	if (nullptr != string_buffer)
	{
		string_size = 0;
		buffer_length = a_new_size;
		return true;
	}
	return false;
}

void OvrString::Reset()
{
	memset(string_buffer , 0, string_size);
	string_size = 0;
}

void OvrString::Clear() 
{
	memset(string_buffer, 0, string_size);
	string_size = 0;
}
