﻿#include "wx_quaternion.h"
#include <math.h>
#include "wx_math_configure.h"
#include "wx_matrix4f.h"
const OvrQuaternion OvrQuaternion::ZERO(0.0f, 0.0f, 0.0f, 0.0f);
const OvrQuaternion OvrQuaternion::IDENTITY;
OvrQuaternion::OvrQuaternion()
	:w(1),x(0),y(0),z(0)
{

}

OvrQuaternion::OvrQuaternion(const float& a_x,const float& a_y,const float& a_z,const float& a_w)
	:w(a_w),x(a_x),y(a_y),z(a_z)
{

}

OvrQuaternion::OvrQuaternion(const OvrVector3& a_axis,const float& a_angle_rad)
{
	set_axis_angle(a_axis, a_angle_rad);
}

OvrQuaternion::OvrQuaternion(const OvrVector3& a_from,const OvrVector3& a_to)
{
	const float cx = a_from[1] * a_to[2] - a_from[2] * a_to[1];
	const float cy = a_from[2] * a_to[0] - a_from[0] * a_to[2];
	const float cz = a_from[0] * a_to[1] - a_from[1] * a_to[0];
	const float dot = a_from[0] * a_to[0] + a_from[1] * a_to[1] + a_from[2] * a_to[2];
	const float cross_length_sq = cx * cx + cy * cy + cz * cz;
	const float magnitude = (float)sqrt( cross_length_sq + dot * dot );
	const float cw = dot + magnitude;
	if ( cw <  ovr::SmallestNonDenormal )
	{
		const float sx = a_to[1] * a_to[1] + a_to[2] * a_to[2];
		const float sz = a_to[0] * a_to[0] + a_to[1] * a_to[1];
		if ( sx > sz )
		{
			const float rcpLength = lvr_rcp_sqrt( sx );
			x = float(0);
			y = a_to[2] * rcpLength;
			z = - a_to[1] * rcpLength;
			w = float(0);
		}
		else
		{
			const float rcpLength = lvr_rcp_sqrt( sz );
			x = a_to[1] * rcpLength;
			y = - a_to[0] * rcpLength;
			z = float(0);
			w = float(0);
		}
		return;
	}
	const float rcpLength = lvr_rcp_sqrt( cross_length_sq + cw * cw );
	x = cx * rcpLength;
	y = cy * rcpLength;
	z = cz * rcpLength;
	w = cw * rcpLength;
}

float& OvrQuaternion::operator[](int a_id)
{
	return _m[a_id];
}

const float& OvrQuaternion::operator[](int a_id) const
{
	return _m[a_id];
}

OvrQuaternion OvrQuaternion::operator+(const OvrQuaternion& a_q) const
{
	return OvrQuaternion(x + a_q[1],y + a_q[2],z + a_q[3],w + a_q[0]);
}

void OvrQuaternion::operator+=(const OvrQuaternion& a_q)
{
	x += a_q[1];
	y += a_q[2];
	z += a_q[3];
	w += a_q[0];
}

OvrQuaternion OvrQuaternion::operator-(const OvrQuaternion& a_q) const
{
	return OvrQuaternion(x - a_q[1],y - a_q[2],z - a_q[3],w - a_q[0]);
}

void OvrQuaternion::operator-=(const OvrQuaternion& a_q)
{
	x -= a_q[1];
	y -= a_q[2];
	z -= a_q[3];
	w -= a_q[0];
}

OvrQuaternion OvrQuaternion::operator*(const float& a_s) const
{
	return OvrQuaternion(x *a_s, y*a_s, z*a_s, w*a_s);
}

void OvrQuaternion::operator*=(const float& a_s)
{
	x *= a_s;
	y *= a_s;
	z *= a_s;
	w *= a_s;
}

OvrQuaternion OvrQuaternion::operator*(const OvrQuaternion& a_q) const
{
	const float& tx = a_q[1];
	const float& ty = a_q[2];
	const float& tz = a_q[3];
	const float& tw = a_q[0];
	return OvrQuaternion(
		// 		w * tx + x * tw + y * tz - z * ty,
		// 		w * ty - x * tz + y * tw + z * tx,
		// 		w * tz + x * ty - y * tx + z * tw,
		// 		w * tw - x * tx - y * ty - z * tz
		tw * x + tx * w + ty * z - tz * y,
		tw * y - tx * z + ty * w + tz * x,
		tw * z + tx * y - ty * x + tz * w,
		tw * w - tx * x - ty * y - tz * z
		);
}

OvrVector3 OvrQuaternion::operator*(const OvrVector3& a_v) const
{
	 return ((*this).get_inverse()*OvrQuaternion(a_v._x, a_v._y, a_v._z, 0)*(*this)).get_imag();
}

OvrQuaternion& OvrQuaternion::set_axis_angle(const OvrVector3& a_axis,const float& a_angle_rad)
{
	if (0 == a_axis.length_sq())
	{
		x = float(0); y = float(0); z = float(0); w = float(1);
		return *this;
	}
	OvrVector3 unit_axis(a_axis);
	unit_axis.normalize();
	float  sin_half_angle = (float)sin(a_angle_rad * float(0.5));

	w = (float)cos(a_angle_rad * float(0.5));
	x = unit_axis[0] * sin_half_angle;
	y = unit_axis[1] * sin_half_angle;
	z = unit_axis[2] * sin_half_angle;
	return *this;
}

float OvrQuaternion::length() const
{
	return (float)sqrt(length_sq());
}

float OvrQuaternion::length_sq() const
{
	return (x*x + y*y + z*z + w*w);
}

void OvrQuaternion::normalize()
{
	float t_l = length();
	if (t_l > LVR_TOLERANCE)
	{
		float rc_tl = float(1.0)/t_l;
		x *= rc_tl;
		y *= rc_tl;
		z *= rc_tl;
		w *= rc_tl;
	}
}

OvrQuaternion OvrQuaternion::get_normalize() const
{
	float t_l = length();
	if (t_l > LVR_TOLERANCE)
	{
		float rc_tl = float(1.0)/t_l;
		return OvrQuaternion(x*rc_tl, y*rc_tl, z*rc_tl, w*rc_tl);
	}
	else
	{
		return *this;
	}
}

OvrQuaternion OvrQuaternion::get_conj() const
{
	return OvrQuaternion(-x,-y,-z,w);
}

void OvrQuaternion::inverse()
{
	x = -x;
	y = -y;
	z = -z;
}

OvrQuaternion OvrQuaternion::get_inverse() const
{
	return OvrQuaternion(-x,-y,-z,w);
}
//There is no difference between get_inverse() and get_conj().So maybe one should delete.
OvrVector3 OvrQuaternion::get_imag() const
{
	return OvrVector3(x,y,z);
}
                                                                           
OvrQuaternion OvrQuaternion::lerp(const OvrQuaternion& a_q0,const OvrQuaternion& a_q1,const float& a_pos)
{
	float t_p = a_pos;
	lvr_clamp(t_p,float(0),float(1.0));
	return a_q0*(float(1.0)-t_p) + a_q1*t_p;

}

 OvrQuaternion OvrQuaternion::make_quat_from_to(const OvrVector3& a_from, const OvrVector3& a_to)
{
	 OvrMatrix4 rotation = OvrMatrix4::make_rotate_from_to_matrix(a_from, a_to);
	 OvrQuaternion quat;
	 rotation.decompose(nullptr, &quat, nullptr);
	 return quat;
}

 OvrQuaternion OvrQuaternion::make_quat_from_up_dir(const OvrVector3& a_up, const OvrVector3& a_dir)
{
	 OvrQuaternion quat;
	if(a_up*OvrVector3::UNIT_Y < 0.5)
	{
		quat = OvrQuaternion::make_quat_from_to(OvrVector3::UNIT_Y, a_up);
	}
	else if( a_dir*(-OvrVector3::UNIT_Z) <0.5)
	{
		quat = OvrQuaternion::make_quat_from_to(-OvrVector3::UNIT_Z, a_dir);
	}
	else
	{
		
	}
	return quat;
}

OvrQuaternion OvrQuaternion::slerp(const OvrQuaternion& a_q1,const OvrQuaternion& a_q2,const float& a_pos)
{
	// It contains no division operations, no trig, no inverse trig
	// and no sqrt. Not only does this code tolerate small constraint
	// errors in the input quaternions, it actually corrects for them.
	OvrQuaternion dst_q;
	float t_ = a_pos;
	lvr_clamp(t_,float(0),float(1));

	if (a_q1[0] == a_q2[0] && a_q1[1] == a_q2[1] && a_q1[2] == a_q2[2] && a_q1[3] == a_q2[3])
	{
		dst_q.x = a_q1[1];
		dst_q.y = a_q1[2];
		dst_q.z = a_q1[3];
		dst_q.w = a_q1[0];
		return dst_q;
	}

	float halfY, alpha, beta;
	float u, f1, f2a, f2b;
	float ratio1, ratio2;
	float halfSecHalfTheta, versHalfTheta;
	float sqNotU, sqU;

	float cosTheta = a_q1[3] * a_q2[3] + a_q1[0] * a_q2[0] + a_q1[1] * a_q2[1] + a_q1[2] * a_q2[2];

	// As usual in all slerp implementations, we fold theta.
	alpha = cosTheta >= 0 ? 1.0f : -1.0f;
	halfY = 1.0f + alpha * cosTheta;

	// Here we bisect the interval, so we need to fold t_ as well.
	f2b = t_ - 0.5f;
	u = f2b >= 0 ? f2b : -f2b;
	f2a = u - f2b;
	f2b += u;
	u += u;
	f1 = 1.0f - u;

	// One iteration of Newton to get 1-cos(theta / 2) to good accuracy.
	halfSecHalfTheta = 1.09f - (0.476537f - 0.0903321f * halfY) * halfY;
	halfSecHalfTheta *= 1.5f - halfY * halfSecHalfTheta * halfSecHalfTheta;
	versHalfTheta = 1.0f - halfY * halfSecHalfTheta;

	// Evaluate series expansions of the coefficients.
	sqNotU = f1 * f1;
	ratio2 = 0.0000440917108f * versHalfTheta;
	ratio1 = -0.00158730159f + (sqNotU - 16.0f) * ratio2;
	ratio1 = 0.0333333333f + ratio1 * (sqNotU - 9.0f) * versHalfTheta;
	ratio1 = -0.333333333f + ratio1 * (sqNotU - 4.0f) * versHalfTheta;
	ratio1 = 1.0f + ratio1 * (sqNotU - 1.0f) * versHalfTheta;

	sqU = u * u;
	ratio2 = -0.00158730159f + (sqU - 16.0f) * ratio2;
	ratio2 = 0.0333333333f + ratio2 * (sqU - 9.0f) * versHalfTheta;
	ratio2 = -0.333333333f + ratio2 * (sqU - 4.0f) * versHalfTheta;
	ratio2 = 1.0f + ratio2 * (sqU - 1.0f) * versHalfTheta;

	// Perform the bisection and resolve the folding done earlier.
	f1 *= ratio1 * halfSecHalfTheta;
	f2a *= ratio2;
	f2b *= ratio2;
	alpha *= f1 + f2a;
	beta = f1 + f2b;

	// Apply final coefficients to a and b as usual.
	float tz = alpha * a_q1[3] + beta * a_q2[3];
	float tw = alpha * a_q1[0] + beta * a_q2[0];
	float tx = alpha * a_q1[1] + beta * a_q2[1];
	float ty = alpha * a_q1[2] + beta * a_q2[2];

	// This final adjustment to the quaternion's length corrects for
	// any small constraint error in the inputs q1 and q2 But as you
	// can see, it comes at the cost of 9 additional multiplication
	// operations. If this error-correcting feature is not required,
	// the following code may be removed.
	f1 = float(1.5) - float(0.5) * (tw * tw + tx * tx + ty * ty + tz * tz);
	dst_q[0] = tw * f1;
	dst_q[1] = tx * f1;
	dst_q[2] = ty * f1;
	dst_q[3] = tz * f1;
	return dst_q;
}

// Definitions of axes for coordinate and rotation conversions.
enum Axis
{
	Axis_X = 0, Axis_Y = 1, Axis_Z = 2
};
// RotateDirection describes the rotation direction around an axis, interpreted as follows:
//  CW  - Clockwise while looking "down" from positive axis towards the origin.
//  CCW - Counter-clockwise while looking from the positive axis towards the origin,
//        which is in the negative axis direction.
//  CCW is the default for the RHS coordinate system. Oculus standard RHS coordinate
//  system defines Y up, X right, and Z back (pointing out from the screen). In this
//  system Rotate_CCW around Z will specifies counter-clockwise rotation in XY plane.
enum RotateDirection
{
	Rotate_CCW = 1,
	Rotate_CW  = -1 
};
// Constants for right handed and left handed coordinate systems
enum HandedSystem
{
	Handed_R = 1, Handed_L = -1
};

// GetEulerAngles extracts Euler angles from the quaternion, in the specified order of
// axis rotations and the specified coordinate system. Right-handed coordinate system
// is the default, with CCW rotations while looking in the negative axis direction.
// Here a,b,c, are the Yaw/Pitch/Roll angles to be returned.
// rotation a around axis A1
// is followed by rotation b around axis A2
// is followed by rotation c around axis A3
// rotations are CCW or CW (D) in LH or RH coordinate system (S)
template <class T,Axis A1, Axis A2, Axis A3, RotateDirection D, HandedSystem S>
void GetEulerAngles( const OvrQuaternion* a_q,T *a, T *b, T *c)
{
	T Q[3] = { a_q->x, a_q->y, a_q->z };  //Quaternion components x,y,z
	T w = a_q->w;
	T ww  = w*w;
	T Q11 = Q[A1]*Q[A1];
	T Q22 = Q[A2]*Q[A2];
	T Q33 = Q[A3]*Q[A3];

	T psign = T(-1);
	// Determine whether even permutation
	if (((A1 + 1) % 3 == A2) && ((A2 + 1) % 3 == A3))
		psign = T(1);

	T s2 = psign * T(2) * (psign*w*Q[A2] + Q[A1]*Q[A3]);

	if (s2 < T(-1) + LVR_TOLERANCE)
	{ // South pole singularity
		*a = T(0);
		*b = -S*D* (float)(LVR_HALF_PI);
		*c = S*D* (float)atan2(T(2)*(psign*Q[A1]*Q[A2] + w*Q[A3]),
			ww + Q22 - Q11 - Q33 );
	}
	else if (s2 > T(1) - LVR_TOLERANCE)
	{  // North pole singularity
		*a = T(0);
		*b = S*D* (float)LVR_HALF_PI;
		*c = S*D* (float)atan2(T(2)*(psign*Q[A1]*Q[A2] + w*Q[A3]),
			ww + Q22 - Q11 - Q33);
	}
	else
	{
		*a = -S*D*(float)atan2(T(-2)*(w*Q[A1] - psign*Q[A2]*Q[A3]),
			ww + Q33 - Q11 - Q22);
		*b = S*D*(float)asin(s2);
		*c = S*D*(float)atan2(T(2)*(w*Q[A3] - psign*Q[A1]*Q[A2]),
			ww + Q11 - Q22 - Q33);
	}
}

void OvrQuaternion::get_axis_angle(OvrVector3& axis,float& ao_angle) const
{
	if ( x*x + y*y + z*z > LVR_TOLERANCE*LVR_TOLERANCE ) {
		axis  = OvrVector3(x, y, z).get_normalize();
		ao_angle = 2 * lvr_acos(w);
		if (ao_angle > LVR_PI) // Reduce the magnitude of the angle, if necessary
		{
			ao_angle = (float)(LVR_TWO_PI - ao_angle);
			axis = axis * static_cast<float>(-1);
		}
	}
	else 
	{
		axis = OvrVector3(static_cast<float>(1), static_cast<float>(0), static_cast<float>(0));
		ao_angle= 0;
	}
}

void OvrQuaternion::get_euler_angles(float& a,float& b, float& c) const
{
	GetEulerAngles<float, Axis_Z, Axis_X, Axis_Y, Rotate_CCW, Handed_R>(this, &c, &a, &b);
}

OvrQuaternion OvrQuaternion::make_euler_quat(const float& a_pitch,const float& a_yaw,const float& a_roll)
{
	return OvrQuaternion(OvrVector3((0.0), (1.0), (0.0)), a_yaw)*
		OvrQuaternion(OvrVector3((1.0), (0.0), (0.0)), a_pitch)*
		OvrQuaternion(OvrVector3((0.0), (0.0), (1.0)), a_roll);
}