﻿#include "wx_vector3.h"
#include <math.h>
const OvrVector3 OvrVector3::ZERO(0.0f, 0.0f, 0.0f);
const OvrVector3 OvrVector3::UNIT_X(1.0f, 0.0f, 0.0f);
const OvrVector3 OvrVector3::UNIT_Y(0.0f, 1.0f, 0.0f);
const OvrVector3 OvrVector3::UNIT_Z(0.0f, 0.0f, 1.0f);
const OvrVector3 OvrVector3::UNIT_SCALE(1.0f, 1.0f, 1.0f);

OvrVector3::OvrVector3() :_x(0), _y(0), _z(0)
{
}

OvrVector3::OvrVector3(float a_v) : _x(a_v), _y(a_v), _z(a_v)
{
}

OvrVector3::OvrVector3(float a_x, float a_y, float a_z) : _x(a_x), _y(a_y), _z(a_z)
{
}


OvrVector3::~OvrVector3()
{
}


float& OvrVector3::operator[](int a_id)
{
	return _m[a_id];
}

const float& OvrVector3::operator[](int a_id) const
{
	return _m[a_id];
}

OvrVector3 OvrVector3::operator+(const OvrVector3& a_v3) const
{
	return OvrVector3(_x + a_v3[0], _y + a_v3[1], _z + a_v3[2]);
}

OvrVector3 OvrVector3::operator-(const OvrVector3& a_v3) const
{
	return OvrVector3(_x - a_v3[0], _y - a_v3[1], _z - a_v3[2]);
}

OvrVector3 OvrVector3::operator-() const
{
	return OvrVector3(-_x, -_y, -_z);
}

float OvrVector3::operator*(const OvrVector3& a_v3) const
{
	return _x*a_v3[0] + _y*a_v3[1] + _z*a_v3[2];
}

OvrVector3 OvrVector3::operator*(float a_scale) const
{
	return OvrVector3(_x*a_scale, _y*a_scale, _z*a_scale);
}

OvrVector3 OvrVector3::operator/(float a_scale) const
{
	return OvrVector3(_x / a_scale, _y / a_scale, _z / a_scale);
}

void OvrVector3::operator+=(const OvrVector3& a_v3)
{
	_x += a_v3[0];
	_y += a_v3[1];
	_z += a_v3[2];
}

void OvrVector3::operator-=(const OvrVector3& a_v3)
{
	_x -= a_v3[0];
	_y -= a_v3[1];
	_z -= a_v3[2];
}

void OvrVector3::operator*=(float a_scale)
{
	_x *= a_scale;
	_y *= a_scale;
	_z *= a_scale;
}

void OvrVector3::operator/=(float a_scale)
{
	_x /= a_scale;
	_y /= a_scale;
	_z /= a_scale;
}

bool OvrVector3::operator == (const OvrVector3& a_v3)const
{
	return _x == a_v3._x && _y == a_v3._y && _z == a_v3._z;
}

bool OvrVector3::operator<(const OvrVector3& a_v2) const
{
	if (_x < a_v2[0])
	{
		return true;
	}
	else if (_x == a_v2[0])
	{
		if (_y < a_v2[1])
		{
			return true;
		}
		else if (_y == a_v2[1] && _z < a_v2[2])
		{
			return true;
		}
	}
	return false;
}

float OvrVector3::length() const
{
	return (float)sqrt(length_sq());
}

float OvrVector3::length_sq() const
{
	return _x*_x + _y*_y + _z*_z;
}

float OvrVector3::normalize()
{
	float t_l = length();
	*this /= t_l;
	return t_l;
}

OvrVector3 OvrVector3::get_normalize() const
{
	float t_l = length();
	return OvrVector3(_x / t_l, _y / t_l, _z / t_l);
}

OvrVector3 OvrVector3::cross(const OvrVector3& a_v3) const
{
	return OvrVector3(_y*a_v3[2] - _z*a_v3[1],
		_z*a_v3[0] - _x*a_v3[2],
		_x*a_v3[1] - _y*a_v3[0]);
}

OvrVector3 OvrVector3::multi(const OvrVector3& a_v3) const
{
	return OvrVector3(_m[0] * a_v3[0], _m[1] * a_v3[1], _m[2] * a_v3[2]);
}

OvrVector3 OvrVector3::lerp(const OvrVector3& a_v3, float a_pos) const
{
	return OvrVector3(_x*a_pos + a_v3[0] * (1.0f - a_pos), _y*a_pos + a_v3[1] * (1.0f - a_pos), _z*a_pos + a_v3[2] * (1.0f - a_pos));
}

bool OvrVector3::is_nan() const
{
	return _x != _x || _y != _y || _z != _z;
}



