﻿#include "wx_file_tools.h"
#include "wx_string.h"

#include<sys/stat.h> 

#ifdef _MSC_VER
#   include <io.h>
#	include <direct.h>
    typedef struct _stat STAT;
#   define     ovr_stat         _stat
#   define     ovr_access       _access
#   define     OVR_S_IFREG      _S_IFREG
#else
#	include <dirent.h>
#   include <stdio.h>
#   include <unistd.h>
    typedef struct stat STAT;
#   define     ovr_stat         stat
#   define     ovr_access       access
#   define     OVR_S_IFREG      S_IFREG
#endif

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <wx_log.h>

bool OvrFileTools::GetPathFromFullPath(const char* fullPath, OvrString& path)
{
	if ( fullPath == NULL)
	{
		return false;
	}

	OvrString var(fullPath);
	TransformSlash(var);

	const char* lastPos = strrchr(var.GetString(), '/');

	if (lastPos == NULL)
	{
		path.SetString("./");
	}
	else
	{
		const char* firstPos = var.GetString();

		if (firstPos == lastPos)
		{
			path.SetString("/");
		}
		else
		{
			path.SetString(firstPos, (ovr::uint32)(strlen(firstPos) - strlen(lastPos))+1);
		}
	}

	return true;
}

bool OvrFileTools::GetFileNameFromPath(const char* path, OvrString& fileName)
{
	if (path == NULL)
	{
		return false;
	}


	OvrString var(path);
	TransformSlash(var);
	const char* startPos = strrchr(var.GetString(), '/');

	if (startPos != NULL)
	{
		startPos++;
	}

	if (startPos == NULL)
	{
		startPos = path;
	}

	const char* endPos = strrchr(startPos, '.');

	if (endPos == NULL)
	{
		fileName.SetString(startPos);
	}
	else
	{
		fileName.SetString(startPos, (ovr::uint32)(strlen(startPos) - strlen(endPos)));
	}

	return true;
}

bool OvrFileTools::GetFileAllNameFromPath(const char* path, OvrString& fileName)
{
	//STAT buf;

	if (path == NULL)
	{
		return false;
	}

	/*if (ovr_access(path, 0) != 0)
	{
		return false;
	}*/

	OvrString var(path);
	TransformSlash(var);
	const char* startPos = strrchr(var.GetString(), '/');

	if (startPos != NULL)
	{
		startPos++;
	}

	if (startPos == NULL)
	{
		startPos = path;
	}

	if (startPos != NULL)
	{
		fileName.SetString(startPos);
	}
	return true;
}

bool OvrFileTools::GetFileSuffixFromPath(const OvrString& a_file_path, OvrString& a_file_suffix)
{
	if (a_file_path.GetStringSize() == 0)
	{
		return false;
	}

	OvrString var = a_file_path;
	TransformSlash(var);

	ovr::int32 start_pos = var.FindLastCharPos('.');
	if (start_pos >= 0)
	{
		a_file_suffix = var.SubString(start_pos + 1, a_file_path.GetStringSize() - start_pos);
	}
	return true;
}

bool OvrFileTools::CreateFolder(  const OvrString& a_dir_path)
{
	OvrString dir_path(a_dir_path);

	if (!CheckPathLastIsSlash(dir_path.GetStringConst()) )
	{
		//保证最后一个字符是 分隔符
		dir_path.AppendChar('/');
	}

	//保证符号都转化为 '/'
	TransformSlash(dir_path);

	return InitDir(dir_path.GetStringConst());
}

bool OvrFileTools::DeleteFile(const OvrString& a_path)
{
	return (remove(a_path.GetStringConst()) == 0);
}

bool OvrFileTools::CopyOneFile(const OvrString& a_src_file_path, const OvrString& a_dst_file_path)
{
	if (!IsFileExist(a_src_file_path))
	{
		return false;
	}

	//保证目录的存在
	OvrString dst_file_dir;
	OvrFileTools::GetPathFromFullPath(a_dst_file_path.GetStringConst(), dst_file_dir);
	OvrFileTools::CreateFolder(dst_file_dir.GetStringConst());

	bool copy_result = false;

	std::ifstream in_file;
	std::ofstream out_file;
	in_file.open(a_src_file_path.GetStringConst(), std::ios::binary);
	out_file.open(a_dst_file_path.GetStringConst(), std::ios::binary);

	if (!in_file.fail() && !out_file.fail())
	{
		out_file << in_file.rdbuf();
		copy_result = true;
	}

	out_file.close();
	in_file.close();

	return copy_result;
}

bool OvrFileTools::IsFileExist(const OvrString& a_file)
{
	return (ovr_access(a_file.GetStringConst(), 0) == 0);
}

bool OvrFileTools::MakeFile(const char* path)
{
	FILE* file_handle = CreateFileX(path);
	if (nullptr != file_handle)
	{
		fclose(file_handle);
		return true;
	}
	return false;
}

FILE* OvrFileTools::CreateFileX(const OvrString& a_file_path)
{
	if ( !CheckPathLastIsSlash(a_file_path.GetStringConst()) )
	{
		OvrString file_path = a_file_path;
		TransformSlash(file_path);
		if ( InitDir(file_path.GetStringConst()) )
		{
			return fopen(file_path.GetStringConst(), "w+b" );
		}
	}

	return nullptr;
}

FILE* OvrFileTools::OpenFile( const char* path )
{
	if ( !CheckPathLastIsSlash( path ) )
	{
		if ( InitDir( path ) )
		{
			return fopen( path, "rb" );
		}
	}

	return NULL;
}

bool OvrFileTools::CheckPathLastIsSlash( const char* a_path )
{
	if (nullptr == a_path)
	{
		return false;
	}
	size_t length = strlen(a_path);

	return (a_path[length - 1] == '\\' || a_path[length - 1] == '/');
}

bool OvrFileTools::TraverseFiles(const char* path, const char* exName, void(*FileFunc)(void*, const char*), void* handlePtr, const unsigned int filePathOffset)
{
#ifdef _MSC_VER
	_finddata_t file_info;
	OvrString tempPath(path);
	OvrString rootPath;
	OvrString filePath;

	if (!CheckPathLastIsSlash(path))
	{
		tempPath.AppendString("/");
	}

	rootPath.SetString(tempPath.GetString());
	unsigned int offset = rootPath.GetStringSize() > filePathOffset ? filePathOffset : rootPath.GetStringSize();

	/*  --------------定义后面的后缀为*.exe，*.txt等来查找特定后缀的文件，*.*是通配符，匹配所有类型,路径连接符左斜杠/，可跨平台 -------------- */
	if (exName == NULL)
	{
		tempPath.AppendString("*.*");
	}
	else
	{
		tempPath.AppendString("*.");
		tempPath.AppendString(exName);
	}

	/*  --------------打开文件查找句柄 --------------  */
	int handle = (int)_findfirst(tempPath.GetString(), &file_info);

	if (handle == -1)
	{
		return false;
	}

	do
	{
		if (file_info.attrib == _A_SUBDIR)
		{
			continue;
		}

		if (FileFunc != NULL)
		{
			filePath.SetString(rootPath.GetString());
			filePath.AppendString(file_info.name);
			FileFunc(handlePtr, filePath.GetString() + offset);
		}

	} while (_findnext(handle, &file_info) == 0);

	_findclose(handle);

#else
#endif

	return true;
}

bool OvrFileTools::TraverseFiles(const char* path, void(*FileFunc)(void*, const char*, FileType), void* handlePtr, const unsigned int filePathOffset)
{
#ifdef _MSC_VER
	_finddata_t file_info;
	FileType type;

	OvrString rootPath;
	OvrString filePath;
	OvrString tempPath(path);

	if (!CheckPathLastIsSlash(path))
	{
		tempPath.AppendString("/");
	}

	rootPath.SetString(tempPath.GetString());

	unsigned int offset = rootPath.GetStringSize() > filePathOffset ? filePathOffset : rootPath.GetStringSize();

	/*  --------------定义后面的后缀为*.exe，*.txt等来查找特定后缀的文件，*.*是通配符，匹配所有类型,路径连接符左斜杠/，可跨平台 -------------- */
	tempPath.AppendString("*.*");

	/*  --------------打开文件查找句柄 --------------  */
	int handle = (int)_findfirst(tempPath.GetString(), &file_info);

	if (handle == -1)
	{
		return false;
	}

	do
	{
		if (file_info.attrib == _A_SUBDIR)
		{
			/*  --------------排除上层目录 -------------- **/
			if (strcmp(file_info.name, "..") == 0 || strcmp(file_info.name, ".") == 0)
			{
				continue;
			}

			type = Type_Dir;
		}
		else
		{
			type = Type_File;
		}

		if (FileFunc != NULL)
		{
			filePath.SetString(rootPath.GetString());
			filePath.AppendString(file_info.name);
			FileFunc(handlePtr, filePath.GetString() + offset, type);
		}

	} while (_findnext(handle, &file_info) == 0);

	_findclose(handle);

#else
#endif

	return true;
}

bool OvrFileTools::DfsFolder(const char* path, const char* exName, void(*FileFunc)(void*, const char*), void* handlePtr, const unsigned int filePathOffset)
{
	if (path == NULL)
	{
		return false;
	}

	unsigned int rootPathLen = (unsigned int)strlen(path);
	unsigned int offset = rootPathLen > filePathOffset ? filePathOffset : rootPathLen;

	return DfsFolderRecursion(path, exName, FileFunc, handlePtr, offset);
}


/*获取目录中的所有文件 不包含子目录*/
ovr::uint32	OvrFileTools::GetFilesInDir(const OvrString& a_dir, std::list<OvrString>& a_file_name_list)
{
	if (!IsFileExist(a_dir))
	{
		OvrLogE("ovr_base", "OvrFileTools::GetFilesInDir dir(%s) is not exist", a_dir.GetStringConst());
		return 0;
	}

	OvrString dir_path = a_dir;
	if (!CheckPathLastIsSlash(a_dir.GetStringConst()))
	{
		dir_path.AppendString("/");
	}

	dir_path.AppendString("*.*");

#if defined(OVR_OS_WIN32)
	intptr_t handle;
	_finddata_t findData;

	handle = _findfirst(dir_path.GetStringConst(), &findData);    // 查找目录中的第一个文件
	if (handle == -1)
	{
		return 0;
	}

	do
	{
		if (findData.attrib & _A_SUBDIR)    // 是否是子目录并且不为"."或".."
		{
			if (strcmp(findData.name, ".") == 0
				&& strcmp(findData.name, "..") == 0) 
			{
			}
		}
		else 
		{
			a_file_name_list.push_back(findData.name);
		}
	} while (_findnext(handle, &findData) == 0);    // 查找目录中的下一个文件

	_findclose(handle);    // 关闭搜索句柄
#else
	DIR *dir = nullptr;
	struct dirent *ptr = nullptr;

	if ((dir = opendir(a_dir.GetStringConst())) == nullptr)
	{
		OvrLogE("ovr_base", "OvrFileTools::GetFilesInDir open dir(%s) failed", a_dir.GetStringConst());
		return 0;
	}

	while ((ptr = readdir(dir)) != NULL)
	{
		if (ptr->d_type == DT_REG)
		{
            a_file_name_list.push_back(ptr->d_name);
		}
	}
	closedir(dir);
#endif
	return (ovr::uint32)a_file_name_list.size();
}


bool OvrFileTools::MakeDir( const char* path )
{
	if ( ovr_access( path, 0 ) != 0 )
	{
#ifdef _MSC_VER
        if ( _mkdir( path ) != 0 )
#else
        if ( mkdir( path, 775 ) != 0 )
#endif
		{
			return false;
		}
	}

	return true;
}

bool OvrFileTools::InitDir( const char* path )
{
	char* tmp = ( char* ) path;
	int index = 0;
	unsigned int len = ( unsigned int )strlen( path ) + 1;
	char* buffer = ( char* ) malloc( len );

	if ( buffer == NULL )
	{
		return false;
	}

	memset( buffer, 0, len );

	while ( *tmp )
	{
		if ( *tmp == '\\' || *tmp == '/' )
		{

			//linux上会出现这个问题 根目录为 /
			if (strlen(buffer) > 0 &&  !MakeDir( buffer ) )
			{
				free( buffer );
				return false;
			}
		}

		buffer[index] = *tmp;

		index++;
		tmp++;
	}

	free( buffer );

	return true;
}

bool OvrFileTools::FilterByExten(const char* fileName, const char* exName)
{
	if (strcmp(exName, "*.*") == 0 || strcmp(exName, "*") == 0 || exName == NULL)
	{
		return true;
	}

	const char* ext = strrchr(fileName, '.');

	if (ext != NULL)
	{
		ext++;

		if (*ext != 0)
		{
			char temp1[32] = { 0 };
			char temp2[32] = { 0 };

			int len1 = (int)strlen(ext);
			int len2 = (int)strlen(exName);

			if (len1 < 32 && len2 < 32)
			{
				for (int i = 0; i < len1; i++)
				{
					temp1[i] = ( char )tolower(ext[i]);
				}

				for (int i = 0; i < len1; i++)
				{
					temp2[i] = ( char )tolower(exName[i]);
				}

				if (strcmp(temp1, temp2) == 0)
				{
					return true;
				}
			}
		}
	}

	return false;
}

void OvrFileTools::TransformSlash(OvrString& a_string)
{
	for (ovr::uint32 index = 0; index < a_string.GetStringSize(); index++)
	{
		if (a_string[index] == '\\')
		{
			a_string[index] = '/';
		}
	}
}

bool OvrFileTools::DfsFolderRecursion(const char* path, const char* exName, void(*FileFunc)(void*, const char*), void* handlePtr, const  unsigned int rootPathLen)
{
#ifdef _MSC_VER
	_finddata_t file_info;

	OvrString filePath;
	OvrString rootPath;
	OvrString tempPath(path);

	if (!CheckPathLastIsSlash(path))
	{
		tempPath.AppendString("/");
	}

	rootPath.SetString(tempPath.GetString());

	/*  --------------定义后面的后缀为*.exe，*.txt等来查找特定后缀的文件，*.*是通配符，匹配所有类型,路径连接符左斜杠/，可跨平台 -------------- */
	tempPath.AppendString("*.*");

	/*  --------------打开文件查找句柄 --------------  */
	int handle = (int)_findfirst(tempPath.GetString(), &file_info);

	if (handle == -1)
	{
		return false;
	}

	do
	{
		if (file_info.attrib == _A_SUBDIR)
		{
			/*  --------------递归遍历子目录, 必须排除上层目录 -------------- **/
			if (strcmp(file_info.name, "..") != 0 && strcmp(file_info.name, ".") != 0)
			{
				filePath.SetString(rootPath.GetString());
				filePath.AppendString(file_info.name);

				DfsFolderRecursion(filePath.GetString(), exName, FileFunc, handlePtr, rootPathLen);
			}
		}
		else
		{
			if (FileFunc != NULL)
			{
				if (FilterByExten(file_info.name, exName))
				{
					filePath.SetString(rootPath.GetString());
					filePath.AppendString(file_info.name);

					FileFunc(handlePtr, filePath.GetString() + rootPathLen);
				}
			}
		}
	} while (_findnext(handle, &file_info) == 0);

	_findclose(handle);

#else
#endif

	return true;
}


bool OvrFileTools::DeleteFiles(const OvrString& a_path)
{
    //OvrLogD("ovr_base", "DeleteFiles begin = %s", a_path.GetStringConst());
	OvrString delete_path = a_path;
	TransformSlash(delete_path);
    if(!CheckPathLastIsSlash(delete_path.GetStringConst()))
    {
        delete_path.AppendChar('/');
    }

   // OvrLogD("ovr_base", "DeleteFiles 1 new path = %s", delete_path.GetStringConst());
#ifdef OVR_OS_WIN32
	intptr_t hFile = 0;
	struct _finddata_t fileInfo;
	std::string pathName;
	std::string fileName;
	int result = 0;
	// \\* 代表要遍历所有的类型
	if ((hFile = _findfirst(pathName.assign(delete_path.GetStringConst()).append("\\*").c_str(), &fileInfo)) == -1)
	{
		return true;
	}
	do
	{
		//判断文件的属性是文件夹还是文件
		if(strcmp(fileInfo.name,".")==0 || strcmp(fileInfo.name, "..")==0 )
			continue;
		
		fileName.assign(delete_path.GetStringConst()).append("//").append(fileInfo.name);
		if (fileInfo.attrib == _A_SUBDIR)
		{
			DeleteFiles(fileName.c_str());
		}
		else
		{
			remove(fileName.c_str());
		}

	} while (_findnext(hFile, &fileInfo) == 0);
	_rmdir(delete_path.GetStringConst());
	_findclose(hFile);
#else
	DIR *dir = nullptr;
	struct dirent *ptr = nullptr;

	if ((dir = opendir(delete_path.GetStringConst())) == nullptr)
	{
        OvrLogE("ovr_base", "opendir failed = %s", delete_path.GetStringConst());
		return false;
	}

   // OvrLogD("ovr_base", "DeleteFiles 2 ");
	if((ptr = readdir(dir)) != NULL)
	{
		do
		{
			OvrString item_path(delete_path);
			item_path.AppendString(ptr->d_name);

			//OvrLogD("ovr_base", "DeleteFiles 2 0  item_path = %s,delete path = %s, name = %s",item_path.GetStringConst(), delete_path.GetStringConst(), ptr->d_name);
			if (ptr->d_type == DT_DIR)
			{
				if (strcmp(ptr->d_name, ".") == 0 || strcmp(ptr->d_name, "..") == 0)
				{
					continue;
				}

				//OvrLogD("ovr_base", "DeleteFiles 3 item_path = %s", item_path.GetStringConst());
				DeleteFiles(item_path.GetStringConst());
			}
			else
			{
				//file
				//OvrLogD("ovr_base", "remove item_path = %s", item_path.GetStringConst());
				remove(item_path.GetStringConst());
			}

		}while ((ptr = readdir(dir)) != NULL);
	}
	//dir is null
	closedir(dir);
	remove(delete_path.GetStringConst());
#endif

    //OvrLogD("ovr_base", "DeleteFiles end ");
	return true;
}



