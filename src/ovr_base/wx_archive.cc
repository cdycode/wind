﻿#include "wx_archive.h"

OvrArchive::OvrArchive(OvrIOInerface* a_io, bool a_little ):is_host_little_endian(a_little), io_handle(a_io)
{
}

ovr::uint32 OvrArchive::GetLength()
{
	if (io_handle == NULL)
	{
		return 0;
	}

	return io_handle->GetLength();
}

bool OvrArchive::Seek(ovr::uint32 pos)
{
	if (io_handle == NULL)
	{
		return false;
	}

	io_handle->SetPosition(pos);

	return true;
}

ovr::uint32 OvrArchive::GetPosition()
{
	if (io_handle == NULL)
	{
		return 0;
	}

	return io_handle->GetPosition();
}