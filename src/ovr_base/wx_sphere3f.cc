#include "wx_sphere3f.h"
#include <cmath>

OvrSphere3f::OvrSphere3f()
	:_pos(0),_radius(0)
{
	
}

OvrSphere3f::OvrSphere3f(float a_r)
	:_pos(0),_radius(a_r)
{
	
}

OvrSphere3f::OvrSphere3f(const OvrVector3& a_pos, float a_r)
	:_pos(a_pos),_radius(a_r)
{
	
}

OvrVector3 OvrSphere3f::GetPos() const
{
	return _pos;
}

float OvrSphere3f::GetRadius() const
{
	return _radius;
}
  
void OvrSphere3f::SetPos(const OvrVector3& a_pos)
{
	_pos = a_pos;
}

void OvrSphere3f::SetRadius(float a_r)
{
	_radius = a_r;
}

int OvrSphere3f::Intersect(float& ao_dist, const OvrVector3& a_ray_origin, const OvrVector3& a_ray_dir)
{
	auto r_count_num = 0;
	OvrVector3 t_diff = a_ray_origin - _pos;
	auto t_fa0 = t_diff*t_diff - _radius*_radius;
	auto t_fa1 = a_ray_dir*t_diff;
	if (t_fa0 <= float(0))//inside the sphere.so there must be intersect.
	{
		r_count_num = 1;
		auto t_fdiscr = t_fa1*t_fa1 - t_fa0;
		if (t_fdiscr >= OVR_TOLERANCE)
		{
			auto froot = sqrt(t_fdiscr);
			if (t_fa1 >= 0.0)
			{
				ao_dist = (-t_fa1) - froot;
			}
			else
			{
				ao_dist = (-t_fa1) + froot;
			}
		}
		else
		{
			ao_dist = 0;
		}
		return r_count_num;
	}
	else if (t_fa1 > float(0))
	{
		return r_count_num;
	}

	auto fdiscr = t_fa1*t_fa1 - t_fa0;
	if (fdiscr >= OVR_TOLERANCE)
	{
		auto t_froot = sqrt(fdiscr);
		auto t0 = (-t_fa1) - t_froot;
		//float t1 = (-fa1) + froot;
		ao_dist = t0;
		r_count_num = 2;
	}
	else
	{
		if (fdiscr >= -OVR_TOLERANCE)
		{
			auto t0 = -t_fa1;
			ao_dist = t0;
			r_count_num = 1;
		}
		//else case,the ray is not intersect with the sphere.point fay away.
		//for closest point calculate is expensive,so we do not do it here.
	}
	return r_count_num;
}




