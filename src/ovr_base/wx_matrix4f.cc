﻿#include "wx_matrix4f.h"
#include <math.h>
#include <string.h>
#include "wx_math_configure.h"
 
OvrMatrix4::OvrMatrix4()
	:_x(float(1),float(0),float(0),float(0)),_y(float(0),float(1),float(0),float(0)),_z(float(0),float(0),float(1),float(0)),_d(float(0),float(0),float(0),float(1))
{

}

 
OvrMatrix4::OvrMatrix4(float a_v)
	:_x(a_v,float(0),float(0),float(0)),_y(float(0),a_v,float(0),float(0)),_z(float(0),float(0),a_v,float(0)),_d(float(0),float(0),float(0),a_v)
{

}

OvrMatrix4::OvrMatrix4(const OvrMatrix4& a_matrix4) 
{
	for (int i = 0; i < 16; i++)
	{
		_m[i] = a_matrix4._m[i];
	}
}
 
OvrMatrix4::OvrMatrix4(const OvrVector4& a_v)
	:_x(a_v[0],float(0),float(0),float(0)),_y(float(0),a_v[1],float(0),float(0)),_z(float(0),float(0),a_v[2],float(0)),_d(float(0),float(0),float(0),a_v[3])
{

}

 
OvrMatrix4::OvrMatrix4(const OvrVector4& a_v0,const OvrVector4& a_v1,const OvrVector4& a_v2,const OvrVector4& a_v3)
	:_x(a_v0),_y(a_v1),_z(a_v2),_d(a_v3)
{

}

 
OvrMatrix4::OvrMatrix4(float a_m00,float a_m01,float a_m02,float a_m03,
	float a_m10,float a_m11,float a_m12,float a_m13,
	float a_m20,float a_m21,float a_m22,float a_m23,
	float a_m30,float a_m31,float a_m32,float a_m33)
	:_x(a_m00,a_m01,a_m02,a_m03),
	_y(a_m10,a_m11,a_m12,a_m13),
	_z(a_m20,a_m21,a_m22,a_m23),
	_d(a_m30,a_m31,a_m32,a_m33)
{

}

 
OvrMatrix4::~OvrMatrix4()
{

}

float& OvrMatrix4::operator[](int a_id)
{
	return _m[a_id];
}

 
const float& OvrMatrix4::operator[](int a_id) const
{
	return _m[a_id];
}

 
float& OvrMatrix4::operator()(int a_row,int a_col)
{
	return _mm[a_row][a_col];
}

 
const float& OvrMatrix4::operator()(int a_row,int a_col) const
{
	return _mm[a_row][a_col];
}

 
void OvrMatrix4::operator=(const OvrMatrix4& a_mat4)
{
	for (int i = 0; i < 16; ++i)
	{
		_m[i] = a_mat4._m[i];
	}
}

OvrVector4 OvrMatrix4::operator*(const OvrVector4& a_v4) const
{
	return OvrVector4(_mm[0][0] * a_v4[0] + _mm[1][0] * a_v4[1] + _mm[2][0] * a_v4[2] + _mm[3][0] * a_v4[3],
		_mm[0][1] * a_v4[0] + _mm[1][1] * a_v4[1] + _mm[2][1] * a_v4[2] + _mm[3][1] * a_v4[3],
		_mm[0][2] * a_v4[0] + _mm[1][2] * a_v4[1] + _mm[2][2] * a_v4[2] + _mm[3][2] * a_v4[3],
		_mm[0][3] * a_v4[0] + _mm[1][3] * a_v4[1] + _mm[2][3] * a_v4[2] + _mm[3][3] * a_v4[3]);
	//return OvrVector4(_x*a_v4,_y*a_v4,_z*a_v4,_d*a_v4);
}

 
OvrVector3 OvrMatrix4::operator*(const OvrVector3& a_vec3) const
{
	return OvrVector3(_mm[0][0] * a_vec3[0] + _mm[1][0] * a_vec3[1] + _mm[2][0] * a_vec3[2],
		_mm[0][1] * a_vec3[0] + _mm[1][1] * a_vec3[1] + _mm[2][1] * a_vec3[2],
		_mm[0][2] * a_vec3[0] + _mm[1][2] * a_vec3[1] + _mm[2][2] * a_vec3[2]);
}

 
OvrMatrix4 OvrMatrix4::operator*(const float& a_s) const
{
	return OvrMatrix4(_m[0]*a_s,_m[1]*a_s,_m[2]*a_s,_m[3]*a_s,
		_m[4]*a_s,_m[5]*a_s,_m[6]*a_s,_m[7]*a_s,
		_m[8]*a_s,_m[9]*a_s,_m[10]*a_s,_m[11]*a_s,
		_m[12]*a_s,_m[13]*a_s,_m[14]*a_s,_m[15]*a_s);
}

 
void OvrMatrix4::operator*=(const float& a_s)
{
	_m[0] *= a_s;
	_m[1] *= a_s;
	_m[2] *= a_s;
	_m[3] *= a_s;
	_m[4] *= a_s;
	_m[5] *= a_s;
	_m[6] *= a_s;
	_m[7] *= a_s;
	_m[8] *= a_s;
	_m[9] *= a_s;
	_m[10] *= a_s;
	_m[11] *= a_s;
	_m[12] *= a_s;
	_m[13] *= a_s;
	_m[14] *= a_s;
	_m[15] *= a_s;
}

 
OvrMatrix4 OvrMatrix4::operator/(const float& a_s) const
{
	return OvrMatrix4(_m[0]/a_s,_m[1]/a_s,_m[2]/a_s,_m[3]/a_s,
		_m[4]/a_s,_m[5]/a_s,_m[6]/a_s,_m[7]/a_s,
		_m[8]/a_s,_m[9]/a_s,_m[10]/a_s,_m[11]/a_s,
		_m[12]/a_s,_m[13]/a_s,_m[14]/a_s,_m[15]/a_s);
}

 
void OvrMatrix4::operator/=(const float& a_s)
{
	_m[0] /= a_s;
	_m[1] /= a_s;
	_m[2] /= a_s;
	_m[3] /= a_s;
	_m[4] /= a_s;
	_m[5] /= a_s;
	_m[6] /= a_s;
	_m[7] /= a_s;
	_m[8] /= a_s;
	_m[9] /= a_s;
	_m[10] /= a_s;
	_m[11] /= a_s;
	_m[12] /= a_s;
	_m[13] /= a_s;
	_m[14] /= a_s;
	_m[15] /= a_s;
}

 
OvrMatrix4 OvrMatrix4::operator*(const OvrMatrix4& a_mat4)const
{
	OvrMatrix4	t_mat;
	for (int i=0;i<4;++i)
	{
		for (int j=0;j<4;++j)
		{
			t_mat(i,j) = _mm[i][0]*a_mat4(0,j) + _mm[i][1]*a_mat4(1,j) + _mm[i][2]*a_mat4(2,j) + _mm[i][3]*a_mat4(3,j);
		}
	}
	return t_mat;
}

 
OvrMatrix4 OvrMatrix4::operator+(const OvrMatrix4& a_mat4)const
{
	return OvrMatrix4(_m[0] + a_mat4[0],_m[1] + a_mat4[1],_m[2] + a_mat4[2],_m[3] + a_mat4[3],
						_m[4] + a_mat4[4],_m[5] + a_mat4[5],_m[6] + a_mat4[6],_m[7] + a_mat4[7],
						_m[8] + a_mat4[8],_m[9] + a_mat4[9],_m[10] + a_mat4[10],_m[11] + a_mat4[11],
						_m[12] + a_mat4[12],_m[13] + a_mat4[13],_m[14] + a_mat4[14],_m[15] + a_mat4[15]);
}

 
void OvrMatrix4::operator+=(const OvrMatrix4& a_mat4)
{
	_m[0] += a_mat4[0];_m[1] += a_mat4[1];_m[2] += a_mat4[2];_m[3] += a_mat4[3];
	_m[4] += a_mat4[4];_m[5] += a_mat4[5];_m[6] += a_mat4[6];_m[7] += a_mat4[7];
	_m[8] += a_mat4[8];_m[9] += a_mat4[9];_m[10] += a_mat4[10];_m[11] += a_mat4[11];
	_m[12] += a_mat4[12];_m[13] += a_mat4[13];_m[14] += a_mat4[14];_m[15] += a_mat4[15];
}

 
OvrMatrix4 OvrMatrix4::operator-(const OvrMatrix4& a_mat4)const
{
	return OvrMatrix4(_m[0] - a_mat4[0],_m[1] - a_mat4[1],_m[2] - a_mat4[2],_m[3] - a_mat4[3],
		_m[4] - a_mat4[4],_m[5] - a_mat4[5],_m[6] - a_mat4[6],_m[7] - a_mat4[7],
		_m[8] - a_mat4[8],_m[9] - a_mat4[9],_m[10] - a_mat4[10],_m[11] - a_mat4[11],
		_m[12] - a_mat4[12],_m[13] - a_mat4[13],_m[14] - a_mat4[14],_m[15] - a_mat4[15]);
}

 
void OvrMatrix4::operator-=(const OvrMatrix4& a_mat4)
{
	_m[0] -= a_mat4[0];_m[1] -= a_mat4[1];_m[2] -= a_mat4[2];_m[3] -= a_mat4[3];
	_m[4] -= a_mat4[4];_m[5] -= a_mat4[5];_m[6] -= a_mat4[6];_m[7] -= a_mat4[7];
	_m[8] -= a_mat4[8];_m[9] -= a_mat4[9];_m[10] -= a_mat4[10];_m[11] -= a_mat4[11];
	_m[12] -= a_mat4[12];_m[13] -= a_mat4[13];_m[14] -= a_mat4[14];_m[15] -= a_mat4[15];
}

 
OvrVector3 OvrMatrix4::transform_vector(const OvrVector3& a_vec3) const
{
	return OvrVector3(_mm[0][0]*a_vec3[0] + _mm[1][0]*a_vec3[1] + _mm[2][0]*a_vec3[2],
		_mm[0][1]*a_vec3[0] + _mm[1][1]*a_vec3[1] + _mm[2][1]*a_vec3[2],
		_mm[0][2]*a_vec3[0] + _mm[1][2]*a_vec3[1] + _mm[2][2]*a_vec3[2]);
}

 
OvrVector3 OvrMatrix4::transform_point(const OvrVector3& a_vec3) const
{
	OvrVector4 t(_mm[0][0]*a_vec3[0] + _mm[1][0]*a_vec3[1] + _mm[2][0]*a_vec3[2] + _mm[3][0],
		_mm[0][1]*a_vec3[0] + _mm[1][1]*a_vec3[1] + _mm[2][1]*a_vec3[2] + _mm[3][1],
		_mm[0][2]*a_vec3[0] + _mm[1][2]*a_vec3[1] + _mm[2][2]*a_vec3[2] + _mm[3][2],
		_mm[0][3]*a_vec3[0] + _mm[1][3]*a_vec3[1] + _mm[2][3]*a_vec3[2] + _mm[3][3]);
	return OvrVector3(t[0]/t[3],t[1]/t[3],t[2]/t[3]);
}

 
float OvrMatrix4::determinant() const
{
	return float(1);
}

 
bool OvrMatrix4::inverse()
{
	float a0 = _m[0] * _m[5] - _m[1] * _m[4];
	float a1 = _m[0] * _m[6] - _m[2] * _m[4];
	float a2 = _m[0] * _m[7] - _m[3] * _m[4];
	float a3 = _m[1] * _m[6] - _m[2] * _m[5];
	float a4 = _m[1] * _m[7] - _m[3] * _m[5];
	float a5 = _m[2] * _m[7] - _m[3] * _m[6];
	float b0 = _m[8] * _m[13] - _m[9] * _m[12];
	float b1 = _m[8] * _m[14] - _m[10] * _m[12];
	float b2 = _m[8] * _m[15] - _m[11] * _m[12];
	float b3 = _m[9] * _m[14] - _m[10] * _m[13];
	float b4 = _m[9] * _m[15] - _m[11] * _m[13];
	float b5 = _m[10] * _m[15] - _m[11] * _m[14];

	// Calculate the determinant.
	float det = a0 * b5 - a1 * b4 + a2 * b3 + a3 * b2 - a4 * b1 + a5 * b0;

	if (fabs(det) < LVR_TOLERANCE)
	{
		return false;
	}
	float tm[16];
	memcpy(tm,_m,16*sizeof(float));
	float t_det_under_1 = (float(1)/det);
	_m[0]  = (tm[5] * b5 - tm[6] * b4 + tm[7] * b3)*t_det_under_1;
	_m[1]  = (-tm[1] * b5 + tm[2] * b4 - tm[3] * b3)*t_det_under_1;
	_m[2]  = (tm[13] * a5 - tm[14] * a4 + tm[15] * a3)*t_det_under_1;
	_m[3]  = (-tm[9] * a5 + tm[10] * a4 - tm[11] * a3)*t_det_under_1;

	_m[4]  = (-tm[4] * b5 + tm[6] * b2 - tm[7] * b1)*t_det_under_1;
	_m[5]  = (tm[0] * b5 - tm[2] * b2 + tm[3] * b1)*t_det_under_1;
	_m[6]  = (-tm[12] * a5 + tm[14] * a2 - tm[15] * a1)*t_det_under_1;
	_m[7]  = (tm[8] * a5 - tm[10] * a2 + tm[11] * a1)*t_det_under_1;

	_m[8]  = (tm[4] * b4 - tm[5] * b2 + tm[7] * b0)*t_det_under_1;
	_m[9]  = (-tm[0] * b4 + tm[1] * b2 - tm[3] * b0)*t_det_under_1;
	_m[10] = (tm[12] * a4 - tm[13] * a2 + tm[15] * a0)*t_det_under_1;
	_m[11] = (-tm[8] * a4 + tm[9] * a2 - tm[11] * a0)*t_det_under_1;

	_m[12] = (-tm[4] * b3 + tm[5] * b1 - tm[6] * b0)*t_det_under_1;
	_m[13] = (tm[0] * b3 - tm[1] * b1 + tm[2] * b0)*t_det_under_1;
	_m[14] = (-tm[12] * a3 + tm[13] * a1 - tm[14] * a0)*t_det_under_1;
	_m[15] = (tm[8] * a3 - tm[9] * a1 + tm[10] * a0)*t_det_under_1;
	return true;
}

 
bool OvrMatrix4::get_inverse(OvrMatrix4& ao_mat4) const
{
	float a0 = _m[0] * _m[5] - _m[1] * _m[4];
	float a1 = _m[0] * _m[6] - _m[2] * _m[4];
	float a2 = _m[0] * _m[7] - _m[3] * _m[4];
	float a3 = _m[1] * _m[6] - _m[2] * _m[5];
	float a4 = _m[1] * _m[7] - _m[3] * _m[5];
	float a5 = _m[2] * _m[7] - _m[3] * _m[6];
	float b0 = _m[8] * _m[13] - _m[9] * _m[12];
	float b1 = _m[8] * _m[14] - _m[10] * _m[12];
	float b2 = _m[8] * _m[15] - _m[11] * _m[12];
	float b3 = _m[9] * _m[14] - _m[10] * _m[13];
	float b4 = _m[9] * _m[15] - _m[11] * _m[13];
	float b5 = _m[10] * _m[15] - _m[11] * _m[14];

	// Calculate the determinant.
	float det = a0 * b5 - a1 * b4 + a2 * b3 + a3 * b2 - a4 * b1 + a5 * b0;

	if (fabs(det) < LVR_TOLERANCE)
	{
		return false;
	}
	ao_mat4[0]  = _m[5] * b5 - _m[6] * b4 + _m[7] * b3;
	ao_mat4[1]  = -_m[1] * b5 + _m[2] * b4 - _m[3] * b3;
	ao_mat4[2]  = _m[13] * a5 - _m[14] * a4 + _m[15] * a3;
	ao_mat4[3]  = -_m[9] * a5 + _m[10] * a4 - _m[11] * a3;

	ao_mat4[4]  = -_m[4] * b5 + _m[6] * b2 - _m[7] * b1;
	ao_mat4[5]  = _m[0] * b5 - _m[2] * b2 + _m[3] * b1;
	ao_mat4[6]  = -_m[12] * a5 + _m[14] * a2 - _m[15] * a1;
	ao_mat4[7]  = _m[8] * a5 - _m[10] * a2 + _m[11] * a1;

	ao_mat4[8]  = _m[4] * b4 - _m[5] * b2 + _m[7] * b0;
	ao_mat4[9]  = -_m[0] * b4 + _m[1] * b2 - _m[3] * b0;
	ao_mat4[10] = _m[12] * a4 - _m[13] * a2 + _m[15] * a0;
	ao_mat4[11] = -_m[8] * a4 + _m[9] * a2 - _m[11] * a0;

	ao_mat4[12] = -_m[4] * b3 + _m[5] * b1 - _m[6] * b0;
	ao_mat4[13] = _m[0] * b3 - _m[1] * b1 + _m[2] * b0;
	ao_mat4[14] = -_m[12] * a3 + _m[13] * a1 - _m[14] * a0;
	ao_mat4[15] = _m[8] * a3 - _m[9] * a1 + _m[10] * a0;
	ao_mat4 = ao_mat4*(float(1)/det);
	return true;
}

 
OvrMatrix4 OvrMatrix4::get_transpose() const
{
	return OvrMatrix4(_mm[0][0],_mm[1][0],_mm[2][0],_mm[3][0],
						_mm[0][1],_mm[1][1],_mm[2][1],_mm[3][1],
						_mm[0][2],_mm[1][2],_mm[2][2],_mm[3][2],
						_mm[0][3],_mm[1][3],_mm[2][3],_mm[3][3]);
}

 
void OvrMatrix4::transpose()
{
	lvr_swap(_mm[0][1],_mm[1][0]);
	lvr_swap(_mm[0][2],_mm[2][0]);
	lvr_swap(_mm[0][3],_mm[3][0]);
	lvr_swap(_mm[1][2],_mm[2][1]);
	lvr_swap(_mm[1][3],_mm[3][1]);
	lvr_swap(_mm[2][3],_mm[3][2]);
}

 
OvrQuaternion OvrMatrix4::to_quaternion() const
{
	OvrQuaternion	r_q;
	return r_q;
}

 
void OvrMatrix4::from_quaternion(const OvrQuaternion& a_q)
{
	const float& x = a_q[1];
	const float& y = a_q[2];
	const float& z = a_q[3];
	const float& w = a_q[0];
	float x2 = x + x;
	float y2 = y + y;
	float z2 = z + z;

	float ww = w * w;
	float xx = x * x;
	float yy = y * y;
	float zz = z * z;

	//float xx2 = x * x2;
	//float yy2 = y * y2;
	//float zz2 = z * z2;
	float xy2 = x * y2;
	float xz2 = x * z2;
	float yz2 = y * z2;
	float wx2 = w * x2;
	float wy2 = w * y2;
	float wz2 = w * z2;

	_m[0] = ww + xx - yy - zz;
	_m[4] = xy2 + wz2;
	_m[8] = xz2 - wy2;
	_m[12] = float(0.0);

	_m[1] = xy2 - wz2;
	_m[5] = ww - xx + yy - zz;
	_m[9] = yz2 + wx2;
	_m[13] = float(0.0);

	_m[2] = xz2 + wy2;
	_m[6] = yz2 - wx2;
	_m[10] = ww - xx - yy + zz;
	_m[14] = float(0.0);

	_m[3] = float(0.0);
	_m[7] = float(0.0);
	_m[11] = float(0.0);
	_m[15] = float(1.0);

	(*this).transpose();

}

 
bool OvrMatrix4::decompose(OvrVector3* ao_scale,OvrQuaternion* ao_rotation,OvrVector3* ao_translation) const
{
	if (ao_translation)
	{
		// Extract the translation.
		(*ao_translation)[0] = _m[12];
		(*ao_translation)[1] = _m[13];
		(*ao_translation)[2] = _m[14];
	}

	// Nothing left to do.
	if (ao_scale == NULL && ao_rotation == NULL)
		return true;

	// Extract the scale.
	// This is simply the length of each axis (row/column) in the matrix.
	OvrVector3 xaxis(_m[0], _m[1], _m[2]);
	float scaleX = xaxis.length();

	OvrVector3 yaxis(_m[4], _m[5], _m[6]);
	float scaleY = yaxis.length();

	OvrVector3 zaxis(_m[8], _m[9], _m[10]);
	float scaleZ = zaxis.length();

	// Determine if we have a negative scale (true if determinant is less than zero).
	// In this case, we simply negate a single axis of the scale.
	float det = determinant();
	if (det < 0)
		scaleZ = -scaleZ;

	if (ao_scale)
	{
		(*ao_scale)[0] = scaleX;
		(*ao_scale)[1] = scaleY;
		(*ao_scale)[2] = scaleZ;
	}

	// Nothing left to do.
	if (ao_rotation == NULL)
		return true;

	// Scale too close to zero, can't_ decompose a_rotation.
	if (scaleX < LVR_TOLERANCE || scaleY < LVR_TOLERANCE || fabs(scaleZ) < LVR_TOLERANCE)
		return false;

	float rn;

	// Factor the scale out of the matrix axes.
	rn = float(1.0) / scaleX;
	xaxis[0] *= rn;
	xaxis[1] *= rn;
	xaxis[2] *= rn;

	rn = float(1.0) / scaleY;
	yaxis[0] *= rn;
	yaxis[1] *= rn;
	yaxis[2] *= rn;

	rn = float(1.0) / scaleZ;
	zaxis[0] *= rn;
	zaxis[1] *= rn;
	zaxis[2] *= rn;

	float& ao_x = (*ao_rotation)[1];
	float& ao_y = (*ao_rotation)[2];
	float& ao_z = (*ao_rotation)[3];
	float& ao_w = (*ao_rotation)[0];
	// Now calculate the a_rotation from the resulting matrix (axes).
	float trace = xaxis[0] + yaxis[1] + zaxis[2] + float(1.0);

	if (trace > LVR_TOLERANCE)
	{
		float s = float(0.5) / (float)sqrt(trace);
		ao_w = float(0.25) / s;
		ao_x = (yaxis[2] - zaxis[1]) * s;
		ao_y = (zaxis[0] - xaxis[2]) * s;
		ao_z = (xaxis[1] - yaxis[0]) * s;
	}
	else
	{
		// Note: since xaxis, yaxis, and zaxis are normalized, 
		// we will never divide by zero in the code below.
		if (xaxis[0] > yaxis[1] && xaxis[0] > zaxis[2])
		{
			float s = float(0.5) / (float)sqrt(float(1.0) + xaxis[0] - yaxis[1] - zaxis[2]);
			ao_w = (yaxis[2] - zaxis[1]) * s;
			ao_x = float(0.25) / s;
			ao_y = (yaxis[0] + xaxis[1]) * s;
			ao_z = (zaxis[0] + xaxis[2]) * s;
		}
		else if (yaxis[1] > zaxis[2])
		{
			float s = float(0.5) / (float)sqrt(float(1.0) + yaxis[1] - xaxis[0] - zaxis[2]);
			ao_w = (zaxis[0] - xaxis[2]) * s;
			ao_x = (yaxis[0] + xaxis[1]) * s;
			ao_y = float(0.25) / s;
			ao_z = (zaxis[1] + yaxis[2]) * s;
		}
		else
		{
			float s = float(0.5) / (float)sqrt(float(1.0) + zaxis[2] - xaxis[0] - yaxis[1] );
			ao_w = (xaxis[1] - yaxis[0] ) * s;
			ao_x = (zaxis[0] + xaxis[2] ) * s;
			ao_y = (zaxis[1] + yaxis[2] ) * s;
			ao_z = float(0.25) / s;
		}
	}
	return true;
}

 
void OvrMatrix4::set_translation(const OvrVector3& a_trans)
{
	_m[12] = a_trans[0];
	_m[13] = a_trans[1];
	_m[14] = a_trans[2];
}

 
OvrVector3 OvrMatrix4::get_translation()
{
	return OvrVector3(_d[0],_d[1],_d[2]);
}

 
void OvrMatrix4::mul(const OvrMatrix4& a_mat0,const OvrMatrix4& a_mat1,OvrMatrix4& ao_mat)
{
	for (int i=0;i<4;++i)
	{
		for (int j=0;j<4;++j)
		{
			ao_mat(i,j) = a_mat0(i,0)*a_mat1(0,j) + a_mat0(i,1)*a_mat1(1,j) + a_mat0(i,2)*a_mat1(2,j) + a_mat0(i,3)*a_mat1(3,j);
		}
	}
}

 
OvrMatrix4 OvrMatrix4::make_translation_matrix(const OvrVector3& a_trans)
{
	return OvrMatrix4(float(1),float(0),float(0),float(0),
		float(0),float(1),float(0),float(0),
		float(0),float(0),float(1),float(0),
		a_trans[0],a_trans[1],a_trans[2],float(1));
}

 
OvrMatrix4 OvrMatrix4::axis_angle_rotate_by_center(const OvrVector3& a_rot_center,const OvrVector3& a_axis,float a_angle_rad)
{
	OvrMatrix4 rot_mat;
	if (fabs(a_angle_rad) < LVR_TOLERANCE)
	{
		rot_mat = OvrMatrix4();
		return rot_mat;
	}
	rot_mat = /*axis_angle_rotate*/make_rotation_matrix_around_axis(a_axis, a_angle_rad);
	//                   
	// set up translation matrix for moving v1 to origin
	//
	OvrMatrix4 m1, m2;
	m1 = OvrMatrix4();
	m2 = OvrMatrix4();

	m1(0, 3) = -a_rot_center[0];
	m1(1, 3) = -a_rot_center[1];
	m1(2, 3) = -a_rot_center[2];

	m2(0, 3) = a_rot_center[0];
	m2(1, 3) = a_rot_center[1];
	m2(2, 3) = a_rot_center[2];

	return m2*(rot_mat*m1);
}

 
OvrMatrix4 OvrMatrix4::make_scale_matrix_around_axis(const OvrVector3& a_axis,float a_s)
{
	return OvrMatrix4();
}


 
OvrMatrix4 OvrMatrix4::make_scale_matrix(const OvrVector3& a_scales)
{
	OvrMatrix4 mat;
	mat[0] = a_scales[0];
	mat[5] = a_scales[1];
	mat[10] = a_scales[2];
	return mat;
}

 
OvrMatrix4 OvrMatrix4::make_rotation_matrix_around_axis(const OvrVector3& a_axis,float a_angle_rad)
{
	float x = a_axis[0];
	float y = a_axis[1];
	float z = a_axis[2];
	// Make sure the input a_axis is normalized.
	float n = x*x + y*y + z*z;
	if (n != 1.0)
	{
		// Not normalized.
		n = (float)sqrt(n);
		// Prevent divide too close to zero.
		if (n > 1e-12)
		{
			n = 1.0f / n;
			x *= n;
			y *= n;
			z *= n;
		}
		else
		{
			//should return identity matrix,for there must be a wrong thing happen.
			return OvrMatrix4();
		}
	}

	float c = (float)cos(a_angle_rad);
	float s = (float)sin(a_angle_rad);

	float t_ = 1.0f - c;
	float tx = t_ * x;
	float ty = t_ * y;
	float tz = t_ * z;
	float txy = tx * y;
	float txz = tx * z;
	float tyz = ty * z;
	float sx = s * x;
	float sy = s * y;
	float sz = s * z;

	OvrMatrix4 dst;
	dst[0] = c + tx*x;
	dst[1] = txy + sz;
	dst[2] = txz - sy;
	dst[3] = 0.0f;

	dst[4] = txy - sz;
	dst[5] = c + ty*y;
	dst[6] = tyz + sx;
	dst[7] = 0.0f;

	dst[8] = txz + sy;
	dst[9] = tyz - sx;
	dst[10] = c + tz*z;
	dst[11] = 0.0f;

	dst[12] = 0.0f;
	dst[13] = 0.0f;
	dst[14] = 0.0f;
	dst[15] = 1.0f;
	return dst;
}

 
OvrMatrix4 OvrMatrix4::make_reflection_matrix_by_plane(const OvrVector3& a_normal,float a_dist_to_origin)
{
	float k = -2.0f * a_dist_to_origin;

	float nxy2 = -2*a_normal[0]*a_normal[1];
	float nxz2 = -2*a_normal[0]*a_normal[2];
	float nyz2 = -2*a_normal[1]*a_normal[2];
	float one_xx2 = 1.0f-2.0f*a_normal[0]*a_normal[0];
	float one_yy2 = 1.0f-2.0f*a_normal[1]*a_normal[1];
	float one_zz2 = 1.0f-2.0f*a_normal[2]*a_normal[2];
	return OvrMatrix4(
		one_xx2,    nxy2,   nxz2,   k*a_normal[0],
		nxy2, one_yy2,   nyz2,   k*a_normal[1],
		nxz2,    nyz2,one_zz2,   k*a_normal[2],
		float(0),    float(0),   float(0),      float(1.0) );
}

 
OvrMatrix4 OvrMatrix4::make_view_matrix(const OvrVector3& a_pos,const OvrVector3& a_target,const OvrVector3& a_up)
{
	OvrVector3 look = a_pos - a_target;
	look.normalize();
	OvrVector3 up(a_up);
	up.normalize();
	OvrVector3 right = up.cross(look);
	right.normalize();
	up = look.cross(right);
	return OvrMatrix4(
		right[0],up[0],look[0],float(0),
		right[1],up[1],look[1],float(0),
		right[2],up[2],look[2],float(0),
		-(right*a_pos),-(up*a_pos),-(look*a_pos),float(1)
		);
}

 
OvrMatrix4 OvrMatrix4::make_ortho_matrix(float a_width,float a_height,float a_near,float a_far)
{
	float f_n = float(1)/(a_far - a_near);
	return OvrMatrix4(
		float(2.0)/a_width,     (0),                     float(0),      float(0),
		float(0),     float(2.0)/a_height,                     float(0),      float(0),
		float(0),               float(0),                    float(-2.0)*f_n,      float(0),
		float(0),               float(0),   -(a_far+a_near)*f_n,    float(1.0) );
}

/* 
OvrMatrix4 OvrMatrix4::make_proj_matrix_rh(float a_view_angle_rad,float a_aspect_ratio,float a_near,float a_far)
{
	float f_n = float(1.0) / (a_far - a_near);
	float theta = a_view_angle_rad * float(0.5);

	float divisor = tan(theta);
	float factor = float(1.0) / divisor;

	return OvrMatrix4(
		(float(1.0) / a_aspect_ratio) * factor,float(0),     float(0),              float(0),
		float(0),  factor,                                  float(0),              float(0),
		float(0),  float(0),      (-(a_far + a_near)) * f_n,    float(-1.0),
		float(0),  float(0),   float(-2.0) * a_far * a_near * f_n,       float(0));
}*/

//for vr case
 
OvrMatrix4 OvrMatrix4::make_proj_matrix_rh(float a_view_angle_rad,float a_aspect_ratio,float a_near,float a_far)
{
	float f_n = float(1.0) / (a_near - a_far);
	float theta = a_view_angle_rad * float(0.5);

	float divisor = (float)tan(theta);
	float factor = float(1.0) / divisor;

	return OvrMatrix4(
		(float(1.0) / a_aspect_ratio) * factor,float(0),     float(0),              float(0),
		float(0),  factor,                                  float(0),              float(0),
		float(0),  float(0),      a_far * f_n,    float(-1.0),
		float(0),  float(0),   a_far * a_near * f_n,       float(0));
}

 
OvrMatrix4 OvrMatrix4::make_proj_matrix_lh(float a_view_angle_rad,float a_aspect_ratio,float a_near,float a_far)
{
	float f_n = float(1.0) / (a_far - a_near);
	float theta = a_view_angle_rad * float(0.5);

	float divisor = (float)tan(theta);
	float factor = float(1.0) / divisor;

	return OvrMatrix4(
		(float(1.0) / a_aspect_ratio) * factor,float(0),     float(0),              float(0),
		float(0),  factor,                                  float(0),              float(0),
		float(0),  float(0),      -a_far * f_n,    float(-1.0),
		float(0),  float(0),   -a_far * a_near * f_n,       float(0));
}

 
OvrMatrix4 OvrMatrix4::make_rotate_from_to_matrix(const OvrVector3& a_src,const OvrVector3 a_dst)
{
	OvrVector3 t_src = a_src;
	double len1 = t_src.normalize();
	OvrVector3 t_dst = a_dst;
	double len2 = t_dst.normalize();
	OvrVector3 axis = t_src.cross(t_dst);
	double cosv = t_src*t_dst;
	lvr_clamp(cosv,-1.0,1.0);
	float angle = (float)acos(cosv);
	return OvrMatrix4::make_rotation_matrix_around_axis(axis,angle);
}
