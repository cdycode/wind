﻿#include "wx_static_serialize.h"

#include <stdlib.h>
#include <string.h>

static union
{
	char c[4];
	ovr::uint32 myLong;
} endian_test = { { 'l', '?', '?', 'b' } };

#define ENDIANNESS                      ( ( char ) endian_test.myLong )

ovr::uint32 OvrStaticSerialize::WriteUint8 ( const ovr::uint8 src, ovr::uint8* des )
{
	if ( des == NULL )
	{
		return 0;
	}

	ovr::uint32 size = sizeof ( ovr::uint8 );

	memcpy ( des, &src, size );

	return size;
}

ovr::uint32 OvrStaticSerialize::WriteUint32 ( const ovr::uint32 src, ovr::uint8* des, bool aIsLittle )
{
	if ( des == NULL )
	{
		return 0;
	}

	ovr::uint32 size = sizeof ( ovr::uint32 );

	if ( aIsLittle )
	{
		for ( ovr::uint32 index = 0; index < size; index++ )
		{
			des[index] = ( ovr::uint8 ) ( ( src >> ( index << 3 ) ) & 0xFF );
		}
	}
	else
	{
		for ( ovr::uint32 index = 0; index < size; index++ )
		{
			des[index] = ( ovr::uint8 ) ( ( src >> ( ( size - 1 - index ) << 3 ) ) & 0xFF );
		}
	}

	return size;
}

ovr::uint32 OvrStaticSerialize::WriteUint16 ( const ovr::uint16 src, ovr::uint8* des, bool aIsLittle )
{
	if ( des == NULL )
	{
		return 0;
	}

	ovr::uint32 size = sizeof ( ovr::uint16 );

	if ( aIsLittle )
	{
		for ( ovr::uint32 index = 0; index < size; index++ )
		{
			des[index] = ( char ) ( ( src >> ( index << 3 ) ) & 0xFF );
		}
	}
	else
	{
		for ( ovr::uint32 index = 0; index < size; index++ )
		{
			des[index] = ( char ) ( ( src >> ( ( size - 1 - index ) << 3 ) ) & 0xFF );
		}
	}

	return size;
}

ovr::uint32 OvrStaticSerialize::WriteFloat ( const float src, ovr::uint8* des, bool aIsLittle )
{
	if ( des == NULL )
	{
		return 0;
	}

	ovr::uint32 size = sizeof ( float );

	union _temp_
	{
		float val;
		ovr::uint8 buf[4];
	} temp;

	temp.val = src;

	memcpy ( des, temp.buf, size );

	if ( aIsLittle )
	{
		if ( ENDIANNESS == 'b' )
		{
			for ( ovr::uint32 index = 0; index < size; index++ )
			{
				des[index] = temp.buf[size - 1 - index];
			}
		}
	}
	else
	{
		if ( ENDIANNESS == 'l' )
		{
			for ( ovr::uint32 index = 0; index < size; index++ )
			{
				des[index] = temp.buf[size - 1 - index];
			}
		}
	}

	return size;
}

ovr::uint32 OvrStaticSerialize::WriteUint64 ( const ovr::uint64 src, ovr::uint8* des, bool aIsLittle )
{
	if ( des == NULL )
	{
		return 0;
	}

	ovr::uint32 size = sizeof ( ovr::uint64 );

	if ( aIsLittle )
	{
		for ( ovr::uint32 index = 0; index < size; index++ )
		{
			des[index] = ( char ) ( ( src >> ( index << 3 ) ) & 0xFF );
		}
	}
	else
	{
		for ( ovr::uint32 index = 0; index < size; index++ )
		{
			des[index] = ( char ) ( ( src >> ( ( size - 1 - index ) << 3 ) ) & 0xFF );
		}
	}

	return size;
}

ovr::uint32 OvrStaticSerialize::WriteDouble ( const double src, ovr::uint8* des, bool aIsLittle )
{
	if ( des == NULL )
	{
		return 0;
	}

	ovr::uint32 size = sizeof ( double );

	union _temp_
	{
		double val;
		ovr::uint8 buf[8];
	} temp;

	temp.val = src;

	memcpy ( des, temp.buf, size );

	if ( aIsLittle )
	{
		if ( ENDIANNESS == 'b' )
		{
			for ( ovr::uint32 index = 0; index < size; index++ )
			{
				des[index] = temp.buf[size - 1 - index];
			}
		}
	}
	else
	{
		if ( ENDIANNESS == 'l' )
		{
			for ( ovr::uint32 index = 0; index < size; index++ )
			{
				des[index] = temp.buf[size - 1 - index];
			}
		}
	}

	return size;
}

ovr::uint32 OvrStaticSerialize::ReadUint8 ( ovr::uint8* ret, const ovr::uint8* src )
{
	*ret = 0;
	ovr::uint32 size = sizeof ( ovr::uint8 );

	if ( src == NULL )
	{
		return 0;
	}

	memcpy ( ret, src, size );

	return size;
}

ovr::uint32 OvrStaticSerialize::ReadUint32 ( ovr::uint32* ret, const ovr::uint8* src, bool aIsLittle )
{
	*ret = 0;
	ovr::uint32 size = sizeof ( ovr::uint32 );

	if ( src == NULL )
	{
		return 0;
	}

	if ( aIsLittle )
	{
		for ( ovr::uint32 index = 0; index < size; index++ )
		{
			*ret += ( ( ( ovr::uint32 ) src[index] & 0xFF ) << ( index << 3 ) );
		}
	}
	else
	{
		for ( ovr::uint32 index = 0; index < size; index++ )
		{
			*ret += ( ( ( ovr::uint32 ) src[index] & 0xFF ) << ( ( size - 1 - index ) << 3 ) );
		}
	}

	return size;
}

ovr::uint32 OvrStaticSerialize::ReadUint16 ( ovr::uint16* ret, const ovr::uint8* src, bool aIsLittle )
{
	*ret = 0;
	ovr::uint32 size = sizeof ( ovr::uint16 );

	if ( src == NULL )
	{
		return 0;
	}

	if ( aIsLittle )
	{
		for ( ovr::uint32 index = 0; index < size; index++ )
		{
			*ret += ( ( ( ovr::uint32 ) src[index] & 0xFF ) << ( index << 3 ) );
		}
	}
	else
	{
		for ( ovr::uint32 index = 0; index < size; index++ )
		{
			*ret += ( ( ( ovr::uint32 ) src[index] & 0xFF ) << ( ( size - 1 - index ) << 3 ) );
		}
	}

	return size;
}

ovr::uint32 OvrStaticSerialize::ReadFloat ( float* ret, const ovr::uint8* src, bool aIsLittle )
{
	ovr::uint32 size = sizeof ( float );

	if ( src == NULL )
	{
		return 0;
	}

	union _temp_
	{
		float val;
		char buf[4];
	} temp;

	memcpy ( temp.buf, src, size );

	if ( aIsLittle )
	{
		if ( ENDIANNESS == 'b' )
		{
			char tmp[5] = { 0 };
			memcpy ( tmp, temp.buf, size );

			for ( ovr::uint32 index = 0; index < size; index++ )
			{
				temp.buf[index] = tmp[size - 1 - index];
			}
		}
	}
	else
	{
		if ( ENDIANNESS == 'l' )
		{
			char tmp[5] = { 0 };
			memcpy ( tmp, temp.buf, size );

			for ( ovr::uint32 index = 0; index < size; index++ )
			{
				temp.buf[index] = tmp[size - 1 - index];
			}
		}
	}

	*ret = temp.val;

	return size;
}

ovr::uint32 OvrStaticSerialize::ReadUint64 ( ovr::uint64* ret, const ovr::uint8* src, bool aIsLittle )
{
	*ret = 0;
	ovr::uint32 size = sizeof ( ovr::uint64 );

	if ( src == NULL )
	{
		return 0;
	}

	if ( aIsLittle )
	{
		for ( ovr::uint32 index = 0; index < size; index++ )
		{
			*ret += ( ( ( ovr::uint32 ) src[index] & 0xFF ) << ( index << 3 ) );
		}
	}
	else
	{
		for ( ovr::uint32 index = 0; index < size; index++ )
		{
			*ret += ( ( ( ovr::uint32 ) src[index] & 0xFF ) << ( ( size - 1 - index ) << 3 ) );
		}
	}

	return size;
}

ovr::uint32 OvrStaticSerialize::ReadDouble ( double* ret, const ovr::uint8* src, bool aIsLittle )
{
	ovr::uint32 size = sizeof ( double );

	if ( src == NULL )
	{
		return 0;
	}

	union _temp_
	{
		double val;
		char buf[8];
	} temp;

	memcpy ( temp.buf, src, size );

	if ( aIsLittle )
	{
		if ( ENDIANNESS == 'b' )
		{
			char tmp[9] = { 0 };
			memcpy ( tmp, temp.buf, size );

			for ( ovr::uint32 index = 0; index < size; index++ )
			{
				temp.buf[index] = tmp[size - 1 - index];
			}
		}
	}
	else
	{
		if ( ENDIANNESS == 'l' )
		{
			char tmp[9] = { 0 };
			memcpy ( tmp, temp.buf, size );

			for ( ovr::uint32 index = 0; index < size; index++ )
			{
				temp.buf[index] = tmp[size - 1 - index];
			}
		}
	}

	*ret = temp.val;

	return size;
}

ovr::uint32 OvrStaticSerialize::WriteData( ovr::uint8* data, ovr::uint32 dataLen, OvrIOInerface* handle )
{
	if ( data!= NULL && dataLen  > 0 )
	{
		if ( handle->Append( data, dataLen ) == dataLen )
		{
			return dataLen;
		}
	}

	return 0;
}

ovr::uint32 OvrStaticSerialize::WriteUint8 ( const ovr::uint8 src, OvrIOInerface* handle )
{
	ovr::uint32 ret = 0;

	if ( handle != NULL )
	{
		ovr::uint8 temp[2] = { 0 };

		if ( ( ret = WriteUint8 ( src, temp ) ) > 0 )
		{
			if ( handle->Append ( &src, ret ) == ret )
			{
				return ret;
			}
		}
	}

	return 0;
}

ovr::uint32 OvrStaticSerialize::WriteUint32 ( const ovr::uint32 src, OvrIOInerface* handle, bool aIsLittle )
{
	ovr::uint32 ret = 0;

	if ( handle != NULL )
	{
		ovr::uint8 temp[5] = { 0 };

		if ( ( ret = WriteUint32 ( src, temp, aIsLittle ) ) > 0 )
		{
			if ( handle->Append ( temp, ret ) == ret )
			{
				return ret;
			}
		}
	}

	return 0;
}

ovr::uint32 OvrStaticSerialize::WriteUint16 ( const ovr::uint16 src, OvrIOInerface* handle, bool aIsLittle )
{
	ovr::uint32 ret = 0;

	if ( handle != NULL )
	{
		ovr::uint8 temp[3] = { 0 };

		if ( ( ret = WriteUint16 ( src, temp, aIsLittle ) ) > 0 )
		{
			if ( handle->Append ( temp, ret ) == ret )
			{
				return ret;
			}
		}
	}

	return 0;
}

ovr::uint32 OvrStaticSerialize::WriteFloat ( const float src, OvrIOInerface* handle, bool aIsLittle )
{
	ovr::uint32 ret = 0;

	if ( handle != NULL )
	{
		ovr::uint8 temp[5] = { 0 };

		if ( ( ret = WriteFloat ( src, temp, aIsLittle ) ) > 0 )
		{
			if ( handle->Append ( temp, ret ) == ret )
			{
				return ret;
			}
		}
	}

	return 0;
}

ovr::uint32 OvrStaticSerialize::WriteUint64 ( const ovr::uint64 src, OvrIOInerface* handle, bool aIsLittle )
{
	ovr::uint32 ret = 0;

	if ( handle != NULL )
	{
		ovr::uint8 temp[9] = { 0 };

		if ( ( ret = WriteUint64 ( src, temp, aIsLittle ) ) > 0 )
		{
			if ( handle->Append ( temp, ret ) == ret )
			{
				return ret;
			}
		}
	}

	return 0;
}

ovr::uint32 OvrStaticSerialize::WriteDouble ( const double src, OvrIOInerface* handle, bool aIsLittle )
{
	ovr::uint32 ret = 0;

	if ( handle != NULL )
	{
		ovr::uint8 temp[9] = { 0 };

		if ( ( ret = WriteDouble ( src, temp, aIsLittle ) ) > 0 )
		{
			if ( handle->Append ( temp, ret ) == ret )
			{
				return ret;
			}
		}
	}

	return 0;
}

ovr::uint32 OvrStaticSerialize::WriteVarChar ( OvrString src, OvrIOInerface* handle, bool aIsLittle )
{
	ovr::uint32 ret = 0;

	if (handle == NULL)
	{
		return 0;
	}
		
	if ((ret = WriteUint16(src.GetStringSize(), handle, aIsLittle)) > 0)
	{
		if (src.GetStringSize() > 0)
		{
			if (handle->Append(src.GetString(), src.GetStringSize()) == src.GetStringSize())
			{
				ret += src.GetStringSize();
			}
		}
		return ret;
	}
	return 0;
}

ovr::uint32 OvrStaticSerialize::WriteVector3( OvrVector3 src, OvrIOInerface* handle, bool aIsLittle )
{
	ovr::uint32 ret = 0, temp = 0;

	if ( handle != NULL )
	{
		for ( int i = 0; i < 3; i++)
		{
			if ( ( temp = WriteFloat( src[i], handle ) ) == 0 )
			{
				return 0;
			}

			ret += temp;
		}

		return ret;
	}

	return 0;
}

ovr::uint32 OvrStaticSerialize::WriteTransform( OvrTransform src, OvrIOInerface* handle, bool aIsLittle )
{
	ovr::uint32 ret = 0, temp = 0;

	if ( handle != NULL )
	{
		if ( ( temp = WriteVector3( src.position, handle ) ) == 0 )
		{
			return 0;
		}

		ret += temp;

		if ( ( temp = WriteVector3( src.scale, handle ) ) == 0 )
		{
			return 0;
		}

		ret += temp;
		
		for ( int i = 0; i < 4; i++ )
		{
			if ( ( temp = WriteFloat(src.quaternion[i], handle) ) == 0 )
			{
				return 0;
			}

			ret += temp;
		}
		
		return ret;
	}

	return 0;
}

ovr::uint32 OvrStaticSerialize::ReadData( ovr::uint8* data, ovr::uint32 dataLen, OvrIOInerface* handle )
{
	if ( handle == NULL || data == NULL || dataLen == 0 || handle->GetPosition() + dataLen > handle->GetLength() )
	{
		return 0;
	}

	//check eof
	if ( handle->Subtract ( data, dataLen ) < dataLen )
	{
		return 0;
	}

	return dataLen;
}

ovr::uint32 OvrStaticSerialize::ReadUint8 ( ovr::uint8* ret, OvrIOInerface* handle )
{
	if ( handle == NULL )
	{
		return 0;
	}

	ovr::uint32 size = sizeof ( ovr::uint8 );
	ovr::uint8 temp[2] = { 0 };

	//check eof
	if ( handle->Subtract ( temp, size ) < size )
	{
		return 0;
	}

	ReadUint8 ( ret, temp );

	return size;
}

ovr::uint32 OvrStaticSerialize::ReadUint32 ( ovr::uint32* ret, OvrIOInerface* handle, bool aIsLittle )
{
	if ( handle == NULL )
	{
		return 0;
	}

	ovr::uint32 size = sizeof ( ovr::uint32 );
	ovr::uint8 temp[5] = { 0 };

	//check eof
	if ( handle->Subtract ( temp, size ) < size )
	{
		return 0;
	}

	ReadUint32 ( ret, temp, aIsLittle );

	return size;
}

ovr::uint32 OvrStaticSerialize::ReadUint16 ( ovr::uint16* ret, OvrIOInerface* handle, bool aIsLittle )
{
	if ( handle == NULL )
	{
		return 0;
	}

	ovr::uint32 size = sizeof ( ovr::uint16 );
	ovr::uint8 temp[3] = { 0 };

	//check eof
	if ( handle->Subtract ( temp, size ) < size )
	{
		return 0;
	}

	ReadUint16 ( ret, temp, aIsLittle );

	return size;
}

ovr::uint32 OvrStaticSerialize::ReadFloat ( float* ret, OvrIOInerface* handle, bool aIsLittle )
{
	if ( handle == NULL )
	{
		return 0;
	}

	ovr::uint32 size = sizeof ( float );
	ovr::uint8 temp[5] = { 0 };

	//check eof
	if ( handle->Subtract ( temp, size ) < size )
	{
		return 0;
	}

	ReadFloat ( ret, temp, aIsLittle );

	return size;
}

ovr::uint32 OvrStaticSerialize::ReadUint64 ( ovr::uint64* ret, OvrIOInerface* handle, bool aIsLittle )
{
	if ( handle == NULL )
	{
		return 0;
	}

	ovr::uint32 size = sizeof ( ovr::uint64 );
	ovr::uint8 temp[9] = { 0 };

	//check eof
	if ( handle->Subtract ( temp, size ) < size )
	{
		return 0;
	}

	ReadUint64 ( ret, temp, aIsLittle );

	return size;
}

ovr::uint32 OvrStaticSerialize::ReadDouble ( double* ret, OvrIOInerface* handle, bool aIsLittle )
{
	if ( handle == NULL )
	{
		return 0;
	}

	ovr::uint32 size = sizeof ( double );
	ovr::uint8 temp[9] = { 0 };

	//check eof
	if ( handle->Subtract ( temp, size ) < size )
	{
		return 0;
	}

	ReadDouble ( ret, temp, aIsLittle );

	return size;
}

ovr::uint32 OvrStaticSerialize::ReadVarChar (OvrString* ret, OvrIOInerface* handle, bool aIsLittle )
{
	if ( handle == NULL )
	{
		return 0;
	}

	ovr::uint16 len = 0;

	ovr::uint32 size = ReadUint16 ( &len, handle, aIsLittle );

	if ( size > 0 && len > 0 && len <= 0xFFFF )
	{
		if ( len <= 2048 )
		{
			char temp[2048] = { 0 };

			//check eof
			if ( handle->Subtract ( temp, len ) < len )
			{
				return 0;
			}

			ret->SetString ( temp );
		}
		else
		{
			char* temp = ( char* ) malloc ( len );

			if ( temp == NULL )
			{
				return 0;
			}

			//check eof
			if ( handle->Subtract ( temp, len ) < len )
			{
				free ( temp );
				return 0;
			}

			ret->SetString ( temp );
			free ( temp );
		}

		size += len;// 2+len
	}

	return size;
}

ovr::uint32 OvrStaticSerialize::ReadVector3( OvrVector3* src, OvrIOInerface* handle, bool aIsLittle )
{
	ovr::uint32 ret = 0, temp = 0;

	if ( handle != NULL )
	{
		for ( int i = 0; i < 3; i++ )
		{
			if ( ( temp = ReadFloat( &( ( *src )[i] ), handle ) ) == 0 )
			{
				return 0;
			}

			ret += temp;
		}

		return ret;
	}

	return 0;
}

ovr::uint32 OvrStaticSerialize::ReadTransform( OvrTransform* src, OvrIOInerface* handle, bool aIsLittle )
{
	ovr::uint32 ret = 0, temp = 0;

	if ( handle != NULL )
	{
		if ( ( temp = ReadVector3( &src->position, handle ) ) == 0 )
		{
			return 0;
		}

		ret += temp;

		if ( ( temp = ReadVector3( &src->scale, handle ) ) == 0)
		{
			return 0;
		}

		ret += temp;
		
		for ( int i = 0; i < 4; i++ )
		{
			if ( ( temp = ReadFloat( &src->quaternion[i], handle ) ) == 0)
			{
				return 0;
			}

			ret += temp;
		}

		return ret;
	}

	return 0;
}
