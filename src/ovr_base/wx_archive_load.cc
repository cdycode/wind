﻿#include "wx_archive.h"
#include "wx_static_serialize.h"

OvrArchiveLoad::OvrArchiveLoad(OvrIOInerface* a_io, bool a_little) : OvrArchive(a_io, a_little)
{
}

bool OvrArchiveLoad::Serialize(ovr::int8& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::ReadUint8((ovr::uint8*)&a_value, io_handle) != sizeof(ovr::uint8));
}

bool OvrArchiveLoad::Serialize(ovr::int16& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::ReadUint16((ovr::uint16*)&a_value, io_handle, is_host_little_endian) != sizeof(ovr::uint16));
}

bool OvrArchiveLoad::Serialize(ovr::int32& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::ReadUint32((ovr::uint32*)&a_value, io_handle, is_host_little_endian) != sizeof(ovr::uint32));
}

bool OvrArchiveLoad::Serialize(ovr::int64& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::ReadUint64((ovr::uint64*)&a_value, io_handle, is_host_little_endian) != sizeof(ovr::uint64));
}

bool OvrArchiveLoad::Serialize(ovr::uint8& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::ReadUint8(&a_value, io_handle) != sizeof(ovr::uint8));
}

bool OvrArchiveLoad::Serialize(ovr::uint16& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::ReadUint16(&a_value, io_handle, is_host_little_endian) != sizeof(ovr::uint16));
}

bool OvrArchiveLoad::Serialize(ovr::uint32& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::ReadUint32(&a_value, io_handle, is_host_little_endian) != sizeof(ovr::uint32));
}

bool OvrArchiveLoad::Serialize(ovr::uint64& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::ReadUint64(&a_value, io_handle, is_host_little_endian) != sizeof(ovr::uint64));
}

bool OvrArchiveLoad::Serialize(float& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::ReadFloat(&a_value, io_handle, is_host_little_endian) != sizeof(float));
}

bool OvrArchiveLoad::Serialize(double& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::ReadDouble(&a_value, io_handle, is_host_little_endian) != sizeof(double));
}

bool OvrArchiveLoad::SerializeBuffer(void* a_buffer, ovr::int32 a_buffer_length, ovr::int32 a_data_size)
{
	if (a_data_size > a_buffer_length)
	{
		return false;
	}

	return (OvrStaticSerialize::ReadData((ovr::uint8*)a_buffer, a_data_size, io_handle) == a_data_size);
}

bool OvrArchiveLoad::Serialize(OvrString& a_string)
{
	if (io_handle == NULL)
	{
		return false;
	}

	//存在空字符串的情况 不只能根据字符串长度来判断是否读取
	OvrStaticSerialize::ReadVarChar(&a_string, io_handle, is_host_little_endian);
	return true;
}

bool OvrArchiveLoad::Serialize(OvrBuffer& a_buffer)
{
	ovr::uint32	data_size = 0;

	if (!Serialize(data_size))
	{
		return false;
	}

	if (data_size > 0)
	{
		a_buffer.SetDataSize(data_size);
		return SerializeBuffer(a_buffer.GetBuffer(), data_size, data_size);
	}

	return true;
}

bool OvrArchiveLoad::Serialize(OvrTransform& a_tf)
{
	return !(io_handle == NULL || OvrStaticSerialize::ReadTransform(&a_tf, io_handle, is_host_little_endian) != 10 * sizeof(float));
}

bool OvrArchiveLoad::Serialize(OvrVector3& a_vector) 
{
	if (nullptr == io_handle)
	{
		return false;
	}

	if (OvrStaticSerialize::ReadFloat(&(a_vector[0]), io_handle, is_host_little_endian) == sizeof(float) &&
		OvrStaticSerialize::ReadFloat(&(a_vector[1]), io_handle, is_host_little_endian) == sizeof(float) &&
		OvrStaticSerialize::ReadFloat(&(a_vector[2]), io_handle, is_host_little_endian) == sizeof(float)) 
	{
		return true;
	}
	return false;
}

bool OvrArchiveLoad::Serialize(OvrVector4& a_vector) 
{
	if (nullptr == io_handle)
	{
		return false;
	}

	if (OvrStaticSerialize::ReadFloat(&(a_vector[0]), io_handle, is_host_little_endian) == sizeof(float) &&
		OvrStaticSerialize::ReadFloat(&(a_vector[1]), io_handle, is_host_little_endian) == sizeof(float) &&
		OvrStaticSerialize::ReadFloat(&(a_vector[2]), io_handle, is_host_little_endian) == sizeof(float) && 
		OvrStaticSerialize::ReadFloat(&(a_vector[3]), io_handle, is_host_little_endian) == sizeof(float))
	{
		return true;
	}
	return false;
}