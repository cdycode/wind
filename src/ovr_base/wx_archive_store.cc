﻿#include "wx_archive.h"
#include "wx_static_serialize.h"

OvrArchiveStore::OvrArchiveStore(OvrIOInerface* a_io, bool a_little) : OvrArchive(a_io, a_little)
{
}

bool OvrArchiveStore::Serialize(ovr::int8& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::WriteUint8((ovr::uint8)a_value, io_handle) != sizeof(ovr::uint8));
}

bool OvrArchiveStore::Serialize(ovr::int16& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::WriteUint16((ovr::uint16)a_value, io_handle, is_host_little_endian) != sizeof(ovr::uint16));
}

bool OvrArchiveStore::Serialize(ovr::int32& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::WriteUint32((ovr::uint32)a_value, io_handle, is_host_little_endian) != sizeof(ovr::uint32));
}

bool OvrArchiveStore::Serialize(ovr::int64& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::WriteUint64((ovr::uint64)a_value, io_handle, is_host_little_endian) != sizeof(ovr::uint64));
}

bool OvrArchiveStore::Serialize(ovr::uint8& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::WriteUint8(a_value, io_handle) != sizeof(ovr::uint8));
}

bool OvrArchiveStore::Serialize(ovr::uint16& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::WriteUint16(a_value, io_handle, is_host_little_endian) != sizeof(ovr::uint16));
}

bool OvrArchiveStore::Serialize(ovr::uint32& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::WriteUint32(a_value, io_handle, is_host_little_endian) != sizeof(ovr::uint32));
}

bool OvrArchiveStore::Serialize(ovr::uint64& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::WriteUint64(a_value, io_handle, is_host_little_endian) != sizeof(ovr::uint64));
}

bool OvrArchiveStore::Serialize(float& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::WriteFloat(a_value, io_handle, is_host_little_endian) != sizeof(float));
}

bool OvrArchiveStore::Serialize(double& a_value)
{
	return !(io_handle == NULL || OvrStaticSerialize::WriteDouble(a_value, io_handle, is_host_little_endian) != sizeof(double));
}

bool OvrArchiveStore::SerializeBuffer(void* a_buffer, ovr::int32 a_buffer_length, ovr::int32 a_data_size)
{
	if (a_data_size > a_buffer_length)
	{
		return false;
	}

	return (OvrStaticSerialize::WriteData((ovr::uint8*)a_buffer, a_data_size, io_handle) == a_data_size);
}

bool OvrArchiveStore::Serialize(OvrString& a_string)
{
	return !(io_handle == NULL || OvrStaticSerialize::WriteVarChar(a_string, io_handle, is_host_little_endian) <= 0);
}

bool OvrArchiveStore::Serialize(OvrBuffer& a_buffer)
{
	ovr::uint32 data_size = a_buffer.GetDataSize();
	if (!Serialize(data_size))
	{
		return false;
	}

	if (data_size > 0)
	{
		return SerializeBuffer(a_buffer.GetBuffer(), data_size, data_size);
	}

	return true;
}

bool OvrArchiveStore::Serialize(OvrTransform& a_tf)
{
	return !(io_handle == NULL || OvrStaticSerialize::WriteTransform(a_tf, io_handle, is_host_little_endian) != 10 * sizeof(float));
}

bool OvrArchiveStore::Serialize(OvrVector3& a_vector) 
{
	if (nullptr == io_handle)
	{
		return false;
	}

	if (OvrStaticSerialize::WriteFloat(a_vector[0], io_handle, is_host_little_endian) == sizeof(float) &&
		OvrStaticSerialize::WriteFloat(a_vector[1], io_handle, is_host_little_endian) == sizeof(float) &&
		OvrStaticSerialize::WriteFloat(a_vector[2], io_handle, is_host_little_endian) == sizeof(float))
	{
		return true;
	}
	return false;
}

bool OvrArchiveStore::Serialize(OvrVector4& a_vector) 
{
	if (nullptr == io_handle)
	{
		return false;
	}

	if (OvrStaticSerialize::WriteFloat(a_vector[0], io_handle, is_host_little_endian) == sizeof(float) &&
		OvrStaticSerialize::WriteFloat(a_vector[1], io_handle, is_host_little_endian) == sizeof(float) &&
		OvrStaticSerialize::WriteFloat(a_vector[2], io_handle, is_host_little_endian) == sizeof(float) &&
		OvrStaticSerialize::WriteFloat(a_vector[3], io_handle, is_host_little_endian) == sizeof(float))
	{
		return true;
	}
	return false;
}