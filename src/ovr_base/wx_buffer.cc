﻿#include <string>
#include "wx_buffer.h"


OvrBuffer::OvrBuffer()
		:buffer(nullptr)
		, buffer_length(0)
		, data_size(0)
		, is_buffer_own(false)
{}

OvrBuffer::~OvrBuffer()
{
	ReleaseBuffer();
}

bool OvrBuffer::Reset(ovr::uint32 a_buffer_length)
{
	if (a_buffer_length <= 0)
	{
		ReleaseBuffer();
		return true;
	}

	char* temp_buffer = new(std::nothrow) char[a_buffer_length];
	if (nullptr == temp_buffer)
	{
		return false;
	}

	ReleaseBuffer();

	is_buffer_own = true;
	buffer = temp_buffer;
	buffer_length = a_buffer_length;
	data_size = 0;

	return true;
}
bool OvrBuffer::Reset(char* a_buffer, ovr::uint32 a_buffer_length, ovr::uint32 a_data_size)
{
	ReleaseBuffer();

	is_buffer_own = false;
	buffer = a_buffer;
	buffer_length = a_buffer_length;
	data_size = a_data_size;
	return true;
}

void OvrBuffer::ReleaseBuffer()
{
	if (is_buffer_own && nullptr != buffer)
	{
		delete[]buffer;
	}
	buffer = nullptr;
	buffer_length = 0;
	data_size = 0;
	is_buffer_own = false;
}
