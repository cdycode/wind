﻿#ifndef OVR_PROJECT_OVR_ZIP_H_
#define OVR_PROJECT_OVR_ZIP_H_

#include "zip.h"
#include "unzip.h"

#include <vector>
#include "wx_string.h"
	
class OvrZip
{
public:
	OvrZip() {}
	~OvrZip(){}
public:
	bool OpenZip(const char* a_zipname);
	/*解压zip到 a_extractdir路径  目前保存在当前工作目录下 catch目录下*/
	bool ExtractZip(const char* a_extractdir);
	/*根据zip路径读取文件*/
	bool ReadFileFromZip(const char* a_filename);
		
	/*先转化路径格式，在读取*/
	bool ReadProjectFileFromZip(const char* a_filename);

	/*获取 zip里面的文件解压后的buff*/
	void* GetBuff();
	/*获取 zip里面的文件解压后的大小*/
	int  GetFileSize();

	void CreateBuff(unsigned int a_size);
	void ReleaseBuff();
	void CloseZip();
private:
	void GetAllFileName();

	//解压到文件夹
	int do_extract_currentfile(const int* a_popt_extract_without_path, int* a_popt_overwrite, const char* a_password);
	void change_file_date(const char *a_filename, uLong a_dosdate, tm_unz a_tmu_date);
	int makedir(const char *a_newdir);
	int do_extract(int a_opt_extract_without_path, int a_opt_overwrite, const char* a_password);
private:
	unzFile			zfile = nullptr;
	char*			buff = nullptr;
	unsigned int	filesize = 0;
	//char			zipmain_dir[128];
};
#endif
