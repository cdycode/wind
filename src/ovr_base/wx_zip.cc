#include "wx_zip.h"
#include <string.h>
#ifdef LVR_OS_ANDROID
# include <unistd.h>
# include <utime.h>
#include <stdlib.h>
#include <errno.h>
#else
# include <direct.h>
# include <io.h>
#include <windows.h>
#endif
#include <algorithm>
#include <wx_file_tools.h>
#include "wx_log.h"

#define OVR_ZIP_UNPRESSBUFF_SIZE 8192
bool OvrZip::OpenZip(const char* a_filename)
{
	CloseZip();
	zfile = unzOpen(a_filename);
		
	if (zfile == NULL)
		return false;
	return true;
}
/*
void OvrZip::GetAllFileName()
{
	unz_global_info gi;
	int err = UNZ_OK;
	err = unzGetGlobalInfo(zfile, &gi);
	if (err != UNZ_OK)
		return;

	char _filename_inzip[256];
	unz_file_info file_info;
	for (unsigned int i = 0; i < gi.number_entry; i++)
	{
		memset(_filename_inzip, 0, 256);
		err = unzGetCurrentFileInfo(zfile, &file_info, _filename_inzip, sizeof(_filename_inzip), NULL, 0, NULL, 0);
		if (err != UNZ_OK)
		{
			unzGoToFirstFile(zfile);
			return;
		}
		filename_inzip.push_back(_filename_inzip);
			
		err = unzGoToNextFile(zfile);
		if (err != UNZ_OK)
		{
			unzGoToFirstFile(zfile);
			return;
		}
	}
	unzGoToFirstFile(zfile);
}*/
/*
bool OvrZip::ReadProjectFileFromZip(const char* a_filename)
{
	std::string temp = a_filename;
	int index = temp.find('/');
	while (index != -1)
	{
		temp = temp.replace(index, 1, "\\");
		index = temp.find('/');
	}
	temp = zipmain_dir + temp;
	return ReadFileFromZip(temp.c_str());
}*/
bool OvrZip::ReadFileFromZip(const char* a_filename)
{
	filesize = 0;
	char filename_replace[256];
	int  filename_replace_num = 0;
	for (int i = 0; a_filename[i] != '\0'; i++)
	{
		if (a_filename[i] == '/')
			filename_replace[i] = '\\';
		else
			filename_replace[i] = a_filename[i];
		filename_replace_num++;
	}
	filename_replace[filename_replace_num] = '\0';
	
	/*
	char fileallpath_inzip[512] = { 0 };
	sprintf(fileallpath_inzip, "%s\\%s", zipmain_dir, a_filename);*/
	if (unzLocateFile(zfile, filename_replace, 0) != UNZ_OK)
		return false;
		
	int err = UNZ_OK;
	char filename_inzip[256];
	char* filename_withoutpath;
	char* p;
	unz_file_info file_info;
	uLong ratio = 0;
	err = unzGetCurrentFileInfo(zfile, &file_info, filename_inzip, sizeof(filename_inzip), NULL, 0, NULL, 0);

	CreateBuff(file_info.uncompressed_size);
	if (err != UNZ_OK)
	{
		return false;
	}

	p = filename_withoutpath = filename_inzip;
	while ((*p) != '\0')
	{
		if (((*p) == '/') || ((*p) == '\\'))
			filename_withoutpath = p + 1;
		p++;
	}

	const char* write_filename;
	int skip = 0;
		
	write_filename = filename_inzip;
	//write_filename = filename_withoutpath;
	err = unzOpenCurrentFile(zfile);
	if (err != UNZ_OK)
	{
		return false;
	}

	int readsize = 0;
	int alreadysize = 0;
	char tempread[OVR_ZIP_UNPRESSBUFF_SIZE];
	do
	{
		memset(tempread, 0, OVR_ZIP_UNPRESSBUFF_SIZE);
		readsize = unzReadCurrentFile(zfile, tempread, OVR_ZIP_UNPRESSBUFF_SIZE);
		if (readsize < 0)
		{
			return false;
		}
		if (readsize > 0)
		{
			memcpy(buff + alreadysize, tempread, readsize);
			alreadysize += readsize;
		}
	} while (readsize > 0);

	return true;
}
void* OvrZip::GetBuff()
{
	return (void*)buff;
}
int  OvrZip::GetFileSize()
{
	return filesize;
}
void OvrZip::ReleaseBuff()
{
	if (buff != NULL)
	{
		delete []buff;
		buff = NULL;
		filesize = 0;
	}
}
void OvrZip::CreateBuff(unsigned int a_size)
{
	ReleaseBuff();

	buff = new char[a_size];
	memset(buff, 0, a_size);
	filesize = a_size;
}
void OvrZip::CloseZip()
{
	ReleaseBuff();
	if (zfile != NULL)
	{
		unzCloseCurrentFile(zfile);
		zfile = NULL;
	}
}
bool OvrZip::ExtractZip(const char* a_extractdir)
{
	bool bsuccess = false;
	OvrString catche_dir = a_extractdir;
	if (!OvrFileTools::IsFileExist(catche_dir))
	{
		bsuccess = OvrFileTools::CreateFolder(catche_dir.GetStringConst());
		if (!bsuccess)
		{
			OvrLogE("ovr_base","Extractzip failed can' createfolder path:%s",catche_dir.GetStringConst());
			return false;
		}
	}
		
	char path[512];
#if LVR_OS_ANDROID
	getcwd(path, 512);
#else
	_getcwd(path, 512);
#endif
#if LVR_OS_ANDROID
    if (chdir(catche_dir.GetStringConst()) == -1)
#else
	if (_chdir(catche_dir.GetStringConst()) == -1)
#endif
	{
		OvrLogE("Can't _chdir %s", catche_dir.GetStringConst());
		return false;
	}
		
	do_extract(0, 1, NULL);
#if LVR_OS_ANDROID
	if (chdir(path) == -1)
#else
    if (_chdir(path) == -1)
#endif
	{
		return false;
	}
	return true;
}

int OvrZip::do_extract_currentfile(const int* a_popt_extract_without_path,int* a_popt_overwrite,const char* a_password)
{
	char filename_inzip[256];
	char* filename_withoutpath;
	char* p;
	int err = UNZ_OK;
	FILE *fout = NULL;
	void* buf;
	uInt size_buf;

	unz_file_info file_info;
	uLong ratio = 0;
	err = unzGetCurrentFileInfo(zfile, &file_info, filename_inzip, sizeof(filename_inzip), NULL, 0, NULL, 0);

	if (err != UNZ_OK)
	{
		OvrLogE("ovr_base","error %d with zipfile in unzGetCurrentFileInfo\n", err);
		return err;
	}

	size_buf = OVR_ZIP_UNPRESSBUFF_SIZE;
	buf = (void*)malloc(size_buf);
	memset(buf, 0, size_buf);
	if (buf == NULL)
	{
		OvrLogE("ovr_base","Error allocating memory\n");
		return UNZ_INTERNALERROR;
	}

	p = filename_withoutpath = filename_inzip;
	while ((*p) != '\0')
	{
		if (((*p) == '/') || ((*p) == '\\'))
			filename_withoutpath = p + 1;
		p++;
	}

	if ((*filename_withoutpath) == '\0')
	{
		if ((*a_popt_extract_without_path) == 0)
		{
			OvrLogD("ovr_base","creating directory: %s\n", filename_inzip);
			OvrFileTools::CreateFolder(filename_inzip);
		}
	}
	else
	{
		const char* write_filename;
		int skip = 0;

		if ((*a_popt_extract_without_path) == 0)
			write_filename = filename_inzip;
		else
			write_filename = filename_withoutpath;

		err = unzOpenCurrentFilePassword(zfile, a_password);
			
		if (err != UNZ_OK)
		{
			OvrLogE("ovr_base","error %d with zipfile in unzOpenCurrentFilePassword\n", err);
		}

		if ((skip == 0) && (err == UNZ_OK))
		{
			fout = OvrFileTools::CreateFileX(write_filename);

			/* some zipfile don't contain directory alone before file */
			if ((fout == NULL) && ((*a_popt_extract_without_path) == 0) &&
				(filename_withoutpath != (char*)filename_inzip))
			{
				char c = *(filename_withoutpath - 1);
				*(filename_withoutpath - 1) = '\0';
				makedir(write_filename);
				*(filename_withoutpath - 1) = c;
				fout = OvrFileTools::CreateFileX(write_filename);
			}

			if (fout == NULL)
			{
				OvrLogE("ovr_base","error opening %s\n", write_filename);
			}
		}

		if (fout != NULL)
		{
			OvrLogD("ovr_base", " extracting: %s\n", write_filename);

			do
			{
				err = unzReadCurrentFile(zfile, buf, size_buf);
					
				if (err < 0)
				{
					OvrLogE("ovr_base", "error %d with zipfile in unzReadCurrentFile\n", err);
					break;
				}
				if (err > 0)
					if (fwrite(buf, err, 1, fout) != 1)
					{
						OvrLogE("ovr_base", "error in writing extracted file\n");
						err = UNZ_ERRNO;
						break;
					}
			} while (err > 0);
			if (fout)
				fclose(fout);

			if (err == 0)
				change_file_date(write_filename, file_info.dosDate,
					file_info.tmu_date);
		}

		if (err == UNZ_OK)
		{
			err = unzCloseCurrentFile(zfile);
			if (err != UNZ_OK)
			{
				OvrLogE("ovr_base", "error %d with zipfile in unzCloseCurrentFile\n", err);
			}
		}
		else
			unzCloseCurrentFile(zfile); /* don't lose the error */
	}

	free(buf);
	return err;
}
/* change_file_date : change the date/time of a file
filename : the filename of the file where date/time must be modified
dosdate : the new date at the MSDos format (4 bytes)
tmu_date : the SAME new date at the tm_unz format */
void OvrZip::change_file_date(const char *a_filename,uLong a_dosdate,tm_unz a_tmu_date)
{
#ifdef WIN32
	HANDLE hFile;
	FILETIME ftm, ftLocal, ftCreate, ftLastAcc, ftLastWrite;

	hFile = CreateFileA(a_filename, GENERIC_READ | GENERIC_WRITE,
		0, NULL, OPEN_EXISTING, 0, NULL);
	GetFileTime(hFile, &ftCreate, &ftLastAcc, &ftLastWrite);
	DosDateTimeToFileTime((WORD)(a_dosdate >> 16), (WORD)a_dosdate, &ftLocal);
	LocalFileTimeToFileTime(&ftLocal, &ftm);
	SetFileTime(hFile, &ftm, &ftLastAcc, &ftm);
	CloseHandle(hFile);
#else
#ifdef unix
	struct utimbuf ut;
	struct tm newdate;
	newdate.tm_sec = tmu_date.tm_sec;
	newdate.tm_min = tmu_date.tm_min;
	newdate.tm_hour = tmu_date.tm_hour;
	newdate.tm_mday = tmu_date.tm_mday;
	newdate.tm_mon = tmu_date.tm_mon;
	if (tmu_date.tm_year > 1900)
		newdate.tm_year = tmu_date.tm_year - 1900;
	else
		newdate.tm_year = tmu_date.tm_year;
	newdate.tm_isdst = -1;

	ut.actime = ut.modtime = mktime(&newdate);
	utime(filename, &ut);
#endif
#endif
}
/* mymkdir and change_file_date are not 100 % portable
As I don't know well Unix, I wait feedback for the unix portion */

int OvrZip::do_extract(int a_opt_extract_without_path,int a_opt_overwrite,const char* a_password)
{
	uLong i;
	unz_global_info gi;
	int err;
	FILE* fout = NULL;

	err = unzGetGlobalInfo(zfile, &gi);
	if (err != UNZ_OK)
		OvrLogE("ovr_base","error %d with zipfile in unzGetGlobalInfo \n", err);
		
	err = unzGoToFirstFile(zfile);
	if (err != UNZ_OK)
		printf("error %d with zipfile in unzGetGlobalInfo \n", err); 

	for (i = 0; i < gi.number_entry; i++)
	{
		if (do_extract_currentfile(&a_opt_extract_without_path,&a_opt_overwrite, a_password) != UNZ_OK)
			break;

		if ((i + 1) < gi.number_entry)
		{
			err = unzGoToNextFile(zfile);
			if (err != UNZ_OK)
			{
				printf("error %d with zipfile in unzGoToNextFile\n", err);
				break;
			}
		}
	}

	return 0;
}

int OvrZip::makedir(const char *a_newdir)
{
	char *buffer;
	char *p;
	int  len = (int)strlen(a_newdir);

	if (len <= 0)
		return 0;

	buffer = (char*)malloc(len + 1);
	strcpy(buffer, a_newdir);

	if (buffer[len - 1] == '/') {
		buffer[len - 1] = '\0';
	}
	if (OvrFileTools::CreateFolder(buffer))
	{
		free(buffer);
		return 1;
	}

	p = buffer + 1;
	while (1)
	{
		char hold;

		while (*p && *p != '\\' && *p != '/')
			p++;
		hold = *p;
		*p = 0;
		if (!OvrFileTools::CreateFolder(buffer))
		{
			printf("couldn't create directory %s\n", buffer);
			free(buffer);
			return 0;
		}
		if (hold == 0)
			break;
		*p++ = hold;
	}
	free(buffer);
	return 1;
}

