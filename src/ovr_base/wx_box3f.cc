﻿#include "wx_box3f.h"
#include "math.h"
#include <algorithm>
 
bool lvr_ray_triangle_intersect(float& ao_dist, const OvrVector3& a_origin, const OvrVector3& a_dir, const OvrVector3& a_p0, const OvrVector3& a_p1, const OvrVector3& a_p2)
{
	static const double t_eps = 1e-20f;
	OvrVector3 e1 = a_p1 - a_p0;
	OvrVector3 e2 = a_p2 - a_p0;
	OvrVector3 p = a_dir.cross(e2);
	double det = e1*p;
	double u, v, t;
	if (det > t_eps)
	{
		OvrVector3 s = a_origin - a_p0;
		u = s*p;
		if ((u < 0) || (u > det))
		{
			return false;
		}
		OvrVector3 q = s.cross(e1);
		v = a_dir*q;
		if ((v < 0) || (u + v > det))
		{
			return false;
		}
		t = e2*q;
		if (t < 0)
		{
			return false;
		}

	}
	else if (det < -t_eps)
	{
		OvrVector3 s = a_origin - a_p0;
		u = s*p;
		if ((u > 0) || (u < det))
		{
			return false;
		}
		OvrVector3 q = s.cross(e1);
		v = a_dir*q;
		if ((v > 0) || (u + v < det))
		{
			return false;
		}
		t = e2*q;
		if (t > 0)
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	t *= 1.0f / det;
	ao_dist = (float)t;
	return true;
}

OvrBox3f::OvrBox3f()
	:_min(float(0)),_max(float(0))
{

}

 
OvrBox3f::OvrBox3f(const OvrVector3 & a_min,const OvrVector3 & a_max)
	:_min(a_min),_max(a_max)
{
}

 
OvrBox3f::OvrBox3f(const float a_min_x,const float a_min_y,const float a_min_z, const float a_max_x,const float a_max_y,const float a_max_z)
	:_min(a_min_x,a_min_y,a_min_z),_max(a_max_x,a_max_y,a_max_z)
{

}

 
OvrBox3f  OvrBox3f::operator*(const float a_s) const
{
	return OvrBox3f(_min*a_s,_max*a_s);
}

 
void OvrBox3f::operator*=(const float a_s)
{
	_min *= a_s;
	_max *= a_s;
}

 
OvrVector3  OvrBox3f::get_size() const
{
	return _max-_min;
}

 
OvrVector3  OvrBox3f::get_center() const
{
	return (_min+_max)*float(0.5);
}

 
const OvrVector3 & OvrBox3f::get_min() const
{
	return _min;
}

 
const OvrVector3 & OvrBox3f::get_max() const
{
	return _max;
}

 
OvrVector3 & OvrBox3f::get_min()
{
	return _min;
}

 
OvrVector3 & OvrBox3f::get_max()
{
	return _max;
}

 
void OvrBox3f::combine(const OvrBox3f & a_b)
{
	
	const OvrVector3 & minb = a_b.get_min();
	const OvrVector3 & maxb = a_b.get_max(); 
	_min[0] = std::min(_min[0],minb[0]);
	_min[1] = std::min(_min[1],minb[1]);
	_min[2] = std::min(_min[2],minb[2]);
	
	_max[0] = std::max(_max[0],maxb[0]);
	_max[1] = std::max(_max[1],maxb[1]);
	_max[2] = std::max(_max[2],maxb[2]);
}

 
void OvrBox3f::translate(const OvrVector3 & a_mov)
{
	_min += a_mov;
	_max += a_mov;
}

 
bool OvrBox3f::contains(const OvrVector3 & a_v3) const
{
	return a_v3[0] >= _min[0] && a_v3[0] <= _max[0]
	&&	a_v3[1] >= _min[1] && a_v3[1] <= _max[1]                      
	&&	a_v3[2] >= _min[2] && a_v3[2] <= _max[2];                     
}

 
void OvrBox3f::expand(const OvrVector3 & a_v3)
{
	if(a_v3[0]<_min[0])
	{
		_min[0] = a_v3[0];
	}
	else if(a_v3[0] > _max[0])
	{
		_max[0] = a_v3[0];
	}

	if(a_v3[1]<_min[1])
	{
		_min[1] = a_v3[1];
	}
	else if(a_v3[1] > _max[1])
	{
		_max[1] = a_v3[1];
	}

	if(a_v3[2]<_min[2])
	{
		_min[2] = a_v3[2];
	}
	else if(a_v3[2] > _max[2])
	{
		_max[2] = a_v3[2];
	}
}

 
int OvrBox3f::ray_interset(const OvrRay3f & a_ray, float& ao_dist) const
{
	OvrVector3  points[8] = { OvrVector3 (_min[0], _min[1], _min[2]), OvrVector3 (_min[0], _max[1], _min[2]),
		OvrVector3 (_max[0], _min[1], _min[2]), OvrVector3 (_max[0], _max[1], _min[2]),

		OvrVector3 (_min[0], _min[1], _max[2]), OvrVector3 (_min[0], _max[1], _max[2]),
		OvrVector3 (_max[0], _min[1], _max[2]), OvrVector3 (_max[0], _max[1], _max[2]) };

	int ids[36] = {0,1,2,1,3,2, 6,4,5,6,5,7, 2,3,6,3,7,6, 0,4,1,4,5,1, 3,1,5,3,5,7};
	const OvrVector3 &  torigin = a_ray.GetOrigin();
	const OvrVector3 &  tdir = a_ray.GetDir();
	float dist = 1e30f;
	int inter_sect_num = 0;
	for (int i = 0; i < 12;++i)
	{
		float a_dist = 1e30f;
		if (lvr_ray_triangle_intersect(a_dist, torigin, tdir, points[ids[3 * i]], points[ids[3 * i+1]], points[ids[3 * i+2]]))
		{
			inter_sect_num++;
			if (dist > a_dist)
			{
				dist = a_dist;
			}
		}
	}
	if (inter_sect_num > 0)
	{
		ao_dist = dist;
	}
	return inter_sect_num;
}

void OvrBox3f::transform_affine(const OvrMatrix4& a_matrix)
{
	OvrVector3 center = get_center();
	OvrVector3 halfSize = get_size() * 0.5f;
	OvrVector3 new_center = a_matrix.transform_point(center);
	OvrVector3 new_halfSize(
		fabsf(a_matrix(0, 0)) * halfSize[0] + fabsf(a_matrix(1, 0)) * halfSize[1] + fabsf(a_matrix(2, 0)) * halfSize[2],
		fabsf(a_matrix(0, 1)) * halfSize[0] + fabsf(a_matrix(1, 1)) * halfSize[1] + fabsf(a_matrix(2, 1)) * halfSize[2],
		fabsf(a_matrix(0, 2)) * halfSize[0] + fabsf(a_matrix(1, 2)) * halfSize[1] + fabsf(a_matrix(2, 2)) * halfSize[2]);
	_max = new_center + new_halfSize;
	_min = new_center - new_halfSize;
}

OvrIntersectStatus OvrBox3f::intersect(const OvrBox3f & a_b) const
{
	const OvrVector3& t_max = a_b.get_max();
	const OvrVector3& t_min = a_b.get_min();
	OvrVector3 t_sizeb = a_b.get_size();
	OvrVector3 t_sizea = get_size();
	OvrVector3 t_size_add = t_sizea + t_sizeb;
	OvrVector3 t_leave = ((t_max + t_min) - (_min+_max))*float(0.5);
	if (fabs(t_leave[0]) > t_size_add[0] || fabs(t_leave[1]) > t_size_add[1] || fabs(t_leave[2]) > t_size_add[2])
	{
		return kIntersectLeave;
	}
	OvrVector3 t_size_leave = t_sizea - t_sizeb;
	if (fabs(t_leave[0]) < fabs(t_size_leave[0]) && fabs(t_leave[1]) < fabs(t_size_leave[1]) && fabs(t_leave[2]) < fabs(t_size_leave[2]))
	{
		return kIntersectInside;
	}
	return kIntersectPart;
}