﻿#include <math.h>
#include "wx_vector4.h"

const OvrVector4 OvrVector4::zero;

float& OvrVector4::operator[](int a_id)
{
	return _m[a_id];
}


const float& OvrVector4::operator[](int a_id) const
{
	return _m[a_id];
}


OvrVector4 OvrVector4::operator+(const OvrVector4& a_v4) const
{
	return OvrVector4(_x + a_v4[0], _y + a_v4[1], _z + a_v4[2], _w + a_v4[3]);
}


OvrVector4 OvrVector4::operator-(const OvrVector4& a_v4) const
{
	return OvrVector4(_x - a_v4[0], _y - a_v4[1], _z - a_v4[2], _w - a_v4[3]);
}


float OvrVector4::operator*(const OvrVector4& a_v4) const
{
	return _x*a_v4[0] + _y*a_v4[1] + _z*a_v4[2] + _w*a_v4[3];
}


OvrVector4 OvrVector4::operator*(float a_scale) const
{
	return OvrVector4(_x*a_scale, _y*a_scale, _z*a_scale, _w*a_scale);
}


OvrVector4 OvrVector4::operator/(float a_scale) const
{
	return OvrVector4(_x / a_scale, _y / a_scale, _z / a_scale, _w / a_scale);
}


void OvrVector4::operator+=(const OvrVector4& a_v4)
{
	_x += a_v4[0];
	_y += a_v4[1];
	_z += a_v4[2];
	_w += a_v4[3];
}


void OvrVector4::operator-=(const OvrVector4& a_v4)
{
	_x -= a_v4[0];
	_y -= a_v4[1];
	_z -= a_v4[2];
	_w -= a_v4[3];
}


void OvrVector4::operator*=(float a_scale)
{
	_x *= a_scale;
	_y *= a_scale;
	_z *= a_scale;
	_w *= a_scale;
}


void OvrVector4::operator/=(float a_scale)
{
	_x /= a_scale;
	_y /= a_scale;
	_z /= a_scale;
	_w /= a_scale;
}

bool OvrVector4::operator == (const OvrVector4& a_v4)const
{
	return _x == a_v4._x && _y == a_v4._y && _z == a_v4._z && _w == a_v4._w;
}

bool OvrVector4::operator<(const OvrVector4& a_v2) const
{
	if (_x < a_v2[0])
	{
		return true;
	}
	else if (_x == a_v2[0])
	{
		if (_y < a_v2[1])
		{
			return true;
		}
		else if (_y == a_v2[1])
		{
			if (_z < a_v2[2])
			{
				return true;
			}
			else if (_z == a_v2[2] && _w < a_v2[3])
			{
				return true;
			}
		}
	}
	return false;
}


OvrVector3 OvrVector4::xyz() const
{
	return OvrVector3(_x, _y, _z);
}


float OvrVector4::length() const
{
	return (float)sqrt(length_sq());
}


float OvrVector4::length_sq() const
{
	return _x*_x + _y*_y + _z*_z + _w*_w;
}


void OvrVector4::normalize()
{
	float t_l = length();
	*this /= t_l;
}


OvrVector4 OvrVector4::get_normalize() const
{
	float t_l = length();
	return OvrVector4(_x / t_l, _y / t_l, _z / t_l, _w / t_l);
}


OvrVector4 OvrVector4::cross(const OvrVector4& a_v4) const
{
	return OvrVector4(_y*a_v4[2] - _z*a_v4[1],
		_z*a_v4[0] - _x*a_v4[2],
		_x*a_v4[1] - _y*a_v4[0], 0);
}


OvrVector4 OvrVector4::multi(const OvrVector4& a_v4) const
{
	return OvrVector4(_m[0] * a_v4[0], _m[1] * a_v4[1], _m[2] * a_v4[2], _m[3] * a_v4[3]);
}


OvrVector4 OvrVector4::lerp(const OvrVector4& a_v4, float a_pos) const
{
	return OvrVector4(_x*a_pos + a_v4[0] * (1.0f - a_pos), _y*a_pos + a_v4[1] * (1.0f - a_pos), _z*a_pos + a_v4[2] * (1.0f - a_pos), _w*a_pos + a_v4[3] * (1.0f - a_pos));
}


bool OvrVector4::is_nan() const
{
	return _x != _x || _y != _y || _z != _z || _w != _w;
}



