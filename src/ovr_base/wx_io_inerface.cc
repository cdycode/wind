﻿#include "wx_io_interface.h"
#include "wx_file_tools.h"
#include <stdlib.h>
#include <string.h>

#define GET_MIN( x, y )					( ( x ) > ( y ) ? ( y ) : ( x ) )
#define GET_MAX( x, y )					( ( x ) < ( y ) ? ( y ) : ( x ) )

OvrIOFileHandle::OvrIOFileHandle() : iLength( 0 ), iPosition( 0 ), iFile( NULL )
{
}

OvrIOFileHandle::~OvrIOFileHandle()
{
	if ( iFile != NULL )
	{
		fclose ( iFile );
		iFile = NULL;
	}
}

bool OvrIOFileHandle::Open( const char* filePath, const bool isWrite )
{
	FILE* file = NULL;

	if ( isWrite )
	{
		file = OvrFileTools::CreateFileX( filePath );
	}
	else
	{
		file = OvrFileTools::OpenFile( filePath );
	}

	if ( file != NULL )
	{
		SetFile( file );
		return true;
	}

	return false;
}

ovr::uint32 OvrIOFileHandle::Append ( const void* data, ovr::uint32 len )
{
	if ( iFile == NULL || data == NULL || len == 0 )
	{
		return 0;
	}

	ovr::uint32 temp = ( ovr::uint32 )fwrite ( data, 1, len, iFile );
	iPosition += temp;

	return temp;
}

ovr::uint32 OvrIOFileHandle::Subtract ( void* dest, ovr::uint32 len )
{
	if ( iFile == NULL || iLength == 0 || dest == NULL || len == 0 )
	{
		return 0;
	}

	ovr::uint32 temp = ( ovr::uint32 )fread ( dest, 1, len, iFile );
	iPosition += temp;

	return temp;
}

ovr::uint32 OvrIOFileHandle::GetLength()
{
	if ( iFile == NULL )
	{
		iLength = 0;
	}
	else
	{
		fseek ( iFile, 0L, SEEK_END );
		iLength = ( ovr::uint32 )ftell ( iFile );
		fseek ( iFile, iPosition, SEEK_SET );
	}

	return iLength;
}

void OvrIOFileHandle::SetPosition (ovr::uint32 pos )
{
	if ( GetLength() == 0 )
	{
		return;
	}

	if ( pos <= iLength )
	{
		iPosition = pos;
		fseek ( iFile, iPosition, SEEK_SET );
	}
	else
	{
		fseek ( iFile, 0L, SEEK_END );
		iPosition = iLength;
	}
}

ovr::uint32 OvrIOFileHandle::GetPosition()
{
	return iPosition;
}

OvrIOFileHandle* OvrIOFileHandle::CreateHandleFromFile( const char* filePath )
{
	FILE* file = OvrFileTools::CreateFileX( filePath );

	if ( file != NULL )
	{
		OvrIOFileHandle* handle = new OvrIOFileHandle();
		handle->SetFile( file );
		return handle;
	}

	return NULL;
}

OvrIOFileHandle* OvrIOFileHandle::LoadHandleFromFile( const char* filePath )
{
	FILE* file = OvrFileTools::OpenFile( filePath );

	if ( file != NULL )
	{
		OvrIOFileHandle* handle = new OvrIOFileHandle();
		handle->SetFile( file );
		return handle;
	}

	return NULL;
}

void OvrIOFileHandle::SetFile( FILE* file )
{
	iFile = file;
	GetLength();
}

/*-----------------------------------------------------------------------------------------------*/

OvrIOVarBufferHandle::OvrIOVarBufferHandle( const ovr::uint32 capacity, bool isInert ) : iBuffer ( NULL ), iLength ( 0 ), iCapacity ( 0 ), iPosition ( 0 ), iModel ( isInert )
{
	Resize ( capacity );
}

OvrIOVarBufferHandle::OvrIOVarBufferHandle( const void* buffer, const ovr::uint32 len, bool isInert )
{
    iBuffer = NULL;
    iModel = isInert;
    iLength = iCapacity  = iPosition = 0;
    Resize ( len );
	Append ( buffer, len );
}

OvrIOVarBufferHandle::~OvrIOVarBufferHandle()
{
	if ( iBuffer != NULL )
	{
		free ( iBuffer );
		iBuffer = NULL;
		Resize ( 0 );
	}
}

void OvrIOVarBufferHandle::ClearData()
{
	if ( iBuffer != NULL )
	{
		memset ( iBuffer, 0, iCapacity );
	}

	iCapacity = iLength = 0;
}

ovr::uint32 OvrIOVarBufferHandle::GetLength()
{
	return iLength;
}

void OvrIOVarBufferHandle::SetPosition (ovr::uint32 pos )
{
	if ( pos <= iLength && pos <= iCapacity )
	{
		iPosition = pos;
	}
}

ovr::uint32 OvrIOVarBufferHandle::GetPosition()
{
	return iPosition;
}

const ovr::uint8* OvrIOVarBufferHandle::GetBuffer()
{
	return iBuffer;
}

void OvrIOVarBufferHandle::Resize ( const ovr::uint32 new_size )
{
	if ( iBuffer == NULL )
	{
		iCapacity = iLength = 0;
	}

	if ( new_size > iCapacity || ( new_size < iCapacity && new_size >= iLength ) )
	{
		ovr::uint8* buf = (ovr::uint8* ) realloc ( iBuffer, new_size );

		/*
		        In case realloc fails, there's not much we can do, except keep things as they are.
		        Note that NULL is a valid return value from realloc when iCapacity == 0, but that is covered too.
		*/
		if ( buf == NULL && new_size != 0 )
		{
			return;
		}

		iBuffer = buf;
		iCapacity = new_size;
	}
}

ovr::uint32 OvrIOVarBufferHandle::Append ( const void* data, ovr::uint32 len )
{
	if ( iBuffer == NULL )
	{
		Resize ( 1024 );
	}

	if ( iModel )
	{
		return Insert ( data, len );
	}
	else
	{
		return Overlap ( data, len );
	}
}

ovr::uint32 OvrIOVarBufferHandle::Subtract ( void* dest, ovr::uint32 len )
{
	if ( iBuffer == NULL || iLength == 0 || iCapacity == 0 || iPosition + len > iLength || iPosition + len > iCapacity || dest == NULL )
	{
		return 0;
	}

	memcpy ( dest, iBuffer + iPosition, len );
	iPosition += len;

	return len;
}

ovr::uint32 OvrIOVarBufferHandle::Overlap ( const void* buf, ovr::uint32 len )
{
	if ( iBuffer == NULL || buf == NULL || len == 0 )
	{
		return 0;
	}

	/* check overflow */
	if ( ~ (ovr::uint64) 0 - (ovr::uint64) iBuffer < len )
	{
		return 0;
	}

	if ( iLength <= iCapacity && iPosition <= iLength )
	{
		/*have space yet*/
		if ( iPosition + len <= iCapacity )
		{
			memcpy ( iBuffer + iPosition, buf, len );
			iPosition += len;//update pos
			iLength = GET_MAX ( iLength, iPosition );
		}
		else/*no more space*/
		{
			ovr::uint8* temp = NULL;
			ovr::uint32 new_size = (ovr::uint32) ( ( iLength + len ) << 1 );

			if ( ( temp = (ovr::uint8* ) realloc ( iBuffer, new_size ) ) != NULL )
			{
				iBuffer = temp;

				memcpy ( iBuffer + iPosition, buf, len );

				iPosition += len;//update pos
				iLength = iPosition;
				iCapacity = new_size;
			}
			else
			{
				len = 0;
			}
		}

		return len;
	}

	return 0;
}

ovr::uint32 OvrIOVarBufferHandle::Insert ( const void* buf, ovr::uint32 len )
{
	if ( iBuffer == NULL || buf == NULL || len == 0 )
	{
		return 0;
	}

	/* check overflow */
	if ( ~ (ovr::uint64) 0 - (ovr::uint64) iBuffer < len )
	{
		return 0;
	}

	if ( iLength <= iCapacity && iPosition <= iLength )
	{
		/*have space yet*/
		if ( iLength + len <= iCapacity )
		{
			if ( iLength > iPosition )
			{
				memmove ( iBuffer + iPosition + len, iBuffer + iPosition, iLength - iPosition );
			}

			memcpy ( iBuffer + iPosition, buf, len );
			iLength += len;
			iPosition += len;
		}
		else/*no more space*/
		{
			ovr::uint8* temp = NULL;
			ovr::uint32 new_size = (ovr::uint32) ( ( iLength + len ) << 1 );

			if ( ( temp = (ovr::uint8* ) realloc ( iBuffer, new_size ) ) != NULL )
			{
				iBuffer = temp;

				if ( iLength > iPosition )
				{
					memmove ( iBuffer + iPosition + len, iBuffer + iPosition, iLength - iPosition );
				}

				memcpy ( iBuffer + iPosition, buf, len );

				iLength += len;
				iPosition += len;
				iCapacity = new_size;
			}
			else
			{
				len = 0;
			}
		}

		return len;
	}

	return 0;
}

void OvrIOVarBufferHandle::Remove (ovr::uint32 len )
{
	if ( len > 0 && len <= iLength )
	{
		memmove ( iBuffer, iBuffer + len, iLength - len );
		iLength -= len;
	}
}

void OvrIOVarBufferHandle::PrintHex ( const char* tag )
{
	if ( iBuffer == NULL || iLength == 0 || iLength > iCapacity )
	{
		printf ( "%s:no data\n", tag );
		return;
	}

	printf ( "%s:\n", tag );

	ovr::uint32 i = 0, k = 0, j = 0, count = 0, num = 0;

	for ( i = 0; i < iLength; i += 16 )
	{
		count = ( iLength < i + 16 ) ? iLength : ( i + 16 );

		for ( k = i; k < count; k += 4 )
		{
			num = ( iLength < k + 4 ) ? iLength : ( k + 4 );

			for ( j = k; j < num; j++ )
			{
				printf ( "%02X ", iBuffer[j] & 0xFF );
			}

			printf ( " " );
		}

		printf ( "\n" );
	}
}
