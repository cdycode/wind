﻿#include <math.h>
#include "wx_plane3f.h"

OvrPlane3f::OvrPlane3f() :_normal(0.0f, 0.0f, 0.0f), _distance_to_origin(0.0f)
{
}

OvrPlane3f::OvrPlane3f(const OvrVector3 a_normal, float a_dist_to_origin) :_normal(a_normal), _distance_to_origin(a_dist_to_origin)
{
}

//Here I added two arguments of the constructor functions.
const OvrVector3& OvrPlane3f::GetNormal() const
{
	return _normal;
}

void OvrPlane3f::SetNormal(const OvrVector3& a_normal)
{
	_normal = a_normal;
}

float OvrPlane3f::GetDistanceToOrigin() const
{
	return _distance_to_origin;
}

void OvrPlane3f::SetDistanceToOrigin(float a_v)
{
	_distance_to_origin = a_v;
}

float OvrPlane3f::DistanceToPoint(const OvrVector3& a_point) const
{
	return _normal*a_point + _distance_to_origin;
}

bool OvrPlane3f::ProjectVector(OvrVector3& ao_dst_vector, const OvrVector3& a_src_vector) const
{
	OvrVector3 t_vec(a_src_vector);
	t_vec.normalize();
	if (fabs(t_vec*_normal) > float(1) - OVR_TOLERANCE)
	{
		return false;
	}
	else
	{
		ao_dst_vector = a_src_vector - _normal*(a_src_vector*_normal);
	}
	return true;
}

bool OvrPlane3f::Intersect(float& ao_dist, const OvrVector3 a_ray_origin, const OvrVector3 a_ray_dir) const
{
	double d = a_ray_dir*_normal;
	double t = a_ray_origin * _normal;
	if (fabs(d) < OVR_TOLERANCE)
	{
		if (fabs(t + _distance_to_origin) < OVR_TOLERANCE)
		{
			ao_dist = 0;
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		ao_dist = (float)((_distance_to_origin - t) / d);
		return true;
	}
}
