﻿#ifndef WX_BASE_WX_MATH_CONFIGURE_H_
#define WX_BASE_WX_MATH_CONFIGURE_H_

//#define LVR_ASSERT(a)  if(!(a))printf("************something wrong happened,lvr_assert.***********\n")
#include <math.h>

#define LVR_PI	3.1415926535898
#define LVR_TWO_PI (LVR_PI*2)
#define LVR_HALF_PI (LVR_PI*0.5)
#define LVR_QUARTER_PI (LVR_PI*0.25)
#define LVR_TOLERANCE			0.00000000001
#define LVR_RAD_TO_DEG(rad)	((rad)*180.0/LVR_PI)
#define LVR_DEG_TO_RAD(deg)	((deg)*LVR_PI/180.0)

#define LVR_MIN(a,b) ((a)<(b)?(a):(b))
#define LVR_MAX(a,b) ((a)>(b)?(a):(b))

template<class T>
T lvr_acos(T a_val)
{
	if (a_val > T(1))return T(0);
	else if(a_val < T(-1)) return T(LVR_PI);
	else return (float)acos(a_val);
};

template<class T>
T lvr_asin(T a_val)
{
	if (a_val > T(1))return T(LVR_HALF_PI);
	else if(a_val < T(-1)) return T(-LVR_HALF_PI);
	else return asin(a_val);
};

template<class T>
void lvr_clamp(T& ao_val,T a_min,T a_max)
{
	if (ao_val > a_max) ao_val = a_max;
	else if(ao_val < a_min) ao_val = a_min;
};

template<class T>
T lvr_lerp(T a_begin,T a_end,float a_pos)
{
	return a_begin*(1.0f-a_pos) + a_end*a_pos;
};

template<class T>
T lvr_lerp(T a_begin,T a_end,double a_pos)
{
	return a_begin*(1.0-a_pos) + a_end*a_pos;
};

template<class T>
void lvr_swap(T& a,T& b)
{
	T tmp = b;
	b = a;
	a = tmp;
}
// Safe reciprocal square root.
template<class T>
T lvr_rcp_sqrt(const T f) { return (f >= ovr::SmallestNonDenormal) ? T(1) / (float)sqrt(f) : ovr::HugeNumber; }


enum lvr_intersect_status
{
	e_intersect_leave,
	e_intersect_part,
	e_intersect_inside
};

#endif
