﻿#include "wx_unique_name.h"
#include "wx_time.h"
#include <stdio.h>

#define UNIQUE_NAME_TRACK						"tck%llu"
#define UNIQUE_NAME_ACTOR						"act%llu"
#define UNIQUE_NAME_SENCE						"snc%llu"
#define UNIQUE_NAME_SEQUENCE					"seq%llu"
#define UNIQUE_NAME_PLOT						"plt%llu"

UniqueNameCreater::UniqueNameCreater()
{
}

UniqueNameCreater::~UniqueNameCreater()
{
}

void UniqueNameCreater::CreateTrackUniqueName(OvrString& string)
{
	char temp[128] = { 0 };
	sprintf(temp, UNIQUE_NAME_TRACK, OvrTime::GetTicksNanos());
	string.SetString(temp);
}

void UniqueNameCreater::CreateActorUniqueName(OvrString& string)
{
	char temp[128] = { 0 };
	sprintf(temp, UNIQUE_NAME_ACTOR, OvrTime::GetTicksNanos());
	string.SetString(temp);
}

void UniqueNameCreater::CreatePlotUniqueName(OvrString& string)
{
	char temp[128] = { 0 };
	sprintf(temp, UNIQUE_NAME_PLOT, OvrTime::GetTicksNanos());
	string.SetString(temp);
}

void UniqueNameCreater::CreateSenceUniqueName(OvrString& string)
{
	char temp[128] = { 0 };
	sprintf(temp, UNIQUE_NAME_SENCE, OvrTime::GetTicksNanos());
	string.SetString(temp);
}

void UniqueNameCreater::CreateSequenceUniqueName(OvrString& string)
{
	char temp[128] = { 0 };
	sprintf(temp, UNIQUE_NAME_SEQUENCE, OvrTime::GetTicksNanos());
	string.SetString(temp);
}
