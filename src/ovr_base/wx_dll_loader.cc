﻿#include "wx_type.h"
#include <wx_dll_loader.h>

#ifdef OVR_OS_WIN32
#	include <windows.h>
#else
#   include <dlfcn.h>
#endif

bool OvrDllLoader::Load(const OvrString& a_dll_path) 
{
	if (nullptr != dll_handle)
	{
		Unload();
	}

#ifdef OVR_OS_WIN32
	dll_handle = LoadLibraryA(a_dll_path.GetStringConst());
#else
	dll_handle = dlopen(a_dll_path.GetStringConst(), RTLD_LAZY | RTLD_GLOBAL | RTLD_NODELETE);
#endif
	return (nullptr != dll_handle);
}

void OvrDllLoader::Unload() 
{
	if (nullptr != dll_handle)
	{
#ifdef OVR_OS_WIN32
		FreeLibrary((HMODULE)dll_handle);
#else
		dlclose(dll_handle);
#endif
		dll_handle = nullptr;
	}
}

void* OvrDllLoader::GetFunctionAddress(const OvrString& a_function_name) 
{
	if (nullptr != dll_handle)
	{
#ifdef OVR_OS_WIN32
		return GetProcAddress((HMODULE)dll_handle, a_function_name.GetStringConst());
#else
		return dlsym(dll_handle, a_function_name.GetStringConst());
#endif
	}
	return nullptr;
}