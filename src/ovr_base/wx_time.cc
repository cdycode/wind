﻿#include "wx_time.h"
#include "wx_type.h"

#if defined(OVR_OS_WIN32)
#	include <windows.h>
#else
#	include <time.h>
#   include <unistd.h>
#	include <sys/time.h>
#endif

double OvrTime::GetSeconds()
{
	return double(GetTicksNanos())*0.000000001;
}


ovr::uint32	OvrTime::GetTicksMs() 
{
	return ovr::uint32(GetTicksNanos() / 1000000);
}

#if defined(OVR_OS_WIN32)
ovr::uint64 OvrTime::GetTicksNanos()
{
	static long long CPUFrequency = 0;
	if (0 == CPUFrequency)
	{
		::QueryPerformanceFrequency((LARGE_INTEGER*)&CPUFrequency);
	}
	long long count = 0;
	long long current = 0;
	BOOL bResult = QueryPerformanceCounter((LARGE_INTEGER*)&current);
	if (bResult && CPUFrequency > 0)
	{
		count = (long long)(((double)current / CPUFrequency) * 1000000000.0);
	}
	else
	{
		count = (long long)::GetTickCount()* 1000000;
	}
	return count;
}

#elif defined(LVR_OS_ANDROID)

ovr::uint64 OvrTime::GetTicksNanos()
{
	struct timespec tp;
	const int status = clock_gettime(CLOCK_MONOTONIC, &tp);

	if (status != 0)
	{
		//LVR_LOG("clock_gettime status=%i", status );
	}
	const uint64_t result = (uint64_t)tp.tv_sec * (uint64_t)(1000 * 1000 * 1000) + uint64_t(tp.tv_nsec);
	return result;
}

#else

ovr::uint64 OvrTime::GetTicksNanos()
{
	uint64_t t_result;
	struct timeval tv;
	gettimeofday(&tv,0);
	t_result = (uint64_t)tv.tv_sec*1000000;
	t_result += tv.tv_usec;
	return t_result*1000;
}

#endif


//单位为毫秒
void OvrTime::SleepMs(ovr::uint32 a_sleep_ms)
{
#if defined(OVR_OS_WIN32)
	timeBeginPeriod(1);
	Sleep(a_sleep_ms);
	timeEndPeriod(1);
#else
    usleep(a_sleep_ms*1000);
#endif
}
