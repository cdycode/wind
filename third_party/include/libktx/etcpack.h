#ifndef __etc_pack_h__20180224__
#define __etc_pack_h__20180224__

#ifdef __cplusplus
extern "C" {
#endif
	static enum{ ETC1_RGB_NO_MIPMAPS, ETC2PACKAGE_RGB_NO_MIPMAPS, ETC2PACKAGE_RGBA_NO_MIPMAPS_OLD, ETC2PACKAGE_RGBA_NO_MIPMAPS, ETC2PACKAGE_RGBA1_NO_MIPMAPS, ETC2PACKAGE_R_NO_MIPMAPS, ETC2PACKAGE_RG_NO_MIPMAPS, ETC2PACKAGE_R_SIGNED_NO_MIPMAPS, ETC2PACKAGE_RG_SIGNED_NO_MIPMAPS, ETC2PACKAGE_sRGB_NO_MIPMAPS, ETC2PACKAGE_sRGBA_NO_MIPMAPS, ETC2PACKAGE_sRGBA1_NO_MIPMAPS };
	static enum { MODE_COMPRESS, MODE_UNCOMPRESS, MODE_PSNR };
	static enum { SPEED_SLOW, SPEED_FAST, SPEED_MEDIUM };
	static enum { METRIC_PERCEPTUAL, METRIC_NONPERCEPTUAL };
	static enum { CODEC_ETC, CODEC_ETC2 };

	void compressImagebuf2fileM(unsigned char *img, unsigned char *alphaimg, int width, int height, unsigned char *dst_buf, int* dst_len, int expandedwidth, int expandedheight,
		int a_mode, int a_speed, int a_metric, int a_codec, int a_format,int a_ktx_file,int suggest_mipmap_level);
	void compressImagebuf2buf(unsigned char *img, unsigned char *alphaimg, int width, int height, unsigned char *dst_buf, int* dst_len, int expandedwidth, int expandedheight,
		int a_mode, int a_speed, int a_metric, int a_codec, int a_format);
	int getDstbufSize(int width, int height, int a_format);
	int getKtxHeaderSize();

#ifdef __cplusplus
}
#endif

#endif