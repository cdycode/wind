#ifndef __lvr_ui_manager_20160524_h__
#define __lvr_ui_manager_20160524_h__

#include "lvr_ui_menu.h"
#include "lvr_ui_multi_image.h"

class LVR_UI_API lvr_ui_manager
{
public:
	lvr_ui_manager();
	~lvr_ui_manager();
public:
	void init(uint32_t a_max_rect_to_show);
	void uninit();
	void add_multi_image(lvr_ui_multi_image* a_umi);
	void add_menu(lvr_ui_menu* a_menu);
	bool remove_menu(lvr_ui_menu* a_menu);
	bool remove_multi_image(lvr_ui_multi_image* a_umi);
	lvr_ui_base* update(const lvr_vector3f& a_start, const lvr_vector3f& a_dir, double a_cur_time);
	void draw(const lvr_matrix4f& a_vp_mat);
	void text_draw(const lvr_matrix4f & a_vp_mat);
private:
	void collect_visible_uis();
	void update_render_object();
	void generate_gl_objects();
private:
	std::vector<lvr_ui_multi_image*>	_multi_image_uis;
	std::vector<lvr_ui_menu*>	_ui_menus;
	std::vector<lvr_ui_2d*>		_all_visible_uis;

	lvr_vertex_buffer*			_vertex_buffer;
	lvr_index_buffer*			_index_buffer;
	lvr_vertex_format*			_vf;
	lvr_program*				_prog;
	uint32_t					_draw_index_num;
	uint32_t					_max_rect_num;
	uint32_t					_reserve_vertex_num;

	lvr_texture*				_ui_texture;

	lvr_program*				_text_prog;

	bool						_update_ui_menu;
};

#endif
