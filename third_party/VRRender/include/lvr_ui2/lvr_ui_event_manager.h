#ifndef __lvr_event_manager_h__
#define __lvr_event_manager_h__

#include "lvr_ui_base.h"

typedef void* (*ui_call_back)(lvr_ui_base* a_hit_ui,void* a_object);

enum e_input_event
{
	e_focus_in,
	e_focus_out,
	e_click
};

struct lvr_event_binding
{
	lvr_ui_base*	_ui;
	void*			_object;
	e_input_event	_event;
	ui_call_back	_func;
};
class LVR_UI_API lvr_event_manager
{
public:
	static lvr_event_manager* get_ui_event_manager();
	static void release();
	bool  add_event_binding(lvr_event_binding a_event_binding);
	bool  remove_event_binding(lvr_ui_base* a_ui_base_ptr,e_input_event a_event_type);
	bool  on_ui_call_back(lvr_ui_base* a_ui);
	bool  on_ui_call_back_focus_in(lvr_ui_base* a_ui);
	bool  on_ui_call_back_focus_out(lvr_ui_base* a_ui);
	~lvr_event_manager(){}
private:
	lvr_event_manager();
	static lvr_event_manager*	_event_mgr;
private:
	std::map<lvr_ui_base*, lvr_event_binding>		_ui_click_map;
	std::map<lvr_ui_base*, lvr_event_binding>		_ui_focus_in_map;
	std::map<lvr_ui_base*, lvr_event_binding>		_ui_focus_out_map;
};

#endif