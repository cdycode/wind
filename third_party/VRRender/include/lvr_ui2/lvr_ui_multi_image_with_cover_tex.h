#ifndef _lvr_ui_multi_image_with_cover_tex__
#define _lvr_ui_multi_image_with_cover_tex__

#include "lvr_ui_configure.h"
#include "lvr_core_lib.h"
#include "lvr_math_configure.h"
#include "lvr_vector3.h"
#include "lvr_matrix4.h"
#include "lvr_vector2.h"
//#include "lvr_singleton.h"

class lvr_program;
class lvr_vertex_buffer;
class lvr_index_buffer;
class lvr_vertex_format;
class lvr_ui_2d;
class lvr_texture2d;
class LVR_UI_API lvr_ui_multi_image_with_cover_tex {
		//   round  corner 
		//
public:
	lvr_ui_multi_image_with_cover_tex();
	~lvr_ui_multi_image_with_cover_tex();

	bool init(int num, int a_image_width, int a_image_height, float radius, const std::string& cover_tex);
	bool uninit();
	int  add(lvr_ui_2d* img_2d);
	void update(const lvr_vector3f& a_pos, const lvr_vector3f& a_look, const lvr_vector3f& a_right, double curr_time);
	void update();

	void draw(const lvr_matrix4f& a_vp_mat);

	bool update_image(uint32_t surface_id, const char* a_buf, int a_w, int a_h);
	bool set_img_visible(int surface_id, bool is_show);
	lvr_ui_2d*  test_ray(const lvr_vector3f& a_start, const lvr_vector3f& a_dir, float& ao_dist);
	bool is_visible();
	void set_visible(bool a_visible);
	void set_to_update();
	void set_alpha(float alpha);
	void set_color_mask(int id, float rate);

private:
	void generate_image_rects();

private:
	struct img_node{
		uint32_t		_id;
		lvr_ui_2d*		_ui_2d;
		float			_color_rate;
		int				_width;
		int				_height;
	};

	lvr_program*				_prog;
	lvr_vertex_buffer*			_vb;
	lvr_index_buffer*			_ib;
	lvr_vertex_format*			_vf;
	lvr_texture2d*				_texture;
	uint32_t					_max_ui_num;
	uint32_t					_draw_ui_num;
	bool						_is_need_update;
	bool						_is_visible;


	std::vector<img_node*>		_vec_img_2d;
	uint32_t					_gen_img_id;

	std::vector<lvr_vector4f>	_rect_uv1x;
	std::vector<lvr_vector4f>	_rect_uv1y;
	std::vector<lvr_vector4f>	_rect_uv2x;
	std::vector<lvr_vector4f>	_rect_uv2y;
	std::vector<lvr_vector2i>	_location_on_tex;
	int							_max_width_per_img;
	int							_max_height_per_img;
	float						_radius_of_corner;
	int							_uniform_color;
	float						_uniform_color_alpha;
	std::string					_cover_texture;
};




#endif