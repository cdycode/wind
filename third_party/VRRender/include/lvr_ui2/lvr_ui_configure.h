#ifndef __lvr_ui_configure_20160525_h__
#define __lvr_ui_configure_20160525_h__
#include "lvr_core_configure.h"
#ifdef lvr_ui2_EXPORTS
	#ifdef LVR_OS_ANDROID
	#define LVR_UI_API
	#else
	#define LVR_UI_API __declspec(dllexport)
	#endif // LVR_OS_ANDROID
#elif defined lvr_ui_IMPORT
	#ifdef LVR_OS_ANDROID
	#define LVR_UI_API
	#else
	#define LVR_UI_API __declspec(dllimport)
	#endif // LVR_OS_ANDROID#else
#else
	#define LVR_UI_API
#endif

#include "lvr_core_lib.h"
#include "lvr_math_lib.h"
// #ifdef LVR_OS_WIN32
// #define lvr_render_IMPORT
// #endif
#include "lvr_render_lib.h"
#include "lvr_bitmap_font_lib.h"

#endif
