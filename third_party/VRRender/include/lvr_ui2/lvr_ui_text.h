#ifndef __lvr_ui_text_20160524_h__
#define __lvr_ui_text_20160524_h__

#include "lvr_ui_base.h"

class LVR_UI_API lvr_ui_text : public lvr_ui_base
{
public:
	lvr_ui_text();
	~lvr_ui_text();
	void set_text(const char* a_str);
	void set_text_align(lvr_enum_font_align_method a_align_method);
	virtual void set_position(const lvr_vector3f& pos);
	virtual void set_orientation(const lvr_quaternionf& a_q);
	virtual void set_orientation(const lvr_vector3f& a_right, const lvr_vector3f& a_up);
	//update only if necessary,or have called the above methods.
	void update();
	virtual int  test_ray(const lvr_vector3f& a_pos, const lvr_vector3f& a_dir, float & ao_dist);
	virtual uint32_t get_vertex_num();
	virtual void update_vertexs(ui_vertex* a_vertexs, uint32_t& ao_cur_offset);
	virtual void set_pickable(bool a_pick_state);
	void set_visible(bool a_visible);
	void set_size(const lvr_vector2f& a_size);
	void set_warp_width(float a_width);
	//default it is 0,set to other num if you want to cut words.
	void set_line_warp(int a_max_lines);
	float get_warp_width();
	const lvr_vector2f& get_size();
	void set_font_size(float a_font_size);
	void set_font_color(const lvr_vector4uc& a_font_color);
	lvr_vector3f get_pos();
	void set_line_spacing(float a_line_spacing);
	void set_font_spacing(float a_font_spacing);
	int get_text_id();
protected:
	//these methods are called to make sure the text can put inside our text.
	void warp_text(std::string& ao_res,const char* ao_text);
	void generate_collider();
private:
	lvr_vector3f		_position;
	lvr_vector2f		_size;
	lvr_vector3f		_right;
	lvr_vector3f		_up;
	//text and font
	std::string			_text;
	int					_text_id;
	float				_warp_width;
	float               _line_spacing;
	float               _font_spacing;
	lvr_vector3f		_text_local_pos;
	lvr_enum_font_align_method	_text_align;
	float				_font_size;
	float				_font_scale;
	int					_lines_to_warp;
	lvr_vector4uc		_font_color;
	lvr_bitmap_font_manager*	_bitmap_font_manager;

	lvr_vector3f		_points[4];
	bool				_visible;
	bool				_text_change;
	bool				_pickable;
	bool				_collision_update;
};

#endif
