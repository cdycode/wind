#ifndef __lvr_ui_element_20160524_h__
#define __lvr_ui_element_20160524_h__

#include "lvr_ui_configure.h"

#define  LVR_UI_UPDATE_RO	0x01
#define  LVR_UI_UPDATE_TEXT	0x02


class lvr_ui_base
{
public:
	struct ui_vertex
	{
		lvr_vector3f	_pos;
		lvr_vector2f	_uv;
	};
public:
	lvr_ui_base(){}
	virtual ~lvr_ui_base(){}
public:
	virtual void set_position(const lvr_vector3f& pos) = 0;
	virtual void set_orientation(const lvr_quaternionf& a_q) = 0;
	virtual void set_orientation(const lvr_vector3f& a_right, const lvr_vector3f& a_up) = 0;
	virtual int  test_ray(const lvr_vector3f& a_pos, const lvr_vector3f& a_dir, float & ao_dist) = 0;
	virtual uint32_t  get_vertex_num() = 0;
	virtual void update_vertexs(ui_vertex* a_vertexs, uint32_t& ao_cur_offset) = 0;
	virtual void set_pickable(bool a_pick_state) = 0;
};

#endif
