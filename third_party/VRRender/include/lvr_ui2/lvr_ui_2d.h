/*!
 * \file lvr_ui_2d.h
 *
 * \brief
 *
 * \author PengYun 
 * \contact pengyun@chengzivr.com
 * \place Beijing BaoNeng Center
 * \date ���� 2016
 */
#ifndef __lvr_ui_2d_20160525_h__
#define __lvr_ui_2d_20160525_h__

#include "lvr_ui_base.h"

class LVR_UI_API lvr_ui_2d : public lvr_ui_base
{
public:
	struct tex_info
	{
	public:
		bool operator ==(tex_info& a_ti)
		{
			return _texture2d == a_ti._texture2d && umin == a_ti.umin
				&& vmin == a_ti.vmin && umax == a_ti.umax && vmax == a_ti.vmax;
		}
		lvr_texture*	_texture2d;
		float umin, vmin, umax, vmax;
	};
public:
	lvr_ui_2d();
	~lvr_ui_2d();
public:
	bool operator <(lvr_ui_2d* a_ui_2d)const;
public:
	virtual void set_position(const lvr_vector3f& pos);
	virtual void set_orientation(const lvr_quaternionf& a_q);
	virtual int test_ray(const lvr_vector3f& a_pos, const lvr_vector3f& a_dir, float & ao_dist);
	virtual void update_vertexs(ui_vertex* a_vertexs, uint32_t& ao_cur_offset);
	virtual uint32_t get_vertex_num();
	virtual void set_pickable(bool a_pick_state);
public:
	//after set all the options,call update manually;
	virtual bool need_update();
	virtual void set_visible(bool _visible);
	virtual bool get_visible();
	virtual void set_update_state(bool a_need_update);
	virtual void set_size(const lvr_vector2f& a_size);
	virtual void set_segment(int a_segment, bool a_horizontal);
	virtual void set_orientation(const lvr_vector3f& a_right, const lvr_vector3f& a_up);
	virtual const lvr_vector3f& get_pos();
	virtual const lvr_vector3f& get_right();
	virtual const lvr_vector3f& get_up();
	virtual const lvr_vector2f& get_size();
	virtual void set_uv_info(tex_info& a_texinfo);
	virtual const lvr_ui_2d::tex_info& get_uv_info();
private:
	void generate_collider();
public:
	int				_layer_id;
protected:
	lvr_vector3f	_position;
	lvr_vector2f	_size;
	lvr_vector3f	_right;
	lvr_vector3f	_up;
	tex_info		_tex_info;
	int				_seg_num;
	std::vector<lvr_vector3f>	_points;//for ray test
	bool			_horizontal_align;
	bool			_need_update;
	bool			_visible;
	bool			_pickable;
};

#endif
