#ifndef __lvr_ui_state_h__
#define __lvr_ui_state_h__

#include "lvr_ui_base.h"
#include "lvr_ui_event_manager.h"
#include "lvr_gaze_cursor.h"

class LVR_UI_API lvr_ui_state
{
public:
	enum focus_anim_style
	{
		anim_none,
		anim_cursor
	};
public:
	lvr_ui_state();
	~lvr_ui_state();
	void set_check_style(int a_style);
	void init(lvr_gaze_cursor* a_cursor);
	void add_except_ui(lvr_ui_base* a_base);
	lvr_ui_base*  get_active_ui();
	void clear_except_uis();
	bool on_key_click();
	void update(lvr_ui_base* a_hit_ui,double a_time);
	void set_ui_auto_trigger_click(bool is_auto);
private:
	const int  EXCEPT_UIS_NUM = 3;
	lvr_ui_base*		_current_active_ui;
	lvr_gaze_cursor*	_gaze_cursor;
	double				_active_time; 
	double				_fire_time;
	lvr_ui_base*		_last_klick_ui;
	std::vector<lvr_ui_base*>  _except_list;
	lvr_ui_base*        _except_ui;
	int					_check_style;
	bool				_is_auto_trigger_click;
};

#endif
