#ifndef __lvr_ui_multi_image_h__
#define __lvr_ui_multi_image_h__

#include "lvr_ui_2d.h"
#include "lvr_texture2d.h"


class LVR_UI_API lvr_ui_multi_image
{
public:
	lvr_ui_multi_image();
	~lvr_ui_multi_image();
public:
	bool init(int num,int a_image_width,int a_image_height, bool a_has_alpha);
	int  add_ui_2d(lvr_ui_2d* a_ui);
	//you can set pos,orientation to the ui and so on.
	lvr_ui_2d*	get_ui(uint32_t a_id);
	//call this when you change ui textures.
	bool update_image(uint32_t surface_id,const char* a_buf,int a_w,int a_h);
	//when you set ui info outside,when finished call this.
	void update();

	//called by ui_manager you do not need to call yourself
	//do not intersect ui inside this multi_image ui,we do not judge that case.return the first one pick.
	lvr_ui_2d*  test_ray(const lvr_vector3f& a_start, const lvr_vector3f& a_dir, float& ao_dist);
	void draw(const lvr_matrix4f& a_vp_mat);
	void release_all();
private:
	void generate_image_rects();
private:
	lvr_program*			_prog;
	lvr_vertex_buffer*		_vb;
	lvr_index_buffer*		_ib;
	lvr_vertex_format*		_vf;
	uint32_t				_draw_ui_num;
	uint32_t				_total_ui_num;
	int						_width;
	int						_height;
	std::vector<lvr_ui_2d*>	_rects;

	std::vector<lvr_vector2i>	_rect_pos;
	std::vector<lvr_vector4f>	_rect_uv;
	lvr_texture2d*	_texture;

	bool					_has_alpha;

public:
	bool is_visible(){ return _visible; }
	void set_visible(bool a_visible){ _visible = a_visible; }
private:
	bool					_visible;
};

#endif
