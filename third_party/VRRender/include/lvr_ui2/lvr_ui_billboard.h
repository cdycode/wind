#ifndef __lvr_ui_billboard_h__20170720__
#define __lvr_ui_billboard_h__20170720__

#include "lvr_ui_base.h"

class lvr_ui_billboard : public lvr_ui_base
{
public:
	lvr_ui_billboard();
	~lvr_ui_billboard();
public:
	virtual void set_position(const lvr_vector3f& pos);
	virtual void set_orientation(const lvr_quaternionf& a_q);
	virtual void set_orientation(const lvr_vector3f& a_right, const lvr_vector3f& a_up);
	virtual int  test_ray(const lvr_vector3f& a_pos, const lvr_vector3f& a_dir, float & ao_dist);
	virtual uint32_t  get_vertex_num();
	virtual void update_vertexs(ui_vertex* a_vertexs, uint32_t& ao_cur_offset);
	virtual void set_pickable(bool a_pick_state);
	void set_texture(lvr_texture* a_texture);
	void update();
	void draw(const lvr_matrix4f& a_vp_mat);
private:
	void generate_collider();
public:
	lvr_texture*	_texture;
	lvr_render_object*	_ro;
	lvr_vector3f	_position;
	lvr_vector2f	_size;
	lvr_vector3f	_right;
	lvr_vector3f	_up;
	int				_seg_num;
	std::vector<lvr_vector3f>	_points;//for ray test
	bool			_horizontal_align;
	bool			_need_update;
	bool			_visible;
	bool			_pickable;
};

#endif
