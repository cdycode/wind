#ifndef __lvr_ui_texture_manager_h__20160721__
#define __lvr_ui_texture_manager_h__20160721__

#include "lvr_ui_configure.h"
#include "lvr_texture2d.h"
class LVR_UI_API lvr_ui_texture_manager
{
public:
	static lvr_ui_texture_manager* get_ui_tex_mgr();
	static void release_tex_mgr();
private:
	lvr_ui_texture_manager();
	static lvr_ui_texture_manager* s_ui_tex_mgr;
public:
	~lvr_ui_texture_manager();
	const lvr_rect2f& get_rect(const char* a_name);
	lvr_texture*  get_texture();
	bool load_from_file(const char* path,const char* a_config_file);
	void release_res();
	bool insert_texture(const char* a_name,const uint8_t* a_buf, int w, int h);
private:
	lvr_texture*						_texture;
	std::map<std::string, lvr_rect2f>	_ui_texture_map;
};


#endif
