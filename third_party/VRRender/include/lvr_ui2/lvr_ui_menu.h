#ifndef __lvr_ui_menu_20160525_h__
#define __lvr_ui_menu_20160525_h__

#include "lvr_ui_2d.h"
#include "lvr_ui_text.h"

class LVR_UI_API lvr_ui_menu
{
public:
	lvr_ui_menu();
	virtual ~lvr_ui_menu();
public:
	void set_visible(bool a_visible);
	bool get_visible();
	void add_ui_2d(lvr_ui_2d* a_ui);
	void add_text(lvr_ui_text* a_text);
	bool remove_ui_2d(lvr_ui_2d* a_ui);
	bool remove_text(lvr_ui_text* a_text);
	void release_all();
	lvr_ui_base* test_ray(const lvr_vector3f& a_start, const lvr_vector3f& a_dir, double a_cur_time,float & ao_dist);
	int get_update_value();
	void clear_status();
	//may be I should not return this struct back,but now I have not think it over.
	const std::vector<lvr_ui_2d*>& get_ui_2ds();
	const std::vector<lvr_ui_text*>& get_ui_texts();
protected:
	int					_update_state;
	std::vector<lvr_ui_2d*>	_ui_group;
	std::vector<lvr_ui_text*>_ui_texts;
	bool				_visible;
};

#endif
