#ifndef __lvr_ui_progress_bar_20160525_h__
#define __lvr_ui_progress_bar_20160525_h__

#include "lvr_ui_2d.h"

class LVR_UI_API lvr_ui_progress_bar
	: public lvr_ui_2d
{
public:
	lvr_ui_progress_bar();
	~lvr_ui_progress_bar();
public:
	//cur progress is between 0-100;
	int		get_cur_progress();
	void	set_cur_progress(int a_cur_progress);
	int		get_cur_focus_progress();
	virtual uint32_t get_vertex_num();
	void	update_vertexs(ui_vertex* a_vertexs, uint32_t& ao_cur_offset);
	virtual int test_ray(const lvr_vector3f& a_pos, const lvr_vector3f& a_dir, float & ao_dist);
private:
	int		_cur_progress;
	int		_cur_focus_value;
};

#endif
