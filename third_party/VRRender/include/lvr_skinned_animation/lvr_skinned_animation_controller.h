#ifndef __lvr_skinned_animation_controller_h__
#define __lvr_skinned_animation_controller_h__

#include "lvr_skinned_animation_configure.h"
#include <map>
class animation_frame;
class lvr_animation_set;
class lvr_skeleton;
class lvr_skinned_mesh;
//controller will copy the skeleton hierarchy,so change the skeleton here.
//and also,this moudle will return the matrixs outside need.
class skeleton_control_info
{
public:
	skeleton_control_info()
		:_skeleton_id(0)
		,_farther_id(0)
		,_sibling_id(0)
		,_first_child_id(0)
		,_finnal_matrix(NULL)
	{

	}
public:
	uint32_t _skeleton_id;
	uint32_t _farther_id;
	uint32_t _sibling_id;
	uint32_t _first_child_id;
	lvr_matrix4f	_local_matrix;
	lvr_matrix4f*	_finnal_matrix;
public:
	void process_final_matrix();
};
class lvr_skinned_animation_controller
{
public:
	lvr_skinned_animation_controller();
	~lvr_skinned_animation_controller();
public:
	void set_control_mesh(lvr_skinned_mesh* a_sm);
	void set_current_animation_set(lvr_animation_set* a_animation_set);
	void change_animation_set(lvr_animation_set* a_animation_set_,float a_time_use);
	void process_animation(float a_current_time);
	const lvr_matrix4f* get_cur_finnal_matrix();
private:
	void copy_skeleton_hierachy(const lvr_skeleton* a_ske_src);
	void update_final_matrix(skeleton_control_info& a_sci);
private:
	lvr_skinned_mesh*			_skin_mesh;
	skeleton_control_info*		_skeletons;
	lvr_animation_set*			_current_set;
	lvr_animation_set*			_to_anim_set;
	lvr_matrix4f*				_finnal_matrixs;
	uint32_t					_frame_begin_time;
	uint32_t					_skeleton_num;
	bool						_testcase;
};

#endif
