#ifndef __lvr_skinned_animation_instance_h__
#define __lvr_skinned_animation_instance_h__

#include "lvr_skinned_animation_configure.h"

#include "lvr_skinned_animation_skeleton.h"
#include "lvr_skinned_animation_set.h"

class lvr_skinned_mesh;
class lvr_skinned_animation_controller;
class LVR_SKINNED_ANIMATION_API lvr_skinned_animation_instance
{
public:
	lvr_skinned_animation_instance();
	~lvr_skinned_animation_instance();
public:
	void set_pos(const lvr_vector3f& a_pos);
	void set_base_mesh(lvr_skinned_mesh* a_sm);
	void set_animation_collections(lvr_animation_set_collection* a_asc);
	void set_anim_now(char* a_name);
	void set_default_ainm();
	void change_anim(char* a_name,float a_time_use);
	bool init_animation_set(char* a_name,float a_time_now);
	bool change_animation(char* a_name,float a_time_use);
	void set_frame_speed(float a_frame_speed);
	void process_anim(float a_time_now);
	void draw(lvr_program* a_prog,const lvr_matrix4f& a_vp);
public:
	lvr_vector3f						_pos;
	//n to n relationship.maintain them and so the work is done.
	lvr_animation_set_collection*			_asc;
	lvr_skinned_animation_controller*	_skinned_anim_controller;
	lvr_skinned_mesh*					_skinned_mesh;
	uint32_t							_last_time;
	float								_frame_speed;
	char								_cur_anim_name[64];
};

#endif
