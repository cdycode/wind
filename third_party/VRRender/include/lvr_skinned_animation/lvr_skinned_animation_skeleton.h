#ifndef __lvr_skinned_animation_skeleton_h__
#define __lvr_skinned_animation_skeleton_h__
#include "lvr_skinned_animation_configure.h"

class LVR_SKINNED_ANIMATION_API lvr_skeleton
{
public:
	typedef lvr_skeleton* lvr_skeleton_ptr;
public:
	lvr_skeleton();
	~lvr_skeleton();
public:
	void set_skeleton_name(char* a_name);
	void set_father_skeleton(lvr_skeleton* a_skeleton_ptr);
	void set_sibling_skeleton(lvr_skeleton* a_skeleton_ptr);
	void attach_first_child_skeleton(lvr_skeleton* a_skeleton_ptr);
	const char* get_skeleton_name();
	void set_skeleton_id(int id_);
	int	 get_skeleton_id() const;
	lvr_skeleton* get_farther_skeleton()const;
	lvr_skeleton* get_sibling_skeleton()const;
	lvr_skeleton* get_first_child_skeleton()const;
	//currently,the next three function make no use.we use final only now.
	void set_local_matrix(const lvr_matrix4f& a_matrix);
	const lvr_matrix4f& get_local_matrix();
	const lvr_matrix4f& get_inv_local_matrix();
	const lvr_matrix4f& get_final_matrix();
	const lvr_matrix4f& get_inv_final_matrix();
	void      init_matrix_by_locals();
	const lvr_matrix4f& get_bone_to_mesh_mat();
	void set_bone_to_mesh_mat(const lvr_matrix4f& a_bone_to_mesh_mat);
private:
	char				_skeleton_name[64];
	int					_skeleton_id;
	lvr_skeleton*		_farther_skeleton;
	lvr_skeleton*		_sibling_skeleton;
	lvr_skeleton*		_first_child_skeleton;
	int					_child_num;
	lvr_matrix4f		_local_matrix;
	lvr_matrix4f		_finnal_bind_matrix;
	lvr_matrix4f		_inv_local_matrix;
	lvr_matrix4f		_inv_finnal_bind_matrix;
	lvr_matrix4f		_bone_to_mesh_mat;
};

#endif