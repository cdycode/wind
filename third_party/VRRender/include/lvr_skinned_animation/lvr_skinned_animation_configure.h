#ifndef __lvr_skinned_animation_configure_h__
#define __lvr_skinned_animation_configure_h__

#include "lvr_math_lib.h"
// 
// #ifdef LVR_OS_WIN32
// #define lvr_render_IMPORT
// #endif
#include "lvr_render_lib.h"

#ifdef lvr_skinned_animation_EXPORTS
#define LVR_SKINNED_ANIMATION_API LVR_API_EXPORT
#elif defined(lvr_skinned_animation_IMPORT)
#define LVR_SKINNED_ANIMATION_API LVR_API_IMPORT
#else
#define LVR_SKINNED_ANIMATION_API LVR_API_STATIC
#endif

#endif
