#ifndef __lvr_skinned_animation_set_H__
#define __lvr_skinned_animation_set_H__

#include "lvr_skinned_animation_configure.h"
#include <map>

class lvr_single_skeleton_anim;
class LVR_SKINNED_ANIMATION_API lvr_animation_set
{
public:
	lvr_animation_set();
	~lvr_animation_set();
public:
	void init_skeleton_num(int skeleton_num);
public:
	lvr_single_skeleton_anim*	get_single_skeleton_anim(int a_id);
	uint32_t				get_animation_bone_num();
	uint32_t				get_anim_total_time();
	const char*				get_anim_set_name();
public:
	char					_set_name[64];
private:
	lvr_single_skeleton_anim*	_animation_skeletons;
	uint32_t				_animation_skeleton_num;
	uint32_t				_frame_total_time;
};
class LVR_SKINNED_ANIMATION_API lvr_animation_set_collection
{
public:
	lvr_animation_set_collection(){}
	~lvr_animation_set_collection(){}
public:
	lvr_animation_set*  get_anim_set(const char* a_name);
	lvr_animation_set*	get_default_set();
public:
	char	_set_collection_name[64];
	std::map<std::string,lvr_animation_set*> _anim_sets;
};

#endif
