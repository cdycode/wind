#ifndef __lvr_skinned_animation_manager_h__
#define __lvr_skinned_animation_manager_h__

#include "lvr_skinned_animation_configure.h"

#include "lvr_skinned_animation_skeleton.h"
#include "lvr_skinned_animation_set.h"

class lvr_skinned_mesh;
class lvr_skinned_animation_instance;
class LVR_SKINNED_ANIMATION_API lvr_skinned_animation_manager
{
public:
	lvr_skinned_animation_manager();
	~lvr_skinned_animation_manager();
public:
	void set_default_render_prog(lvr_program* a_prog);
	void load_mesh(lvr_skinned_mesh* a_skin_mesh_ptr);
	void load_anim_set_collection(lvr_animation_set_collection* a_anim_set_collection_ptr);
	//to do ...
	void load_mesh_file(const char* a_skinned_mesh_path,const char* a_mesh_name);
	lvr_skinned_animation_instance* create_skinned_anim_instance(const char* a_inst_name,const char* a_mesh_name,const char* a_anim_set_collection_name);
	void delete_animation_instance(char* a_skinned_instance_name);
	void delete_mesh(char* a_mesh_name);

	void change_animation(char* a_skinned_instance_name,char* a_animation_name,float a_change_time);
	void process_animation(float a_time_now);
	void draw_skinned_meshes(const lvr_matrix4f& a_vp);
private:
	std::map<std::string,lvr_skinned_mesh*>	_skinned_meshe_map;
	std::map<std::string,lvr_animation_set_collection*> _animation_set_collection_map;
	std::map<std::string,lvr_skinned_animation_instance*> _skinned_mesh_instance_map;
	lvr_program*		_render_program;
};

#endif
