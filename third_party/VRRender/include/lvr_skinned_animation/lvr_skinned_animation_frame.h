#ifndef __lvr_skinned_animation_frame_h__
#define __lvr_skinned_animation_frame_h__
#include "lvr_skinned_animation_configure.h"

//to do ...
//this is not so good since matrix interpolation happened.
//better to use joint and rotate work to replace this work.

//if the key frame is not fix time.use a different soultion.or just
//change the frame that not is fix time to fix time.

//time in ms,that is 1 = 0.001s.
class LVR_SKINNED_ANIMATION_API lvr_single_skeleton_anim
{
public:
	lvr_single_skeleton_anim();
	~lvr_single_skeleton_anim();
public:
	lvr_matrix4f*	_local_matrixs;// root T,other only R
	int				_skeleton_id;
	uint32_t		_key_frame_num;
	uint32_t		_frame_total_time;
public:
	//interpolation between matrixs by time to get the result matrix.
	lvr_matrix4f	get_accurate_matrix(float a_time);
	//do not interpolation,just choose the nearest frame value.
	const lvr_matrix4f&	 get_matrix(float a_time);
};

#endif
