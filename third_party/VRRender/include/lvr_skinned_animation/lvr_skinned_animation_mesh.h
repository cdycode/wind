#ifndef __lvr_skinned_animation_mesh_h__
#define __lvr_skinned_animation_mesh_h__

#include "lvr_skinned_animation_configure.h"
#include "lvr_skinned_animation_set.h"

//generate the skeleton hierarchy
//each instance should have a copy of this.
class lvr_skeleton;
typedef lvr_matrix4f* matrix4f_ptr;
class LVR_SKINNED_ANIMATION_API lvr_skinned_mesh
{
public:
	lvr_skinned_mesh();
	~lvr_skinned_mesh();
public:
	void set_mesh_name(const char* mesh_name_);
	//to be modified by real user who analysis the structure from file.
	void load_mesh(const char* a_mesh_path);
	bool add_texture(int a_usage,lvr_texture* a_tex);
	lvr_render_object*	get_render_object();
	//the skeleton will be in a group,not a list.
	const lvr_skeleton* get_skeleton_hierarchy();
	//need updated to a more effect one method.used to get binding matrix.
	lvr_skeleton*		get_skeleton_by_id(unsigned int a_id);
	int					get_skeleton_num();
public:
	int				_skeleton_num;
	lvr_skeleton*	_root_skeleton;
	std::map<int,lvr_texture*>	_textures;
	lvr_render_object*	_ro;
	char			_meshname[64];
	std::map<std::string,int> _skeleton_id_map;
};

#endif // !__lvr_skinned_animation_mesh_h__
