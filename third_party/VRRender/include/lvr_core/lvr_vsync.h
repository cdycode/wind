#ifndef __lvr_sync_h__
#define __lvr_sync_h__

#include "lvr_data_types.h"

typedef struct lvr_vsync_state__
{
public:
	int64_t		vsync_count;
	double		vsync_peroid_nano;
	double		vsync_base_nano;
}lvr_vsync_state;

extern double	lvr_get_fractional_vsync(const lvr_vsync_state* a_vsync_state);
extern double	lvr_frame_point_time(const lvr_vsync_state* a_vsync_state,const double a_frame_point);
extern float	lvr_sleep_until_time_point(const double a_target_seconds,const bool a_busy_wait);

#endif