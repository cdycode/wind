#ifndef __lvr_ref_h__
#define __lvr_ref_h__

#include "lvr_core_configure.h"

class LVR_CORE_ITEM lvr_ref
{
public:
	lvr_ref():_ref_count(0){}
	virtual ~lvr_ref();
public:
	virtual void add_ref();
	virtual void release();
public:
	int get_ref_count()const{return _ref_count;}
private:
	volatile int _ref_count;
};

#endif
