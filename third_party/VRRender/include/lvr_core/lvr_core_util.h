#ifndef __lvr_core_util_h__
#define __lvr_core_util_h__

#include "lvr_data_types.h"
#include <string.h>
#include<string>
#include <stdarg.h>

#ifdef LVR_OS_WIN32
inline char* LVR_CDECL lvr_itoa(int val, char *dest, uint32_t destsize, int radix)
{
#if defined(LVR_MSVC_SAFESTRING)
	_itoa_s(val, dest, destsize, radix);
	return dest;
#else
	LVR_UNUSED(destsize);
	return itoa(val, dest, radix);
#endif
}
#else
inline char* lvr_itoa(int val, char* dest, uint32_t len, int radix)
{
	if (val == 0)
	{
		if (len > 1)
		{
			dest[0] = '0';
			dest[1] = '\0';  
		}
		return dest;
	}

	int cur = val;
	unsigned int i    = 0; 
	unsigned int sign = 0;

	if (val < 0)
	{
		val = -val;
		sign = 1;
	}

	while ((val != 0) && (i < (len - 1 - sign)))        
	{
		cur    = val % radix;
		val   /= radix;

		if (radix == 16)
		{
			switch(cur)
			{
			case 10:
				dest[i] = 'a';
				break;
			case 11:
				dest[i] = 'b';
				break;
			case 12:
				dest[i] = 'c';
				break;
			case 13:
				dest[i] = 'd';
				break;
			case 14:
				dest[i] = 'e';
				break;
			case 15:
				dest[i] = 'f';
				break;
			default:
				dest[i] = (char)('0' + cur);
				break;
			}
		} 
		else
		{
			dest[i] = (char)('0' + cur);
		}
		++i;
	}

	if (sign)
	{
		dest[i++] = '-';
	}

	for (unsigned int j = 0; j < i / 2; ++j)
	{
		char tmp        = dest[j];
		dest[j]         = dest[i - 1 - j];
		dest[i - 1 - j] = tmp;
	}
	dest[i] = '\0';

	return dest;
}

#endif

inline uint32_t LVR_CDECL lvr_strlen(const char* str)
{
	return strlen(str);
}
inline int LVR_CDECL lvr_strcmp(const char* dest, const char* src)
{
	return strcmp(dest, src);
}

inline const char* LVR_CDECL lvr_strchr(const char* str, char c)
{
	return strchr(str, c);
}

inline char* LVR_CDECL lvr_strchr(char* str, char c)
{
	return strchr(str, c);
}

inline const char* lvr_strrchr(const char* str, char c)
{
	uint32_t len = lvr_strlen(str);
	for (uint32_t i=len; i>0; i--)     
		if (str[i]==c) 
			return str+i;
	return 0;
}
inline char* LVR_CDECL lvr_strrchr(char* str, char c)
{
	uint32_t len = lvr_strlen(str);
	for (uint32_t i=len; i>0; i--)     
		if (str[i]==c) 
			return str+i;
	return 0;
}

inline int lvr_align_4(int a_v)
{
	if (0 == a_v % 4 )
	{
		return a_v;
	}
	return (a_v >> 2) << 2;
}

inline int LVR_CDECL lvr_strncmp(const char* ws1, const char* ws2, uint32_t size)
{
	return strncmp(ws1, ws2, size);
}

inline int LVR_CDECL lvr_tolower(int c)
{
	return (c >= 'A' && c <= 'Z') ? c - 'A' + 'a' : c;
}

inline int LVR_CDECL lvr_toupper(int c)
{
	return (c >= 'a' && c <= 'z') ? c - 'a' + 'A' : c;
}

inline char* LVR_CDECL lvr_strcpy_s(char* dest, int32_t destsize, const char* src)
{
#if defined(LVR_MSVC_SAFESTRING)
	//strcpy_s(dest, destsize, src);
	strncpy(dest, src, destsize);
	return dest;
#elif defined(LVR_OS_ANDROID)
	strlcpy( dest, src, destsize );
	return dest;
#else
	LVR_UNUSED(destsize);
	return strcpy(dest, src);
#endif
}

inline char * LVR_CDECL lvr_strcat_s(char* dest, uint32_t destsize, const char* src)
{
#if defined(LVR_MSVC_SAFESTRING)
	strcat_s(dest, destsize, src);
	return dest;
#elif defined(LVR_OS_ANDROID)
	strlcat( dest, src, destsize );
	return dest;
#else
	LVR_UNUSED(destsize);
	return strcat(dest, src);
#endif
}

inline int LVR_CDECL lvr_snprintf(char *dest, int32_t destsize, const char* format, ...)
{
	if ( destsize <= 0 || dest == NULL )
	{
		LVR_ASSERT( destsize > 0 );
		return -1;
	}
	va_list argList;
	va_start(argList,format);
	int ret;
#if defined(LVR_CC_MSVC)
#if defined(LVR_MSVC_SAFESTRING)
	ret = _vsnprintf_s(dest, destsize, _TRUNCATE, format, argList);
#else
	// FIXME: this is a security issue on Windows platforms that don't have _vsnprintf_s
	LVR_UNUSED(destsize);
	ret = _vsnprintf(dest, destsize - 1, format, argList); // -1 for space for the null character
	dest[destsize-1] = 0;	// may leave trash in the destination...
#endif
#else
	ret = vsnprintf( dest, destsize, format, argList );
	// In the event of the output string being greater than the buffer size, vsnprintf should 
	// return the size of the string before truncation. In that case we zero-terminate the
	// string to ensure that the result is the same as _vsnprintf_s would return for the
	// MSVC compiler. vsnprintf is supposed to zero-terminate in this case, but to be sure
	// we zero-terminate it ourselves. 
	if ( ret >= (int)destsize )
	{
		dest[destsize - 1] = '\0';
	}
#endif
	// If an error occurs, vsnprintf should return -1, in which case we set zero byte to null character.
	LVR_ASSERT( ret >= 0 );	// ensure the format string is not malformed
	if ( ret < 0 )
	{
		dest[0] ='\0';
	}
	va_end(argList);
	return ret;
}

//-----------------------------------------------------------------------------------
extern const uint8_t UpperBitTable[256];
extern const uint8_t LowerBitTable[256];


template <typename T> LVR_FORCE_INLINE void Swap(T &a, T &b) 
{  T temp(a); a = b; b = temp; }

//-----------------------------------------------------------------------------------
inline uint8_t UpperBit(uint32_t val)
{
#ifndef LVR_64BIT_POINTERS

	if (val & 0xFFFF0000)
	{
		return (val & 0xFF000000) ? 
			UpperBitTable[(val >> 24)       ] + 24: 
		UpperBitTable[(val >> 16) & 0xFF] + 16;
	}
	return (val & 0xFF00) ?
		UpperBitTable[(val >> 8) & 0xFF] + 8:
	UpperBitTable[(val     ) & 0xFF];

#else

	if (val & 0xFFFFFFFF00000000)
	{
		if (val & 0xFFFF000000000000)
		{
			return (val & 0xFF00000000000000) ?
				UpperBitTable[(val >> 56)       ] + 56: 
			UpperBitTable[(val >> 48) & 0xFF] + 48;
		}
		return (val & 0xFF0000000000) ?
			UpperBitTable[(val >> 40) & 0xFF] + 40:
		UpperBitTable[(val >> 32) & 0xFF] + 32;
	}
	else
	{
		if (val & 0xFFFF0000)
		{
			return (val & 0xFF000000) ? 
				UpperBitTable[(val >> 24)       ] + 24: 
			UpperBitTable[(val >> 16) & 0xFF] + 16;
		}
		return (val & 0xFF00) ?
			UpperBitTable[(val >> 8) & 0xFF] + 8:
		UpperBitTable[(val     ) & 0xFF];
	}

#endif
}

//-----------------------------------------------------------------------------------
inline uint8_t LowerBit(uint32_t val)
{
#ifndef LVR_64BIT_POINTERS

	if (val & 0xFFFF)
	{
		return (val & 0xFF) ?
			LowerBitTable[ val & 0xFF]:
		LowerBitTable[(val >> 8) & 0xFF] + 8;
	}
	return (val & 0xFF0000) ?
		LowerBitTable[(val >> 16) & 0xFF] + 16:
	LowerBitTable[(val >> 24) & 0xFF] + 24;

#else

	if (val & 0xFFFFFFFF)
	{
		if (val & 0xFFFF)
		{
			return (val & 0xFF) ?
				LowerBitTable[ val & 0xFF]:
			LowerBitTable[(val >> 8) & 0xFF] + 8;
		}
		return (val & 0xFF0000) ?
			LowerBitTable[(val >> 16) & 0xFF] + 16:
		LowerBitTable[(val >> 24) & 0xFF] + 24;
	}
	else
	{
		if (val & 0xFFFF00000000)
		{
			return (val & 0xFF00000000) ?
				LowerBitTable[(val >> 32) & 0xFF] + 32:
			LowerBitTable[(val >> 40) & 0xFF] + 40;
		}
		return (val & 0xFF000000000000) ?
			LowerBitTable[(val >> 48) & 0xFF] + 48:
		LowerBitTable[(val >> 56) & 0xFF] + 56;
	}

#endif
}



// ******* Special (optimized) memory routines
// Note: null (bad) pointer is not tested
class lvr_mem_util
{
public:

	// Memory compare
	static int      cmp  (const void* p1, const void* p2, uint32_t byteCount)      { return memcmp(p1, p2, byteCount); }
	static int      cmp16(const void* p1, const void* p2, uint32_t int16Count);
	static int      cmp32(const void* p1, const void* p2, uint32_t int32Count);
	static int      cmp64(const void* p1, const void* p2, uint32_t int64Count); 
};

// ** Inline Implementation

inline int lvr_mem_util::cmp16(const void* p1, const void* p2, uint32_t int16Count)
{
	int16_t*  pa  = (int16_t*)p1; 
	int16_t*  pb  = (int16_t*)p2;
	unsigned ic  = 0;
	if (int16Count == 0)
		return 0;
	while (pa[ic] == pb[ic])
		if (++ic==int16Count)
			return 0;
	return pa[ic] > pb[ic] ? 1 : -1;
}
inline int lvr_mem_util::cmp32(const void* p1, const void* p2, uint32_t int32Count)
{
	int32_t*  pa  = (int32_t*)p1;
	int32_t*  pb  = (int32_t*)p2;
	unsigned ic  = 0;
	if (int32Count == 0)
		return 0;
	while (pa[ic] == pb[ic])
		if (++ic==int32Count)
			return 0;
	return pa[ic] > pb[ic] ? 1 : -1;
}
inline int lvr_mem_util::cmp64(const void* p1, const void* p2, uint32_t int64Count)
{
	int64_t*  pa  = (int64_t*)p1;
	int64_t*  pb  = (int64_t*)p2;
	unsigned ic  = 0;
	if (int64Count == 0)
		return 0;
	while (pa[ic] == pb[ic])
		if (++ic==int64Count)
			return 0;
	return pa[ic] > pb[ic] ? 1 : -1;
}

// ** End Inline Implementation


//-----------------------------------------------------------------------------------
// ******* Byte Order Conversions
namespace lvr_byte_util {

	// *** Swap Byte Order
	template< typename _type_ >
	inline _type_ SwapOrder( const _type_ & value )
	{
		_type_ bytes = value;
		char * b = (char *)& bytes;
		if ( sizeof( _type_ ) == 1 )
		{
		}
		else if ( sizeof( _type_ ) == 2 )
		{
			Swap( b[0], b[1] );
		}
		else if ( sizeof( _type_ ) == 4 )
		{
			Swap( b[0], b[3] );
			Swap( b[1], b[2] );
		}
		else if ( sizeof( _type_ ) == 8 )
		{
			Swap( b[0], b[7] );
			Swap( b[1], b[6] );
			Swap( b[2], b[5] );
			Swap( b[3], b[4] );
		}
		else
		{
			LVR_ASSERT( false );
		}
		return bytes;
	}

	// *** Byte-order conversion

#if (LVR_BYTE_ORDER == LVR_LITTLE_ENDIAN)
	// Little Endian to System (LE)
	template< typename _type_ >
	inline _type_ LEToSystem( _type_ v ) { return v; }

	// Big Endian to System (LE)
	template< typename _type_ >
	inline _type_ BEToSystem( _type_ v ) { return SwapOrder( v ); }

	// System (LE) to Little Endian
	template< typename _type_ >
	inline _type_ SystemToLE( _type_ v ) { return v; }

	// System (LE) to Big Endian
	template< typename _type_ >
	inline _type_ SystemToBE( _type_ v ) { return SwapOrder( v ); }

//#elif (LVR_BYTE_ORDER == LVR_BIG_ENDIAN)
#else
	// Little Endian to System (BE)
	template< typename _type_ >
	inline _type_ LEToSystem( _type_ v ) { return SwapOrder( v ); }

	// Big Endian to System (BE)
	template< typename _type_ >
	inline _type_ BEToSystem( _type_ v ) { return v; }

	// System (BE) to Little Endian
	template< typename _type_ >
	inline _type_ SystemToLE( _type_ v ) { return SwapOrder( v ); }

	// System (BE) to Big Endian
	template< typename _type_ >
	inline _type_ SystemToBE( _type_ v ) { return v; }

//#else
//#error "LVR_BYTE_ORDER must be defined to LVR_LITTLE_ENDIAN or LVR_BIG_ENDIAN"
#endif
}

	//wchar_t转成UTF-8 
	inline int lvr_FW2UTF8Convert( const wchar_t* a_szSrc, int a_nSrcSize, char* a_szDest, int a_nDestSize )  
	{  
#ifdef LVR_OS_WIN32  
		return WideCharToMultiByte( CP_UTF8, 0, a_szSrc, -1, a_szDest, a_nDestSize, NULL, NULL );  
#elif defined(LVR_OS_LINUX)
		size_t result;  
		iconv_t env;  
		env = iconv_open("UTF-8","WCHAR_T");  
		if (env==(iconv_t)-1)  
		{  
			printf("iconv_open WCHAR_T->UTF8 error%s %d/n",strerror(errno),errno) ;  
			return -1;  
		}  
		result = iconv(env,(char**)&a_szSrc,(size_t*)&a_nSrcSize,(char**)&a_szDest,(size_t*)&a_nDestSize);  
		if (result==(size_t)-1)  
		{  
			printf("iconv WCHAR_T->UTF8 error %d/n",errno) ;  
			return -1;  
		}  
		iconv_close(env);  
		return (int)result;  
#else
		return 0;
#endif  
	} 

	//UTF-8转成wchar_t  
	inline int lvr_FUTF82WConvert( const char* a_szSrc, wchar_t* a_szDest, int a_nDestSize )  
	{  
#ifdef LVR_OS_WIN32  
		return MultiByteToWideChar( CP_UTF8, 0, a_szSrc, -1, a_szDest, a_nDestSize );  
#elif defined(LVR_OS_LINUX)
		size_t result;  
		iconv_t env;  
		int size = strlen(a_szSrc)+1 ;  
		env = iconv_open("WCHAR_T","UTF-8");  
		if (env==(iconv_t)-1)  
		{  
			printf("iconv_open UTF8->WCHAR_T error %d/n",errno) ;  
			return -1;  
		}  
		result = iconv(env,(char**)&a_szSrc,(size_t*)&size,(char**)&a_szDest,(size_t*)&a_nDestSize);  
		if (result==(size_t)-1)  
		{  
			printf("iconv UTF8->WCHAR_T error %d/n",errno) ;  
			return -1;  
		}  
		iconv_close(env);  
		return (int)result; 
#else
		return 0;
#endif  
	}  

	//wchar_t转成utf16  
	inline int FW2UConvert( const wchar_t* a_szSrc, int  a_nSize,char* a_szDest, int a_nDestSize )  
	{  
#ifdef LVR_OS_WIN32  
		memcpy_s((wchar_t*)a_szDest,a_nDestSize,a_szSrc,a_nSize);  
		return a_nSize ;  
#elif defined(LVR_OS_LINUX)
		size_t result;  
		iconv_t env;  
		env = iconv_open("UCS-2-INTERNAL","UCS-4-INTERNAL");  
		if (env==(iconv_t)-1)  
		{  
			printf("iconv_open WCHAR_T->UTF16 error%s %d/n", strerror(errno),errno);  
			return -1;  
		}  
		result = iconv(env,(char**)&a_szSrc,(size_t*)&a_nSize,(char**)&a_szDest,(size_t*)&a_nDestSize);  
		if (result==(size_t)-1)  
		{  
			printf("iconv WCHAR_T->UTF16 error %s %d/n", strerror(errno), errno);  
			return -1;  
		}  
		iconv_close(env);  
		return (int)result;  
#else
		return 0;
#endif  
	}  

	//utf16转成wchar_t  
	inline int FU2WConvert( const  char* a_szSrc, int a_nSize, wchar_t* a_szDest, int a_nDestSize )  
	{  
#ifdef LVR_OS_WIN32  
		memcpy_s(a_szDest,a_nDestSize,(const wchar_t*)a_szSrc,a_nSize);  
		return a_nSize ;  
#elif defined(LVR_OS_LINUX)
		size_t result;  
		iconv_t env;  
		env = iconv_open("UCS-4-INTERNAL","UCS-2-INTERNAL");  
		if (env==(iconv_t)-1)  
		{  
			printf("iconv_open error %d/n",errno) ;  
			return -1;  
		}  
		result = iconv(env,(char**)&a_szSrc,(size_t*)&a_nSize,(char**)&a_szDest,(size_t*)&a_nDestSize);  
		if (result==(size_t)-1)  
		{  
			printf("UTF16 -> WCHAR_T conv error %d/n",errno) ;  
			return -1;  
		}  
		iconv_close(env);  
		return (int)result; 
#else
		return 0;
#endif  
	}  

#endif
