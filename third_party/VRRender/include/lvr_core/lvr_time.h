#ifndef __lvr_time_h__
#define __lvr_time_h__

#include "lvr_data_types.h"

class LVR_CORE_ITEM lvr_time
{
public:
	enum{
		e_ms_per_second = 1000,
		e_naons_per_second = e_ms_per_second*1000*1000
	};
	static double get_seconds();
	static uint64_t get_tick_count();
};

#endif
