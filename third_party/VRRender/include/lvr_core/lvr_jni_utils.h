#ifndef LVR_JniUtils_h
#define LVR_JniUtils_h

#include "lvr_core_configure.h"
#include "lvr_log.h"
#ifdef LVR_OS_ANDROID
#include <unistd.h>
#include <jni.h>

//==============================================================
// JavaObject
//
// Releases a java object reference on destruction
class JavaObject
{
public:
	JavaObject( JNIEnv * jni_, jobject const JObject_ ) :
		jni( jni_ ),
		JObject( JObject_ )
	{
		LVR_ASSERT( jni != NULL );
	}
	~JavaObject()
	{
		LVR_ASSERT( jni != NULL && JObject != NULL );
		jni->DeleteLocalRef( JObject );
		if ( jni->ExceptionOccurred() )
		{
			DROIDLOG( "OvrJNI", "JNI exception occured calling DeleteLocalRef!" );
		}
		jni = NULL;
		JObject = NULL;
	}

	jobject			GetJObject() const { return JObject; }
	JNIEnv *		GetJNI() const { return jni; }

protected:
	void			SetJObject( jobject const & obj ) { JObject = obj; }

private:
	JNIEnv *		jni;
	jobject			JObject;
};

//==============================================================
// JavaString
//
// Creates a java string on construction and releases it on destruction.
class JavaString : public JavaObject
{
public:
	JavaString( JNIEnv * jni_, char const * str ) :
		JavaObject( jni_, NULL )
	{
		SetJObject( GetJNI()->NewStringUTF( str ) );
		if ( GetJNI()->ExceptionOccurred() )
		{
			//DROIDLOG( "OvrJNI", "JNI exception occured calling NewStringUTF!" );
		}
	}

	JavaString( JNIEnv * jni_, jstring JString_ ) :
		JavaObject( jni_, JString_ )
	{
		LVR_ASSERT( JString_ != NULL );
	}

	jstring			GetJString() const { return static_cast< jstring >( GetJObject() ); }
};

//==============================================================
// JavaUTFChars
//
// Gets a java string object as a buffer of UTF characters and
// releases the buffer on destruction.
// Use this only if you need to store a string reference and access
// the string as a char buffer of UTF8 chars.  If you only need
// to store and release a reference to a string, use JavaString.
class JavaUTFChars : public JavaString
{
public:
	JavaUTFChars( JNIEnv * jni_, jstring const JString_ ) :
		JavaString( jni_, JString_ ),
		UTFString( NULL )
	{
		UTFString = GetJNI()->GetStringUTFChars( GetJString(), NULL );
		if ( GetJNI()->ExceptionOccurred() )
		{
			//DROIDLOG( "OvrJNI", "JNI exception occured calling GetStringUTFChars!" );
		}
	}

	~JavaUTFChars()
	{
		LVR_ASSERT( UTFString != NULL );
		GetJNI()->ReleaseStringUTFChars( GetJString(), UTFString );
		if ( GetJNI()->ExceptionOccurred() )
		{
			//DROIDLOG( "OvrJNI", "JNI exception occured calling ReleaseStringUTFChars!" );
		}
	}

	operator char const * () const { return UTFString; }

private:
	char const *	UTFString;
};

jclass		lvr_get_global_class_reference( JNIEnv * jni, const char * className );
jmethodID	lvr_get_static_method_id( JNIEnv * jni, jclass jniclass, const char * name, const char * signature );
jmethodID	lvr_get_method_id( JNIEnv * jni, jclass jniclass, const char * name, const char * signature );

jclass		lvr_get_vr_activity_class();
void		lvr_set_vr_activity_class( jclass clazz );

void		lvr_load_dev_config( bool const forceReload );

// Tries to read the Home package name from the dev config. Defaults to "com.oculus.home".
const char * lvr_get_home_package_name( char * packageName, int const maxLen );

// Get the current package name, for instance "com.oculus.home".
const char * lvr_get_current_package_name( JNIEnv * jni, jobject activityObject, char * packageName, int const maxLen );

// Get the current activity class name, for instance "com.oculusvr.vrlib.PlatformActivity".
const char * lvr_get_current_activity_name( JNIEnv * jni, jobject activityObject, char * className, int const maxLen );

// For instance packageName = "com.oculus.home".
bool lvr_is_current_package( JNIEnv * jni, jobject activityObject, const char * packageName );

// For instance className = "com.oculusvr.vrlib.PlatformActivity".
bool lvr_is_current_activity( JNIEnv * jni, jobject activityObject, const char * className );

// Returns true if this is the Home package.
bool lvr_is_home_package( JNIEnv * jni, jobject activityObject );

bool lvr_query_android_info( JNIEnv *env, jobject activity, jclass vrActivityClass,
	float & screenWidthMeters, float & screenHeightMeters);

#endif

#endif	// LVR_JniUtils_h
