/*
 * lvr_singleton.h
 *
 *  Created on: 2015-11-17
 *      Author: Administrator
 */

#ifndef __LING_LAUNCHER_LVR_SINGLETON_H_
#define __LING_LAUNCHER_LVR_SINGLETON_H_

#include <stdio.h>

template<class SINGLETON_TYPE>
class lvr_singleton
{
public:
	static SINGLETON_TYPE* get_ins(void)
	{
		if(NULL == _sg_singleton_ptr)
		{
			_sg_singleton_ptr = new SINGLETON_TYPE();
		}
		return _sg_singleton_ptr;
	}
	static void release_ins(void)
	{
		if(NULL != _sg_singleton_ptr)
		{
			delete _sg_singleton_ptr;
			_sg_singleton_ptr = NULL;
		}
	}
	static void del_ins(void)
	{
		if(NULL != _sg_singleton_ptr)
		{
			delete _sg_singleton_ptr;
			_sg_singleton_ptr = NULL;
		}
	}

protected:
	lvr_singleton(void){}
	virtual ~lvr_singleton(void){}

private:
	lvr_singleton(const lvr_singleton&);
	lvr_singleton& operator = (const lvr_singleton&);

private:
	static SINGLETON_TYPE*		_sg_singleton_ptr;
};

template<class SINGLETON_TYPE>
SINGLETON_TYPE*	lvr_singleton<SINGLETON_TYPE>::_sg_singleton_ptr = NULL;


#endif /* __LING_LAUNCHER_LVR_SINGLETON_H_ */
