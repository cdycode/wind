#ifndef __lvr_util_utf8_h__
#define __lvr_util_utf8_h__

#include "lvr_data_types.h"

// Determines the length of UTF8 string in characters.
// If source length is specified (in bytes), null 0 character is counted properly.
int32_t LVR_CORE_ITEM lvr_get_length(const char* a_utf8_str,int32_t a_len = -1);

// Gets a decoded UTF8 character at index; you can access up to the index returned
// by GetLength. 0 will be returned for out of bounds access.
uint32_t   LVR_CORE_ITEM lvr_get_char_at(int32_t index, const char* putf8str, int32_t length = -1);

// Converts UTF8 character index into byte offset.
// -1 is returned if index was out of bounds.
int32_t    LVR_CORE_ITEM lvr_get_byte_index(int32_t index, const char* putf8str, int32_t length = -1);

// *** 16-bit Unicode string Encoding/Decoding routines.

// Determines the number of bytes necessary to encode a string.
// Does not count the terminating 0 (null) character.
int32_t  LVR_CORE_ITEM lvr_get_encode_string_len(const int16_t* a_chars,int32_t a_len = -1);

// Encodes a unicode (UCS-2 only) string into a buffer. The size of buffer must be at
// least GetEncodeStringSize() + 1.
void     LVR_CORE_ITEM lvr_encode_string(char *ao_buff, const int16_t* a_chars, int32_t length = -1);

// Decode UTF8 into a wchar_t buffer. Must have GetLength()+1 characters available.
// Characters over 0xFFFF are replaced with 0xFFFD.
// Returns the length of resulting string (number of characters)
uint32_t LVR_CORE_ITEM lvr_decode_string(int16_t *pbuff, const char* putf8str, int32_t bytesLen = -1);

// *** Individual character Encoding/Decoding.

// Determined the number of bytes necessary to encode a UCS character.
int32_t LVR_CORE_ITEM lvr_get_encode_char_size(uint32_t a_ucs_char);

// Encodes the given UCS character into the given UTF-8 buffer.
// Writes the data starting at buffer[offset], and 
// increments offset by the number of bytes written.
// May write up to 6 bytes, so make sure there's room in the buffer
void LVR_CORE_ITEM lvr_encode_char(char* ao_buf,int32_t* a_offset,uint32_t a_ucs_char);


// Return the next Unicode character in the UTF-8 encoded buffer.
// Invalid UTF-8 sequences produce a U+FFFD character as output.
// Advances *utf8_buffer past the character returned. Pointer advance
// occurs even if the terminating 0 character is hit, since that allows
// strings with middle '\0' characters to be supported.
uint32_t LVR_CORE_ITEM lvr_decode_next_char_advance0(const char** putf8Buffer);

// Safer version of DecodeNextChar, which doesn't advance pointer if
// null character is hit.
inline uint32_t lvr_decode_next_char(const char** putf8Buffer)
{
	uint32_t ch = lvr_decode_next_char_advance0(putf8Buffer);
	if (ch == 0)
		(*putf8Buffer)--;
	return ch;
}

#endif
