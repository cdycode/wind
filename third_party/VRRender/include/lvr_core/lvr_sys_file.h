#ifndef __LVR_SysFile_h
#define __LVR_SysFile_h

#include "lvr_file.h"

// ***** Declared classes
class   lvr_sys_file;

//-----------------------------------------------------------------------------------
// *** File Statistics

// This class contents are similar to _stat, providing
// creation, modify and other information about the file.
struct FileStat
{
    // No change or create time because they are not available on most systems
    int64_t  ModifyTime;
    int64_t  AccessTime;
    int64_t  FileSize;

    bool operator== (const FileStat& stat) const
    {
        return ( (ModifyTime == stat.ModifyTime) &&
                 (AccessTime == stat.AccessTime) &&
                 (FileSize == stat.FileSize) );
    }
};

//-----------------------------------------------------------------------------------
// *** System File

// System file is created to access objects on file system directly
// This file can refer directly to path.
// System file can be open & closed several times; however, such use is not recommended
// This class is realy a wrapper around an implementation of File interface for a 
// particular platform.

class lvr_sys_file : public lvr_delegated_file
{
protected:
  lvr_sys_file(const lvr_sys_file &source) : lvr_delegated_file () { LVR_UNUSED(source); }
public:

    // ** Constructor
    lvr_sys_file();
    // Opens a file
    lvr_sys_file(const std::string& path, int flags = lvr_file::open_read | lvr_file::open_buffered, int mode = open_read_write); 

    // ** Open & management 
    bool  Open(const std::string& path, int flags = lvr_file::open_read | lvr_file::open_buffered, int mode = lvr_file::open_read_write);
        
    LVR_FORCE_INLINE bool  Create(const std::string& path, int mode = lvr_file::open_read_write)
    { return Open(path, lvr_file::open_read_write|lvr_file::open_create, mode); }

    // Helper function: obtain file statistics information. In LVR, this is used to detect file changes.
    // Return 0 if function failed, most likely because the file doesn't exist.
    static bool LVR_CDECL GetFileStat(FileStat* pfileStats, const std::string& path);
    
    // ** Overrides
    // Overridden to provide re-open support
    virtual int   GetErrorCode();

    virtual bool  IsValid();

    virtual bool  Close();    
};

#endif
