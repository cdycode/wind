#ifndef __lvr_data_types_h__
#define __lvr_data_types_h__

#include "lvr_core_configure.h"

// Byte order constants, LVR_BYTE_ORDER is defined to be one of these.
#define LVR_LITTLE_ENDIAN       1
#define LVR_BIG_ENDIAN          2


// Force inline substitute - goes before function declaration
#if defined(LVR_CC_MSVC)
#  define LVR_FORCE_INLINE  __forceinline
#elif defined(LVR_CC_GNU)
#  define LVR_FORCE_INLINE  inline __attribute__((always_inline))
#else
#  define LVR_FORCE_INLINE  inline
#endif  // LVR_CC_MSVC

#ifdef LVR_OS_WIN32
#include <Windows.h>
//#include <stdint.h>
#define LVR_BYTE_ORDER  LVR_LITTLE_ENDIAN

#ifdef __cplusplus_cli
#  define LVR_FASTCALL      __stdcall
#else
#  define LVR_FASTCALL      __fastcall
#endif

#define LVR_STDCALL         __stdcall
#define LVR_CDECL           __cdecl

typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef unsigned char byte;
typedef short int16_t;
typedef unsigned short uint16_t;
typedef int  int32_t;
typedef unsigned int uint32_t;
typedef long long int64_t;
typedef unsigned long long uint64_t;

#elif defined LVR_OS_ANDROID
#include <sys/types.h>
//need more work to make sure of this.
#if (defined(__ARMEB__) || defined(LVR_CPU_PPC) || defined(LVR_CPU_PPC64))
#  define LVR_BYTE_ORDER    LVR_BIG_ENDIAN
#else
#  define LVR_BYTE_ORDER  LVR_LITTLE_ENDIAN
#endif

#define LVR_FASTCALL
#define LVR_STDCALL
#define LVR_CDECL

#else

#define LVR_FASTCALL
#define LVR_STDCALL
#define LVR_CDECL

typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef unsigned char byte;
typedef short int16_t;
typedef unsigned short uint16_t;
typedef int  int32_t;
typedef unsigned int uint32_t;
typedef long long int64_t;

#endif

static const float SmallestNonDenormal	= float( 1.1754943508222875e-038f );	// ( 1U << 23 )
static const float HugeNumber			= float( 1.8446742974197924e+019f );	// ( ( ( 127U * 3 / 2 ) << 23 ) | ( ( 1 << 23 ) - 1 ) )
static const float MinPositiveValue =  1.1754943508222875e-38f;
static const float MaxValue	= 3.4028234663852886e+38f;


struct lvr_image_info
{
	char	name[256];
	int		format;
	uint32_t	gl_format;
	uint32_t	gl_internal_format;
	int		width;
	int		height;
	const uint8_t* data;
	int32_t data_size;
	int		mipcount;
	uint32_t  data_type;
	bool	use_srgb;
	bool	image_size_stored;
	bool	is_cube_tex;
};

#endif