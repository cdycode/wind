#ifndef __lvr_path_util_h__
#define __lvr_path_util_h__

#include "lvr_data_types.h"
#include <vector>
#include <string>
#ifdef LVR_OS_ANDROID
#include <jni.h>
#endif

#define R_OK  4  /* Read */
#define W_OK  2  /* Write */
#define X_OK  1  /* Execute */
#define F_OK  0  /* Existence */

enum lvr_storage_type
{
	e_internal_storage,
	e_primary_external_storage,
	e_secondary_external_storage,
	e_storage_type_num
};
enum lvr_folder_type
{
	e_folder_type_root,
	e_folder_type_files,
	e_folder_type_cache,
	e_folder_type_count
};

class lvr_storage_paths
{
public:
#ifdef LVR_OS_ANDROID
	lvr_storage_paths(JNIEnv* jni,jobject activityObj);
#elif defined LVR_OS_WIN32
	lvr_storage_paths();
#else
	lvr_storage_paths();
#endif
	void		PushBackSearchPathIfValid( lvr_storage_type toStorage, lvr_folder_type toFolder, const char * subfolder, std::vector<std::string> & searchPaths ) const;
	void		PushBackSearchPathIfValidPermission( lvr_storage_type toStorage, lvr_folder_type toFolder, const char * subfolder, int16_t permission, std::vector<std::string> & searchPaths ) const;
	bool		GetPathIfValidPermission( lvr_storage_type toStorage, lvr_folder_type toFolder, const char * subfolder, int16_t permission, std::string & outPath ) const;
public:
	//temp exist,will omit in the near future.
	std::vector<std::string> _search_paths;
	std::string		_storage_folder_paths[e_storage_type_num][e_folder_type_count];
};

std::string  lvr_get_full_path(const std::vector<std::string>& a_search_paths,const std::string a_relative_path);
bool		 lvr_get_full_path(const std::vector<std::string>& a_search_paths,const char* a_relative_path,char* ao_path,const int32_t a_max_len);
bool		 lvr_get_full_path(const std::vector<std::string>& a_search_paths,const char* a_relative_path,std::string& ao_path,const int32_t a_max_len);
bool		 lvr_to_relative_path(const std::vector<std::string>& a_search_paths,const char* a_full_path,char* ao_path ,const int32_t a_max_len);
bool		 lvr_to_relative_path(const std::vector<std::string>& a_search_paths,const char* a_full_path,std::string& ao_path);
void		 lvr_scan_files_with_extension(const char* a_dir, const char* a_ext, std::vector<std::string>& a_records);

#endif
