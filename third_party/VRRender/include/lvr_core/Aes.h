#ifndef AES_H_INCLUDED
#define AES_H_INCLUDED

#ifndef uint8
#define uint8  unsigned char
#endif

#ifndef uint32
#define uint32 unsigned long int
#endif

#ifdef __cplusplus
extern "C"{
#endif


typedef struct
{
    uint32 erk[64];     /* encryption round keys */
    uint32 drk[64];     /* decryption round keys */
    int nr;             /* number of rounds */
}
aes_context;

///key长度:bits  (16:128   24:192  32:256)
extern void Encrypt( const unsigned char* src, int len, const unsigned char* key, unsigned char* out, int* enLen, int bits );
extern void Decrypt(const unsigned char* src, int len, const unsigned char* key, unsigned char* out, int* deLen, int bits);
extern void EncryptWithPKCS5Padding(const unsigned char* src, int len, const unsigned char* key, unsigned char* out, int* enLen, int bits);
extern void DecryptWithPKCS5Padding(const unsigned char* src, int len, const unsigned char* key, unsigned char* out, int* deLen, int bits);

extern int aes_set_key(aes_context* ctx, uint8* key, int nbits);
extern void aes_encrypt(aes_context* ctx, uint8 input[16], uint8 output[16]);
extern void aes_decrypt(aes_context* ctx, uint8 input[16], uint8 output[16]);


#ifdef __cplusplus
}
#endif

#endif // AES_H_INCLUDED
