#ifndef LVR_BinaryFile_h
#define LVR_BinaryFile_h

#include "lvr_data_types.h"
#include <vector>
#include <memory>
#include <string.h>
/*
	This is a simple helper class to read binary data next to a JSON file.
*/

class lvr_binary_reader
{
public:
	lvr_binary_reader( const uint8_t * binData, const int binSize ) :
		Data( binData ),
		Size( binSize ),
		Offset( 0 ),
		Allocated( false ) {}
	~lvr_binary_reader();

	lvr_binary_reader( const char * path, const char ** perror );

// 	template<type T>
// 	bool ReadBasicType(T* ao_v)
// 	{
// 		const int bytes = sizeof(T);
// 		if (Data == NULL || bytes > Size - Offset)
// 		{
// 			return false;
// 		}
// 		*ao_v = *(T *)(Data + Offset);
// 		Offset += bytes;
// 		return true;
// 	}

	uint32_t ReadUInt32() const
	{
		const int bytes = sizeof(unsigned int);
		if (Data == NULL || bytes > Size - Offset)
		{
			return 0;
		}
		Offset += bytes;
		return *(uint32_t *)(Data + Offset - bytes);
	}

	bool ReadInt32(int32_t* ao_v)
	{
		const int bytes = sizeof(int32_t);
		if (Data == NULL || bytes > Size - Offset)
		{
			return false;
		}
		*ao_v = *(int32_t *)(Data + Offset);
		Offset += bytes;
		return true;
	}

	bool ReadFloat(float* ao_v)
	{
		const int bytes = sizeof(float);
		if (Data == NULL || bytes > Size - Offset)
		{
			return false;
		}
		*ao_v = *(float *)(Data + Offset);
		Offset += bytes;
		return true;
	}

	bool ReadString(char* ao_buf, int32_t a_len)
	{
		if (Data == NULL || a_len > Size - Offset)
		{
			return false;
		}
		memcpy(ao_buf, Data + Offset, a_len*sizeof(char));
		ao_buf[a_len] = '\0';
		Offset += a_len*sizeof(char);
		return true;
	}

	template< typename _type_ >
	bool ReadArray( std::vector< _type_ > & out, const int numElements ) const
	{
		const int bytes = numElements * sizeof( out[0] );
		if ( Data == NULL || bytes > Size - Offset )
		{
			out.resize( 0 );
			return false;
		}
		out.resize( numElements );
		memcpy( &out[0], &Data[Offset], bytes );
		Offset += bytes;
		return true;
	}

	bool IsAtEnd() const
	{
		return ( Offset == Size );
	}

private:
	const uint8_t *	Data;
	int32_t			Size;
	mutable int32_t	Offset;
	bool			Allocated;
};


#endif // LVR_BinaryFile_h
