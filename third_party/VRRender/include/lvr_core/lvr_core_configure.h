#ifndef __lvr_core_configure_h__
#define __lvr_core_configure_h__

#define LVR_CPU_x86

#ifdef _WINDOWS
//#define LVR_OS_WIN
#define LVR_OS_WIN32
#define LVR_CC_MSVC 1600
#define LVR_MSVC_SAFESTRING

#pragma warning(disable: 4275)/*********non dll base class for dll class other place***********************/
#pragma warning(disable: 4251)/*****************non dll base class for dll class other place***************/
#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif
//#pragma warning(disable: 4819)/****** character represented in the current code page in Unicode fromat **************************/
#pragma warning(disable: 4099)/****** pdb debug info **************************/

#elif defined _IOS
#define LVR_OS_IOS
#else
#define LVR_OS_ANDROID
#endif
#ifdef LVR_OS_ANDROID
	#define LVR_API_IMPORT
	#define LVR_API_EXPORT
#else
	#define LVR_API_IMPORT __declspec(dllimport)
	#define LVR_API_EXPORT __declspec(dllexport)
#endif // LVR_OS_ANDROID

#define LVR_API_STATIC

#ifdef lvr_core_EXPORTS
#define LVR_CORE_ITEM LVR_API_EXPORT
#elif defined lvr_core_IMPORT
#define LVR_CORE_ITEM LVR_API_IMPORT
#else
#define LVR_CORE_ITEM LVR_API_STATIC
#endif

#ifdef LVR_OS_WIN32
#define LVR_ASM                  _asm
#else
#define LVR_ASM                  __asm__
#endif

#if defined(LVR_OS_WIN32)
#  ifdef LVR_CPU_X86
#    if defined(__cplusplus_cli)
#      define LVR_DEBUG_BREAK   do { __debugbreak(); } while(0)
#    elif defined(LVR_CC_GNU)
#      define LVR_DEBUG_BREAK   do { LVR_ASM("int $3\n\t"); } while(0)
#    else
#      define LVR_DEBUG_BREAK   do { LVR_ASM int 3 } while (0)
#    endif
#  else
#    define LVR_DEBUG_BREAK     do { __debugbreak(); } while(0)
#  endif
// Android specific debugging support
#elif defined(LVR_OS_ANDROID)
#  define LVR_DEBUG_BREAK       do { __builtin_trap(); } while(0)
// Unix specific debugging support
#elif defined(LVR_CPU_X86) || defined(LVR_CPU_X86_64)
#  define LVR_DEBUG_BREAK       do { LVR_ASM("int $3\n\t"); } while(0)
#else
#  define LVR_DEBUG_BREAK       do { *((int *) 0) = 1; } while(0)
#endif

#ifndef LVR_ASSERT
	#if defined(_DEBUG) || defined(LVR_BUILD_DEBUG)
	#define LVR_ASSERT(p)  do { if (!(p))  { LVR_DEBUG_BREAK; } } while(0)
	#else
	#define LVR_ASSERT(a)  ((void)0)
	#endif
#endif

#if defined(LVR_CC_GNU)
#  define   LVR_UNUSED(a)   do {__typeof__ (&a) __attribute__ ((unused)) __tmp = &a; } while(0)
#else
#  define   LVR_UNUSED(a)   (a)
#endif

#define     LVR_UNUSED1(a1) LVR_UNUSED(a1)
#define     LVR_UNUSED2(a1,a2) LVR_UNUSED(a1); LVR_UNUSED(a2)
#define     LVR_UNUSED3(a1,a2,a3) LVR_UNUSED2(a1,a2); LVR_UNUSED(a3)
#define     LVR_UNUSED4(a1,a2,a3,a4) LVR_UNUSED3(a1,a2,a3); LVR_UNUSED(a4)
#define     LVR_UNUSED5(a1,a2,a3,a4,a5) LVR_UNUSED4(a1,a2,a3,a4); LVR_UNUSED(a5)
#define     LVR_UNUSED6(a1,a2,a3,a4,a5,a6) LVR_UNUSED4(a1,a2,a3,a4); LVR_UNUSED2(a5,a6)
#define     LVR_UNUSED7(a1,a2,a3,a4,a5,a6,a7) LVR_UNUSED4(a1,a2,a3,a4); LVR_UNUSED3(a5,a6,a7)
#define     LVR_UNUSED8(a1,a2,a3,a4,a5,a6,a7,a8) LVR_UNUSED4(a1,a2,a3,a4); LVR_UNUSED4(a5,a6,a7,a8)
#define     LVR_UNUSED9(a1,a2,a3,a4,a5,a6,a7,a8,a9) LVR_UNUSED4(a1,a2,a3,a4); LVR_UNUSED5(a5,a6,a7,a8,a9)

#define LVR_COMPILER_ASSERT(p)

#ifndef LVR_OS_ANDROID
#define LVR_FAIL(...) 
#endif

#ifndef lvr_min
#define  lvr_min(a,b) ((a)<(b)?(a):(b))
#define  lvr_max(a,b) ((a)>(b)?(a):(b))
#endif

#ifndef NULL
#define NULL 0
#endif


#include <stdio.h>
#include <stdlib.h>


#endif