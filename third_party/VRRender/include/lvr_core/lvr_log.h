#ifndef __lvr_log_h__
#define __lvr_log_h__

#include "lvr_data_types.h"
#include <stdarg.h>

#if defined(LVR_OS_WIN32)

	#include <stdio.h>
	#define LVR_LOG(...) printf( __VA_ARGS__ );printf("\n")
	#define LVR_LOG_ERROR( ... ) printf( __VA_ARGS__ );printf("\n")
#elif defined(LVR_OS_ANDROID)

#include <android/log.h>

// Strips the directory and extension from fileTag to give a concise log tag
void LogWithFileTag( int prio, const char * fileTag, const char * fmt, ... );

// Our standard logging (and far too much of our debugging) involves writing
// to the system log for viewing with logcat.  Previously we defined separate
// LOG() macros in each file to give them file-specific tags for filtering;
// now we use this helper function to massage the __FILE__ macro into just a
// file base -- jni/App.cpp becomes the tag "App".
#define LVR_LOG( ... ) LogWithFileTag( ANDROID_LOG_WARN, __FILE__, __VA_ARGS__ )
#define LVR_LOG_ERROR( ... ) LogWithFileTag( ANDROID_LOG_ERROR, __FILE__, __VA_ARGS__ )
#define LVR_FAIL( ... ) {LogWithFileTag( ANDROID_LOG_WARN, __FILE__, __VA_ARGS__ );abort();}

#define DROIDLOG( __tag__, ...) ( (void)__android_log_print(ANDROID_LOG_WARN, __tag__, __VA_ARGS__) )
#define DROIDFAIL( __tag__, ... ) {(void)__android_log_print(ANDROID_LOG_WARN, __tag__, __VA_ARGS__);abort();}

#elif defined(LVR_OS_IOS)

#define LVR_LOG(...) printf( __VA_ARGS__ );printf("\n")

#else
#define LVR_LOG(...)
#endif

#define LVR_ASSERT_LOG(c, args)     ((void)0)


#endif
