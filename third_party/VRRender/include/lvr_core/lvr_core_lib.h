#ifndef __lvr_core_lib_h__
#define __lvr_core_lib_h__

#include "lvr_data_types.h"
#include "lvr_allocator.h"
#include "lvr_time.h"
#include "lvr_log.h"
#include "lvr_ref.h"
#include "lvr_core_util.h"
#include "lvr_path_util.h"
#include "lvr_thread.h"
#include "lvr_util_utf8.h"
#include "lvr_vsync.h"

#ifdef LVR_OS_WIN32
#define _CRTDBG_MAP_ALLOC 
#include <stdlib.h>
#include <crtdbg.h> 

#ifdef _DEBUG
#define new  new(_NORMAL_BLOCK, __FILE__, __LINE__) 
#endif
//////////////////////////////////////////////////////////////////////////
/* put this with your main.cpp. so it will check your memory leak.
inline void EnableMemLeakCheck()
{
	//该语句在程序退出时自动调用 _CrtDumpMemoryLeaks(),用于多个退出出口的情况.
	//如果只有一个退出位置，可以在程序退出之前调用 _CrtDumpMemoryLeaks()
	_CrtSetDbgFlag(_CrtSetDbgFlag(_CRTDBG_REPORT_FLAG) | _CRTDBG_LEAK_CHECK_DF);
}*/
//////////////////////////////////////////////////////////////////////////
#endif//lvr_os_win32

#endif
