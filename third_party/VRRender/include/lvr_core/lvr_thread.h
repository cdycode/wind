#ifndef __lvr_thread_h__
#define __lvr_thread_h__


class lvr_mutex
{
public:
	virtual ~lvr_mutex(){}

	virtual void lock() = 0;
	virtual	bool try_lock() = 0;
	virtual	void unlock() = 0;
};

class scoped_lock
{
public:
	scoped_lock(){}
	scoped_lock(lvr_mutex* mutex_)
		: _mutex(mutex_)
	{
		mutex_->lock();
	}

	~scoped_lock()
	{
		_mutex->unlock();
	}

private:
	lvr_mutex* _mutex;
};

class lvr_condition
{
public:
	virtual ~lvr_condition(){}

	virtual void wait() = 0;
	virtual void notify_one() = 0;
	virtual void notify_all() = 0;
};

class lvr_thread
{
public:
	virtual ~lvr_thread(){}
	virtual void join() = 0;
	virtual void set_name(const char* a_name) = 0;
	virtual void exit(int _Code) = 0;
};

class lvr_thread_manager
{
public:
	virtual ~lvr_thread_manager(){}

	virtual lvr_mutex* create_mutex() = 0;
	virtual lvr_condition* create_condition(lvr_mutex* a_mutex) = 0;
	virtual lvr_thread* create_thread(void*(*thread_start_function)(void *), void *parameter) = 0;
	virtual void sleep(unsigned long milliseconds) = 0;
};

extern lvr_thread_manager* lvr_get_thread_manager();

#define auto_lock(mutex_) scoped_lock __scoped_lock##mutex_(mutex_)

#endif