/*
 * lvr_media_player.h
 *
 *  Created on: 2016-1-7
 *      Author: Administrator
 */

#ifndef LVR_MEDIA_PLAYER_H_
#define LVR_MEDIA_PLAYER_H_

#include "lvr_media_interface.h"
#ifdef LVR_OS_ANDROID_XX
#include "MessageQueue.h"
using namespace OVR;
class lvr_movie_render_manager;
class lvr_surface_texture;
class lvr_media_player : public lvr_media_interface
{
private:
	lvr_media_player(void);
public:
	static lvr_media_player* get_media_player();
	virtual ~lvr_media_player(void);

	static void init_media_player(JNIEnv * a_jni_ptr);

	bool init(JNIEnv * a_jni_ptr);
	void release(void);
	int	update(double a_current_time);
	lvr_texture* get_texture();
	//a_start_pos:单位为秒
	bool	play(const char* a_file,const char* subtitle,int a_start_pos);
	bool	play();
	void 	stop(void);
	void	pause(void);
	void 	resume(void);

	//a_seek_pos:单位为秒
	void	seek(int a_seek_pos);

	int		get_position(void);
	int 	get_duration(void);
	
	int		get_width(void);
	int		get_height(void);

	void	set_callback( void* a_cb);
	bool	is_playing();

	void    set_movie_render_manager(lvr_movie_render_manager* a_mrm){_mrm = a_mrm;}

private:
	void command(const char* a_msg);
public:
	MessageQueue	_message_queue;
private:
	static jclass		_lvr_media_player_class;

	JNIEnv*		_jni_env;

	static jmethodID 	_play_movie_method_id;
	static jmethodID	_stop_movie_method_id;
	static jmethodID	_pause_movie_method_id;
	static jmethodID 	_resume_movie_method_id;
	static jmethodID 	_get_position_method_id;
	static jmethodID 	_seek_method_id;
	static jmethodID 	_get_duration_method_id;
	lvr_surface_texture*	_movie_surface_texture;
	lvr_movie_render_manager* _mrm;
	bool					_is_play;
	bool					_new_video;
};
#endif //end os android

#endif /* LVR_MEDIA_PLAYER_H_ */
