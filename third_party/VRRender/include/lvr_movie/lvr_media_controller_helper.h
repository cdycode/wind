#ifndef LVR_MEDIA_CONTROLLER_HELPER_0706_H__
#define LVR_MEDIA_CONTROLLER_HELPER_0706_H__

#include <functional>
#include <string>
#include "lvr_vector3.h"
#include "lvr_matrix4.h"
#include "lvr_movie_configure.h"

class lvr_ui_base;
class lvr_ui_2d;
class lvr_ui_manager;
class lvr_media_controller_ui_v2;
class lvr_json;
class lvr_media_interface;


class LVR_MOVIE_API lvr_media_controller_helper{
public:

	enum movie_dpi
	{
		E_1K,
		E_2K,
		E_4K,
		E_dpi_Count
	};


	lvr_media_controller_helper();
	~lvr_media_controller_helper();
	static lvr_media_controller_helper* get_simple_ui();
	static void release();

	bool  init(lvr_ui_manager* ui_mgr, lvr_media_interface* media_ptr);
	void  uninit();
	lvr_ui_base*  update(const lvr_vector3f& a_pos, const lvr_vector3f& a_look, const lvr_vector3f& a_right, double a_time, int a_movie_time);
	void update_background(const lvr_vector3f& a_pos, const lvr_vector3f& a_look, const lvr_vector3f& a_sight_right);
	void  draw(const lvr_matrix4f& a_vp_mat);

	void  set_movie_src(bool a_is_local);
	void  set_seek_time(int a_ms);
	void  set_current_time(int a_ms);
	void  set_total_time(int a_ms);
	void  set_movie_mode(int a_mode);
	void  set_movie_name(const char* a_movie_name);

	void  set_dpi_types(int a_id);
	void  set_dpi_types(const std::string& dpis);	// 1k;2k;4k;  and so on...
	void  set_movie_dpi(movie_dpi a_dpi_mode);
	void  set_movie_dpi(const std::string& dpi);	// 1k , 2k , 4k 
	void  set_visible(bool a_visible);
	bool  get_visible();

	void  reset();
	void  set_video_end();

	void set_event_handle_cb(bool(*func)(const std::string&args, void*), void* obj);
	static bool media_control_handle(const std::string&args, void*);
	void set_voice_brightness_cb(void (*vb_func)(const std::string&cmd, void *obj, int& inoutdata));
	static void media_control_operate_voice_and_brightness(const std::string&cmd, void* obj, int& inoutdata);
	static void media_control_get_list_data(const std::string&args, void* obj, std::string& outdata);
	static void media_more_list_load_img(const std::string&url, void* obj, std::string& outdata);
	static int media_state_func(char*, void*);


	void set_relative_list_data(const std::string& data);
	void set_continue_list_data(const std::string& data);
	std::string get_listdata(const std::string& args);
	void update_more_list_image(const std::string& args, const std::string& localpath);

	void append_event(const std::string& args, void* p);
	void operate_event();
	void set_screen_center(const lvr_vector3f center);

	void extra_menu_set_download_func(void(*func)(void *obj, const std::string& url, std::function<void(bool success, const std::string& localpath)> &callback), void *obj);


	void set_pick_type(bool a_use_camera_pick);
	void set_pick_ray(const lvr_vector3f& a_pos, const lvr_vector3f& a_dir);
	bool is_show_none();
	bool show_menu(bool is_show);
	lvr_vector3f*	get_main_region();
	void set_ui_type(int t);
	void set_interactive_video(bool tag);
private:

	static lvr_media_controller_helper* s_simple_ui;

	enum	ui_tag{
		normal_2d = 1,
		normal_3d = 10,
		aiqiyi_3d = 20,
		xiaomi_3d = 30,
		sumsang_3d = 40,
	};

	struct media_event{
		std::string args;
		void* obj;
		void* from;
	};
	lvr_media_interface*		_media_ptr;
	bool(*_handle_func)(const std::string&args, void*);
	void* _handler_obj;
	void(*_handle_vb_func)(const std::string&cmd, void *obj, int& inoutdata);
	std::vector<media_event>	_vec_media_event1;
	std::vector<media_event>	_vec_media_event2;
	int							_num_operate_media_event;

	lvr_media_controller_ui_v2*	_ui_v2;
	lvr_json*					_relative_list_data;
	lvr_json*					_continue_list_data;

	lvr_program*				_background_prog;
	lvr_vertex_buffer*			_background_vb;
	lvr_index_buffer*			_background_ib;
	lvr_vertex_format*			_background_vf;
	bool						_visible;
	bool						_show_background;
};


#endif