#ifndef __lvr_focus_sphere_h__20160905__
#define __lvr_focus_sphere_h__20160905__

#include "lvr_math_lib.h"
#include "lvr_render_lib.h"

class lvr_focus_sphere
{
public:
	lvr_focus_sphere();
	~lvr_focus_sphere();
public:
	void init();
	void uninit();
	void set_color(const lvr_vector4uc& a_color);
	void set_position(const lvr_vector3f& a_pos);
	void set_orientation(const lvr_vector3f& a_right, const lvr_vector3f& a_up);
	void set_size(const lvr_vector2f& a_size);
	void draw(const lvr_matrix4f& a_vp_mat);
private:
	lvr_render_object		_ro;
	lvr_vector3f			_right;
	lvr_vector3f			_up;
	lvr_vector3f			_pos;
	lvr_vector2f			_size;
	lvr_vector4uc			_color;
	lvr_program*			_prog;
	bool					_update_parms;
};

#endif
