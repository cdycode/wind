#ifndef __lvr_hot_point_manager_H__
#define __lvr_hot_point_manager_H__

#include "lvr_movie_configure.h"
#include "lvr_hot_point.h"
#include <map>

#define HT_RING_NUM 3
class LVR_MOVIE_API lvr_hot_point_manager
{
public:
static	lvr_hot_point_manager* get_manager();
static  void release();
	~lvr_hot_point_manager(){}
private:
	lvr_hot_point_manager();
	static lvr_hot_point_manager* s_hot_point_manager_ptr;
public:
	void init(int segment = 32, int style = 0);
	void uninit();
	void set_texture(lvr_texture* a_tex);
	void add_hot_point(lvr_hot_point* a_hot_point_ptr);
	void remove_hot_point(lvr_hot_point* a_hot_point_ptr);
	void update(float a_time);
	void draw(const lvr_matrix4f& a_vp_mat,const lvr_vector3f& a_cam_pos);
private:
	void draw_one_hot_point(lvr_hot_point* a_hp, const lvr_vector3f& a_cam_pos);
private:
	std::map<lvr_hot_point*,lvr_hot_point*>	_hot_points;//just make convenient from map find.
	lvr_render_object	_center_wave_work;
//	const static int	HT_RING_NUM = 5;
	static float	HT_SCALE_MIN;
	static float	HT_SCALE_MAX;
	static float		_delta_value;
	float				_scale[HT_RING_NUM];
	float				_last_time;
	int					_center_pos;
	int					_ring_pt_num;
	lvr_program*		_program;
	lvr_texture*		_texture;
	int					_pos_loc;
	int					_right_loc;
	int					_up_loc;
	int					_scale_pos_alpha_loc;
	int					_play_style;

};

class LVR_MOVIE_API lvr_direct_draw
{
public:
	static	lvr_direct_draw* get_direct_draw();
	static  void release();
	~lvr_direct_draw();
private:
	static lvr_direct_draw* s_direct_draw_ptr;
	lvr_direct_draw();
public:
	void init();
	void uninit();
	void set_texture_to_show(const lvr_vector3f a_pos, const lvr_vector3f& a_right, const lvr_vector3f& a_up,const lvr_vector2f& a_size, lvr_texture* a_tex_ptr);
	void show();
	void hide();
	void draw(const lvr_matrix4f& a_vp_mat);
private:
	lvr_program*		_program;
	lvr_texture*		_texture;
	int					_pos_loc;
	int					_right_loc;
	int					_up_loc;
	lvr_vertex_buffer*	_vb;
	lvr_vertex_format*	_vf;
	bool				_visible;
	bool				_inited;
	
};

#endif
