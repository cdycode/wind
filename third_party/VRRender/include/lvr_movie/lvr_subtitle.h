#ifndef __lvr_subtitle_h__20161222__
#define __lvr_subtitle_h__20161222__

#include "lvr_movie_configure.h"
#include "lvr_ui_manager.h"
class LVR_MOVIE_API lvr_subtitle : public lvr_ui_menu
{
public:
	lvr_subtitle();
	~lvr_subtitle();
public:
	void init();
	void set_content_info(const lvr_vector3f& a_pos, const lvr_vector3f& a_right, const lvr_vector3f& a_up, const char* a_str);
private:
	lvr_ui_text*	_text;
};

#endif
