#ifndef __lvr_movie_configure_h__
#define __lvr_movie_configure_h__


#include "lvr_log.h"
#include "lvr_time.h"

#ifdef lvr_movie_EXPORTS
#define LVR_MOVIE_API __declspec(dllexport)
#elif defined lvr_movie_IMPORT
#define LVR_MOVIE_API __declspec(dllimport)
#else
#define LVR_MOVIE_API
#endif


#include "lvr_render_lib.h"
#include "lvr_bitmap_font_manager.h"
#include "lvr_model_file.h"
#include "lvr_package_files.h"
#include "lvr_ui_manager.h"
#include "lvr_quaternion.h"
#include "lvr_render_lib.h"


#endif
