#ifndef __lvr_nonlinear_movie_event_ui_h__
#define __lvr_nonlinear_movie_event_ui_h__

#include "lvr_nonlinear_movie_event.h"
#include "lvr_ui_menu.h"
#include "lvr_focus_sphere.h"
#include "lvr_transform.h"
//call fire_on_event to add ui
extern lvr_quaternionf get_suitable_q(const lvr_vector3f& a_dir);
class lvr_nonlinear_movie_choice_ui : public lvr_ui_menu
{
public:
	lvr_nonlinear_movie_choice_ui();
	~lvr_nonlinear_movie_choice_ui();
public:
	bool	set_event(lvr_nonlinear_movie_choice* a_event);
	bool	fire_on_ui(lvr_ui_base*	a_ui);
	void	init_ui(const lvr_vector3f& a_center, float a_w, float a_h);
	void	set_pos(const lvr_vector3f& a_pos,const lvr_vector3f& a_dir);
	void	set_pick_enable(bool pick);//specially for video work.
private:
	lvr_nonlinear_movie_choice*		_movie_choice;
	lvr_ui_text*					_title;
	lvr_ui_text*					_description;
	lvr_ui_text*					_choice_text[NONLINEAR_MOVIE_MAX_CHOICE_NUM];
	lvr_ui_2d*						_background_rect[NONLINEAR_MOVIE_MAX_CHOICE_NUM+2];
	lvr_vector3f					_back_pos[3];
	lvr_ui_2d*						_background;
	lvr_ui_2d*						_split_line;
	int								_choice_num;
	float							_padding_value;
};

class lvr_nonlinear_movie_info_ui : public lvr_ui_menu
{
public:
	lvr_nonlinear_movie_info_ui();
	~lvr_nonlinear_movie_info_ui();
public:
	bool	set_event(lvr_nonlinear_movie_info* a_event);
	bool	fire_on_ui(lvr_ui_base*	a_ui);
	void	init_ui(const lvr_vector3f& a_center, float a_w, float a_h);
	void	set_pos(const lvr_vector3f& a_pos, const lvr_vector3f& a_dir,lvr_texture* atex);
	lvr_focus_sphere& get_focus_sphere();
	bool	active_hot_point(lvr_hot_point *ptr, lvr_nonlinear_movie_info* ev);
	lvr_nonlinear_movie_info* get_curr_event();
	void	set_pick_in(bool in);
	void	reset();
public:
	void	draw(const lvr_matrix4f& a_vp_matrix);
	void	update_alpha();
private:
	lvr_nonlinear_movie_info*		_movie_info;
	lvr_ui_text*					_title;
	lvr_ui_text*					_description;
	lvr_ui_text*					_ui_ok_text;

	lvr_ui_2d*						_ui_ok_background;
	lvr_ui_2d*						_background;
	lvr_render_object				_background_ro;
	lvr_transform					_ro_transform;
	lvr_texture*					_background_texture;
	float							_padding_value;
	float							_magic_circle_radius;
	lvr_focus_sphere				_focus_sphere;
	int								_use_model_state;
	lvr_nonlinear_ui_info			_center_rect_pic;
	std::map<lvr_hot_point*, lvr_nonlinear_movie_info*>	_map_hot_event;
	lvr_hot_point*					_curr_focus_hot_point;
	float							_curr_info_alpha;
	int								_alpha_dir;				// 0 dont change  1: add   -1:sub
	lvr_program*					_prog;
	int								_alpha_loc;
};

class lvr_nonlinear_movie_comment_ui : public lvr_ui_menu
{
public:
	lvr_nonlinear_movie_comment_ui();
	~lvr_nonlinear_movie_comment_ui();
	bool	set_event(lvr_nonlinear_movie_comment* a_event);
	void	init_ui();
	void	set_pos(const lvr_vector3f& a_pos,float a_dist);
private:
	lvr_nonlinear_movie_comment*	_comment;
	lvr_ui_text*					_title;
	lvr_ui_text*					_description;
};

// class lvr_nonlinear_movie_subtitle_ui : public lvr_ui_menu
// {
// public:
// 	lvr_nonlinear_movie_subtitle_ui();
// 	~lvr_nonlinear_movie_subtitle_ui();
// public:
// 	void set_event(lvr_nonlinear_movie_event* a_event);
// 	void set_pos(const lvr_vector3f& a_pos,const lvr_vector3f& a_dir, float a_dist);
// public:
// 	lvr_nonlinear_movie_event*	_subtitle_event;
// 	lvr_ui_text*				_content_str;
// };

#endif
