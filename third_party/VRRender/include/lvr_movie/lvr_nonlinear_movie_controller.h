#ifndef __lvr_nonlinear_movie_controller_h__
#define __lvr_nonlinear_movie_controller_h__

#include "lvr_nonlinear_movie_event.h"
#include "lvr_ui_manager.h"
#include "lvr_focus_sphere.h"

class lvr_zip;
class lvr_camera;
class LVR_MOVIE_API lvr_nonlinear_movie_controller
{
public:
	static lvr_nonlinear_movie_controller* get_nonlinear_movie_ctrl();
	static void release();
	~lvr_nonlinear_movie_controller();
private:
	lvr_nonlinear_movie_controller();
	static lvr_nonlinear_movie_controller*	_lnmc;
public:
	void	init();
	void	clear_all_events();
	void    set_camera(lvr_camera* a_cam_ptr);
	void    set_media(lvr_media_interface* a_media_ptr);
	lvr_media_interface* get_control_media();
	lvr_camera*	get_camera();
	void	uninit();
	void	set_ui_manager(lvr_ui_manager* a_ui_mgr);
	bool	load_from_file(const char* a_file);
	bool	write_to_file(const char* a_file);
	bool	add_event(lvr_nonlinear_movie_event* a_event);
	bool	remove_event(lvr_nonlinear_movie_event* a_event);
	bool	preprocess();
	int		process(int a_cur_frame_id,const lvr_vector3f& a_dir);
	void	re_cal_deal_pos(int frame_id);
	bool	need_draw_extra();
	void	draw_extra(const lvr_matrix4f& a_vp_mat);
	lvr_zip* get_current_czm_zip_file();
	void set_app_handle_cb(bool(*func)(const std::string&args, void*), void* obj);
	bool send_app_handle_msg(const std::string& args);
private:
	bool	load_from_buffer(const uint8_t* a_buf, const uint32_t a_buf_len);
private:
	lvr_zip*				_czm_zip_file;
	lvr_camera*				_camera_ptr;
	lvr_media_interface*	_media_ptr;
	lvr_ui_manager*		_ui_mgr;
	lvr_ui_menu*		_nonlinear_movie_ui[2];
	lvr_ui_menu*		_hot_point_menu;
	std::map<lvr_nonlinear_movie_event*,lvr_ui_menu*>	_comment_ui_map;
	std::vector<lvr_nonlinear_movie_event*>	_events;
	uint32_t		_deal_pos;
	std::vector<lvr_nonlinear_movie_event*>	_current_effecting_events;
	lvr_nonlinear_movie_info*			_current_pos_info;
	lvr_nonlinear_movie_choice*			_current_choice_show;
	int32_t								_last_frame_id;//in case of jump
	bool							_show_focus_sphere;
	bool							_is_in_interactive_state;
	bool(*_app_handle_func)(const std::string&args, void*);
	void* _app_handler_obj;
};

#endif
