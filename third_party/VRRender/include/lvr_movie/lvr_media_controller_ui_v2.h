#ifndef _lvr_media_controller_ui_v2_
#define _lvr_media_controller_ui_v2_


#include "lvr_ui_manager.h"
#include "lvr_camera.h"
#include "lvr_ui_menu.h"
#include "lvr_cinema_render.h"
#include "lvr_media_interface.h"

#include<map>
#include <functional>

#include <lvr_ui_event_manager.h>

//#include "lvr_popup_menu.h"

class lvr_image;
class lvr_popup_menu;
class lvr_ui_multi_image_with_cover_tex;

class ui_point_progress_bar : public lvr_ui_2d{
public:
	ui_point_progress_bar();
	~ui_point_progress_bar();
	void init(lvr_ui_menu* ui_menu, int point_num);
	void uninit();
	void set_progress_texture(const std::string& default_pic, const std::string& progress_pic);
	virtual void set_size(const lvr_vector2f& a_size);
	virtual void set_position(const lvr_vector3f& pos);
	virtual void set_orientation(const lvr_vector3f& a_right, const lvr_vector3f& a_up);
	void gen_progress();
	void set_progress(float rate);

	void config_ui_click(lvr_event_binding leb, lvr_event_binding leb_in, lvr_event_binding leb_out);
	float check_ui_progress(lvr_ui_base* ui);

	void set_camera_pos(const lvr_vector3f& pos);

	virtual void set_visible(bool _visible);

	void set_border_pic(const std::string& pre_pic, const std::string& post_pic, float pre_small_degree = 1.0f);
	void bar_on_ui_focus(lvr_ui_base* a_ui, bool in);

	static void* bar_focus_ui_cb(lvr_ui_base* a_ui, void* xx);
	static void* bar_leave_ui_cb(lvr_ui_base* a_ui, void* xx);

private:
	float				_rate;
	float				_pre_border;    // percent
	float				_post_border;
	int					_point_num;
	lvr_vector2f		_point_size;
	lvr_ui_manager*		_ui_mgr;
	lvr_ui_text*		_pp_texts;
	lvr_ui_2d*			_pp_rects;
	lvr_ui_2d*			_pre_ui;
	lvr_ui_2d*			_post_ui;
	lvr_rect2f			_pp_rect_uv[2];
	lvr_ui_menu*		_pp_menu;
	float				_pre_small_degree;
	lvr_vector3f		_camera_pos;
};




class  lvr_media_controller_ui_v2
{
public:
	enum ui_rect_name
	{
		E_Rect_PausePlay = 0,
		E_Rect_Brightness_Sel,
		E_Rect_Voice_Sel,
		E_Rect_Info,
		E_Rect_Resize_Screen,
		E_Rect_ShowHide,
		E_Rect_Goback,
		E_Rect_Video_Repeat,
		E_Rect_Return,
		E_Rect_Progress_background,
		E_Rect_Progress_passed,
		E_Rect_Progress_passed_begin,
		E_Rect_Progress_passed_end,
		E_Rect_Progress_cached,
		E_Rect_Main_background,
		E_Rect_MovieType_background,
		E_Rect_DpiChoose_background,
		E_Rect_Resize_Screen_background,
		E_Rect_SceneChoose_background,
		E_Rect_Brightness_bar,
		E_Rect_Voice_bar,
		E_Rect_SceneChoose,
		E_Rect_LockSight,
		E_Rect_UnlockSight,
		E_Rect_Count
	};
	enum ui_text_name
	{
		E_Text_360,
		E_Text_360LR,
		E_Text_360RL,
		E_Text_360TD,
		E_Text_360DT,
		E_Text_180,
		E_Text_180LR,
		E_Text_180RL,
		E_Text_180TD,
		E_Text_180DT,
		E_Text_3D,
		E_Text_3DLR,
		E_Text_3DRL,
		E_Text_3DTD,
		E_Text_3DDT,
		//		E_Text_SceneTitle,
		E_Text_MediaScene1,
		E_Text_MediaScene2,
		E_Text_MediaScene3,
		E_Text_Scene,
		E_Text_MovieName,
		E_Text_MovieName2,
		E_Text_CurPos,
		E_Text_Seek,
		E_Text_Total,
		E_Text_1K,
		E_Text_2K,
		E_Text_4K,
		E_Text_Screen_Size1,
		E_Text_Screen_Size2,
		E_Text_Screen_Size3,
		E_Text_Count
	};
	enum movie_dpi
	{
		E_1K,
		E_2K,
		E_4K,
		E_dpi_Count
	};
	enum movie_scene
	{
		E_Huwaishatan,
		E_Jiatingyingyuan,
		E_Jumuyingting,
		E_Scene_Count
	};
private:
	enum menu_state
	{
		E_Menu_Hide_Show,
		E_Menu_Main,
		E_Menu_Dpi,
		E_Menu_MovieType,
		E_Menu_Scene_Select,
		E_Menu_Brightness,
		E_Menu_Voice,
		E_Menu_Resize_Screen,
		E_Menu_Video_End,
		E_Menu_Count,
		E_Menu_None,
	};
public:
	~lvr_media_controller_ui_v2();
	lvr_media_controller_ui_v2();

public:
	bool  init(lvr_ui_manager* ui_mgr);
	bool  config();
	void  uninit();
	void update(const lvr_vector3f& a_pos, const lvr_vector3f& a_look, const lvr_vector3f& a_right, double a_time, int a_movie_time);
	void set_pick_type(bool a_use_camera_pick);
	void set_pick_ray(const lvr_vector3f& a_pos, const lvr_vector3f& a_dir);
	void  set_movie_src(bool a_is_local);
	void  set_seek_time(int a_ms);
	void  set_current_time(int a_ms);
	void  set_total_time(int a_ms);
	void  set_movie_mode(int a_mode);
	void  set_movie_name(const char* a_movie_name);
	void  set_movie_dpi(movie_dpi a_dpi_mode);
	void  set_dpi_types(int a_id);
	void  set_visible(bool a_visible);
	bool  get_visible();
	void on_ui_click(lvr_ui_base* a_ui);
	void on_ui_focus(lvr_ui_base* a_ui);
	void on_ui_leave(lvr_ui_base* a_ui);
	//make sure you give a unit vector for camera right outside
	void set_ui_pos(const lvr_vector3f& a_camera_pos, const lvr_vector3f& a_camera_right, const lvr_vector3f&a_up, bool is_use_screen_dir = false);
	static void* click_ui_cb(lvr_ui_base* a_ui, void* xx);
	static void* focus_ui_cb(lvr_ui_base* a_ui, void* xx);
	static void* leave_ui_cb(lvr_ui_base* a_ui, void* xx);
	void set_media_definition(int dpi);
	void set_look_up_style(bool a_use_look);
	lvr_vector3f*	get_main_region();

	void set_ui_type(int t);

	bool is_show_none();
	bool show_menu(bool is_show);
	void  set_video_end();

	void set_click_btn_cb(bool(*func)(const std::string&args, void*), void* obj);
	void set_get_media_state_cb(int (*func)(char* , void*));
	void set_screen_center(const lvr_vector3f center);
	void set_voice_and_brightness_method(void (*mthd)(const std::string&cmd, void*, int& inoutdata));
	void set_more_list_data_cb(void(*func)(const std::string&args, void* obj, std::string& outdata));
	static void _more_list_data(const std::string&args, void* obj, std::string& outdata);





	//additional menu list ===================================== start
	bool extra_menu_init();
	void extra_menu_uninit();

	lvr_ui_base* extra_menu_update(const lvr_vector3f& a_test_ray_pos, const lvr_vector3f& a_test_ray_look, const lvr_vector3f& a_pos, const lvr_vector3f& a_up, const lvr_vector3f& a_right, double curr_time);
	void extra_menu_draw(const lvr_matrix4f& a_vp_mat);


	bool extra_menu_is_visible();
	void extra_menu_set_visible(bool a_visible);

	void extra_menu_config_menu(const lvr_vector3f& pos, const lvr_vector3f& right, const lvr_vector3f& up, lvr_ui_manager* ui_mgr);
	void extra_menu_show_right_menu(bool visible, float alpha);
	void extra_menu_show_bottom_menu(bool visible);
	void extra_menu_reset();
	void extra_menu_on_download_over(std::string url, void *obj, bool success, const std::string& localpath);
	void extra_menu_set_download_func(void(*func)(void *obj, const std::string& url, std::function<void(bool success, const std::string& localpath)> &callback), void* obj);

	void set_interactive_video(bool tag);
	//additional menu list =============================end



private:
	void update_ui_state(menu_state a_ui_state);
	void set_play_state(bool a_play_pause);
	void update_seek_time();
	void update_ui_pos(const lvr_vector3f& a_camera_pos);
	void update_show_hide_ui_pos(const lvr_vector3f& a_pos, const lvr_vector3f& a_right, const lvr_vector3f& a_up);
	inline void generate_uv_info(int a_id, lvr_ui_2d::tex_info& ao_ti);
	inline void int_to_time_text(char* a_time_text, int a_ms);
	void clear_select_ui_state();
	void set_select_ui_state(ui_rect_name id);

private:
	lvr_vector3f	_pos;
	lvr_vector3f	_right, _up;
	lvr_vector3f	_pick_ray_origin, _pick_ray_dir;
	bool			_use_camera_pick;
	lvr_ui_text*	_texts[E_Text_Count];
	lvr_ui_2d*		_rects[E_Rect_Count];
	lvr_rect2f		_rect_uv[(E_Rect_Return + 4) * 2];//E_Rect_Return+1 is showing rect num the final couple is for pause/play uv change.show/hide
	lvr_ui_menu		_menus[E_Menu_Count];
	lvr_ui_manager*	_ui_mgr;
	lvr_vector3f	_seek_region[4];//only useful for seek time get.
	lvr_vector3f	_mv_type_region[4];//total movie type region for test to see if we need to close mv type choose
	lvr_vector3f	_main_region[4];//main ui region,to judge if we need to hide ui,currently we use down to show ui.
	lvr_vector3f	_cur_mv_type_region[4];//curmovieTypeText region
	lvr_vector2f	_rect_pos[E_Rect_Count];
	lvr_vector2f	_text_pos[E_Text_Count];
	lvr_vector2f	_rect_size[E_Rect_Count];
	lvr_vector2f	_ui_progress_bar_pos;
	lvr_vector2f	_ui_cur_mov_pos;
	lvr_vector2f	_ui_cur_mov_size;
	lvr_vector2f	_movie_type_size;
	lvr_vector2f	_ui_movie_type_choose_pos;
	lvr_vector2f	_progress_bar_pos;
	lvr_vector2f	_progress_bar_left_center_pos;
	char			_movie_type_names[E_Text_3DDT + 1][12];
	char			_movie_dpi_names[3][8];
	char			_movie_scene_names[3][20];
	char			_movie_scene_filenames[3][20];
	char			_movie_screen_size[3][20];
	int				_movie_mode;
	movie_dpi		_movie_dpi_mode;
	menu_state		_cur_ui_state;
	char			_time_text[3][10];
	int				_total_time;
	float			_current_hit_pos;
	float			_current_play_pos;
	char			_movie_name[50];
	bool			_show_seek_time;
	bool			_need_update_pos;
	bool			_main_ui_update;
	bool			_playing;
	bool			_is_local_movie;
	bool			_visible;
	bool			_need_update_movie_name;
	bool			_is_show_scene_select;
	bool			_use_direct_control;
	ui_text_name	_selected_scene_name;

	lvr_ui_menu		_back_ui_menu;
	lvr_ui_2d*		_back_ui;
	struct select_ui{
		lvr_ui_2d*		back_ui;
		ui_rect_name	rect_id;
	};
	select_ui		_select_ui;

	bool			_is_in_brightness_bar;
	bool			_is_in_voice_bar;
	bool			_is_in_progress_region;
	lvr_vector3f	_menu_src_dir;

	enum popup_ui{
		E_Popup_Dpi,
		E_Popup_MovieType,
		E_Popup_SceneSel,
		E_Popup_Brightness,
		E_Popup_Voice,
		E_Popup_Resize_Screen,
		E_Popup_Count,
	};

	enum ui_type{
		cz_2d_ui,
		cz_scene_ui,
	};
	lvr_popup_menu*	_vec_pop_menus[E_Popup_Count];

	popup_ui		_curr_popup_menu;

	double				_last_interaction_time;
	double				_last_update_time;

	bool					_is_lock_cinema;
	lvr_texture*			_video_end_texture;
	lvr_vector3f			_screen_center;
	bool					_is_lockto_screen_center;   // use in 3d video  not 360 video
	bool					_is_in_video_end;

	ui_type					_curr_ui_type;

	void*	_handle_obj;
	bool(*_handle_func)(const std::string&args, void*);
	int(*_get_media_state_func)(char* , void*);
	void(*_voice_brightness_func)(const std::string&cmd, void*, int& inoutdata);
	void(*_get_list_data_func)(const std::string&args, void* obj, std::string& outdata);



	// additional menu list ======================================================start

	enum extra_UI{
		ui_right_1 = 0,
		ui_right_2,
		ui_right_3,
		ui_right_4,
		ui_right_5,				//refresh
		ui_bottom_1,
		ui_bottom_2,
		ui_bottom_3,           // 1��2��3  pictures
		ui_bottom_4,			// priv
		ui_bottom_5,			// post
		ui_end,
	};

	void set_show_img(int pos, int group_id, const std::string &url, int width, int height);
	bool set_pkg_tex(lvr_ui_multi_image_with_cover_tex* img_pos, int pid, const std::string& file);
	void on_extra_menu_ui_click(lvr_ui_base* a_ui);
	void on_extra_menu_ui_focus(lvr_ui_base* a_ui);
	void on_extra_menu_ui_leave(lvr_ui_base* a_ui);
	void on_focus_img(extra_UI id);
	void on_leave_img(extra_UI id);
	
	struct extra_menu_ui{
		extra_UI		_ui_id;
		int				_item_id;
		int				_img_surface;
		int				_dl_update;
		int				_video_id;
		int				_border_loop_id;
		lvr_ui_2d		_ui_play;
		lvr_ui_text		_ui_title;
		lvr_ui_2d 		_ui_img;
		lvr_image*		_update_image;
		std::string		_cmd_data;
		int				_status;
	};


	lvr_vector3f							_ui_pos[ui_end];
	bool									_extra_menu_visible;
	std::vector<extra_menu_ui*>				_extra_menu_ui_rects;

	float									_right_menu_alpha;
	float									_click_alpha;
	bool									_is_init;
	lvr_ui_multi_image_with_cover_tex*		_round_corner_right_imgs;
	lvr_ui_multi_image_with_cover_tex*		_round_corner_bottom_imgs;

	bool									_is_need_update_right;
	bool									_is_need_update_bottom;
	bool									_is_need_update_image;
	bool									_is_visible_right;
	bool									_is_visible_bottom;

	extra_UI								_curr_focus_ui;
	lvr_ui_menu								_ui_menu;

	extra_UI								_curr_bottom_wait_ui;
	bool									_in_reset_right;
	bool									_in_reset_bottom;
	void(*_extra_menu_download_func)(void *obj, const std::string& url, std::function<void(bool success, const std::string& localpath)> &callback);
	void*									_extra_menu_download_obj;

	bool									_is_interactive_video;

	// additional menu list =============================end


};




#endif