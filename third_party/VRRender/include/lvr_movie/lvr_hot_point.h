#ifndef __lvr_hot_point_H__
#define __lvr_hot_point_H__

#include "lvr_movie_configure.h"

class lvr_ui_2d;
class lvr_hot_point : public lvr_ref
{
public:
	lvr_hot_point();
	~lvr_hot_point();
public:
	void set_anim_visible(bool a_visible);
	bool get_anim_visible();
	void set_world_info(const lvr_vector3f& a_pos, const lvr_quaternionf& a_rot);
	void set_world_info(const lvr_vector3f& a_pos, const lvr_vector3f& a_right, const lvr_vector3f& a_up);
	const lvr_vector3f& get_pos();
	const lvr_vector3f& get_right();
	const lvr_vector3f& get_up();
	void set_clickable(bool tag);
	lvr_ui_2d*	get_ui_2d();
private:
	lvr_vector3f	_pos;
	lvr_vector3f	_right;
	lvr_vector3f	_up;
	bool _anim_visible;
	lvr_ui_2d*		_ui_2d;
	bool			_is_clickable;
};



#endif
