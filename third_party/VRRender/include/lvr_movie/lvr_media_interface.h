#ifndef __lvr_media_interface_h__
#define __lvr_media_interface_h__

#include "lvr_movie_configure.h"

typedef void(*PTRMovieCallBack)(void);
typedef void(*PTRMovieIntCallBack)(int);
typedef void(*PTRMovieAudioCallBack)(float*,float*);

class lvr_media_interface
{
public:
	enum media_state
	{
		E_Ready,
		E_Paused,
		E_Finished,
		E_WaitingForCache,
		E_Playing
	};
public:
	lvr_media_interface(){}
	virtual ~lvr_media_interface(){}
public:
	virtual bool play(const char* a_file_or_url,const char* a_subtitle_file,int a_start_pos) = 0;
	virtual int update(double a_current_time) = 0;
	virtual lvr_texture* get_texture() = 0;
	virtual void pause() = 0;
	virtual void resume() = 0;
	virtual void stop() = 0;
	//in second
	virtual void seek(int a_seek_pos) = 0;
	virtual int get_position() = 0;
	virtual int get_duration() = 0;
	virtual bool is_playing() = 0;
	virtual lvr_media_interface::media_state get_media_state() = 0;
	virtual int get_width() = 0;
	virtual int get_height() = 0;
};

#endif
