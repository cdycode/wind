#ifndef __czvr_hw_media_player_h__
#define __czvr_hw_media_player_h__

#include "lvr_movie_configure.h"
#if defined LVR_OS_ANDROID || defined LVR_OS_IOS
#include "lvr_media_interface.h"
#include "Player.h"
class lvr_texture2d_new : public lvr_texture
{
public:
	lvr_texture2d_new():lvr_texture(e_texture_type_2d){_is_huashu_media = false;}
	void set_texture_id(unsigned int id){_tex_id = id;}
	void set_width(int w){_width = w;}
	void set_height(int h){_height = h;}
	bool		_is_huashu_media;
};

class czvr_hw_media_player : public lvr_media_interface
{
public:
	static czvr_hw_media_player* get_media_player();
	~czvr_hw_media_player();
private:
	czvr_hw_media_player();

public:

	void init(void* a_jvm);
	void set_callbacks(PTRMovieIntCallBack aFinishCB,PTRMovieCallBack aMovieCacheOverCB);
	void set_audio_callback(PTRMovieAudioCallBack aAudioCallback);
	void uninit();
	bool play(const char* a_movie_file,const char* a_subtitle_file,int a_start_pos);
	int update(double a_current_time,int* subtitle_state,char* subtitle_str);
	int update(double a_current_time);
	lvr_texture* get_texture();
	bool is_playing();
	bool is_hardware_decode();
	void pause();
	void resume();
	void stop();
	//in second
	void seek(int a_seek_pos);
	int get_position();
	int get_duration();
	int get_width();
	int get_height();
	virtual media_state get_media_state();
private:
	PTRMovieIntCallBack _pFunMovieFinish;
	PTRMovieCallBack _pFunMovieCacheOver;
	PTRMovieAudioCallBack _pFunMovieAudio;
	lvr_texture2d_new		_texture;
	Player*			_player;
	media_state		_media_state;
	bool		_is_playing;
	bool		_is_caching;
	double		_last_time;
	double		_cur_farme_time;

	lvr_texture2d*	_texture_bg;
};

#endif

#endif
