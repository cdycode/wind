#ifndef __lvr_test_movie_texture_h__
#define __lvr_test_movie_texture_h__

#include "lvr_movie_configure.h"
#include "lvr_render_texture.h"
#include "lvr_media_interface.h"
class LVR_MOVIE_API movie_texture : public lvr_media_interface
{
public:
	movie_texture();
	~movie_texture();
public:
	bool play(const char* a_movie_file, const char* a_subtitle_file, int a_start_pos);
	int update(double a_current_time);
	lvr_texture* get_texture();
	bool is_playing();

	void pause();
	void resume();
	void stop();
	//in second
	void seek(int a_seek_pos);
	int get_position();
	int get_duration();
	virtual int get_width();
	virtual int get_height();
	virtual lvr_media_interface::media_state get_media_state();
private:
	void set_up_vbo();
	void set_up_shader();
	void set_up_texture();
private:
	FILE* _movie_file;
	media_state _media_state;
	lvr_program*		_program;
	uint32_t			tex_y, tex_u, tex_v;
	uint32_t textureUniformY, textureUniformU, textureUniformV;

	int		_pixel_width,_pixel_height;
	lvr_render_texture*	_render_texture;

	//vbo
	uint32_t	_vbo;

	bool		_is_playing;
	double		_last_time;
	double		_cur_farme_time;

};

#endif
