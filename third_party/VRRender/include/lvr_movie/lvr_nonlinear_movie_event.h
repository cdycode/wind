#ifndef __lvr_nonlinear_movie_event_h__
#define __lvr_nonlinear_movie_event_h__

#include "lvr_movie_configure.h"
#include <string>
#include "lvr_media_interface.h"
#include "lvr_hot_point.h"
class lvr_nonlinear_movie_event :public lvr_ref
{
public:
	enum type
	{
		movie_jump = 0x10000000,
		movie_wait_for_choice,
		movie_info,
		movie_return,
		movie_comment
	};
	enum flag
	{
		flag_return_to_ui = 0x01,
		flag_pause = 0x02,
		flag_user_ui_define = 0x04,
		flag_background = 0x08,
		flag_loop = 0x10,
		flag_video_ui = 0x20
	};
public:
	lvr_nonlinear_movie_event();
	~lvr_nonlinear_movie_event();
	static bool	get_position_coord_info(float ax, float ay,lvr_vector3f* ao_ray_look,lvr_vector3f* ao_right,lvr_vector3f* ao_up);
public:
	int		_frame_id;
	type	_event_type;
	std::string	_title;
	std::string _description;

};
class lvr_nonlinear_movie_jump : public lvr_nonlinear_movie_event
{
public:
	lvr_nonlinear_movie_jump();
	~lvr_nonlinear_movie_jump();
public:
	int		_target_frame_id;
	int			_jump_flag;//0 表示视频内跳转 1表示视频外跳转
	std::string	_movie_jump;
};

class lvr_nonlinear_movie_return_focus : public lvr_nonlinear_movie_event
{
public:
	lvr_nonlinear_movie_return_focus();
	~lvr_nonlinear_movie_return_focus();
};

typedef struct 
{
	float xpos, ypos, xsize, ysize;
	std::string background_texture_name;
}lvr_nonlinear_ui_info;

#define NONLINEAR_MOVIE_MAX_CHOICE_NUM 3
class lvr_nonlinear_movie_choice : public lvr_nonlinear_movie_event
{
public:
	lvr_nonlinear_movie_choice();
	~lvr_nonlinear_movie_choice();
	void cal_yaw_value();
public:
	int			_choice_num;
	int			_choice_target_frame_id[NONLINEAR_MOVIE_MAX_CHOICE_NUM];
	int32_t		_flags;
	int32_t		_end_frame_id;
	int32_t		_default_jump_id;
	std::string	_choice[NONLINEAR_MOVIE_MAX_CHOICE_NUM];
	lvr_nonlinear_ui_info	_ui_info[NONLINEAR_MOVIE_MAX_CHOICE_NUM + 2];
	float		_return_yaw_value;//calculate from ui_info,get the center x,and than calculate the yaw value.
	int			_choice_flag[NONLINEAR_MOVIE_MAX_CHOICE_NUM];//0 表示视频内跳转 1表示视频外跳转
	std::string	_choice_jump[NONLINEAR_MOVIE_MAX_CHOICE_NUM];
};

class lvr_nonlinear_movie_info : public lvr_nonlinear_movie_event
{
public:
	lvr_nonlinear_movie_info();
	~lvr_nonlinear_movie_info();
	void update_ray();
public:
	int				_end_frame_id;
#define _FLAG_NONE		0x00	//正常
#define _FLAG_PAUSE	0x01	//暂停
#define _FLAG_JUMP		0x02	//跳转视频
	int				_flags;
	int				_jump_frame_id;
	float			_xpos;
	float			_ypos;
	float			_xpos_effect_edge;
	lvr_vector3f	_look_up_ray;
	std::string		_ok_string;
	std::string		_jump_movie_info;
	lvr_hot_point	_hot_point;
	float			_radius_cos_angle;
	int				_temp_id;
	int				_loop_flag;
	float			_x_size;
	float			_y_size;
	std::string		_pic_filename;
};

class lvr_nonlinear_movie_comment : public lvr_nonlinear_movie_event
{
public:
	lvr_nonlinear_movie_comment();
	~lvr_nonlinear_movie_comment();
	void update_ray();
public:
	int				_end_frame_id;
	float			_xpos;
	float			_ypos;
	float			_xsize;
	float			_ysize;
	float			_xpos_effect_edge;
	lvr_vector3f	_look_up_ray;
	lvr_vector3f	_right;
	lvr_vector3f	_up;
};

// class lvr_nonlinear_movie_subtitle : public lvr_nonlinear_movie_event
// {
// public:
// 	lvr_nonlinear_movie_subtitle();
// 	~lvr_nonlinear_movie_subtitle();
// public:
// 	int				_end_frame_id;
// 	float			_tilt_angle;
// 	std::string		_subtitle_str;
// };


#endif
