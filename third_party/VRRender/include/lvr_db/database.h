//
//  database.h
//  URLegend
//
//  Created by afei on 14-2-18.
//
//

#ifndef __URLegend__UserDataManager__
#define __URLegend__UserDataManager__

//#include "cocos2d.h"
#include "Sqlite3WithKey.h"
class DataBase {
    
    
protected:
    
    bool        isKeyExsit(const char* tablename, const char* columname, const char* key);
    void        deleteKey(const char* tablename,const char* columname,const char* key);
    void        setStringForKey(const char* tablename, const char* keyc, const char* valuec, const char* key, const char* value);
    
    std::vector<std::string> getStringForKeyVecs(const char* tablename, const char* keyc, const char* valuec, std::string keys[], int nkeys);
    std::string getStringForKey(const char* tablename, const char* keyc, const char* valuec, const char* key);
    float       getFloatForKey(const char* tablename,  const char* keyc, const char* valuec, const char* key);
    int         getIntForKey(const char* tablename,    const char* keyc, const char* valuec, const char* key);
    bool        getBoolForKey(const char* tablename,   const char* keyc, const char* valuec, const char* key);
    
    std::vector<std::string> getStringForKeyVecs(const char* tablename, const char* keyc1, const char* valuec1, const char* keyc2, const char* valuec2, std::string keys[], int nkeys);
    std::string getStringForKey(const char* tablename, const char* keyc1, const char* valuec1, const char* keyc2, const char* valuec2, const char* key);
    float       getFloatForKey( const char* tablename, const char* keyc1, const char* valuec1, const char* keyc2, const char* valuec2, const char* key);
    int         getIntForKey(const char* tablename,    const char* keyc1, const char* valuec1, const char* keyc2, const char* valuec2, const char* key);
    bool        getBoolForKey(const char* tablename,   const char* keyc1, const char* valuec1, const char* keyc2, const char* valuec2, const char* key);
public:
    Sqlite3WithKey* getSqliteWith(){
        return &_sqlitewithkey;
    };
protected:
    std::vector<std::string> getStringForKeyVecs(const char* tablename, std::string keys[], int nkeys);
protected:
    Sqlite3WithKey _sqlitewithkey;
    
};

#endif /* defined(__URLegend__UserDataManager__) */
