//
//  helpfunctions.h
//  helpLayer
//
//  Created by afei on 13-10-30.
//
//

#ifndef helpLayer_helpfunctions_h
#define helpLayer_helpfunctions_h

#ifndef CCLOG
#define CCLOG do while(0){};
#endif

#define pos(x,y) (Point(x, y) + VisibleRect::leftBottom())

#define isPad (fabs(m_pos_rtop.x/m_pos_rtop.y-1024.0/768.0)<0.001)
#define isiPhone4 (fabs(m_pos_rtop.x/m_pos_rtop.y-960./640.)<0.001)
#define isiPhone5 (fabs(m_pos_rtop.x/m_pos_rtop.y-1136./640.)<0.001)

#define CCLOG_POINT(p) CCLOG("x: %f, y: %f",(p).x,(p).y);
#define CCLOG_POINT_NAME(p,name) CCLOG("%s x: %f, y: %f",name,(p).x,(p).y);

#define CCLOG_SIZE(s) CCLOG("width:%f,height:%f",s.width,s.height);
#define CCLOG_SIZE_NAME(s,name) CCLOG("%s width:%f,height:%f",name,s.width,s.height);

#define CCLOG_RECT(r) CCLOG("x:%f,y:%f,width:%f,height:%f",r.origin.x,r.origin.y,r.size.width,r.size.height);
#define CCLOG_RECT_NAME(s,name) CCLOG("%s x:%f,y:%f,width:%f,height:%f",name,r.origin.x,r.origin.y,r.size.width,r.size.height);

#define CCLOG_FLOAT(f) CCLOG("%f",f);
#define CCLOG_FLOAT_NAME(f,name) CCLOG("%s:%f",name,f);

#define CCLOG_INT(d) CCLOG("%d",d);
#define CCLOG_INT_NAME(d,name) CCLOG("%s:%d",name,d);

#define CCLOG_LONG(d) CCLOG("%ld",d);
#define CCLOG_LONG_NAME(d,name) CCLOG("%s:%ld",name,d);

//#define FONT_NAME "STYuanti-SC-Regular.ttf"
#define FONT_NAME "ArialRoundedMTBold.ttf"


#define MISSFILE "icon_h_ro_dy.png"

#define MEARSURE_DURATION(fun) CFunctionDuration fun(std::string(std::string(__FUNCTION__) + " " +std::string(#fun)).c_str() );


#define PROPERTY(type, value) private: type value;\
public:\
inline void set##value(type v){ value = v;}\
inline type get##value(){return value;}

#include <time.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <map>
#include <thread>
#include <mutex>

//#ifdef WIN32
//
//#include <objbase.h>
//
//#else
//
//#include <uuid/uuid.h>
//#endif

#define TYPE_COIN     "coin"
#define TYPE_EQUIP    "equip"
#define TYPE_WEAPON   "weapon"
#define TYPE_STONE    "stone"
#define TYPE_FRAG     "frag"
#define TYPE_MATER    "mater"
#define TYPE_HERO     "hero"
#define TYPE_CONSUME  "consume"

#define str_key_value(key,value,sep) (((value) == 0)?"":((key)+std::string(":")+convert<string>(value)+(sep)))


const float k_atk_dis = 0.6f;
class CFunctionDuration {
    
public:
    CFunctionDuration(const char* funname)
    {
        m_start_df = clock();
        sprintf(m_funname, "%s",funname);
    }
    ~CFunctionDuration()
    {
        double du = (clock() - m_start_df)/CLOCKS_PER_SEC;
        if (du >= 0) {
 //           LVR_LOG("%s running duration:%f(ms)",m_funname,du * 1000.0);
//            printf("%s running duration:%f(ms) \n",m_funname,du);
        }
    }
    
private:
    double m_start_df;
    char m_funname[256];
};

class CLockMutex{
	std::mutex *_lock;
public:
	CLockMutex(std::mutex *lock) :_lock(lock){
		_lock->lock();
    };
    ~CLockMutex(){
		_lock->unlock();
    };
private:
    CLockMutex(const CLockMutex &other){};
    void operator=(const CLockMutex &other){};
};
bool isLuckEnough(int percent);

template <class T>
void getNumFromString(std::string str, T &x, T &y) {

    std::stringstream streamstr;
//    streamstr.str("");
    size_t loc1 =  str.find('{');
    if(loc1 != std::string::npos)
    {
        str = str.replace(loc1,1,"");
    }
    loc1 = str.find('}');
    if (loc1 != std::string::npos) {
        str = str.replace(loc1,1,"");
    }
    loc1 = str.find(',');
    if (loc1 != std::string::npos) {
        str = str.replace(loc1,1," ");
    }
    streamstr<<str;
    streamstr>>x;
    streamstr>>y;
    streamstr.str("");
}
/*/
 std::string nuustr = "{200., 100.0, 1.0, 200.1}";
  std::string nuustr = "200., 100.0, 1.0, 200.1";
  std::string nuustr = "200.,100.0,1.0,200.1}";
  std::string nuustr = "{200., 100.0, 1.0, 200.1";
 std::vector<float> color ;
 getNumFromString(nuustr,&color);
 for (int i = 0; i < color.size(); i ++) {
 CCLOG_FLOAT(color[i]);
 }
*///

bool isStrValid(const std::string& str);

template <class T>
bool getNumFromString(std::string str, std::vector<T> *vec) {
    
    std::string num="";
    int numdot = 0;
    std::stringstream strs(num);
    for (int i = 0; i < str.length() + 1; i ++) {
        if (str[i]<='9' && str[i] >= '0' ) {
            num = num + str[i];
//            num.append(&str[i]);
        }
        else if(str[i]=='.' && numdot == 0 && num.length() > 0)
        {
            numdot ++;
            num = num + str[i];
        }
        else if(num.length() > 0)
        {
            T data;
            strs.str(num);
            strs>>data;
            vec->push_back(data);
            numdot = 0;
            num.clear();
            strs.clear();
        }
    }
    strs.str("");
    return true;
}
void saveDataToFile(unsigned char* data,unsigned long size, const char* filepath);
std::string getTimeNowHMS();
std::string getTimeHMS(unsigned long seccond);
double getCurrentTime();
long getCurrentTime_us();
std::string getStrForKey(const std::string& str,const std::string& key);
std::string getStrForKey(const std::string& str,const std::string& key,int index);
int         getCounterForKey(const std::string& str,const std::string& key);
std::string formNumStr(const std::string& num);

long getMSSecondNow();
template <class T>
std::string thousandFormatNumber( const T &num ) {
    std::stringstream s;
    s.imbue(std::locale(""));
    s<<num;
    return s.str();
}
std::string thousandFormatNumber();
template <class T>
void pushvectorifnotcontain(std::vector<T*>*vec,const T* item)
{
    if (item != nullptr) {
        for (int i = 0; i < vec->size(); i ++) {
            if(item == vec->at(i))
            {
                return;
            }
        }
        vec->push_back(item);
    }
}

bool isContainString(const std::vector<std::string>& vec, const std::string &str);
bool isMapContainKey(const std::map<std::string,std::string> &m, const std::string &key);
template <class T>
void eraseFromvector( std::vector<T*> *vec,const T *obj) {
    for (int i = 0; i < vec->size(); i ++) {
        if (vec->at(i) == obj) {
            vec->erase(vec->begin() + i);
            return;
        }
    }
}

template <class T>
void eraseFromvector( std::vector<T*> *vec,const int index) {
    vec->erase(vec->begin() + index);
}
inline size_t LenCN(){
    const static std::string str("��");
    return str.size();
}

std::vector<std::string> strSplit(const std::string &storg, const std::string& sepatator);
//std::string GetGUID();
std::string strInsert(const std::string& strSou, const size_t &len, const std::string &strIn);

//
//int atoi(const std::string& str);
//double atof(const std::string& str);

template <class typeout, class typein>
typeout convert(const typein &d) {
    
    static std::stringstream str;
    str.clear();
    str.str("");
    str<<d;
    typeout val;
    str>>val;
    return val;
}

template <class typeout>
typeout convert_Str(const std::string &d) {
    
	static std::mutex lock;
	std::unique_lock <std::mutex> lck(lock);
    static std::stringstream str;
    
    
    if (d.length() == 0) {
        return 0;
    }else{
        str.clear();
        str.str(d);
        typeout val;
        str>>val;
        return val;
    }
}

//0~360
float getAngle(float x, float y);
inline float  DEG_2_RATIO(float d){
    return ((d)/180.0f*3.14159265f);
}
inline float  RATIO_2_DEG(float d){
    return ((d)*180.0f/3.14159265f);
}
#endif
