//
//  Sqlite3WithKey.h
//  URDemo
//
//  Created by afei on 13-12-24.
//
//

#ifndef __URDemo__Sqlite3WithKey__
#define __URDemo__Sqlite3WithKey__
#define SQLITE_HAS_CODEC 1

#include "sqlite3.h"
#include <string>
#include <vector>

struct sqlite3;

class Sqlite3WithKey {
    
    
public:
    Sqlite3WithKey();
    ~Sqlite3WithKey();
//    static Sqlite3WithKey* getInstance();
    
    /**
     *  打开数据库文件，路径必须正确；若文件不正确，则创建文件
     *
     *  @param filepath 完成路径名
     *  @param key      数据库密码
     *
     *  @return 创建失败返回false
     *
     *  @since 2013-12-24
     */
    bool open(const char* filepath, const char* key);
    
    /**
     *  若果数据库没有密码，则设置密码为key；如果有密码则改为key;必须在紧跟open之后调用
     *
     *  @param key 数据库密码
     *
     *  @since 2013-12-24
     */
    void setKey(const char* key);
    
    
    
public:
    /**
     *  如果数据库已打开则关闭
     *
     *  @since 2013-12-24
     */
    void closeDB();
    
protected:
    void init();
    
    /**
     *  执行sql语句，执行完成之后sqlite3库会调用 int receiveData(void * para, int n_column, char ** column_value, char **column_name )
     *
     *  @since 2013-12-24
     */
    bool excute();
    /**
     *  sqlite3执行完sql命令之后的回调函数
     *
     *  @param para         在sqlite3_exec中传递的参数
     *  @param n_column     这一条记录有多少个字段
     *  @param column_value 返回的查询数据，是一维数组；
     *  @param column_name  各个字段对应的名字
     *
     *  @return 返回0
     *
     *  @since  2013-12-24
     */
    static int receiveData(void * para, int n_column, char ** column_value, char **column_name );
    
public:
    bool getTableColums(const char* tablename,std::vector<std::string> *values);
    
    bool queryData(const char* tablename, const std::string keysc[],const std::string valuesc[],int counter, std::string keyt[],int counter1, std::vector<std::string> *values);
    
    bool updateData(const char* tablename, const std::string keysc[],const std::string valuesc[],int counter,const std::string keyt[],const std::string valuet[],int counter1);
    
    void insertRow(const char* tablename, const std::string keysc[],const std::string valuesc[],int cols);
    
    void insertRow(const char* tablename, const std::vector<std::string> &keysc,const std::vector<std::string> &valuesc);
    
    void insertOrUpdate(const char* tablename, const std::string keysc[],const std::string valuesc[],int cols);
    
    void insertOrUpdate(const char* tablename, const std::string keysc[],const std::string valuesc[],int cols,const std::string keyt[],const std::string valuet[],int counter1);
    
    bool isexsitsData(const char* tablename, const std::string keysc[],const std::string valuesc[],int cols);
    
    bool createTable(const char* tablename,const std::string keysc[], const std::string types[], int counter, const std::string primarykey[], int counterp);
    
    void deleteTable(const char* tablename);
    
    void deleteData(const char* tablename, const std::string keysc[],const std::string valuesc[],int cols);
    
    void querySql(const char*  sql,sqlite3_callback xCallback = nullptr);
    
    std::vector<std::vector<std::string>> getTableDatas(const std::string tablename);
    bool getTable(const std::string& tablename, std::vector<std::vector<std::string>> *table);
    
    std::string getErrorMess(){
        return _error_mess;
    };
    void clearErrorMess(){
        _error_mess.clear();
    };
    //
    bool insertOtherDB(const char* dnpath, const char* dbkey);
    void start_query();
    void end_query();
    double getTimeTaken(){return _duration;};
    int  getCounter(){return _count;};
    void cleanTimeCounter(){_count = 0; _duration = 0;}
private:
    /**
     *  数据库指针
     *
     *  @since 2013-12-24
     */
    sqlite3 * _db;
    
    
    std::string _str_sql;
    
    
    bool _cansetkey;
    
    std::string _error_mess;
    
    int _count;
    double _duration;
    double _time_start;
    
    class timecounter{
    public:
        Sqlite3WithKey *_p;
        timecounter(Sqlite3WithKey* p){
            _p = p;
            _p->start_query();
        }
        ~timecounter(){
            _p->end_query();
            _p = nullptr;
        }
    };
#define TIME_COUNTER() 
    // timecounter tssxxx(this)
};


#endif /* defined(__URDemo__Sqlite3WithKey__) */
