//
//  DBManger.h
//  URLengend
//
//  Created by afei on 14-4-11.
//
//

#ifndef __URLengend__DBManger__
#define __URLengend__DBManger__

#include <string>
#include <vector>
#include "database.h"


using namespace std;



class DBManager:public DataBase {
    
    static DBManager *g_dbmanager;
    class AutoDealloc
    {
        ~AutoDealloc()
        {
            if (g_dbmanager != nullptr) {
                delete g_dbmanager;
                g_dbmanager = nullptr;
            }
        }
    };
    static AutoDealloc autodeallloc;
public:

public:
    void init();
    
    static DBManager* getInstance();

    void set_user_data_for_key(const string& data, const string& key);
    string get_user_data_for_key(const string& key);
    int    get_user_data_for_key_i(const string& key);
    float  get_user_data_for_key_f(const string& key);
    void   set_volume(int volume);
    int    get_volume();
    void   set_light(int light);
    int    get_light();
public:

//#endif
private:

    const char* TABLE_USER_DATA = "user_data";


    const char* USER_DATA_KEY   = "key";
    const char* USER_DATA_DATA  = "data";
    const char* USER_KEY_VOLUME = "volume";
    const char* USER_KEY_LIGHT  = "light";
    void init_user_table();

    const char* USER_KEY_VERSION  = "version";
    void init_version();
    float get_version();

};


#endif /* defined(__URLengend__DBManger__) */
