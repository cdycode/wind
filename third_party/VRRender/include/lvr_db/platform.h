//
// Created by sunpengfei on 16/6/2.
//

#ifndef CINEMAVR_PLATFORM_H
#define CINEMAVR_PLATFORM_H

#include "lvr_singleton.h"
#include <string>
using namespace std;

class platform {
    platform(){

    };

public:
    static platform* get_ins(){
        static platform instance;
        return &instance;
    };
    ~platform(){};

    string get_path_for_write();

    string get_package_name();

    string encode_des_base64(const string& str);
    string decode_base64_des(const string& str);
};


#endif //CINEMAVR_PLATFORM_H



