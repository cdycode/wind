//
// Created by sunpengfei on 16/7/1.
//

#ifndef CINEMAVR_URLENCODE_H
#define CINEMAVR_URLENCODE_H

#include <string>
using namespace std;
void urlencode( unsigned char * src, int  src_len, string* dest);
unsigned char* urldecode(unsigned char* encd,unsigned char* decd);
#endif //CINEMAVR_URLENCODE_H
