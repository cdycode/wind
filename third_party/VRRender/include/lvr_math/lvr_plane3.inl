template<class T>
lvr_plane3<T>::lvr_plane3() :_normal(T(0),T(0),T(0)),_distance_to_origin(T(0))
{

}


template<class T>

lvr_plane3<T>::lvr_plane3(const lvr_vector3<T> a_normal, T a_dist_to_origin) :_normal(a_normal), _distance_to_origin(a_dist_to_origin)
{

}




//Here I added two arguments of the constructor functions.



template<class T>
const lvr_vector3<T>& lvr_plane3<T>::get_normal() const
{
	return _normal;
}

template<class T>
void lvr_plane3<T>::set_normal(const lvr_vector3<T>& a_normal)
{
	_normal = a_normal;
}

template<class T>
T lvr_plane3<T>::get_distance_to_origin() const
{
	return _distance_to_origin;
}

template<class T>
void lvr_plane3<T>::set_distance_to_origin(T a_v)
{
	_distance_to_origin = a_v;
}

template<class T>
T lvr_plane3<T>::distance_to_point(const lvr_vector3<T>& a_point) const
{
	return _normal*a_point + _distance_to_origin;
}

template<class T>
bool lvr_plane3<T>::project_vector(lvr_vector3<T>& ao_dst_vector,const lvr_vector3<T>& a_src_vector) const
{
	lvr_vector3<T> t_vec(a_src_vector);
	t_vec.normalize();
	if (fabs(t_vec*_normal) > T(1)-LVR_TOLERANCE)
	{
		return false;
	}
	else
	{
		ao_dst_vector = a_src_vector - _normal*(a_src_vector*_normal);
	}
	return true;
}

template<class T>
bool lvr_plane3<T>::intersect(T& ao_dist,const lvr_vector3<T> a_ray_origin,const lvr_vector3<T> a_ray_dir) const
{
	T d = a_ray_dir*_normal;
	T t = a_ray_origin * _normal;
	if (fabs(d) < LVR_TOLERANCE)
	{
		if (fabs(t + _distance_to_origin) < LVR_TOLERANCE)
		{
			ao_dist = 0;
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		ao_dist = (_distance_to_origin - t)/d;
		return true;
	}
}
