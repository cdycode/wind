
template<class T>
bool lvr_ray_triangle_intersect(T& ao_dist,const lvr_vector3<T>& a_origin,const lvr_vector3<T>& a_dir, const lvr_vector3<T>& a_p0,const lvr_vector3<T>& a_p1,const lvr_vector3<T>& a_p2)
{
	static const double t_eps = 1e-20f;
	lvr_vector3<T> e1 = a_p1 - a_p0;
	lvr_vector3<T> e2 = a_p2 - a_p0;
	lvr_vector3<T> p = a_dir.cross(e2);
	double det = e1*p;
	double u,v,t;
	if (det > t_eps)
	{
		lvr_vector3<T> s = a_origin - a_p0;
		u = s*p;
		if ((u < 0) || (u>det))
		{
			return false;
		}
		lvr_vector3<T> q = s.cross(e1);
		v = a_dir*q;
		if ((v < 0) || ( u+v > det))
		{
			return false;
		}
		t = e2*q;
		if (t < 0)
		{
			return false;
		}

	}
	else if (det < -t_eps)
	{
		lvr_vector3<T> s = a_origin - a_p0;
		u = s*p;
		if ((u > 0.0) || (u<det))
		{
			return false;
		}
		lvr_vector3<T> q = s.cross(e1);
		v = a_dir*q;
		if ((v > 0) || ( u+v < det))
		{
			return false;
		}
		t = e2*q;
		if (t > 0)
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	t *= T(1.0/det);
	ao_dist = T(t);
	return true;
}

template<class T>
bool lvr_get_circumcircle_center(lvr_vector2<T>& ao_center,const lvr_vector2<T>& a_pa, const lvr_vector2<T>& a_pb,const lvr_vector2<T>& a_pc)
{
	double a = a_pa[0]*a_pa[0] + a_pa[1]*a_pa[1];
	double b = a_pb[0]*a_pb[0] + a_pb[1]*a_pb[1];
	double c = a_pc[0]*a_pc[0] + a_pc[1]*a_pc[1];
	double g = (a_pc[1] - a_pb[1])*a_pa[0] + (a_pa[1] - a_pc[1])*a_pb[0] + (a_pb[1]-a_pa[1])*a_pc[0];
	if (fabs(g) > 1e-6)
	{
		ao_center[0] = (T)(((b-c)*a_pa[1] + (c-a)*a_pb[1] + (a-b)*a_pc[1])/(2.0*g));
		ao_center[1] = (T)(((c - b)*a_pa[0] + (a - c)*a_pb[0] + (b - a)*a_pc[0]) / (2.0*g));
		return true;
	}
	return false;
}

template<class T>
bool lvr_get_circumcircle_center(lvr_vector3<T>& ao_center,const lvr_vector3<T>& a_pa, const lvr_vector3<T>& a_pb,const lvr_vector3<T>& a_pc)
{
	lvr_vector3<T> tmporigin = (a_pa + a_pb + a_pc)*T(1.0/3.0);
	lvr_vector3<T> xcoord = a_pa - a_pb;
	lvr_vector3<T> ycoord = a_pa - a_pc;
	lvr_vector3<T> normal = xcoord.cross(ycoord);
	if (normal.length() < LVR_TOLERANCE)//collinear
	{
		return false;
	}
	ycoord = normal.cross(xcoord);
	xcoord = ycoord.cross(normal);
	xcoord.normalize();
	ycoord.normalize();
	lvr_vector2<T> a2(a_pa*xcoord,a_pa*ycoord);
	lvr_vector2<T> b2(a_pb*xcoord,a_pb*ycoord);
	lvr_vector2<T> c2(a_pc*xcoord,a_pc*ycoord);
	lvr_vector2<T> t_c(T(0));
	if(lvr_get_circumcircle_center(a2,b2,c2,t_c))
	{
		ao_center = xcoord*t_c[0] + ycoord * t_c[1];
		return true;
	}
	return false;
}

template<typename T>
T lvr_angle_fromxy(T x, T y)
{
	T theta = T(0.0);

	// Quadrant I or IV
	if (x >= T(0.0))
	{
		// If x = 0, then atanf(y/x) = +pi/2 if y > 0
		//                atanf(y/x) = -pi/2 if y < 0
		theta = T(atan((double)y / (double)x)); // in [-pi/2, +pi/2]

		if (theta < T(0.0))
			theta += T(2.0*LVR_PI); // in [0, 2*pi).
	}

	// Quadrant II or III
	else
		theta = T(atan((double)y / (double)x) + LVR_PI); // in [0, 2*pi).

	return theta;

}