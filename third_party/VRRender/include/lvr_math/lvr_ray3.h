#ifndef __lvr_ray3_h__
#define __lvr_ray3_h__

#include"lvr_vector3.h"
template<class T>
class lvr_ray3
{
public:
	lvr_ray3();
	lvr_ray3(const lvr_vector3<T>& a_origin,const lvr_vector3<T>& a_dir);
	~lvr_ray3(){}
public:
	lvr_vector3<T> get_origin()const;
	lvr_vector3<T> get_dir()const;
	void set_origin(const lvr_vector3<T>& a_origin);
	void set_dir(const lvr_vector3<T>& a_dir);
private:
	lvr_vector3<T>	_origin_point;
	lvr_vector3<T>	_dir_look;
};

#include "lvr_ray3.inl"

typedef lvr_ray3<float> lvr_ray3f;
typedef lvr_ray3<double> lvr_ray3d;
#endif
