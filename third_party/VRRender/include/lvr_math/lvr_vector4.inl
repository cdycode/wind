#include "lvr_vector4.h"

template<class T>
T& lvr_vector4<T>::operator[](int a_id)
{
	LVR_ASSERT(a_id < 4 && a_id >= 0);
	return _m[a_id];
}

template<class T>
const T& lvr_vector4<T>::operator[](int a_id) const
{
	LVR_ASSERT(a_id < 4 && a_id >= 0);
	return _m[a_id];
}

template<class T>
lvr_vector4<T> lvr_vector4<T>::operator+(const lvr_vector4& a_v4) const
{
	return lvr_vector4<T>(_x + a_v4[0],_y + a_v4[1],_z + a_v4[2],_w + a_v4[3]);
}

template<class T>
lvr_vector4<T> lvr_vector4<T>::operator-(const lvr_vector4& a_v4) const
{
	return lvr_vector4<T>(_x - a_v4[0],_y - a_v4[1],_z - a_v4[2],_w - a_v4[3]);
}

template<class T>
T lvr_vector4<T>::operator*(const lvr_vector4& a_v4) const
{
	return _x*a_v4[0] + _y*a_v4[1] + _z*a_v4[2] + _w*a_v4[3];
}

template<class T>
lvr_vector4<T> lvr_vector4<T>::operator*(T a_scale) const
{
	return lvr_vector4<T>(_x*a_scale,_y*a_scale,_z*a_scale,_w*a_scale);
}

template<class T>
lvr_vector4<T> lvr_vector4<T>::operator/(T a_scale) const
{
	LVR_ASSERT(a_scale > LVR_TOLERANCE);
	return lvr_vector4<T>(_x/a_scale,_y/a_scale,_z/a_scale,_w/a_scale);
}

template<class T>
void lvr_vector4<T>::operator+=(const lvr_vector4& a_v4)
{
	_x += a_v4[0];
	_y += a_v4[1];
	_z += a_v4[2];
	_w += a_v4[3];
}

template<class T>
void lvr_vector4<T>::operator-=(const lvr_vector4& a_v4)
{
	_x -= a_v4[0];
	_y -= a_v4[1];
	_z -= a_v4[2];
	_w -= a_v4[3];
}

template<class T>
void lvr_vector4<T>::operator*=(T a_scale)
{
	_x *= a_scale;
	_y *= a_scale;
	_z *= a_scale;
	_w *= a_scale;
}

template<class T>
void lvr_vector4<T>::operator/=(T a_scale)
{
	_x /= a_scale;
	_y /= a_scale;
	_z /= a_scale;
	_w /= a_scale;
}

template<class T>
bool lvr_vector4<T>::operator<(const lvr_vector4<T>& a_v2) const
{
	if (_x < a_v2[0])
	{
		return true;
	}
	else if (_x==a_v2[0])
	{
		if (_y<a_v2[1])
		{
			return true;
		}
		else if (_y == a_v2[1] )
		{
			if (_z < a_v2[2])
			{
				return true;
			}
			else if(_z == a_v2[2] && _w < a_v2[3])
			{
				return true;
			}
		}
	}
	return false;
}

template<class T>
lvr_vector3<T> lvr_vector4<T>::xyz() const
{
	return lvr_vector3<T>(_x,_y,_z);
}

template<class T>
T lvr_vector4<T>::length() const
{
	return sqrt(length_sq());
}

template<class T>
T lvr_vector4<T>::length_sq() const
{
	return _x*_x + _y*_y + _z*_z + _w*_w;
}

template<class T>
void lvr_vector4<T>::normalize()
{
	T t_l = length();
	LVR_ASSERT(t_l > LVR_TOLERANCE);
	*this /= t_l;
}

template<class T>
lvr_vector4<T> lvr_vector4<T>::get_normalize() const
{
	T t_l = length();
	LVR_ASSERT(t_l > LVR_TOLERANCE);
	return lvr_vector4<T>(_x/t_l,_y/t_l,_z/t_l,_w/t_l);
}

template<class T>
lvr_vector4<T> lvr_vector4<T>::cross(const lvr_vector4& a_v4) const
{
	return lvr_vector4<T>(_y*a_v4[2] - _z*a_v4[1],
		_z*a_v4[0] - _x*a_v4[2],
		_x*a_v4[1] - _y*a_v4[0],0);
}

template<class T>
lvr_vector4<T> lvr_vector4<T>::multi(const lvr_vector4<T>& a_v4) const
{
	return lvr_vector4<T>(_m[0]*a_v4[0], _m[1]*a_v4[1], _m[2]*a_v4[2], _m[3]*a_v4[3]);
}

template<class T>
lvr_vector4<T> lvr_vector4<T>::lerp(const lvr_vector4& a_v4,T a_pos) const
{
	return lvr_vector4<T>(_x*a_pos + a_v4[0] * (1.0 - a_pos), _y*a_pos + a_v4[1] * (1.0 - a_pos), _z*a_pos + a_v4[2] * (1.0 - a_pos), _w*a_pos + a_v4[3] * (1.0 - a_pos));
}

template<class T>
bool lvr_vector4<T>::is_nan() const
{
	return _x != _x || _y != _y || _z != _z || _w != _w;
}