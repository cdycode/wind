#include "lvr_quaternion.h"

template<class T>
lvr_matrix4<T>::lvr_matrix4()
	:_x(T(1),T(0),T(0),T(0)),_y(T(0),T(1),T(0),T(0)),_z(T(0),T(0),T(1),T(0)),_d(T(0),T(0),T(0),T(1))
{

}

template<class T>
lvr_matrix4<T>::lvr_matrix4(T a_v)
	:_x(a_v,T(0),T(0),T(0)),_y(T(0),a_v,T(0),T(0)),_z(T(0),T(0),a_v,T(0)),_d(T(0),T(0),T(0),a_v)
{

}

template<class T>
lvr_matrix4<T>::lvr_matrix4(const lvr_vector4<T>& a_v)
	:_x(a_v[0],T(0),T(0),T(0)),_y(T(0),a_v[1],T(0),T(0)),_z(T(0),T(0),a_v[2],T(0)),_d(T(0),T(0),T(0),a_v[3])
{

}

template<class T>
lvr_matrix4<T>::lvr_matrix4(const lvr_vector4<T>& a_v0,const lvr_vector4<T>& a_v1,const lvr_vector4<T>& a_v2,const lvr_vector4<T>& a_v3)
	:_x(a_v0),_y(a_v1),_z(a_v2),_d(a_v3)
{

}

template<class T>
lvr_matrix4<T>::lvr_matrix4(T a_m00,T a_m01,T a_m02,T a_m03,
	T a_m10,T a_m11,T a_m12,T a_m13,
	T a_m20,T a_m21,T a_m22,T a_m23,
	T a_m30,T a_m31,T a_m32,T a_m33)
	:_x(a_m00,a_m01,a_m02,a_m03),
	_y(a_m10,a_m11,a_m12,a_m13),
	_z(a_m20,a_m21,a_m22,a_m23),
	_d(a_m30,a_m31,a_m32,a_m33)
{

}

template<class T>
T& lvr_matrix4<T>::operator[](int a_id)
{
	LVR_ASSERT(a_id>=0 && a_id <= 15);
	return _m[a_id];
}

template<class T>
const T& lvr_matrix4<T>::operator[](int a_id) const
{
	LVR_ASSERT(a_id>=0 && a_id <= 15);
	return _m[a_id];
}

template<class T>
T& lvr_matrix4<T>::operator()(int a_row,int a_col)
{
	LVR_ASSERT(a_row >=0 && a_row<=3 && a_col>=0 && a_col<= 3);
	return _mm[a_row][a_col];
}

template<class T>
const T& lvr_matrix4<T>::operator()(int a_row,int a_col) const
{
	LVR_ASSERT(a_row >=0 && a_row<=3 && a_col>=0 && a_col<= 3);
	return _mm[a_row][a_col];
}

template<class T>
lvr_vector4<T> lvr_matrix4<T>::operator*(const lvr_vector4<T>& a_v4) const
{
	return lvr_vector4<T>(_x*a_v4,_y*a_v4,_z*a_v4,_d*a_v4);
}

template<class T>
lvr_vector3<T> lvr_matrix4<T>::operator*(const lvr_vector3<T>& a_vec3) const
{
	return lvr_vector3<T>(_mm[0][0]*a_vec3[0] + _mm[0][1]*a_vec3[1] + _mm[0][2]*a_vec3[2],
		_mm[1][0]*a_vec3[0] + _mm[1][1]*a_vec3[1] + _mm[1][2]*a_vec3[2],
		_mm[2][0]*a_vec3[0] + _mm[2][1]*a_vec3[1] + _mm[2][2]*a_vec3[2]);
}

template<class T>
lvr_matrix4<T> lvr_matrix4<T>::operator*(const T& a_s) const
{
	return lvr_matrix4<T>(_m[0]*a_s,_m[1]*a_s,_m[2]*a_s,_m[3]*a_s,
		_m[4]*a_s,_m[5]*a_s,_m[6]*a_s,_m[7]*a_s,
		_m[8]*a_s,_m[9]*a_s,_m[10]*a_s,_m[11]*a_s,
		_m[12]*a_s,_m[13]*a_s,_m[14]*a_s,_m[15]*a_s);
}

template<class T>
void lvr_matrix4<T>::operator*=(const T& a_s)
{
	_m[0] *= a_s;
	_m[1] *= a_s;
	_m[2] *= a_s;
	_m[3] *= a_s;
	_m[4] *= a_s;
	_m[5] *= a_s;
	_m[6] *= a_s;
	_m[7] *= a_s;
	_m[8] *= a_s;
	_m[9] *= a_s;
	_m[10] *= a_s;
	_m[11] *= a_s;
	_m[12] *= a_s;
	_m[13] *= a_s;
	_m[14] *= a_s;
	_m[15] *= a_s;
}

template<class T>
lvr_matrix4<T> lvr_matrix4<T>::operator/(const T& a_s) const
{
	return lvr_matrix4<T>(_m[0]/a_s,_m[1]/a_s,_m[2]/a_s,_m[3]/a_s,
		_m[4]/a_s,_m[5]/a_s,_m[6]/a_s,_m[7]/a_s,
		_m[8]/a_s,_m[9]/a_s,_m[10]/a_s,_m[11]/a_s,
		_m[12]/a_s,_m[13]/a_s,_m[14]/a_s,_m[15]/a_s);
}

template<class T>
void lvr_matrix4<T>::operator/=(const T& a_s)
{
	_m[0] /= a_s;
	_m[1] /= a_s;
	_m[2] /= a_s;
	_m[3] /= a_s;
	_m[4] /= a_s;
	_m[5] /= a_s;
	_m[6] /= a_s;
	_m[7] /= a_s;
	_m[8] /= a_s;
	_m[9] /= a_s;
	_m[10] /= a_s;
	_m[11] /= a_s;
	_m[12] /= a_s;
	_m[13] /= a_s;
	_m[14] /= a_s;
	_m[15] /= a_s;
}

template<class T>
lvr_matrix4<T> lvr_matrix4<T>::operator*(const lvr_matrix4<T>& a_mat4)const
{
	lvr_matrix4<T>	t_mat;
	for (int i=0;i<4;++i)
	{
		for (int j=0;j<4;++j)
		{
			t_mat(i,j) = _mm[i][0]*a_mat4(0,j) + _mm[i][1]*a_mat4(1,j) + _mm[i][2]*a_mat4(2,j) + _mm[i][3]*a_mat4(3,j);
		}
	}
	return t_mat;
}

template<class T>
lvr_matrix4<T> lvr_matrix4<T>::operator+(const lvr_matrix4<T>& a_mat4)const
{
	return lvr_matrix4<T>(_m[0] + a_mat4[0],_m[1] + a_mat4[1],_m[2] + a_mat4[2],_m[3] + a_mat4[3],
						_m[4] + a_mat4[4],_m[5] + a_mat4[5],_m[6] + a_mat4[6],_m[7] + a_mat4[7],
						_m[8] + a_mat4[8],_m[9] + a_mat4[9],_m[10] + a_mat4[10],_m[11] + a_mat4[11],
						_m[12] + a_mat4[12],_m[13] + a_mat4[13],_m[14] + a_mat4[14],_m[15] + a_mat4[15]);
}

template<class T>
void lvr_matrix4<T>::operator+=(const lvr_matrix4<T>& a_mat4)
{
	_m[0] += a_mat4[0];_m[1] += a_mat4[1];_m[2] += a_mat4[2];_m[3] += a_mat4[3];
	_m[4] += a_mat4[4];_m[5] += a_mat4[5];_m[6] += a_mat4[6];_m[7] += a_mat4[7];
	_m[8] += a_mat4[8];_m[9] += a_mat4[9];_m[10] += a_mat4[10];_m[11] += a_mat4[11];
	_m[12] += a_mat4[12];_m[13] += a_mat4[13];_m[14] += a_mat4[14];_m[15] += a_mat4[15];
}

template<class T>
lvr_matrix4<T> lvr_matrix4<T>::operator-(const lvr_matrix4<T>& a_mat4)const
{
	return lvr_matrix4<T>(_m[0] - a_mat4[0],_m[1] - a_mat4[1],_m[2] - a_mat4[2],_m[3] - a_mat4[3],
		_m[4] - a_mat4[4],_m[5] - a_mat4[5],_m[6] - a_mat4[6],_m[7] - a_mat4[7],
		_m[8] - a_mat4[8],_m[9] - a_mat4[9],_m[10] - a_mat4[10],_m[11] - a_mat4[11],
		_m[12] - a_mat4[12],_m[13] - a_mat4[13],_m[14] - a_mat4[14],_m[15] - a_mat4[15]);
}

template<class T>
void lvr_matrix4<T>::operator-=(const lvr_matrix4<T>& a_mat4)
{
	_m[0] -= a_mat4[0];_m[1] -= a_mat4[1];_m[2] -= a_mat4[2];_m[3] -= a_mat4[3];
	_m[4] -= a_mat4[4];_m[5] -= a_mat4[5];_m[6] -= a_mat4[6];_m[7] -= a_mat4[7];
	_m[8] -= a_mat4[8];_m[9] -= a_mat4[9];_m[10] -= a_mat4[10];_m[11] -= a_mat4[11];
	_m[12] -= a_mat4[12];_m[13] -= a_mat4[13];_m[14] -= a_mat4[14];_m[15] -= a_mat4[15];
}

template<class T>
lvr_vector3<T> lvr_matrix4<T>::transform_vector(const lvr_vector3<T>& a_vec3) const
{
	return lvr_vector3<T>(_mm[0][0]*a_vec3[0] + _mm[1][0]*a_vec3[1] + _mm[2][0]*a_vec3[2],
		_mm[0][1]*a_vec3[0] + _mm[1][1]*a_vec3[1] + _mm[2][1]*a_vec3[2],
		_mm[0][2]*a_vec3[0] + _mm[1][2]*a_vec3[1] + _mm[2][2]*a_vec3[2]);
}

template<class T>
lvr_vector3<T> lvr_matrix4<T>::transform_point(const lvr_vector3<T>& a_vec3) const
{
	lvr_vector4<T> t(_mm[0][0]*a_vec3[0] + _mm[1][0]*a_vec3[1] + _mm[2][0]*a_vec3[2] + _mm[3][0],
		_mm[0][1]*a_vec3[0] + _mm[1][1]*a_vec3[1] + _mm[2][1]*a_vec3[2] + _mm[3][1],
		_mm[0][2]*a_vec3[0] + _mm[1][2]*a_vec3[1] + _mm[2][2]*a_vec3[2] + _mm[3][2],
		_mm[0][3]*a_vec3[0] + _mm[1][3]*a_vec3[1] + _mm[2][3]*a_vec3[2] + _mm[3][3]);
	return lvr_vector3<T>(t[0]/t[3],t[1]/t[3],t[2]/t[3]);
}

template<class T>
T lvr_matrix4<T>::determinant() const
{
	return T(1);
}

template<class T>
bool lvr_matrix4<T>::inverse()
{
	T a0 = _m[0] * _m[5] - _m[1] * _m[4];
	T a1 = _m[0] * _m[6] - _m[2] * _m[4];
	T a2 = _m[0] * _m[7] - _m[3] * _m[4];
	T a3 = _m[1] * _m[6] - _m[2] * _m[5];
	T a4 = _m[1] * _m[7] - _m[3] * _m[5];
	T a5 = _m[2] * _m[7] - _m[3] * _m[6];
	T b0 = _m[8] * _m[13] - _m[9] * _m[12];
	T b1 = _m[8] * _m[14] - _m[10] * _m[12];
	T b2 = _m[8] * _m[15] - _m[11] * _m[12];
	T b3 = _m[9] * _m[14] - _m[10] * _m[13];
	T b4 = _m[9] * _m[15] - _m[11] * _m[13];
	T b5 = _m[10] * _m[15] - _m[11] * _m[14];

	// Calculate the determinant.
	T det = a0 * b5 - a1 * b4 + a2 * b3 + a3 * b2 - a4 * b1 + a5 * b0;

	if (fabs(det) < LVR_TOLERANCE)
	{
		return false;
	}
	T tm[16];
	memcpy(tm,_m,16*sizeof(T));
	T t_det_under_1 = (T(1)/det);
	_m[0]  = (tm[5] * b5 - tm[6] * b4 + tm[7] * b3)*t_det_under_1;
	_m[1]  = (-tm[1] * b5 + tm[2] * b4 - tm[3] * b3)*t_det_under_1;
	_m[2]  = (tm[13] * a5 - tm[14] * a4 + tm[15] * a3)*t_det_under_1;
	_m[3]  = (-tm[9] * a5 + tm[10] * a4 - tm[11] * a3)*t_det_under_1;

	_m[4]  = (-tm[4] * b5 + tm[6] * b2 - tm[7] * b1)*t_det_under_1;
	_m[5]  = (tm[0] * b5 - tm[2] * b2 + tm[3] * b1)*t_det_under_1;
	_m[6]  = (-tm[12] * a5 + tm[14] * a2 - tm[15] * a1)*t_det_under_1;
	_m[7]  = (tm[8] * a5 - tm[10] * a2 + tm[11] * a1)*t_det_under_1;

	_m[8]  = (tm[4] * b4 - tm[5] * b2 + tm[7] * b0)*t_det_under_1;
	_m[9]  = (-tm[0] * b4 + tm[1] * b2 - tm[3] * b0)*t_det_under_1;
	_m[10] = (tm[12] * a4 - tm[13] * a2 + tm[15] * a0)*t_det_under_1;
	_m[11] = (-tm[8] * a4 + tm[9] * a2 - tm[11] * a0)*t_det_under_1;

	_m[12] = (-tm[4] * b3 + tm[5] * b1 - tm[6] * b0)*t_det_under_1;
	_m[13] = (tm[0] * b3 - tm[1] * b1 + tm[2] * b0)*t_det_under_1;
	_m[14] = (-tm[12] * a3 + tm[13] * a1 - tm[14] * a0)*t_det_under_1;
	_m[15] = (tm[8] * a3 - tm[9] * a1 + tm[10] * a0)*t_det_under_1;
	return true;
}

template<class T>
bool lvr_matrix4<T>::get_inverse(lvr_matrix4<T>& ao_mat4) const
{
	T a0 = _m[0] * _m[5] - _m[1] * _m[4];
	T a1 = _m[0] * _m[6] - _m[2] * _m[4];
	T a2 = _m[0] * _m[7] - _m[3] * _m[4];
	T a3 = _m[1] * _m[6] - _m[2] * _m[5];
	T a4 = _m[1] * _m[7] - _m[3] * _m[5];
	T a5 = _m[2] * _m[7] - _m[3] * _m[6];
	T b0 = _m[8] * _m[13] - _m[9] * _m[12];
	T b1 = _m[8] * _m[14] - _m[10] * _m[12];
	T b2 = _m[8] * _m[15] - _m[11] * _m[12];
	T b3 = _m[9] * _m[14] - _m[10] * _m[13];
	T b4 = _m[9] * _m[15] - _m[11] * _m[13];
	T b5 = _m[10] * _m[15] - _m[11] * _m[14];

	// Calculate the determinant.
	T det = a0 * b5 - a1 * b4 + a2 * b3 + a3 * b2 - a4 * b1 + a5 * b0;

	if (fabs(det) < LVR_TOLERANCE)
	{
		return false;
	}
	ao_mat4[0]  = _m[5] * b5 - _m[6] * b4 + _m[7] * b3;
	ao_mat4[1]  = -_m[1] * b5 + _m[2] * b4 - _m[3] * b3;
	ao_mat4[2]  = _m[13] * a5 - _m[14] * a4 + _m[15] * a3;
	ao_mat4[3]  = -_m[9] * a5 + _m[10] * a4 - _m[11] * a3;

	ao_mat4[4]  = -_m[4] * b5 + _m[6] * b2 - _m[7] * b1;
	ao_mat4[5]  = _m[0] * b5 - _m[2] * b2 + _m[3] * b1;
	ao_mat4[6]  = -_m[12] * a5 + _m[14] * a2 - _m[15] * a1;
	ao_mat4[7]  = _m[8] * a5 - _m[10] * a2 + _m[11] * a1;

	ao_mat4[8]  = _m[4] * b4 - _m[5] * b2 + _m[7] * b0;
	ao_mat4[9]  = -_m[0] * b4 + _m[1] * b2 - _m[3] * b0;
	ao_mat4[10] = _m[12] * a4 - _m[13] * a2 + _m[15] * a0;
	ao_mat4[11] = -_m[8] * a4 + _m[9] * a2 - _m[11] * a0;

	ao_mat4[12] = -_m[4] * b3 + _m[5] * b1 - _m[6] * b0;
	ao_mat4[13] = _m[0] * b3 - _m[1] * b1 + _m[2] * b0;
	ao_mat4[14] = -_m[12] * a3 + _m[13] * a1 - _m[14] * a0;
	ao_mat4[15] = _m[8] * a3 - _m[9] * a1 + _m[10] * a0;
	ao_mat4 = ao_mat4*(T(1)/det);
	return true;
}

template<class T>
lvr_matrix4<T> lvr_matrix4<T>::get_transpose() const
{
	return lvr_matrix4<T>(_mm[0][0],_mm[1][0],_mm[2][0],_mm[3][0],
						_mm[0][1],_mm[1][1],_mm[2][1],_mm[3][1],
						_mm[0][2],_mm[1][2],_mm[2][2],_mm[3][2],
						_mm[0][3],_mm[1][3],_mm[2][3],_mm[3][3]);
}

template<class T>
void lvr_matrix4<T>::transpose()
{
	lvr_swap(_mm[0][1],_mm[1][0]);
	lvr_swap(_mm[0][2],_mm[2][0]);
	lvr_swap(_mm[0][3],_mm[3][0]);
	lvr_swap(_mm[1][2],_mm[2][1]);
	lvr_swap(_mm[1][3],_mm[3][1]);
	lvr_swap(_mm[2][3],_mm[3][2]);
}

template<class T>
lvr_quaternion<T> lvr_matrix4<T>::to_quaternion() const
{
	lvr_quaternion<T>	r_q;
	decompose(NULL, &r_q, NULL);
	return r_q;
}

template<class T>
void lvr_matrix4<T>::from_quaternion(const lvr_quaternion<T>& a_q)
{
	const T& x = a_q[1];
	const T& y = a_q[2];
	const T& z = a_q[3];
	const T& w = a_q[0];
	T x2 = x + x;
	T y2 = y + y;
	T z2 = z + z;

	T ww = w * w;
	T xx = x * x;
	T yy = y * y;
	T zz = z * z;

	//T xx2 = x * x2;
	//T yy2 = y * y2;
	//T zz2 = z * z2;
	T xy2 = x * y2;
	T xz2 = x * z2;
	T yz2 = y * z2;
	T wx2 = w * x2;
	T wy2 = w * y2;
	T wz2 = w * z2;

	_m[0] = ww + xx - yy - zz;
	_m[4] = xy2 + wz2;
	_m[8] = xz2 - wy2;
	_m[12] = T(0.0);

	_m[1] = xy2 - wz2;
	_m[5] = ww - xx + yy - zz;
	_m[9] = yz2 + wx2;
	_m[13] = T(0.0);

	_m[2] = xz2 + wy2;
	_m[6] = yz2 - wx2;
	_m[10] = ww - xx - yy + zz;
	_m[14] = T(0.0);

	_m[3] = T(0.0);
	_m[7] = T(0.0);
	_m[11] = T(0.0);
	_m[15] = T(1.0);
}

template<class T>
bool lvr_matrix4<T>::decompose(lvr_vector3<T>* ao_scale,lvr_quaternion<T>* ao_rotation,lvr_vector3<T>* ao_translation) const
{
	if (ao_translation)
	{
		// Extract the translation.
		(*ao_translation)[0] = _m[12];
		(*ao_translation)[1] = _m[13];
		(*ao_translation)[2] = _m[14];
	}

	// Nothing left to do.
	if (ao_scale == NULL && ao_rotation == NULL)
		return true;

	// Extract the scale.
	// This is simply the length of each axis (row/column) in the matrix.
	lvr_vector3<T> xaxis(_m[0], _m[1], _m[2]);
	T scaleX = xaxis.length();

	lvr_vector3<T> yaxis(_m[4], _m[5], _m[6]);
	T scaleY = yaxis.length();

	lvr_vector3<T> zaxis(_m[8], _m[9], _m[10]);
	T scaleZ = zaxis.length();

	// Determine if we have a negative scale (true if determinant is less than zero).
	// In this case, we simply negate a single axis of the scale.
	T det = determinant();
	if (det < 0)
		scaleZ = -scaleZ;

	if (ao_scale)
	{
		(*ao_scale)[0] = scaleX;
		(*ao_scale)[1] = scaleY;
		(*ao_scale)[2] = scaleZ;
	}

	// Nothing left to do.
	if (ao_rotation == NULL)
		return true;

	// Scale too close to zero, can't_ decompose a_rotation.
	if (scaleX < LVR_TOLERANCE || scaleY < LVR_TOLERANCE || fabs(scaleZ) < LVR_TOLERANCE)
		return false;

	T rn;

	// Factor the scale out of the matrix axes.
	rn = T(1.0) / scaleX;
	xaxis[0] *= rn;
	xaxis[1] *= rn;
	xaxis[2] *= rn;

	rn = T(1.0) / scaleY;
	yaxis[0] *= rn;
	yaxis[1] *= rn;
	yaxis[2] *= rn;

	rn = T(1.0) / scaleZ;
	zaxis[0] *= rn;
	zaxis[1] *= rn;
	zaxis[2] *= rn;

	T& ao_x = (*ao_rotation)[1];
	T& ao_y = (*ao_rotation)[2];
	T& ao_z = (*ao_rotation)[3];
	T& ao_w = (*ao_rotation)[0];
	// Now calculate the a_rotation from the resulting matrix (axes).
	T trace = xaxis[0] + yaxis[1] + zaxis[2] + T(1.0);

	if (trace > LVR_TOLERANCE)
	{
		T s = T(0.5) / sqrt(trace);
		ao_w = T(0.25) / s;
		ao_x = (yaxis[2] - zaxis[1]) * s;
		ao_y = (zaxis[0] - xaxis[2]) * s;
		ao_z = (xaxis[1] - yaxis[0]) * s;
	}
	else
	{
		// Note: since xaxis, yaxis, and zaxis are normalized, 
		// we will never divide by zero in the code below.
		if (xaxis[0] > yaxis[1] && xaxis[0] > zaxis[2])
		{
			T s = T(0.5) / sqrt(T(1.0) + xaxis[0] - yaxis[1] - zaxis[2]);
			ao_w = (yaxis[2] - zaxis[1]) * s;
			ao_x = T(0.25) / s;
			ao_y = (yaxis[0] + xaxis[1]) * s;
			ao_z = (zaxis[0] + xaxis[2]) * s;
		}
		else if (yaxis[1] > zaxis[2])
		{
			T s = T(0.5) / sqrt(T(1.0) + yaxis[1] - xaxis[0] - zaxis[2]);
			ao_w = (zaxis[0] - xaxis[2]) * s;
			ao_x = (yaxis[0] + xaxis[1]) * s;
			ao_y = T(0.25) / s;
			ao_z = (zaxis[1] + yaxis[2]) * s;
		}
		else
		{
			T s = T(0.5) / sqrt(T(1.0) + zaxis[2] - xaxis[0] - yaxis[1] );
			ao_w = (xaxis[1] - yaxis[0] ) * s;
			ao_x = (zaxis[0] + xaxis[2] ) * s;
			ao_y = (zaxis[1] + yaxis[2] ) * s;
			ao_z = T(0.25) / s;
		}
	}
	return true;
}

template<class T>
void lvr_matrix4<T>::set_translation(const lvr_vector3<T>& a_trans)
{
	_m[12] = a_trans[0];
	_m[13] = a_trans[1];
	_m[14] = a_trans[2];
}

template<class T>
lvr_vector3<T> lvr_matrix4<T>::get_translation()
{
	return lvr_vector3<T>(_d[0],_d[1],_d[2],_d[3]);
}

template<class T>
void lvr_matrix4<T>::mul(const lvr_matrix4<T>& a_mat0,const lvr_matrix4<T>& a_mat1,lvr_matrix4<T>& ao_mat)
{
	for (int i=0;i<4;++i)
	{
		for (int j=0;j<4;++j)
		{
			ao_mat(i,j) = a_mat0(i,0)*a_mat1(0,j) + a_mat0(i,1)*a_mat1(1,j) + a_mat0(i,2)*a_mat1(2,j) + a_mat0(i,3)*a_mat1(3,j);
		}
	}
}

template<class T>
lvr_matrix4<T> lvr_matrix4<T>::make_translation_matrix(const lvr_vector3<T>& a_trans)
{
	return lvr_matrix4<T>(T(1),T(0),T(0),T(0),
		T(0),T(1),T(0),T(0),
		T(0),T(0),T(1),T(0),
		a_trans[0],a_trans[1],a_trans[2],T(1));
}

template<class T>
lvr_matrix4<T> lvr_matrix4<T>::axis_angle_rotate_by_center(const lvr_vector3<T>& a_rot_center,const lvr_vector3<T>& a_axis,T a_angle_rad)
{
	lvr_matrix4<T> rot_mat;
	if (fabs(a_angle_rad) < LVR_TOLERANCE)
	{
		rot_mat = lvr_matrix4<T>();
		return rot_mat;
	}
	rot_mat = /*axis_angle_rotate*/make_rotation_matrix_around_axis(a_axis, a_angle_rad);
	//                   
	// set up translation matrix for moving v1 to origin
	//
	lvr_matrix4 m1, m2;
	m1 = lvr_matrix4<T>();
	m2 = lvr_matrix4<T>();

	m1(0, 3) = -a_rot_center[0];
	m1(1, 3) = -a_rot_center[1];
	m1(2, 3) = -a_rot_center[2];

	m2(0, 3) = a_rot_center[0];
	m2(1, 3) = a_rot_center[1];
	m2(2, 3) = a_rot_center[2];

	return m2*(rot_mat*m1);
}

template<class T>
lvr_matrix4<T> lvr_matrix4<T>::make_scale_matrix_around_axis(const lvr_vector3<T>& a_axis,T a_s)
{
	return lvr_matrix4<T>();
}


template<class T>
lvr_matrix4<T> lvr_matrix4<T>::make_scale_matrix(const lvr_vector3<T>& a_scales)
{
	lvr_matrix4<T> mat;
	mat[0] = a_scales[0];
	mat[5] = a_scales[1];
	mat[10] = a_scales[2];
	return mat;
}

template<class T>
lvr_matrix4<T> lvr_matrix4<T>::make_rotation_matrix_around_axis(const lvr_vector3<T>& a_axis,T a_angle_rad)
{
	T x = a_axis[0];
	T y = a_axis[1];
	T z = a_axis[2];
	// Make sure the input a_axis is normalized.
	T n = x*x + y*y + z*z;
	if (n != 1.0)
	{
		// Not normalized.
		n = sqrt(n);
		// Prevent divide too close to zero.
		if (n > 1e-12)
		{
			n = 1.0f / n;
			x *= n;
			y *= n;
			z *= n;
		}
		else
		{
			//should return identity matrix,for there must be a wrong thing happen.
			return lvr_matrix4<T>();
		}
	}

	T c = cos(a_angle_rad);
	T s = sin(a_angle_rad);

	T t_ = 1.0f - c;
	T tx = t_ * x;
	T ty = t_ * y;
	T tz = t_ * z;
	T txy = tx * y;
	T txz = tx * z;
	T tyz = ty * z;
	T sx = s * x;
	T sy = s * y;
	T sz = s * z;

	lvr_matrix4<T> dst;
	dst[0] = c + tx*x;
	dst[1] = txy + sz;
	dst[2] = txz - sy;
	dst[3] = 0.0f;

	dst[4] = txy - sz;
	dst[5] = c + ty*y;
	dst[6] = tyz + sx;
	dst[7] = 0.0f;

	dst[8] = txz + sy;
	dst[9] = tyz - sx;
	dst[10] = c + tz*z;
	dst[11] = 0.0f;

	dst[12] = 0.0f;
	dst[13] = 0.0f;
	dst[14] = 0.0f;
	dst[15] = 1.0f;
	return dst;
}

template<class T>
lvr_matrix4<T> lvr_matrix4<T>::make_reflection_matrix_by_plane(const lvr_vector3<T>& a_normal,T a_dist_to_origin)
{
	T k = -2.0f * a_dist_to_origin;

	T nxy2 = -2*a_normal[0]*a_normal[1];
	T nxz2 = -2*a_normal[0]*a_normal[2];
	T nyz2 = -2*a_normal[1]*a_normal[2];
	T one_xx2 = 1.0f-2.0f*a_normal[0]*a_normal[0];
	T one_yy2 = 1.0f-2.0f*a_normal[1]*a_normal[1];
	T one_zz2 = 1.0f-2.0f*a_normal[2]*a_normal[2];
	return lvr_matrix4<T>(
		one_xx2,    nxy2,   nxz2,   k*a_normal[0],
		nxy2, one_yy2,   nyz2,   k*a_normal[1],
		nxz2,    nyz2,one_zz2,   k*a_normal[2],
		T(0),    T(0),   T(0),      T(1.0) );
}

template<class T>
lvr_matrix4<T> lvr_matrix4<T>::make_view_matrix(const lvr_vector3<T>& a_pos,const lvr_vector3<T>& a_target,const lvr_vector3<T>& a_up)
{
	lvr_vector3<T> look = a_pos - a_target;
	look.normalize();
	lvr_vector3<T> up(a_up);
	up.normalize();
	lvr_vector3<T> right = up.cross(look);
	right.normalize();
	up = look.cross(right);
	return lvr_matrix4<T>(
		right[0],up[0],look[0],T(0),
		right[1],up[1],look[1],T(0),
		right[2],up[2],look[2],T(0),
		-(right*a_pos),-(up*a_pos),-(look*a_pos),T(1)
		);
}

template<class T>
lvr_matrix4<T> lvr_matrix4<T>::make_ortho_matrix(T a_width,T a_height,T a_near,T a_far)
{
	T f_n = T(1)/(a_far - a_near);
	return lvr_matrix4<T>(
		T(2.0)/a_width,     (0),                     T(0),      T(0),
		T(0),     T(2.0)/a_height,                     T(0),      T(0),
		T(0),               T(0),                    T(-2.0)*f_n,      T(0),
		T(0),               T(0),   -(a_far+a_near)*f_n,    T(1.0) );
}

/*template<class T>
lvr_matrix4<T> lvr_matrix4<T>::make_proj_matrix_rh(T a_view_angle_rad,T a_aspect_ratio,T a_near,T a_far)
{
	T f_n = T(1.0) / (a_far - a_near);
	T theta = a_view_angle_rad * T(0.5);

	T divisor = tan(theta);
	T factor = T(1.0) / divisor;

	return lvr_matrix4<T>(
		(T(1.0) / a_aspect_ratio) * factor,T(0),     T(0),              T(0),
		T(0),  factor,                                  T(0),              T(0),
		T(0),  T(0),      (-(a_far + a_near)) * f_n,    T(-1.0),
		T(0),  T(0),   T(-2.0) * a_far * a_near * f_n,       T(0));
}*/

//for vr case
template<class T>
lvr_matrix4<T> lvr_matrix4<T>::make_proj_matrix_rh(T a_view_angle_rad,T a_aspect_ratio,T a_near,T a_far)
{
	T f_n = T(1.0) / (a_near - a_far);
	T theta = a_view_angle_rad * T(0.5);

	T divisor = tan(theta);
	T factor = T(1.0) / divisor;

	return lvr_matrix4<T>(
		(T(1.0) / a_aspect_ratio) * factor,T(0),     T(0),              T(0),
		T(0),  factor,                                  T(0),              T(0),
		T(0),  T(0),      a_far * f_n,    T(-1.0),
		T(0),  T(0),   a_far * a_near * f_n,       T(0));
}

template<class T>
lvr_matrix4<T> lvr_matrix4<T>::make_proj_matrix_lh(T a_view_angle_rad,T a_aspect_ratio,T a_near,T a_far)
{
	T f_n = T(1.0) / (a_far - a_near);
	T theta = a_view_angle_rad * T(0.5);

	T divisor = tan(theta);
	T factor = T(1.0) / divisor;

	return lvr_matrix4<T>(
		(T(1.0) / a_aspect_ratio) * factor,T(0),     T(0),              T(0),
		T(0),  factor,                                  T(0),              T(0),
		T(0),  T(0),      -a_far * f_n,    T(-1.0),
		T(0),  T(0),   -a_far * a_near * f_n,       T(0));
}

template<class T>
lvr_matrix4<T> lvr_matrix4<T>::make_rotate_from_to_matrix(const lvr_vector3<T>& a_src,const lvr_vector3<T> a_dst)
{
	lvr_vector3<T> t_src = a_src;
	double len1 = t_src.normalize();
	lvr_vector3<T> t_dst = a_dst;
	double len2 = t_dst.normalize();
	lvr_vector3<T> axis = t_src.cross(t_dst);
	axis.normalize();
	double cosv = t_src*t_dst;
	lvr_clamp(cosv,-1.0,1.0);
	T angle = T(acos(cosv));
	return lvr_matrix4<T>::make_rotation_matrix_around_axis(axis,angle);
}
