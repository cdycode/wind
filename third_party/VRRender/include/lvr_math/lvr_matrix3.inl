
template<class T>
lvr_matrix3<T>::lvr_matrix3()
	:_x(T(1),T(0),T(0)),_y(T(0),T(1),T(0)),_z(T(0),T(0),T(1))
{
	//_m[1] = _m[2] = _m[3] = _m[5] = _m[6] = _m[7] = T(0.0);
	//_m[0] = _m[4] = _m[8] = T(1.0);
}

template<class T>
lvr_matrix3<T>::lvr_matrix3(T a_v)
	:_x(a_v,T(0),T(0)),_y(T(0),a_v,T(0)),_z(T(0),T(0),a_v)
{
	//_m[1] = _m[2] = _m[3] = _m[5] = _m[6] = _m[7] = T(0.0);
	//_m[0] = _m[4] = _m[8] = a_v;
	
}

template<class T>
lvr_matrix3<T>::lvr_matrix3(const lvr_vector3<T>& a_v3)
	:_x(a_v3[0],T(0),T(0)),_y(T(0),a_v3[1],T(0)),_z(T(0),T(0),a_v3[2])
{
	//_m[1] = _m[2] = _m[3] = _m[5] = _m[6] = _m[7] = T(0.0);
	//_m[0] = a_v3[0];
	//_m[4] = a_v3[1];
	//_m[8] = a_v3[2];
}

template<class T>
lvr_matrix3<T>::lvr_matrix3(const lvr_vector3<T>& a_v0,const lvr_vector3<T>& a_v1,const lvr_vector3<T>& a_v2)
	:_x(a_v0),_y(a_v1),_z(a_v2)
{

}

template<class T>
lvr_matrix3<T>::lvr_matrix3(T a_m00,T a_m01,T a_m02, T a_m10,T a_m11,T a_m12, T a_m20,T a_m21,T a_m22)
	:_x(a_m00,a_m01,a_m02),_y(a_m10,a_m11,a_m12),_z(a_m20,a_m21,a_m22)
{

}

template<class T>
T& lvr_matrix3<T>::operator[](int a_id)
{
	LVR_ASSERT(a_id >=0 && a_id <= 8);
	return _m[a_id];
}

template<class T>
const T& lvr_matrix3<T>::operator[](int a_id) const
{
	LVR_ASSERT(a_id >=0 && a_id <= 8);
	return _m[a_id];
}

template<class T>
T& lvr_matrix3<T>::operator()(int a_row,int a_col)
{
	LVR_ASSERT(a_row>=0&& a_row<=2 && a_col>=0&& a_col<=2 );
	return _mm[a_row][a_col];
}

template<class T>
const T& lvr_matrix3<T>::operator()(int a_row,int a_col) const
{
	LVR_ASSERT(a_row>=0&& a_row<=2 && a_col>=0&& a_col<=2 );
	return _mm[a_row][a_col];
}

template<class T>
lvr_vector3<T> lvr_matrix3<T>::operator*(const lvr_vector3<T>& a_v3) const
{
	return lvr_vector3<T>(_x*a_v3,_y*a_v3,_z*a_v3);
}

template<class T>
lvr_matrix3<T> lvr_matrix3<T>::operator*(const T& a_s) const
{
	return lvr_matrix3<T>(_m[0]*a_s,_m[1]*a_s,_m[2]*a_s,
						_m[3]*a_s,_m[4]*a_s,_m[5]*a_s,
						_m[6]*a_s,_m[7]*a_s,_m[8]*a_s);
}

template<class T>
void lvr_matrix3<T>::operator*=(const T& a_s)
{
	_m[0]*= a_s;
	_m[1]*= a_s;
	_m[2]*= a_s;
	_m[3]*= a_s;
	_m[4]*= a_s;
	_m[5]*= a_s;
	_m[6]*= a_s;
	_m[7]*= a_s;
	_m[8]*= a_s;
}

template<class T>
lvr_matrix3<T> lvr_matrix3<T>::operator/(const T& a_s) const
{
	return lvr_matrix3<T>(_m[0]/a_s,_m[1]/a_s,_m[2]/a_s,
		_m[3]/a_s,_m[4]/a_s,_m[5]/a_s,
		_m[6]/a_s,_m[7]/a_s,_m[8]/a_s);
}

template<class T>
void lvr_matrix3<T>::operator/=(const T& a_s)
{
	_m[0]/= a_s;
	_m[1]/= a_s;
	_m[2]/= a_s;
	_m[3]/= a_s;
	_m[4]/= a_s;
	_m[5]/= a_s;
	_m[6]/= a_s;
	_m[7]/= a_s;
	_m[8]/= a_s;
}

template<class T>
lvr_matrix3<T> lvr_matrix3<T>::operator*(const lvr_matrix3<T>& a_mat3)const
{
	lvr_matrix3<T>	r_mat;
	for (int i=0;i<3;++i)
	{
		for (int j=0;j<3;++j)
		{
			r_mat(i,j) = _mm[i][0]*a_mat3(0,j) + _mm[i][1]*a_mat3(1,j) + _mm[i][2]*a_mat3(2,j);
		}
	}
	return r_mat;
}

template<class T>
void lvr_matrix3<T>::be_identity()
{
	_mm[0][1] = _mm[0][2] = _mm[1][0] = _mm[1][2] = _mm[2][0] = _mm[2][1] = T(0.0);
	_mm[0][0] = _mm[1][1] = _mm[2][2] = T(1.0);
}

template<class T>
T lvr_matrix3<T>::determinant() const
{
	T cx = _m[4]*_m[8] - _m[5]*_m[7];
	T cy = _m[7]*_m[2] - _m[1]*_m[8];
	T cz = _m[1]*_m[5] - _m[2]*_m[4];
	return _m[0]*cx + _m[3]*cy + _m[6]*cz;
}

template<class T>
bool lvr_matrix3<T>::inverse()
{
	return false;
}

template<class T>
lvr_matrix3<T> lvr_matrix3<T>::get_inverse() const
{
	T det = _mm[0][0] * (_mm[1][1] * _mm[2][2] - _mm[2][1] * _mm[1][2])
		- _mm[1][0] * (_mm[0][1] * _mm[2][2] - _mm[2][1] * _mm[0][2])
		+ _mm[2][0] * (_mm[0][1] * _mm[1][2] - _mm[1][1] * _mm[0][2]);
	if (fabs(det) < LVR_TOLERANCE)
	{
		return lvr_matrix3<T>(1.0);
	}
	T t_d_det = T(1.0)/det;
	return lvr_matrix3<T>(
		(_mm[1][1] * _mm[2][2] - _mm[1][2] * _mm[2][1])*t_d_det,
		(_mm[2][1] * _mm[0][2] - _mm[2][2] * _mm[0][1])*t_d_det,
		(_mm[0][1] * _mm[1][2] - _mm[0][2] * _mm[1][1])*t_d_det,
		(_mm[2][0] * _mm[1][2] - _mm[1][0] * _mm[2][2])*t_d_det,
		(_mm[0][0] * _mm[2][2] - _mm[2][0] * _mm[0][2])*t_d_det,
		(_mm[1][0] * _mm[0][2] - _mm[0][0] * _mm[1][2])*t_d_det,
		(_mm[2][1] * _mm[1][0] - _mm[1][1] * _mm[2][0])*t_d_det,
		(_mm[0][1] * _mm[2][0] - _mm[2][1] * _mm[0][0])*t_d_det,
		(_mm[1][1] * _mm[0][0] - _mm[0][1] * _mm[1][0])*t_d_det
		);
	// 		_mm[1][1] * _mm[2][2] - _mm[2][1] * _mm[1][2],
	// 		_mm[1][2] * _mm[2][0] - _mm[2][2] * _mm[1][0],
	// 		_mm[1][0] * _mm[2][1] - _mm[2][0] * _mm[1][1],
	// 		_mm[0][2] * _mm[2][1] - _mm[0][1] * _mm[2][2],
	// 		_mm[0][0] * _mm[2][2] - _mm[0][2] * _mm[2][0],
	// 		_mm[0][1] * _mm[2][0] - _mm[0][0] * _mm[2][1],
	// 		_mm[1][2] * _mm[0][1] - _mm[1][1] * _mm[0][2],
	// 		_mm[1][0] * _mm[0][2] - _mm[1][2] * _mm[0][0],
	// 		_mm[1][1] * _mm[0][0] - _mm[1][0] * _mm[0][1]);
}

template<class T>
lvr_matrix3<T> lvr_matrix3<T>::get_transpose() const
{
	lvr_matrix3<T> r_m(*this);
	r_m.transpose();
	return r_m;
}

template<class T>
void lvr_matrix3<T>::transpose()
{
	lvr_swap(_mm[0][1],_mm[1][0]);
	lvr_swap(_mm[0][2],_mm[2][0]);
	lvr_swap(_mm[1][2],_mm[2][1]);
}

template<class T>
bool lvr_matrix3<T>::to_quaternion( lvr_quaternion<T>& ao_q) const
{
	// Extract the scale.
	// This is simply the lengkth of each axis (row/column) in the matrix.
	lvr_vector3<T> xaxis(_m[0],_m[1],_m[2]);
	T scaleX = xaxis.length();

	lvr_vector3<T> yaxis(_m[3],_m[4],_m[5]);
	T scaleY = yaxis.length();

	lvr_vector3<T> zaxis(_m[6],_m[7],_m[8]);
	T scaleZ = zaxis.length();

	// Determine if we have a negative scale (true if determinant is less than zero).
	// In this case, we simply negate a single axis of the scale.
	T det = determinant();
	if (det < 0)
		scaleZ = -scaleZ;

	// Scale too close to zero, can't_ decompose rotation.
	if (scaleX < LVR_TOLERANCE || scaleY < LVR_TOLERANCE || fabs(scaleZ) < LVR_TOLERANCE)
		return false;

	T rn;

	// Factor the scale out of the matrix axes.
	rn = 1.0f / scaleX;
	xaxis[0] *= rn;
	xaxis[1] *= rn;
	xaxis[2] *= rn;

	rn = 1.0f / scaleY;
	yaxis[0] *= rn;
	yaxis[1] *= rn;
	yaxis[2] *= rn;

	rn = 1.0f / scaleZ;
	zaxis[0] *= rn;
	zaxis[1] *= rn;
	zaxis[2] *= rn;

	// Now calculate the rotation from the resulting matrix (axes).
	T trace = xaxis[0] + yaxis[1] + zaxis[2] + T(1.0);

	if (trace > LVR_TOLERANCE)
	{
		T s = T(0.5) / sqrt(trace);
		ao_q[3] = T(0.25) / s;
		ao_q[0] = (yaxis[2] - zaxis[1]) * s;
		ao_q[1] = (zaxis[0] - xaxis[2]) * s;
		ao_q[2] = (xaxis[1] - yaxis[0]) * s;
	}
	else
	{
		// Note: since xaxis, yaxis, and zaxis are normalized, 
		// we will never divide by zero in the code below.
		if (xaxis[0] > yaxis[1] && xaxis[0] > zaxis[2])
		{
			T s = T(0.5) / sqrt(T(1.0) + xaxis[0] - yaxis[1] - zaxis[2]);
			ao_q[3] = (yaxis[2] - zaxis[1]) * s;
			ao_q[0] = T(0.25) / s;
			ao_q[1] = (yaxis[0] + xaxis[1]) * s;
			ao_q[2] = (zaxis[0] + xaxis[2]) * s;
		}
		else if (yaxis[1] > zaxis[2])
		{
			T s = T(0.5) / sqrt(T(1.0) + yaxis[1] - xaxis[0] - zaxis[2]);
			ao_q[3] = (zaxis[0] - xaxis[2]) * s;
			ao_q[0] = (yaxis[0] + xaxis[1]) * s;
			ao_q[1] = T(0.25) / s;
			ao_q[2] = (zaxis[1] + yaxis[2]) * s;
		}
		else
		{
			T s = T(0.5) / sqrt(T(1.0) + zaxis[2] - xaxis[0] - yaxis[1] );
			ao_q[3] = (xaxis[1] - yaxis[0] ) * s;
			ao_q[0] = (zaxis[0] + xaxis[2] ) * s;
			ao_q[1] = (zaxis[1] + yaxis[2] ) * s;
			ao_q[2] = T(0.25) / s;
		}
	}
	return true;
}

template<class T>
void lvr_matrix3<T>::from_quaternion(const lvr_quaternion<T>& a_q)
{
	T x2 = a_q[0] + a_q[0];
	T y2 = a_q[1] + a_q[1];
	T z2 = a_q[2] + a_q[2];

	T xx2 = a_q[0] * x2;
	T yy2 = a_q[1] * y2;
	T zz2 = a_q[2] * z2;
	T xy2 = a_q[0] * y2;
	T xz2 = a_q[0] * z2;
	T yz2 = a_q[1] * z2;
	T wx2 = a_q[3] * x2;
	T wy2 = a_q[3] * y2;
	T wz2 = a_q[3] * z2;

	_m[0] = 1.0f - yy2 - zz2;
	_m[1] = xy2 + wz2;
	_m[2] = xz2 - wy2;

	_m[3] = xy2 - wz2;
	_m[4] = 1.0f - xx2 - zz2;
	_m[5] = yz2 + wx2;

	_m[6] = xz2 + wy2;
	_m[7] = yz2 - wx2;
	_m[8] = 1.0f - xx2 - yy2;
}

template<class T>
void lvr_matrix3<T>::mul(const lvr_matrix3<T>& a_mat0,const lvr_matrix3<T>& a_mat1,lvr_matrix3<T>& ao_mat2)
{
	for (int i=0;i<3;++i)
	{
		for (int j=0;j<3;++j)
		{
			ao_mat2[i][j] = a_mat0(i,0)*a_mat1(0,j) + a_mat0(i,1)*a_mat1(1,j) + a_mat0(i,2)*a_mat1(2,j);
		}
	}
}                        //Why we need to get a_mat1 and a_mat2?confused.

template<class T>
lvr_matrix3<T> lvr_matrix3<T>::axis_angle_rotate(const lvr_vector3<T>& a_axis,T a_angle_rad)
{
	lvr_matrix3<T> rot_mat(T(1.0));

	if ( fabs(a_angle_rad) < LVR_TOLERANCE ) 
	{
		return rot_mat;		
	}

	T axis_len = a_axis.length();

	T cos_a = cos(a_angle_rad);
	T one_c = 1.0 - cos_a;
	T sin_a = sin(a_angle_rad);
	T ux	= a_axis[0] / axis_len;
	T uy	= a_axis[1] / axis_len;
	T uz	= a_axis[2] / axis_len;

	rot_mat(0,0) = ux * ux * one_c + cos_a;
	rot_mat(1,0) = ux * uy * one_c - uz * sin_a;
	rot_mat(2,0) = ux * uz * one_c + uy * sin_a;

	rot_mat(0,1) = uy * ux * one_c + uz * sin_a;
	rot_mat(1,1) = uy * uy * one_c + cos_a;
	rot_mat(2,1) = uy * uz * one_c - ux * sin_a;

	rot_mat(0,2) = uz * ux * one_c - uy * sin_a;
	rot_mat(1,2) = uz * uy * one_c + ux * sin_a;
	rot_mat(2,2) = uz * uz * one_c + cos_a;

	return rot_mat;
}

template<class T>
lvr_matrix3<T> lvr_matrix3<T>::make_rotation_matrix_around_axis(const lvr_vector3<T>& a_axis, T a_angle_rad)
{
	T x = a_axis[0];
	T y = a_axis[1];
	T z = a_axis[2];
	// Make sure the input a_axis is normalized.
	T n = x*x + y*y + z*z;
	if (n != 1.0)
	{
		// Not normalized.
		n = sqrt(n);
		// Prevent divide too close to zero.
		if (n > 1e-12)
		{
			n = 1.0f / n;
			x *= n;
			y *= n;
			z *= n;
		}
		else
		{
			//should return identity matrix,for there must be a wrong thing happen.
			return lvr_matrix3<T>();
		}
	}

	T c = cos(a_angle_rad);
	T s = sin(a_angle_rad);

	T t_ = 1.0f - c;
	T tx = t_ * x;
	T ty = t_ * y;
	T tz = t_ * z;
	T txy = tx * y;
	T txz = tx * z;
	T tyz = ty * z;
	T sx = s * x;
	T sy = s * y;
	T sz = s * z;

	lvr_matrix3<T> dst;
	dst[0] = c + tx*x;
	dst[1] = txy + sz;
	dst[2] = txz - sy;

	dst[3] = txy - sz;
	dst[4] = c + ty*y;
	dst[5] = tyz + sx;

	dst[6] = txz + sy;
	dst[7] = tyz - sx;
	dst[8] = c + tz*z;

	return dst;
}

template<class T>
lvr_matrix3<T> lvr_matrix3<T>::make_rotate_from_to_matrix(const lvr_vector3<T>& a_src, const lvr_vector3<T> a_dst)
{
	lvr_vector3<T> t_src = a_src;
	T len1 = t_src.normalize();
	lvr_vector3<T> t_dst = a_dst;
	T len2 = t_dst.normalize();
	lvr_vector3<T> axis = t_src.cross(t_dst);
	axis.normalize();
	T cosv = t_src*t_dst;
	lvr_clamp(cosv, T(-1.0), T(1.0));
	T angle = acos(cosv);
	return lvr_matrix3<T>::make_rotation_matrix_around_axis(axis, angle);
}
