#ifndef __lvr_box_h__
#define __lvr_box_h__

#include "lvr_vector3.h"
#include "lvr_ray3.h"

template<class T>
class lvr_box3
{
public:
	lvr_box3();
	lvr_box3(const lvr_vector3<T>& a_min,const lvr_vector3<T>& a_max);
	lvr_box3(const T a_min_x,const T a_min_y,const T a_min_z,
			const T a_max_x,const T a_max_y,const T a_max_z);
	~lvr_box3(){}

public:
	lvr_box3<T> operator*(const T a_s)const;
	void operator*=(const T a_s);

public:
	lvr_vector3<T> get_size() const;
	lvr_vector3<T> get_center() const;
	const lvr_vector3<T>& get_min()const;
	const lvr_vector3<T>& get_max()const;

	lvr_vector3<T>& get_min();
	lvr_vector3<T>& get_max();

	void combine(const lvr_box3<T>& a_b);
	void translate(const lvr_vector3<T>& a_mov);
	bool contains(const lvr_vector3<T>& a_v3)const;
	void expand(const lvr_vector3<T>& a_v3);
	int ray_interset(const lvr_ray3<T>& a_ray,T& ao_dist)const;

	lvr_intersect_status intersect(const lvr_box3<T>& a_b)const;
private:
	lvr_vector3<T>	_min;
	lvr_vector3<T>	_max;
};

#include "lvr_box3.inl"

typedef lvr_box3<float> lvr_box3f;
typedef lvr_box3f lvr_bounding_box3f;
typedef lvr_box3<double> lvr_box3d;
typedef lvr_box3d lvr_bounding_box3d;

#endif
