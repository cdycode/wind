#ifndef __lvr_rect2_h__
#define __lvr_rect2_h__

#include"lvr_vector2.h"
template<class T>
class lvr_rect2
{
public:
	lvr_rect2();
	lvr_rect2(T a_x,T a_y,T a_w,T a_h);
	lvr_rect2(const lvr_vector2<T>& a_min,lvr_vector2<T>& a_max);
	lvr_rect2(const lvr_vector2<T>& a_size);
	~lvr_rect2(){}
public:
	lvr_vector2<T> get_size() const;
	lvr_vector2<T> get_center() const;
	const lvr_vector2<T>& get_min()const;
	const lvr_vector2<T>& get_max()const;

	lvr_vector2<T>& get_min();
	lvr_vector2<T>& get_max();

	void combine(const lvr_rect2<T>& a_b);
	void translate(const lvr_vector2<T>& a_mov);
	bool contains(const lvr_vector2<T>& a_v2)const;
	void expand(const lvr_vector2<T>& a_v2);
private:
	lvr_vector2<T>	_min;
	lvr_vector2<T>	_max;
};

#include "lvr_rect2.inl"

typedef lvr_rect2<float> lvr_rect2f;
typedef lvr_rect2<double> lvr_rect2d;

#endif
