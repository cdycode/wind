#ifndef _LVR_BEXIER_CURVE_H_
#define _LVR_BEXIER_CURVE_H_
#include "lvr_alg.h"
#include "lvr_vector3.h"
#include <vector>

class  lvr_ctrl_point
{
public:
	enum ctrl_point_type{
		e_point,
		e_quadricPoint,
		e_cubicPoint
	};
public:
	lvr_ctrl_point(){ _ctrl_point_type = e_point; }
	lvr_ctrl_point(const lvr_ctrl_point& a_point)
	{
		_ctrl_point_type = a_point._ctrl_point_type;
		_pos = a_point._pos;
	}
	~lvr_ctrl_point(){}

	ctrl_point_type _ctrl_point_type;
	lvr_vector3f  _pos;
};



class lvr_bezier_curve
{
public:
	lvr_bezier_curve();
	~lvr_bezier_curve();
public:
	bool add_point(const lvr_ctrl_point& a_point);
	bool get_point(int segment_id, float a_pos, lvr_vector3f& ao_point);
	bool get_tangent(int segment_id, float a_pos,lvr_vector3f& ao_tanget);
	bool get_point_tanget(int segment_id, float a_pos, lvr_vector3f& ao_point, lvr_vector3f& ao_tangent);
	bool check_curve_state();
private:
	int _points_count;
	int _segment_count;
	std::vector<lvr_ctrl_point>  _ctrl_points;
	std::vector<int>          _segment_begin_pt_ids;
};

#endif