#include "lvr_vector2.h"

template<class T>
T& lvr_vector2<T>::operator[](int a_id)
{
	LVR_ASSERT(a_id < 2 && a_id >= 0);
	return _m[a_id];
}

template<class T>
const T& lvr_vector2<T>::operator[](int a_id) const
{
	LVR_ASSERT(a_id < 2 && a_id >= 0);
	return _m[a_id];
}

template<class T>
lvr_vector2<T> lvr_vector2<T>::operator+(const lvr_vector2<T>& a_v2) const
{
	return lvr_vector2<T>(_x + a_v2[0],_y + a_v2[1]);
}

template<class T>
lvr_vector2<T> lvr_vector2<T>::operator-(const lvr_vector2<T>& a_v2) const
{
	return lvr_vector2<T>(_x - a_v2[0],_y - a_v2[1]);
}

template<class T>
T lvr_vector2<T>::operator*(const lvr_vector2<T>& a_v2) const
{
	return _x*a_v2[0] + _y*a_v2[1];
}

template<class T>
lvr_vector2<T> lvr_vector2<T>::operator*(T a_scale) const
{
	return lvr_vector2<T>(_x*a_scale,_y*a_scale);
}

template<class T>
lvr_vector2<T> lvr_vector2<T>::operator/(T a_scale) const
{
	return lvr_vector2<T>(_x/a_scale,_y/a_scale);
}

template<class T>
void lvr_vector2<T>::operator+=(const lvr_vector2<T>& a_v2)
{
	_x += a_v2[0];
	_y += a_v2[1];
}

template<class T>
void lvr_vector2<T>::operator-=(const lvr_vector2<T>& a_v2)
{
	_x -= a_v2[0];
	_y -= a_v2[1];
}

template<class T>
void lvr_vector2<T>::operator*=(T a_scale)
{
	_x *= a_scale;
	_y *= a_scale;
}

template<class T>
void lvr_vector2<T>::operator/=(T a_scale)
{
	_x /= a_scale;
	_y /= a_scale;
}

template<class T>
bool lvr_vector2<T>::operator<(const lvr_vector2<T>& a_v2)const
{
	if (_x < a_v2[0])
	{
		return true;
	}
	else if(_x==a_v2[0] && _y < a_v2[1])
	{
		return true;
	}
	return false;
}

template<class T>
T lvr_vector2<T>::length() const
{
	return sqrt(length_sq());
}

template<class T>
T lvr_vector2<T>::length_sq() const
{
	return _x*_x + _y*_y;
}

template<class T>
void lvr_vector2<T>::normalize()
{
	T t_l = length();
	LVR_ASSERT(t_l > LVR_TOLERANCE);
	*this /= t_l;
}

template<class T>
lvr_vector2<T> lvr_vector2<T>::get_normlize() const
{
	T t_l = length();
	LVR_ASSERT(t_l > LVR_TOLERANCE);
	return lvr_vector2<T>(_x/t_l,_y/t_l);
}

template<class T>
lvr_vector2<T> lvr_vector2<T>::lerp(const lvr_vector2<T>& a_v2,T a_pos) const
{
	return lvr_vector2<T>(_x*a_pos + a_v2[0]*(1.0-a_pos),_y*a_pos + a_v2[1]*(1.0-a_pos));
}

template<class T>
bool lvr_vector2<T>::is_nan() const
{
	return _x != _x || _y != _y;
}
