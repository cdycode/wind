#ifndef __lvr_quaternion_h__
#define __lvr_quaternion_h__

#include "lvr_vector3.h"

template<class T>
class lvr_quaternion
{
public:
	lvr_quaternion();
	lvr_quaternion(const T& a_x,const T& a_y,const T& a_z,const T& a_w);
	lvr_quaternion(const lvr_vector3<T>& a_axis,const T& a_angle_rad);
	lvr_quaternion(const lvr_vector3<T>& a_from,const lvr_vector3<T>& a_to);
	~lvr_quaternion(){}
public:
	T& operator[](int id);
	const T& operator[](int id)const;
	lvr_quaternion<T>	operator+(const lvr_quaternion<T>& a_q)const;
	void	operator+=(const lvr_quaternion<T>& a_q);
	lvr_quaternion<T>	operator-(const lvr_quaternion<T>& a_q)const;
	void	operator-=(const lvr_quaternion<T>& a_q);
	lvr_quaternion<T> operator*(const T& a_s)const;
	void operator*=(const T& a_s);
	lvr_quaternion<T> operator*(const lvr_quaternion<T>& a_q)const;
	lvr_vector3<T> operator*(const lvr_vector3<T>& a_q)const;

public:
	T length()const;
	T length_sq()const;
	void normalize();
	lvr_quaternion<T> get_normalize()const;
	lvr_quaternion<T> get_conj()const;
	void inverse();
	lvr_quaternion<T> get_inverse()const;
	lvr_vector3<T>	get_imag()const;
	lvr_quaternion<T>& set_axis_angle(const lvr_vector3<T>& a_axis,const T& a_angle);
	void	get_axis_angle(lvr_vector3<T>& axis,T& ao_angle) const;
	void	get_euler_angles(T& a,T& b, T& c) const;
	bool    is_nan() const;
public:
	static lvr_quaternion<T> lerp(const lvr_quaternion<T>& a_q0,const lvr_quaternion<T>& a_q1,const T& a_pos);
	static lvr_quaternion<T> slerp(const lvr_quaternion<T>& a_q1,const lvr_quaternion<T>& a_q2,const T& a_pos);

	static lvr_quaternion<T> make_euler_quat(const T& a_pitch,const T& a_yaw,const T& a_roll );
	//I do not know whether is good or not to make this public,since somebody want to directly use w,x,y,z
	//directly,it is not like vector4f or something else,but here,may be I should open it for somebody really
	//need to know who is w.may be I will close it in the near future,but now,this is my choice,I do not want to,
	//but it is like I have to.
public:
	union
	{
		struct
		{
			T w,x,y,z;
		};
		T	_m[4];
	};
	
};

#include "lvr_quaternion.inl"
typedef lvr_quaternion<float> lvr_quaternionf;
typedef lvr_quaternion<double> lvr_quaterniond;

#endif
