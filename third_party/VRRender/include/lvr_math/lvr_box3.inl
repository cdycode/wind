#include "lvr_alg.h"
template<class T>
lvr_box3<T>::lvr_box3()
	:_min(T(0)),_max(T(0))
{

}

template<class T>
lvr_box3<T>::lvr_box3(const lvr_vector3<T>& a_min,const lvr_vector3<T>& a_max)
	:_min(a_min),_max(a_max)
{
}

template<class T>
lvr_box3<T>::lvr_box3(const T a_min_x,const T a_min_y,const T a_min_z, const T a_max_x,const T a_max_y,const T a_max_z)
	:_min(a_min_x,a_min_y,a_min_z),_max(a_max_x,a_max_y,a_max_z)
{

}

template<class T>
lvr_box3<T> lvr_box3<T>::operator*(const T a_s) const
{
	return lvr_box3<T>(_min*a_s,_max*a_s);
}

template<class T>
void lvr_box3<T>::operator*=(const T a_s)
{
	_min *= a_s;
	_max *= a_s;
}

template<class T>
lvr_vector3<T> lvr_box3<T>::get_size() const
{
	return _max-_min;
}

template<class T>
lvr_vector3<T> lvr_box3<T>::get_center() const
{
	return (_min+_max)*T(0.5);
}

template<class T>
const lvr_vector3<T>& lvr_box3<T>::get_min() const
{
	return _min;
}

template<class T>
const lvr_vector3<T>& lvr_box3<T>::get_max() const
{
	return _max;
}

template<class T>
lvr_vector3<T>& lvr_box3<T>::get_min()
{
	return _min;
}

template<class T>
lvr_vector3<T>& lvr_box3<T>::get_max()
{
	return _max;
}

template<class T>
void lvr_box3<T>::combine(const lvr_box3<T>& a_b)
{
	
	const lvr_vector3<T>& minb = a_b.get_min();
	const lvr_vector3<T>& maxb = a_b.get_max(); 
	_min[0] = LVR_MIN(_min[0],minb[0]);
	_min[1] = LVR_MIN(_min[1],minb[1]);
	_min[2] = LVR_MIN(_min[2],minb[2]);
	
	_max[0] = LVR_MAX(_max[0],maxb[0]);
	_max[1] = LVR_MAX(_max[1],maxb[1]);
	_max[2] = LVR_MAX(_max[2],maxb[2]);
}

template<class T>
void lvr_box3<T>::translate(const lvr_vector3<T>& a_mov)
{
	_min += a_mov;
	_max += a_mov;
}

template<class T>
bool lvr_box3<T>::contains(const lvr_vector3<T>& a_v3) const
{
	return a_v3[0] >= _min[0] && a_v3[0] <= _max[0]
	&&	a_v3[1] >= _min[1] && a_v3[1] <= _max[1]                      
	&&	a_v3[2] >= _min[2] && a_v3[2] <= _max[2];                     
}

template<class T>
void lvr_box3<T>::expand(const lvr_vector3<T>& a_v3)
{
	if(a_v3[0]<_min[0])
	{
		_min[0] = a_v3[0];
	}
	else if(a_v3[0] > _max[0])
	{
		_max[0] = a_v3[0];
	}

	if(a_v3[1]<_min[1])
	{
		_min[1] = a_v3[1];
	}
	else if(a_v3[1] > _max[1])
	{
		_max[1] = a_v3[1];
	}

	if(a_v3[2]<_min[2])
	{
		_min[2] = a_v3[2];
	}
	else if(a_v3[2] > _max[2])
	{
		_max[2] = a_v3[2];
	}
}

template<class T>
int lvr_box3<T>::ray_interset(const lvr_ray3<T>& a_ray, T& ao_dist) const
{
	lvr_vector3<T> points[8] = { lvr_vector3<T>(_min[0], _min[1], _min[2]), lvr_vector3<T>(_min[0], _max[1], _min[2]),
		lvr_vector3<T>(_max[0], _min[1], _min[2]), lvr_vector3<T>(_max[0], _max[1], _min[2]),

		lvr_vector3<T>(_min[0], _min[1], _max[2]), lvr_vector3<T>(_min[0], _max[1], _max[2]),
		lvr_vector3<T>(_max[0], _min[1], _max[2]), lvr_vector3<T>(_max[0], _max[1], _max[2]) };

	int ids[36] = {0,1,2,1,3,2, 6,4,5,6,5,7, 2,3,6,3,7,6, 0,4,1,4,5,1, 3,1,5,3,5,7};
	const lvr_vector3<T>&  torigin = a_ray.get_origin();
	const lvr_vector3<T>&  tdir = a_ray.get_dir();
	T dist = 1e30;
	int inter_sect_num = 0;
	for (int i = 0; i < 12;++i)
	{
		T a_dist = 1e30;
		if (lvr_ray_triangle_intersect(a_dist, torigin, tdir, points[ids[3 * i]], points[ids[3 * i+1]], points[ids[3 * i+2]]))
		{
			inter_sect_num++;
			if (dist > a_dist)
			{
				dist = a_dist;
			}
		}
	}
	if (inter_sect_num > 0)
	{
		ao_dist = dist;
	}
	return inter_sect_num;
}

template<class T>
lvr_intersect_status lvr_box3<T>::intersect(const lvr_box3<T>& a_b) const
{
	const lvr_vector3f& t_max = a_b.get_max();
	const lvr_vector3f& t_min = a_b.get_min();
	lvr_vector3f t_sizeb = a_b.get_size;
	lvr_vector3f t_sizea = get_size();
	lvr_vector3f t_size_add = t_sizea + t_sizeb;
	lvr_vector3f t_leave = ((t_max + t_min) - (_min+_max))*T(0.5);
	if (fabs(t_leave[0]) > t_size_add[0] || fabs(t_leave[1]) > t_size_add[1] || fabs(t_leave[2]) > t_size_add[2])
	{
		return e_intersect_leave;
	}
	lvr_vector3f t_size_leave = t_sizea - t_sizeb;
	if (fabs(t_leave[0]) < fabs(t_size_leave[0]) && fabs(t_leave[1]) < fabs(t_size_leave[1]) && fabs(t_leave[2]) < fabs(t_size_leave[2]))
	{
		return e_intersect_inside;
	}
	return e_intersect_part;
}