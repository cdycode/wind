template<class T>
lvr_pose<T>::lvr_pose()
	:_quat(T(0),T(0),T(0),T(1)),_position(T(0))
{

}

template<class T>
lvr_pose<T>::lvr_pose(const lvr_quaternion<T>& a_q,const lvr_vector3<T>& a_pos)
	:_quat(a_q),_position(a_pos)
{

}

template<class T>
void lvr_pose<T>::translate(const lvr_vector3<T>& a_v3)
{
	_position += a_v3;
}

template<class T>
void lvr_pose<T>::rotate(const lvr_quaternion<T>& a_q)
{
	_quat = a_q*_quat;
}

template<class T>
lvr_matrix4<T> lvr_pose<T>::get_matrix() const
{
	lvr_matrix4<T> r_m;
	r_m.from_quaternion(_quat);
	r_m[12] = _position[0];
	r_m[13] = _position[1];
	r_m[14] = _position[2];
	return r_m;
}

template<class T>
const lvr_vector3<T>& lvr_pose<T>::get_position() const
{
	return _position;
}


template<class T>
const lvr_quaternion<T>& lvr_pose<T>::get_orientation() const
{
	return _quat;
}

template<class T>
void lvr_pose<T>::set_position(const lvr_vector3<T>& a_pos)
{
	_position = a_pos;
}

template<class T>
void lvr_pose<T>::set_orientation(const lvr_quaternion<T>& a_orientation)
{
	_quat = a_orientation;
}
