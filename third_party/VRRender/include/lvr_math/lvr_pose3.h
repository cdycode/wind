#ifndef __lvr_pose_h__
#define __lvr_pose_h__

#include "lvr_matrix4.h"
#include "lvr_quaternion.h"

template<class T>
class lvr_pose
{
public:
	lvr_pose();
	lvr_pose(const lvr_quaternion<T>& a_q,const lvr_vector3<T>& a_pos);
	~lvr_pose(){}
public:
	void				translate(const lvr_vector3<T>& a_v3);
	void				rotate(const lvr_quaternion<T>& a_q);
	lvr_matrix4<T>		get_matrix()const;
	const lvr_vector3<T>&		get_position()const;
	const lvr_quaternion<T>&	get_orientation()const;
	void	set_position(const lvr_vector3<T>& a_pos);
	void	set_orientation(const lvr_quaternion<T>& a_orientation);
private:
	lvr_quaternion<T>	_quat;
	lvr_vector3<T>		_position;
};


typedef lvr_pose<float> lvr_posef;          //Here we miss a typedef,so I construct it.
typedef lvr_pose<double> lvr_posed;
#include "lvr_pose3.inl"

#endif
