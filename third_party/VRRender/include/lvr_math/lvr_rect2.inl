template<class T>
lvr_rect2<T>::lvr_rect2()
:_min(T(0),T(0)),_max(T(0),T(0))
{

}

template<class T>
lvr_rect2<T>::lvr_rect2(T a_x,T a_y,T a_w,T a_h)
:_min(a_x-T(0.5)*a_w,a_y-T(0.5)*a_h),_max(a_x+T(0.5)*a_w,a_y+T(0.5)*a_h)
{

}

template<class T>
lvr_rect2<T>::lvr_rect2(const lvr_vector2<T>& a_min,lvr_vector2<T>& a_max)
:_min(a_min),_max(a_max)
{

}

template<class T>
lvr_rect2<T>::lvr_rect2(const lvr_vector2<T>& a_size)
:_min(a_size*-0.5),_max(a_size*0.5)
{

}

template<class T>
void lvr_rect2<T>::expand(const lvr_vector2<T>& a_v2)
{
	if(a_v2[0]<_min[0])
	{
		_min[0] = a_v2[0];
	}
	else if(a_v2[0] > _max[0])
	{
		_max[0] = a_v2[0];
	}

	if(a_v2[1]<_min[1])
	{
		_min[1] = a_v2[1];
	}
	else if(a_v2[1] > _max[1])
	{
		_max[1] = a_v2[1];
	}
}

template<class T>
bool lvr_rect2<T>::contains(const lvr_vector2<T>& a_v2) const
{
	return a_v2[0] >= _min[0] && a_v2[0] <= _max[0]
	&&	a_v2[1] >= _min[1] && a_v2[1] <= _max[1];
}

template<class T>
void lvr_rect2<T>::translate(const lvr_vector2<T>& a_mov)
{
	_min += a_mov;
	_max += a_mov;
}

template<class T>
void lvr_rect2<T>::combine(const lvr_rect2<T>& a_b)
{
	const lvr_vector2<T>& minb = a_b.get_min();
	const lvr_vector2<T>& maxb = a_b.get_max(); 
	_min[0] = LVR_MIN(_min[0],minb[0]);
	_min[1] = LVR_MIN(_min[1],minb[1]);

	_max[0] = LVR_MAX(_max[0],maxb[0]);
	_max[1] = LVR_MAX(_max[1],maxb[1]);
}

template<class T>
lvr_vector2<T>& lvr_rect2<T>::get_max()
{
	return _max;
}

template<class T>
lvr_vector2<T>& lvr_rect2<T>::get_min()
{
	return _min;
}

template<class T>
const lvr_vector2<T>& lvr_rect2<T>::get_max() const
{
	return _max;
}

template<class T>
const lvr_vector2<T>& lvr_rect2<T>::get_min() const
{
	return _min;
}

template<class T>
lvr_vector2<T> lvr_rect2<T>::get_center() const
{
	return (_max+_min)*T(0.5);
}

template<class T>
lvr_vector2<T> lvr_rect2<T>::get_size() const
{
	return _max-_min;
}
