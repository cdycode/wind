template<class T>
lvr_quaternion<T>::lvr_quaternion()
	:w(1),x(0),y(0),z(0)
{

}

template<class T>
lvr_quaternion<T>::lvr_quaternion(const T& a_x,const T& a_y,const T& a_z,const T& a_w)
	:w(a_w),x(a_x),y(a_y),z(a_z)
{

}

template<class T>
lvr_quaternion<T>::lvr_quaternion(const lvr_vector3<T>& a_axis,const T& a_angle_rad)
{
	set_axis_angle(a_axis, a_angle_rad);
}

template<class T>
lvr_quaternion<T>::lvr_quaternion(const lvr_vector3<T>& a_from,const lvr_vector3<T>& a_to)
{
	const T cx = a_from[1] * a_to[2] - a_from[2] * a_to[1];
	const T cy = a_from[2] * a_to[0] - a_from[0] * a_to[2];
	const T cz = a_from[0] * a_to[1] - a_from[1] * a_to[0];
	const T dot = a_from[0] * a_to[0] + a_from[1] * a_to[1] + a_from[2] * a_to[2];
	const T cross_length_sq = cx * cx + cy * cy + cz * cz;
	const T magnitude = sqrt( cross_length_sq + dot * dot );
	const T cw = dot + magnitude;
	if ( cw <  SmallestNonDenormal )
	{
		const T sx = a_to[1] * a_to[1] + a_to[2] * a_to[2];
		const T sz = a_to[0] * a_to[0] + a_to[1] * a_to[1];
		if ( sx > sz )
		{
			const T rcpLength = lvr_rcp_sqrt( sx );
			x = T(0);
			y = a_to[2] * rcpLength;
			z = - a_to[1] * rcpLength;
			w = T(0);
		}
		else
		{
			const T rcpLength = lvr_rcp_sqrt( sz );
			x = a_to[1] * rcpLength;
			y = - a_to[0] * rcpLength;
			z = T(0);
			w = T(0);
		}
		return;
	}
	const T rcpLength = lvr_rcp_sqrt( cross_length_sq + cw * cw );
	x = cx * rcpLength;
	y = cy * rcpLength;
	z = cz * rcpLength;
	w = cw * rcpLength;
}

template<class T>
T& lvr_quaternion<T>::operator[](int a_id)
{
	LVR_ASSERT(a_id >=0 && a_id <=3);
	return _m[a_id];
}

template<class T>
const T& lvr_quaternion<T>::operator[](int a_id) const
{
	LVR_ASSERT(a_id >=0 && a_id <=3);
	return _m[a_id];
}

template<class T>
lvr_quaternion<T> lvr_quaternion<T>::operator+(const lvr_quaternion<T>& a_q) const
{
	return lvr_quaternion<T>(x + a_q[1],y + a_q[2],z + a_q[3],w + a_q[0]);
}

template<class T>
void lvr_quaternion<T>::operator+=(const lvr_quaternion<T>& a_q)
{
	x += a_q[1];
	y += a_q[2];
	z += a_q[3];
	w += a_q[0];
}

template<class T>
lvr_quaternion<T> lvr_quaternion<T>::operator-(const lvr_quaternion<T>& a_q) const
{
	return lvr_quaternion<T>(x - a_q[1],y - a_q[2],z - a_q[3],w - a_q[0]);
}

template<class T>
void lvr_quaternion<T>::operator-=(const lvr_quaternion<T>& a_q)
{
	x -= a_q[1];
	y -= a_q[2];
	z -= a_q[3];
	w -= a_q[0];
}


template<class T>
lvr_quaternion<T> lvr_quaternion<T>::operator*(const T& a_s) const
{
	return lvr_quaternion<T>(x *a_s, y*a_s, z*a_s, w*a_s);
}

template<class T>
void lvr_quaternion<T>::operator*=(const T& a_s)
{
	x *= a_s;
	y *= a_s;
	z *= a_s;
	w *= a_s;
}

template<class T>
lvr_quaternion<T> lvr_quaternion<T>::operator*(const lvr_quaternion<T>& a_q) const
{
	const T& tx = a_q[1];
	const T& ty = a_q[2];
	const T& tz = a_q[3];
	const T& tw = a_q[0];
	return lvr_quaternion<T>(
		w * tx + x * tw + y * tz - z * ty,
		w * ty - x * tz + y * tw + z * tx,
		w * tz + x * ty - y * tx + z * tw,
		w * tw - x * tx - y * ty - z * tz
		);
}

template<class T>
lvr_vector3<T> lvr_quaternion<T>::operator*(const lvr_vector3<T>& a_v) const
{
	 return ((*this * lvr_quaternion<T>(a_v._x, a_v._y, a_v._z, T(0))) * get_inverse()).get_imag();
}

template<class T>
lvr_quaternion<T>& lvr_quaternion<T>::set_axis_angle(const lvr_vector3<T>& a_axis,const T& a_angle_rad)
{
	if (0 == a_axis.length_sq())
	{
		LVR_ASSERT(0 == a_angle_rad);
		x = T(0); y = T(0); z = T(0); w = T(1);
		return *this;
	}
	lvr_vector3<T> unit_axis(a_axis);
	unit_axis.normalize();
	T  sin_half_angle = sin(a_angle_rad * T(0.5));

	w = cos(a_angle_rad * T(0.5));
	x = unit_axis[0] * sin_half_angle;
	y = unit_axis[1] * sin_half_angle;
	z = unit_axis[2] * sin_half_angle;
	return *this;
}

template<class T>
T lvr_quaternion<T>::length() const
{
	return sqrt(length_sq());
}

template<class T>
T lvr_quaternion<T>::length_sq() const
{
	return (x*x + y*y + z*z + w*w);
}

template<class T>
void lvr_quaternion<T>::normalize()
{
	T t_l = length();
	if (t_l > LVR_TOLERANCE)
	{
		T rc_tl = T(1.0)/t_l;
		x *= rc_tl;
		y *= rc_tl;
		z *= rc_tl;
		w *= rc_tl;
	}
}

template<class T>
lvr_quaternion<T> lvr_quaternion<T>::get_normalize() const
{
	T t_l = length();
	if (t_l > LVR_TOLERANCE)
	{
		T rc_tl = T(1.0)/t_l;
		return lvr_quaternion<T>(x*rc_tl, y*rc_tl, z*rc_tl, w*rc_tl);
	}
	else
	{
		return *this;
	}
}

template<class T>
lvr_quaternion<T> lvr_quaternion<T>::get_conj() const
{
	return lvr_quaternion<T>(-x,-y,-z,w);
}

template<class T>
void lvr_quaternion<T>::inverse()
{
	x = -x;
	y = -y;
	z = -z;
}

template<class T>
lvr_quaternion<T> lvr_quaternion<T>::get_inverse() const
{
	return lvr_quaternion<T>(-x,-y,-z,w);
}
//There is no difference between get_inverse() and get_conj().So maybe one should delete.
template<class T>
lvr_vector3<T> lvr_quaternion<T>::get_imag() const
{
	return lvr_vector3<T>(x,y,z);
}
                                                                           
template<class T>
lvr_quaternion<T> lvr_quaternion<T>::lerp(const lvr_quaternion<T>& a_q0,const lvr_quaternion<T>& a_q1,const T& a_pos)
{
	T t_p = a_pos;
	lvr_clamp(t_p,T(0),T(1.0));
	return a_q0*(T(1.0)-t_p) + a_q1*t_p;

}

template<class T>
lvr_quaternion<T> lvr_quaternion<T>::slerp(const lvr_quaternion<T>& a_q1,const lvr_quaternion<T>& a_q2,const T& a_pos)
{
	// It contains no division operations, no trig, no inverse trig
	// and no sqrt. Not only does this code tolerate small constraint
	// errors in the input quaternions, it actually corrects for them.
	lvr_quaternion<T> dst_q;
	T t_ = a_pos;
	lvr_clamp(t_,T(0),T(1));

	if (a_q1[0] == a_q2[0] && a_q1[1] == a_q2[1] && a_q1[2] == a_q2[2] && a_q1[3] == a_q2[3])
	{
		dst_q._x = a_q1[1];
		dst_q._y = a_q1[2];
		dst_q._z = a_q1[3];
		dst_q._w = a_q1[0];
		return dst_q;
	}

	T halfY, alpha, beta;
	T u, f1, f2a, f2b;
	T ratio1, ratio2;
	T halfSecHalfTheta, versHalfTheta;
	T sqNotU, sqU;

	T cosTheta = a_q1[3] * a_q2[3] + a_q1[0] * a_q2[0] + a_q1[1] * a_q2[1] + a_q1[2] * a_q2[2];

	// As usual in all slerp implementations, we fold theta.
	alpha = cosTheta >= 0 ? 1.0f : -1.0f;
	halfY = 1.0f + alpha * cosTheta;

	// Here we bisect the interval, so we need to fold t_ as well.
	f2b = t_ - 0.5f;
	u = f2b >= 0 ? f2b : -f2b;
	f2a = u - f2b;
	f2b += u;
	u += u;
	f1 = 1.0f - u;

	// One iteration of Newton to get 1-cos(theta / 2) to good accuracy.
	halfSecHalfTheta = 1.09f - (0.476537f - 0.0903321f * halfY) * halfY;
	halfSecHalfTheta *= 1.5f - halfY * halfSecHalfTheta * halfSecHalfTheta;
	versHalfTheta = 1.0f - halfY * halfSecHalfTheta;

	// Evaluate series expansions of the coefficients.
	sqNotU = f1 * f1;
	ratio2 = 0.0000440917108f * versHalfTheta;
	ratio1 = -0.00158730159f + (sqNotU - 16.0f) * ratio2;
	ratio1 = 0.0333333333f + ratio1 * (sqNotU - 9.0f) * versHalfTheta;
	ratio1 = -0.333333333f + ratio1 * (sqNotU - 4.0f) * versHalfTheta;
	ratio1 = 1.0f + ratio1 * (sqNotU - 1.0f) * versHalfTheta;

	sqU = u * u;
	ratio2 = -0.00158730159f + (sqU - 16.0f) * ratio2;
	ratio2 = 0.0333333333f + ratio2 * (sqU - 9.0f) * versHalfTheta;
	ratio2 = -0.333333333f + ratio2 * (sqU - 4.0f) * versHalfTheta;
	ratio2 = 1.0f + ratio2 * (sqU - 1.0f) * versHalfTheta;

	// Perform the bisection and resolve the folding done earlier.
	f1 *= ratio1 * halfSecHalfTheta;
	f2a *= ratio2;
	f2b *= ratio2;
	alpha *= f1 + f2a;
	beta = f1 + f2b;

	// Apply final coefficients to a and b as usual.
	T tz = alpha * a_q1[3] + beta * a_q2[3];
	T tw = alpha * a_q1[0] + beta * a_q2[0];
	T tx = alpha * a_q1[1] + beta * a_q2[1];
	T ty = alpha * a_q1[2] + beta * a_q2[2];

	// This final adjustment to the quaternion's length corrects for
	// any small constraint error in the inputs q1 and q2 But as you
	// can see, it comes at the cost of 9 additional multiplication
	// operations. If this error-correcting feature is not required,
	// the following code may be removed.
	f1 = T(1.5) - T(0.5) * (tw * tw + tx * tx + ty * ty + tz * tz);
	dst_q[0] = tw * f1;
	dst_q[1] = tx * f1;
	dst_q[2] = ty * f1;
	dst_q[3] = tz * f1;
	return dst_q;
}

// Definitions of axes for coordinate and rotation conversions.
enum Axis
{
	Axis_X = 0, Axis_Y = 1, Axis_Z = 2
};
// RotateDirection describes the rotation direction around an axis, interpreted as follows:
//  CW  - Clockwise while looking "down" from positive axis towards the origin.
//  CCW - Counter-clockwise while looking from the positive axis towards the origin,
//        which is in the negative axis direction.
//  CCW is the default for the RHS coordinate system. Oculus standard RHS coordinate
//  system defines Y up, X right, and Z back (pointing out from the screen). In this
//  system Rotate_CCW around Z will specifies counter-clockwise rotation in XY plane.
enum RotateDirection
{
	Rotate_CCW = 1,
	Rotate_CW  = -1 
};
// Constants for right handed and left handed coordinate systems
enum HandedSystem
{
	Handed_R = 1, Handed_L = -1
};

// GetEulerAngles extracts Euler angles from the quaternion, in the specified order of
// axis rotations and the specified coordinate system. Right-handed coordinate system
// is the default, with CCW rotations while looking in the negative axis direction.
// Here a,b,c, are the Yaw/Pitch/Roll angles to be returned.
// rotation a around axis A1
// is followed by rotation b around axis A2
// is followed by rotation c around axis A3
// rotations are CCW or CW (D) in LH or RH coordinate system (S)
template <class T,Axis A1, Axis A2, Axis A3, RotateDirection D, HandedSystem S>
void GetEulerAngles( const lvr_quaternion<T>* a_q,T *a, T *b, T *c)
{
	T Q[3] = { a_q->x, a_q->y, a_q->z };  //Quaternion components x,y,z
	T w = a_q->w;
	T ww  = w*w;
	T Q11 = Q[A1]*Q[A1];
	T Q22 = Q[A2]*Q[A2];
	T Q33 = Q[A3]*Q[A3];

	T psign = T(-1);
	// Determine whether even permutation
	if (((A1 + 1) % 3 == A2) && ((A2 + 1) % 3 == A3))
		psign = T(1);

	T s2 = psign * T(2) * (psign*w*Q[A2] + Q[A1]*Q[A3]);

	if (s2 < T(-1) + T(LVR_TOLERANCE))
	{ // South pole singularity
		*a = T(0);
		*b = -S*D*T(LVR_HALF_PI);
		*c = S*D*atan2(T(2)*(psign*Q[A1]*Q[A2] + w*Q[A3]),
			ww + Q22 - Q11 - Q33 );
	}
	else if (s2 > T(1) - T(LVR_TOLERANCE))
	{  // North pole singularity
		*a = T(0);
		*b = S*D*T(LVR_HALF_PI);
		*c = S*D*atan2(T(2)*(psign*Q[A1]*Q[A2] + w*Q[A3]),
			ww + Q22 - Q11 - Q33);
	}
	else
	{
		*a = -S*D*atan2(T(-2)*(w*Q[A1] - psign*Q[A2]*Q[A3]),
			ww + Q33 - Q11 - Q22);
		*b = S*D*asin(s2);
		*c = S*D*atan2(T(2)*(w*Q[A3] - psign*Q[A1]*Q[A2]),
			ww + Q11 - Q22 - Q33);
	}
}

template<class T>
void lvr_quaternion<T>::get_axis_angle(lvr_vector3<T>& axis,T& ao_angle) const
{
	if ( x*x + y*y + z*z > LVR_TOLERANCE*LVR_TOLERANCE ) {
		axis  = lvr_vector3<T>(x, y, z).get_normalize();
		ao_angle = 2 * lvr_acos(w);
		if (ao_angle > LVR_PI) // Reduce the magnitude of the angle, if necessary
		{
			ao_angle = T(LVR_TWO_PI) - ao_angle;
			axis = axis * static_cast<T>(-1);
		}
	}
	else 
	{
		axis = lvr_vector3<T>(static_cast<T>(1), static_cast<T>(0), static_cast<T>(0));
		ao_angle= 0;
	}
}

template<class T>
void lvr_quaternion<T>::get_euler_angles(T& a,T& b, T& c) const
{
	GetEulerAngles<T, Axis_Y, Axis_X, Axis_Z, Rotate_CCW, Handed_R>(this, &b, &a, &c);
}

template<class T>
bool lvr_quaternion<T>::is_nan() const
{
	return w !=w || x != x || y != y || z != z;
}

template<class T>
lvr_quaternion<T> lvr_quaternion<T>::make_euler_quat(const T& a_pitch,const T& a_yaw,const T& a_roll)
{
	return lvr_quaternion<T>(lvr_vector3<T>(T(0.0), T(1.0), T(0.0)), a_yaw)*
		lvr_quaternion<T>(lvr_vector3<T>(T(1.0), T(0.0), T(0.0)), a_pitch)*
		lvr_quaternion<T>(lvr_vector3<T>(T(0.0),T(0.0),T(1.0)),a_roll);
}