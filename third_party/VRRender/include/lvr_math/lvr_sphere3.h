#ifndef __lvr_sphere3_h__
#define __lvr_sphere3_h__

#include "lvr_ray3.h"
#include"lvr_vector3.h"
template<class T>
class lvr_sphere3
{
public:
	lvr_sphere3();
	lvr_sphere3(T a_r);
	lvr_sphere3(const lvr_vector3<T>& a_pos,T a_r);
	~lvr_sphere3(){}
public:
	lvr_vector3<T>	get_pos()const;
	T	get_radius()const;
	void set_pos(const lvr_vector3<T>& a_pos);
	void set_radius(T a_r);

	int intersect(const lvr_ray3<T>& a_ray3,T& ao_dist);
private:
	lvr_vector3<T>	_pos;
	T				_radius;
};

#include "lvr_sphere3.inl"

typedef lvr_sphere3<float>	lvr_sphere3f;
typedef lvr_sphere3<double> lvr_sphere3d;

#endif
