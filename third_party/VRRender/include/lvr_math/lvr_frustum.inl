template<class T>
lvr_intersect_status lvr_frustum<T>::sphere_intersect(const lvr_vector3<T>& a_pos,T a_r) const
{
	lvr_vector3<T> view_pos = a_pos - _render_origin;
	int inside_num = 0;
	double t_dist = _near_plane.distance_to_point(view_pos);
	if ( t_dist<= -a_r)
	{
		return e_intersect_leave;
	}
	else if (t_dist > a_r)
	{
		++inside_num;
	}
	t_dist = _far_plane.distance_to_point(view_pos);
	if ( t_dist <= -a_r)
	{
		return e_intersect_leave;
	}
	else if (t_dist > a_r)
	{
		++inside_num;
	}
	t_dist = _top_plane.distance_to_point(view_pos);
	if ( t_dist<= -a_r)
	{
		return e_intersect_leave;
	}
	else if (t_dist > a_r)
	{
		++inside_num;
	}
	t_dist = _bottom_plane.distance_to_point(view_pos);
	if ( t_dist <= -a_r)
	{
		return e_intersect_leave;
	}
	else if (t_dist > a_r)
	{
		++inside_num;
	}
	t_dist = _left_plane.distance_to_point(view_pos);
	if ( t_dist <= -a_r)
	{
		return e_intersect_leave;
	}
	else if (t_dist > a_r)
	{
		++inside_num;
	}
	t_dist = _right_plane.distance_to_point(view_pos);
	if ( t_dist <= -a_r)
	{
		return e_intersect_leave;
	}
	else if (t_dist > a_r)
	{
		++inside_num;
	}
	if (6 == inside_num)
	{
		return e_intersect_inside;
	}
	return e_intersect_part;
}

template<class T>
void lvr_frustum<T>::set_frustum(const lvr_matrix4<T>& a_proj_view_matrix,const lvr_vector3<T>& a_render_origin)
{
	_mat_proj_view = a_proj_view_matrix;
	_render_origin = a_render_origin;
	update_planes();
}

template<class T>
void lvr_frustum<T>::update_planes()
{
	lvr_vector3<T>	t_normal(_mat_proj_view(3,0) + _mat_proj_view(2,0), _mat_proj_view(3,1) + _mat_proj_view(2,1), _mat_proj_view(3,2) + _mat_proj_view(2,2));
	T	t_l = t_normal.normalize();
	_near_plane.set_normal(t_normal);
	_near_plane.set_distance_to_origin((_mat_proj_view(3,3) + _mat_proj_view(2,3))/t_l);

	t_normal = lvr_vector3<T>(_mat_proj_view(3,0) - _mat_proj_view(2,0), _mat_proj_view(3,1) - _mat_proj_view(2,1), _mat_proj_view(3,2) - _mat_proj_view(2,2));
	t_l = t_normal.normalize();
	_far_plane.set_normal(t_normal);
	_far_plane.set_distance_to_origin((_mat_proj_view(3,3) - _mat_proj_view(2,3))/t_l);

	t_normal = lvr_vector3<T>(_mat_proj_view(3,0) + _mat_proj_view(1,0), _mat_proj_view(3,1) + _mat_proj_view(1,1), _mat_proj_view(3,2) + _mat_proj_view(1,2)); 
	t_l = t_normal.normalize();
	_bottom_plane.set_normal(t_normal);
	_bottom_plane.set_distance_to_origin((_mat_proj_view(3,3) + _mat_proj_view(1,3))/t_l);

	t_normal = lvr_vector3<T>(_mat_proj_view(3,0) - _mat_proj_view(1,0), _mat_proj_view(3,1) - _mat_proj_view(1,1), _mat_proj_view(3,2) - _mat_proj_view(1,2));
	t_l = t_normal.normalize();
	_top_plane.set_normal(t_normal);
	_top_plane.set_distance_to_origin((_mat_proj_view(3,3) - _mat_proj_view(1,3))/t_l);

	t_normal = lvr_vector3<T>(_mat_proj_view(3,0) + _mat_proj_view(0,0), _mat_proj_view(3,1) + _mat_proj_view(0,1), _mat_proj_view(3,2) + _mat_proj_view(0,2));
	t_l = t_normal.normalize();
	_left_plane.set_normal(t_normal);
	_left_plane.set_distance_to_origin((_mat_proj_view(3,3) + _mat_proj_view(0,3))/t_l);

	t_normal = lvr_vector3<T>(_mat_proj_view(3,0) - _mat_proj_view(0,0), _mat_proj_view(3,1) - _mat_proj_view(0,1), _mat_proj_view(3,2) - _mat_proj_view(0,2));
	t_l = t_normal.normalize();
	_right_plane.set_normal(t_normal);
	_right_plane.set_distance_to_origin((_mat_proj_view(3,3) - _mat_proj_view(0,3))/t_l);
}
