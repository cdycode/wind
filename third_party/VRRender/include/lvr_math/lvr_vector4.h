#ifndef __lvr_vector4_h__
#define __lvr_vector4_h__

#include "lvr_math_configure.h"

#include "lvr_vector3.h"

template<class T>
class lvr_vector4
{
public:
	union
	{
		struct{
			T _x,_y,_z,_w;
		};
		T _m[4];
	};
	lvr_vector4():_x(0),_y(0),_z(0),_w(0){}
	explicit lvr_vector4(T a_v):_x(a_v),_y(a_v),_z(a_v),_w(a_v){}
	lvr_vector4(T a_x,T a_y,T a_z, T a_w):_x(a_x),_y(a_y),_z(a_z),_w(a_w){}
	lvr_vector4(const lvr_vector3<T>& a_v3,T a_w)
		:_x(a_v3[0]),_y(a_v3[1]),_z(a_v3[2]),_w(a_w){}
	~lvr_vector4(){}
public:
	T& operator[](int a_id);
	const T& operator[](int a_id) const;

	lvr_vector4<T> operator +(const lvr_vector4<T>& a_v4) const;
	lvr_vector4<T> operator -(const lvr_vector4<T>& a_v4) const;
	T operator *(const lvr_vector4<T>& a_v4) const;
	lvr_vector4<T> operator *(T a_scale) const;
	lvr_vector4<T> operator /(T a_scale) const;

	void operator +=(const lvr_vector4<T>& a_v4);
	void operator -=(const lvr_vector4<T>& a_v4);
	void operator *=(T a_scale);
	void operator /=(T a_scale);

	bool operator <(const lvr_vector4<T>& a_v2)const;
public:
	lvr_vector3<T> xyz()const;
	T length()const;
	T length_sq()const;
	void normalize();
	lvr_vector4<T> get_normalize()const;
	lvr_vector4<T> cross(const lvr_vector4<T>& a_v4) const;
	lvr_vector4<T> multi(const lvr_vector4<T>& a_v4) const;

	lvr_vector4<T> lerp(const lvr_vector4<T>& a_v4,T a_pos)const;

	bool	is_nan( ) const;
private:
	//union
	//{
	//	struct{
	//		T _x,_y,_z,_w;
	//	};
	//	T _m[4];
	//};
};

#include "lvr_vector4.inl"

typedef lvr_vector4<uint8_t> lvr_vector4uc;
typedef lvr_vector4<float> lvr_vector4f;
typedef lvr_vector4<double> lvr_vector4d;
typedef lvr_vector4<int> lvr_vector4i;
typedef lvr_vector4<short> lvr_vector4s;

#endif
