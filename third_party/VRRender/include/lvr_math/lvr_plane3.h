#ifndef __lvr_plane3_h__
#define __lvr_plane3_h__

#include"lvr_vector3.h"

template<class T>
class lvr_plane3
{
public:
	lvr_plane3();
	lvr_plane3(const lvr_plane3<T>& a_plane);
	lvr_plane3(const lvr_vector3<T> a_normal, T a_dist_to_origin);
	~lvr_plane3(){}
public:
	const lvr_vector3<T>& get_normal()const;
	void	set_normal(const lvr_vector3<T>& a_normal);
	T		get_distance_to_origin()const;
	void	set_distance_to_origin(T a_v);
	T	distance_to_point(const lvr_vector3<T>& a_point) const;
	bool project_vector(lvr_vector3<T>& ao_dst_vector, const lvr_vector3<T>& a_src_vector)const;
	bool intersect(T& ao_dist, const lvr_vector3<T> a_ray_origin, const lvr_vector3<T> a_ray_dir)const;
private:
	union
	{
		struct
		{
			lvr_vector3<T>	_normal;
			T				_distance_to_origin;
		};
		T		_m[4];
	};
	
};

template<class T>
lvr_plane3<T>::lvr_plane3(const lvr_plane3<T>& a_plane)
{
	this->_normal = a_plane._normal;
	this->_distance_to_origin = a_plane._distance_to_origin;
}

typedef lvr_plane3<float> lvr_plane3f;
typedef lvr_plane3<double> lvr_plane3d;
typedef lvr_plane3<int> lvr_plane3i;                       //Here I add two statements.

#include "lvr_plane3.inl"


#endif
