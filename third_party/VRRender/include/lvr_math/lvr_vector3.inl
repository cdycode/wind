#include "lvr_vector3.h"

template<class T>
T& lvr_vector3<T>::operator[](int a_id)
{
	LVR_ASSERT(a_id < 3 && a_id >= 0);
	return _m[a_id];
}

template<class T>
const T& lvr_vector3<T>::operator[](int a_id) const
{
	LVR_ASSERT(a_id < 3 && a_id >= 0);
	return _m[a_id];
}

template<class T>
lvr_vector3<T> lvr_vector3<T>::operator+(const lvr_vector3& a_v3) const
{
	return lvr_vector3<T>(_x + a_v3[0],_y + a_v3[1],_z + a_v3[2]);
}

template<class T>
lvr_vector3<T> lvr_vector3<T>::operator-(const lvr_vector3& a_v3) const
{
	return lvr_vector3<T>(_x - a_v3[0],_y - a_v3[1],_z - a_v3[2]);
}

template<class T>
lvr_vector3<T> lvr_vector3<T>::operator-() const
{
	return lvr_vector3<T>(-_x, -_y, -_z);
}

template<class T>
T lvr_vector3<T>::operator*(const lvr_vector3& a_v3) const
{
	return _x*a_v3[0] + _y*a_v3[1] + _z*a_v3[2];
}

template<class T>
lvr_vector3<T> lvr_vector3<T>::operator*(T a_scale) const
{
	return lvr_vector3<T>(_x*a_scale,_y*a_scale,_z*a_scale);
}

template<class T>
lvr_vector3<T> lvr_vector3<T>::operator/(T a_scale) const
{
	return lvr_vector3(_x/a_scale,_y/a_scale,_z/a_scale);
}

template<class T>
void lvr_vector3<T>::operator+=(const lvr_vector3& a_v3)
{
	_x += a_v3[0];
	_y += a_v3[1];
	_z += a_v3[2];
}

template<class T>
void lvr_vector3<T>::operator-=(const lvr_vector3& a_v3)
{
	_x -= a_v3[0];
	_y -= a_v3[1];
	_z -= a_v3[2];
}

template<class T>
void lvr_vector3<T>::operator*=(T a_scale)
{
	_x *= a_scale;
	_y *= a_scale;
	_z *= a_scale;
}

template<class T>
void lvr_vector3<T>::operator/=(T a_scale)
{
	_x /= a_scale;
	_y /= a_scale;
	_z /= a_scale;
}

template<class T>
bool lvr_vector3<T>::operator<(const lvr_vector3<T>& a_v2) const
{
	if (_x < a_v2[0])
	{
		return true;
	}
	else if (_x==a_v2[0])
	{
		if (_y<a_v2[1])
		{
			return true;
		}
		else if (_y == a_v2[1] && _z < a_v2[2])
		{
			return true;
		}
	}
	return false;
}

template<class T>
T lvr_vector3<T>::length() const
{
	return sqrt(length_sq());
}

template<class T>
T lvr_vector3<T>::length_sq() const
{
	return _x*_x + _y*_y + _z*_z;
}

template<class T>
T lvr_vector3<T>::normalize()
{
	T t_l = length();
	LVR_ASSERT(t_l > LVR_TOLERANCE);
	*this /= t_l;
	return t_l;
}

template<class T>
lvr_vector3<T> lvr_vector3<T>::get_normalize() const
{
	T t_l = length();
	LVR_ASSERT(t_l > LVR_TOLERANCE);
	return lvr_vector3<T>(_x/t_l,_y/t_l,_z/t_l);
}

template<class T>
lvr_vector3<T> lvr_vector3<T>::cross(const lvr_vector3<T>& a_v3) const
{
	return lvr_vector3<T>(_y*a_v3[2] - _z*a_v3[1],
						_z*a_v3[0] - _x*a_v3[2],
						_x*a_v3[1] - _y*a_v3[0]);
}

template<class T>
lvr_vector3<T> lvr_vector3<T>::multi(const lvr_vector3<T>& a_v3) const
{
	return lvr_vector3<T>(_m[0] * a_v3[0], _m[1] * a_v3[1], _m[2] * a_v3[2]);
}

template<class T>
lvr_vector3<T> lvr_vector3<T>::lerp(const lvr_vector3<T>& a_v3,T a_pos) const
{
	return lvr_vector3<T>(_x*a_pos + a_v3[0]*(1.0-a_pos),_y*a_pos + a_v3[1]*(1.0-a_pos),_z*a_pos + a_v3[2]*(1.0-a_pos));
}

template<class T>
bool lvr_vector3<T>::is_nan() const
{
	return _x != _x || _y != _y || _z != _z;
}