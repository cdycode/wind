
template<class T>
lvr_sphere3<T>::lvr_sphere3()
:_pos(T(0)),_radius(T(1))
{

}

template<class T>
lvr_sphere3<T>::lvr_sphere3(T a_r)
:_pos(T(0)),_radius(a_r)
{

}

template<class T>
lvr_sphere3<T>::lvr_sphere3(const lvr_vector3<T>& a_pos,T a_r)
:_pos(a_pos),_radius(a_r)
{

}

template<class T>
lvr_vector3<T> lvr_sphere3<T>::get_pos() const
{
	return _pos;
}

template<class T>
T lvr_sphere3<T>::get_radius() const
{
	return _radius;
}

template<class T>
void lvr_sphere3<T>::set_pos(const lvr_vector3<T>& a_pos)
{
	_pos = a_pos;
}

template<class T>
void lvr_sphere3<T>::set_radius(T a_r)
{
	_radius = a_r;
}

template<class T>
int lvr_sphere3<T>::intersect(const lvr_ray3<T>& a_ray3,T& ao_dist)
{
	int r_count_num = 0;
	lvr_vector3<T> t_diff = a_ray3.get_origin() - _pos;
	T t_fa0 = t_diff*t_diff - _radius*_radius;
	T t_fa1 = a_ray3.get_dir()*t_diff;
	if (t_fa0 <= T(0))//inside the sphere.so there must be intersect.
	{
		r_count_num = 1;
		T t_fdiscr = t_fa1*t_fa1 - t_fa0;
		if (t_fdiscr >= LVR_TOLERANCE)
		{
			T froot = sqrt(t_fdiscr);
			if (t_fa1 >= 0.0)
			{
				ao_dist = (-t_fa1) - froot;
			}
			else
			{
				ao_dist = (-t_fa1) + froot;
			}
		}
		else
		{
			ao_dist = 0;
		}
		return r_count_num;
	}
	else if (t_fa1 > T(0))
	{
		return r_count_num;
	}

	T fdiscr = t_fa1*t_fa1 - t_fa0;
	if (fdiscr >= LVR_TOLERANCE)
	{
		T t_froot = sqrt(fdiscr);
		T t0 = (-t_fa1) - t_froot;
		//T t1 = (-fa1) + froot;
		ao_dist = t0;
		r_count_num = 2;
	}
	else
	{
		if (fdiscr >= -LVR_TOLERANCE)
		{ 
			T t0 = -t_fa1;
			ao_dist = t0;
			r_count_num = 1;
		}
		//else case,the ray is not intersect with the sphere.point fay away.
		//for closest point calculate is expensive,so we do not do it here.
	}
	return r_count_num;
}