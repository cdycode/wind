#ifndef __lvr_matrix3_h__
#define __lvr_matrix3_h__

#include "lvr_vector3.h"

template<class T>class lvr_quaternion;
//row matrix,when using finally to openGL ,do a transpose
template<class T>
class lvr_matrix3
{
public:
	lvr_matrix3();
	lvr_matrix3(T a_v);
	lvr_matrix3(const lvr_vector3<T>& a_v3);
	lvr_matrix3(const lvr_matrix3<T>& a_mat);
	lvr_matrix3(const lvr_vector3<T>& a_v0,const lvr_vector3<T>& a_v1,const lvr_vector3<T>& a_v2);
	lvr_matrix3(T a_m00,T a_m01,T a_m02,
				T a_m10,T a_m11,T a_m12,
				T a_m20,T a_m21,T a_m22);
	~lvr_matrix3(){}

public:
	T& operator[](int a_id);
	const T& operator[](int a_id)const;
	T& operator()(int a_row,int a_col);
	const T& operator()(int a_row,int a_col)const;

	lvr_vector3<T> operator*(const lvr_vector3<T>& a_v3)const;
	lvr_matrix3 operator*(const T& a_s) const;
	void operator*=(const T& a_s);
	lvr_matrix3 operator/(const T& a_s) const;
	void operator/=(const T& a_s);
	lvr_matrix3 operator*(const lvr_matrix3<T>& a_mat3)const;
public:
	void be_identity();
	T	determinant() const;
	bool inverse();
	lvr_matrix3 get_inverse()const;
	lvr_matrix3 get_transpose()const;
	void transpose();
	bool to_quaternion(lvr_quaternion<T>& ao_q)const;
	void from_quaternion(const lvr_quaternion<T>& a_q);
public:
	static void mul(const lvr_matrix3<T>& a_mat0,const lvr_matrix3<T>& a_mat1,lvr_matrix3<T>& a_mat2);
	static lvr_matrix3<T> axis_angle_rotate(const lvr_vector3<T>& a_axis,T a_angle_rad);
	static lvr_matrix3<T> make_rotation_matrix_around_axis(const lvr_vector3<T>& a_axis, T a_angle_rad);
	static lvr_matrix3<T> make_rotate_from_to_matrix(const lvr_vector3<T>& a_src, const lvr_vector3<T> a_dst);
private:
	union
	{
		struct
		{
			lvr_vector3<T>	_x,_y,_z;
		};
		T _m[9];
		T _mm[3][3];
	};

};

template<class T>
lvr_matrix3<T>::lvr_matrix3(const lvr_matrix3<T>& a_mat)
{
	for (int i = 0; i < 9; ++i)
	{
		this->_m[i] = a_mat[i];
	}
}

#include "lvr_matrix3.inl"

typedef lvr_matrix3<float> lvr_matrix3f;
typedef lvr_matrix3<double> lvr_matrix3d;

#endif
