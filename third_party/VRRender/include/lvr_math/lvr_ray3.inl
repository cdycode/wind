
template<class T>
lvr_ray3<T>::lvr_ray3()
:_origin_point(T(0),T(0),T(0)),_dir_look(T(1),T(0),T(0))
{

}

template<class T>
lvr_ray3<T>::lvr_ray3(const lvr_vector3<T>& a_origin,const lvr_vector3<T>& a_dir)
:_origin_point(a_origin),_dir_look(a_dir)
{

}

template<class T>
lvr_vector3<T> lvr_ray3<T>::get_origin() const
{
	return _origin_point;
}

template<class T>
lvr_vector3<T> lvr_ray3<T>::get_dir() const
{
	return _dir_look;
}

template<class T>
void lvr_ray3<T>::set_origin(const lvr_vector3<T>& a_origin)
{
	_origin_point = a_origin;
}

template<class T>
void lvr_ray3<T>::set_dir(const lvr_vector3<T>& a_dir)
{
	_dir_look = a_dir;
	_dir_look.normalize();
}