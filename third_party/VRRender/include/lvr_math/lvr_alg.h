#ifndef __lvr_alg_h__
#define __lvr_alg_h__

#include "lvr_vector2.h"
#include "lvr_vector3.h"

template<class T>
bool lvr_ray_triangle_intersect(T& ao_dist,const lvr_vector3<T>& a_origin,const lvr_vector3<T>& a_dir,
	const lvr_vector3<T>& a_p0,const lvr_vector3<T>& a_p1,const lvr_vector3<T>& a_p2);

template<class T>
bool lvr_get_circumcircle_center(lvr_vector2<T>& ao_center,const lvr_vector2<T>& a_pa,
								const lvr_vector2<T>& a_pb,const lvr_vector2<T>& a_pc);
template<class T>
bool lvr_get_circumcircle_center(lvr_vector3<T>& ao_center,const lvr_vector3<T>& a_pa,
	const lvr_vector3<T>& a_pb,const lvr_vector3<T>& a_pc);

// Safe reciprocal square root.
template<class T>
T lvr_rcp_sqrt( const T f ) { return ( f >= SmallestNonDenormal ) ? T(1) / sqrt( f ) : HugeNumber; }

template <typename T>
inline T
lvr_sign(T const & x)
{
	return x < T(0) ? T(-1) : (x > T(0) ? T(1) : T(0));
}

template <typename T>
T lvr_clip(const T& n, const T& lower, const T& upper) {
	return lvr_max(lower, lvr_min(n, upper));
}

template<class T>
T lvr_random( const T& a_min,const T& a_max ) 
{
#ifdef LVR_OS_WIN32
	int t_r = rand()%1000;
#else
	int t_r = lrand48()%1000;
#endif
	float t_s = t_r*0.001f;
	return T(a_min*(1.0f-t_s) + a_max*t_s);
}

template<class T>
T lvr_mix( const T& a0,const T& a1,float t_s) 
{
	return a0*(1.0f-t_s) + a1*t_s;
}

template<class TC,typename T>
void lvr_bezier2(TC& ao_pt,const TC& a_begin_pt,const TC& a_ctrl_pt,const TC& a_end_pt,T t)
{
	 ao_pt = a_begin_pt*((T(1)-t)*(T(1)-t)) + a_ctrl_pt*(T(2)*t*(T(1)-t)) + a_end_pt*(t*t);
}

template<class TC, typename T>
void lvr_bezier_dir2(TC& ao_pt, const TC& a_begin_pt, const TC& a_ctrl_pt, const TC& a_end_pt, T t)
{
	ao_pt = a_begin_pt*(T(2)*t - T(2)) + a_ctrl_pt*(T(2) - T(4)*t) + a_end_pt*(T(2)*t);
}


template<class TC,typename T>
void lvr_bezier3(TC& ao_pt,const TC& a_begin_pt,const TC& a_ctrl_pt0,const TC& a_ctrl_pt1,const TC& a_end_pt,T t)
{
	T ft2 = t*t;
	T ft3 = ft2*t;
	T ft22 = (T(1)-t)*(T(1)-t);
	T ft23 = ft22*(T(1)-t);
	ao_pt = a_begin_pt*ft23 + a_ctrl_pt0*T(3)*t*ft22 + a_ctrl_pt1*T(3)*ft2*(T(1)-t)+a_end_pt*ft3;
}

template<class TC, typename T>
void lvr_bezier_dir3(TC& ao_pt, const TC& a_begin_pt, const TC& a_ctrl_pt0, const TC& a_ctrl_pt1, const TC& a_end_pt, T t)
{
	T ft1 = T(6)*t -T(3)*t*t - T(3);
	T ft2 = T(9)*t*t - T(12)*t + T(3);
	T ft3 = T(6)*t - T(9)*t*t;
	T ft4 = T(3)*t*t;
	ao_pt = a_begin_pt*ft1 + a_ctrl_pt0*ft2 + a_ctrl_pt1*ft3 + a_end_pt*ft4;
}

template<typename T>
T lvr_angle_fromxy(T x, T y);

#include "lvr_alg.inl"

#endif

