#ifndef __lvr_vector2_h__
#define __lvr_vector2_h__

#include "lvr_math_configure.h"

template<class T>
class lvr_vector2
{
public:
	union
	{
		struct{
			T _x,_y;
		};
		T _m[2];
	};
	lvr_vector2():_x(0),_y(0){}
	explicit lvr_vector2(T a_v):_x(a_v),_y(a_v){}
	lvr_vector2(T a_x,T a_y):_x(a_x),_y(a_y){}
	~lvr_vector2(){}
public:
	T& operator[](int a_id);
	const T& operator[](int a_id) const;

	lvr_vector2<T> operator +(const lvr_vector2<T>& a_v2) const;
	lvr_vector2<T> operator -(const lvr_vector2<T>& a_v2) const;
	T operator *(const lvr_vector2<T>& a_v2) const;
	lvr_vector2<T> operator *(T a_scale) const;
	lvr_vector2<T> operator /(T a_scale) const;

	void operator +=(const lvr_vector2<T>& a_v2);
	void operator -=(const lvr_vector2<T>& a_v2);
	void operator *=(T a_scale);
	void operator /=(T a_scale);
	bool operator <(const lvr_vector2<T>& a_v2)const;

public:

	T length()const;
	T length_sq()const;
	void normalize();
	lvr_vector2<T> get_normlize()const;

	lvr_vector2<T> lerp(const lvr_vector2<T>& a_v2,T a_pos)const;
	bool	is_nan( ) const;
private:
	/*union
	{
		struct{
			T _x,_y;
		};
		T _m[2];
	};*/
};

#include "lvr_vector2.inl"

typedef lvr_vector2<float> lvr_vector2f;
typedef lvr_vector2<double> lvr_vector2d;
typedef lvr_vector2<int> lvr_vector2i;
typedef lvr_vector2<short> lvr_vector2s;

#endif
