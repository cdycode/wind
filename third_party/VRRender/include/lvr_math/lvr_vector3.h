#ifndef __lvr_vector3_h__
#define __lvr_vector3_h__

#include "lvr_math_configure.h"

template<class T>
class lvr_vector3
{
public:
	lvr_vector3():_x(0),_y(0),_z(0){}
	explicit lvr_vector3(T a_v):_x(a_v),_y(a_v),_z(a_v){}
	lvr_vector3(T a_x,T a_y,T a_z):_x(a_x),_y(a_y),_z(a_z){}
	~lvr_vector3(){}
public:
	T& operator[](int a_id);
	const T& operator[](int a_id) const;

	lvr_vector3<T> operator +(const lvr_vector3<T>& a_v3) const;
	lvr_vector3<T> operator -(const lvr_vector3<T>& a_v3) const;
	lvr_vector3<T> operator -() const;
	T operator *(const lvr_vector3<T>& a_v3) const;
	lvr_vector3<T> operator *(T a_scale) const;
	lvr_vector3<T> operator /(T a_scale) const;

	void operator +=(const lvr_vector3<T>& a_v3);
	void operator -=(const lvr_vector3<T>& a_v3);
	void operator *=(T a_scale);
	void operator /=(T a_scale);

	bool operator <(const lvr_vector3<T>& a_v2)const;
public:

	T length()const;
	T length_sq()const;
	T normalize();
	lvr_vector3<T> get_normalize()const;
	lvr_vector3<T> cross(const lvr_vector3<T>& a_v3) const;
	lvr_vector3<T> multi(const lvr_vector3<T>& a_v3) const;
	lvr_vector3<T> lerp(const lvr_vector3<T>& a_v3,T a_pos)const;
	bool	is_nan( ) const;
public:
	union
	{
		struct{
			T _x,_y,_z;
		};
		T _m[3];
	};
};

#include "lvr_vector3.inl"
typedef lvr_vector3<unsigned char> lvr_vector3uc;
typedef lvr_vector3<unsigned short> lvr_vector3us;
typedef lvr_vector3<unsigned int> lvr_vector3ui;
typedef lvr_vector3<short> lvr_vector3s;
typedef lvr_vector3<int> lvr_vector3i;
typedef lvr_vector3<float> lvr_vector3f;
typedef lvr_vector3<double> lvr_vector3d;

#endif
