#ifndef __lvr_matrix4_h__
#define __lvr_matrix4_h__

#include "lvr_vector4.h"
template<class T>class lvr_quaternion;

template<class T>
class lvr_matrix4
{
public:
	lvr_matrix4();
	lvr_matrix4(T a_v);
	lvr_matrix4(const lvr_vector4<T>& a_v);
	lvr_matrix4(const lvr_vector4<T>& a_v0,const lvr_vector4<T>& a_v1,const lvr_vector4<T>& a_v2,const lvr_vector4<T>& a_v3);
	lvr_matrix4(const lvr_matrix4<T>& a_mat);
	lvr_matrix4(T a_m00,T a_m01,T a_m02,T a_m03,
		T a_m10,T a_m11,T a_m12,T a_m13,
		T a_m20,T a_m21,T a_m22,T a_m23,
		T a_m30,T a_m31,T a_m32,T a_m33);
	~lvr_matrix4(){}

public:
	T& operator[](int a_id);
	const T& operator[](int a_id)const;
	T& operator()(int a_row,int a_col);
	const T& operator()(int a_row,int a_col)const;

	lvr_vector4<T> operator*(const lvr_vector4<T>& a_vec4)const;
	lvr_vector3<T> operator*(const lvr_vector3<T>& a_vec3)const;
	lvr_matrix4<T> operator*(const T& a_s) const;
	void operator*=(const T& a_s);
	lvr_matrix4<T> operator/(const T& a_s) const;
	void operator/=(const T& a_s);
	lvr_matrix4<T> operator*(const lvr_matrix4<T>& a_mat4)const;
	lvr_matrix4<T> operator+(const lvr_matrix4<T>& a_mat4)const;
	void operator+=(const lvr_matrix4<T>& a_mat4);
	lvr_matrix4<T> operator-(const lvr_matrix4<T>& a_mat4)const;
	void operator-=(const lvr_matrix4<T>& a_mat4);
public:
	//this two method is only for working with mvp projection,should be considered.
	lvr_vector3<T> transform_vector(const lvr_vector3<T>& a_vec3)const;
	lvr_vector3<T> transform_point(const lvr_vector3<T>& a_vec3)const;
	T	determinant() const;
	bool inverse();
	bool get_inverse(lvr_matrix4<T>& ao_mat4)const;
	lvr_matrix4<T> get_transpose()const;
	void transpose();
	lvr_quaternion<T> to_quaternion()const;
	void from_quaternion(const lvr_quaternion<T>& a_q);

	bool decompose(lvr_vector3<T>* ao_scale,lvr_quaternion<T>* a_rotation,lvr_vector3<T>* a_translation)const;

	void set_translation(const lvr_vector3<T>& a_trans);
	lvr_vector3<T> get_translation();
public:
	static void mul(const lvr_matrix4<T>& a_mat0,const lvr_matrix4<T>& a_mat1,lvr_matrix4<T>& ao_mat);
	static lvr_matrix4<T> make_translation_matrix(const lvr_vector3<T>& a_trans);
	static lvr_matrix4<T> axis_angle_rotate_by_center(const lvr_vector3<T>& a_rot_center,const lvr_vector3<T>& a_axis,T a_angle_rad);
	static lvr_matrix4<T> make_scale_matrix_around_axis(const lvr_vector3<T>& a_axis,T a_s);
	static lvr_matrix4<T> make_scale_matrix(const lvr_vector3<T>& a_scales);
	static lvr_matrix4<T> make_rotation_matrix_around_axis(const lvr_vector3<T>& a_axis,T a_angle_rad);
	static lvr_matrix4<T> make_reflection_matrix_by_plane(const lvr_vector3<T>& a_normal,T a_dist_to_origin);
	static lvr_matrix4<T> make_view_matrix(const lvr_vector3<T>& a_pos,const lvr_vector3<T>& a_target,const lvr_vector3<T>& a_up);
	static lvr_matrix4<T> make_ortho_matrix(T a_width,T a_height,T a_near,T a_far);
	static lvr_matrix4<T> make_proj_matrix_rh(T a_view_angle_rad,T a_aspect_ratio,T a_near,T a_far);
	static lvr_matrix4<T> make_proj_matrix_lh(T a_view_angle_rad,T a_aspect_ratio,T a_near,T a_far);
	static lvr_matrix4<T> make_rotate_from_to_matrix(const lvr_vector3<T>& a_src,const lvr_vector3<T> a_dst);

private:
	union
	{
		struct
		{
			lvr_vector4<T>	_x,_y,_z,_d;
		};
		T _m[16];
		T _mm[4][4];
	};

};

template<class T>
lvr_matrix4<T>::lvr_matrix4(const lvr_matrix4<T>& a_mat)
{
	for (int i = 0; i < 16;++i)
	{
		this->_m[i] = a_mat[i];
	}
}

#include "lvr_matrix4.inl"

typedef lvr_matrix4<float> lvr_matrix4f;
typedef lvr_matrix4<double> lvr_matrix4d;

#endif
