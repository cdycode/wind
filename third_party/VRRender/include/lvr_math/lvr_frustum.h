#ifndef __lvr_frustum_h__
#define __lvr_frustum_h__
#include"lvr_matrix4.h"
#include"lvr_vector3.h"
#include"lvr_plane3.h"

template<class T>
class lvr_frustum
{
public:
	lvr_frustum(){}
	~lvr_frustum(){}
public:
	lvr_intersect_status sphere_intersect(const lvr_vector3<T>& a_pos,T a_r)const;
	void set_frustum(const lvr_matrix4<T>& a_proj_view_matrix,const lvr_vector3<T>& a_render_origin);
private:
	void update_planes();
private:
	lvr_matrix4<T>	_mat_proj_view;
	lvr_vector3<T>	_render_origin;
	lvr_plane3<T>	_near_plane;
	lvr_plane3<T>	_far_plane;
	lvr_plane3<T>	_left_plane;
	lvr_plane3<T>	_right_plane;
	lvr_plane3<T>	_bottom_plane;
	lvr_plane3<T>	_top_plane;
};
#include "lvr_frustum.inl"

typedef lvr_frustum<float> lvr_frustumf;
typedef lvr_frustum<double> lvr_frustumd;

#endif
