//
// Created by liuwenzhi on 2016/10/25.
//

#ifndef CINEMAVR_VR_DISPATCH_INTERFACE_H
#define CINEMAVR_VR_DISPATCH_INTERFACE_H

#include <functional>
#include "lvr_json.h"
#include <map>

class vr_dispatch_interface {
public:
    vr_dispatch_interface();
    virtual ~vr_dispatch_interface();
    virtual void on_dispatch(const std::string &cmd,const std::string &jsnargs){};

    virtual void add_clickshow_uis(std::string ids);
    virtual void add_clickhide_uis(std::string ids);
    virtual void on_display_ui_by_file(bool normal = true);
    virtual void on_event_ui_by_file();

    lvr_json *get_dispatch_args() {
        return _dispatch_args;
    }

	void set_dispatch_args(std::string jsnargs); 

    virtual void add_map_ui_event(int id, std::string event){
		char buf[64] = { 0 };
		sprintf(buf, "%d:%s", id, event.c_str());
		_vec_ui_event.push_back(buf);
    };

    virtual void set_view_id(const int id){
        _view_id = id;
        if(_dispatch_args== nullptr){
            set_dispatch_args("{}");
        }
    }
    virtual int get_view_id(){
        return _view_id;
    }

    virtual void follow_sight(lvr_matrix4f &rot_mat){};

	virtual void on_update_view(){}
	virtual int get_view_type(){ return 0; }

private:
    int _view_id;
    std::string     _type;
    std::string     _classname;
    lvr_json*           _dispatch_args;

    //std::map<int, std::string> _map_ui_event;
	std::vector < std::string > _vec_ui_event;

    static const int _DEFAULT_CLICK_UIS= 20;
    std::vector<int>    _on_clickshow_uis;
    std::vector<int>    _on_clickhide_uis;

};


#endif //CINEMAVR_VR_DISPATCH_INTERFACE_H
