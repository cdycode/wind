#ifndef _VR_IMAGE_VIEW_TOPIC_H__
#define _VR_IMAGE_VIEW_TOPIC_H__

#include "vr_image_view.h"

class vr_image_view_topic : public vr_image_view{
	DECLARE_CLASS(vr_image_view_topic)

public:
	vr_image_view_topic();
	bool init() override;
	virtual void on_dispatch(const std::string &cmd, const std::string &jsnargs) override;

	static  void* on_click_ui(lvr_ui_base* a_hit_ui, void* a_object);
	void on_clicked();

private:
	int _topic_id;
	std::string _downloading_skybox_url;
};



#endif