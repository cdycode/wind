#ifndef __VR_INDEX_BTN_ELEMENT_H__
#define __VR_INDEX_BTN_ELEMENT_H__



#include "vr_button_view.h"
#include "vr_view_list_container.h"
#include <string>


class vr_view_list_container;
class vr_index_btn_element : public vr_button_view{
	DECLARE_CLASS(vr_index_btn_element)
public:
	vr_index_btn_element();
	virtual ~vr_index_btn_element(){};
	virtual void on_clicked();
	virtual void on_focus_gain();
	virtual void on_focus_lost();
	virtual void set_first_element_number(int number);
	virtual void set_list_container(vr_view_list_container* container);
private:

	vr_view_list_container* _container;
    int _element_number;
	bool _is_valid;

};


#endif