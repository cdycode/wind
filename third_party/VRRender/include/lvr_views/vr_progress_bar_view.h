//
// Created by liuwenzhi on 2016/11/1.
//

#ifndef CINEMAVR_VR_PROGRESS_BAR_VIEW_H
#define CINEMAVR_VR_PROGRESS_BAR_VIEW_H


#include "lvr_ui_progress_bar.h"
#include "vr_dispatch_interface.h"
#include "vr_view_factory.h"
#include "poster_data_mgr.h"
#include "vr_download_btn_view.h"

class vr_progress_bar_view : public lvr_ui_progress_bar, public vr_dispatch_interface,public vr_view_factory {
DECLARE_CLASS(vr_progress_bar_view)
public:
    vr_progress_bar_view();
    virtual void on_dispatch(const std::string &cmd,const std::string &jsnargs) override ;
    virtual void update_progress(int tag);
    virtual void init();
private:
    void config_progress_info();
    poster_data_mgr::POSTER_TYPE    _poster_type;
    int                             _poster_id;
    int                             _dl_task_id;
    vr_download_btn_view::DOWNLOAD_STATUS _status;

    int _schedule_timer;
};


#endif //CINEMAVR_VR_PROGRESS_BAR_VIEW_H
