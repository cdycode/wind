#ifndef  _VR_MESSAGE_BOX_BUTTON_H_
#define _VR_MESSAGE_BOX_BUTTON_H_
#include "vr_button_view.h"
class vr_message_box_button :public vr_button_view
{
public:
	vr_message_box_button(){}
	~vr_message_box_button(){}
public:
	virtual bool init();
	virtual void on_focus_gain();
	virtual void on_focus_lost();
	virtual void on_clicked();
	static void* on_focus_in(lvr_ui_base *a_hit_ui, void* a_object);
	static void* on_focus_out(lvr_ui_base *a_hit_ui, void* a_object);
	static  void* on_click_ui(lvr_ui_base* a_hit_ui, void* a_object);

};
#endif