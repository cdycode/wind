//
// Created by liuwenzhi on 2016/10/25.
//

#ifndef CINEMAVR_VR_VIEW_CONTAINER_H
#define CINEMAVR_VR_VIEW_CONTAINER_H


#include <string>
#include "lvr_ui_multi_image.h"
#include "lvr_list.h"
#include "vr_view_factory.h"
#include "lvr_ui_menu.h"

#include "vr_image_view.h"
#include "vr_button_view.h"
#include "vr_label_view.h"
#include "vr_media_view.h"


class vr_view_container: public vr_view_factory,public vr_dispatch_interface {
    DECLARE_CLASS(vr_view_container)
public:
    virtual bool init();
	virtual bool un_init();
	vr_view_container() : _menu(NULL), _multi_image(NULL), _media_ptr(NULL){};
    ~vr_view_container();

    lvr_ui_multi_image *get_multi_image() const {
        return _multi_image;
    }

    lvr_ui_menu *get_menu() const {
        return _menu;
    }

    int config_images(int num,int a_image_width,int a_image_height, bool a_has_alpha);
  //  lvr_ui_multi_image* getui_multi_image(){return _multi_image;};

    virtual int add_containers(vr_view_container*);
    virtual int add_btn(vr_button_view*);
    virtual int add_img(vr_image_view *);
    virtual int add_label(vr_label_view *label);
	virtual int add_mediaview(vr_media_view *label);

    virtual void set_visible(bool _visible);
    virtual bool get_visible() const {
        return _is_visible;
    }

    void set_lookdown_ui(int id);
    virtual void follow_sight(lvr_matrix4f &rot_mat) override ;
    virtual void on_dispatch(const std::string &cmd,const std::string &jsnargs) override ;

	virtual void set_page_tag(const std::string &page_tag);

protected:
	lvr_ui_menu*                        _menu;
	lvr_ui_multi_image*                 _multi_image;

private:
	std::vector<vr_button_view*>        _buttons;
	std::vector<vr_view_container*>     _containers;

	bool _is_visible;

	vr_media_view*						_media_ptr;
	std::string							_page_tag;
	int									_width_per_image;
	int									_height_per_image;

};

#endif //CINEMAVR_VR_VIEW_CONTAINER_H
