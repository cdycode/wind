//
// Created by liuwenzhi on 2016/10/25.
//

#ifndef CINEMAVR_VR_VIEW_LIST_CONTAINER_H
#define CINEMAVR_VR_VIEW_LIST_CONTAINER_H

#include "vr_view_container.h"
#include "vr_label_view.h"
#include "vr_button_view.h"
#include "vr_index_btn_page.h"
#include "vr_index_btn_element.h"

class vr_index_btn_page;

class vr_view_list_container:public vr_view_container  {
DECLARE_CLASS(vr_view_list_container)
public:
    vr_view_list_container();
    void map_gid_uiid(int gid, int uiid);
    void set_visible(bool _visible);
    virtual void on_dispatch(const std::string &cmd,const std::string &jsnargs) override ;


protected:
    static const int MAX_GROUP_NUMBER = 20;
    static const int MAX_ELEM_NUMBER_PER_GROUP = 10;

    int     _grpid_uiid[MAX_GROUP_NUMBER][MAX_ELEM_NUMBER_PER_GROUP] ;

    int     _number_of_all_elem;
    int     _current_page;
    int     _number_per_page;
    int     _number_of_all_page;


    std::vector<vr_button_view*>	_index_btns;
	vr_label_view*	_index_label;

    std::string     _index_type;

    std::function<int(int)> number_of_all_elem_callback;


public:
	virtual int add_idx_btns(vr_button_view*);
	virtual int add_idx_labels(vr_label_view*);
	virtual void set_index_type(const std::string &_index_type);
	virtual int init_index(bool config_page=true);
	virtual int config_index(int page);
    virtual int change_pages(int page = 0);
	virtual int change_elements(int number);
	virtual void set_page_args(std::string data);

	void set_number_of_all_elem_callback(std::function<int(int)> &callback);
};


#endif //CINEMAVR_VR_VIEW_LIST_CONTAINER_H
