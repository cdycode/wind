//
// Created by liuwenzhi on 2016/10/25.
//

#ifndef CINEMAVR_VR_BUTTON_VIEW_H
#define CINEMAVR_VR_BUTTON_VIEW_H

#include "vr_dispatch_interface.h"
#include "vr_view_factory.h"
#include "lvr_ui_2d.h"
#include "lvr_ui_text.h"
#include "lvr_vector3.h"


class vr_button_view : public vr_dispatch_interface, public lvr_ui_2d, /*public lvr_ui_button,*/ public vr_view_factory{
DECLARE_CLASS(vr_button_view)
public:
    vr_button_view();
	virtual ~vr_button_view();
	virtual bool init();
    virtual void on_dispatch(const std::string &cmd,const std::string &jsnargs) override ;
    virtual void set_orientation(const lvr_vector3f& a_right, const lvr_vector3f& a_up) ;
    virtual void set_position(const lvr_vector3f& pos) ;
	virtual void set_label_font_size(int size);
	virtual void set_label_padding(lvr_vector2f& padding);

    virtual void on_clicked();

    lvr_ui_text* get_label_text() const;
    void set_label_text(std::string text);

    int set_auto_hide(bool auto_hide);
	bool is_auto_hide();
	bool has_valid_data();
    virtual void on_focus_gain() ;
	virtual void on_focus_lost();
    void set_visible(bool a_visible);

    void set_tex(const std::string& pic_normal, const std::string& pic_focus, const std::string& pic_selected, const std::string& pic_invalid);
    virtual int test_ray(const lvr_vector3f& a_pos, const lvr_vector3f& a_dir, float & ao_dist) override ;

    virtual void follow_sight(lvr_matrix4f &rot_mat) override ;

	void set_update_label(const std::string &l);

	void set_normal_position(const lvr_vector3f &_normal_position);
	void set_normal_right(const lvr_vector3f &_normal_right);
	void set_normal_up(const lvr_vector3f &_normal_up);

	void set_enable_sight_in_enlarge(bool is_enable);
	bool enlarge_by_direct(lvr_vector3f &direct, float rate);
	void set_enable_sight_in_edge(bool is_enable);
	void set_btn_layer_level(int level);
	void set_label_line_num(int num);
	void set_look_down_show(bool is_show);
	void set_test_show_string(const std::string& str);


	static void* on_focus_in(lvr_ui_base *a_hit_ui, void* a_object);
	static void* on_focus_out(lvr_ui_base *a_hit_ui, void* a_object);
	static  void* on_click_ui(lvr_ui_base* a_hit_ui, void* a_object);

public:
    std::string			_invalidimg;
    std::string			_normalimg;
    std::string			_focusimg;
    std::string			_selectimg;
	lvr_vector3f		_normal_position;
	lvr_vector3f		_normal_right;
	lvr_vector3f		_normal_up;
	lvr_ui_2d::tex_info	_cur_texture;
	bool			_enable_sight_in_enlarge;
	bool			_enable_sight_in_edge;

private:
	std::string	 _update_label;
    lvr_ui_text* _label_text;
	lvr_vector3f _label_position;
	bool			 _auto_hide;
	bool			_auto_follow;
	int				_lable_font_size;
	lvr_vector2f	_lable_padding;
	int				_label_line_num;
	int				_video_id;
	int				_btn_layer_level;
	bool			_is_look_down_show;
	std::string		_test_show_string;
};


#endif //CINEMAVR_VR_BUTTON_VIEW_H
