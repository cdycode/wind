#ifndef  _VR_MESSAGE_BOX_H_
#define _VR_MESSAGE_BOX_H_
#include "lvr_ui_menu.h"
#include <string>
class vr_message_box_button;
enum MESSAGE_CHOICES
{
	NETWORK_ERROR,
	OTHER_ERROR
};
class vr_message_box :public lvr_ui_menu
{
public:
	vr_message_box() :_mButton(NULL), _mBackGround(NULL), _mText(NULL){}
	virtual ~vr_message_box();
public:
	void init(MESSAGE_CHOICES a_choice = NETWORK_ERROR);
	void uinit();
	void set_pos(const lvr_vector3f &a_pos );
	void set_orientation(const lvr_vector3f & a_right,const lvr_vector3f & a_up);
	void set_size(const lvr_vector2f &a_size);
	void set_tex(const std::string &pic_normal, const std::string &pic_focus,
		const std::string &pic_selected,const std::string & pic_background);
	lvr_vector3f& get_pos();
	MESSAGE_CHOICES get_message_type()const;
	lvr_ui_2d* get_back_button();
	lvr_ui_text* get_message_text();
	lvr_ui_2d* get_message_backgroud();

	void set_label_text(const std::string & a_text);

private:
	void add_button();
	MESSAGE_CHOICES _mType;
	std::string _mMessage;
	lvr_vector2f	_mSize;
	lvr_vector3f    _mRight;
	lvr_vector3f    _mUp;
	lvr_vector3f	_mPos;
	vr_message_box_button*      _mButton;
	lvr_ui_2d*      _mBackGround;
	lvr_ui_text*    _mText;
private:
	std::string _normalimg;
	std::string _focusimg;
	std::string _selectimg;
	std::string _backgroundPic;
	lvr_ui_2d::tex_info	_cur_texture;
};
#endif

