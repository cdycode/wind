//
// Created by liuwenzhi on 2016/10/25.
//

#ifndef CINEMAVR_VR_INDEX_BTN_PAGE_H
#define CINEMAVR_VR_INDEX_BTN_PAGE_H


#include "vr_button_view.h"
#include "vr_view_list_container.h"
#include <string>

class vr_view_list_container;
class vr_index_btn_page: public vr_button_view{
DECLARE_CLASS(vr_index_btn_page)
public:
    vr_index_btn_page();
    virtual void on_clicked();
	virtual void on_focus_gain();
	virtual void on_focus_lost();
    virtual void set_page_number(int page);
    virtual void set_list_container(vr_view_list_container* container);
	virtual void on_dispatch(const std::string &cmd, const std::string &jsnargs) override;
private:
	vr_view_list_container* _container;
    int						_page_number;
	bool					_is_valid;
};


#endif //CINEMAVR_vr_index_btn_page_H
