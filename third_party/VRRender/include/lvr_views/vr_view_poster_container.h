#ifndef _VR_VIEW_POSTER_CONTAINER_H__
#define _VR_VIEW_POSTER_CONTAINER_H__

#include "vr_view_factory.h"
#include "vr_dispatch_interface.h"
#include "vr_button_view.h"
#include "vr_image_view.h"
#include "lvr_ui_texture_mananger.h"
#include "lvr_ui_event_manager.h"


class vr_view_poster_container : public vr_view_factory, public vr_dispatch_interface{
	DECLARE_CLASS(vr_view_poster_container)

public:
	vr_view_poster_container();
	~vr_view_poster_container();
	virtual int add_btn(vr_button_view*) ;
	virtual int add_img(vr_image_view *) ;

	void on_ui_focus(lvr_ui_base* a_ui, bool is_in);
	static  void* on_ui_focus_in_event(lvr_ui_base* a_hit_ui, void* a_object);
	static  void* on_ui_focus_out_event(lvr_ui_base* a_hit_ui, void* a_object);

private:
	lvr_event_manager*				_ui_event;
	lvr_event_binding				_focus_in;
	lvr_event_binding				_focus_out;
	std::vector<vr_button_view*>	_vec_buttons;
	std::vector<vr_image_view*>		_vec_images;
	lvr_ui_2d						_focus_ui;

	lvr_vector2f	_size;
	lvr_vector3f	_pos;


};



#endif