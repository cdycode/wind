#ifndef _VR_MEDIA_VIEW_H_
#define  _VR_MEDIA_VIEW_H_


#include "lvr_subtitle.h"
#include "lvr_media_interface.h"
#include "vr_dispatch_interface.h"
#include "vr_view_factory.h"
#include "lvr_ui_manager.h"
#include "lvr_camera.h"


#define use_controller_ui_v2

#ifdef use_controller_ui_v2
#include "vr_media_controller_ui_v2.h"
#else
#include "vr_media_controller_ui.h"
#endif


#ifdef use_controller_ui_v2
#include "lvr_ui_state.h"
class vr_media_controller_ui_v2;
#else
class vr_media_controller_ui;
#endif

class lvr_cinema_render;

class vr_media_view : public vr_dispatch_interface, public vr_view_factory{
DECLARE_CLASS(vr_media_view)
public:
	vr_media_view();
	~vr_media_view();

	bool init();
	void active(){ _is_active = true; }
	void inactive(){ _is_active = false; }
	bool is_active();
	int play(const std::string &url, const std::string &subtitle, int ms_pos);
	int play_retry();
	int playnext();
	int playprev();
	int stop();
	int get_local_media();
	bool config_media(lvr_ui_manager* ui_mgr, lvr_camera* camera, lvr_cinema_render* render, const std::string& res_path);
	void update(lvr_vector3f a_pos, lvr_vector3f a_look, lvr_vector3f a_up, double curr_time);

	virtual void on_dispatch(const std::string &cmd, const std::string &jsnargs) override;
	virtual void on_change_media_def(int def);

	void set_to_play(const std::string &url, const std::string &subtitle, int ms_pos, int src_type=0);
#ifdef use_controller_ui_v2
	vr_media_controller_ui_v2* get_medial_controller_ui();
	void draw_menu(const lvr_matrix4f& a_vp_mat);
#else
	vr_media_controller_ui* get_medial_controller_ui();
#endif
	int get_current_volume();
	int get_max_volume();
	void set_volume(int val);
	int get_current_brightness();
	int get_max_brightness();
	void set_brightness(int val);
	void set_callmethod(bool(*method)(int id, int& value));
	bool(*_callmethod)(int id, int& value);


	void huashu_load_media(const std::string& video);
	void huashu_pause_media();
	void huashu_resume_media();
	void huashu_unload_media();
	void set_huashu_callmethod(bool(*method)(int id, int& value, const std::string& str));
	bool(*_huashu_callmethod)(int id, int& value, const std::string& str);
	void set_huashu_media(const std::string& video);
	

	bool is_play_end();
	bool repeat_play();

	std::string get_play_page();

	int change_cinema_scene(int scene_id, const std::string& name);
	void set_scene_name(const std::string& name);
    lvr_subtitle* get_subtitle();

	void lock_cinema_screen(bool is_lock);
	bool is_lock_cinema_screen();
	void set_app_czm_video_func(bool (*func)(const char* a_path, void* obj), void* obj);
	void set_app_czm_video_sync_func(void (*func)(int sync_pos, void* obj));

	bool is_media_playing();
	void media_pause();
	void media_resume();
	void media_seek(int a_seek_pos);

private:
	enum play_state{
		NO_PLAY,
		READY_TO_PLAY,
		IS_PLAYING,
		UNKNOWN
	};
	std::string				_url;
	lvr_media_interface*	_media_ptr;
	int						_ms_curr_pos;
	int						_video_width;
	int						_video_height;
	bool					_is_active;
	int						_play_try_times;
#ifdef use_controller_ui_v2
	vr_media_controller_ui_v2*	_controller_ui;
#else
	vr_media_controller_ui*	_controller_ui;
#endif
	lvr_camera*				_camera_ptr;
	bool					_need_update_screen_render;
	int						_curr_play_id;
	std::string				_curr_play_page;
	int						_curr_video_id;
	std::string				_curr_video_name;
	int						_curr_video_definition;
	int						_curr_movie_play_time;
	int						_curr_movie_total_time;
	int						_curr_video_cate_id;
	lvr_movie_render::movie_format	 _curr_video_format;
	static bool				_media_is_finish;
	static int				_media_finish_status;
	lvr_subtitle*			_subtitle;
	lvr_ui_manager*			_ui_mgr;
	lvr_cinema_render*		_cinema_render;
	lvr_json*				_video_urls_json;

	int						_video_format;
	std::string				_video_urls;
	std::string				_video_img_url;
	std::string				_play_url;
	std::string				_play_subtitle;
	std::string				_play_subtitle_url;
	int						_play_ms_pos;
	std::string				_resource_path;
	play_state				_play_state;
	lvr_model_file*			_cinema_scene;

	int						_subtitle_dl_task_id;
	double					_ready_play_time_s;
	std::string				_scene_name;

	enum media_src{
		chengzi_media = 0,
		huashu_media,
	};
	media_src				_media_src;
	bool					_is_lock_cinema_screen;
	void*	_app_obj;
	bool(*_app_czm_start_func)(const char* a_path, void* obj);
	void(*_app_czm_sync_func)(int sync_pos, void* obj);
	std::string				_interactive_video;
	int						_interactive_video_dl_task_id;
public:
	lvr_media_interface* get_media_interface(){
		return _media_ptr;
	}
	static void media_finish_cb(int a);
	static void media_cache_over_cb();

};


#endif
