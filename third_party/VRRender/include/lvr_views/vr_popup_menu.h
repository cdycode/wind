#ifndef  __VR_POPUP_MENU__H__
#define  __VR_POPUP_MENU__H__


#include "lvr_ui_menu.h"
#include "vr_trans_animation.h"

class vr_popup_menu{
public:
	vr_popup_menu(lvr_ui_menu* menu);
	~vr_popup_menu();

	// consider background ui has the max width and height
	bool init();
	void add_ui_2d(lvr_ui_2d *ui, bool is_bg);
	void add_ui_text(lvr_ui_text *ui, const double font_size);
	void add_animation(const lvr_vector3f& _start_pos, const lvr_vector2f _start_size, const double duration = 0.4);
	bool update(double curr_time);
	void set_visible(bool visible);

private:

	struct ui_text{
		lvr_ui_text*	ui;
		double			font_size;
	};
	lvr_ui_menu*				_menu;
	vr_trans_animation*			_ani;
	std::vector<lvr_ui_2d*>		_vec_ui_2d;
	std::vector<ui_text>		_vec_ui_text;

	lvr_vector3f				_pos;
	lvr_vector2f				_size;
	lvr_vector3f				_ani_pos;
	lvr_vector2f				_ani_size;
	double						_duration_time;
	bool						_is_visible;
	bool						_is_need_update;
	lvr_ui_2d*					_ui_background;

};



#endif