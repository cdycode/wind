#ifndef __VR_VIEW_BULLET_SCREEN_H__
#define __VR_VIEW_BULLET_SCREEN_H__

#include "lvr_ui_menu.h"


struct bullet_node{

	int				_status;
	int				_fix_point;
	int				_level;
	float			_length;
	double			_start_time;
	double			_speed;
	lvr_ui_text		_ui_text;
};

class vr_view_bullet_screen : public lvr_ui_menu{
public:
	vr_view_bullet_screen();
	virtual ~vr_view_bullet_screen();

	bool init(int screen_width, int screen_height);
	bool un_init();
	void update(const lvr_vector3f& a_start, const lvr_vector3f& a_dir, const lvr_vector3f& a_up, double a_cur_time);
	bool add_bullet_node(double start_time, const char* str, const lvr_vector4uc& color, const double font_size, const double speed, int fix_point =0/* 1left 2middle 3right*/);

private:
	int							_bullet_height;
	int							_screen_width;
	int							_screen_height;
	std::map<int, std::vector<bullet_node*> >	_map_nodes;
	
};

#endif
