//
// Created by liuwenzhi on 2016/11/3.
//

#ifndef CINEMAVR_VR_VIEW_RADIOBOX_CONTAINER_H
#define CINEMAVR_VR_VIEW_RADIOBOX_CONTAINER_H

#include "vr_view_list_container.h"

class vr_view_radiobox_container :public vr_view_list_container {
	DECLARE_CLASS(vr_view_radiobox_container)
public:
	vr_view_radiobox_container();
	~vr_view_radiobox_container();
    virtual int add_btn(vr_button_view* btn) override ;
    virtual void on_dispatch(const std::string &cmd,const std::string &jsnargs) override ;
    virtual void set_visible(bool _visible) override ;
	virtual int add_idx_btns(vr_button_view*) override;
	virtual int change_pages(int page = 0) override;
	virtual int change_elements(int number) override;
	//virtual int change_pages(int page = 0);

	int get_video_num(int a);

private:
    std::vector<vr_button_view*> _vec_button;
	std::string					 _video_classify;
	int							 _curr_selected_btn_index;

};



#endif //CINEMAVR_VR_VIEW_RADIOBOX_CONTAINER_H
