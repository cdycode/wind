#ifndef _VR_IMG_ROUND_CORNER_CORVER__
#define _VR_IMG_ROUND_CORNER_CORVER__

#include "lvr_core_lib.h"
#include "lvr_math_configure.h"
#include "lvr_vector3.h"
#include "lvr_matrix4.h"
#include "lvr_vector2.h"
//#include "lvr_singleton.h"

class lvr_program;
class lvr_vertex_buffer;
class lvr_index_buffer;
class lvr_vertex_format;
class lvr_ui_2d;
class lvr_texture2d;
class vr_img_round_corner_corver {
public:
	vr_img_round_corner_corver();
	~vr_img_round_corner_corver();

	bool init(int num, int a_image_width, int a_image_height, float radius);
	bool uninit();
	int  add(lvr_ui_2d* img_2d);
	void update(const lvr_vector3f& a_pos, const lvr_vector3f& a_look, const lvr_vector3f& a_right, double curr_time);
	void update();

	void draw(const lvr_matrix4f& a_vp_mat);

	bool update_image(uint32_t surface_id, const char* a_buf, int a_w, int a_h);
	bool set_img_visible(int surface_id, bool is_show);
	lvr_ui_2d*  test_ray(const lvr_vector3f& a_start, const lvr_vector3f& a_dir, float& ao_dist);
	bool is_visible();
	void set_visible(bool a_visible);
	void set_to_update();
	void set_alpha(float alpha);
	void set_color_mask(int id, float rate);

private:
	void generate_image_rects();

private:
	struct img_node{
		uint32_t		_id;
		lvr_ui_2d*		_ui_2d;
		float			_color_rate;
	};

	lvr_program*				_prog;
	lvr_vertex_buffer*			_vb;
	lvr_index_buffer*			_ib;
	lvr_vertex_format*			_vf;
	lvr_texture2d*				_texture;
	uint32_t					_max_ui_num;
	uint32_t					_draw_ui_num;
	bool						_is_need_update;
	bool						_is_visible;


	std::vector<img_node*>		_vec_img_2d;
	uint32_t					_gen_img_id;

	std::vector<lvr_ui_2d*>		_rects;
	std::vector<lvr_vector4f>	_rect_uv1x;
	std::vector<lvr_vector4f>	_rect_uv1y;
	std::vector<lvr_vector4f>	_rect_uv2x;
	std::vector<lvr_vector4f>	_rect_uv2y;
	std::vector<lvr_vector2i>	_location_on_tex;
	int							_width;
	int							_height;
	float						_radius_of_corner;
	int							_uniform_color;
	float						_uniform_color_alpha;
};




#endif