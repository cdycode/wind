//
// Created by liuwenzhi on 2016/10/26.
//

#ifndef CINEMAVR_VR_VIEW_DRIVER_H
#define CINEMAVR_VR_VIEW_DRIVER_H

#include "lvr_data_types.h"
#include "vr_view_parser.h"
#include "vr_media_view.h"
#include "vr_view_container.h"
#include "vr_view_list_container.h"
#include <map>
#include "vr_button_view.h"
#include "lvr_singleton.h"
#include "vr_label_view.h"
#include "vr_view_radiobox_container.h"
#include "vr_view_poster_container.h"
#include "lvr_ui_manager.h"
#include <stack>
#include <vector>

class vr_view_container;
class vr_view_list_container;

struct update_view{
	double	_repeat_timer;
	void*	_object;
};

class vr_view_driver:public lvr_singleton<vr_view_driver> {
public:
	enum page_tag{
		tag_common_page,
		tag_media_page,
		tag_topic_page
	};

public:
	vr_view_driver();
	~vr_view_driver();
	bool init();
	bool un_init();
	int config_ui(lvr_ui_manager* ui_mgr);
    vr_dispatch_interface* getuiinterfacebyid(int id);
    int pushcontainerviews();
    int popcontainerviews();
	int get_view_containers(std::vector<vr_view_container*> &vec);

	void add_watch_update_view(vr_dispatch_interface*);
	void del_watch_update_view(vr_dispatch_interface*);
	int  update(lvr_ui_base* ui, lvr_vector3f a_pos, lvr_vector3f a_look, lvr_vector3f a_up, double curr_time);
	void on_network_response(int a_error);
	void on_network_error_back(double curr_time);
	void set_media_view(vr_media_view* view){ _media_view = view; }
	lvr_media_interface* get_media_interface(){ return _media_view == NULL ? NULL : _media_view->get_media_interface(); }
	vr_media_view*	get_media_view(){ return _media_view; }
	virtual void set_common_skybox_path(const std::string &path);
	virtual void set_topic_skybox_path(const std::string &path);

	bool set_curr_page(const std::string page_tag);
	page_tag get_curr_page_tag();

	void set_resource_cache_path(const char* path);
	std::string get_resource_cache_path();
	void set_cursor_visible(bool a_is_visible);
	bool is_cursor_visible();
	bool is_goto_exit();
	void set_goto_exit(bool is_exit);
	int  get_num_back_view();

private:
    vr_view_container* init_container_ui(lvr_json* jsondata);
    vr_view_list_container* init_listcontainer_ui(lvr_json* jsondata);
    vr_button_view* init_button_ui(lvr_json* jsondata);
	vr_image_view* init_image_ui(vr_view_container* container, lvr_json* jsondata);
    vr_label_view* init_label_ui(lvr_json* jsondata);
	vr_media_view* init_media_ui(lvr_json* jsondata);

private:
	double                                  _lastMessageTime;
	vr_media_view*							_media_view;
	lvr_ui_manager*							_ui_mgr;
    vr_view_container*						_display_view;
    vr_view_parser*							_view_parser;

	lvr_matrix4f							_trans_mat;
	float									_scale_size;

	std::string								_common_skybox;
	std::string								_topic_skybox;
	page_tag								_curr_page_tag;
	std::map<int, vr_dispatch_interface*>	_map_id_interface;

	std::map<int, vr_view_container*>		_map_id_containers;
	std::stack<std::string>					_back_views_id;
	std::vector<vr_dispatch_interface*>		_update_views;
	std::vector<vr_dispatch_interface*>		_update_views_curr;
	std::vector<vr_image_view*>				_not_clickable_image_views;
	std::vector<vr_view_poster_container*>	_vec_posters;

	std::string								_resource_cache_path;
	bool									_is_cursor_visible;
	bool									_is_goto_exit;


};


#endif