//
// Created by liuwenzhi on 2016/9/30.
//

#ifndef CINEMAVR_POSTER_DATA_MGR_H
#define CINEMAVR_POSTER_DATA_MGR_H

#include "DownloadMgr.h"
#include "lvr_singleton.h"
#include "lvr_json.h"
#include "curl/curl.h"
#include "CCMD5.h"


//#define _UNITY3D_WORKSPACE_

#define USE_CINEMA_VR_TEST 0

#if USE_CINEMA_VR_TEST
#define CZ_DEBUG_LOG(...) {		\
	if(1){							\
		LVR_LOG(__VA_ARGS__);			\
			}								\
}
#else
#define CZ_DEBUG_LOG(...)
#endif


#define CinemaPosterBackgroundPicture "assets/asd_05.png"

class poster_data_mgr:public lvr_singleton<poster_data_mgr>
{
public:
    enum POSTER_TYPE
    {
        POSTER_TYPE_HOME,
        POSTER_TYPE_VIDEO,
        POSTER_TYPE_3D,
        POSTER_TYPE_TOPIC,
		POSTER_TYPE_INFO,
		POSTER_TYPE_USER_BRAND,
		POSTER_TYPE_LOCAL,
        POSTER_TYPE_UNKNOWN
    };

    enum DATA_STATUS
    {
        POSTERDATA_NO_DATA,
		POSTERDATA_LOADING,
		POSTERDATA_READY,
		POSTERDATA_EXIT,
		POSTERDATA_UNKNOWN
    };
public:
	poster_data_mgr();
	~poster_data_mgr();
    int init();
	int un_init();
    void set_local_file_list(const std::string& listfiles);
	int getallposternumber(const std::string &  video_typename);
	std::string get_string_data_by_key(const std::string& node_type,const std::string &  sub_type, int id, const char* keyname);
	int get_int32_data_by_key(const std::string& node_type, const std::string& sub_type, int id, const char* keyname);
	std::string utf8_substr(const std::string &str, uint64_t start, uint64_t leng);

	int save_dl_poster_status(const std::string &  title, const std::string &  path, DownloadTask::TaskStatus status, int progress, int flush);
	DownloadTask::TaskStatus get_download_poster_status(const std::string &  title);
	int get_download_poster_progress(const std::string &  title);
	std::string get_download_poster_path(const std::string &  title);

	int update_signature();
	int update_cate_list();
	int update_home_page();
	int update_topic_page(int page_index = 0);
	int update_video_3d_page();
	int update_info_page();
	int update_user_brand_page();
	int update_local_video_page();
	int update_my_account();
	int update_home_videolist();
	int init_movie_history();
	int add_movie_history(const std::string& title, const int video_id, const std::string& url, const std::string& video_img, const int video_format, const std::string& subtitle_url, const int play_time_ms);

	int	add_home_videolist_data(char * jsn_buffer);
	int parse_home_json(char * jsn_buffer);
	int parse_topic_json(char * jsn_buffer);
	int parse_video_json(char * jsn_buffer);
	int parse_3d_json(char * jsn_buffer);
	int parse_info_json(char * jsn_buffer);
	int parse_user_brand_json(char * jsn_buffer);

	void set_curr_video_cls_in_video_and_3d(const std::string &  classify, const std::string &  type);
	void set_curr_topic_id_in_topic(const std::string &  topicid);
	void set_curr_info_video_id(int videoid);
	void set_curr_user_brand_id(int user_brand_id);
	void set_http_user_agent(const std::string &ua);
	void update_user_info(int status, const std::string &name, const std::string &url, int user_id = 0);

	std::string get_new_play_url(int video_id, int definition);
	lvr_json*	get_video_classes(const char*);
	std::string get_video_type_by_class(const char* video_type, int id, const char* keyname);

	void update_play_statistic_data(int video_id, int cata_id, const std::string& title, int video_size, int play_time, int play_duration, int duration, int vide_status = 2, int net_type = 1);
	int get_curr_cate_id();

#ifdef LVR_OS_ANDROID
	void list_dir_for_local_video(const std::string & dir);
#endif
    void set_app_source_path(const char* path);
    const std::string get_app_source_path();

	void set_local_usb_volume(const std::string& volume_path);
	void remove_local_usb_volume();
	void update_correlation_video(int videoid);
	void update_correlation_video_in_thread(int videoid);

private:
	void local_video_thread_function();
    int getdatafromserver(const std::string& url, const std::string & postdata="");
    static size_t download_callback(void *pBuffer, size_t nSize, size_t nMemByte, void *pParam);

#ifdef LVR_OS_ANDROID
	static int filter_fn(const struct dirent* ent);
	void scan_local_media(const char* dir_name);
#endif


private:
    DATA_STATUS _status;
    CURL*      _curl_handle;

	char*		_data_buffer;
	int			_data_bufsize;
	int			_data_bufsize_max;
	char		_sign_key_md5[128];
	CCMD5*		_md5sum;

	lvr_json*	_download_data_jsn;
	lvr_json*	_video_classify;
	lvr_json*	_3d_classify;
	lvr_json*	_home_page_data_jsn;
	lvr_json*	_curr_info_data_jsn;
	lvr_json*	_local_video_data_jsn;
	lvr_json*	_local_video_from_android_jsn;
	lvr_json*	_my_account_jsn;
	lvr_json*	_my_movie_history_jsn;
	lvr_json*	_curr_correlation_video_jsn;


//#define _USE_OLD__
#ifdef _USE_OLD__
	std::map<std::string, lvr_json*>	_topic_data;
#else
	std::map<std::string, std::map<int, lvr_json*>>	_topic_data;
#endif
	std::map<std::string, lvr_json*>	_video_data;
	std::map<std::string, lvr_json*>	_3d_data;
	std::map<int, lvr_json*>			_brand_data;

	std::string		_curr_video_classify_in_video_and_3d;
	std::string		_curr_video_type_name_in_video_and_3d;
	int				_curr_video_type_id_in_video_and_3d;
	std::string		_curr_topic_id_in_topic;
	int				_curr_video_id;
	int				_curr_user_brand_id;
	bool			_local_video_init_ok;

	unsigned char*  _aes_key;
	std::string		_http_useragent;
	std::thread* 	_thread_local_file_ptr;
	std::thread* 	_thread_local_usb_file_ptr;
	std::thread* 	_thread_update_correlation_ptr;


	static char		_curr_local_media_path[2048];
	int				_curr_home_videolist_page;
	lvr_json*		_home_videolist_jsn;
	int				_user_id;
    std::string     _app_source_path;
	std::string		_curr_usb_media_volume;

};

#endif //CINEMAVR_POSTER_DATA_MGR_H

