#ifndef __VR_CLASSIFY_BTN_VIDEO_TYPE_H__
#define __VR_CLASSIFY_BTN_VIDEO_TYPE_H__

#include "vr_button_view.h"

class vr_classify_btn_video_type : public vr_button_view{
	DECLARE_CLASS(vr_classify_btn_video_type)

public:
	vr_classify_btn_video_type();
	virtual ~vr_classify_btn_video_type();
	virtual void on_dispatch(const std::string &cmd, const std::string &jsnargs) override;
	virtual void on_clicked() override;
	virtual void set_position(const lvr_vector3f& pos) override;
	virtual void on_focus_gain() override;
	virtual void on_focus_lost() override;

private:
	std::string	_type_name;
	bool		_is_selected;
};

#endif