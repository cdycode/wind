#ifndef _VR_IMAGE_VIEW_HEAD_PIC_H__
#define _VR_IMAGE_VIEW_HEAD_PIC_H__

#include "vr_image_view.h"


class vr_image_view_head_pic : public vr_image_view{
	DECLARE_CLASS(vr_image_view_head_pic)

public:
	bool init() override;
	virtual void on_update_view() override;
	virtual void on_dispatch(const std::string &cmd, const std::string &jsnargs) override;

	static  void* on_click_ui(lvr_ui_base* a_hit_ui, void* a_object);
	void on_clicked();

private:
	int _user_id;
};


#endif