#ifndef __VR_MEDIA_MORE_MENU__H__
#define __VR_MEDIA_MORE_MENU__H__


#include "lvr_ui_2d.h"
#include "lvr_texture2d.h"

#include "lvr_ui_menu.h"
#include "lvr_ui_text.h"


class lvr_image;
class vr_img_round_corner_corver;
class vr_media_view;
class lvr_ui_manager;

class LVR_UI_API vr_media_more_menu
{
public:
	vr_media_more_menu();
	~vr_media_more_menu();
public:
	bool init();
	void uninit();

	void update_background(const lvr_vector3f& a_pos, const lvr_vector3f& a_look, const lvr_vector3f& a_sight_right);
	lvr_ui_base* update(const lvr_vector3f& a_pos, const lvr_vector3f& a_look, const lvr_vector3f& a_sight_right, const lvr_vector3f& a_up, const lvr_vector3f& a_right, double curr_time);
	void draw(const lvr_matrix4f& a_vp_mat);


	bool is_visible(){ return _visible; }
	void set_visible(bool a_visible);


	void config_menu(const lvr_vector3f& pos, const lvr_vector3f& right, const lvr_vector3f& up, lvr_ui_manager* ui_mgr);
	void show_right_menu(bool visible, float alpha);
	void show_bottom_menu(bool visible);
	void set_media_view(vr_media_view *ptr);
	void reset();

private:
	enum UI{
		ui_right_1 = 0,
		ui_right_2,
		ui_right_3,
		ui_right_4,
		ui_right_5,				//refresh
		ui_bottom_1,
		ui_bottom_2,
		ui_bottom_3,           // 1��2��3  pictures
		ui_bottom_4,			// priv
		ui_bottom_5,			// post
		ui_end,
	};
	lvr_vector3f				_ui_pos[ui_end];

	bool						_visible;

	struct common_ui{
		lvr_ui_2d		_ui_2d;
		lvr_ui_text		_ui_text;
		std::string		_cmd_data;
		int				_video_id;
		int				_item_id;
	};
	std::vector<common_ui*>		_common_ui_right_rects;
	std::vector<common_ui*>		_common_ui_bottom_rects;


	struct corner_imgs{
		int				_img_surface;
		lvr_ui_2d 		_ui_2d;
		lvr_image*		_update_image;
		int				_dl_update;
		int				_video_id;
		int				_border_loop_id;
	};
	std::vector<corner_imgs*>		_img_ui_right;
	std::vector<corner_imgs*>		_img_ui_bottom;


	float							_right_menu_alpha;
	float							_click_alpha;
	bool							_is_init;
	vr_img_round_corner_corver*		_round_corner_right_imgs;
	vr_img_round_corner_corver*		_round_corner_bottom_imgs;

	struct download_task{
		int				_position;
		int				_group_id;
		char*			_buffer_img_data;
		const int		_buffer_img_length = 1024 * 1024;
	};
	std::map<int, download_task>		_dl_tasks;
	bool								_is_need_update_right;
	bool								_is_need_update_bottom;
	bool								_is_need_update_image;
	bool								_is_visible_right;
	bool								_is_visible_bottom;

	vr_media_view*						_media_view;
	UI									_curr_focus_ui;

	lvr_ui_menu							_ui_menu;
	lvr_ui_manager*						_ui_mgr;

	void set_show_img(int pos, int group_id, const std::string &url, int width, int height);
	void on_download_over(bool success, int taskid);
	bool set_pkg_tex(vr_img_round_corner_corver* img_pos, int pid, const std::string& file);


	static void* click_ui_cb(lvr_ui_base* a_ui, void* xx);
	static void* focus_ui_cb(lvr_ui_base* a_ui, void* xx);
	static void* leave_ui_cb(lvr_ui_base* a_ui, void* xx);
	void on_ui_click(lvr_ui_base* a_ui);
	void on_ui_focus(lvr_ui_base* a_ui);
	void on_ui_leave(lvr_ui_base* a_ui);

	void on_focus_img(UI id);
	void on_leave_img(UI id);



	lvr_program*				_background_prog;
	lvr_vertex_buffer*			_background_vb;
	lvr_index_buffer*			_background_ib;
	lvr_vertex_format*			_background_vf;

	UI							_curr_bottom_wait_ui;
	lvr_ui_2d*					_back_ui;
	bool						_in_reset_right;
	bool						_in_reset_bottom;
};


#endif