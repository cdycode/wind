#ifndef  _LVR_MESSAGE_BOX_MANAGER_H_
#define _LVR_MESSAGE_BOX_MANAGER_H_
#include "lvr_singleton.h"
#include "vr_message_box.h"
class lvr_ui_state;
class lvr_ui_manager;
class vr_message_box;
class lvr_texture;
class vr_message_box_manager:public lvr_singleton<vr_message_box_manager>
{
public:
	void init();
	void add_to_ui_mgr(lvr_ui_manager* a_ui_mgr);
	void unit();
	lvr_ui_menu*  choose_message_to_show(MESSAGE_CHOICES a_choice);
	void update(const lvr_vector3f &a_cameraPos, const lvr_vector3f& a_cameraDir,lvr_ui_state& a_state);
	lvr_ui_menu*  get_curr_message_box();
	void set_curr_text(const std::string& text);
	bool is_showing_msg();
	void set_showing_msg(bool a_is_show);

private:
	//lvr_texture*                 _mGreyRectGround;
	lvr_vector3f  _mScreenCenter;
	lvr_vector3f  _mCameraPos;
	lvr_vector3f  _mCameraDir;
private:
	std::vector<vr_message_box*>  _mMessageBox;
	vr_message_box*              _mCurrMessageBox;
	lvr_vector3f _mInitBoxPos;
	int 		 _is_showing_msg;

};
#endif