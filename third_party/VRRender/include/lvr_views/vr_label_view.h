//
// Created by liuwenzhi on 2016/11/2.
//

#ifndef CINEMAVR_VR_LABEL_VIEW_H
#define CINEMAVR_VR_LABEL_VIEW_H


#include "lvr_ui_text.h"
#include "vr_dispatch_interface.h"
#include "vr_view_factory.h"
#include "poster_data_mgr.h"

class vr_label_view: public vr_dispatch_interface, public lvr_ui_text, public vr_view_factory {
DECLARE_CLASS(vr_label_view)
public:
    vr_label_view();
    void init(){};
	void set_label_line_num(int num);
	void set_label_width(float len);
private:
    void config_label_info();
    virtual void on_dispatch(const std::string &cmd,const std::string &jsnargs) override ;

public:
    void set_content_name(const std::string &_content_name) {
        vr_label_view::_content_name = _content_name;
    }

private:
    std::string                     _content_name;
    poster_data_mgr::POSTER_TYPE    _poster_type;
    int                             _poster_id;
	int								_label_line_num;
	float							_label_width;
};


#endif //CINEMAVR_VR_LABEL_VIEW_H
