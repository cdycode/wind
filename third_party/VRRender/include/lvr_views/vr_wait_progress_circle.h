#ifndef  __VR_WAIT_PROGRESS_CIRCLE__H__
#define __VR_WAIT_PROGRESS_CIRCLE__H__

#include "lvr_vector3.h"
#include "lvr_singleton.h"
#include "lvr_matrix4.h"
#include "lvr_rect2.h"

class lvr_index_buffer;
class lvr_vertex_format;
class lvr_program;
class lvr_texture;
class lvr_vertex_buffer;

class vr_wait_progress_circle : public lvr_singleton<vr_wait_progress_circle>
{
public:
	vr_wait_progress_circle();
	~vr_wait_progress_circle();

	bool init();
	bool uninit();
	void update(double curr_time);
	void draw(const lvr_matrix4f& a_vp_mat);
	void set_percentage(float rate);
	void set_visible(bool visible);
	void set_position(const lvr_vector3f& pos);
	void set_orientation(const lvr_vector3f& a_right, const lvr_vector3f& a_up);
	void start(double start_time, float duration = 1.5f);
	void end();
	bool is_in_good_end();

private:
	double					_start_time;
	float					_duration;
	int						_curr_id;
	int						_column;
	int						_row;
	bool					_is_visible;
	float					_percentage;
	lvr_vector3f			_position;
	lvr_vector3f			_right;
	lvr_vector3f			_up;
	bool					_is_need_update;
	lvr_index_buffer*		_ib;
	lvr_vertex_format*		_vf;
	lvr_vertex_buffer*		_vb;
	lvr_program*			_prog;
	lvr_texture*			_texture;
	lvr_rect2f				_rect_uv;

	enum status{
		hide = 0,
		show = 1,
		wait = 2,
		good_end = 3,
		bad_end = 4
	};

	int						_status;
};


#endif