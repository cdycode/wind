//
// Created by liuwenzhi on 2016/11/4.
//

#ifndef CINEMAVR_VR_LOOKAT_EVENT_H
#define CINEMAVR_VR_LOOKAT_EVENT_H


#include <vector>
#include "lvr_ui_2d.h"

class vr_lookat_event : public lvr_ui_2d {
public:
    vr_lookat_event();
    void set_lookdown_show_ui(int id);
    virtual int test_ray(const lvr_vector3f& a_pos, const lvr_vector3f& a_dir, float & ao_dist) override ;

private:
    enum SIGHTLINE{
        NORMAL = 1,
        LOOKDOWN,
        LOOKUP,
    };
    int    _lookdown_show_id;
    SIGHTLINE _status;
};


#endif //CINEMAVR_VR_LOOKAT_EVENT_H
