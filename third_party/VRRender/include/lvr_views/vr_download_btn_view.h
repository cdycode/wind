//
// Created by liuwenzhi on 2016/11/1.
//

#ifndef CINEMAVR_VR_DOWNLOAD_BTN_VIEW_H
#define CINEMAVR_VR_DOWNLOAD_BTN_VIEW_H


#include "vr_button_view.h"
#include "poster_data_mgr.h"

class vr_download_btn_view : public vr_button_view{
DECLARE_CLASS(vr_download_btn_view)
public:
    vr_download_btn_view();
    virtual void on_dispatch(const std::string &cmd,const std::string &jsnargs) override ;

    virtual void on_clicked();
    int  start_donwload();
    void on_download_over(bool success, int taskid);
    void config_download_info();

public:
    enum DOWNLOAD_STATUS{
        VIEW_NOTDOWNLOAD = 0,
        VIEW_DOWNLOADING=1,
        VIEW_DOWN_PAUSE,
        VIEW_DOWN_FINISH,
    };

private:

    DOWNLOAD_STATUS _status;
    int             _dl_task_id;
    std::string     _dl_url;
    poster_data_mgr::POSTER_TYPE    _poster_type;
    int                             _poster_id;

    int on_start_download();
    int on_pause_download();
    int on_resume_download();
    int on_remove_download();

};


#endif //CINEMAVR_VR_DOWNLOAD_BTN_VIEW_H
