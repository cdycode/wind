#ifndef __lvr_views_configure_h__
#define __lvr_views_configure_h__

#include "lvr_log.h"
#include "lvr_time.h"

#ifdef lvr_views_EXPORTS
#define LVR_VIEWS_API __declspec(dllexport)
#elif defined lvr_views_IMPORT
#define LVR_VIEWS_API __declspec(dllimport)
#else
#define LVR_VIEWS_API
#endif


#endif