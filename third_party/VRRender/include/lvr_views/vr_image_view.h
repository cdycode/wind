//
// Created by liuwenzhi on 2016/10/26.
//

#ifndef CINEMAVR_VR_IMAGE_VIEW_H
#define CINEMAVR_VR_IMAGE_VIEW_H

#include "vr_dispatch_interface.h"
#include "vr_image_view.h"
#include "vr_view_factory.h"
#include "lvr_ui_multi_image.h"

class lvr_image;
class vr_view_driver;
class vr_view_container;

class vr_image_view : public vr_dispatch_interface, public lvr_ui_2d, public vr_view_factory{
DECLARE_CLASS(vr_button_view)

public:
    vr_image_view();
    virtual ~vr_image_view();
	virtual bool init();
	virtual void on_dispatch(const std::string &cmd, const std::string &jsnargs) override;
	virtual void config(lvr_ui_multi_image *menu, int uiid);
	virtual void set_tex(const std::string& pic);
	virtual void set_texture_path(const std::string& pic);
	virtual void set_tex(const lvr_image_info& pic);
	virtual void set_tex(const std::string& picname, const char* buffer, size_t buffer_length);
	virtual void set_url(const std::string& url, int width, int height);
	virtual void on_img_focus_gain(int status);
	virtual void addfocusshowid(std::string show_ids);
	void on_download_over(bool success, int taskid);
	virtual void on_update_view();

	virtual void set_position(const lvr_vector3f& pos);
	void set_enable_sight_in_enlarge(bool is_enable);
	bool enlarge_by_direct(lvr_vector3f &direct, float rate);
	void set_enable_sight_in_edge(bool is_enable);
	void set_update_label(const std::string& name);
	lvr_image* get_update_image();
	void set_default_image_path(const std::string &path);
	void set_show_circle(bool use_circle);
	static void on_network_error_callback(int error_id);
protected:
    void on_receive_pic(lvr_image* image);
    // virtual void set_uv_info(tex_info& a_texinfo) override {};
    //0 png; 1: jpg
    int check_image_formate(char* buffer);
private:
    lvr_ui_multi_image *_menu;
    int  _ui_id;
    std::string _url;
    std::string _picpath;
    int _picwidth;
    int _picheight;
    std::vector<int> _on_focus_show_id;

	lvr_image*	_update_image;
	lvr_image*  _curr_image;
	int	_need_update_img;
	char*		_buffer_img_data;
	const int   _buffer_img_length = 1024 * 1024;
	std::string _video_url;
	int check_pic_formate(char *buffer);
	int	_download_task_id;
	bool			_enable_sight_in_enlarge;
	bool			_enable_sight_in_edge;
	lvr_vector3f	_normal_position;
	std::string		_default_image_path;
	std::string		_update_label;
	bool			_is_show_circle;

public:
	static  void* on_ui_focus_in_event(lvr_ui_base* a_hit_ui, void* a_object);
	static  void* on_ui_focus_out_event(lvr_ui_base* a_hit_ui, void* a_object);

};


#endif //CINEMAVR_VR_IMAGE_VIEW_H
