//
// Created by liuwenzhi on 2016/10/26.
//

#ifndef CINEMAVR_VR_VIEW_FACTORY_H
#define CINEMAVR_VR_VIEW_FACTORY_H

#include <string>


#if 0
class vr_view_factory;
class ClassInfo;

typedef vr_view_factory* (*ObjectConstructorFn)(void);
bool Register(ClassInfo* ci);

using namespace std;

class ClassInfo
{
public:
    ClassInfo(const std::string className,ObjectConstructorFn ctor)
            :m_className(className) ,m_objectConstructor(ctor)
    {
        Register(this);
    }
    virtual ~ClassInfo(){}
    vr_view_factory* CreateObject()const { return m_objectConstructor ? (*m_objectConstructor)() : 0;    }
    bool IsDynamic()const { 
		return NULL != m_objectConstructor;
	}
    const std::string GetClassName()const { return m_className;}
    ObjectConstructorFn GetConstructor()const{ return m_objectConstructor;}
public:
    string m_className;
    ObjectConstructorFn m_objectConstructor;
};

#define DECLARE_CLASS(name) \
     protected: \
         static ClassInfo ms_classinfo; \
     public:  \
         virtual ClassInfo* GetViewClassInfo() const; \
         static vr_view_factory* CreateObject();

#define IMPLEMENT_CLASS_COMMON(name,func) \
     ClassInfo name::ms_classinfo((#name), \
              (ObjectConstructorFn) func); \
                           \
     ClassInfo *name::GetViewClassInfo() const \
         {return &name::ms_classinfo;}

#define IMPLEMENT_CLASS(name)  \
     IMPLEMENT_CLASS_COMMON(name,name::CreateObject) \
     vr_view_factory* name::CreateObject()                   \
         { return new name;}



class vr_view_factory
{
DECLARE_CLASS(vr_view_factory)
public:
    vr_view_factory(){}
    virtual ~vr_view_factory(){}
    static bool Register(ClassInfo* ci);
    static vr_view_factory* CreateObject(string name);
};


#else

#define DECLARE_CLASS(name) 
#define IMPLEMENT_CLASS(name)

class vr_view_factory
{
	DECLARE_CLASS(vr_view_factory)
public:
	vr_view_factory(){}
	virtual ~vr_view_factory(){}
	static vr_view_factory* CreateObject(const std::string& name);
};

#endif


#endif //CINEMAVR_VR_VIEW_FACTORY_H
