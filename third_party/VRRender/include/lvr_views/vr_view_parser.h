//
// Created by liuwenzhi on 2016/10/25.
//

#ifndef CINEMAVR_VR_VIEW_PARSER_H
#define CINEMAVR_VR_VIEW_PARSER_H

#include "lvr_json.h"
#include <map>
#include "lvr_ui_2d.h"

class vr_view_parser {
public:
    int init();
	vr_view_parser();
	virtual ~vr_view_parser();

private:
    int parse_container(lvr_json *json);
    int parse_list_container(lvr_json *json);
    int parse_button(lvr_json *json);
    int parse_image(lvr_json *json);
    int parse_progressbar(lvr_json *json);
    int parse_label(lvr_json *json);

    std::string _config_filename;


private:
    lvr_json*    _jsn_page;
    lvr_json*    _jsn_page_view;
    lvr_json*    _jsn_page_style;

public:
    const std::map<int, lvr_json *> &get_map_id_jsn() const {
        return _map_id_jsn;
    }

    lvr_json *get_page_view_jsn() const {
        return _jsn_page_view;
    }

private:
    std::map<int, lvr_json*> _map_id_jsn;

};


#endif
