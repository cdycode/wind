#ifndef __lvr_simple_mesh_h__20170510__
#define __lvr_simple_mesh_h__20170510__

#include "lvr_scene_configure.h"
#include "lvr_transform.h"
//currently,this is only for handle test.
class LVR_SCENE_API lvr_simple_mesh
{
public:
	lvr_simple_mesh();
	~lvr_simple_mesh();
public:
	void load_from_file(const char* a_file);
	void load_from_buffer(const char* a_buf);
	void release_res();
	void set_diffuse_texture(lvr_texture* a_tex);
	lvr_transform* get_transform();
	void draw(const lvr_matrix4f& a_view_proj_mat);
private:
	lvr_transform	_transform;
	lvr_material*	_material;
	lvr_program*	_program;
	lvr_render_object*	_ro;
};

#endif
