#ifndef  _LVR_BINARY_FILE_H_
#define  _LVR_BINARY_FILE_H_
#include "lvr_data_types.h"
#include "lvr_scene_configure.h"
#include "lvr_binaryFile.h"
#include <vector>
#include <fstream>

class LvrFileHeader
{
	enum LvrDataType
	{
		eLvrTexture,
		eLvrMaterial,
		eLvrMesh,
		eLvrSkeleton,
		eLvrSkeletalAnimation
	};
public:
	LvrFileHeader(){}
	~LvrFileHeader(){}
public:
	void set_data_type(LvrDataType a_type){ _data_type = a_type; }
	void set_data_size(uint32_t a_size){ _data_size = a_size; }
	LvrDataType get_data_type(){ return (LvrDataType)_data_type; }
	uint32_t get_data_size(){ return _data_size; }

private:
	LvrDataType _data_type;
	uint32_t    _data_size;
};


class LVR_SCENE_API lvr_binary_file_read
{
public:
	lvr_binary_file_read() :_data(NULL), data_length(0), Offset(0), _offset(0){}
	lvr_binary_file_read(const uint8_t* a_data, uint32_t a_length) :_data(a_data), data_length(a_length)
		, Offset(0),_offset(0){}
	lvr_binary_file_read(const char* file_name);
	~lvr_binary_file_read(){}
public:
	void read_uint8(uint8_t *out, uint32_t a_length);
	void read_uint32(uint32_t *out,uint32_t a_length);
	void read_int8(int8_t *out,uint32_t a_length);
	void read_int32(int32_t *out, uint32_t a_length);
	void read_float(float *out, uint32_t a_length);
	void read_string(char* out, uint32_t a_length);
	template<typename _type_>
	bool read_array(std::vector<_type_> &out, const int numElements);

	template< typename _type_ >
	bool ReadArray(std::vector< _type_ > & out, const int numElements) const
	{
		const int bytes = numElements * sizeof(out[0]);
		if (_data == NULL || bytes > data_length - Offset)
		{
			out.resize(0);
			return false;
		}
		out.resize(numElements);
		memcpy(&out[0], &_data[Offset], bytes);
		Offset += bytes;
		return true;
	}

	bool IsAtEnd() const
	{
		return (_offset == data_length);
	}
	//bool  read_from_file(uint32_t a_pos,uint32_t a_length,uint8_t* out);
	uint32_t get_binary_length(){ return data_length; }
	void close();
private:
	const uint8_t* _data;
	uint32_t  data_length;
	mutable uint32_t	Offset;
	FILE*	 _my_file;
	uint32_t   _offset;
};

template<typename _type_>
bool lvr_binary_file_read::read_array(std::vector<_type_> &out, const int numElements)
{
	const uint32_t bytes = numElements * sizeof(out[0]);
	if ( bytes > data_length - _offset)
	{
		out.resize(0);
		return false;
	}
	out.resize(numElements);
	//_my_file.seekg(_offset);
	//_my_file.read((char*)&out[0], bytes);

	fseek(_my_file, _offset, SEEK_SET);
	fread((char*)&out[0], 1, bytes, _my_file);
	_offset += bytes;

	return true;
}


class LVR_SCENE_API lvr_binary_file_write
{
public:
	lvr_binary_file_write();
	lvr_binary_file_write(const char* a_file_name);
public:
	//void write_to_file(const uint8_t* a_src,const uint32_t data_length, const char* a_dst);
	void write_int8(const int8_t* a_data,const uint8_t numElements);
	void write_uint8(const uint8_t* a_data, const uint32_t numElements);
	void write_uint16(const uint16_t* a_data,const uint32_t numElements);
	void write_uinit32(const uint32_t* a_data, const uint32_t numElements);
	void write_float(const float* a_data,const uint32_t numElements);
	void write_string(const char* a_data,const uint32_t numElements);
	template< typename _type_ >
	void write_array(std::vector<_type_> &a_in,const uint32_t numElements);
	void end_write();
private:
	std::vector<LvrFileHeader>  _headers;
	uint8_t *   _data;
	const char* _name;
	FILE*  _myFile;
	uint32_t   offset;

};

template< typename _type_ >
void lvr_binary_file_write::write_array(std::vector<_type_> &a_in, const uint32_t numElements)
{
	//_myFile.seekp(offset);
	//_myFile.write((char*)(&a_in[0]), sizeof(a_in[0])*numElements);

	const int bytes = sizeof(a_in[0])*numElements;
	fseek(_myFile, offset, SEEK_SET);
	fwrite((char*)(&a_in[0]), 1, bytes, _myFile);
	offset += bytes;
}




#endif