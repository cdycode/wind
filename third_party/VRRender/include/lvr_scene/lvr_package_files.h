#ifndef __lvr_package_files_h__
#define __lvr_package_files_h__

#include "lvr_scene_configure.h"

class lvr_model_file;
class lvr_model_programs;
class lvr_material_parms;

class LVR_SCENE_API lvr_apk_file
{
public:
	lvr_apk_file(void* a_zip_file);
	~lvr_apk_file();
	operator void* ()const {return _zip_file;}
	operator bool() const {return _zip_file != 0;}
private:
	void* _zip_file;
};

// Call this to open a specific package and use the returned handle in calls to functions for
// loading from other application packages.
LVR_SCENE_API void* lvr_open_other_application_package( const char * packageName );

// Call this to close another application package after loading resources from it.
LVR_SCENE_API void lvr_close_other_application_package( void * & zipFile );

// These are probably NOT thread safe!
LVR_SCENE_API bool lvr_other_package_file_exists( void * zipFile, const char * nameInZip );

// Returns NULL buffer if the file is not found.
LVR_SCENE_API void lvr_read_file_from_other_application_package( void * zipFile, const char * nameInZip, int &length, void * & buffer );

// Returns 0 if the file is not found.
// For a file placed in the project assets folder, nameInZip would be
// something like "assets/cube.pvr".
// See GlTexture.h for supported formats.
LVR_SCENE_API lvr_texture*	lvr_load_texture_from_other_application_package( void * zipFile, const char * nameInZip,
	const uint32_t a_texture_flags, int & width, int & height );

// Returns NULL if the file is not found.
LVR_SCENE_API lvr_model_file *		lvr_load_model_file_from_other_application_package( void * zipFile, const char* nameInZip,
	const lvr_model_programs & programs,
	const lvr_material_parms & materialParms );

//--------------------------------------------------------------
// Functions for reading assets from this process's application package
//--------------------------------------------------------------

// returns the zip file for the applications own package
LVR_SCENE_API void *	lvr_get_application_package_file();

// This can be called multiple times, but it is ignored after the first one.
LVR_SCENE_API void lvr_open_application_package( const char * packageName );

// These are probably NOT thread safe!
LVR_SCENE_API bool lvr_package_file_exists( const char * nameInZip );

// Returns NULL buffer if the file is not found.
LVR_SCENE_API void lvr_read_file_from_application_package( const char * nameInZip, int & length, void * & buffer );

LVR_SCENE_API void lvr_release_package_file_buf(void * & buffer);

// Returns 0 if the file is not found.
// For a file placed in the project assets folder, nameInZip would be
// something like "assets/cube.pvr".
// See GlTexture.h for supported formats.

//lvr_e_texture_flags
//with mipmaps,and use normal rgb texture,if failed will return default 4*4 texture.
//#define LVR_TEXTUREFLAG_DEFAULT		0x00
// Normally, a failure to load will create an 8x8 default texture, but
// if you want to take explicit action, setting this flag will cause
// it to return 0 for the texId.
//#define LVR_TEXTUREFLAG_NO_DEFAULT  0x01
// Use GL_SRGB8 / GL_SRGB8_ALPHA8 / GL_COMPRESSED_SRGB8_ETC2 formats instead
// of GL_RGB / GL_RGBA / GL_ETC1_RGB8_OES
//#define LVR_TEXTUREFLAG_USE_SRGB  0x02
// No mip maps are loaded or generated when this flag is specified.
//#define LVR_TEXTUREFLAG_NO_MIPMAPS 0x04
//end lvr_e_texture_flags

LVR_SCENE_API lvr_texture*	lvr_load_texture_from_application_package( const char * nameInZip,
	const uint32_t a_texture_flags, int & width, int & height );

// Returns NULL if the file is not found.
LVR_SCENE_API lvr_model_file *		load_model_file_from_application_package( const char* nameInZip,
	const lvr_model_programs & programs,
	const lvr_material_parms & materialParms );

//////////////////////////////////////////////////////////////////////////
//absolute path add texture
LVR_SCENE_API lvr_texture*	lvr_load_texture_from_file( const char * a_name,
	const uint32_t a_texture_flags, int & ao_width, int & ao_height );


#endif
