#ifndef __lvr_cursor_h__
#define __lvr_cursor_h__

#include "lvr_scene_configure.h"
//common call procedure
//call once 
//init();


//call when need
//show_cursor();//call it to show the cursor
//hide_cursor();//call if you want to hide it
//set_state(state)//use different texture to present cursor
//or call this.
//set_cursor_texture


//called every frame
//0.update(view_mat,time_past_since_last_frame)//ex:0.0167
//1.draw(view_proj_matrix);//called at the end of a frame

enum lvr_enum_gaze_cursor_state_type
{
	LVR_CURSOR_STATE_NORMAL = 0,		// tap will not activate, but might drag
	LVR_CURSOR_STATE_HILIGHT = 1,		// tap will activate
	LVR_CURSOR_STATE_PRESS = 2,			// release will activate
	LVR_CURSOR_STATE_HAND = 3,			// dragging
	LVR_CURSOR_STATE_MAX = 4
};

class LVR_SCENE_API lvr_gaze_cursor
{
public:
	static const float	CURSOR_MAX_DIST;
	//change this to nums like 8 or 4 or such things,we will show the past 8 frames cursor place
	//ghost showing.
	static const int	TRAIL_GHOSTS = 1;
public:
	lvr_gaze_cursor();
	virtual ~lvr_gaze_cursor();

	// Initialize the gaze cursor system and loads the cursor images from
	// the application package.
	void				init();

	// Shutdown the gaze cursor system.
	void				uninit();

	// Updates the gaze cursor distance if the distance passed is less than the current
	// distance.  Systems that use the gaze cursor should use this method so that they
	// interact civilly with other systems using the gaze cursor.
	void				set_state(	lvr_enum_gaze_cursor_state_type const a_state );
	//choose one to work with new style.
	void				set_cursor_texture(lvr_enum_gaze_cursor_state_type const state,lvr_texture* a_texture);
	void				set_cursor_render_object(lvr_render_object*	a_ro);
	//////////////////////////////////////////////////////////////////////////
	void				set_tex_trans_info(const lvr_vector4f& a_tex_mat);
	//you can either call the following two api or just call the one above
	void				set_current_frame_id(int a_id);
	void				set_sequence_frame_info(int a_row_num,int a_column_num);
	int					get_sequence_frame_num();
	//////////////////////////////////////////////////////////////////////////
	void				set_rot_angle(float a_rot_angle);
	void				set_draw_percentage(float a_percentage);
	// Call when the scene changes or the camera moves a large amount to clear out the cursor trail
	void				clear_ghosts();

	// Called once per frame to update logic.
	void				update( lvr_matrix4f const & viewMatrix, float const deltaTime );

	// Renders the gaze cursor.
	void				draw( lvr_matrix4f const & vp ) const;

	// Returns the current info about the gaze cursor.
	lvr_enum_gaze_cursor_state_type  get_state() const;

	// Force the distance to a specific value -- this will set the distance even if
	// it is further away than the current distance. Unless your intent is to overload
	// the distance set by all other systems that use the gaze cursor, don't use this.
	void				set_distance( float const d );

	// Sets the rate at which the gaze cursor icon will spin.
	void				set_rotation_rate( float const degreesPerSec );

	// Sets the scale factor for the cursor's size.
	void				set_cursor_scale( float const scale );

	// Hide the gaze cursor.
	void				hide_cursor(){_b_hidden = true;}

	// Show the gaze cursor.
	void				show_cursor(){_b_hidden = false;}

	// Hide the gaze cursor for a specified number of frames
	// Used in App::Resume to hide the cursor interpolating to the gaze orientation from its initial orientation
	void				hide_cursor_for_frames( const int hideFrames ){_i_hidden_frames = hideFrames;}

	// Sets an addition distance to offset the cursor for rendering. This can help avoid
	// z-fighting but also helps the cursor to feel more 3D by pushing it away from surfaces.
	// This is the amount to move towards the camera, so it should be positive to bring the
	// cursor towards the viewer.
	void				set_distance_offset( float const offset ){_f_distance_offset = offset;}

	//four method with timer.

	// Start a timer that will be shown animating the cursor. If timeBeforeShowingTimer
	// > 0, then the cursor will not show the timer until that much time has passed.
	void				start_timer( float const durationSeconds, 
		float const timeBeforeShowingTimer );

	void				cancel_timer();
	bool				timer_active() const;
	void				reset_cursor();
private:
	float						_f_cursor_rotation;			// current cursor rotation
	float						_f_rotation_rate_radians;	// rotation rate in radians
	float						_f_cursor_scale;			// scale of the cursor
	float						_f_distance_offset;			// additional distance to offset towards the camera.
	int							_i_hidden_frames;			// Hide cursor for a number of frames 
	lvr_matrix4f				_cursor_transform_mats[TRAIL_GHOSTS];	// transform for each ghost
	lvr_matrix4f				_cursor_scatter_transform_mats[TRAIL_GHOSTS];	// transform for each depth-fail ghost
	int							_i_current_transform;		// the next CursorTransform[] to fill
	lvr_matrix4f				_timer_transform_mat;			// current transform of the timing cursor
	lvr_vector2f				_color_table_offset;		// offset into color table for color-cycling effects

	double						_timer_show_time;			// time when the timer cursor should show
	double						_timer_end_time;			// time when the timer will expire

	lvr_render_object			_cursor_geometry;			// VBO for the cursor
	lvr_texture*				_cursor_texture_ptrs[LVR_CURSOR_STATE_MAX];	// handle to the cursor's texture
	lvr_texture*				_timer_texture_ptr;		// handle to the texture for the timer
	lvr_texture*				_color_table_tex_ptr;		// handle to the cursor's color table texture
	lvr_program*				_cursor_program_ptr;			// vertex and pixel shaders for the cursor
	lvr_program*				_timer_program_ptr;			// vertex and pixel shaders for the timer
	int32_t						_u_color_table_offset;
	float						_f_distance_to_eye;
	lvr_enum_gaze_cursor_state_type		_cursor_state;

	lvr_render_object*			_user_cursor_object;
	lvr_vector4f				_tex_scale_mov_mat;
	float						_percentage;
	float						_rot_angle;
	int							_row,_column,_cur_frame_id;
	bool						_b_initialized;			// true once initialized
	bool						_b_hidden;					// true if the cursor should not render
};

#endif