#ifndef __lvr_axis_h__20170704__
#define __lvr_axis_h__20170704__

#include "lvr_scene_configure.h"

class LVR_SCENE_API lvr_axis_base
{
public:
	enum axis_type
	{
		e_axis_common,
		e_axis_rotate,
		e_axis_scale
	};
	enum aixs_name
	{
		e_axis_none = 0x0,
		e_axis_x = 0x1,
		e_axis_y = 0x2,
		e_axis_z = 0x4,
		e_axis_xy = 0x3,
		e_axis_yz = 0x6,
		e_axis_zx = 0x5
	};
public:
	lvr_axis_base();
	virtual ~lvr_axis_base();
public:
	virtual void init_with_gl() = 0;
	virtual int test_ray(const lvr_vector3f& a_origin, const lvr_vector3f& a_dir) = 0;
	virtual void set_operate_value(float a_value) = 0;
	virtual void draw(const lvr_matrix4f& a_view_proj_mat) = 0;

	virtual void set_axis(const lvr_vector3f& a_x, const lvr_vector3f& a_y, const lvr_vector3f& a_z);
	virtual void set_visible(bool a_visible);
	virtual bool get_visible();
	virtual void set_position(const lvr_vector3f& a_pos);
	virtual void set_orientation(const lvr_quaternionf& a_q);
	virtual float adjust_size(const lvr_vector3f& a_camera_pos);
	virtual void lock_operate_axis(int axis_info);
	int get_lock_axis()const { return _lock_axis_info; }
protected:
	lvr_vector3f	_position;
	lvr_vector3f	_scale;
	lvr_vector3f	_axis[3];
	lvr_vector4uc	_color[3];
	lvr_quaternionf	_rotation;
	int				_lock_axis_info;
	bool			_visible;
};

#endif
