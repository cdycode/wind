#ifndef __lvr_movie_render_h__
#define __lvr_movie_render_h__

#include "lvr_scene_util.h"

class LVR_SCENE_API lvr_movie_render
{
public:
	enum movie_format{
		unknown = 0,
		common_2d = 1,
		left_right_3d,
		left_right_3d_full,
		right_left_3d,
		right_left_3d_full,
		top_bottom_3d,
		top_bottom_3d_full,
		bottom_top_3d,
		bottom_top_3d_full,
		pano_360 ,//10
		pano_360_LR,
		pano_360_RL,
		pano_360_TD,
		pano_360_DT,
		pano_180,
		pano_180_LR,
		pano_180_RL,
		pano_180_TD,
		pano_180_DT,
		pano_cb,
		pano_cb_LR,
		pano_cb_RL,
		pano_cb_TD,
		pano_cb_DT,
		movie_count
	};
public:
	static bool		generate_tex_mat_info(movie_format a_format,lvr_vector4f ao_texmat[2]);
public:
	lvr_movie_render();
	~lvr_movie_render();
public:
	void		init();
	void		uninit();
	//void		set_icon_texture(lvr_texture* a_texture);//this should be cared outside
	//called after set movie type.don't call it before set_movie_type;
	void		set_free_screen_pos(const lvr_vector3f& a_pos);
	void		set_screen_scale(float w_scale, float h_scale);
	void		set_screen_ro(lvr_render_object* a_ro);
	void		set_movie_type(movie_format a_movie_type);
	lvr_movie_render::movie_format get_movie_type();
	void		set_movie_texture(lvr_texture*	a_texture);
	void		set_screen_center(const lvr_vector3f& a_pos);
	void		set_screen_size(float a_width,float a_height);
	void 		draw(const lvr_matrix4f& a_view_proj_mat,int a_eye);
	lvr_program*	get_current_movie_prog();
	lvr_vector4f*	get_current_movie_texm();
private:
	void		generate_pi_mesh(float a_horizontal_angle, float a_view_angle);//should between 0-pi/2
	void		generate_cube_mesh(float a_radius);
private:
	lvr_program*		_single_texture_prog;
	lvr_program*		_single_texture_prog_oes;

	lvr_program*		_program_use;
	lvr_texture*		_movie_texture;
	lvr_render_object*	_free_movie_screen_ro;
	lvr_vertex_buffer*	_screen_vbo;
	lvr_render_object*	_globe_mesh_ro;
	lvr_render_object*	_cube_mesh_ro;
	lvr_render_object*	_fan_mesh_ro;
	lvr_render_object*	_current_ro;
	movie_format		_movie_type;
	lvr_vector3f		_pos;
	float				_width,_height;
	lvr_vector4f		_texmat[2];//work with the movie type
	float				_radius_use;
	float				_w_scale_screen;
	float				_h_scale_screen;
	float				_h_over_w;
	bool				_need_update_ro;
};

#endif

