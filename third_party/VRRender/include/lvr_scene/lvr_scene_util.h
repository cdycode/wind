#ifndef __lvr_scene_util_h__
#define __lvr_scene_util_h__

#include "lvr_scene_configure.h"
#include "lvr_primitive_data_base.h"

LVR_SCENE_API lvr_texture* lvr_load_texture_from_buffer(const char* a_file_name,const char * a_buffer, const int a_size,const uint32_t a_tex_flags);
//pos uv only,currently we do not use normal
LVR_SCENE_API lvr_render_object* lvr_create_standard_primitive_ro(lvr_primitive_data_base* t_pb);
LVR_SCENE_API void lvr_release_primitive_ro(lvr_render_object*& ao_ro);

LVR_SCENE_API void lvr_change_gpu_state( const lvr_gpu_state& oldState, const lvr_gpu_state& newState );

#endif