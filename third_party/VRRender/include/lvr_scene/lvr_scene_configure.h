#ifndef __lvr_scene_configure_h__
#define __lvr_scene_configure_h__

#include "lvr_core_lib.h"
// #ifdef LVR_OS_WIN32
// #define lvr_render_IMPORT
// #endif
#include "lvr_render_lib.h"

#ifdef lvr_scene_EXPORTS
#define LVR_SCENE_API LVR_API_EXPORT
#elif defined lvr_scene_IMPORT
#define LVR_SCENE_API LVR_API_IMPORT
#else
#define LVR_SCENE_API LVR_API_STATIC
#endif

#define  LVR_MAX_JOINTS 16

#endif
