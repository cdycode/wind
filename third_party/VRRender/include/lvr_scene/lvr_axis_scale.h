#ifndef __lvr_axis_scale_h__20170705__
#define __lvr_axis_scale_h__20170705__

#include "lvr_axis_base.h"

class LVR_SCENE_API lvr_axis_scale : public lvr_axis_base
{
public:
	lvr_axis_scale();
	~lvr_axis_scale();
public:
	virtual void init_with_gl();
	virtual void set_position(const lvr_vector3f& a_pos);
	virtual void set_operate_value(float a_value);
	virtual int test_ray(const lvr_vector3f& a_origin, const lvr_vector3f& a_dir);
	virtual void draw(const lvr_matrix4f& a_view_proj_mat);
private:
	bool intersect_axis(const lvr_vector3f& axis, float w, float h, const lvr_vector3f& a_origin, const lvr_vector3f& a_dir, float& dist);
public:
	float				_origin_axis_w, _origin_axis_h;
	lvr_render_object	_scale_ro;
	lvr_render_object	_operate_ro;
	lvr_program*		_program;
	uint32_t			_per_axis_index_num;
	float				_radius;
	float				_scale_value;
};

#endif