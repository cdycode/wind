#ifndef __lvr_scene_manager_h__
#define __lvr_scene_manager_h__

#include "lvr_scene_configure.h"
#include "lvr_model_file.h"
#include "lvr_light.h"

class LVR_SCENE_API lvr_scene_manager
{
public:
	static lvr_scene_manager*	get_ins(void);
	static void				destory_ins(void);
protected:
	lvr_scene_manager(void);
	~lvr_scene_manager(void);

public:
	bool	init(void);
	void	uninit(void);

	void	set_current_scene(lvr_model_file* a_model_file);
	lvr_model_file*	get_current_scene();
	const lvr_vector3f& get_current_camera_pos()const;

	void	update(const float timeInSeconds);
	void 	draw(const lvr_matrix4f& a_view_mat,const lvr_matrix4f& a_proj_mat);

	lvr_model_file*	load_scene(const char* a_scene_path,bool a_from_package);
	void	release_model_file(lvr_model_file*& a_model_file);
	lvr_light&  get_direction_light();
private:
	void	set_to_default_gpu_state();
	void	animate_joints( const float timeInSeconds );
private:
	static	lvr_scene_manager*	_sg_secne_mgr;

	lvr_gpu_state				_basic_mode_gpu_state;
	lvr_model_file*				_current_render_scene;
	lvr_model_programs			_default_programs;
	lvr_model_programs			_dynamic_programs;
	lvr_vector3f				_current_camera_pos;

	lvr_light					_point_light;
	lvr_light					_direction_light;
	lvr_vector4f				_env_light_color_intensity;

	std::vector<lvr_matrix4f>	_joints;
	uint32_t					_joint_num;
};

#endif
