#ifndef __lvr_axis_common_h__20170704__
#define __lvr_axis_common_h__20170704__

#include "lvr_axis_base.h"

class LVR_SCENE_API lvr_axis_common : public lvr_axis_base
{
public:
	lvr_axis_common();
	~lvr_axis_common();
public:
	virtual void init_with_gl();
	virtual void set_position(const lvr_vector3f& a_pos);
	virtual void set_axis(const lvr_vector3f& a_x, const lvr_vector3f& a_y, const lvr_vector3f& a_z);
	virtual int test_ray(const lvr_vector3f& a_origin, const lvr_vector3f& a_dir);
	virtual void set_operate_value(float a_value);
	virtual void draw(const lvr_matrix4f& a_view_proj_mat);
private:
	bool   intersect_axis(const lvr_vector3f& axis,float w,float h,const lvr_vector3f& a_origin, const lvr_vector3f& a_dir, float& dist);
public:
	float				_origin_axis_w, _origin_axis_h;
	lvr_render_object	_ro_axises;
	lvr_render_object	_ro_faces;//xy,yz,zx face
	lvr_render_object	_ro_face_line;
	lvr_vector3f		_face_pos[9];
	lvr_program*		_program;
	int					_face_id_to_draw;
	uint32_t			_per_axis_index_num;
	bool				_standard_axis;
};

#endif