#ifndef __lvr_sky_box_h__20161121__
#define __lvr_sky_box_h__20161121__

#include "lvr_scene_configure.h"
#include "lvr_texture_cube.h"
class lvr_zip;


class LVR_SCENE_API lvr_sky_box_cube
{
public:
	lvr_sky_box_cube();
	~lvr_sky_box_cube();
public:
	//you can also directly use load to surface without init this,we will use the first surface parms to init sky box.
	bool init(int a_cube_resolution,int format);
	void uninit();
	void set_pos(lvr_vector3f	a_pos);
	void set_radius(float	a_radius);
	//the second parm is not in use currently.
	void load_from_json(lvr_zip* a_zip);
	bool load_to_surface(const char* a_file_path,lvr_enum_cube_face a_face);
	bool load_to_surface_from_mem(const char* a_file_buf,int32_t a_buf_len,const char* a_ext, lvr_enum_cube_face a_face);
	bool load_from_zip(const char* a_zip_file_path);
	void draw(const lvr_matrix4f& a_vp_mat);
	static lvr_enum_cube_face get_cube_map_face(const char* a_name);
public:
	lvr_texture_cube*		_cube_tex;
	lvr_render_object*		_sphere_ro;
	lvr_matrix4f			_world_matrix;
	lvr_vector3f			_pos;
	float					_radius;
	lvr_program*			_prog;
	bool					_inited;
	lvr_matrix4f            _rotateMatOppsite;
};

#endif