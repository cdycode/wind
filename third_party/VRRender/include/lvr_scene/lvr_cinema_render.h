#ifndef __lvr_cinema_render_h__
#define __lvr_cinema_render_h__

#include "lvr_model_file.h"
#include "lvr_movie_render.h"
//usage
//1. init();
//   set_movie_texture(tex);//update this texture every frame
//	 option A:
//   set_scene(lvr_mode_file*);//if you do not set scene,then it is black scene.useful for 360 or free screen.
//	 option B:
//   lvr_movie_render& get_movie_render();
//		 movie_render.set_movie_type();//set left_right_3d, 360,and so on.
//		do call use_free_screen(false);if you want to use scrren scene supported.
//		call use_free_screen();//set if you want free screen or scene supported.
//2.  pre_play(3.0f);//use 3.0s to see movie playing
//	  post_stop(3.0f);//when movie is over,use 3.0s to back to normal sceen rendering.
//3.  called every frame:
//	  update(time_now in seconds);
//    draw(viewLeft,proj,0);draw(viewRight,proj,1);

//4.the next three function used to get proper camera postion.also can set camera pos to movie_render in free screen. 
//  set movie_render pos to zero if you are using screen supported by current scene.
//	get_total_seat_num();
//	choose_seat();
//	get_seat_pos();

class LVR_SCENE_API lvr_cinema_render
{
public:
	lvr_cinema_render();
	~lvr_cinema_render();
public:
	void		init();
	void		uninit();
	lvr_movie_render&	get_movie_render();
	void		set_scene(lvr_model_file*	_scene);

	int			get_total_seat_num()	const;
	void		choose_seat(int		a_seat_id);
	const lvr_vector3f& get_seat_pos()const;
	void		set_movie_texture(lvr_texture*	a_texture);

	void		pre_play(float time_to_play);
	void		post_stop(float time_to_reback);
	void		update(const float a_time);
	void		use_free_screen(bool a_using_free_screen);
	void 		draw(const lvr_matrix4f& a_view_mat, const lvr_matrix4f& a_proj_mat,int a_eye);
	void		set_light_brightness(float a_s);
	bool		get_screen_center(lvr_vector3f& pos);
private:
	void		lights_on(const float a_duration);
	void		lights_off(const float a_duration);
	void		set_to_default_gpu_state();
	void		draw_static_scene(const lvr_matrix4f& a_view_mat, const lvr_matrix4f& a_proj_mat);
	void		draw_dynamic_movie_scene(const lvr_matrix4f& a_view_mat, const lvr_matrix4f& a_proj_mat,int a_eye);
	//currently,we move this into the above two methods.
	void		draw_movie_screen(const lvr_matrix4f& a_view_mat, const lvr_matrix4f& a_proj_mat);
	void		update_render_queue(const lvr_vector3f& a_sort_center);
private:
	lvr_movie_render	_movie_render;
	lvr_model_file*		_scene;
	int					_current_seat_id;
	lvr_vector3f		_current_seat_pos;

	lvr_program*		_opaque_prog;
	lvr_program*		_dynamic_only_prog;
	lvr_program*		_additive_prog;
	lvr_program*		_single_texture_prog;
	lvr_program*		_single_texture_prog_oes;

	lvr_texture*		_movie_texture;
	uint32_t			_joint_num;
	lvrSurfaceDef*			_movie_screen;
	lvr_render_object*	_free_movie_screen_ro;
	float				_current_light_value;
	int					_lighting_state;
	float				_begin_time;
	float				_end_time;
	float				_width;
	float				_height;
	float				_light_brightness;
	int					_light_bright_loc;
	bool				_use_screen_for_movie;
	//movie playing
	bool				_use_dynamic_program;
	bool				_use_free_screen;
	lvr_gpu_state		_basic_mode_gpu_state;
	std::vector<lvr_matrix4f>	_joints;
	std::vector<lvrSurfaceDef*>	_render_queue;

};

#endif
