#ifndef __lvr_model_file_h__
#define __lvr_model_file_h__

#include "lvr_scene_configure.h"
#include <vector>
#include <string>
#include "lvr_RtTrace.h"

class lvr_material_parms
{
public:
	lvr_material_parms() :
UseSrgbTextureFormats( false ),
	EnableDiffuseAniso( false ),
	EnableEmissiveLodClamp( true ),
	Transparent( false ),
	PolygonOffset( false ) { }

bool	UseSrgbTextureFormats;		// use sRGB textures
bool	EnableDiffuseAniso;			// enable anisotropic filtering on the diffuse texture
bool	EnableEmissiveLodClamp;	// enable LOD clamp on the emissive texture to avoid light bleeding
bool	Transparent;				// surfaces with this material flag need to render in a transparent pass
bool	PolygonOffset;				// render with polygon offset enabled
};

struct lvr_ModelTexture
{
	std::string		name;
	lvr_texture*	tex;
};

enum lvrModelJointAnimation
{
	MODEL_JOINT_ANIMATION_NONE_LVR,
	MODEL_JOINT_ANIMATION_ROTATE_LVR,
	MODEL_JOINT_ANIMATION_SWAY_LVR,
	MODEL_JOINT_ANIMATION_BOB_LVR
};

struct lvrModelJoint
{
	int					index;
	std::string			name;
	lvr_matrix4f		transform;
	lvrModelJointAnimation	animation;
	lvr_vector3f		parameters;
	float				timeOffset;
	float				timeScale;
};

struct lvrModelTag
{
	std::string			name;
	lvr_matrix4f		matrix;
	lvr_vector4i		jointIndices;
	lvr_vector4f		jointWeights;
	std::vector<lvr_vector3f>	_points;
	std::vector<uint16_t>		_indices;
};
// can be made as high as 16
static const int MAX_PROGRAM_TEXTURES_LVR = 5;

// number of vec4
static const int MAX_PROGRAM_UNIFORMS_LVR = 1;


struct lvrMaterialDef
{
	lvrMaterialDef() :
programObject( 0 ),
	uniformMvp( -1 ),
	uniformModel( -1 ),
	uniformView( -1 ),
	uniformProjection( -1 ),
	uniformJoints( -1 ),
	numTextures( 0 ) {
		uniformSlots[0] = -1;
		accept_shadow = false;
}

lvr_gpu_state	gpu_state;				// blending, depth testing, etc

// We might want to reference a gpuProgram_t object instead of copying these.
uint32_t	programObject;
int32_t		uniformMvp;
int32_t		uniformModel;
int32_t		uniformView;
int32_t		uniformProjection;
int32_t		uniformJoints;

const lvr_program*	prog;

// Parameter setting stops when uniformSlots[x] == -1
int32_t		uniformSlots[MAX_PROGRAM_UNIFORMS_LVR];
GLfloat		uniformValues[MAX_PROGRAM_UNIFORMS_LVR][4];

// Additional uniforms for lighting and so on will need to be added.

// Currently assumes GL_TEXTURE_2D for all; will need to be
// extended for GL_TEXTURE_CUBE and GL_TEXTURE_EXTERNAL_OES.
//
// There should never be any 0 textures in the active elements.
//
// This should be a range checked container.
int			numTextures;
lvr_texture*	textures[MAX_PROGRAM_TEXTURES_LVR];
bool		accept_shadow;
};

class lvrSurfaceDef
{
public:
	lvrSurfaceDef():_scale(1,1,1),_use_world_pose(false),_visible(true) {};
	~lvrSurfaceDef(){
		geo.release_res();
	}

	// Name from the model file, can be used to control surfaces with code.
	// May be multiple semi-colon separated names if multiple source meshes
	// were merged into one surface.
	std::string			surfaceName;

	// We may want a do-not-cull flag for trivial quads and
	// skybox sorts of geometry.
	lvr_bounding_box3f		cullingBounds;

	// There is a space savings to be had with triangle strips
	// if primitive restart is supported, but it is a net speed
	// loss on most architectures.  Adreno docs still recommend,
	// so it might be worth trying.
	lvr_render_object		geo;

	// This could be a constant reference, but inline has some
	// advantages for now while the definition is small.
	lvrMaterialDef		materialDef;
	//lvr_posef		
	lvr_posef		_world_pose;//initly it is identity
	lvr_vector3f	_scale;
	bool			_use_world_pose;
	bool			_visible;
	//lvr_matrix4f	_local_mat;//inv should also be stored,but now,store it in the near future.
	//do not change this value outside,this is the origin information.
	lvr_vector3f	_world_position;//origion position.//currently only translate info.
};

class LVR_SCENE_API lvrModelDef
{
public:
	lvrModelDef() {};
	void release()
	{
		std::vector<lvrSurfaceDef*>::iterator it = surfaces.begin();
		while(it != surfaces.end())
		{
			delete *it;
			it++;
		}
	}

	std::vector<lvrSurfaceDef*>	surfaces;
};

class LVR_SCENE_API lvr_collision_polytope
{
public:
	void	add_plane( const lvr_plane3f & p ) { Planes.push_back( p ); }

	// Returns true if the given point is inside this polytope.
	bool	test_point( const lvr_vector3f & p ) const;

	// Returns true if the ray hits the polytope.
	// The length of the ray is clipped to the point where the ray enters the polytope.
	// Optionally the polytope boundary plane that is hit is returned.
	bool	test_ray( const lvr_vector3f & start, const lvr_vector3f & dir, float & length, lvr_plane3f * plane ) const;

	// Pops the given point out of the polytope if inside.
	bool	pop_out_point( lvr_vector3f & p ) const;

public:
	std::string			Name;
	std::vector< lvr_plane3f > Planes;
};

class LVR_SCENE_API lvr_collision_model
{
public:
	// Returns true if the given point is inside solid.
	bool	test_point( const lvr_vector3f & p ) const;

	// Returns true if the ray hits solid.
	// The length of the ray is clipped to the point where the ray enters solid.
	// Optionally the solid boundary plane that is hit is returned.
	bool	test_ray( const lvr_vector3f & start, const lvr_vector3f & dir, float & length, lvr_plane3f * plane ) const;

	// Pops the given point out of any collision geometry the point may be inside of.
	bool	pop_out_point( lvr_vector3f & p ) const;

public:
	std::vector< lvr_collision_polytope > Polytopes;
};

class lvr_model_programs
{
public:
	lvr_model_programs() :
ProgVertexColor( NULL ),
	ProgSingleTexture( NULL ),
	ProgLightMapped( NULL ),
	ProgReflectionMapped(NULL), 
	ProgRealTimeRendering(NULL),
	ProgSkinnedVertexColor( NULL ),
	ProgSkinnedSingleTexture( NULL ),
	ProgSkinnedLightMapped( NULL ),
	ProgSkinnedReflectionMapped( NULL ),
	ProgSkinnedRealTimeRendering(NULL),
	HighLevelCase(false)
		{}
lvr_model_programs( const lvr_program* singleTexture ) :
ProgVertexColor( singleTexture ),
	ProgSingleTexture( singleTexture ),
	ProgLightMapped( singleTexture ),
	ProgReflectionMapped( singleTexture ),
	ProgRealTimeRendering(singleTexture),//???
	ProgSkinnedVertexColor( singleTexture ),
	ProgSkinnedSingleTexture( singleTexture ),
	ProgSkinnedLightMapped( singleTexture ),
	ProgSkinnedReflectionMapped( singleTexture ),
	ProgSkinnedRealTimeRendering(singleTexture)//??
		{}
lvr_model_programs( const lvr_program* singleTexture, const lvr_program* dualTexture ) :
ProgVertexColor( singleTexture ),
	ProgSingleTexture( singleTexture ),
	ProgLightMapped( dualTexture ),
	ProgReflectionMapped( dualTexture ),
	ProgRealTimeRendering(dualTexture),//??
	ProgSkinnedVertexColor( singleTexture ),
	ProgSkinnedSingleTexture( singleTexture ),
	ProgSkinnedLightMapped( dualTexture ),
	ProgSkinnedReflectionMapped( dualTexture ),
	ProgSkinnedRealTimeRendering(dualTexture)//??
		{}

const lvr_program* ProgVertexColor;
const lvr_program* ProgSingleTexture;
const lvr_program* ProgLightMapped;
const lvr_program* ProgReflectionMapped;
const lvr_program* ProgRealTimeRendering;
const lvr_program* ProgSkinnedVertexColor;
const lvr_program* ProgSkinnedSingleTexture;
const lvr_program* ProgSkinnedLightMapped;
const lvr_program* ProgSkinnedReflectionMapped;
const lvr_program* ProgSkinnedRealTimeRendering;
bool			HighLevelCase;
};

// A ModelFile is the in-memory representation of a digested model file.
// It should be imutable in normal circumstances, but it is ok to load
// and modify a model for a particular task, such as changing materials.
class LVR_SCENE_API lvr_model_file
{
public:
	lvr_model_file();
	lvr_model_file( const char * name ) : FileName( name ) {}
	~lvr_model_file();	// Frees all textures and geometry

	lvrSurfaceDef *				FindNamedSurface( const char * name ) const;
	const lvr_ModelTexture *		FindNamedTexture( const char * name ) const;
	const lvrModelJoint *			FindNamedJoint( const char * name ) const;
	lvrModelTag *			FindNamedTag( const char * name );

	int							GetJointCount() const { return Joints.size(); }
	const lvrModelJoint *			GetJoint( const int index ) const { return &Joints[index]; }
	lvr_box3f					GetBounds() const;
	int							get_suggested_camera_pos_num();
	bool						get_suggest_camera_pos(int a_pos_id,lvr_vector3f* ao_pos_ptr);

public:
	std::string						FileName;
	bool						UsingSrgbTextures;

	// Textures will need to be freed if the model is unloaded,
	// and applications may include additional textures not
	// referenced directly by the scene geometry, such as
	// extra lighting passes.
	std::vector<lvr_ModelTexture>		Textures;

	std::vector<lvrModelJoint>			Joints;

	std::vector<lvrModelTag>			Tags;

	// This is used by the rendering code
	lvrModelDef					Def;

	// This is used by the movement code
	lvr_collision_model				Collisions;
	lvr_collision_model				GroundCollisions;

	// This is typically used for gaze selection.
	RtTrace						TraceModel;
	int							_suggest_camera_pos_num;
};

// Pass in the programs that will be used for the model materials.
// Obviously not very general purpose.
LVR_SCENE_API lvr_model_file * lvr_load_model_file_from_memory( const char * fileName,
	const void * buffer, int bufferLength,
	const lvr_model_programs & programs,
	const lvr_material_parms & materialParms );

LVR_SCENE_API lvr_model_file * lvr_load_model_file( const char * fileName,
	const lvr_model_programs & programs,
	const lvr_material_parms & materialParms );

#endif
