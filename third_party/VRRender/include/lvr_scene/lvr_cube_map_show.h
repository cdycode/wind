#ifndef __lvr_cube_map_show_h__20170814__
#define __lvr_cube_map_show_h__20170814__

#include "lvr_scene_configure.h"

class lvr_cube_map_show
{
public:
	lvr_cube_map_show();
	~lvr_cube_map_show();
public:
	void init(const lvr_vector3f& a_pos, float a_radius);
	void uninit();
	void set_cube_map_texture(lvr_texture* a_texture);
	void draw(const lvr_matrix4f& a_view_proj_mat);
public:
	lvr_program*		_prog;
	lvr_render_object*	_sphere_ro;
	lvr_vector3f		_pos;
	lvr_texture*		_cube_map_tex;;
};


#endif
