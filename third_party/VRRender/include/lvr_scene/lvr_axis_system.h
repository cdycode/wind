#ifndef __lvr_scene_axis_system_h__
#define __lvr_scene_axis_system_h__

#include "lvr_scene_configure.h"

class LVR_SCENE_API lvr_axis_system
{
public:
	lvr_axis_system();
	~lvr_axis_system();
public:
	void init();
	void uninit();
	void update(lvr_vector3f a_camera_pos,const lvr_matrix4f& a_view_mat);
	void draw(const lvr_matrix4f& a_view_proj_mat,lvr_program* a_prog);
private:
	lvr_render_object	_axis_system;
	lvr_vector3f		_axis_pos;
};

#endif