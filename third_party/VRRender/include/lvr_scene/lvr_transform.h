#ifndef __lvr_transform_h__
#define __lvr_transform_h__

#include "lvr_scene_configure.h"

class  LVR_SCENE_API lvr_transform
{
public:
	 lvr_transform();
	~lvr_transform();
public:
	void set_position(const lvr_vector3f& a_pos);
	lvr_vector3f get_translation();
	void set_scale(const lvr_vector3f& a_scale);
	void set_roatation(const lvr_quaternionf& a_q);
	const lvr_quaternionf& get_rotation();
	bool contains_nan() const
	{
		return (_translation.is_nan() || _rotation.is_nan() || _scale.is_nan());
	}
	const lvr_matrix4f& get_matrix();
public:
	int				_flags;
	lvr_matrix4f	_trans_matrix;
	lvr_vector3f	_translation;
	lvr_quaternionf	_rotation;
	//lvr_vector3f	_euler_angles;
	lvr_vector3f	_scale;
};


#endif
