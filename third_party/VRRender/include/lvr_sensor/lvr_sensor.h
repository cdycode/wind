//
// Created by sunpengfei on 2016/5/6.
//

#ifndef LAUNCHER_LVR_SENSOR_H
#define LAUNCHER_LVR_SENSOR_H

extern "C"
{
void lvr_init_sensors( int a_gyro_type );

void lvr_get_sensor_quat( double time, float* x, float* y, float* z, float* w );

void lvr_uninit_sensors(void);
bool lvr_get_sensorbox_status();
void lvr_set_sensorbox_status(bool flag);
void lvr_get_calibration( float* acc_x, float* acc_y, float* acc_z, float* mag_x, float* mag_y, float* mag_z, float* gyr_x, float* gyr_y, float* gyr_z );
void lvr_set_calibration_gyr(  float gyr_x, float gyr_y, float gyr_z );
void lvr_get_sensor_position( float* x, float* y, float* z );

void lvr_begin_tune();

void lvr_end_tune();

int lvr_get_progress();

void lvr_get_calibration_gyr( float* gyr_x, float* gyr_y, float* gyr_z );

//直接暴露算法的V2版本接口
void*	lvr_create_sensor_fusion (void);

void	lvr_destory_sensor_fusion ( void** a_fusion_ptr);

void	lvr_update_sensor_data_v2 (void* a_fusion_ptr, float accx, float accy, float accz, float gyrox, float gyroy, float gyroz );

void	lvr_get_current_orientation_v2 (void* a_fusion_ptr, double a_time_point, float* ao_x, float* ao_y, float* ao_z, float* ao_w );


//void __lvr_update_sensor_data( lsf_e_data_type a_data_type, long long a_time_stamp, float a_data_x, float a_data_y, float a_data_z );
};

#endif //LAUNCHER_LVR_SENSOR_H
