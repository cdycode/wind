#ifndef __lvr_engine_define_h__
#define __lvr_engine_define_h__

enum lvr_mouse_action
{
	lvr_mouse_lb_down,
	lvr_mouse_lb_up,
	lvr_mouse_rb_down,
	lvr_mouse_rb_up,
	lvr_mouse_lb_click,
	lvr_mouse_rb_click,
	lvr_mouse_lb_dbclick,
	lvr_mouse_rb_dbclick,
	lvr_mouse_wheel,
	lvr_mouse_move
};
enum lvr_gamepad
{
	lvr_gamepad_A,
	lvr_gamepad_B,
	lvr_gamepad_X,
	lvr_gamepad_Y,
	lvr_gamepad_L1,
	lvr_gamepad_L2,
	lvr_gamepad_L3,
	lvr_gamepad_R1,
	lvr_gamepad_R2,
	lvr_gamepad_R3,
	lvr_gamepad_UP,
	lvr_gamepad_DOWN,
	lvr_gamepad_LEFT,
	lvr_gamepad_RIGHT,
	lvr_gamepad_MENU1,
	lvr_gamepad_MENU2,
	lvr_gamepad_MENU3
};
enum lvr_action
{
	lvr_action_up,
	lvr_action_down,
	lvr_action_repeat//key hold.
};

enum lvr_mouse_button
{
	lvr_mouse_left,
	lvr_mouse_middle,
	lvr_mouse_right
};


class lvr_call_backs
{
public:
	virtual void error_cb(int a_error_code,const char* a_msg){}
	virtual void win_pos_cb(int a_xpos,int a_ypos){}
	virtual void reshape_cb(int a_w,int a_h){}
	virtual void joy_cb(int a_joy_code,int a_action){}
	virtual void mouse_cb(int a_button,int a_action,int a_mods){}
	virtual void key_cb(int key,int scancode,int action,int mods){}
	virtual void mouse_move_cb(double a_xpos,double a_ypos){}
	virtual void refresh_cb(){}
};

#endif
