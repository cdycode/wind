#ifndef __lvr_engine_gl_context_h__
#define __lvr_engine_gl_context_h__

#include "lvr_engine_configure.h"

class lvr_gl_configuration
{
public:
	lvr_gl_configuration(uint32_t a_major,uint32_t a_minor,uint32_t a_red_bits,
		uint32_t a_green_bits,uint32_t a_blue_bits,uint32_t a_alpha_bits,uint32_t a_depth_bits,uint32_t a_stencil_bits)
	:major(a_major),minor(a_minor),red_bits(a_red_bits),green_bits(a_green_bits),blue_bits(a_blue_bits),
	alpha_bits(a_alpha_bits),depth_bits(a_depth_bits),stencil_bits(a_stencil_bits){}
	lvr_gl_configuration()
		:major(2),minor(0),red_bits(8),green_bits(8),blue_bits(8),alpha_bits(8)
		,depth_bits(16),stencil_bits(0){}
public:
	uint32_t major; ///< depth buffer depth in bits
	uint32_t minor; ///< stencil buffer depth in bits
	uint32_t red_bits; ///< red color channel depth in bits
	uint32_t green_bits; ///< green color channel depth in bits
	uint32_t blue_bits; ///< blue color channel depth in bits
	uint32_t alpha_bits; ///< alpha color channel depth in bits
	uint32_t depth_bits; ///< depth buffer depth in bits
	uint32_t stencil_bits; ///< stencil buffer depth in bits
};

class lvr_gl_context
{
public:
	typedef void (*GLproc)(void);
	virtual GLproc getGLProcAddress(const char* procname) = 0;
	virtual bool bind() = 0;
	virtual bool unbind() = 0;
	virtual bool swap() = 0;
	virtual bool set_swap_interval(int32_t interval) = 0;
	virtual int32_t get_width() = 0;
	virtual int32_t get_height() = 0;
protected:
	lvr_gl_configuration	_gl_config;
};

#endif