#ifndef __lvr_app_base_h__
#define __lvr_app_base_h__

#include "lvr_engine.h"
#include "lvr_gl_context.h"
class lvr_app_base : public lvr_call_backs
{
public:
	lvr_app_base(lvr_engine* a_eng,const char* a_title);
	virtual ~lvr_app_base();
public:
	virtual void init() = 0;
	virtual void uninit() = 0;
	virtual void update() = 0;
	virtual void draw() = 0;
	virtual void focus_change(bool a_focused){}
	virtual bool hander_key(uint32_t a_key,lvr_action a_act){return false;}
	virtual bool hander_mouse(lvr_mouse_action a_act,int xpos,int ypos,int a_val){return false;}
	virtual void main_loop();
	virtual bool get_requested_window_size(int32_t& width, int32_t& height) { return false; }
	lvr_gl_context*	get_gl_context(){
		return _context;
	}
	void	set_gl_context(lvr_gl_context* a_context){_context = a_context;}
	lvr_engine*		get_engine(){
		return _engine;
	}
protected:
	void request_exit();
	bool is_exiting(){return _request_exit;}
	int  _width;
	int  _height;
	lvr_engine*		_engine;
	lvr_gl_context*	_context;
	std::string _app_title;
	bool _request_exit;
	bool _show_fps;
};

extern lvr_app_base*  lvr_app_factory(lvr_engine* a_engine);

#endif
