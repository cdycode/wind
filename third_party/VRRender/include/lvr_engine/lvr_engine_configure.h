#ifndef __lvr_engine_configure_h__
#define __lvr_engine_configure_h__

#ifdef lvr_engine_EXPORTS
#define LVR_ENGINE_API __declspec(dllexport)
#elif lvr_engine_IMPORT
#define LVR_ENGINE_API __declspec(dllimport)
#else
#define LVR_ENGINE_API
#endif

#include "lvr_core_lib.h"
#include "lvr_math_lib.h"
// #ifdef LVR_OS_WIN32
// #define lvr_scene_IMPORT
// #define lvr_ui_IMPORT
// #endif
#include "lvr_scene_lib.h"
//#include "lvr_ui_lib.h"

#endif
