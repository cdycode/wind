#ifndef __lvr_engine_h__
#define __lvr_engine_h__

#include "lvr_engine_configure.h"
#include "lvr_engine_define.h"

class lvr_engine
{;
public:
	lvr_engine(){}
	virtual ~lvr_engine(){}
public:
	virtual bool is_app_running() = 0;
	virtual void request_exit() = 0;
	virtual bool poll_events(lvr_call_backs*) = 0;
	virtual bool is_context_lost() = 0;
	virtual bool is_context_bound() = 0;
	virtual bool should_render() = 0;
	virtual bool has_window_resized() = 0;
	virtual void set_app_title(const char* a_title) = 0;
};

#endif
