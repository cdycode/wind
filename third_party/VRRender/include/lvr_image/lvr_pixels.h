#ifndef __lvr_pixels_h__
#define __lvr_pixels_h__

#include "lvr_image_configure.h"

class lvr_pixels
{
public:
	lvr_pixels()
		:pixels(0),width(0),height(0),buf_size(0),pixel_type(lvr_e_pixel_unknown)
	{

	}
public:
	uint8_t*			pixels;
	int32_t				width;
	int32_t				height;
	int32_t				buf_size;
	lvr_enum_pixel_type	pixel_type;
public:
	void clear()
	{
		if (pixels)
		{
			delete[] pixels;
			pixels = 0;
		}
		width = 0;
		height = 0;
		buf_size = 0;
		pixel_type = lvr_e_pixel_unknown;
	}
};

#endif
