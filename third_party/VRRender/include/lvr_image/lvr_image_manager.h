#ifndef __lvr_pixels_manager_h__
#define __lvr_pixels_manager_h__

#include "lvr_pixels.h"

class LVR_IMAGE_API lvr_image_manager
{
public:
	enum load_error
	{
		e_no_error,
		e_name_repeat,
		e_unknown_file,
		e_unknown_error
	};
public:
	lvr_image_manager();
	virtual ~lvr_image_manager();
public:
	load_error		load_pic_from_mem(const uint8_t* a_file_mem,const int8_t* a_pic_name,const char* extension,uint32_t a_buf_len,lvr_image_info& ao_ii);
	load_error		load_pic_from_path(const int8_t* a_file_path,const int8_t* a_pic_name,lvr_image_info& ao_ii);
	lvr_pixels		get_pixel_data(const int8_t* a_pic_name);
	bool			release_image(const int8_t* a_pic_name);

private:
	class impl;
	impl*	_this;
};

#endif
