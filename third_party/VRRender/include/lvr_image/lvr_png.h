#ifndef __lvr_png_h__
#define __lvr_png_h__

#include "lvr_image_configure.h"
#include "lvr_image.h"

class lvr_png:public lvr_image
{
public:
	lvr_png();
	~lvr_png();
public:
	bool load_from_mem(const int8_t* a_file_name,const uint8_t* a_buf,uint32_t a_buf_len,lvr_image_info& ao_ii);
	void release();
	const lvr_pixels& get_pixels();
private:
	lvr_pixels _pixel_info;
};

#endif
