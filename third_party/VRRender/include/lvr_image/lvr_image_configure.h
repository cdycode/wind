#ifndef __lvr_image_configure_h__
#define __lvr_image_configure_h__

#include "lvr_core_lib.h"

#ifdef lvr_image_EXPORTS
#define LVR_IMAGE_API __declspec(dllexport)
#elif defined lvr_image_IMPORT
#define LVR_IMAGE_API __declspec(dllimport)
#else
#define LVR_IMAGE_API
#endif


enum lvr_enum_pixel_type
{
	lvr_e_pixel_unknown = 0,
	lvr_e_pixel_rgb = 1,
	lvr_e_pixel_rgba = 2,
	lvr_e_pixel_a8 = 3,
	lvr_e_pixel_r32 = 4
};

#endif
