#ifndef __lvr_image_util_h__
#define __lvr_image_util_h__

#include "lvr_image_configure.h"

int32_t	LVR_IMAGE_API lvr_get_pixel_size(lvr_enum_pixel_type a_pixel_type);

#endif
