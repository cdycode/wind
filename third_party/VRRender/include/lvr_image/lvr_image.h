#ifndef __lvr_image_h__
#define __lvr_image_h__

#include "lvr_pixels.h"

class lvr_image
{
public:
	lvr_image(){}
	virtual ~lvr_image(){}
public:
	virtual void release() = 0;
	virtual const lvr_pixels& get_pixels() = 0;
};

#endif
