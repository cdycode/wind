#ifndef __lvr_ktx_h__
#define __lvr_ktx_h__

#include "lvr_image_configure.h"
/*

KTX Container Format

KTX is a format for storing textures for OpenGL and OpenGL ES applications.
It is distinguished by the simplicity of the loader required to instantiate
a GL texture object from the file contents.

Byte[12] identifier
uint32_t endianness
uint32_t glType
uint32_t glTypeSize
uint32_t glFormat
uint32_t glInternalFormat
uint32_t glBaseInternalFormat
uint32_t pixelWidth
uint32_t pixelHeight
uint32_t pixelDepth
uint32_t numberOfArrayElements
uint32_t numberOfFaces
uint32_t numberOfMipmapLevels
uint32_t bytesOfKeyValueData
  
for each keyValuePair that fits in bytesOfKeyValueData
    uint32_t   keyAndValueByteSize
    Byte     keyAndValue[keyAndValueByteSize]
    Byte     valuePadding[3 - ((keyAndValueByteSize + 3) % 4)]
end
  
for each mipmap_level in numberOfMipmapLevels*
    uint32_t imageSize;
    for each array_element in numberOfArrayElements*
       for each face in numberOfFaces
           for each z_slice in pixelDepth*
               for each row or row_of_blocks in pixelHeight*
                   for each pixel or block_of_pixels in pixelWidth
                       Byte data[format-specific-number-of-bytes]**
                   end
               end
           end
           Byte cubePadding[0-3]
       end
    end
    Byte mipPadding[3 - ((imageSize + 3) % 4)]
end

*/

#pragma pack(1)
struct lvr_ktx_header
{
	uint8_t	identifier[12];
	uint32_t	endianness;
	uint32_t	glType;
	uint32_t	glTypeSize;
	uint32_t	glFormat;
	uint32_t	glInternalFormat;
	uint32_t	glBaseInternalFormat;
	uint32_t	pixelWidth;
	uint32_t	pixelHeight;
	uint32_t	pixelDepth;
	uint32_t	numberOfArrayElements;
	uint32_t	numberOfFaces;
	uint32_t	numberOfMipmapLevels;
	uint32_t	bytesOfKeyValueData;
};
#pragma pack()

class lvr_ktx
{
public:
	lvr_ktx();
	~lvr_ktx();
public:
	static bool load_from_mem(const int8_t* a_file_name,const uint8_t* a_buffer,const int32_t a_buf_len,
		bool use_srgb,bool nomipmaps,lvr_image_info& ao_ii);
};

#endif
