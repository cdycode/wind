#ifndef __lvr_camera_h__
#define __lvr_camera_h__

#include "lvr_browser_configure.h"

class lvr_camera
{
public:
	enum type
	{
		ortho_normal,
		ortho_origin,
		prespective_normal,
		prespective_origin,
	};
	struct view_port
	{
		int _xOrigin;
		int _yOrigin;
		int _width;
		int _height;
	};
public:
	virtual ~lvr_camera(){}
public:
	virtual void set_pos(const lvr_vector3f& camera_pos_ ) = 0;
	virtual void set_dir(const lvr_vector3f& dir_) = 0;
	virtual void set_up(const lvr_vector3f& up_) = 0;
	virtual void set_heading(float heading_) = 0;
	virtual void set_tilt(float tilt_) = 0;
	virtual void set_roll(float roll_) = 0;
	virtual void set_aspect_ratio(float aspect_ratio) = 0;
	virtual void set_near(float near_) = 0;
	virtual void set_far(float far_) = 0;
	virtual void sync_changes() = 0;//tmp api,will emit later.

	virtual const lvr_vector3f& get_pos() const = 0;
	virtual const lvr_vector3f& get_dir() const = 0;
	virtual const lvr_vector3f& get_up() const = 0;
	virtual float    get_heading() const = 0;
	virtual float    get_tilt() const = 0;
	virtual float    get_roll() const = 0;
	virtual float	get_near() const = 0;
	virtual float    get_far() const = 0;

	virtual float    get_aspect_ratio() = 0;
	virtual float			get_view_dist() = 0;
	virtual float			get_field_view_angle() = 0;

	virtual lvr_vector3f get_current_forward_vec() = 0;
	virtual lvr_vector3f get_current_right_vec() = 0;
	virtual lvr_vector3f get_current_up_vec() = 0;

	virtual const lvr_frustumf& get_frustum() = 0;
	//when get view/proj matrix ,we will make a synchronization.
	virtual lvr_matrix4f get_view_matrix(bool column_major_ = true) = 0;
	virtual lvr_matrix4f get_proj_matrix(bool column_major_ = true) = 0;
	//this will set the pos,dir,up value.prepare for get_view_matrix use
	virtual void set_camera_style(const lvr_vector3f& eye,float heading_,float tilt_,float roll_) = 0;
	virtual void set_look_at(const lvr_vector3f& eye_,const lvr_vector3f& target_,const lvr_vector3f& up) = 0;
	virtual void set_perspective(float field_view_rad_,float aspect_ratio_,float near_,float far_) = 0;
	virtual void set_orthographic(float zoomx,float zoomy,float near_,float far_) = 0;
	//at most 64 chars.
	virtual type get_camera_type() = 0;
	virtual void get_camera_name(char* camera_name_) = 0;
	virtual void set_camera_name(char* camera_name_) = 0;
	virtual lvr_ray3f pick_ray(const view_port& view_port_,int x_,int y_) = 0;
	virtual lvr_vector3f project_point(const view_port& view_port_,const lvr_vector3f& pt_) = 0;
	virtual lvr_ray3f generate_pick_ray(float x_,float y_) = 0;

	virtual void unproject(float x_,float y_,float depth_,lvr_vector3f& dst_) = 0;
	virtual void project(const lvr_vector3f& pt_,lvr_vector3f& dst_) = 0;
};

#endif
