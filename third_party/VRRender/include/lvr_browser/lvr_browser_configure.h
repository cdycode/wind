#ifndef	__lvr_browser_configure_h__ 
#define __lvr_browser_configure_h__

#include "lvr_core_lib.h"
#include "lvr_math_lib.h"

//////////////////////////////////////////////////////////////////////////
#ifdef lvr_browser_EXPORTS
#define LVR_BROWSER_API LVR_API_EXPORT
#elif lvr_browser_IMPORT
#define LVR_BROWSER_API LVR_API_IMPORT
#else
#define LVR_BROWSER_API LVR_API_STATIC
#endif

#endif
