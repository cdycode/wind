#ifndef __lvr_browser_manager_h__
#define __lvr_browser_manager_h__

#include "lvr_camera.h"

class LVR_BROWSER_API lvr_browser_manager
{
public:
	~lvr_browser_manager();

public:
	static lvr_browser_manager* get_browser_manager();
	static lvr_camera*			create_camera(lvr_camera::type a_type);
public:
	void set_move_speed(float a_meters_per_second);
	float get_move_speed();
	void set_cur_camera(lvr_camera* camera_ptr_);
	//real time action
	bool move_dist(const lvr_vector3f& dist_vec_);
	bool move_forward_back(const float distance_);
	bool move_left_right(const float distance_);
	bool zoom_in_out(float height_change_);
	bool move_forward();
	bool move_back();
	bool move_left();
	bool move_right();
	bool rotate_axis_angle(const lvr_vector3f& axis,const float angle_rad);
	bool rotate_axis_angle_center(const lvr_vector3f& center_,const lvr_vector3f& axis,const float angle_rad_);
	//last time action
	bool auto_north(float deg_in_rad_per_second_);
private:
	void adjust_rotate(const lvr_matrix4f& rot_mat,const lvr_vector3f& a_axis,lvr_vector3f& a_dir,lvr_vector3f& a_up);
private:
	lvr_browser_manager();
public:
	lvr_camera*			_camera;
private:
	float               _time_now;
	float				_speed_move;
};

#endif
