#ifndef __lvr_camera_base_h__
#define __lvr_camera_base_h__

#include "lvr_browser_configure.h"
#include "lvr_camera.h"
class lvr_camera_base :public lvr_camera
{
public:
	lvr_camera_base();
	~lvr_camera_base();
public:
	void set_pos(const lvr_vector3f& camera_pos_ );
	void set_dir(const lvr_vector3f& dir_);
	void set_up(const lvr_vector3f& up_);
	void set_heading(float heading_);
	void set_tilt(float tilt_);
	void set_roll(float roll_);
	void set_aspect_ratio(float aspect_ratio);
	void set_near(float near_);
	void set_far(float far_);
	virtual void sync_changes();

	const lvr_vector3f& get_pos() const;
	const lvr_vector3f& get_dir() const;
	const lvr_vector3f& get_up() const;
	float		get_heading() const;
	float		get_tilt() const;
	float		get_roll() const;
	float		get_near() const;
	float		get_far() const;
	float		get_aspect_ratio();
	float			get_view_dist();
	float			get_field_view_angle();

	virtual lvr_vector3f get_current_forward_vec();
	virtual lvr_vector3f get_current_right_vec();
	virtual lvr_vector3f get_current_up_vec();

	const lvr_frustumf&  get_frustum();
	//when get view/proj matrix ,we will make a synchronization.
	lvr_matrix4f get_view_matrix(bool camepos_zero_ = true);
	lvr_matrix4f get_proj_matrix(bool column_major_ = true);
	//this will set the pos,dir,up value.prepare for get_view_matrix use
	void set_camera_style(const lvr_vector3f& eye,float heading_,float tilt_,float roll_);
	void set_look_at(const lvr_vector3f& eye_,const lvr_vector3f& target_,const lvr_vector3f& up);
	void set_perspective(float field_view_rad_,float aspect_ratio_,float near_,float far_);
	void set_orthographic(float zoomx,float zoomy,float near_,float far_);
	lvr_camera::type get_camera_type();
	//at most 64 chars.
	void get_camera_name(char* camera_name_);
	void set_camera_name(char* camera_name_);
	void set_camera_type(lvr_camera::type camera_type_);
	lvr_ray3f pick_ray(const lvr_camera::view_port& view_port_,int x_,int y_);
	lvr_vector3f project_point(const lvr_camera::view_port& view_port_,const lvr_vector3f& pt_);
	lvr_ray3f generate_pick_ray(float x_,float y_);
	//private:
	void unproject(float x_,float y_,float depth_,lvr_vector3f& dst_);
	void project(const lvr_vector3f& pt_,lvr_vector3f& dst_);
protected:
	lvr_vector3f  _pos;
	lvr_vector3f  _dir;
	lvr_vector3f  _up;
	float	  _tilt;
	float	  _heading;
	float	  _roll;
	lvr_frustumf   _frustum;
	float     _field_view;
	float     _fzoom[2];
	float     _aspect_ratio;
	float     _near_dist;
	float     _far_dist;
	lvr_camera::type _camera_type;
	char	_camera_name[64];
};

#endif
