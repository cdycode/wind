#ifndef __lvr_sensor_camera_h__
#define __lvr_sensor_camera_h__

#include "lvr_camera_base.h"

class lvr_sensor_camera : public lvr_camera_base
{
public:
	lvr_sensor_camera();
	~lvr_sensor_camera();
public:
	void set_orientation(const lvr_quaternionf& a_q);
	void return_all();
	void return_only_horizontal(float a_yaw_value = 0);
	void set_heading(float a_heading_angle);
	void set_adjust_tilt(float a_tiltx);
	virtual lvr_vector3f get_current_forward_vec();
	lvr_quaternionf  get_rotation();
private:
	lvr_quaternionf	_q_now;
	float _adjust_pitch;
	float _adjust_yaw;
	float _adjust_roll;
};

#endif
