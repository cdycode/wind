
#ifndef _LVR_CINEMA_SHOW_H_0314_
#define _LVR_CINEMA_SHOW_H_0314_

#include <functional>
#include <vector>
#include <map>
#include "lvr_gaze_cursor.h"
#include "lvr_ui_manager.h"
#include "lvr_ui_state.h"
#include "vr_view_driver.h"
#include "lvr_sensor_camera.h"
#include "lvr_scene_manager.h"
#include "lvr_cinema_render.h"

#include "lvr_misc_configure.h"
class lvr_line;
#ifdef LVR_OS_ANDROID
#include <jni.h>
#endif

#ifndef ForAiQiYi
#define pick_ray_origin_fix_pos
#endif


typedef int(*GetIntFuncPtr)(void);
typedef void(*SetIntFuncPtr)(int);
typedef void(*SetStrFuncPtr)(const char*, int);
typedef void(*VoidFuncPtr)(void);

class lvr_simple_mesh;
class NonLinearPanoMovie;
class LVR_MISC_API lvr_cinema_show {

public:
	lvr_cinema_show();
	virtual ~lvr_cinema_show();

private:

	lvr_texture*			 _cursor_texture;
	lvr_gaze_cursor*		_gaze_cursor_ptr;
	lvr_ui_manager			_ui_mgr;
	lvr_ui_state			_lus;
	lvr_camera*				_camera_ptr;
	vr_view_driver*			_view_driver;
	lvr_model_file*			_curr_scene;
	lvr_model_file*			_cinema_scene;

	lvr_cinema_render		_cinema_render;
	lvr_program*			_prog;
	lvr_media_interface*	_media_ptr;

	lvr_vector4uc			_fill_color;

	int						_screen_width;
	int						_screen_height;

	char    				_scene_path[128];
	char        			_phone_info[128];
	lvr_vector3f			_pick_origin;
	lvr_vector3f			_pick_dir;
	lvr_line*				_pick_line;
	lvr_camera*				_pick_camera;
	int						_pick_style;
	lvr_simple_mesh*		_handle_object;
	bool					_pick_ray_fix_dist;
	bool					_fix_camera_enabled;
	bool					_is_need_goback;
	bool					_handle_visible;
	bool					_pick_sphere_visible;
	lvr_camera*				_lock_camera;
	lvr_render_object*		_pick_sphere;
	lvr_vector3f			_pick_sphere_pos;
	float					_pick_sphere_size;
	NonLinearPanoMovie*		_non_linear_movie;
	bool					_is_czm_video;

	static int				_need_update_handle_visible;
	int						_current_handle_id;
	bool					_is_auto_trigger_ui_click;
private:
	void load_handle(int handle_id);
public:
	//must with opengl
	void set_handle_in_use(int handle_id);
	virtual void draw(int eye);
	virtual void init(const char* scene_path, const char* phone_info);
	void set_window_info(int w, int h);
	void update_camera(float x, float y, float z, float w);
	//called after camera update.
	void update_scene();
	bool on_keypressed(int key);
	void set_ui_check_style(int style);
	void set_handle_visible(int avisible);
	bool IsMediaPlaying();

	void set_media_scene_brightness(float a_s);//0.0-1.0;
	void fill_edge_color(int fbWidth, int fbHeight, const lvr_vector4uc& col, int pixels);
	void un_init();
	bool on_press_goback();
	void notify_android_network_status(int status);
	void set_user_info(int user_status, const char* user_name, const char* user_url, int user_id = 0);
	void add_local_file(const char* a_path);
	void set_local_usb_volume(const char* a_path);
	void remove_local_usb_volume();
	void SetAutoTriggerClick(bool is_auto);
#ifdef LVR_OS_WIN32
	void set_camera(lvr_camera* a_cam_ptr);
#endif
	void media_pause();
	void media_resume();
	void media_resume2();
	void SetHuashuMedia(const char* media);
	void update_camera_heading(float heading_);

	void SetPickRayOrigin(float x, float y, float z);
	void SetPickRay(float x, float y, float z, float w);
	void SetPickRay(const lvr_vector3f& a_ray_dir);
	void SetMediaControlUiVisible(int a_visible);
	bool SetCzmFile(const char* a_path);
	static bool start_czm_video(const char* a_path, void* obj);
	void sync_czm_video_impl(int sync_pos);
	static void sync_czm_video(int sync_pos, void* obj);

	enum method{
		exitApp = 0,
		getCurrentVolume = 1,
		getMaxVolume ,
		setVolume ,
		getCurrentBrightness ,
		getMaxBrightness,
		setBrightness ,
		setHandlerVisible = 100,
	};

	enum huashu_method{
		huashuLoad = 1,
		huashuPause ,
		huashuResume ,
		huashuUnload,
	};
#ifdef LVR_OS_ANDROID
public:
	void init_android_callback(JNIEnv * a_jni_ptr, const jclass& clazz );
private:
	
	static JNIEnv*		_jni_env;
	static jclass		_native_class;
	static jmethodID 	_get_current_volume_method_id;
	static jmethodID	_get_max_volume_method_id;
	static jmethodID	_set_volume_method_id;
	static jmethodID	_get_current_Brightness_method_id;
	static jmethodID	_get_max_Brightness_method_id;
	static jmethodID	_set_Brightness_method_id;
	static jmethodID	_exit_app_method_id;

	static jmethodID	_huashu_media_load_method_id;	
	static jmethodID	_huashu_media_pause_method_id;
	static jmethodID	_huashu_media_resume_method_id;
	static jmethodID	_huashu_media_unload_method_id;
#else


	static GetIntFuncPtr _get_max_volume_cb;
	static GetIntFuncPtr _get_curr_volume_cb;
	static SetIntFuncPtr _set_volume_cb;
	static GetIntFuncPtr _get_max_brightness_cb;
	static GetIntFuncPtr _get_curr_brightness_cb;
	static SetIntFuncPtr _set_brightness_cb;

	static SetStrFuncPtr _load_huashu_media_cb;
	static VoidFuncPtr	  _pause_huashu_media_cb;
	static VoidFuncPtr	  _resume_huashu_media_cb;
	static VoidFuncPtr	  _unload_huashu_media_cb;

	void set_volume_setting_cb(GetIntFuncPtr max_cb, GetIntFuncPtr curr_cb, SetIntFuncPtr set_cb);
	void set_brightness_setting_cb(GetIntFuncPtr max_cb, GetIntFuncPtr curr_cb, SetIntFuncPtr set_cb);
	void set_huashu_cb(SetStrFuncPtr load_cb, VoidFuncPtr pause_cb, VoidFuncPtr resume_cb, VoidFuncPtr unload_cb);

#endif

	static bool callmethod1(int id, int& value);
	static bool callhuashumethod(int id, int& value, const std::string& str);

};


#endif //DEMO1_LVR_WORD_MINE_H
