#ifndef __NonLinearMovie_H__20160926__
#define __NonLinearMovie_H__20160926__

#include "lvr_misc_configure.h"
#include "lvr_sensor_camera.h"
#include "lvr_media_interface.h"
#include "lvr_ui_manager.h"
#include <map>

#include "lvr_nonlinear_movie_controller.h"
#include "lvr_ui_state.h"

class LVR_MISC_API NonLinearPanoMovie
{
public:
	NonLinearPanoMovie();
	~NonLinearPanoMovie();
public:
	void init(lvr_camera* a_cam_ptr,lvr_ui_manager* a_ui_mgr);
	void reset();
	void uninit();
	void set_media(lvr_media_interface* a_media);
	bool set_czm( const char* a_media_czm_file);
	int  update(double a_time);
	void draw(const lvr_matrix4f& a_view_mat, const lvr_matrix4f& a_proj_mat, int eye);
	void sync_media_control(int pos);
	void set_app_handle_cb(bool(*func)(const std::string&args, void*), void* obj);

private:
	float					_last_op_time;
	float					_ui_radius;
	lvr_ui_manager*			_ui_mgr;
	lvr_texture*			_focus_tex;
	lvr_camera*				_camera_ptr;
	lvr_media_interface*	_media_ptr;
	lvr_nonlinear_movie_controller*	_nmc;
	bool					_activate_state;
	int						_sync_media_control_delay;
};

#endif
