/*
* lvr_headers.h
*
*  Created on: 2015-10-13
*      Author: Administrator
*/

#ifndef __lvr_misc_configure_h__20161107__
#define __lvr_misc_configure_h__20161107__

#include "lvr_log.h"
#include "lvr_time.h"

#ifdef lvr_misc_EXPORTS
#define LVR_MISC_API __declspec(dllexport)
#elif defined lvr_misc_IMPORT
#define LVR_MISC_API __declspec(dllimport)
#else
#define LVR_MISC_API
#endif

#include "lvr_render_lib.h"
#include "lvr_bitmap_font_manager.h"
#include "lvr_model_file.h"
#include "lvr_package_files.h"
#include "lvr_ui_manager.h"
#include "lvr_quaternion.h"
#include "lvr_render_lib.h"

#endif /* __LINGVR_RENDER_LVR_HEADERS_H_ */
