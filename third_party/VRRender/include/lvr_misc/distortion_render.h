#ifndef __distortion_render_h__20160924__
#define __distortion_render_h__20160924__
#include "lvr_misc_configure.h"
#include "lvr_render_object.h"
#include "lvr_program.h"
typedef struct
{
	float	_lens_separation;//in meters
	float	_eye_texture_fov;
	float	_screen_width_meters;//in meters
	float	_screen_height_meters;//in meters
	int		_screen_width_pixels;//in pixels
	int		_screen_height_pixels;//in pixels
}czvr_hmd_info;

typedef struct 
{
public:
	enum e_distortion_method
	{
		e_distortion_method_poly4,
		e_distortion_method_recip_poly4,
		e_distortion_method_catmullrom10,
		e_distortion_method_catmullrom20,
		e_distortion_method_catmullrom,
	};

public:
	e_distortion_method	_distortion_method;
	float				_k[21];
	float				_kr[21];
	float				_kb[21];
	float				_meters_per_tanangle_at_center;
	float				_chromatic_aberration[4];
}czvr_distortion_parms;

typedef struct {
	float*	_red_data;
	float*	_green_data;
	float*	_blue_data;
	int		_mesh_grid_num_per_eye;//pow of 2,like 16 32 and so on ..
	int		_vertex_num;
}czvr_distortion_mesh;

class LVR_MISC_API distortion_render
{
public:
	distortion_render();
	~distortion_render();
public:
	void set_distortion_parms(const czvr_distortion_parms& a_distortion_parms);
	void set_reflect_need_flag(bool need_reflect);
	void set_hmd_info(const czvr_hmd_info& a_hmd_info);
	void set_aberration_flag(bool a_ab);
	void update_mesh();
	void init();
	void uninit();
	void warp_eye(unsigned int aTex,int eye);
public:
	void generate_warp_program();
	float eval_catmullrom_spline(float* a_k, float a_scaled_sq, int a_num_segs);
	lvr_vector3f get_scaled_chroma(float a_rsq);
	void warp_texcoord_chroma(const float a_in[2], float ao_r[2], float ao_g[2], float ao_b[2]);
	void generate_mesh(int a_mesh_grid_num_per_eye);
	void release_distortion_mesh();
	void generate_warp_mesh();
	void bind_warp_mesh();
	void unbind_warp_mesh();
public:
	lvr_render_object*	_ro;
	int					_total_indices_num;
private:
	czvr_distortion_parms	_distortion_parms;
	czvr_hmd_info			_hmd_info;
	czvr_distortion_mesh	_distortion_mesh;
	lvr_program*			_simple_warp_prog;
	lvr_program*			_ab_warp_prog;

	unsigned int			_warp_mesh_vbo;
	unsigned int			_warp_mesh_ibo;
	unsigned int			_half_index_count;

	bool					_ab;
	bool					_need_update_warp_mesh;
	bool					_need_flip_u;
};



#endif
