#ifndef __lvr_cube_picture_view_h__20170609__
#define __lvr_cube_picture_view_h__20170609__
#include "lvr_misc_configure.h"
#include "lvr_camera.h"
class LVR_MISC_API lvr_cube_picture_view
{
public:
	lvr_cube_picture_view();
	~lvr_cube_picture_view();
public:
	void init(lvr_camera* a_camera, const char*a_extra_path);
	void change_picture(const char* a_texture_path,int format);
	void uninit();
	void SetWindowInfo(int w, int h);
	void Rotate(float);
	void Tilt(float);
	void update();
	void draw();
private:
	void update_camera(float x, float y, float z, float w);
private:
	lvr_camera*	_camera_ptr;
	lvr_program*		_single_tex_prog;
	lvr_texture*		_texture;
	lvr_render_object*	_cube_sphere;
	lvr_vector4f		_tex_mat;
	char				_texture_path[1024];
	bool				_need_change_texture;
};

#endif