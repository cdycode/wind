#ifndef __movie_app_h__20160922__
#define __movie_app_h__20160922__

#include "lvr_misc_configure.h"
#include "lvr_cinema_render.h"
#include "lvr_media_interface.h"
#include "lvr_camera.h"

#include "distortion_render.h"
#include "NonLinearPanoMovie.h"
#include "lvr_gaze_cursor.h"
#include "lvr_model_file.h"
#include "lvr_subtitle.h"

#define _USE_CONTROLLER_V2_
#include <functional>

class lvr_distortion_render_ar_light_field;
class lvr_screen_sphere;
class LVR_MISC_API czvr_movie_app
{
private:
	enum State
	{
		E_UnInited,
		E_Inited,
		E_Initing,
		E_Exiting,
		E_NeedExit
	};
public:
	czvr_movie_app();
	~czvr_movie_app();
public:
#ifdef LVR_OS_WIN32
	void set_camera(lvr_camera* a_cam_ptr);
#endif
	bool Init();
	void UnInit();
	void SetPlayMode(int mode);
	void SetUiVisible(bool a_visible);
	void SetCursorVisible(bool a_cursor_visible);
	bool Play(const char* url, const char* a_subtitle_file, int ms_pos);
	bool PlayNext(const char* url, const char* a_subtitle_file, int ms_pos);
	bool StopMedia();
	void SetRotateAngle(float a_angle);
	void SetAdjustValue(int a, int b, int c, int d,int flaga,int flagb,int flagc,int flagd);
	void AdjustTilt(float a_angle);
	void SetCheckState(int a_state);
	void SetMovieSrc(bool a_isLocal);
	void SetMovieDpi(int a_res_id);
	void SetMovieName(const char* a_name);
	void SetMovieDpiTypes(int a_code);
	bool SetScene(const char* a_scene_path, bool a_from_package);
	void Display();
	void SetScreenSize(int w, int h);
	void SetDistortionMethod(int id);
	void SetWindowInfo(float* parms,float wm,float hm,float eye_len,float z,int w,int h);
	void SetVideoInfo(int w,int h);
	void SetAxisZ(float z);
	void SetViewPortScale(float zoom_scale);
	void SetDistortionFlag(bool flag);
	void SetFillColor(const lvr_vector4uc& a_color );
	void FreezeCamera(bool a_use_default_style);
	void CameraReturn();
	void Close();
	bool SetCzmFile(const char* a_path);
	void sync_czm_media(int sync_pos);
	void				SetAdjustAngle(float a, float b, float c);
	void set_check_state_view_angle_deg(float a_view_degree);
	lvr_media_interface* GetMedia();
	lvr_camera*			 GetCamera();
	lvr_ui_manager*		 GetUiMgr();
    lvr_subtitle*        get_subtitle();
private:
	void draw_eye(int eye);
	void warp_eyes();
	void sync_render_object();
	void sync_sensor();
	void sync_media();
	bool get_movie_uv_mat(lvr_movie_render::movie_format mode, lvr_vector4f& tex_mat0,lvr_vector4f& tex_mat1);
	void fill_edge_color(int fbWidth, int fbHeight, const lvr_vector4uc& col,int pixels);
	void update_viewports();
private:
	lvr_cinema_render		_cinema_render;
	lvr_render_object		_direct_movie_screen;
	lvr_render_object		_light_field_ro;
	distortion_render		_distortion_render;
	lvr_distortion_render_ar_light_field*	_light_field_distortion;
	int						_distortion_method_id;
	lvr_media_interface*	_media_ptr;
	lvr_camera*				_camera_ptr;
	lvr_model_file*			_cinema_scene_ptr;
	lvr_vector4i			_viewport[2];
	int						_half_screen_width;
	int						_screen_height;
	int						_video_width;
	int						_video_height;
	lvr_vertex_format*		_format;
	lvr_program*			_direct_movie_program;
	lvr_render_texture		_rt[2];
	lvr_vector4f			_texmat[2];
	lvr_vector4uc			_fill_color;
	NonLinearPanoMovie*		_non_linear_movie;
	lvr_subtitle*			_subtitle;
	lvr_gaze_cursor*		_gaze_cursor_ptr;
	lvr_texture*			_cursor_texture;
	lvr_ui_manager			_ui_mgr;
	lvr_ui_state			_lus;
	int						_cur_movie_play_time;
	int						_cur_movie_total_time;
	double					_cur_time;
	float					_camera_z;
	float					_viewport_scale;
	float					_check_view_angle_rad;
	int						_movie_mode;
	bool					_vr_case_enable;
	bool					_vr_distortion_require;
	bool					_need_update_freen_screen_render_object;
	bool					_fix_camera_enabled;
	bool					_change_scrren_size;
	bool					_show_cursor;
	bool					_ui_visible;
	bool					_cursor_visible;
	bool					_is_local_movie;
	bool					_is_use_free_screen;
	bool					_update_rt_need;
	bool					_fake_distortion;//distortion flag is always true,but when parms are all 1.0,take it as no distortion mode
	bool					_check_state;
	State					_AppState;
	bool					_is_czm_video;
	float					_interaction_video_change_circle_redius;
	int						_test_id[4];
	int						_flag[4];
	lvr_vector3f			_adjust_rot_angle;
	float					_eye_distantce;

	enum czm_change_status{
		ccs_normal,
		ccs_hide,
		ccs_cache_show,
		ccs_show,
	};
	czm_change_status		_change_interaction_video;			// 0:normal  1:show   -1:hide
	double					_change_interaction_video_begin_time;
	lvr_screen_sphere*		_circle_sphere_ptr;
	std::string				_interaction_video_args;

// media controller v2 start
private:
	struct ui_download_img_task{
		int dlid;
		std::function<void(bool success, const std::string&)> callback;
	};

	std::vector<ui_download_img_task>	_ui_img_dl_task;
	std::string				_resource_path;
	std::string				_repeat_play_url;
	std::string				_repeat_subtitle_url;
	std::vector<std::string>	_vec_process_event1;
	std::vector<std::string>	_vec_process_event2;
	int							_which_of_operate_process_event;

public:
	void add_process_event(const std::string& args);
	bool process_event();
	void process_media_event_impl(const std::string& args);
	bool SetMediaMoreInfo(const char* data, int tag);
	static bool process_media_event(const std::string& args, void *obj);
	static void media_voice_and_brightness(const std::string&cmd, void *obj, int& inoutdata);
	static void media_ui_download_img_func(void *obj, const std::string& url, std::function<void(bool success, const std::string&)> &callback);
	void on_media_ui_img_download_over(bool success, int taskid);
	void set_resource_path(const char* path);
// media controller v2 end

};

#endif

