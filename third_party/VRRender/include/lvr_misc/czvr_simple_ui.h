#ifndef __czvr_simple_ui_h__
#define __czvr_simple_ui_h__

#include "lvr_misc_configure.h"
#include "lvr_ui_menu.h"
#include<map>

class czvr_movie_app;
class LVR_MISC_API czvr_simple_ui
{
public:
	enum ui_rect_name
	{
		E_Rect_PausePlay = 0,
		E_Rect_Prev,
		E_Rect_Next,
		E_Rect_ShowHide,
		E_Rect_Return,
		E_Rect_Progress_background,
		E_Rect_Progress_passed,
		E_Rect_Progress_cached,
		E_Rect_Main_background,
		E_Rect_MovieType_background,
		E_Rect_DpiChoose_background,
		E_Rect_Count
	};
	enum ui_text_name
	{
		E_Text_360,
		E_Text_360LR,
		E_Text_360RL,
		E_Text_360TD,
		E_Text_360DT,
		E_Text_180,
		E_Text_180LR,
		E_Text_180RL,
		E_Text_180TD,
		E_Text_180DT,
		E_Text_Info,
		E_Text_MovieName,
		E_Text_Return,
		E_Text_CurPos,
		E_Text_Seek,
		E_Text_Total,
		E_Text_1K,
		E_Text_2K,
		E_Text_4K,
		E_Text_DpiTitle,
		E_Text_Count
	};
	enum movie_dpi
	{
		E_1K,
		E_2K,
		E_4K,
		E_dpi_Count
	};
private:
	enum menu_state
	{
		E_Menu_Hide_Show,
		E_Menu_Main,
		E_Menu_Dpi,
		E_Menu_MovieType,
		E_Menu_Count,
		E_Menu_None,
	};
public:
	~czvr_simple_ui();
public:
	static czvr_simple_ui* get_simple_ui();
	static void release();
private:
	czvr_simple_ui();
	static czvr_simple_ui* s_simple_ui;
public:
	bool  init(czvr_movie_app* app);
	void  uninit();
	void update(const lvr_vector3f& a_pos, const lvr_vector3f& a_look, const lvr_vector3f& a_right, double a_time,int a_movie_time);
	void  set_movie_src(bool a_is_local);
	void  set_seek_time(int a_ms);
	void  set_current_time(int a_ms);
	void  set_total_time(int a_ms);
	void  set_movie_mode(int a_mode);
	void  set_movie_name(const char* a_movie_name);
	void  set_dpi_types(int a_id);
	void  set_movie_dpi(movie_dpi a_dpi_mode);
	void  set_visible(bool a_visible);
	void on_ui_click(lvr_ui_base* a_ui);
	void on_ui_focus(lvr_ui_base* a_ui);
	void on_ui_leave(lvr_ui_base* a_ui);
	//make sure you give a unit vector for camera right outside
	void set_ui_pos(const lvr_vector3f& a_camera_pos, const lvr_vector3f& a_camera_right,const lvr_vector3f&a_up);
private:
	void update_ui_state(menu_state a_ui_state);
	void set_play_state(bool a_play_pause);
	void update_seek_time();
	void update_ui_pos(const lvr_vector3f& a_camera_pos);
	void update_show_hide_ui_pos(const lvr_vector3f& a_pos, const lvr_vector3f& a_right, const lvr_vector3f& a_up);
	inline void generate_uv_info(int a_id, lvr_ui_2d::tex_info& ao_ti);
	inline void int_to_time_text(char* a_time_text,int a_ms);
private:
	lvr_vector3f	_pos;
	lvr_vector3f	_right, _up;
	lvr_ui_text*	_texts[E_Text_Count];
	lvr_ui_2d*		_rects[E_Rect_Count];
	lvr_rect2f		_rect_uv[(E_Rect_Return+3)*2];//E_Rect_Return+1 is showing rect num the final couple is for pause/play uv change.show/hide
	lvr_ui_menu		_menus[E_Menu_Count];
	czvr_movie_app*	_app;
	lvr_vector3f	_seek_region[4];//only useful for seek time get.
	lvr_vector3f	_mv_type_region[4];//total movie type region for test to see if we need to close mv type choose
	lvr_vector3f	_main_region[4];//main ui region,to judge if we need to hide ui,currently we use down to show ui.
	lvr_vector3f	_cur_mv_type_region[4];//curmovieTypeText region
	//the pos here are all center of ui. do remember.
	lvr_vector2f	_rect_pos[E_Rect_Count];
	lvr_vector2f	_text_pos[E_Text_Count];
	lvr_vector2f	_rect_size[E_Rect_Count];
	lvr_vector2f	_ui_progress_bar_pos;
	lvr_vector2f	_ui_cur_mov_pos;
	lvr_vector2f	_ui_cur_mov_size;
	lvr_vector2f	_movie_type_size;
	lvr_vector2f	_ui_movie_type_choose_pos;
	//lvr_vector2f	_rect_size[E_Rect_MovieType_background];
	lvr_vector2f	_progress_bar_pos;
	//lvr_vector2f	_rect_size[E_Rect_Progress_background];
	lvr_vector2f	_progress_bar_left_center_pos;
	char			_movie_type_names[E_Text_Info][12];
	char			_movie_dpi_names[3][8];
	int				_movie_mode;
	movie_dpi		_movie_dpi_mode;
	menu_state		_cur_ui_state;
	char			_time_text[3][10];
	int				_total_time;
	float			_current_hit_pos;
	float			_current_play_pos;
	char			_movie_name[50];
	bool			_show_seek_time;
	bool			_need_update_pos;
	bool			_main_ui_update;
	bool			_playing;
	bool			_is_local_movie;
	bool			_visible;
	bool			_need_update_movie_name;
};


#endif
