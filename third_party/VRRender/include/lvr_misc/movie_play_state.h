#ifndef __movie_play_state_20161010_h__
#define __movie_play_state_20161010_h__

#include "lvr_misc_configure.h"
#ifdef LVR_OS_ANDROID
#include <jni.h>

class movie_play_state
{
public:
	~movie_play_state();
	static movie_play_state* get_movie_play_state();
private:
	movie_play_state();
	static movie_play_state* s_mps;
public:
	void init(JNIEnv * a_jni_ptr);
	void uninit();
	void on_waiting_for_data();
	void on_cache_finish_continue();
	void on_finish(int a_vaule);
	void on_play_next(int a_forward);
	void on_change_def(int a_def);

	void on_video_repeat(int a);
	void on_new_play(const std::string& str);
	void on_new_play_by_args(const std::string& args);
	bool operate_voice_and_brightness(int id, void *obj, int& inoutdata);

private:
	JNIEnv*		_jni_env;
	static jclass	_czvr_native_player_class;
	static jmethodID 	_on_waiting_for_cache_method_id;
	static jmethodID	_on_finish_movie_method_id;
	static jmethodID	_on_chach_finish_continue_method_id;
	static jmethodID	_on_play_next_method_id;
	static jmethodID	_on_change_def_method_id;


	enum method{
		exitApp = 0,
		getCurrentVolume = 1,
		getMaxVolume ,
		setVolume ,
		getCurrentBrightness ,
		getMaxBrightness,
		setBrightness ,
	};
	static jmethodID	_on_new_play_method_id;
	static jmethodID	_on_new_play_by_args_method_id;
	static jmethodID 	_get_current_volume_method_id;
	static jmethodID	_get_max_volume_method_id;
	static jmethodID	_set_volume_method_id;
	static jmethodID	_get_current_Brightness_method_id;
	static jmethodID	_get_max_Brightness_method_id;
	static jmethodID	_set_Brightness_method_id;
	static jmethodID	_on_video_repeat_method_id;

};
#else

typedef void(*MPSVoidFuncPtr)(void);
typedef void(*MPSIntFuncPtr)(int);

typedef int(*MPSGetIntFuncPtr)(void);
typedef void(*MPSSetIntFuncPtr)(int);
typedef void(*MPSSetStrFuncPtr)(const char*);


class movie_play_state
{
public:
	~movie_play_state();
	static movie_play_state* get_movie_play_state();
private:
	movie_play_state();
	static movie_play_state* s_mps;
public:
	void set_waiting_for_data_callback(MPSVoidFuncPtr afunc);
	void set_cache_finish_continue(MPSVoidFuncPtr afunc);
	void set_finih_cb(MPSIntFuncPtr a_func);
	void set_play_next_cb(MPSIntFuncPtr a_func);
	void set_change_def_cb(MPSIntFuncPtr a_func);
public:
	void init();
	void uninit();
	void on_waiting_for_data();
	void on_cache_finish_continue();
	void on_finish(int a_vaule);
	void on_play_next(int a_forward);
	void on_change_def(int a_def);

	void on_video_repeat(int a);
	void on_new_play(const std::string& str);
	void on_new_play_by_args(const std::string& args);
	void set_new_play_cb(MPSSetStrFuncPtr set_cb);
	void set_new_play_by_args_cb(MPSSetStrFuncPtr set_cb);
	void set_volume_setting_cb(MPSGetIntFuncPtr max_cb, MPSGetIntFuncPtr curr_cb, MPSSetIntFuncPtr set_cb);
	void set_brightness_setting_cb(MPSGetIntFuncPtr max_cb, MPSGetIntFuncPtr curr_cb, MPSSetIntFuncPtr set_cb);
	bool operate_voice_and_brightness(int id, void *obj, int& inoutdata);
	void set_video_repeat_cb(MPSSetIntFuncPtr set_cb);

private:
	MPSVoidFuncPtr	_waiting_for_data_cb;
	MPSVoidFuncPtr  _cache_finish_continue_cb;
	MPSIntFuncPtr   _finish_cb;
	MPSIntFuncPtr   _play_next_cb;
	MPSIntFuncPtr   _change_def_cb;


	enum method{
		getCurrentVolume = 1,
		getMaxVolume,
		setVolume,
		getCurrentBrightness,
		getMaxBrightness,
		setBrightness,
	};
	MPSGetIntFuncPtr _get_max_volume_cb;
	MPSGetIntFuncPtr _get_curr_volume_cb;
	MPSSetIntFuncPtr _set_volume_cb;
	MPSGetIntFuncPtr _get_max_brightness_cb;
	MPSGetIntFuncPtr _get_curr_brightness_cb;
	MPSSetIntFuncPtr _set_brightness_cb;
	MPSSetStrFuncPtr _on_new_play_cb;
	MPSSetStrFuncPtr _on_new_play_by_args_cb;
	MPSSetIntFuncPtr _on_video_repeat_cb;


};
#endif

#endif
