//
// Created by sunpengfei on 16/5/26.
//

#ifndef _LVR_CINEMA_APP_VR_H
#define _LVR_CINEMA_APP_VR_H

#include <functional>
#include <vector>
#include <map>
#include "distortion_render.h"

#include "lvr_misc_configure.h"
#include "lvr_cinema_show.h"

class lvr_distortion_render_ar_light_field;
class LVR_MISC_API lvr_cinema_app {

public:
	lvr_cinema_app();
	virtual ~lvr_cinema_app();

private:
	distortion_render		_distortion_render;
	lvr_distortion_render_ar_light_field*	_light_field_distortion;
	int						_distortion_method_id;
	float					_eye_distantce;
	lvr_render_texture		_rt[2];
	bool 					_update_rt_need;
	lvr_vector4uc			_fill_color;

	int						_half_screen_width;
	int						_screen_height;
	lvr_vector4i			_viewport[2];
	bool					_vr_case_enable;
	bool					_vr_distortion_require;

    char    				_scene_path[128];
    char        			_phone_info[128];
	int						_framebuffer_id;
	lvr_cinema_show*		_cinema_show;

public:
	virtual void on_display();
	virtual void on_init();
	void SetWindowInfo(float* parms, float wm, float hm, float eye_len, float z, int w, int h);
	void SetSceneLightBrightness(float a_s);//0.0-1.0;
	void update_viewports();
	void fill_edge_color(int fbWidth, int fbHeight, const lvr_vector4uc& col, int pixels);
	void warp_eyes();
	void UnInit();
	void set_phone_info(const char* phoneinfo);
	void set_scene_path(const char* scenepath);
	bool on_press_android_goback();
	void notify_android_network_status(int status);
	void set_user_info(int user_status, const char* user_name, const char* user_url,int user_id=0);
	void SetLocalFileString(const char* a_path);
#ifdef LVR_OS_WIN32
	void set_camera(lvr_camera* a_cam_ptr);
#endif
    void MediaPause();
	void MediaResume();
	void MediaResume2();
	void on_keypressed(int key);
	void set_handle_id(int id);

	void set_ui_check_style(int style);
	void set_handle_visible(int avisible);
	void SetPickRayOrigin(float x, float y, float z);
	void SetPickRay(float x, float y, float z, float w);

	void SetHuashuMedia(const char* media);

#ifdef LVR_OS_ANDROID
public:
	void init_android_callback(JNIEnv * a_jni_ptr, const jclass& clazz);

#else
public:
	void set_volume_setting_cb(GetIntFuncPtr max_cb, GetIntFuncPtr curr_cb, SetIntFuncPtr set_cb);
	void set_brightness_setting_cb(GetIntFuncPtr max_cb, GetIntFuncPtr curr_cb, SetIntFuncPtr set_cb);
	void set_huashu_cb(SetStrFuncPtr load_cb, VoidFuncPtr pause_cb, VoidFuncPtr resume_cb, VoidFuncPtr unload_cb);
#endif

private:
	void draw_eye(int eye);

};


#endif //DEMO1_LVR_WORD_MINE_H
