#ifndef __lvr_bitmap_font_manager_h__
#define __lvr_bitmap_font_manager_h__

#include "lvr_bitmap_font_configure.h"

//currently we do not support change real time.
#define LVR_FONT_SIZE 32
#define  LVR_FONT_METERS_PER_PIXEL (1.0f/512.0f)
class lvr_bitmap_font_manager
{
public:
	virtual ~lvr_bitmap_font_manager(){}
public:
	//font_path if in package ,relative path,otherwise, absolute path
	virtual bool	init(const char* a_font_path,bool a_from_package) = 0;
	virtual void set_defalut_align_method(lvr_enum_font_align_method a_align_method) = 0;
	virtual bool	is_initiated() = 0;
	virtual void	release() = 0;
	//a_scale is the scale value under  1meter = 512pixels
	//use this result mult with your scale info,then you get the real x leave of this word.
	virtual float get_advance_x(uint32_t char_code) = 0;
	virtual float get_vertical_space() = 0;//same metric as advance_x;when use,multi with 1.125 will be the same as we use inside.
	virtual int add_render_string(const char* a_str, float a_scale, float a_max_width, const lvr_vector3f& a_pos, const lvr_vector3f& a_right, const lvr_vector3f& a_up, float a_line_spacing, float a_font_spacing) = 0;
	virtual int add_render_string(const char* a_str, float a_scale, const lvr_vector3f& a_pos, const lvr_vector3f& a_right, const lvr_vector3f& a_up, float a_line_spacing, float a_font_spacing) = 0;
	virtual void set_render_string_color(int a_id,lvr_vector4uc a_color) = 0;
	virtual void set_render_string_size(int a_id,float a_sclae) = 0;
	virtual void set_warp_width(int a_id, float a_width) = 0;
	virtual void set_render_string_algin(int a_id,lvr_enum_font_align_method a_align_method) = 0;
	virtual void set_render_string_position(int a_id,lvr_vector3f a_position) = 0;
	virtual void query_render_string_size(int a_id,float& ao_width,float& ao_heigh) = 0;
	virtual void update_render_string(int a_id,float a_scale,const lvr_vector3f& a_pos,const lvr_vector3f& a_right,const lvr_vector3f& a_up) = 0;
	virtual void remove_render_string(int a_id) = 0;
	virtual void submit_for_render(int a_id) = 0;
	//when submit all the things you want to render,call update
	//only call update if you have change words' info,if not do not call it.
	//also if you have call submit_for_render different from last frame,call it ,if they are the same,do not call.
	virtual void update() = 0;
	//call draw only once per frame.
	virtual void draw(const lvr_matrix4f& a_view_proj_mat,lvr_program* prog2) = 0;
};
;
LVR_BITMAP_FONT_API lvr_bitmap_font_manager* lvr_get_bitmap_font_manager();
LVR_BITMAP_FONT_API void lvr_release_bitmap_font_manager();

#endif
