#ifndef __lvr_bitmap_font_configure_h__
#define __lvr_bitmap_font_configure_h__

#include "lvr_core_lib.h"
#include "lvr_math_lib.h"
// #ifdef LVR_OS_WIN32
// #define lvr_render_IMPORT
// #endif
#include "lvr_render_lib.h"

#ifdef lvr_bitmap_font_EXPORTS
#define LVR_BITMAP_FONT_API __declspec(dllexport)
#elif defined lvr_bitmap_font_IMPORT
#define LVR_BITMAP_FONT_API __declspec(dllimport)
#else
#define LVR_BITMAP_FONT_API
#endif

// #ifdef LVR_OS_WIN32
// #define lvr_font_IMPORT
// #endif
#include "lvr_font_lib.h"

enum lvr_enum_font_align_method
{
	lvr_e_font_align_left,
	lvr_e_font_align_middle,
	lvr_e_font_align_right
};

#endif
