#ifndef __lvr_bitmap_font_render_object_h__
#define __lvr_bitmap_font_render_object_h__

#include "lvr_bitmap_font_configure.h"

class lvr_bitmap_font_render_object
{
public:
	lvr_bitmap_font_render_object();
	~lvr_bitmap_font_render_object();
public:
	int				_word_num;
	lvr_vector3f	_postion;
	lvr_vector3f	_right;
	lvr_vector3f	_up;
	float			_scale_value;
	float			_warp_width;
	float           _line_spacing;
	float           _font_spacing;
	lvr_vector4uc	_color;
	std::vector<uint16_t>	_string_codes;
	lvr_enum_font_align_method _align_method;
	float			_total_cross_value;
	int				_lines;
	float			_last_line_width;

};

#endif
