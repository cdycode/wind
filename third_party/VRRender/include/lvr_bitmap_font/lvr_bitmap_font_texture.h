#ifndef __lvr_bitmap_font_texture_h__
#define __lvr_bitmap_font_texture_h__

#include "lvr_bitmap_font_configure.h"
#include "lvr_texture2d.h"

class space_control;
class lvr_font_glyph_info
{
public:
	lvr_font_glyph_info()
		:char_code(0),
		x(0.0f),
		y(0.0f),
		width(0.0f),
		height(0.0f),
		advance_x(0.0f),
		advance_y(0.0f),
		bearing_x(0.0f),
		bearing_y(0.0f)
	{}
public:
	int32_t		char_code;
	float		x;
	float		y;
	float		width;
	float		height;
	float		advance_x;
	float		advance_y;
	float		bearing_x;
	float		bearing_y;
};

class lvr_bitmap_font_texture
{
public:
	lvr_bitmap_font_texture();
	~lvr_bitmap_font_texture();
public:
	void init(int a_w,int a_h,int a_char_size);
	void bind(int );
	lvr_font_glyph_info*	get_font_glyph_info(int32_t a_char_code);
	lvr_font_glyph_info*	put_glyph(lvr_font_glyph* a_font_glyph);
	void release();
public:
	std::map<int32_t,lvr_font_glyph_info*>  _page;
	unsigned int	_tex_id;
	int				_width,_height;
	int				_char_size;
	space_control*	_sc;
	float			_half_word_advance_x;
};

#endif
