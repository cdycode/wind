//
// Created by sunpengfei on 16/6/30.
//

#ifndef CINEMAVR_JSONHELPER_H
#define CINEMAVR_JSONHELPER_H

#include "lvr_json.h"
#include <string>
using namespace std;
class JSONHelper :public lvr_json{

public:
    JSONHelper(JSONItemType type);
    string toString();
};


#endif //CINEMAVR_JSONHELPER_H
