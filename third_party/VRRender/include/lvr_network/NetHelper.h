//
// Created by sunpengfei on 16/6/30.
//

#ifndef CINEMAVR_NETHELPER_H
#define CINEMAVR_NETHELPER_H

#include "lvr_singleton.h"
#include "HttpClient.h"
#include "lvr_json.h"
#include <map>
#include <functional>
#include <mutex>
#include <vector>

using namespace std;
class NetHelper: public lvr_singleton<NetHelper> {

    friend class lvr_singleton<NetHelper>;
    NetHelper();
public:
    ~NetHelper();
    void init();
    void request_post(const string& url,
                      lvr_json* param,
                      const function<void(lvr_json*) >& callback);

protected:
    void on_receive(HttpClient* client, HttpResponse* response);
    void gen_requst_body(lvr_json* param, const string &url);

    string encode(const string& str);
    string decode(const string& str);
    lvr_json* decode(vector<char> *);
    string get_current_time_mill();
    void test();
private:

    mutex _lock;
    map<string, function<void(lvr_json*) >> _map_callbacks;
    vector<string> _request_header;

    string _tag;
    string _requst_body;
    string _http_url;
public:
    static const char* CATE_TAG_LIST ;
    static const char* CATE_LIST     ;
    static const char* INDEX_CATE_LIST    ;
    static const char* LIST               ;
    static const char* SLIDESHOW_LIST;
};


#endif //CINEMAVR_NETHELPER_H
