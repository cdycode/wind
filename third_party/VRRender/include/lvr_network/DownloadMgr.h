//
// Created by sunpengfei on 16/7/4.
//

#ifndef CINEMAVR_DOWNLOADMGR_H
#define CINEMAVR_DOWNLOADMGR_H

#include "lvr_network_configure.h"
#include "lvr_singleton.h"
#include <string>
#include <map>
#include <condition_variable>
#include <mutex>
#include <thread>
#include <list>
#include <atomic>
#include <queue>

#include "lvr_thread.h"

#ifdef LVR_OS_IOS
#include "curl.h"
#else
#include "curl/curl.h"
#endif

typedef void(*NetWork_Error_Func)(int);
typedef struct DownloadTask{
    enum TaskStatus{
        STATUS_INIT=0,
        STATUS_WAIT_FOR_DOWNLOAD,
        STATUS_DOWNLOADING,
        STATUS_DOWNLOAD_PAUSE,
        STATUS_DOWNLOAD_SUCCESS,
        STATUS_DOWNLOAD_FAIL,
        STATUS_DOWNLOAD_UNKONWN,
    };
	int			_task_id;
	int			_workerid;
	void*		_databuffer;
	int			_databuffer_all_length;
	int			_databuffer_used_length;
	bool		_is_down_to_file;
	bool		_is_anew;
	bool		_is_need_cache;
    TaskStatus	_status = STATUS_INIT;
    std::string _url;
    std::string _title;
    std::string _save_path;
    std::string _save_name;
	std::string _err_string;
	std::function<void(bool, int)> _end_callback;
} DownloadTask;


class Downloader;
class LVR_NETWORK_API DownloadMgr/*: public lvr_singleton<DownloadMgr>*/ {
  //  friend class lvr_singleton<DownloadMgr>;

public:
	static DownloadMgr* get_ins();
	static void release_ins();
private:
	static DownloadMgr*	_singleptr;
	DownloadMgr();

public:
    ~DownloadMgr();

public:
	void thread_running();

private:
    static const int NUM_TOTAL_WORDER = 5;
    std::string                         _default_dir_down;
    std::mutex							_mutex_task;
	std::condition_variable				_condition_task;
    std::map<int, DownloadTask*>		_map_dl_task;
	std::queue<int>						_que_waitfor_task_id;

    static Downloader                         _dl_workers[NUM_TOTAL_WORDER];
	int									_last_download_time;
	std::thread*						_mgr_thread;
	bool								_is_exit;

public:
    void init(int num_work = NUM_TOTAL_WORDER);
	void set_cache_path(const char* a_cache_path);
	int  gen_download_task();
    void set_task_url(int taskid, std::string url);
	void set_task_save_to_file(int taskid, std::string path = "");
    void set_task_title(int taskid, std::string title);
    void set_task_callback(int taskid, std::function<void(bool success, int taskid)> &callback);
	void set_task_databuffer(int taskid, char* buffer, int all_length);
	void set_tast_network_error_callback(int taskid, NetWork_Error_Func acb);
	int get_data_buffer_used_length(int taskid);
	void set_task_download_cache(int taskid, bool need_cache);


    int task_start(int taskid, bool anew=true);
    int task_stop(int taskid);
    int task_pause(int taskid);
    int task_resume(int taskid);

    int get_task_progress(int taskid);
	
	std::string get_task_save_path(int taskid);

    void update(int tag);
	void on_download_over(bool isok, Downloader* down);

    void save_status_to_db(int taskid);

	void http_get(){}
	void http_post(){}

	// 0 not down  1: downloading  2:already in file
	int check_download_file(std::string &url, std::string dir="");
	int get_task_status(int taskid);
	void set_default_cache_path(const std::string& path);
    const std::string& get_default_cache_path();
    void un_init();
private:
    std::string get_file_name(const std::string& url);


};


#endif
