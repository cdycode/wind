#ifndef __lvr_network_configure_h__
#define __lvr_network_configure_h__

#include "lvr_core_lib.h"

#ifdef lvr_network_EXPORTS
#define LVR_NETWORK_API LVR_API_EXPORT
#elif defined lvr_network_IMPORT
#define LVR_NETWORK_API LVR_API_IMPORT
#else
#define LVR_NETWORK_API LVR_API_STATIC
#endif


#endif