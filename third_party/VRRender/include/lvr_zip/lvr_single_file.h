#ifndef __lvr_single_file_h__
#define __lvr_single_file_h__

#include "lvr_zip_configure.h"

class lvr_single_file
{
public:
	lvr_single_file(){}
	virtual ~lvr_single_file(){}
public:
	virtual const int8_t* get_file_name() = 0;
	virtual int32_t get_file_buf_len() = 0;
	virtual const int8_t* get_file_buf() = 0;
};

#endif
