#ifndef __lvr_zip_h__
#define __lvr_zip_h__

#include "lvr_zip_configure.h"
#include "lvr_single_file.h"

class lvr_zip
{
public:
	lvr_zip(){}
	virtual ~lvr_zip(){}
public:
	virtual bool open_zip_file(const int8_t* a_file_path) = 0;
	virtual bool open_memory_zip_file(const int8_t* a_zip_buf) = 0;
	virtual lvr_single_file* get_single_file(const int8_t* a_file_name) = 0;
	virtual lvr_single_file* get_next_file_with_extension(const int8_t* a_ext) = 0;
	virtual void close() = 0;
};

LVR_ZIP_API lvr_zip* lvr_create_zip_file();
LVR_ZIP_API void lvr_release_zip_file(lvr_zip* ao_zip);

#endif
