#ifndef __lvr_zip_configure_h__
#define __lvr_zip_configure_h__

#include "lvr_core_lib.h"

#ifdef lvr_zip_EXPORTS
#define LVR_ZIP_API __declspec(dllexport)
#elif lvr_zip_IMPORT
#define LVR_ZIP_API __declspec(dllimport)
#else
#define LVR_ZIP_API
#endif


#endif
