#ifndef  __lvr_util_configure_h__
#define  __lvr_util_configure_h__


#include "lvr_core_lib.h"
#include "lvr_math_lib.h"


#ifdef lvr_util_EXPORTS
#define LVR_UTIL_API LVR_API_EXPORT
#elif lvr_util_IMPORT
#define LVR_UTIL_API LVR_API_IMPORT
#else
#define LVR_UTIL_API LVR_API_STATIC
#endif

#include <list>
using std::list;

#endif
