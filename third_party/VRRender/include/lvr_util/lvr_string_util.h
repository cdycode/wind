#ifndef __lvr_string_util_h__
#define __lvr_string_util_h__

#include "lvr_util_configure.h"
#include <vector>
namespace lvr_string_util
{
	static const int MAX_PATH_LENGTH	= 1024;

	template < size_t size >
	inline const char * Copy( char (&dest)[size], const char * src )
	{
		size_t length = strlen( src ) + 1;
		strncpy( dest, src, length < size ? length : size );
		dest[size - 1] = '\0';
		return dest;
	}

	inline std::string ReplaceChar( const char *text, const char charToReplace, const char newChar )
	{
		std::string result;

		for( uint32_t index = 0; text[ index ]  != '\0'; index++ )
		{
			char ch = text[ index ];
			if ( ch == charToReplace )
			{
				ch = newChar;
			}
			result.append(1, ch );
		}

		return result;
	}

	template < size_t size >
	inline const char * SPrintf( char (&dest)[size], const char * format, ... )
	{
		va_list args;
		va_start( args, format );
		vsnprintf( dest, size, format, args );
		dest[size - 1] = '\0';
		return dest;
	}

	template < size_t size >
	inline const char * VSPrintf( char (&dest)[size], const char * format, va_list args )
	{
		vsnprintf( dest, size, format, args );
		dest[size - 1] = '\0';
		return dest;
	}

	template < size_t size >
	inline const char * GetCleanPath( char (&dest)[size], const char * path, const char separator = '/' )
	{
		for ( int i = 0; i < (int)size; path++ )
		{
			if ( path[0] == '/' || path[0] == '\\' )
			{
				if ( i == 0 || dest[i - 1] != separator )
				{
					dest[i++] = separator;
				}
			}
			else if ( path[0] == '.' && path[1] == '.' &&
				i > 2 && dest[i - 1] == separator && dest[i - 2] != '.' )
			{
				for ( --i; i > 0 && dest[i - 1] != separator; --i ) {}
				path++;
			}
			else
			{
				dest[i++] = path[0];
				if ( path[0] == '\0' )
				{
					break;
				}
			}
		}
		return dest;
	}

	inline int PathCharCmp( const char a, const char b )
	{
		const char c = tolower( a );
		const char d = tolower( b );
		if ( c == d )
		{
			return 0;
		}
		if ( ( c == '\\' || c == '/' ) && ( d == '\\' || d == '/' ) )
		{
			return 0;
		}
		return c - d;
	}

	template < size_t size >
	inline void GetRelativePath( char (&dest)[size], const char * path, const char * relativeTo, const char separator = '/' )
	{
		int match = 0;
		for ( ; path[match] != '\0' && PathCharCmp( path[match], relativeTo[match] ) == 0; match++ ) {}
		if ( match == 0 )
		{
			Copy( dest, path );
			return;
		}
		int folders = 0;
		for ( int i = match; relativeTo[i] != '\0'; i++ )
		{
			if ( relativeTo[i] == '/' || relativeTo[i] == '\\' )
			{
				folders++;
			}
		}
		int length = 0;
		for ( int i = 0; i < folders && length < (int)size - 4; i++ )
		{
			dest[length++] = '.';
			dest[length++] = '.';
			dest[length++] = separator;
		}
		for ( ; path[match] == '\\' || path[match] == '/'; match++ ) {}
		for ( int i = match; path[i] != '\0' && length < (int)size - 1; i++ )
		{
			dest[length++] = path[i];
		}
		dest[length] = '\0';
	}

	template < size_t size >
	inline void GetFolder( char (&dest)[size], const char * path )
	{
		int nameOffset = 0;
		int length = 0;
		for ( int index = 0; path[index] != '\0' && index < (int)size - 1; index++, length++ )
		{
			dest[index] = path[index];
			if ( path[index] == '/' || path[index] == '\\' || path[index] == ':' )
			{
				nameOffset = index + 1;
			}
		}
		dest[nameOffset] = '\0';
	}

	template < size_t size >
	inline void GetFileName( char (&dest)[size], const char * path )
	{
		int nameOffset = 0;
		int length = 0;
		for ( int index = 0; path[index] != '\0'; index++, length++ )
		{
			if ( path[index] == '/' || path[index] == '\\' || path[index] == ':' )
			{
				nameOffset = index + 1;
			}
		}
		int index = 0;
		for ( ; nameOffset + index < length && index < (int)size - 1; index++ )
		{
			dest[index] = path[nameOffset + index];
		}
		dest[index] = '\0';
	}

	template < size_t size >
	inline void GetFileBase( char (&dest)[size], const char * path )
	{
		int nameOffset = 0;
		int extensionOffset = -1;
		int length = 0;
		for ( int index = 0; path[index] != '\0'; index++, length++ )
		{
			if ( path[index] == '/' || path[index] == '\\' || path[index] == ':' )
			{
				nameOffset = index + 1;
			}
			else if ( path[index] == '.' )
			{
				extensionOffset = index;
			}
		}
		if ( extensionOffset == -1 )
		{
			extensionOffset = length;
		}
		int index = 0;
		for ( ; nameOffset + index < extensionOffset && index < (int)size - 1; index++ )
		{
			dest[index] = path[nameOffset + index];
		}
		dest[index] = '\0';
	}

	template < size_t size >
	inline void GetFileExtension( char (&dest)[size], const char * path )
	{
		int extensionOffset = -1;
		for ( int index = 0; path[index] != '\0'; index++ )
		{
			if ( path[index] == '.' )
			{
				extensionOffset = index + 1;
			}
		}
		int index = 0;
		if ( extensionOffset != -1 )
		{
			for ( ; path[extensionOffset + index] != '\0' && index < (int)size - 1; index++ )
			{
				dest[index] = path[extensionOffset + index];
			}
		}
		dest[index] = '\0';
	}

	template < size_t size >
	inline void SetFileExtension( char (&dest)[size], const char * path, const char * extension )
	{
		int extensionOffset = -1;
		int length = 0;
		for ( int index = 0; path[index] != '\0' && index < (int)size - 1; index++, length++ )
		{
			dest[index] = path[index];
			if ( path[index] == '.' )
			{
				extensionOffset = index;
			}
		}
		if ( length >= (int)size - 1 )
		{
			return;
		}
		if ( extensionOffset == -1 )
		{
			extensionOffset = length;
		}
		dest[extensionOffset++] = '.';
		if ( extension[0] == '.' )
		{
			extension++;
		}
		int index = 0;
		for ( ; extension[index] != '\0' && extensionOffset + index < (int)size - 1; index++ )
		{
			dest[extensionOffset + index] = extension[index];
		}
		dest[extensionOffset + index] = '\0';
	}

	inline std::string GetCleanPathString( const char * path, const char separator = '/' ) { char buffer[MAX_PATH_LENGTH]; GetCleanPath( buffer, path, separator ); return std::string( buffer ); }
	inline std::string GetRelativePathString( const char * path, const char * relativeTo, const char separator = '/' ) { char buffer[MAX_PATH_LENGTH]; GetRelativePath( buffer, path, relativeTo, separator ); return std::string( buffer ); }
	inline std::string GetFolderString( const char * path ) { char buffer[MAX_PATH_LENGTH]; GetFolder( buffer, path ); return std::string( buffer ); }
	inline std::string GetFileNameString( const char * path ) { char buffer[MAX_PATH_LENGTH]; GetFileName( buffer, path ); return std::string( buffer ); }
	inline std::string GetFileBaseString( const char * path ) { char buffer[MAX_PATH_LENGTH]; GetFileBase( buffer, path ); return std::string( buffer ); }
	inline std::string GetFileExtensionString( const char * path ) { char buffer[MAX_PATH_LENGTH]; GetFileExtension( buffer, path ); return std::string( buffer ); }
	inline std::string SetFileExtensionString( const char * path, const char * extension ) { char buffer[MAX_PATH_LENGTH]; SetFileExtension( buffer, path, extension ); return std::string( buffer ); }

	// std::string format functor.
	class Va
	{
	public:

		Va( const char * format, ... )
		{
			va_list args;
			va_start( args, format );
			VSPrintf( buffer, format, args );
		}

		operator const char * () { return buffer; }

	private:
		char buffer[MAX_PATH_LENGTH];
	};

#if defined( _MSC_VER ) && _MSC_VER <= 1700
	// MSVC doesn't fully support C99
	inline float strtof( const char * str, char ** endptr )	{ return (float)strtod( str, endptr ); }
#endif

#if (__ANDROID_API__ > 19)
	#define 	strtof strtod
#endif
	//
	// Convert a common type to a string.
	//

	template< typename _type_ > inline std::string ToString( const _type_ & value ) { return std::string(); }

	template< typename _type_ > inline std::string ToString( const _type_ * valueArray, const int count )
	{
		std::string string = "{";
		for ( int i = 0; i < count; i++ )
		{
			string += ToString( valueArray[i] );
		}
		string += "}";
		return string;
	}

	template< typename _type_ > inline std::string ToString( const std::vector< _type_ > & valueArray )
	{
		std::string string = "{";
		for ( int i = 0; i < valueArray.size(); i++ )
		{
			string += ToString( valueArray[i] );
		}
		string += "}";
		return string;
	}

	// specializations

	template<> inline std::string ToString( const short &          value ) { return std::string( Va( " %hi", value ) ); }
	template<> inline std::string ToString( const unsigned short & value ) { return std::string( Va( " %uhi", value ) ); }
	template<> inline std::string ToString( const int &            value ) { return std::string( Va( " %li", value ) ); }
	template<> inline std::string ToString( const unsigned int &   value ) { return std::string( Va( " %uli", value ) ); }
	template<> inline std::string ToString( const float &          value ) { return std::string( Va( " %f", value ) ); }
	template<> inline std::string ToString( const double &         value ) { return std::string( Va( " %f", value ) ); }

	template<> inline std::string ToString( const lvr_vector2f & value ) { return std::string( Va( "{ %f %f }", value[0], value[1] ) ); }
	template<> inline std::string ToString( const lvr_vector2d & value ) { return std::string( Va( "{ %f %f }", value[0], value[1] ) ); }
	template<> inline std::string ToString( const lvr_vector2i & value ) { return std::string( Va( "{ %d %d }", value[0], value[1] ) ); }

	template<> inline std::string ToString( const lvr_vector3f & value ) { return std::string( Va( "{ %f %f %f }", value[0], value[1], value[2] ) ); }
	template<> inline std::string ToString( const lvr_vector3d & value ) { return std::string( Va( "{ %f %f %f }", value[0], value[1], value[2] ) ); }
	template<> inline std::string ToString( const lvr_vector3i & value ) { return std::string( Va( "{ %d %d %d }", value[0], value[1], value[2] ) ); }

	template<> inline std::string ToString( const lvr_vector4f & value ) { return std::string( Va( "{ %f %f %f %f }", value[0], value[1], value[2], value[3] ) ); }
	template<> inline std::string ToString( const lvr_vector4d & value ) { return std::string( Va( "{ %f %f %f %f }", value[0], value[1], value[2], value[3] ) ); }
	template<> inline std::string ToString( const lvr_vector4i & value ) { return std::string( Va( "{ %d %d %d %d }", value[0], value[1], value[2], value[3] ) ); }
	template<> inline std::string ToString(const lvr_vector4uc & value) { return std::string(Va("{ %d %d %d %d }", value[0], value[1], value[2], value[3])); }


	template<> inline std::string ToString( const lvr_matrix4f & value ) { return std::string( Va( "{ %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f }", value(0,0), value( 0, 1 ), value( 0, 2 ), value( 0, 3 ), value( 1, 0 ), value( 1, 1 ), value( 1, 2 ), value( 1, 3 ), value( 2, 0 ), value( 2, 1 ), value( 2, 2 ), value( 2, 3 ), value( 3, 0 ), value( 3, 1 ), value( 3, 2 ), value( 3, 3 ) ) ); }
	template<> inline std::string ToString( const lvr_matrix4d & value ) { return std::string( Va( "{ %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f }", value( 0, 0 ), value( 0, 1 ), value( 0, 2 ), value( 0, 3 ), value( 1, 0 ), value( 1, 1 ), value( 1, 2 ), value( 1, 3 ), value( 2, 0 ), value( 2, 1 ), value( 2, 2 ), value( 2, 3 ), value( 3, 0 ), value( 3, 1 ), value( 3, 2 ), value( 3, 3 ) ) ); }

	template<> inline std::string ToString( const lvr_quaternionf &    value ) { return std::string( Va( "{ %f %f %f %f }", value[0], value[1], value[2], value[3] ) ); }
	template<> inline std::string ToString( const lvr_quaterniond &    value ) { return std::string( Va( "{ %f %f %f %f }", value[0], value[1], value[2], value[3] ) ); }

	template<> inline std::string ToString( const lvr_plane3f &   value ) { return std::string( Va( "{ %f %f %f %f }", value.get_normal()[0], value.get_normal()[1], value.get_normal()[2], value.get_distance_to_origin() ) ); }
	template<> inline std::string ToString( const lvr_plane3d &   value ) { return std::string( Va( "{ %f %f %f %f }", value.get_normal()[0], value.get_normal()[1], value.get_normal()[2], value.get_distance_to_origin() ) ); }

	template<> inline std::string ToString( const lvr_bounding_box3f & value ) { return std::string( Va( "{{ %f %f %f }{ %f %f %f }}", value.get_min()[0], value.get_min()[1], value.get_min()[2], value.get_max()[0], value.get_max()[1], value.get_max()[2] ) ); }
	template<> inline std::string ToString( const lvr_bounding_box3d & value ) { return std::string( Va( "{{ %f %f %f }{ %f %f %f }}", value.get_min()[0], value.get_min()[1], value.get_min()[2], value.get_max()[0], value.get_max()[1], value.get_max()[2] ) ); }

	//
	// Convert a string to a common type.
	//

	template< typename _type_ > inline size_t StringTo( _type_ & value, const char * string ) { return 0; }

	template< typename _type_ > inline size_t StringTo( _type_ * valueArray, const int count, const char * string )
	{
		size_t length = 0;
		length += strspn( string + length, "{ \t\n\r" );
		for ( int i = 0; i < count; i++ )
		{
			length += StringTo< _type_ >( valueArray[i], string + length );
		}
		length += strspn( string + length, "} \t\n\r" );
		return length;
	}

	template< typename _type_ > inline size_t StringTo( std::vector< _type_ > & valueArray, const char * string )
	{
		size_t length = 0;
		length += strspn( string + length, "{ \t\n\r" );
		for ( ; ; )
		{
			_type_ value;
			size_t s = StringTo< _type_ >( value, string + length );
			if ( s == 0 ) break;
			valueArray.push_back( value );
			length += s;
		}
		length += strspn( string + length, "} \t\n\r" );
		return length;
	}

	// specializations
	template<> inline size_t StringTo(char & value, const char * str) { char * endptr; value = (char)strtoul(str, &endptr, 10); return endptr - str; }
	template<> inline size_t StringTo(unsigned char & value, const char * str) { char * endptr; value = (unsigned char)strtoul(str, &endptr, 10); return endptr - str; }
	template<> inline size_t StringTo( short &          value, const char * str ) { char * endptr; value = (short) strtol( str, &endptr, 10 ); return endptr - str; }
	template<> inline size_t StringTo( unsigned short & value, const char * str ) { char * endptr; value = (unsigned short) strtoul( str, &endptr, 10 ); return endptr - str; }
	template<> inline size_t StringTo( int &            value, const char * str ) { char * endptr; value = strtol( str, &endptr, 10 ); return endptr - str; }
	template<> inline size_t StringTo( unsigned int &   value, const char * str ) { char * endptr; value = strtoul( str, &endptr, 10 ); return endptr - str; }
	template<> inline size_t StringTo( float &          value, const char * str ) { char * endptr; value = strtof( str, &endptr ); return endptr - str; }
	template<> inline size_t StringTo( double &         value, const char * str ) { char * endptr; value = strtod( str, &endptr ); return endptr - str; }

	template<> inline size_t StringTo(std::string & value, const char * string) { value = string;  return 0; }

	template<> inline size_t StringTo( lvr_vector2f & value, const char * string ) { return StringTo( &value[0], 2, string ); }
	template<> inline size_t StringTo( lvr_vector2d & value, const char * string ) { return StringTo( &value[0], 2, string ); }
	template<> inline size_t StringTo( lvr_vector2i & value, const char * string ) { return StringTo( &value[0], 2, string ); }

	template<> inline size_t StringTo( lvr_vector3f & value, const char * string ) { return StringTo( &value[0], 3, string ); }
	template<> inline size_t StringTo( lvr_vector3d & value, const char * string ) { return StringTo( &value[0], 3, string ); }
	template<> inline size_t StringTo( lvr_vector3i & value, const char * string ) { return StringTo( &value[0], 3, string ); }

	template<> inline size_t StringTo( lvr_vector4f & value, const char * string ) { return StringTo( &value[0], 4, string ); }
	template<> inline size_t StringTo( lvr_vector4d & value, const char * string ) { return StringTo( &value[0], 4, string ); }
	template<> inline size_t StringTo( lvr_vector4i & value, const char * string ) { return StringTo( &value[0], 4, string ); }
	template<> inline size_t StringTo(lvr_vector4uc & value, const char * string)  {  return  StringTo( &value[0], 4, string); }

	template<> inline size_t StringTo( lvr_matrix4f & value, const char * string ) { return StringTo( &value[0], 16, string ); }
	template<> inline size_t StringTo( lvr_matrix4d & value, const char * string ) { return StringTo( &value[0], 16, string ); }

	template<> inline size_t StringTo( lvr_quaternionf &    value, const char * string ) { return StringTo( &value[0], 4, string ); }
	template<> inline size_t StringTo( lvr_quaterniond &    value, const char * string ) { return StringTo( &value[0], 4, string ); }

	template<> inline size_t StringTo( lvr_plane3f &   value, const char * string ) { return StringTo( &value.get_normal()[0], 4, string ); }
	template<> inline size_t StringTo( lvr_plane3d &   value, const char * string ) { return StringTo( &value.get_normal()[0], 4, string ); }

	template<> inline size_t StringTo( lvr_bounding_box3f & value, const char * string ) { return StringTo( &(value.get_min()), 2, string ); }
	template<> inline size_t StringTo( lvr_bounding_box3d & value, const char * string ) { return StringTo( &(value.get_min()), 2, string ); }
}

#endif