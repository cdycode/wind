#ifndef __lvr_json_h__
#define __lvr_json_h__

#include "lvr_util_configure.h"
#include "lvr_ref.h"
#include "lvr_list.h"

enum JSONItemType
{
	JSON_None      = 0,
	JSON_Null      = 1,
	JSON_Bool      = 2,
	JSON_Number    = 3,
	JSON_String    = 4,
	JSON_Array     = 5,
	JSON_Object    = 6
};

class lvr_json : public lvr_ref,public lvr_list_node<lvr_json>
{
public:
	lvr_json(JSONItemType itemType = JSON_Object);
	~lvr_json();
public:
	static lvr_json*	create_object(){return new lvr_json(JSON_Object);}
	static lvr_json*    CreateNull()                 { return new lvr_json(JSON_Null); }
	static lvr_json*    CreateArray()                { return new lvr_json(JSON_Array); }
	static lvr_json*    CreateBool(bool b)           { return createHelper(JSON_Bool, b ? 1.0 : 0.0); }
	static lvr_json*    CreateNumber(double num)     { return createHelper(JSON_Number, num); }
	static lvr_json*    CreateString(const char *s)  { return createHelper(JSON_String, 0.0, s); }

	// Creates a new lvr_json object from parsing the given string.
	// Returns a null pointer and fills in *perror in case of parse error.
	static lvr_json*    Parse(const char* buff, const char** perror = 0);

	// Loads and parses a lvr_json object from a file.
	// Returns a null pointer and fills in *perror in case of parse error.
	static lvr_json*    Load(const char* path, const char** perror = 0);

	// Saves a lvr_json object to a file.
	bool            Save(const char* path);

	// *** Object Member Access

	// Child item access functions
	void            AddItem(const char *string, lvr_json* item);
	void            AddBoolItem(const char* name, bool b)            { AddItem(name, CreateBool(b)); }
	void            AddNumberItem(const char* name, double n)        { AddItem(name, CreateNumber(n)); }
	void            AddStringItem(const char* name, const char* s)   { AddItem(name, CreateString(s)); }

	// These provide access to child items of the list.
	bool            HasItems() const         { return Children.IsEmpty(); }
	// Returns first/last child item, or null if child list is empty.
	lvr_json*           GetFirstItem()           { return (!Children.IsEmpty()) ? Children.GetFirst() : 0; }
	lvr_json*           GetLastItem()            { return (!Children.IsEmpty()) ? Children.GetLast() : 0; }

	// Counts the number of items in the object; these methods are inefficient.
	unsigned        GetItemCount() const;
	lvr_json*           GetItemByIndex(unsigned i);
	lvr_json*           GetItemByName(const char* name);

	// Returns next item in a list of children; 0 if no more items exist.
	lvr_json*           GetNextItem(lvr_json* item)  { return Children.IsNull(item->pNext) ? 0 : item->pNext; }
	lvr_json*           GetPrevItem(lvr_json* item)  { return Children.IsNull(item->pPrev) ? 0 : item->pPrev; }

	// Value access with range checking where possible.
	// Using the JsonReader class is recommended instead of using these.
	bool			GetBoolValue() const;
	int32_t			GetInt32Value() const;
	int64_t			GetInt64Value() const;
	float			GetFloatValue() const;
	double			GetDoubleValue() const;
	const std::string &	GetStringValue() const;

	// *** Array Element Access

	// Add new elements to the end of array.
	void            AddArrayElement(lvr_json *item);
	void            AddArrayBool(bool b)			{ AddArrayElement(CreateBool(b)); }
	void            AddArrayNumber(double n)        { AddArrayElement(CreateNumber(n)); }
	void            AddArrayString(const char* s)   { AddArrayElement(CreateString(s)); }
	void			EmptyArray();
	// Accessed array elements; these methods are inefficient.
	int             GetArraySize();
	double          GetArrayNumber(int index);
	const char*     GetArrayString(int index);

	//need free the return value
	char*           PrintValueString(int depth, bool fmt){return PrintValue(depth,fmt);};

protected:
	static lvr_json*    createHelper(JSONItemType itemType, double dval, const char* strVal = 0);

	// lvr_json Parsing helper functions.
	const char*     parseValue(const char *buff, const char** perror);
	const char*     parseNumber(const char *num);
	const char*     parseArray(const char* value, const char** perror);
	const char*     parseObject(const char* value, const char** perror);
	const char*     parseString(const char* str, const char** perror);

	char*           PrintValue(int depth, bool fmt);
	char*           PrintObject(int depth, bool fmt);
	char*           PrintArray(int depth, bool fmt);

	friend class JsonReader;
public:
	JSONItemType	_type;
	std::string		_name;
	std::string		_value;
	double			_dvalue;
protected:
	lvr_list<lvr_json>	Children;
};

class JsonReader
{
public:
	JsonReader( const lvr_json * json ) :
	  Parent( json ),
		  Child( json != NULL ? json->Children.GetFirst() : NULL ) {}

	  bool			IsValid() const { return Parent != NULL; }
	  bool			IsObject() const { return Parent != NULL && Parent->_type == JSON_Object; }
	  bool			IsArray() const { return Parent != NULL && Parent->_type == JSON_Array; }
	  bool			IsEndOfArray() const { LVR_ASSERT( Parent != NULL ); return Parent->Children.IsNull( Child ); }

	  const lvr_json *	GetChildByName( const char * childName ) const;

	  bool			GetChildBoolByName( const char * childName, const bool defaultValue = false ) const;
	  int32_t			GetChildInt32ByName( const char * childName, const int32_t defaultValue = 0 ) const;
	  int64_t			GetChildInt64ByName( const char * childName, const int64_t defaultValue = 0 ) const;
	  float			GetChildFloatByName( const char * childName, const float defaultValue = 0.0f ) const;
	  double			GetChildDoubleByName( const char * childName, const double defaultValue = 0.0 ) const;
	  const std::string	GetChildStringByName( const char * childName, const std::string & defaultValue = std::string( "" ) ) const;

	  const lvr_json *	GetNextArrayElement() const;

	  bool			GetNextArrayBool( const bool defaultValue = false ) const;
	  int32_t			GetNextArrayInt32( const int32_t defaultValue = 0 ) const;
	  int64_t			GetNextArrayInt64( const int64_t defaultValue = 0 ) const;
	  float			GetNextArrayFloat( const float defaultValue = 0.0f ) const;
	  double			GetNextArrayDouble( const double defaultValue = 0.0 ) const;
	  const std::string	GetNextArrayString( const std::string & defaultValue = std::string( "" ) ) const;

private:
	const lvr_json *			Parent;
	mutable const lvr_json *	Child;		// cached child pointer
};

#endif
