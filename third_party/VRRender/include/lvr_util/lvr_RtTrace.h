#ifndef __RTTRACE_H__
#define __RTTRACE_H__

#include "lvr_util_configure.h"
#include <vector>


const int RT_KDTREE_MAX_LEAF_TRIANGLES	= 4;

struct kdtree_header_t
{
	int			numVertices;
	int			numUvs;
	int			numIndices;
	int			numNodes;
	int			numLeafs;
	int			numOverflow;
	lvr_bounding_box3f	bounds;
};

struct kdtree_node_t
{
	// bits [ 0,0] = leaf flag
	// bits [ 2,1] = split plane (0 = x, 1 = y, 2 = z, 3 = invalid)
	// bits [31,3] = index of left child (+1 = right child index), or index of leaf data
	unsigned int	data;
	float			dist;
};

struct kdtree_leaf_t
{
	int			triangles[RT_KDTREE_MAX_LEAF_TRIANGLES];
	int			ropes[6];
	lvr_bounding_box3f	bounds;
};

struct traceResult_t
{
	int			triangleIndex;
	float		fraction;
	lvr_vector2f	uv;
	lvr_vector3f	normal;
};

class RtTrace
{
public:
							RtTrace() {}
							~RtTrace() {}

	traceResult_t			Trace( const lvr_vector3f & start, const lvr_vector3f & end ) const;
	traceResult_t			Trace_Exhaustive( const lvr_vector3f & start, const lvr_vector3f & end ) const;

public:
	kdtree_header_t			header;
	std::vector< lvr_vector3f >		vertices;
	std::vector< lvr_vector2f >		uvs;
	std::vector< int >			indices;
	std::vector< kdtree_node_t >	nodes;
	std::vector< kdtree_leaf_t >	leafs;
	std::vector< int >			overflow;
};


#endif // !__RTTRACE_H__
