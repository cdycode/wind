#ifndef __lvr_log_math_object_h__
#define __lvr_log_math_object_h__

#include "lvr_util_configure.h"

inline void lvr_log_vector3f(const char* a_log_tag,const lvr_vector3f& a_vec)
{
	LVR_LOG("%s %.5f %.5f %.5f",a_log_tag,a_vec[0],a_vec[1],a_vec[2]);
}
inline void lvr_log_vector4f(const char* a_log_tag,const lvr_vector4f& a_vec)
{
	LVR_LOG("%s %.5f %.5f %.5f %.5f",a_log_tag,a_vec[0],a_vec[1],a_vec[2],a_vec[3]);
}

inline void lvr_log_quaternionf(const char* a_log_tag,const lvr_quaternionf& a_q)
{
	LVR_LOG("%s w:%.5f x:%.5f y:%.5f z:%.5f",a_log_tag,a_q[0],a_q[1],a_q[2],a_q[3]);
}

inline void lvr_log_matrix4f(const char* a_log_tag,const lvr_matrix4f& a_m)
{
	LVR_LOG("%s 0:%.5f %.5f %.5f %.5f 1:%.5f %.5f %.5f %.5f 2:%.5f %.5f %.5f %.5f 3:%.5f %.5f %.5f %.5f",
		a_log_tag,a_m[0],a_m[1],a_m[2],a_m[3],a_m[4],a_m[5],a_m[6],a_m[7],
		a_m[8],a_m[9],a_m[10],a_m[11],a_m[12],a_m[13],a_m[14],a_m[15]);
}



#endif