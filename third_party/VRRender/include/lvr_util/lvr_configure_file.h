#ifndef __lvr_configure_file_h__
#define __lvr_configure_file_h__

#include "lvr_json.h"

//singleton or member
//do not add space before '}'
class lvr_configure_file
{
public:
	static lvr_configure_file& get_configure_data();
	~lvr_configure_file(){}
public:
	void load_configure(const char* file_buf);
	void load_default();
private:
	void check_distortion_parms();
public:
	lvr_vector3f		_camera_pos;
	lvr_quaternionf		_init_orientation;
	float				_eye_distance;
	float				_meters_per_tanangle;
	float				_center_scale;
	lvr_vector4f		_aberration_coeff;//use 4 floats
	std::vector<float>	_distortion_kg;//use 11 floats
	std::vector<float>	_distortion_kr;
	std::vector<float>	_distortion_kb;
	float				_ATW_latency;
	std::string			_glass_name;

	int					_depth_bits;
	int					_stencil_bits;
	int					_msaa;

	int					_frame_rate;
	float				_frame_begin_latency;
	
	bool				_aberration_enable;
	bool				_front_buffer_enable;
	bool				_multi_thread_rendering_enable;
	//////////////////////////////////////////////////////////////
	std::string			_test_movie_name;
	std::string			_test_movie_czm_name;
	std::string			_movie_type_name;
	std::string			_boxOfficePath;
	std::string			_TheatersPath;

	//////////////////////////////////////////////////////////////////////////
	//movie info ui
	float				_max_effect_scale;
	int					_padding_pixels;
	//movie choice ui

private:
	lvr_configure_file();
};

#endif
