#ifndef __lvr_particle_emitter_circle_h__
#define __lvr_particle_emitter_circle_h__

#include "lvr_particle_emitter.h"

class LVR_PARTICLE_SYSTEM_API lvr_particle_emitter_circle
	:public lvr_particle_emitter
{
public:
	lvr_particle_emitter_circle(float a_radius):_radius(a_radius){}
	~lvr_particle_emitter_circle(){}
public:
	virtual void emitter_particle(lvr_particle* ao_p);
private:
	float		_radius;
};

#endif
