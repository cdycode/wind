#ifndef __lvr_particle_emitter_rect_h__
#define __lvr_particle_emitter_rect_h__

#include "lvr_particle_emitter.h"

class LVR_PARTICLE_SYSTEM_API lvr_particle_emitter_rect
	:public lvr_particle_emitter
{
public:
	lvr_particle_emitter_rect(){}
	~lvr_particle_emitter_rect(){}
public:
	virtual void emitter_particle(lvr_particle* ao_p);
private:
	lvr_vector3f	_pos_pts[5];
};

#endif
