#ifndef __lvr_fixed_particle_system_h__
#define __lvr_fixed_particle_system_h__

#include "lvr_particle_system_configure.h"
#include "lvr_particle_system.h"

//in this mode,all the emitter do the same work
//that's to say,you can do nothing but just look
//for fixed case,you may need one program per emitter type.
class LVR_PARTICLE_SYSTEM_API lvr_fixed_particle_system
	:public lvr_particle_system
{
public:
	lvr_fixed_particle_system();
	~lvr_fixed_particle_system();
public:
	virtual void init_with_gl();
	virtual void set_particle_num(uint32_t a_particle_num);
	virtual void update(float a_time_now);
	virtual void set_pose(const lvr_vector3f& a_right,const lvr_vector3f& a_up);
	virtual void draw(const lvr_matrix4f& a_view_proj);
private:
	static lvr_program*			_prog;
	static int					_region_loc;
	static int					_right_loc;
	static int					_up_loc;
	static int					_force_loc;
	static int					_size_loc;
	static int					_life_loc;
	static int					_fire_dir_loc;
	static int					_time_loc;
	static uint32_t				_max_particle_num;
	static lvr_render_object*	_emitter_ro;
	bool						_need_update_uniforms;
	lvr_vector3f				_right;
	lvr_vector3f				_up;
	float						_time_now;
	std::vector<lvr_vector4f>	_pos3as1;
};

#endif