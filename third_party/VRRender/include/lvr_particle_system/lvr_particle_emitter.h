#ifndef __lvr_particle_emitter_h__
#define __lvr_particle_emitter_h__

#include "lvr_particle_system_configure.h"

class lvr_particle
{
public:
	lvr_particle()
		:_pos(0,0,0)
		,_cur_v(0,0,0)
		//,_right(1,0,0)
		//,_up(0,1,0)
		,_size(0)
		,_rest_life(-1)
	{}
public:
	lvr_vector3f	_pos;
	lvr_vector3f	_cur_v;
	//lvr_vector3f	_right;
	//lvr_vector3f	_up;
	lvr_vector3f	_dir;
	lvr_quaternionf	_rot_q;
	float			_size;
	float			_life_now;
	float			_rest_life;//for alpha
};

class lvr_particle_emitter
{
public:
	lvr_particle_emitter()
		:_pos(0.0f)
		,_main_dir(0,1,0)
		,_max_size(0.5f)
		,_min_size(0.1f)
		,_max_life(5.0f)
		,_min_life(3.0f)
		,_max_speed(3.0f)
		,_min_speed(0.1f)
		,_sputter_angle((float)LVR_DEG_TO_RAD(30.0)){}
	virtual ~lvr_particle_emitter(){}
public:
	virtual void emitter_particle(lvr_particle* ao_p) = 0;
	virtual void set_max_size(float a_size){_max_size = a_size;}
	virtual void set_min_size(float a_min_size){_min_size = a_min_size;}
	virtual void set_max_speed(float a_max_speed){_max_speed = a_max_speed;}
	virtual void set_min_speed(float a_min_speed){_min_speed = a_min_speed;}
	virtual void set_max_life(float a_max_life){_max_life = a_max_life;}
	virtual void set_min_life(float a_min_life){_min_life = a_min_life;}
	virtual float get_min_life(){return _min_life;}
	virtual float get_min_size(){return _min_size;}
	virtual float get_max_life(){return _max_life;}
	virtual float get_max_size(){return _max_size;}
	virtual void set_center(lvr_vector3f* a_pos){_pos = *a_pos;}
	virtual lvr_vector3f get_main_dir(){return _main_dir;}
	virtual void set_sputter_angle(float a_angle){_sputter_angle = a_angle;}
	virtual float get_sputter_angle(){return _sputter_angle;}
	virtual void set_init_rot_q(const lvr_quaternionf& q){ _init_q = q; }
	virtual void set_main_dir(lvr_vector3f* a_main_dir,float a_sputter_angle)
	{
		_main_dir = *a_main_dir;
		_sputter_angle = a_sputter_angle;
	}
protected:
	lvr_vector3f	_pos;
	lvr_vector3f	_main_dir;
	float			_max_size;
	float			_min_size;
	float			_max_life;
	float			_min_life;
	float			_max_speed;
	float			_min_speed;
	float			_sputter_angle;
	lvr_quaternionf	_init_q;
};

#endif