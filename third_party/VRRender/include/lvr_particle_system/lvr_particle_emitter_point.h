#ifndef __lvr_particle_emitter_point_h__
#define __lvr_particle_emitter_point_h__

#include "lvr_particle_emitter.h"

class LVR_PARTICLE_SYSTEM_API lvr_particle_emitter_point
	:public lvr_particle_emitter
{
public:
	lvr_particle_emitter_point(const lvr_vector3f& a_pos)
	{
		_pos = a_pos;
	}
	~lvr_particle_emitter_point(){}
public:
	virtual void emitter_particle(lvr_particle* ao_p);
};

#endif
