#ifndef __lvr_particle_system_h__
#define __lvr_particle_system_h__

#include "lvr_particle_system_configure.h"
#include "lvr_particle_emitter.h"

typedef struct lvr_force__
{
	lvr_vector3f	_force_direction;
	float			_intensity;
}lvr_force;

class lvr_particle_system
{
public:
	lvr_particle_system()
		:_texture(NULL)
		,_emitter(NULL)
		,_particles(NULL)
		,_ro(NULL)
		,_last_time(0.0f)
		,_particle_num(0)
		,_max_alpha(1.0f)
		,_min_alpha(0.0f){}
	virtual ~lvr_particle_system(){}
public:
	virtual void init_with_gl() = 0;
	virtual void set_particle_num(uint32_t a_particle_num) = 0;
	virtual void update(float a_time_now) = 0;
	virtual void draw(const lvr_matrix4f& a_view_proj) = 0;
	virtual void set_max_min_alpha(float a_max_alpha,float a_min_alpha)
	{
		_max_alpha = a_max_alpha;
		_min_alpha = a_min_alpha;
	}
	void set_texture(lvr_texture* a_tex)
	{
		if (a_tex)
		{
			_texture = a_tex;
		}
	}
	void add_froce(lvr_force*	a_force)
	{
		if (a_force)
		{
			_forces.push_back(a_force);
		}
	}
	void set_emitter(lvr_particle_emitter* a_emitter)
	{
		if (a_emitter)
		{
			_emitter = a_emitter;
		}
	}
	lvr_particle*	get_particles(uint32_t& ao_particle_num)
	{
		if (_particles)
		{
			ao_particle_num = _particle_num;
			return _particles;
		}
		else
		{
			ao_particle_num = 0;
			return NULL;
		}
	}
protected:
	lvr_texture*			_texture;
	lvr_particle_emitter*	_emitter;
	lvr_particle*			_particles;
	lvr_render_object*		_ro;
	float					_last_time;
	uint32_t				_particle_num;
	float					_max_alpha;
	float					_min_alpha;
	std::vector<lvr_force*>	_forces;
};

#endif
