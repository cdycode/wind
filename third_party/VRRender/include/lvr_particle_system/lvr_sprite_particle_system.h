#ifndef __lvr_sprite_particle_system_h__
#define __lvr_sprite_particle_system_h__

#include "lvr_particle_system_configure.h"
#include "lvr_particle_system.h"

class lvr_particle_emitter;
class LVR_PARTICLE_SYSTEM_API lvr_sprite_particle_system
	:public lvr_particle_system
{
public:
	lvr_sprite_particle_system();
	~lvr_sprite_particle_system();
public:
	virtual void init_with_gl();
	virtual void set_particle_num(uint32_t a_particle_num);
	virtual void update(float a_time_now);
	virtual void draw(const lvr_matrix4f& a_view_proj);
private:
	//the release of this program is not add,add it in the future. to do ....
	static lvr_program*			_prog;
	static int					_max_size_loc;
	std::vector<lvr_vector4f>	_pos3as1;
};

#endif