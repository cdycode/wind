#ifndef __lvr_index_particle_system_h__
#define __lvr_index_particle_system_h__

#include "lvr_particle_system_configure.h"
#include "lvr_particle_system.h"

class lvr_particle_emitter;
class LVR_PARTICLE_SYSTEM_API lvr_index_particle_system
	:public lvr_particle_system
{
public:
	lvr_index_particle_system();
	~lvr_index_particle_system();
public:
	virtual void init_with_gl();
	virtual void set_particle_num(uint32_t a_particle_num);
	virtual void set_emitter_object(lvr_render_object* a_ro);
	virtual void update(float a_time_now);
	virtual void set_pose(const lvr_vector3f& a_right,const lvr_vector3f& a_up);
	virtual void draw(const lvr_matrix4f& a_view_proj);
private:
	//the release of this program is not add,add it in the future. to do ....
	static lvr_program*			_prog;
	static int					_max_size_loc;
	static int					_right_loc;
	static int					_up_loc;
	lvr_render_object*			_emitter_ro;
	lvr_vector3f				_right;
	lvr_vector3f				_up;
	std::vector<lvr_vector4f>	_pos3as1;
};

#endif