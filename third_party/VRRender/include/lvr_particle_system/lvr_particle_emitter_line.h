#ifndef __lvr_particle_emitter_line_h__
#define __lvr_particle_emitter_line_h__

#include "lvr_particle_emitter.h"

class LVR_PARTICLE_SYSTEM_API lvr_particle_emitter_line
	:public lvr_particle_emitter
{
public:
	lvr_particle_emitter_line(const lvr_vector3f& a_begin_pt,const lvr_vector3f& a_end_pt)
		:_begin_pt(a_begin_pt),_end_pt(a_end_pt){}
	~lvr_particle_emitter_line(){}
public:
	virtual void emitter_particle(lvr_particle* ao_p);
	void	set_begin_pt(const lvr_vector3f& a_begin_pt);
	void	set_end_pt(const lvr_vector3f& a_end_pt);
private:
	lvr_vector3f	_begin_pt;
	lvr_vector3f	_end_pt;
};

#endif
