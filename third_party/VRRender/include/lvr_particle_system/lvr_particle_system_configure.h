#ifndef __lvr_particle_system_configure_h__
#define __lvr_particle_system_configure_h__

#include "lvr_core_lib.h"
// #ifdef LVR_OS_WIN32
// #define lvr_render_IMPORT
// #endif
#include "lvr_render_lib.h"

#ifdef lvr_particle_system_EXPORTS
#define LVR_PARTICLE_SYSTEM_API LVR_API_EXPORT
#elif defined lvr_particle_system_IMPORT
#define LVR_PARTICLE_SYSTEM_API LVR_API_IMPORT
#else
#define LVR_PARTICLE_SYSTEM_API LVR_API_STATIC
#endif


#endif
