#ifndef __lvr_particle_emitter_box3_h__
#define __lvr_particle_emitter_box3_h__

#include "lvr_particle_emitter.h"

class LVR_PARTICLE_SYSTEM_API lvr_particle_emitter_box3
	:public lvr_particle_emitter
{
public:
	lvr_particle_emitter_box3():_bounding_box3(lvr_vector3f(-1,-1,-1),lvr_vector3f(1,1,1)){}
	lvr_particle_emitter_box3(const lvr_bounding_box3f& a_box3):_bounding_box3(a_box3){}
	~lvr_particle_emitter_box3(){}
public:
	void set_bounding_box(const lvr_bounding_box3f& a_box3);
	virtual void emitter_particle(lvr_particle* ao_p);
private:
	void get_region(int id,lvr_vector3f* ao_region);
private:
	lvr_bounding_box3f		_bounding_box3;	
};

#endif
