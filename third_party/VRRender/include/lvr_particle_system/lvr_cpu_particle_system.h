#ifndef __lvr_cpu_particle_system_h__
#define __lvr_cpu_particle_system_h__

#include "lvr_particle_system_configure.h"
#include "lvr_particle_system.h"

class LVR_PARTICLE_SYSTEM_API lvr_cpu_particle_system
	:public lvr_particle_system
{
public:
	lvr_cpu_particle_system();
	~lvr_cpu_particle_system();
public:
	virtual void init_with_gl();
	//as you have alread seen the title 'cpu',you know you should not set too big a num to it.
	//do remember this.
	virtual void set_particle_num(uint32_t a_particle_num);
	virtual void update(float a_time_now);
	virtual void set_pose(const lvr_vector3f& a_right,const lvr_vector3f& a_up);
	virtual void draw(const lvr_matrix4f& a_view_proj);
private:
	void update_buffer();
	void set_ibo(uint32_t a_pp_num);
private:
	static lvr_program*			_prog;
	static int					_max_size_loc;
	static int					_right_loc;
	static int					_up_loc;

	uint32_t					_vbo_id;//pos uv
	static uint32_t				_ibo_id;
	static uint32_t				_ibo_pt_num;

	lvr_vector3f				_right;
	lvr_vector3f				_up;
	std::vector<lvr_vector4f>	_pos3as1;
	bool						_ready;
};

#endif