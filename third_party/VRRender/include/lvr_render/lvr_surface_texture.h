#ifndef __lvr_surface_texture_h__
#define __lvr_surface_texture_h__

#include "lvr_render_configure.h"
#ifdef LVR_OS_ANDROID
#include "lvr_texture.h"
#include <jni.h>
class lvr_surface_texture : public lvr_texture
{
public:
	jobject			javaObject;
	JNIEnv * 		jni;

	// Updated when Update() is called, can be used to
	// check if a new frame is available and ready
	// to be processed / mipmapped by other code.
	long long		nanoTimeStamp;

	jmethodID 		updateTexImageMethodId;
	jmethodID 		getTimestampMethodId;
	jmethodID 		setDefaultBufferSizeMethodId;

	lvr_surface_texture( JNIEnv * jni_ );
	~lvr_surface_texture();

	// For some java-side uses, you can set the size
	// of the buffer before it is used to control how
	// large it is.  Video decompression and camera preview
	// always override the size automatically.
	void			set_default_buffer_size( int width, int height );

	// This can only be called with an active GL context.
	// As a side effect, the _tex_id will be bound to the
	// GL_TEXTURE_EXTERNAL_OES target of the currently active
	// texture unit.
	void 			update();
};

#endif

#endif