#ifndef __lvr_vertex_format_h__
#define __lvr_vertex_format_h__

#include "lvr_render_configure.h"
#include <stdarg.h>

class LVR_RENDER_API lvr_vertex_format : public lvr_ref
{
public:
	//ex:
	//lvr_vertex_format* lvf = lvr_vertex_format::create(3,
	//	e_location_position,e_attrib_type_float2,
	//	e_location_uv0,e_attrib_type_float2,
	//	e_location_color,e_attrib_type_ubyte4);
	static lvr_vertex_format* create(int num_attributes_,...);
public:
	//deprecated,suggest not call constructed function outside.use the static method above
	lvr_vertex_format(){}
	~lvr_vertex_format(){}
	void set_attribute (int attribute, unsigned int offset, lvr_enum_vertex_attribute_type type, lvr_enum_vertex_attribute_usage usage);
	void set_stride (int stride);

	int  get_num_attributes();
	unsigned int get_offset(int attribute);
	lvr_enum_vertex_attribute_type get_attribute_type(int attribute);
	lvr_enum_vertex_attribute_usage  get_attribute_usage(int attribute);
	int  get_stride();

	void bind();
	static void unbind_all();
	void unbind();
protected:
	bool set_num_attributes(int num_attributes)
	{
		if (num_attributes <0 || num_attributes > 8)
		{
			return false;
		}
		_num_attributes = num_attributes;
		return true;
	}
	friend class lvr_render_object;
private:
	struct element
	{
		unsigned int offset;
		lvr_enum_vertex_attribute_type type;
		lvr_enum_vertex_attribute_usage usage;
		//int								stride;
		//is this right to support different strides in a vertex buffer?
	};
	int  _num_attributes;
	element _elements[e_max_attribute_num];//32*4 = 128 byte
	int		_stride;
	static int _s_type_size[e_vertex_attribute_num];
	static int _s_type_channel[e_vertex_attribute_num];
	static int _s_type_type[e_vertex_attribute_num];
};

#endif
