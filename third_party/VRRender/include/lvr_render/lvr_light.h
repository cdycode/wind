#ifndef __lvr_light_h__
#define __lvr_light_h__

#include "lvr_render_configure.h"

class LVR_RENDER_API lvr_light
{
public:
	enum type
	{
		e_point_light = 0,
		e_direction_light = 1,
		e_spot_light = 2,
		e_light_num
	};
public:
	lvr_light(){}
	~lvr_light(){}
public:
	void get_light_rect();
public:
	type	_type;
	lvr_vector4uc	_diffuse_color;
	lvr_vector4uc	_ambient_color;
	lvr_vector4uc	_specular_color;
	lvr_vector3f	_attenuation;
	lvr_vector3f	_pos;
	lvr_vector3f	_direction;
	float		_range;
	float		_falloff;
	float		_theta;
	float		_phi;
	float		_intensity;
};

#endif

