#ifndef __lvr_context_h__
#define __lvr_context_h__

#include "lvr_render_configure.h"

struct display_setting
{
	display_setting()
	{
		red_bits = 8;
		green_bits = 8;
		blue_bits = 8;
		alpha_bits = 8;
		depth_bits =24;
		stencil_bits =0;
		multi_sample = 0;
		major_version = 2;
		minor_version = 0;
		height =100;
		width =100;
		pos_x =0;
		pos_y =0;
	}
	unsigned int red_bits;
	unsigned int green_bits;
	unsigned int blue_bits;
	unsigned int alpha_bits;
	unsigned int depth_bits;//bits of the depth buffer
	unsigned int stencil_bits;//bits of the stencil buffer,ģ�建��
	unsigned int multi_sample;//level of antialiasing,�������
	unsigned int major_version;//OpenGL/es major version
	unsigned int minor_version;//OpenGL/es minor version
	unsigned int height;
	unsigned int width;
	unsigned int pos_x;
	unsigned int pos_y;
};

class lvr_context
{
public:
	lvr_context(){}
	virtual ~lvr_context(){}
public:
	virtual int make_current() = 0;
	virtual void swap_buffers() = 0;
	virtual bool share(lvr_context* a_contex) = 0;
};

LVR_RENDER_API lvr_context* lvr_create_context_windows(display_setting& a_setting,uint32_t handle,lvr_context* a_share_context);
LVR_RENDER_API void lvr_release_context(lvr_context* a_ct);

#endif
