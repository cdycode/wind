#ifndef __lvr_gl_windows_define_h__
#define __lvr_gl_windows_define_h__

#include "lvr_gl_define.h"

#ifdef LVR_OS_WIN32
#define LVR_GLSL_VERSION_DEF  ""
#define LVR_GLSL_LEVEL_HIGH   0
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#include "gl/glew.h"
#include <Windows.h>

#define glGenVertexArraysOES_ glGenVertexArrays
#define glBindVertexArrayOES_ glBindVertexArray
#define glDeleteVertexArraysOES_ glDeleteVertexArrays
#define glIsVertexArrayOES_		glIsVertexArray
#define glTexImage3DOES			glTexImage3D
#define glTexSubImage3DOES		glTexSubImage3D
#define GL_TEXTURE_3D_OES		GL_TEXTURE_3D

#define glDrawElementsInstanced_ glDrawElementsInstanced
#define glDrawArraysInstanced_ glDrawArraysInstanced
#define glVertexAttribDivisor_ glVertexAttribDivisor

#define  glBlitFramebuffer_	glBlitFramebuffer
#define  glRenderbufferStorageMultisample_	glRenderbufferStorageMultisample
#define  glRenderbufferStorageMultisampleIMG_ glRenderbufferStorageMultisample
#define  glFramebufferTexture2DMultisampleIMG_ 0x00//only in img exentension

#define glMultiDrawArrays_ glMultiDrawArrays
#define glMultiDrawElements_ glMultiDrawElements
#define glMapBufferRange_ glMapBufferRange

#define HIGHP " "
#define LOWP " "
#define MEDIUMP " " 

#endif//end os win 32
#endif