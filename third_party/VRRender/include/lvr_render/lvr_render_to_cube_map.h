#ifndef __lvr_render_to_cube_map_h__
#define __lvr_render_to_cube_map_h__

#include "lvr_render_texture.h"

class LVR_RENDER_API lvr_render_to_cube_map :public lvr_texture
{
public:
	lvr_render_to_cube_map();
	~lvr_render_to_cube_map();
public:
	bool set_up(color_format a_col_format, int a_size, int a_depth_bits, int a_stencil_bits, int a_msaa = 0);
	const lvr_matrix4f& get_face_view_matrix(lvr_enum_cube_face a_face);
	const lvr_matrix4f& get_proj_matrix();
	void enable_render();
	void render_face(lvr_enum_cube_face a_face);
	void end_render_face(lvr_enum_cube_face a_face);
	void disable_render();
	//option functions
	void set_position(const lvr_vector3f& a_pos);
	//for most case,this function will never called,but I still put it here in case of global work case.like earth simulation
	void set_pose(const lvr_vector3f& a_pos, const lvr_quaternionf& a_q);
private:
	unsigned int	_fbo_id;
    int	            _old_fbo_id;
	unsigned int	_depth_buffer;
	unsigned int	_multisample_color_buffer;
	unsigned int	_resolve_frame_buffer;
	lvr_matrix4f	_view_matrix[6];
	//may be it should multi to the view matrix,but what if outside wish to use its own matrix,and also the interface
	lvr_matrix4f	_proj_matrix;
};


#endif
