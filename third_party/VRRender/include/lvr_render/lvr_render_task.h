#ifndef __lvr_render_task_h__
#define __lvr_render_task_h__

#include "lvr_render_configure.h"
#include "lvr_program.h"
#include "lvr_render_object.h"
#include "lvr_texture.h"
#include "lvr_material.h"

#include <map>
#include <string>

class LVR_RENDER_API lvr_render_task
{
public:
	lvr_render_task();
	~lvr_render_task();
protected:
	enum data_type
	{
		e_render_task_type_float = 0x0100,
		e_render_task_type_float2 = 0x0101,
		e_render_task_type_float3 = 0x0102,
		e_render_task_type_float4 = 0x0104,
		e_render_task_type_matrix4 = 0x0110,
		e_render_task_type_int = 0x0200,
		e_render_task_type_int2 = 0x0201,
		e_render_task_type_int3 = 0x0202,
		e_render_task_type_int4 = 0x0204,
		e_render_task_type_texture = 0x0400,
		e_render_task_type_mask = 0xff00
	};
public:
	void set_render_program(const char* a_vs,const char* a_ps);
	void set_render_program(lvr_program* a_prog);
	//void set_material(lvr_material*	a_material);
	void add_render_object(lvr_render_object* a_ro);

	lvr_program* get_program()const;

	bool set_texture(const char *name, lvr_texture* a_texture);
	bool set_uniform1i(const char *name, int value);
	bool set_uniform1f(const char *name, float value);
	bool set_uniform2i(const char *name, lvr_vector2i& a_info);
	bool set_uniform2f(const char *name, lvr_vector2f& a_info);
	bool set_uniform3i(const char *name, lvr_vector3i& a_info);
	bool set_uniform3f(const char *name, lvr_vector3f& a_info);
	bool set_uniform4i(const char *name, lvr_vector4i& a_info);
	bool set_uniform4f(const char *name, lvr_vector4f& a_info);
	bool set_uniform1fv(const char *name, const float* value, int count = 1);
	bool set_uniform2fv(const char *name, const float* value, int count = 1);
	bool set_uniform3fv(const char *name, const float* value, int count = 1);
	bool set_uniform4fv(const char *name, const float* value, int count = 1);
	bool set_uniform_matrix4fv(const char *name,const float *m, int count=1, bool transpose=false);
	bool execute_task();
	void release_task();
private:
	void add_uniform_info(int a_id,void* a_data,int a_stride,int a_num,data_type a_type);
	int find_uniform_id(const char* a_name);
	void collect_uniforms();
private:
	struct parm_info
	{
	public:
		parm_info()
			:id(-1)
			,stride(0)
			,num(0)
			,dt(e_render_task_type_float)
			,info(NULL)
		{

		}
	public:
		void release()
		{
			if (info)
			{
				switch(dt)
				{
				case  e_render_task_type_texture:
					break;
				case e_render_task_type_float:
					delete[] (float*)info;
					break;
				case e_render_task_type_int:
					delete[] (int*)info;
					break;
				default:
					break;
				}
			}
			info = NULL;
		}
		int id;
		int stride;
		int num;
		data_type dt;
		void* info;
	};
private:
	std::map<std::string,int>	_uniform_locations;
	std::vector<parm_info>		_parm_info;
	std::vector<lvr_render_object*>	_ros;
	lvr_program*				_program;
	//lvr_material*				_material;
	bool						_own_program;
};

#endif
