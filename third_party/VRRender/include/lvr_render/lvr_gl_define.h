#ifndef __lvr_gl_define_h__
#define __lvr_gl_define_h__

#include "lvr_core_lib.h"

#ifdef lvr_render_EXPORTS
#define LVR_RENDER_API LVR_API_EXPORT
#elif defined lvr_render_IMPORT
#define LVR_RENDER_API LVR_API_IMPORT
#else
#define LVR_RENDER_API LVR_API_STATIC
#endif

#include "lvr_math_lib.h"
#include <stdio.h>
#include <stdlib.h>

extern bool lvr_es3_gpu;
extern bool lvr_discard_instead_of_clear;
extern bool lvr_vertex_array_object_support;
extern bool lvr_shader_texture_lod_support;
extern bool lvr_multi_sampled_render_to_texture;
extern bool LVR_gl_draw_instanced;
extern bool LVR_gl_multi_draw;


//need this api? may be not,may omit in the near future
LVR_RENDER_API void lvr_configure_extensions();
LVR_RENDER_API void lvr_gl_invalidate_framebuffer( bool isFBO, bool colorBuffer, bool depthBuffer );
LVR_RENDER_API void lvr_gl_finish();
LVR_RENDER_API void lvr_gl_flush();

//lvr_e_texture_flags
//with mipmaps,and use normal rgb texture,if failed will return default 4*4 texture.
#define LVR_TEXTUREFLAG_DEFAULT		0x00
	// Normally, a failure to load will create an 8x8 default texture, but
	// if you want to take explicit action, setting this flag will cause
	// it to return 0 for the texId.
#define LVR_TEXTUREFLAG_NO_DEFAULT  0x01
	// Use GL_SRGB8 / GL_SRGB8_ALPHA8 / GL_COMPRESSED_SRGB8_ETC2 formats instead
	// of GL_RGB / GL_RGBA / GL_ETC1_RGB8_OES
#define LVR_TEXTUREFLAG_USE_SRGB  0x02
	// No mip maps are loaded or generated when this flag is specified.
#define LVR_TEXTUREFLAG_NO_MIPMAPS 0x04
//end lvr_e_texture_flags

#endif