#ifndef __lvr_vertexbuffer_h__
#define __lvr_vertexbuffer_h__

#include "lvr_render_configure.h"
#include "lvr_hardware_buffer.h"

class lvr_vertex_format;
class LVR_RENDER_API lvr_vertex_buffer : public lvr_hardware_buffer
{
public:
	lvr_vertex_buffer();
	~lvr_vertex_buffer();
public:
	void set_vertex_buffer(lvr_vertex_format* vf_,const int8_t* vbs,unsigned int size_,bool a_update_seldom = true);
	unsigned int get_verterx_num();
	virtual void* lock(unsigned int mode_);
	virtual void update_data(int a_offset,int a_update_data_len,const void* a_datas);
	virtual void unlock();
	virtual void bind();
	virtual void unbind();
private:
	lvr_vertex_format*	_vertex_format;
	unsigned int	_vertex_num;
};

#endif
