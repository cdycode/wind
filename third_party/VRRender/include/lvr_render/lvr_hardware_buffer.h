#ifndef __lvr_haderware_buffer_h__
#define __lvr_haderware_buffer_h__

#include "lvr_render_configure.h"

class LVR_RENDER_API lvr_hardware_buffer : public lvr_ref
{
public:
	lvr_hardware_buffer();
	~lvr_hardware_buffer();
public:
	virtual void* lock(unsigned int a_mode) = 0;
	virtual void update_data(int a_offset,int a_update_data_len,const void* a_datas) = 0;
	virtual void unlock() = 0;
	virtual void bind() = 0;
	virtual void unbind() = 0;
public:
	unsigned int _buf_id;
};

#endif
