#ifndef __lvr_render_object_h__
#define __lvr_render_object_h__

#include "lvr_render_configure.h"
#include <vector>
struct VertexAttribs
{
	std::vector< lvr_vector3f > position;
	std::vector< lvr_vector3f > normal;
	std::vector< lvr_vector3f > tangent;
	std::vector< lvr_vector3f > binormal;
	std::vector< lvr_vector4f > color;
	std::vector< lvr_vector2f > uv0;
	std::vector< lvr_vector2f > uv1;
	std::vector< lvr_vector4uc> jointIndices;
	std::vector< lvr_vector4f > jointWeights;
};

class lvr_vertex_buffer;
class lvr_index_buffer;
class lvr_vertex_format;
class LVR_RENDER_API lvr_render_object
{
public:
	lvr_render_object();
	~lvr_render_object();
public:
	bool create(VertexAttribs& v,std::vector<uint16_t>& ids);
	void set_up(lvr_vertex_buffer* a_vb,lvr_index_buffer* a_ib,lvr_vertex_format* a_vf);
	//only make effect if there is no ib exist.so be careful of using this.it will not make effect if
	//your index buffer has 0 indexs.
	void set_primitive_type(lvr_enum_primitive_type a_prim_type);
	void release_res();
public:
	//if a_firsts is NULL,then draw elements,else draw arrarys;
	void draw_multi(int** a_firsts,int* a_counts,int a_prim_count)const;
	void draw()const;//simply call draw,or call the three downside
	void draw_instanced(int inst_num) const;
	void bind()const;
	//begin pos is in bytes currently if you have index buffer.if not is vertex count
	void draw_part(uint32_t a_begin_pos,uint32_t a_count)const;
	void draw_percent_part(float a_percentage)const;
	void unbind()const;
	lvr_vertex_buffer* get_vertex_buffer();
	lvr_index_buffer*  get_index_buffer();
	lvr_vertex_format* get_vertex_format();
private:
//public:
	lvr_vertex_buffer*		_vb;
	lvr_index_buffer*		_ib;
	lvr_vertex_format*		_vf;
	unsigned int			_primitive_type;
	unsigned int			_vao;
};

#endif
