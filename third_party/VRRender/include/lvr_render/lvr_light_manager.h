#ifndef __lvr_light_manager_h__
#define __lvr_light_manager_h__

#include "lvr_light.h"
#include <vector>

class lvr_render_texture;
typedef struct light_info_
{
	lvr_matrix4d				proj_view;
	lvr_light*					light;
	lvr_render_texture*			shadow_texture;
}light_info;

class LVR_RENDER_API lvr_light_manager
{
public:
	~lvr_light_manager();
public:
	static lvr_light_manager* get_light_manager();
	static void			release_light_manager();
public:
	bool addlight(lvr_light* light_);
private:
	lvr_light_manager(){}
public:
	std::vector<light_info*>	_lights;
};

#endif
