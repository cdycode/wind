#ifndef __lvr_render_to_shadow_cube_h__20171216__
#define __lvr_render_to_shadow_cube_h__20171216__

#include "lvr_render_configure.h"
#include "lvr_texture.h"

class LVR_RENDER_API lvr_render_to_shadow_cube:public lvr_texture
{
public:
	lvr_render_to_shadow_cube() :lvr_texture(e_texture_type_cube), _fbo_id(0), _old_fbo_id(0)
	{
		//_tex_type = e_texture_type_2d;
	}
	~lvr_render_to_shadow_cube();
public:
	void set_up(texture_filter a_tex_filter,int a_width,int a_depth_bits);
	void release_res();
	const lvr_matrix4f& get_face_view_matrix(lvr_enum_cube_face a_face);
	const lvr_matrix4f& get_proj_matrix();
	void enable_render();
	void render_face(lvr_enum_cube_face a_face);
	void end_render_face(lvr_enum_cube_face a_face);
	void disable_render();
	//option functions
	void set_position(const lvr_vector3f& a_pos);
	lvr_vector3f get_position();
private:
	unsigned int	_fbo_id;
	int				_old_fbo_id;
	lvr_matrix4f	_view_matrix[6];
	//may be it should multi to the view matrix,but what if outside wish to use its own matrix,and also the interface
	lvr_matrix4f	_proj_matrix;
	lvr_vector3f	_position;
};

#endif
