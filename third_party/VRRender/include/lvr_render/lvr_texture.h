#ifndef __lvr_texture_h__
#define __lvr_texture_h__

#include "lvr_render_configure.h"

extern bool TextureFormatToGlFormat( const int format, const bool useSrgbFormat, GLenum & glFormat, GLenum & glInternalFormat );
extern bool GlFormatToTextureFormat( int & format, const GLenum glFormat, const GLenum glInternalFormat );
extern int32_t lvr_get_texture_size( const int format, const int w, const int h );
class LVR_RENDER_API lvr_texture : public lvr_ref
{
public:
	enum color_format
	{
		e_COLOR_R32F,
		e_COLOR_565,
		e_COLOR_8888,
		e_COLOR_8888_sRGB,
		e_COLOR_NONE
	};
	enum texture_filter
	{
		e_TEXTURE_FILTER_NEAREST,		// Causes significant aliasing, only for performance testing.
		e_TEXTURE_FILTER_BILINEAR,	// This should be used under almost all circumstances.
		e_TEXTURE_FILTER_ANISO_2,		// Anisotropic filtering can in some cases reduce aliasing.
		e_TEXTURE_FILTER_ANISO_4
	};
public:
	lvr_texture(lvr_enum_texture_type tt_):_tex_type(tt_),_tex_id(0),_width(0),_height(0){}
	lvr_texture():_tex_type(e_texture_type_none),_tex_id(0),_width(0),_height(0){}
	virtual ~lvr_texture();
public:
	unsigned int get_tex(){return _tex_id;}
	lvr_enum_texture_type get_tex_type(){return _tex_type;}
	void bind(unsigned int slot);
	void unbind(unsigned int slot);
	int  get_width(){return _width;}
	int  get_height(){return _height;}
protected:
	lvr_enum_texture_type	 _tex_type;
	unsigned int _tex_id;
	int _width,_height;
}; 

#endif
