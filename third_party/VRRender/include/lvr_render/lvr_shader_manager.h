#ifndef __py_shader_manager_h__
#define __py_shader_manager_h__

#include "lvr_render_configure.h"
#include <map>
#include <string>

class lvr_program;
class LVR_RENDER_API lvr_shader_manager
{
public:
	static lvr_shader_manager* get_shader_mgr();
	static void release_shader_mgr();
public:
	~lvr_shader_manager();
public:
	//maybe I should remove this two from our project,since our program may never use this,is this true?I have no idea
	void add_compute_program_file(const char* a_name,const char* cs_file);
	void add_compute_program_str(const char* a_name,const char* cs_file);
	//if you give a_name a NULL ptr,it means you do not want manager to take care of the release work.
	lvr_program* add_program_from_file(const char* a_name,const char* a_vs_file,const char* a_fs_file,const char* a_gs_file = 0);
	lvr_program* add_program_from_str(const char* a_name,const char* a_va_str,const char* a_fs_str,const char* a_gs_str = 0);
	lvr_program* get_shader_program(const char* name_);
	void init_inside_programs();
	void release_all_programs();
	void release_managed_shader(const char* a_name);
	void release_shader(lvr_program*& ao_prog);
public:
	std::map<std::string,lvr_program*>	_shaders;
	bool								_need_init;
private:
	lvr_shader_manager():_need_init(true){}
};

#endif
