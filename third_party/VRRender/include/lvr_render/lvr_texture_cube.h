#ifndef __lvr_texture_cube_h__
#define __lvr_texture_cube_h__

#include "lvr_texture.h"

class LVR_RENDER_API lvr_texture_cube : public lvr_texture
{
public:
	lvr_texture_cube():lvr_texture(e_texture_type_cube){}
public:
	//usage 1:
	bool load_from_file(char* filename,char* datatype);
	//usage 2:
	bool create(int a_size, lvr_enum_texture_format a_tf);
	bool load_surface(const lvr_image_info& a_ii, lvr_enum_cube_face a_face);
	//usage 3:
	bool create(const lvr_image_info& a_ii);
private:
	lvr_enum_texture_format	_tf;
};

#endif
