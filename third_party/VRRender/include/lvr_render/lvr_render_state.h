#ifndef __lvr_render_state_h__
#define __lvr_render_state_h__

struct lvr_gpu_state
{
	lvr_gpu_state()
	{
		blendSrc = GL_ONE;
		blendDst = GL_ZERO;
		depthFunc = GL_LEQUAL;
		blendEnable = false;
		depthEnable = true;
		depthMaskEnable = true;
		polygonOffsetEnable = false;
	}

	bool Equals( const lvr_gpu_state & a )
	{
		return blendSrc == a.blendSrc
			&& blendDst == a.blendDst
			&& depthFunc == a.depthFunc
			&& blendEnable == a.blendEnable
			&& depthEnable == a.depthEnable
			&& depthMaskEnable == a.depthMaskEnable
			&& polygonOffsetEnable == a.polygonOffsetEnable;
	}

	GLenum	blendSrc;
	GLenum	blendDst;
	GLenum	depthFunc;
	bool	blendEnable;
	bool	depthEnable;
	bool	depthMaskEnable;
	bool	polygonOffsetEnable;
};

class lvr_render_state
{
public:
	lvr_render_state(){}
	~lvr_render_state(){}
};

#endif
