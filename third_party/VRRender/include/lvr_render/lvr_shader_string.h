#ifndef __lvr_shader_string_h__
#define __lvr_shader_string_h__

#include "lvr_render_configure.h"

extern const char* copyMovieVertexShaderSrc;
extern const char* copyMovieVertexShaderSrcRotating;
extern const char* copyMovieFragmentShaderSourceRotating;

extern const char* copyMovieFragmentShaderSource;

extern const char* movieUiVertexShaderSrc;

extern const char* movieExternalUiFragmentShaderSource;

extern const char* SceneStaticVertexShaderSrc;
extern const char* SceneStaticVertexShaderSrcFont;

extern const char* SceneDynamicVertexShaderSrc;

extern const char* SceneDynamicVertexShaderSrc_ES2;

extern const char* SceneBlackFragmentShaderSrc;

extern const char* SceneStaticFragmentShaderSrc;

extern const char* SceneStaticAndDynamicFragmentShaderSrc;

extern const char* SceneDynamicFragmentShaderSrc;

extern const char* SceneStaticAndDynamicFragmentShaderSrc_LOD;

extern const char* SceneDynamicFragmentShaderSrc_LOD;

extern const char* SceneStaticAndDynamicFragmentShaderSrc_SIM;

extern const char* SceneDynamicFragmentShaderSrc_SIM;

extern const char* SceneAdditiveFragmentShaderSrc;

extern const char * UniformColorVertexProgSrc;

extern const char * UniformColorFragmentProgSrc;

//////////////////////////////////////////////////////////////////////////
// It probably isn't worth keeping these shared here, each user
// should just duplicate them.
extern const char * externalFragmentShaderSource;
extern const char * textureFragmentShaderSource;
extern const char * identityVertexShaderSource;
extern const char * untexturedFragmentShaderSource;

extern const char * VertexColorVertexShaderSrc;
extern const char * VertexColorSkinned1VertexShaderSrc;
extern const char * VertexColorFragmentShaderSrc;

extern const char * SingleTextureVertexShaderSrc;
extern const char * SingleTextureVertexShaderSrcMovTexcoord;
extern const char * SingleTextureSkinned1VertexShaderSrc;
extern const char * SingleTextureSkinned2VertexShaderSrc;
extern const char * SingleTextureFragmentShaderSrc;
extern const char * SingleTextureFragmentShaderSrcWithColor;
extern const char * SingleTextureFragmentShaderSrcExternalOES;

extern const char * LightMappedVertexShaderSrc;
extern const char * LightMappedSkinned1VertexShaderSrc;
extern const char * LightMappedSkinned2VertexShaderSrc;
extern const char * LightMappedFragmentShaderSrc;

extern const char * ReflectionMappedVertexShaderSrc;
extern const char * ReflectionMappedSkinned1VertexShaderSrc;
extern const char * ReflectionMappedFragmentShaderSrc;

extern const char * screen_pos_vs;
extern const char * font_vs_2d;
extern const char * font_vs_3d;
extern const char * font_vs_2d_simple;
extern const char * font_vs_3d_simple;
extern const char * font_ps;
extern const char * font_ps_simple;

extern const char * color_multi_texture_ps;

extern const char * moving_tex_multi_vs;
extern const char * moving_tex_multi_ps;

extern const char * moving_tex_single_vs;

extern const char * sky_box_vertex_shader_src;
extern const char * sky_box_fragment_shader_src;

extern const char * simple_vs;
extern const char * simple_ps;

extern const char * alpha_map_vs;
extern const char * alpha_map_ps;

extern const char * alpha_tex_vs;
extern const char * alpha_tex_ps;

//ovrengine shader
extern const char * BlendShapeAnimVertexShaderSrc;
extern const char * BlendShapeAnimFragmentShaderSrc;
extern const char * StandardLightVertexShaderSrc;
extern const char * StandardLightFragmentShaderSrc;
extern const char * StandardLightNormalMapFragmentShaderSrc;


extern const char* gc_standard_vs;
extern const char* gc_standard_skin_vs;
extern const char* gc_standard_middle_ps;

extern const char* gc_shadow_ps_2d;
extern const char* gc_shadow_ps_cube_point;
#endif
