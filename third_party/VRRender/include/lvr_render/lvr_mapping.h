#ifndef __lvr_mapping_h__ 
#define __lvr_mapping_h__

#include "lvr_render_configure.h"

extern uint32_t g_texture_internal_format[e_texture_format_num];

extern uint32_t g_texture_format[e_texture_format_num];

extern uint32_t g_texture_target[e_texture_type_num];

#endif
