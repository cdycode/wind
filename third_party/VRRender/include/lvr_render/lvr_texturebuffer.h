#pragma once
#include "lvr_render_configure.h"
#include "lvr_texture.h"

class LVR_RENDER_API lvr_texturebuffer : public lvr_texture
{
public:
	lvr_texturebuffer() :lvr_texture(e_texture_type_buffer) {}
	~lvr_texturebuffer() {}

public:
	bool load_from_mem(const char* file_buf, uint32_t buf_len);
private:
	unsigned int _buf_id;
};