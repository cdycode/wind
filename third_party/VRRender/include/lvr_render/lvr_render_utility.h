#ifndef __lvr_render_utility_h__
#define __lvr_render_utility_h__

#include "lvr_render_configure.h"

extern void LVR_RENDER_API lvr_check_error(GLuint program_id_);
extern int LVR_RENDER_API lvr_check_opengl_error(const char * file, int line); 

#endif
