#ifndef __lvr_texture_manager_h__
#define __lvr_texture_manager_h__

#include "lvr_render_configure.h"
#include <map>
#include <string>
class lvr_texture;
class LVR_RENDER_API lvr_texture_manager
{
public:
	~lvr_texture_manager();
public:
	lvr_texture*	add_texture_from_file(char* filename,char* texname);
	lvr_texture*	add_texture_from_mem(const lvr_image_info& lii);
	bool		add_texture(const char* texname,lvr_texture* tex_);
	bool		release_texture(const char* texname);
	lvr_texture*	get_tex_by_name(const char* tex_name);
	lvr_texture*	get_defalut_texture();
public:
	static lvr_texture_manager* get_texture_manager();
	static void				release_texture_manager();
private:
	lvr_texture_manager(){}
public:
	std::map<std::string,lvr_texture*> _tex_map;
};

#endif
