#ifndef __lvr_index_buffer_h__
#define __lvr_index_buffer_h__
#include "lvr_hardware_buffer.h"

class LVR_RENDER_API lvr_index_buffer :public lvr_hardware_buffer
{
public:
	lvr_index_buffer();
	~lvr_index_buffer();
public:
	void set_index_buffer(const int8_t* a_indexs,unsigned int a_index_num,int a_per_index_size,lvr_enum_primitive_type a_prim_type);
	unsigned int get_index_type();
	unsigned int get_index_size();
	unsigned int get_prim_type();
	virtual void* lock(unsigned int mode_);
	virtual void unlock();
	virtual void update_data(int a_offset,int a_update_data_len,const void* a_datas);
	virtual void bind();
	virtual void unbind();
private:
	unsigned int	_index_size;
	unsigned int	_index_type;
	unsigned int	_primitive_type;
};

#endif
