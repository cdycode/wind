#ifndef __shader_h__
#define __shader_h__

#include "lvr_render_configure.h"

class LVR_RENDER_API lvr_shader : public lvr_ref
{
public:
	enum type
	{
		e_vertex_shader,
		e_pixel_shader,
		e_geometry_shader,
		e_compute_shader,
		e_tess_control_shader,
		e_tess_evaluation_shader
	};
public:
	lvr_shader();
	~lvr_shader();
public:
	unsigned int	load_shader_from_file(const char* file_name_,type type_);
	unsigned int	load_shader_from_str(const char* shader_str_,int str_len_,type type_);

	unsigned int	get_shader_id(){return _shader_id;}
private:
	void			load_file_to_str(const char* filename,char*& str_,int& str_len_);
private:
	char			_shader_name[64];
	unsigned int	_shader_id;
};

#endif
