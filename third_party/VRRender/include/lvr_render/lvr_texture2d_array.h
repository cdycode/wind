#ifndef __lvr_texture_2d_array_h__
#define __lvr_texture_2d_array_h__

#include "lvr_render_configure.h"
#include "lvr_texture.h"

class LVR_RENDER_API lvr_texture2d_array : public lvr_texture
{
public:
	lvr_texture2d_array():lvr_texture(e_texture_type_3d){}
	~lvr_texture2d_array(){}
public:
	//count may be 2 or 4 or 8,better use power of 2.so just use 4
	//usage 1
	bool create(int width,int height,int count,lvr_enum_texture_format tf_);
	//usage 2
	bool create(const lvr_image_info& a_ii);

	bool update_surface(const char* a_bufdata, int a_w, int a_h, lvr_enum_texture_format buf_format, uint32_t a_surface_id);
private:
	uint32_t		_surface_num;
	lvr_enum_texture_format	_surface_format;
};

#endif
