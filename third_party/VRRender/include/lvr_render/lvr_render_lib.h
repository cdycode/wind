#ifndef __lvr_render_h__
#define __lvr_render_h__

#include "lvr_render_configure.h"
#include "lvr_context.h"
#include "lvr_index_buffer.h"
#include "lvr_vertex_buffer.h"
#include "lvr_vertex_format.h"
#include "lvr_render_object.h"
#include "lvr_program.h"
#include "lvr_material.h"
#include "lvr_shader_manager.h"
#include "lvr_texture.h"
#include "lvr_texture_manager.h"
#include "lvr_render_texture.h"
#include "lvr_render_state.h"
#include "lvr_render_utility.h"

#endif
