#ifndef __lvr_render_need_h__
#define __lvr_render_need_h__

#include "lvr_gl_windows_define.h"
#include "lvr_gl_android_define.h"
#include "lvr_gl_ios_define.h"

#if LVR_GLSL_LEVEL_HIGH
#define  VR_DECL_POSITION " layout (location = 0) in " HIGHP " vec4 Position;\n"
#define  VR_DECL_UV0 "layout (location = 1) in " HIGHP " vec2 TexCoord;\n"
#define  VR_DECL_TANGENT "layout (location = 2) in " HIGHP " vec3 Tangent;\n"
#define  VR_DECL_UV1 "layout (location = 3) in " HIGHP " vec2 TexCoord1;\n"
#define  VR_DECL_VERTEX_COLOR "layout (location = 4) in " HIGHP " vec2 VertexColor;\n"
#define  VR_DECL_NORMAL 	" layout (location = 5) in " HIGHP " vec3 Normal;\n"
#define  VR_DECL_JOINT_INDICES "layout (location = 6) in " HIGHP " vec4 JointIndices;\n"
#define  VR_DECL_JOINT_WEIGHTS "layout (location = 7) in " HIGHP " vec4 JointWeights;\n"
#define  VR_DECL_BINORMAL "layout (location = 9) in " HIGHP " vec3 Binormal;\n"
#define  VR_DECL_IN " in "
#define  VR_DECL_OUT " out "
#else
#define  VR_DECL_POSITION "attribute " HIGHP " vec4 Position;\n"
#define  VR_DECL_UV0 "attribute " HIGHP " vec2 TexCoord;\n"
#define  VR_DECL_TANGENT "attribute " HIGHP " vec3 Tangent;\n"
#define  VR_DECL_UV1 "attribute " HIGHP " vec2 TexCoord1;\n"
#define  VR_DECL_VERTEX_COLOR "attribute " HIGHP " vec2 VertexColor;\n"
#define  VR_DECL_NORMAL "attribute " HIGHP " vec3 Normal;\n"
#define  VR_DECL_JOINT_INDICES "attribute " HIGHP " vec4 JointIndices;\n"
#define  VR_DECL_JOINT_WEIGHTS "attribute " HIGHP " vec4 JointWeights;\n"
#define  VR_DECL_BINORMAL "attribute " HIGHP " vec3 Binormal;\n"
#define  VR_DECL_IN " varying "
#define  VR_DECL_OUT " varying "
#endif

//these enum defines may not set here,maybe we need to make a new file.
enum lvr_enum_primitive_type
{
	e_primitive_triangle_list,
	e_primitive_triangle_fan,
	e_primitive_triangle_strip,
	e_primitive_line_list,
	e_primitive_line_strip,
	e_primitive_line_loop,
	e_primitive_points,
	e_primitive_support_num
};

extern unsigned int lvr_s_primtive_type_map[e_primitive_support_num];

enum lvr_enum_vertex_attribute_usage
{
	e_location_position		= 0,
	e_location_normal		= 5,
	e_location_tangent		= 2,
	e_location_binormal		= 9,
	e_location_uv1			= 3,
	e_location_color		= 4,
	e_location_uv0			= 1,
	e_location_joint_indices = 6,
	e_location_joint_weights = 7,
	e_max_attribute_num = 8,
	e_location_no_support
};

enum lvr_enum_vertex_attribute_type
{
	e_attribute_type_none	= 0,
	e_attrib_type_float1	= 1,
	e_attrib_type_float2	= 2,
	e_attrib_type_float3	= 3,
	e_attrib_type_float4	= 4,
	e_attrib_type_half1		= 5,
	e_attrib_type_half2		= 6,
	e_attrib_type_half3		= 7,
	e_attrib_type_half4		= 8,
	e_attrib_type_ubyte4	= 9,
	e_attrib_type_short1	= 10,
	e_attrib_type_short2	= 11,
	e_attrib_type_short4	= 12,
	e_attrib_type_int1		= 13,
	e_attrib_type_int2		= 14,
	e_attrib_type_int3		= 15,
	e_attrib_type_int4		= 16,
	e_vertex_attribute_num
};

enum lvr_enum_texture_type
{
	e_texture_type_none,
	e_texture_type_1d,
	e_texture_type_2d,
	e_texture_type_2d_array,
	e_texture_type_3d,
	e_texture_type_cube,
	e_texture_type_cube_array,
	e_texture_type_buffer,
#ifdef LVR_OS_ANDROID
	e_texture_type_external,
#endif
	e_texture_type_num
};

enum lvr_enum_texture_format
{
	// Small-bit color formats.
	e_texture_format_r5g6b5 = 1,
	e_texture_format_a1r5g5b5 = 2,
	e_texture_format_a4r4g4b4 = 3,

	// 8-bit integer formats.
	e_texture_format_a8 = 4,
	e_texture_format_l8 = 5,
	e_texture_format_a8l8 = 6,
	e_texture_format_r8g8b8 = 7,
	e_texture_format_a8r8g8b8 = 8,
	e_texture_format_a8b8g8r8 = 9,

	// 16-bit integer formats.
	e_texture_format_l16 = 10,
	e_texture_format_g16r16 = 11,
	e_texture_format_a16b16g16r16 = 12,

	// 16-bit floating-point formats ('half float' channels).
	e_texture_format_r16f = 13,
	e_texture_format_g16r16f = 14,
	e_texture_format_a16b16g16r16f = 15,

	// 32-bit floating-point formats ('float' channels).
	e_texture_format_r32f = 16,
	e_texture_format_g32r32f = 17,
	e_texture_format_a32b32g32r32f = 18,
	e_texture_format_d24s8 = 19,
	e_texture_format_num = 20,

	e_texture_format_none	= 0x00000,
	e_texture_format_R		= 0x00100,//R8
	e_texture_format_RGB	= 0x00200,
	e_texture_format_RGBA   = 0x00300,
	e_texture_format_ALPHA	= 0x00400,
	e_texture_format_R16 = 0x00500,
	// DXT compressed formats.
	e_texture_format_dxt1			= 0x01100,
	e_texture_format_dxt3			= 0x01200,
	e_texture_format_dxt5			= 0x01300,
	e_texture_format_PVR4bRGB		= 0x01400,
	e_texture_format_PVR4bRGBA      = 0x01500,
	e_texture_format_ATC_RGB        = 0x01600,
	e_texture_format_ATC_RGBA       = 0x01700,
	e_texture_format_ETC1			= 0x01800,
	e_texture_format_ETC2_RGB		= 0x01900,
	e_texture_format_ETC2_RGBA		= 0x01A00,
	e_texture_format_ASTC_4x4		= 0x01B00,	// single channel, 4x4 block encoded ASTC
	e_texture_format_ASTC_6x6		= 0x01C00,	// single channel, 6x6 block encoded ASTC

	// Depth-stencil format.
	e_texture_format_Depth           = 0x08000,
	e_texture_format_TypeMask        = 0x0ff00,
	e_texture_format_Compressed      = 0x01000,
	e_texture_format_SamplesMask     = 0x000ff,
	e_texture_format_RenderTarget    = 0x10000,
	e_texture_format_GenMipmaps      = 0x20000
};

enum lvr_enum_texture_usage
{
	e_texture_usage_diffuse = 0,
	e_texture_usage_illumination = 1,
	e_texture_usage_normal = 2,
	e_texture_usage_displacement = 3,
	e_texture_usage_ambient = 4,
	e_texture_usage_specular = 5,
	e_texture_usage_aux0 = 6,
	e_texture_usage_other = 7
};

enum lvr_enum_cube_face
{
	e_texture_cube_face_positive_x = 0,
	e_texture_cube_face_negative_x = 1,
	e_texture_cube_face_positive_y = 2,
	e_texture_cube_face_negative_y = 3,
	e_texture_cube_face_positive_z = 4,
	e_texture_cube_face_negative_z = 5,
	e_texture_cube_face_num = 6
};

static const int MAX_GEOMETRY_VERTICES	= 1 << 16 ;
static const int MAX_GEOMETRY_INDICES	= 1024 * 1024 * 3;
#ifdef LVR_OS_WIN32
static const int LVR_MAX_TEXTURE_BIND	= 16;
#else
static const int LVR_MAX_TEXTURE_BIND	= 8;
#endif

#endif
