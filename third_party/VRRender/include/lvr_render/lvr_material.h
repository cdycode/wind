#ifndef __lvr_material_h__
#define __lvr_material_h__

#include "lvr_render_configure.h"
#include "lvr_texture.h"
#include "lvr_program.h"
class LVR_RENDER_API lvr_color_channel
{
public:
	lvr_color_channel() : texture(NULL)
		, color(1.0f,1.0f,1.0f,1.0f){}
	~lvr_color_channel(){}
	lvr_texture* texture;
	lvr_vector4f color;
};
class LVR_RENDER_API lvr_material : public lvr_ref
{
public:
	lvr_material(){ 
		_diffuse_color.texture = NULL;
		_ambient_color.texture = NULL;
		_emissive_color.texture = NULL;
		_specular_color.texture = NULL;
		_specular_color.color = lvr_vector4f(lvr_random(0.0f, 1.0f), lvr_random(0.0f, 1.0f), lvr_random(0.0f, 1.0f), lvr_random(0.0f, 1.0f));
		_aux5.texture = NULL;
		_aux6.texture = NULL;
		_aux7.texture = NULL;
		_normal.texture = NULL;
		_shiness = 32.0f;
		_prog = NULL;
		_shadow_cast_prog = NULL;
	}
	~lvr_material();
public:
	void bind();
	void bindforshadow(bool a_cube_case = false);
	void unbind();
	lvr_color_channel	_diffuse_color;
	lvr_color_channel	_ambient_color;
	lvr_color_channel	_emissive_color;
	lvr_color_channel	_specular_color;
	lvr_color_channel	_normal;
	lvr_color_channel	_aux5;
	lvr_color_channel	_aux6;
	lvr_color_channel	_aux7;
	float			_shiness;
	lvr_program*	_prog;
	lvr_program*	_shadow_cast_prog;
	lvr_program*	_shadow_cast_prog_cube;
};

#endif
