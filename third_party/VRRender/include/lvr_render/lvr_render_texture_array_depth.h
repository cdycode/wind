#ifndef __lvr_render_texture_array_depth_h__20171211__
#define __lvr_render_texture_array_depth_h__20171211__

#include "lvr_render_configure.h"
#include "lvr_texture.h"

class LVR_RENDER_API lvr_render_texture_array_depth:public lvr_texture
{
public:
	lvr_render_texture_array_depth() :lvr_texture(e_texture_type_2d_array), _fbo_id(0), _depth_buffer(0)
		, _multisample_color_buffer(0), _resolve_frame_buffer(0), _old_fbo_id(0), _clear_color(0.0f, 0.0f, 0.0f, 0.0f)
	{
		//_tex_type = e_texture_type_2d;
	}
	~lvr_render_texture_array_depth();
public:
	void set_up(color_format a_col_format,texture_filter a_tex_filter,int a_width,int a_height,int a_depth_bits,int a_stencil_bits,int a_msaa,int layer_num);
	void set_clear_color(lvr_vector4f a_clear_color);
	void release_res();
	void enable_render(int id_pos);
	void disable_render();
	GLuint get_texture_id();
private:
	unsigned int	_fbo_id;
	unsigned int	_depth_buffer;
	unsigned int	_multisample_color_buffer;
	unsigned int	_resolve_frame_buffer;
	int				_old_fbo_id;
	lvr_vector4f	_clear_color;
};

#endif
