#ifndef lvr_deferred_work_h__20171206__
#define lvr_deferred_work_h__20171206__

#include "lvr_render_configure.h"

class LVR_RENDER_API lvr_mrt_work
{
public:
	lvr_mrt_work();
	~lvr_mrt_work();
public:
	void setup(int w,int h,int a_stencil_bits);
	void uninit();
	void enable_mrt_render();
	void end_mrt_render();
	void bind_textures(int id,int slot);
	void bind_all_texture();
	int  get_depth_buf();
	unsigned int* get_texture_ids();
public:
	unsigned int	_fbo_id;
	unsigned int	_depth_buffer;
	unsigned int	_textures[4];
	int				_width;
	int				_height;
	int				_old_fbo_id;
};

#endif