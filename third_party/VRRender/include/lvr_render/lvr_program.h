#ifndef __lvr_program_h__
#define __lvr_program_h__

#include "lvr_render_configure.h"
#include "lvr_shader.h"
#include <map>
#include <string>

class LVR_RENDER_API lvr_program :public lvr_ref
{

public:
	lvr_program();
	~lvr_program();
public:
	void set_shaders(lvr_shader* a_vs,lvr_shader* a_ps,lvr_shader* a_gs = NULL);
	void set_shaders(lvr_shader** a_s,int num);
	void bind()const;
	void unbind()const;

	void bind_texture2d(const char *name, int unit, uint32_t tex)const;
	void bind_texture2d(int index, int unit, uint32_t tex)const;

	void bind_texture_array(const char *name, int unit, uint32_t tex)const;
	void bind_texture_array(int index, int unit, uint32_t tex)const;

	void set_uniform1i(const char *name, int value)const;
	void set_uniform1f(const char *name, float value)const;
	
	void set_uniform2i(const char *name, int x, int y)const;
	void set_uniform2f(const char *name, float x, float y)const;

	void set_uniform3i(const char *name, int x, int y, int z)const;
	void set_uniform3f(const char *name, float x, float y, float z)const;

	void set_uniform4i(const char *name, int x, int y, int z,int w)const;
	void set_uniform4f(const char *name, float x, float y, float z, float w)const;

	void set_uniform1iv(int index, const int *value,int count = 1)const;
	void set_uniform2iv(int index, const int *value,int count = 1)const;
	void set_uniform3iv(int index, const int *value, int count= 1)const;
	void set_uniform4iv(int index, const int *value, int count= 1)const;

	void set_uniform1fv(const char *name, const float* value,int count = 1)const;
	void set_uniform2fv(const char *name, const float* value,int count = 1)const;
	void set_uniform3fv(const char *name, const float *value, int count=1)const;
	void set_uniform4fv(const char *name, const float *value, int count=1)const;

	void set_uniform1i(int index, int value)const;
	void set_uniform1f(int index, float value)const;

	void set_uniform2i(int index, int x, int y)const;
	void set_uniform2f(int index, float x, float y)const;

	void set_uniform3i(int index, int x, int y, int z)const;
	void set_uniform3f(int index, float x, float y, float z)const;

	void set_uniform4i(int index, int x, int y, int z, int w)const;
	void set_uniform4f(int index, float x, float y, float z, float w)const;

	void set_uniform1fv(int index, const float *value, int count=1)const;
	void set_uniform2fv(int index, const float *value, int count=1)const;
	void set_uniform3fv(int index, const float *value, int count=1)const;
	void set_uniform4fv(int index, const float *value, int count=1)const;

	void set_uniform_matrix3fv(const char *name, const float *m, int count = 1, bool transpose = false)const;
	void set_uniform_matrix3fv(const int index, const float *m, int count = 1, bool transpose = false)const;

	void set_uniform_matrix4fv(const char *name,const float *m, int count=1, bool transpose=false)const;
	void set_uniform_matrix4fv(const int index,const float *m, int count=1, bool transpose=false)const;

	int get_attrib_location(const char* attribute, bool isOptional = false)const;

	int get_uniform_location(const char* uniform, bool isOptional = false)const;

	inline uint32_t get_program_id()const { return _program_id; }

	/// Relinks an existing shader program to update based on external changes
	bool relink();
private:
	void bind_default_attributes()const;
	void get_basic_uniforms();
private:
	lvr_shader*	_vs;
	lvr_shader*	_ps;
	lvr_shader*	_gs;
public:
	//to speed up,here we make serval uniform public
	unsigned int _program_id;
	// Uniforms that aren't found will have a -1 value
	int		u_mvp_mat;				// uniform u_mvp_mat
	int		u_model_mat;				// uniform u_model_mat
	int		u_view_mat;				// uniform u_view_mat
	int		u_proj_mat;		// uniform u_proj_mat
	int		u_color;				// uniform u_color
	int		u_tex_mat0;				// uniform u_tex_mat0
	int		u_tex_mat1;				// uniform u_tex_mat1
	int		u_joint_num;			// uniform u_joint_num
	int		u_direction_light_color_intensity;
	int		u_direction_light_dir;
	int		u_direction_light_vp_mat;
	int		u_env_color_intensity;
	int		u_point_light_color_intensity;
	int		u_point_light_position_radius;
	int		u_point_light_radius_for_shadow;
	//since we use cube for point light now.point light vp mat is of no use.
	int		u_point_light_vp_mat;
	//since we only support at most one point light,we can ignore it now.
	int		u_point_light_num;
	int		u_spot_light_color_intensity;
	int		u_spot_light_position_exponent;
	int		u_spot_light_dir_cutoff;
	int		u_spot_light_vp_mat;
	int		u_shininess;
	int		u_material_diffuse_color;
	int		u_material_specular_color;
	//three int,order: directional,spot,point
	int		u_near_far_plane;
	int		u_light_nums;
	int		u_eye_pos;
	//support time about floats
	int		u_offsets;
	//fog
	int		u_fog_color;
	int		u_fog_parm;
};

#endif
