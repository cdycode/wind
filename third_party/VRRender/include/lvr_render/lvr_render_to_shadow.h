#ifndef __lvr_render_to_shadow_h__20171219__
#define __lvr_render_to_shadow_h__20171219__

#include "lvr_render_configure.h"
#include "lvr_texture.h"

class LVR_RENDER_API lvr_render_to_shadow : public lvr_texture
{
public:
	lvr_render_to_shadow() :lvr_texture(e_texture_type_2d), _fbo_id(0), _old_fbo_id(0)
	{
		//_tex_type = e_texture_type_2d;
	}
	~lvr_render_to_shadow();
public:
	void set_up(color_format a_col_format,texture_filter a_tex_filter,int a_width,int a_height,int a_depth_bits,int a_stencil_bits,int a_msaa);
	void release_res();
	void enable_render();
	void disable_render();
private:
	unsigned int	_fbo_id;
	int				_old_fbo_id;
};

#endif
