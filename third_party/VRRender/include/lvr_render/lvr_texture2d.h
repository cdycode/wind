#ifndef __lvr_texture_2d_h__
#define __lvr_texture_2d_h__

#include "lvr_render_configure.h"
#include "lvr_texture.h"

class LVR_RENDER_API lvr_texture2d : public lvr_texture
{
public:
	lvr_texture2d():lvr_texture(e_texture_type_2d){}
	~lvr_texture2d(){}
public:
	//usage 1:
	bool load_from_file(char* filename,char* datatype);
	bool load_from_mem(const char* file_buf,uint32_t buf_len,const char* data_type);
	//usage 2:
	bool create(int width,int height,lvr_enum_texture_format tf_);
	bool create(const lvr_image_info& a_ii);
	bool update(int a_xpos, int a_ypos, int a_w, int a_h,const char* a_datas,uint32_t a_data_size,lvr_enum_texture_format a_tf);
	void* lock(int level,GLuint lockmode);
	void unlock(int level);
private:
	bool load_dds_mem(const char* buf,uint32_t buf_len);
	bool load_bmp_mem(const char* buf,uint32_t buf_len);
	bool load_dds(char* filename);
	bool load_bmp(char* filename);
private:
	lvr_enum_texture_format	_tf;
};

#endif
