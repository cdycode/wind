#ifndef _LVR_SILHOUETTE_BASE_H_
#define _LVR_SILHOUETTE_BASE_H_
#include "lvr_math_lib.h"
class lvr_silhouette_base
{
public:
	lvr_silhouette_base() :_pos(0), _mLength(0), _visible(false){}
	lvr_silhouette_base(const lvr_vector3f& a_pos, const lvr_vector3f & a_right, const lvr_vector3f & a_up)
		:_pos(a_pos),
		_mRight(a_right),
		_mUp(a_up){}
	virtual ~lvr_silhouette_base(){}
public:
	virtual void set_pos(const lvr_vector3f &a_pos);
	virtual const lvr_vector3f& get_pos()const;
	virtual void set_state(bool a_visible);
	virtual bool get_state()const;
	virtual void set_outline_length(float a_length);
	virtual void set_Id(uint16_t a_id);
	virtual uint16_t get_Id()const;
	virtual void set_orientation(const lvr_vector3f & a_right,const lvr_vector3f & a_up);
	virtual const lvr_vector3f& get_right()const;
	virtual const lvr_vector3f& get_up()const;
	virtual void set_size(const lvr_vector2f &a_size);
	virtual const lvr_vector2f& get_size()const;
	virtual float get_outline_length() const;
protected:
	lvr_vector2f _mSize;
	lvr_vector3f _pos;
	lvr_vector3f _mRight;
	lvr_vector3f _mUp;
	float _mLength;
	uint16_t    _mId;
	bool _visible;
};
#endif