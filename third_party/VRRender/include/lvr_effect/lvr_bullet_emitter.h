#ifndef LVR_BULLET_EMITTER
#define LVR_BULLET_EMITTER
#include "lvr_bullet_emitter_base.h"
#include <vector>
#include "lvr_primitive_rect.h"
#include "lvr_primitive_sphere3.h"
class lvr_render_object;
static const float  rrr = 6.0f;
class lvr_bullet_emitter:public lvr_bullet_emitter_base
{
public:
	lvr_bullet_emitter();
	~lvr_bullet_emitter();
public:
	void setUpEmitter();
	void destroyEmitter();   // when one movie is over 
public:
	void setFlyingOrientation(const lvr_vector3f & a_flying_oritation);
	void setFlyingOrientation(const float  a_flying_rad);
	void setFlyingSpeed(float a_speed);
	void setPlaylist(std::vector<lvr_bullet_screen> & a_bul_list);
	void setMovieBeginTime(double a_time);
	void update(double curr_time,const lvr_vector3f &a_camera_pos, const lvr_vector3f& a_camero_dir);
	void draw(const lvr_matrix4f& a_vp);
private:
	lvr_primitive_rect tmpRect;
	lvr_primitive_sphere3 tmpSphere;
	lvr_texture* _mTex;
	lvr_render_object* _mRO;
	lvr_program* _mProg;
};
#endif // !LVR_BULLET_EMITTER
