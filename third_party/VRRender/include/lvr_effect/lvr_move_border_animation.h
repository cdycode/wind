#ifndef __lvr_move_border_animation_HH__
#define __lvr_move_border_animation_HH__


#include <vector>
#include <string>
#include "lvr_vector4.h"
#include "lvr_singleton.h"
#include "lvr_matrix4.h"

class lvr_ui_2d;
class lvr_program;
class lvr_index_buffer;
class lvr_vertex_format;
class lvr_vertex_buffer;
class lvr_texture;

#include "lvr_effect_configure.h"

class LVR_EFFECT_API  lvr_move_border_animation {
public:
	lvr_move_border_animation();
	virtual ~lvr_move_border_animation();

	static lvr_move_border_animation* get_inst(void);
	static void release_ins(void);

	void init();
	int  add_animation(lvr_ui_2d* ui, float radius, const std::string& tex, double duration);
	bool remove_animation(int id);
	bool update(double curr_time);
	void draw(const lvr_matrix4f& a_vp_mat);

	bool start(int id);
	bool pause(int id);
	bool resume(int id);
	double get_take_time_s(int id);

private:
	enum animation_status{
		HIDE,
		SHOW,
		RUN,
		PAUSE,
	};

	struct animation_node{
		int					_id;
		lvr_ui_2d*			_ui;
		animation_status	_status;
		lvr_vector4f		_texture_offset;
		float				_radius;
		double				_duration;
		double				_start_time;
		lvr_vertex_buffer*		_vb;
		lvr_texture*			_texture;
	};

	static	int				_gen_node_id;
	lvr_texture*			_texture;
	lvr_index_buffer*		_ib;
	lvr_vertex_format*		_vf;
	lvr_program*			_prog;
	double					_last_update_time;

	std::vector<animation_node*>	_vec_animations;

	static lvr_move_border_animation* _sg_singleton_ptr;
};

extern lvr_move_border_animation* get_lvr_move_border_animation_ins();
extern lvr_move_border_animation* g_lvr_move_border_animation_ptr;;


#endif