#ifndef _LVR_SILHOUETTE_MANAGER_H_
#define _LVR_SILHOUETTE_MANAGER_H_
#include "lvr_silhouette_base.h"
#include "lvr_silhouette_rect.h"
#include "lvr_silhouette_round.h"
#include "lvr_singleton.h"
#include<vector>
#include "lvr_matrix4.h"
#include "lvr_render_lib.h"
#include "lvr_ui_2d.h"
class lvr_texture;

#include "lvr_effect_configure.h"

class LVR_EFFECT_API  lvr_silhouette_manager :public lvr_singleton<lvr_silhouette_manager>
{
public:
	lvr_silhouette_manager() :_vb(NULL), _ib(NULL), _vf(NULL), _mProg(NULL), _mTex(NULL), _is_need_update(false){}
	~lvr_silhouette_manager(){}
	void init(int max_num =2 );
	void update(lvr_ui_2d* a_base);
	void draw(const lvr_matrix4f & vp);
	void uinit();
private:
	void generate_render_buffer();
	void update_vertex(int sil_id, lvr_ui_2d::ui_vertex* a_vertexs, uint32_t& ao_cur_offset);
public:
	void add_silhouette(lvr_ui_2d *a_base);
	bool remove_silhouette(lvr_ui_2d *a_base);
private:

	std::vector<lvr_silhouette_base*>   _mSilhouettes;
	lvr_vertex_buffer*  _vb;
	lvr_index_buffer*  _ib;
	lvr_vertex_format* _vf;
	lvr_program*  _mProg;
	lvr_texture*  _mTex;
	lvr_silhouette_base* _currRect;
	int _mMaxRectNum;
	unsigned int _SilhouetteSize;
	bool _is_need_update;
public:
	static int g_breakpoint;
};
#endif