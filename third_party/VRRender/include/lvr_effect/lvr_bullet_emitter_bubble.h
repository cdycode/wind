#ifndef LVR_BULLET_EMITTER_BUBBLE
#define LVR_BULLET_EMITTER_BUBBLE
#define LVR_BULLET_SCREEN_ON  1
#include "lvr_bullet_emitter_base.h"
class lvr_render_object;
class lvr_program;
class lvr_texture;
class lvr_primitive_rect;
class lvr_bullet_emitter_bubble:public lvr_bullet_emitter_base
{
public:
	lvr_bullet_emitter_bubble();
	~lvr_bullet_emitter_bubble();
public:
	virtual void setUpEmitter();
	virtual void destroyEmitter();
public:
	virtual void draw(const lvr_matrix4f& a_vp);
	virtual void update(double curr_time, const lvr_vector3f &a_camera_pos, const lvr_vector3f& a_camero_dir);
private:
	lvr_primitive_rect *    _mBlockRect;
	lvr_vector3f            _mBlockRectPos;
	lvr_vector2f            _mBlockRectSize;
	lvr_texture* _mTex;
	lvr_render_object* _mRO;
	lvr_program* _mProg;

};
#endif