#ifndef LVR_BULLET_SCREEN_MANAGER
#define LVR_BULLET_SCREEN_MANAGER
#include "lvr_singleton.h"
#include "lvr_bullet_emitter.h"
#include "lvr_bullet_emitter_bubble.h"
#include<unordered_map>
class lvr_ui_manager;
class lvr_bullet_emitter;
class lvr_bullet_screen_manager:public lvr_singleton<lvr_bullet_screen_manager>
{
public:
	void init(int  a_width,int a_height);   //daiwancheng
	void uinit();
	void addBulletScreen(int a_movie_id, const lvr_bullet_screen &a_bullet_screen);  //弹幕初始位置计算交给外面，因此这里就不需要传入相机射线了;
	void addToUiMgr(lvr_ui_manager* a_mgr);
	void EmitBullet(int a_movie_id);
	void update(double a_curr_time,const lvr_vector3f &a_camera_pos,const lvr_vector3f& a_camero_dir);
	void draw(const lvr_matrix4f & a_vp);
private:
	lvr_bullet_emitter_bubble  _mEmitter;
	std::unordered_map<int, std::vector<lvr_bullet_screen>>   _mAllPlayList;
	int _mScreenWidth;
	int _mScreenHeight;
};
#endif