#ifndef _LVR_SILHOUETTE_RECT_H_
#define _LVR_SILHOUETTE_RECT_H_
#include "lvr_silhouette_base.h"
class lvr_vertex_buffer;
class lvr_index_buffer;
class lvr_vertex_format;
class lvr_silhouette_rect :public lvr_silhouette_base
{
public: 
	lvr_silhouette_rect() :lvr_silhouette_base(){}
	lvr_silhouette_rect(const lvr_vector3f& a_pos, const lvr_vector3f & a_right, const lvr_vector3f & a_up) 
		:lvr_silhouette_base(a_pos, a_right, a_up){}
	virtual~lvr_silhouette_rect();
public:
	virtual void set_pos(const lvr_vector3f &a_pos);
	virtual void set_state(bool a_visible);
	virtual bool get_state();
	virtual void set_orientation(const lvr_vector3f & a_right, const lvr_vector3f & a_up);
public:
	void set_rect_info(lvr_vector2f a_size); // 0- right,1 - up 
private:
	lvr_vector3f   _mLeftDownCorner;
	lvr_vector3f   _mRightUpCorner;

};
#endif