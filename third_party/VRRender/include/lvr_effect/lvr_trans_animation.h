#ifndef _lvr_trans_animation__H___
#define _lvr_trans_animation__H___

#include "lvr_singleton.h"
#include "lvr_math_lib.h"
#include "lvr_ui_2d.h"
#include "lvr_ui_text.h"

#include "lvr_effect_configure.h"

class LVR_EFFECT_API lvr_trans_animation :public lvr_singleton<lvr_trans_animation>{

	struct trans_animation{
		bool	is_ui_2d;
		lvr_ui_2d* ui;
		lvr_ui_text* ui_text;
		lvr_vector3f src_pos;
		lvr_vector3f dst_pos;
		lvr_vector2f src_size;
		lvr_vector2f dst_size;
		float src_font_size;
		float dst_font_size;
		float duration;
		float start_time;
		bool   end_reset;
		lvr_ui_2d*	back_ui;
	};

public:
	lvr_trans_animation();
	~lvr_trans_animation();
	void init();
	void uninit();

	void update(float curr_time);
	bool add_animathion(lvr_ui_2d* ui, const lvr_vector3f& src_pos, const lvr_vector3f& dst_pos, const lvr_vector2f& src_size, const lvr_vector2f& dst_size, const float duration, bool end_reset = false, lvr_ui_2d* back_ui = NULL);
	bool add_animathion_text(lvr_ui_text* ui, const lvr_vector3f& src_pos, const lvr_vector3f& dst_pos, const float src_font_size, const float dst_font_size, const float duration, bool end_reset = false);
	bool remove_animation(lvr_ui_base* ui);

	void set_back_ui(lvr_ui_2d* ui);

private:
	float							_last_update_time;
	std::vector<trans_animation*>	_vec_animations;
	lvr_ui_2d*						_back_ui;
};



#endif