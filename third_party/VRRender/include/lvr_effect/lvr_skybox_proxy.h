#ifndef _LVR_SKYBOX_EFFECT
#include "lvr_matrix4.h"
#include <string>
#include "lvr_singleton.h"
class lvr_sky_box_cube;

#include "lvr_effect_configure.h"

class LVR_EFFECT_API  lvr_skybox_effect //:public lvr_singleton<lvr_skybox_effect>
{
public:
	static lvr_skybox_effect* get_ins();
	static void release_ins();
private:
	static lvr_skybox_effect*	_singleptr;

public:
	lvr_skybox_effect() :_mRadius(200.0f), _mVisbile(true), _mSkyBox(NULL){}
	~lvr_skybox_effect();
public:
	void init(std::string & a_path, float a_radius = 200);
	void uinit();
	void set_skybox(std::string &a_path);
	const std::string get_skybox_path();
	void set_skybox_radius(float a_radius);
	void set_visible(bool a_visible);
	bool update(std::string  & a_path);
	void draw(const lvr_matrix4f& a_vp);
private:
	float _mRadius;
	bool _mVisbile;
	lvr_sky_box_cube*  _mSkyBox;
	std::string _mPath;

	
};
#endif