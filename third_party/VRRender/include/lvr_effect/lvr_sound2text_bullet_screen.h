#ifndef LVR_SOUND2TEXT_BULLET_SCREEN
#define LVR_SOUND2TEXT_BULLET_SCREEN
#include <string>
#include "lvr_vector2.h"
#include "lvr_vector3.h"
#include "lvr_vector4.h"
struct  bullet_info
{
	std::string mBulletText;
	lvr_vector4uc mColor;
	lvr_vector2f  mSize;
	lvr_vector3f  mBeginPos;
	double mBeginTime;              //这个时间记录的是相对于视频开始时间的延迟;
};
class lvr_ui_text;
class lvr_bullet_screen
{
public:
	lvr_bullet_screen();
	~lvr_bullet_screen();
public:
	void init( const std::string & aBulletText,double aBeginTime,const lvr_vector3f& aBeginPos);
	void init(const bullet_info& a_info);
	void unit();
	lvr_ui_text* getBulletUiText();
	const std::string & getBulletTextString();
	void setBulletTextString(const std::string & a_text);
	double getBulletBeginTime();
	void setBluttetBeginTime(double a_time);
	const lvr_vector3f &getBulletPos();
	void setBulletPos(const  lvr_vector3f& a_pos);

private:
	lvr_ui_text*  _mBulletUi;
	bullet_info   _mBulletInfo;
};
#endif