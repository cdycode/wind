#ifndef __lvr_effect_configure_h__
#define __lvr_effect_configure_h__

#include "lvr_core_lib.h"

#ifdef lvr_effect_EXPORTS
#define LVR_EFFECT_API LVR_API_EXPORT
#elif defined lvr_effect_IMPORT
#define LVR_EFFECT_API LVR_API_IMPORT
#else
#define LVR_EFFECT_API LVR_API_STATIC
#endif

#endif
