#ifndef LVR_BULLET_EMITTER_BASE
#define LVR_BULLET_EMITTER_BASE
#include "lvr_ui_menu.h"
#include "lvr_sound2text_bullet_screen.h"
class lvr_bullet_emitter_base :public lvr_ui_menu
{
public:
	lvr_bullet_emitter_base();
	virtual ~lvr_bullet_emitter_base(){}
public:
	virtual void setUpEmitter();
	virtual void destroyEmitter();
public:
	virtual void setFlyingOrientation(const lvr_vector3f & a_flying_oritation);
	virtual void setFlyingOrientation(const float  a_flying_rad);
	virtual void setFlyingSpeed(float a_speed);
	virtual void setPlaylist(std::vector<lvr_bullet_screen> & a_bul_list);
	virtual void setMovieBeginTime(double a_time);
	virtual void update(double curr_time, const lvr_vector3f &a_camera_pos, const lvr_vector3f& a_camero_dir);
	virtual void draw(const lvr_matrix4f& a_vp){}
public:
	const int NUM_OF_BULLETS = 10;
	double _mTransparency;
	double _mMovieBeginTime;
	double _mCurrBulletScreenEmitTime;
	float  _mFlyingSpeed;
	float  _mFlyingRad;
	lvr_vector3f _mFlyingOrientation;
	std::vector<lvr_bullet_screen>  _mBulletScreens;

};
#endif