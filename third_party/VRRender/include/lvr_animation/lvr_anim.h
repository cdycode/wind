#ifndef __lvr_anim_base_h__20170406__
#define __lvr_anim_base_h__20170406__

#include "lvr_anim_configure.h"
#include "lvr_ref.h"

typedef void (*anim_end_callback)(void*);
class LVR_ANIM_API lvr_anim : public lvr_ref
{
public:
	lvr_anim();
	virtual ~lvr_anim();
public:
	virtual void set_anim_end_call_back(anim_end_callback a_end_func);
	virtual void begin(float a_time);
	virtual void stop();
	virtual void update(float t_delta_time);
	virtual bool is_active();
protected:
	anim_end_callback	_end_call_back;
	bool	_active;
};


#endif
