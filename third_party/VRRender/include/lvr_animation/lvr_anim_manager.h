#ifndef __lvr_anim_manager_h__20170612__
#define __lvr_anim_manager_h__20170612__

#include "lvr_anim.h"
#include <string>
#include <map>

class LVR_ANIM_API lvr_anim_manager
{
public:
	lvr_anim_manager();
	~lvr_anim_manager();
public:
	bool add_animation(const char* a_name,lvr_anim* a_anim);
	bool removie_animation(const char* a_name);
	void update(float a_time);
public:
	float	_last_time;
	std::map<std::string,lvr_anim*>	_anim_map;
};

#endif
