#ifndef __lvr_anim_configure_h__20170406__
#define __lvr_anim_configure_h__20170406__


#include "lvr_core_lib.h"
#ifdef LVR_OS_WIN32
#define lvr_animation_IMPORT
#endif

#ifdef lvr_animation_EXPORTS
#define LVR_ANIM_API LVR_API_EXPORT
#elif defined lvr_animation_IMPORT
#define LVR_ANIM_API LVR_API_IMPORT
#else
#define LVR_ANIM_API LVR_API_STATIC
#endif


#endif
