#ifndef __lvr_primitive_sphere3_h__
#define __lvr_primitive_sphere3_h__

#include "lvr_primitive_data.h"

class lvr_primitive_sphere3
	:public lvr_primitive_data
{
public:
	lvr_primitive_sphere3();
	~lvr_primitive_sphere3();
public:
	void	set_pos(const lvr_vector3f& a_pos);
	void	set_radius(float a_r);
	void	set_segments(int a_seg);
	void	set_rings(int a_ring);
	virtual bool	update();
private:
	float	_radius;
	int		_seg_num;
	int		_ring_num;
	lvr_vector3f	_pos;
};

#endif
