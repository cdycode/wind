#ifndef __lvr_primitive_data_base_h__
#define __lvr_primitive_data_base_h__

#include "lvr_standard_primitives_configure.h"

//cw front orientation make
class lvr_primitive_data_base
{
public:
	struct vertex_fromat
	{
		lvr_vector3f pos;
		lvr_vector3f normal;
		//lvr_vector3f tangle;
		lvr_vector2f uv;
	};
public:
	lvr_primitive_data_base(){}
	virtual ~lvr_primitive_data_base(){}
public:
	virtual const vertex_fromat* get_data() = 0;
	virtual const uint16_t* get_tri_index() = 0;
	virtual void flip_normal() = 0;
	virtual void flip_orientation() = 0;
	virtual int32_t get_tri_num() = 0;
	virtual int32_t get_vertex_num() = 0;
	virtual bool	update() = 0;
	virtual void	set_pos(const lvr_vector3f& a_pos) = 0;
	virtual bool	is_triangle_strip_supported() = 0;
};

#endif
