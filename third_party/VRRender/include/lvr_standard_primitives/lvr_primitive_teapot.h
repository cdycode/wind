#ifndef __lvr_primitive_teapot_h__
#define __lvr_primitive_teapot_h__

#include "lvr_primitive_data.h"

class lvr_primitive_teapot
	:public lvr_primitive_data
{
public:
	lvr_primitive_teapot();
	~lvr_primitive_teapot();
public:
	void	set_pos(const lvr_vector3f& a_pos);
	void	set_size(float a_size);
	void	set_grid(int a_grid);
	virtual bool	update();
private:
	void generatePatches(float * v, float * n, float * tc, unsigned int* el, int grid);
	void buildPatchReflect(int patchNum, float *B, float *dB, float *v, float *n, float *tc, unsigned int *el, int &index, int &elIndex, int &tcIndex, int grid, bool reflectX, bool reflectY);
	void buildPatch(lvr_vector3f patch[][4], float *B, float *dB, float *v, float *n, float *tc, unsigned int *el, int &index, int &elIndex, int &tcIndex, int grid, lvr_matrix3f reflect, bool invertNormal);
	void getPatch(int patchNum, lvr_vector3f patch[][4], bool reverseV);
	void computeBasisFunctions(float * B, float * dB, int grid);
	lvr_vector3f evaluate(int gridU, int gridV, float *B, lvr_vector3f patch[][4]);
	lvr_vector3f evaluateNormal(int gridU, int gridV, float *B, float *dB, lvr_vector3f patch[][4]);
private:
	float	_radius;
	int		_grid_num;
	lvr_vector3f	_pos;
	float			_size;
};

#endif
