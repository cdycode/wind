#ifndef __lvr_primitive_geosphere_h__20161122__
#define __lvr_primitive_geosphere_h__20161122__

#include "lvr_primitive_data.h"
//its uv generate is not so easy,and do not have a good value set.so just do not use it uv info.
class lvr_primitive_geosphere :public lvr_primitive_data
{
public:
	lvr_primitive_geosphere();
	~lvr_primitive_geosphere();
public:
	void	set_pos(const lvr_vector3f& a_pos);
	void	set_radius(float a_r);
	void	set_level(int a_level);//1-5
	bool	update();
private:
	float	_radius;
	int		_level;
	lvr_vector3f	_pos;
};


#endif
