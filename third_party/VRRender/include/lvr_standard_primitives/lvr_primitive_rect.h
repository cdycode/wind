#ifndef __lvr_primitive_rect_h__
#define __lvr_primitive_rect_h__

#include "lvr_primitive_data.h"

class lvr_primitive_rect
	:public lvr_primitive_data
{
public:
	lvr_primitive_rect();
	~lvr_primitive_rect(){}
public:
	void set_width(float a_width);
	void set_height(float a_height);
	void set_width_segs(int a_w_seg);
	void set_height_segs(int a_h_seg);
	void set_uv_warp(float u,float v);
	void set_coord_axis(const lvr_vector3f& a_x,const lvr_vector3f& a_y);
	virtual bool	update();
private:
	float			_w;
	float			_h;
	int				_width_seg_num;
	int				_height_seg_num;
	float			_u_warp, _v_warp;
	lvr_vector3f	_xv;
	lvr_vector3f	_yv;
};


#endif