#ifndef __lvr_primitive_round_square2_h__
#define __lvr_primitive_round_square2_h__

#include "lvr_primitive_data.h"

//you can also only use positions to get triangle_strip
class lvr_primitive_round_square2
	:public lvr_primitive_data
{
public:
	lvr_primitive_round_square2();
	~lvr_primitive_round_square2();
public:
	//do not make w smaller than h,otherwise you should give a reasonable ratio.a number smaller than 1.0
	void set_rect_size(float a_w,float a_h);
	// thickness should not bigger than half w neither half h.
	void set_frame_thickness(float a_thick);
	void set_round_ratio(float a_ratio);//between 0-positive max,suggest 0.2-2.0
	void set_plane(const lvr_vector3f& a_v0,const lvr_vector3f& a_v1);
	void set_center_position(const lvr_vector3f& a_center);
	virtual bool	update();
private:
	float			_w;
	float			_h;
	float			_ratio;
	float			_thickness;
	lvr_vector3f	_xv;
	lvr_vector3f	_yv;
};

#endif
