#ifndef __lvr_primitive_box_h__
#define __lvr_primitive_box_h__

#include "lvr_primitive_data.h"

class lvr_primitive_box
	:public lvr_primitive_data
{
public:
	lvr_primitive_box(){}
	~lvr_primitive_box(){}
public:
 	void set_depth(float a_len);
 	void set_width(float a_width);
 	void set_height(float a_height);
 	//void set_length_segs();
 	//void set_width_segs();
 	//void set_height_segs();
	virtual bool	update();
private:
	lvr_vector3f	_size;
	int				_length_seg_num;
	int				_width_seg_num;
	int				_height_seg_num;
};

#endif
