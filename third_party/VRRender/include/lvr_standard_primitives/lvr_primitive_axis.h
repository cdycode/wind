#ifndef __lvr_primitive_axis_h__
#define __lvr_primitive_axis_h__

#include "lvr_primitive_data.h"

class lvr_axis : public lvr_primitive_data
{
public:
	lvr_axis(){}
	~lvr_axis(){}
public:
	void set_up(int seg,int circle_num,float a_scale_xz,float breakpoint_,float outline_size_,float a_size = 1.0f);
	bool update();
private:
	int		_segment;
	int		_circle_num;
	float	_scale_xz;
	float	_size;
	float	_break_point;
	float	_outline_size;

};

#endif
