#ifndef __lvr_primitive_tube2_h__
#define __lvr_primitive_tube2_h__

#include "lvr_primitive_data.h"

//you can also only use positions to get triangle_strip
class lvr_primitive_tube2
	:public lvr_primitive_data
{
public:
	lvr_primitive_tube2();
	~lvr_primitive_tube2();
public:
	void set_sides(int a_sides);
	void set_radius(float a_inner_r,float a_outer_r);
	void set_plane(const lvr_vector3f& a_v0,const lvr_vector3f& a_v1);
	virtual bool	update();
private:
	float			_r[2];
	float			_h;
	int				_sides;
	lvr_vector3f	_xv;
	lvr_vector3f	_yv;
};

#endif
