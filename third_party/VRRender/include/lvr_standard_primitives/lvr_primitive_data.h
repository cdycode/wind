#ifndef __lvr_primitive_data_h__
#define __lvr_primitive_data_h__

#include "lvr_primitive_data_base.h"

class lvr_primitive_data :public lvr_primitive_data_base
{
public:
	lvr_primitive_data();
	~lvr_primitive_data();
public:
	const vertex_fromat* get_data();
	const uint16_t* get_tri_index();
	virtual void flip_normal();
	virtual void flipu();
	virtual void flipv();
	virtual void flip_orientation();
	int32_t		get_tri_num();
	int32_t		get_vertex_num();
	bool	is_triangle_strip_supported();
	void	set_pos(const lvr_vector3f& a_pos);
	virtual void	release_res();
	virtual bool	update() = 0;
public:
	vertex_fromat*	_vertex_buffers;
	void*		_index_buffers;
	uint32_t		_triangle_num;
	uint32_t		_vertex_num;
	lvr_vector3f	_pos;
	bool			_triangle_strip_supported;
};


#endif
