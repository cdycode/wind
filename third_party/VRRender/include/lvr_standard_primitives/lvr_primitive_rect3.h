#ifndef __lvr_primitive_rect3_h__
#define __lvr_primitive_rect3_h__

#include "lvr_primitive_data.h"

//you can also only use positions to get triangle_strip
class lvr_primitive_rect3
	:public lvr_primitive_data
{
public:
	lvr_primitive_rect3();
	~lvr_primitive_rect3();
public:
	void set_size(float a_width,float a_height);
	void set_thickness(float a_frame_thick);
	void set_plane(const lvr_vector3f& a_v0,const lvr_vector3f& a_v1);
	virtual bool	update();
private:
	float			_width;
	float			_height;
	float			_thickness;
	lvr_vector3f	_xv;
	lvr_vector3f	_yv;
};

#endif
