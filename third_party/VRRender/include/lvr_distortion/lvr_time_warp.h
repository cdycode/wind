#ifndef __lvr_time_warp_h__
#define __lvr_time_warp_h__

//1.generate several eye render textures,outside will get these render textures for render
//may be this work should make a single class to do.
//2.make a different thread for high priority render,directly render two eye buffers to screen.
//so call sensor to get current sensor data is needed here.since sensor is only android currently.
//we can do this very effective.
#include "lvr_render_lib.h"
#include "lvr_thread.h"
#include "lvr_vsync.h"
#include "lvr_distortion_warp.h"
#include "lvr_math_lib.h"
#include "lvr_jni_utils.h"
#include "lvr_warp_source.h"
#include "lvr_distortion_render.h"
class lvr_warp_info
{
public:
	lvr_hmd_info _hmd_info;
	lvr_vsync_state* _vsync;
	glass_type _glass_type;
#ifdef LVR_OS_ANDROID
	JavaVM *			JavaVm;
	jclass				VrLibClass;
	jobject				ActivityObject;
	pid_t				GameThreadTid;
#endif
	float				meters_per_tanangle;
	float				parms[11];
	float				aberration_coefficient[4];
	bool _chromatic_aberration_enable;
	bool _using_front_buffer;
	bool _asynchronous_time_warp;
	//std::string ExternalStorageDirectory;
};

class LVR_DISTORTION_API lvr_time_warp
{
public:
	lvr_time_warp();
	~lvr_time_warp();
public:
	void			init(lvr_warp_info* a_warp_info);
	void set_chromatic_aberration_state(bool a_aberration_enable);
	static void*	thread_starter(void* parm);
	lvr_thread*		get_warp_thread();
	void			warp_thread();
	void			warp_thread_init();
	void			warp_thread_uninit();
	void			uninit();
	//only call once per frame.do not call multi times.
	//this strategy is very important,maintain the state of each buffer is a carefule work.
	lvr_warp_source&	get_warp_source_for_render(int id);
	void 			warp_swap(lvr_warp_source& a_warp_src);
private:
	//call sensor to get current quaternion,and then call distortion_render for draw.
	//this is the two work it should do.it's also can be called outside.if we use asynchronous_time_warp
	void			warp_to_screen(const double a_sync_base);
private:
	//use lock to make sure we do not mess,or we can just make a strategy that we do not need to lock.
	//when this return -1,we will jump one frame.
	int	choose_one_render_texture_for_warp();
	//the return value is between 0 , cs_total_eye_buf_num-1
	int choose_one_render_texture_for_render();
	void generate_time_warp_matrix(lvr_matrix4f& ao_mat2,lvr_warp_source* a_ws,lvr_quaternionf& a_real_q);
public:
	const static int	cs_total_eye_buf_num = 3;
private:
	lvr_distortion_render	_distortion_render;
	lvr_vsync_state*	_vsync_state;
	lvr_thread*			_warp_thread;
	lvr_condition*		_swap_cond;
	lvr_mutex*			_swap_mutex;
	int					_current_drawing_buffer;
	int					_current_warp_buffer;
	lvr_warp_source		_warp[cs_total_eye_buf_num];
	int64_t				_swap_vsync_count;
	int					_eye_buffer_count;
#ifdef LVR_OS_ANDROID
	pid_t			StartupTid;
	JNIEnv *		Jni;

	EGLDisplay		eglDisplay;

	EGLSurface 		windowSurface;
	EGLSurface		eglPbufferSurface;
	EGLSurface		eglMainThreadSurface;
	EGLConfig		eglConfig;
	EGLint			eglClientVersion;	// TimeWarp can work with EGL 2.0 or 3.0
	EGLContext		eglShareContext;

	// Our private context, only used for warping to the screen.
	EGLContext		eglWarpContext;
	GLuint			contextPriority;
	jmethodID		SetSchedFifoMethodId;
#endif
	double				_last_warp_time;
	lvr_warp_info		_warp_info;
	static	bool		_shuting_down;//as our warp thread need it.
};

#endif
