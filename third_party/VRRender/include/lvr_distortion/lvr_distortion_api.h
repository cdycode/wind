#ifndef __lvr_distortion_api_h__
#define __lvr_distortion_api_h__

#include "lvr_device_info.h"

#ifdef __cplusplus
extern "C"{
#endif

//api c
int  lvr_set_up_warp(glass_type gt,const lvr_hmd_info* a_hmd_info,int a_chromatic_aberration_enable);
int  lvr_set_chromatic_aberration_state(int a_chromatic_aberration_enable);
int  lvr_begin_render_eye(int eye);
int  lvr_end_render_eye(int eye);
int  lvr_process_warp();
int  lvr_process_warp_separate(int eye);
int  lvr_process_warp_outside(int tex0,int tex1);
int  lvr_release_res();

#ifdef __cplusplus
};
#endif

#endif