#ifndef __lvr_distortion_render_h__
#define __lvr_distortion_render_h__

//deal with the eye buffers,draw them to the screen.
//eye buffers is given outside,in this class,we only deal the buffer time info,
//and also the buffer quaternion,then inside we will need current quaternion.
//make a slight transform,then draw to the screen.
//so get current quaternion,should be called inside or outside,
//here we do this outside
//so this class's responsibility is 
//1.make slight transform with current quaternion.
//2.warp eye buffer to screen(front buffer) or we call back buffer.
#include "lvr_warp_source.h"
#include "lvr_distortion.h"
#include "lvr_jni_utils.h"

class lvr_distortion_render
{
public:
	enum render_method
	{
		FB_TILED_RENDERING,
		FB_BINNING_CONTROL,
		FB_WRITEONLY_RENDERING,
		FB_PVR,
		FB_MALI
	};
public:
	lvr_distortion_render();
	~lvr_distortion_render();
public:
	bool set_glass(glass_type gt);
	//this two function is called for distortion work,maybe we should directly make a distortion_work instance outside.
	//here we keep one inside,since there are only two method added.
	void set_hmd_info(const lvr_hmd_info& a_hmd_info);
	const lvr_hmd_info& get_hmd_info()const;
	void set_distortion_parms(const lvr_distortion_parms& a_parms);
	//use aberration or not is a very important choice.
	void set_chromatic_aberration_state(bool a_use_or_not);
	//init with width,height,using aberration or not is enough,we do not need more.
	bool init(int a_width,int a_height,bool a_chromatic_aberration_enable);
	void uninit();
	//set which eye buffers we are going to use.
	void set_warp_source(lvr_warp_source* a_warp_source);
	//use current quaternion to do our work,it is likely a slightly correction to the sensor state.
	void process_warp(int eye,lvr_quaternionf* a_q,float a_time_to_swap);
	//***********do not use this method,only use it for test or something like that.
	//we call also not set warp source,but directly set two textures,may be we should remove this method.
	void process_warp_eye_textures(int a_eye_id0,int a_eye_id1);
	void swap_buffers();

protected:
	void begin_rendering(int x,int y,int w,int h);
	void end_rendering()const;
private:
	void generate_warp_program();
	void generate_warp_mesh();
	void bind_warp_mesh();
	void unbind_warp_mesh();
	unsigned int build_program(const char* a_vs,const char* a_ps);
	lvr_distortion_parms choose_proper_distortion_parms_t(glass_type gt);
private:
	lvr_warp_source*	_warp_souce;
	lvr_distortion	_distortion_work;
	int	_tex_mat_loc;
	int	_tex_mat2_loc;
	unsigned int	_screen_width;
	unsigned int	_screen_height;
	unsigned int	_warp_mesh_vbo;
	unsigned int	_warp_mesh_ibo;
	unsigned int	_warp_mesh_index_count;

	unsigned int	_warp_program_simple;
	unsigned int	_warp_program_aberration;
	render_method	_render_method;
	lvr_matrix4f	_time_warp_matrixs[2][2];
	bool			_with_aberration_adjust;
#ifdef LVR_OS_ANDROID
public:
	EGLSurface 		windowSurface;		// swapbuffers will be called on this
private:
	EGLDisplay		display;
	EGLContext		context;
#endif
};

#endif
