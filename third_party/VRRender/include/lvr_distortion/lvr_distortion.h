#ifndef __lvr_distortion_h__
#define __lvr_distortion_h__

#include "lvr_distortion_configure.h"
#include "lvr_device_info.h"
#include "lvr_distortion_parms.h"
#include <math.h>
struct lvr_distortion_mesh{
	float*	_red_data;
	float*	_green_data;
	float*	_blue_data;
	int		_mesh_grid_num_per_eye;//pow of 2,like 16 32 and so on ..
	int		_vertex_num;
};

typedef struct  dis_vector3f__
{
	float x,y,z;
}dis_vector3f;

typedef struct  dis_vector2f__
{
	float x,y;
}dis_vector2f;
class dis_matrix4f
{
public:
	dis_matrix4f()
	{
		for (int i=0;i<4;++i)
		{
			for (int j=0;j<4;++j)
			{
				if (i==j)
				{
					_mm[i][j] = 1.0f;
				}
				else
				{
					_mm[i][j] = 0.0f;
				}
			}
		}
	}
	dis_matrix4f(float a_m00,float a_m01,float a_m02,float a_m03,
		float a_m10,float a_m11,float a_m12,float a_m13,
		float a_m20,float a_m21,float a_m22,float a_m23,
		float a_m30,float a_m31,float a_m32,float a_m33)
	{
		_mm[0][0] = a_m00;
		_mm[0][1] = a_m01;
		_mm[0][2] = a_m02;
		_mm[0][3] = a_m03;

		_mm[1][0] = a_m10;
		_mm[1][1] = a_m11;
		_mm[1][2] = a_m12;
		_mm[1][3] = a_m13;

		_mm[2][0] = a_m20;
		_mm[2][1] = a_m21;
		_mm[2][2] = a_m22;
		_mm[2][3] = a_m23;

		_mm[3][0] = a_m30;
		_mm[3][1] = a_m31;
		_mm[3][2] = a_m32;
		_mm[3][3] = a_m33;
	}
public:
	static dis_matrix4f make_proj_matrix_rh(float a_view_angle_rad,float a_aspect_ratio,float a_near,float a_far)
	{
		float f_n = float(1.0) / (a_far - a_near);
		float theta = a_view_angle_rad * float(0.5);

		float divisor = tan(theta);
		float factor = float(1.0) / divisor;

		return dis_matrix4f(
			(float(1.0) / a_aspect_ratio) * factor,float(0),     float(0),              float(0),
			float(0),  factor,                                  float(0),              float(0),
			float(0),  float(0),      (-(a_far + a_near)) * f_n,    float(-1.0),
			float(0),  float(0),   float(-2.0) * a_far * a_near * f_n,       float(0));
	}
	static dis_matrix4f make_translation_matrix(float x,float y,float z)
	{
		return dis_matrix4f(1,0,0,x,
							0,1,0,y,
							0,0,1,z,
							0,0,0,1);
	}
	float& operator[](int id)
	{
		return _m[id];
	}
	const float& operator[](int id)const
	{
		return _m[id];
	}
	dis_matrix4f operator*(const dis_matrix4f& a_mat4)
	{
		dis_matrix4f dm;
		for (int i=0;i<4;++i)
		{
			for (int j=0;j<4;++j)
			{
				dm._mm[i][j] = _mm[i][0]*a_mat4._mm[0][j] + _mm[i][1]*a_mat4._mm[1][j] + _mm[i][2]*a_mat4._mm[2][j] + _mm[i][3]*a_mat4._mm[3][j];
			}
		}
		return dm;
	}
public:
	union
	{
		float _m[16];
		float _mm[4][4];
	};
};

class lvr_distortion
{
public:
	lvr_distortion();
	~lvr_distortion();
	void set_hmd_info(const lvr_hmd_info& a_hmd_info);
	const lvr_hmd_info& get_hmd_info()const;
	void set_distortion_parms(const lvr_distortion_parms& a_parms);
	const lvr_distortion_parms& get_distortion_parms();
	//a_mesh_grid_num_per_eye : pow of 2 like 16 32 is preferred.
	void generate_mesh(int a_mesh_grid_num_per_eye);
	const lvr_distortion_mesh& get_mesh();
	void		release_mesh();
private:
	void		warp_texcoord_chroma(const float a_in[2],float ao_r[2],float ao_g[2],float ao_b[2]);
	dis_vector3f	get_scaled_chroma(float a_rsq);
	float	eval_catmullrom_spline(float* a_k,float a_scaled_sq,int a_num_segs);
private:
	lvr_hmd_info			_hmd_info;
	lvr_distortion_parms	_distortion_parms;
	lvr_distortion_mesh		_distortion_mesh;
};

#endif
