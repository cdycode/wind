#ifndef __lvr_warp_source_h__
#define __lvr_warp_source_h__

#include "lvr_distortion_configure.h"
#include "lvr_distortion_render_texture.h"
#include "lvr_time_warp_parms.h"
typedef void* lvr_synv_khr;
class lvr_warp_source
{
public:
	lvr_warp_source()
		:_sync_id(0.0)
		,_gpu_sync(NULL)
	{

	}
public:
	double				_sync_id;
	int64_t				_first_displayed_vsync[2];	// External velocity is added after this vsync.
	lvr_synv_khr		_gpu_sync;
	lvr_time_warp_parms	_time_warp_parms;
	lvr_distortion_render_texture	_eye[2];
	lvr_quaternionf		_orientation;
};


#endif
