#ifndef __lvr_distortion_render_texture_h__
#define __lvr_distortion_render_texture_h__


enum colorFormat_t
{
	COLOR_565,
	COLOR_8888,
	COLOR_8888_sRGB
};
enum textureFilter_t
{
	TEXTURE_FILTER_NEAREST,		// Causes significant aliasing, only for performance testing.
	TEXTURE_FILTER_BILINEAR,	// This should be used under almost all circumstances.
	TEXTURE_FILTER_ANISO_2,		// Anisotropic filtering can in some cases reduce aliasing.
	TEXTURE_FILTER_ANISO_4
};
class lvr_distortion_render_texture
{
public:
	lvr_distortion_render_texture():_fbo_id(0),_depth_buffer(0),_multisample_color_buffer(0),_resolve_frame_buffer(0),_tex_id(0),_width(0),_height(0){}
	~lvr_distortion_render_texture();
public:
	void			set_up(colorFormat_t a_col_format,textureFilter_t a_tex_filter,int a_width,int a_height,int a_depth_bits,int a_stencil_bits,int a_msaa = 0);
	void			enable_render();
	void			disable_render();
	unsigned int	get_texture_id();
	void			release();
private:
	unsigned int	_fbo_id;
	unsigned int	_depth_buffer;
	unsigned int	_multisample_color_buffer;
	unsigned int	_resolve_frame_buffer;
	unsigned int	_tex_id;
	int				_width;
	int				_height;
};

#endif
