#ifndef __lvr_distortion_warp_h__
#define __lvr_distortion_warp_h__

#include "lvr_device_info.h"
#include "lvr_distortion_api.h"

class lvr_distortion_warp
{
public:
	enum type//later will be used,currently the interface with this is not stable
	{
		simple_2d,
		left_right_3d,
		up_down_3d,
		panorama
	};
public:
	lvr_distortion_warp(){}
	virtual ~lvr_distortion_warp(){}
public:
	//suggest a_chromatic_aberration_enable to be false,as in windows case,when it is true,
	//you can see some pixels that not supported to be seen.
	virtual void set_hmd_info(const lvr_hmd_info& a_hmd_info) = 0;
	virtual bool init(int a_width,int a_height,bool a_chromatic_aberration_enable) = 0;
	virtual bool set_glass(glass_type gt) = 0;
	virtual void begin_render_eye(int a_eye) = 0;
	virtual void end_render_eye(int a_eye) = 0;
	virtual void process_warp() = 0;
	virtual void process_warp_separate(int a_eye) = 0;

	//same with init case's varible.
	virtual void set_chromatic_aberration_state(bool a_use_or_not) = 0;

	//this interface has some problems,so close it.
	virtual void process_texture_warp(type a_type,const float* a_view_mat) = 0;

	virtual void release() = 0;

	virtual void process_warp_eyes(int eye0,int eye1) = 0;
};
//api cpp
lvr_distortion_warp* create_distortion_warp();

#endif
