#ifndef __lvr_device_info_h__
#define __lvr_device_info_h__


#ifdef __cplusplus
extern "C"{
#endif

typedef enum __glass_type__
{
	gear_vr,
	storm_magic_mirror2,
	storm_magic_mirror3,
	ling_bai1,
	ling_bai2,
	cardboard,
	svr_glass,
	vir_glass,
	play_glass,
	bobovr2,//xiao zhai
	depth_vr,
	vr_box,
	dream_vr,
	vr_one,
	user_define_glass,
	other_glass
}glass_type;

typedef struct lvr_hmd_info__
{
	float	_lens_separation;//in meters
	float	_eye_texture_fov;
	float	_screen_width_meters;//in meters
	float	_screen_height_meters;//in meters
	int		_screen_width_pixels;//in pixels
	int		_screen_height_pixels;//in pixels
	int		_depth_bits;
}lvr_hmd_info;

#ifdef __cplusplus
};
#endif

#endif
