#ifndef __lvr_distortion_parms_h__
#define __lvr_distortion_parms_h__


extern int g_catmullrom_parm_num;

struct lvr_distortion_parms
{
public:
	enum e_distortion_method
	{
		e_distortion_method_poly4,
		e_distortion_method_recip_poly4,
		e_distortion_method_catmullrom10,
		e_distortion_method_catmullrom20,
		e_distortion_method_catmullrom,
	};

public:
	e_distortion_method	_distortion_method;
	float				_k[64];
	float				_kr[64];
	float				_kb[64];
	float				_meters_per_tanangle_at_center;
	float				_chromatic_aberration[4];
};

#endif
