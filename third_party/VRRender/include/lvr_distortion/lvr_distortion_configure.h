#ifndef __lvr_distortion_configure_h__
#define __lvr_distortion_configure_h__

#include "lvr_core_lib.h"

// #ifdef LVR_OS_WIN32
// #define lvr_render_IMPORT
// #endif
#include "lvr_render_lib.h"

#ifdef lvr_distortion_EXPORTS
#define LVR_DISTORTION_API LVR_API_EXPORT
#elif defined lvr_distortion_IMPORT
#define LVR_DISTORTION_API LVR_API_IMPORT
#else
#define LVR_DISTORTION_API LVR_API_STATIC
#endif

#endif
