#ifndef __lvr_font_configure_h__
#define __lvr_font_configure_h__

#include "lvr_core_lib.h"
#include "lvr_math_lib.h"

#ifdef lvr_font_EXPORTS
#ifdef LVR_OS_ANDROID
#define LVR_FONT_API
#else
#define LVR_FONT_API __declspec(dllexport)
#endif // LVR_OS_ANDROID
#elif defined(lvr_font_IMPORT)
#ifdef LVR_OS_ANDROID
#define LVR_FONT_API
#else
#define LVR_FONT_API __declspec(dllimport)
#endif // LVR_OS_ANDROID
#else
#define LVR_FONT_API
#endif

#endif
