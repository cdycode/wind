#ifndef __lvr_font_manager_h__
#define __lvr_font_manager_h__

#include "lvr_font_configure.h"

class lvr_font;
class LVR_FONT_API lvr_font_manager
{
public:
	lvr_font_manager();
public:
	static lvr_font_manager*	get_font_manager();
private:
	~lvr_font_manager();
public:
	bool add_font_from_file(const char* a_file_path,const char* a_font_name);
	bool add_font_from_mem(const char* a_file_bytes,const int a_buf_len,const char* a_font_name);
	lvr_font*	get_font(const char* a_font_name);
	void		release_font(const char* a_font_name);
	void		release_all();
private:
	class impl;
	impl*	_this;
};

#endif
