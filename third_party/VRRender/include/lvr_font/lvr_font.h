#ifndef __lvr_font_h__
#define __lvr_font_h__

#include "lvr_font_configure.h"

//class lvr_multi_polygon2f;

typedef struct lvr_alpha_text__
{
	uint8_t*	alpha_bits;
	uint32_t	width,height;
}lvr_alpha_text;


class lvr_font_glyph
{
public:
	lvr_font_glyph()
		:alpha_bits(0)
		,width(0)
		,height(0)
		,advance_x(0)
		,advance_y(0)
		,bearing_x(0)
		,bearing_y(0)
	{}
	void release()
	{
		if (alpha_bits)
		{
			delete[] alpha_bits;
			alpha_bits = 0;
		}
	}
	~lvr_font_glyph()
	{
		
	}
public:
	int32_t		char_code;
	uint8_t*	alpha_bits;
	uint32_t	width,height;
	int			advance_x;
	int			advance_y;
	int			bearing_x;
	int			bearing_y;
};

class lvr_font
{
public:
	lvr_font(){}
	virtual ~lvr_font(){}
public:
	//str in utf8 code.
	virtual void set_font_size(uint32_t a_font_size) = 0;
	virtual uint32_t get_font_size() = 0;
	virtual lvr_alpha_text	get_text_alpha(const uint16_t* a_str,uint32_t a_len) = 0;
	virtual lvr_font_glyph  get_font_glyph(const uint16_t a_char_code) = 0;
	//virtual bool get_font_outline(lvr_multi_polygon2f* ao_multi_poly,const uint16_t a_char_code) = 0;
};

#endif
