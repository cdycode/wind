﻿#include "headers.h"
#include <wx_network/wx_i_tcp_toy_server.h>
#include "wx_tcp_test.h"

bool wxTcpClientTest::Open() 
{
	Close();

	tcp_client = wxITcpClient::CreateIns();
	if (nullptr == tcp_client)
	{
		return false;
	}
	if (!tcp_client->Open(this)) 
	{
		return false;
	}
	if (!tcp_client->Connect(wxSocketAddress("127.0.0.1", 6000))) 
	{
		return false;
	}
	return true;
}

void wxTcpClientTest::Close() 
{
	if (nullptr != tcp_client)
	{
		tcp_client->Close();
		wxITcpClient::DestoryIns(tcp_client);
		tcp_client = nullptr;
	}
}

void wxTcpClientTest::SendData(const char* a_data) 
{
	if (nullptr != tcp_client)
	{
		tcp_client->SendData(a_data, strlen(a_data)+1);
	}
}

// 收到数据
void wxTcpClientTest::OnRecvData(char * apPackData, int aiPackLen) 
{
	std::cout << "recv data: " << apPackData << std::endl;
}

// 断开连接
void wxTcpClientTest::OnDisconnect(void) 
{
	std::cout << "disconnect " << std::endl;
}


bool wxTcpServerTest::Open() 
{
	Close();

	tcp_server = wxITcpToyServer::CreateIns();
	if (nullptr == tcp_server)
	{
		return false;
	}
	if (!tcp_server->Open("127.0.0.1", 6000, this))
	{
		return false;
	}
	return true;
}

void wxTcpServerTest::Close() 
{
	if (nullptr != tcp_server)
	{
		tcp_server->Close();
		wxITcpToyServer::DestoryIns(tcp_server);
		tcp_server = nullptr;
	}
}

//收到新的Socket
void wxTcpServerTest::OnNewClient(UINT64 aui64ClientID, const char* apSrcIPAddress, unsigned short ausBindPort) 
{
	std::cout << "new client connected，clientid="<< aui64ClientID<< " ip=" << apSrcIPAddress << " port=" << ausBindPort << std::endl;
}

//断开连接
void wxTcpServerTest::OnBreakClient(UINT64 aui64ClientID) 
{
	std::cout << "new client connected，clientid=" << aui64ClientID << std::endl;
}

// 收到数据
void wxTcpServerTest::OnRecvData(UINT64 aui64ClientID, const char * apPackData, int aiPackDataLen) 
{
	std::cout << "aui64ClientID = "<< aui64ClientID << " recv data: " << apPackData << std::endl;

	tcp_server->SendData(aui64ClientID, apPackData, aiPackDataLen);
}

void TcpClientSender() 
{
	std::cout << "测试Tcp发送, 输入 e 退出" << std::endl;

	wxTcpClientTest test;
	test.Open();

	char temp[100];
	while (1)
	{
		std::cin >> temp;
		if (strlen(temp) == 1)
		{
			if (temp[0] == 'e' || temp[0] == 'E')
			{
				break;
			}
		}
		test.SendData(temp);
	}
	test.Close();
}

void TcpServerRecv() 
{
	std::cout << "测试Tcp接收, 输入任意字符退出" << std::endl;
	wxTcpServerTest test;
	test.Open();
	char recv = getchar();
}
