﻿#ifndef NETWORK_TEST_WX_UDP_TEST_H_
#define NETWORK_TEST_WX_UDP_TEST_H_

#include <wx_network/wx_i_udp_socket.h>

class wxUdpTest : public wxIUdpRecvEvent
{
public:
	wxUdpTest() {}
	virtual ~wxUdpTest() {}

	bool	Open(ovr::uint16 a_bind_port);
	void	Close();

	void	SendData(const char* apSendData, int anDataLen, const wxSocketAddress& aoDstAddress);

	virtual void UdpRecvData(const char * apPackData, int aiPackLen, const wxSocketAddress& aoFromAddress) override;

private:
	wxIUdpSocket*	udp_socket = nullptr;
};

void	UdpSender();
void	UdpReceiver();

#endif
