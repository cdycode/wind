#include "headers.h"
#include "wx_broadcast_test.h"


bool wxBroadcastTest::Open(ovr::uint16 a_bind_port, ovr::uint16 a_dest_port)
{
	broadcast_socket = wxIBroadcastSocket::CreateIns();
	if (nullptr == broadcast_socket)
	{
		return false;
	}

	if (!broadcast_socket->Open(this, a_bind_port, a_dest_port))
	{
		return false;
	}

	return true;
}

void wxBroadcastTest::Close() 
{
	if (nullptr != broadcast_socket)
	{
		broadcast_socket->Close();
		wxIBroadcastSocket::DestoryIns(broadcast_socket);
		broadcast_socket = nullptr;
	}
}

void wxBroadcastTest::SendData(const char* a_data) 
{
	if (nullptr != broadcast_socket)
	{
		broadcast_socket->BroadcastData(a_data, strlen(a_data));
	}
}

// 收到数据
void wxBroadcastTest::BroadcastRecvData(const char * apPackData, int aiPackLen, const wxSocketAddress& aoFromAddress) 
{
	std::cout << "recv data: " << apPackData << std::endl;
}

void BroadcastSender() 
{
	std::cout << "测试广播发送, 输入 e 退出" << std::endl;

	wxBroadcastTest test;
	test.Open(5999, 6000);

	char temp[100];
	while (1)
	{
		std::cin >> temp;
		if (strlen(temp) == 1)
		{
			if (temp[0] == 'e' || temp[0] == 'E')
			{
				break;
			}
		}
		test.SendData(temp);
	}
	test.Close();
}

void BroadcastReceiver() 
{
	std::cout << "测试广播接收, 输入任意字符退出" << std::endl;
	wxBroadcastTest test;
	test.Open(6000, 60002);
	char recv = getchar();
}
