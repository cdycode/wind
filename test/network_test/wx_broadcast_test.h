#ifndef NETWORK_TEST_WX_BROADCAST_TEST_H_
#define NETWORK_TEST_WX_BROADCAST_TEST_H_

#include <wx_network/wx_i_broadcast_socket.h>

class wxBroadcastTest : public wxIBroadcastEvent
{
public:
	wxBroadcastTest() {}
	virtual ~wxBroadcastTest() {}

	bool	Open(ovr::uint16 a_bind_port, ovr::uint16 a_dest_port);
	void	Close();

	void	SendData(const char* a_data);

	// �յ�����
	virtual void BroadcastRecvData(const char * apPackData, int aiPackLen, const wxSocketAddress& aoFromAddress) override;

private:
	wxIBroadcastSocket*	broadcast_socket = nullptr;
};

void	BroadcastSender();
void	BroadcastReceiver();
#endif
