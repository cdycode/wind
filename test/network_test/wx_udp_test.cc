﻿#include "headers.h"
#include "wx_udp_test.h"

bool wxUdpTest::Open(ovr::uint16 a_bind_port) 
{
	Close();

	udp_socket = wxIUdpSocket::CreateIns();
	if (nullptr == udp_socket)
	{
		return false;
	}

	if (!udp_socket->Open(this, "127.0.0.1", a_bind_port)) 
	{
		return false;
	}
	return true;
}

void wxUdpTest::Close() 
{
	if (nullptr != udp_socket)
	{
		udp_socket->Close();
		wxIUdpSocket::DestoryIns(udp_socket);
		udp_socket = nullptr;
	}
}

void wxUdpTest::SendData(const char* apSendData, int anDataLen, const wxSocketAddress& aoDstAddress) 
{
	if (nullptr != udp_socket)
	{
		udp_socket->SendData(apSendData, anDataLen, aoDstAddress);
	}
}

void wxUdpTest::UdpRecvData(const char * apPackData, int aiPackLen, const wxSocketAddress& aoFromAddress) 
{
	std::cout << "recv data: " << apPackData << std::endl;
}

void	UdpSender() 
{
	std::cout << "测试UDP发送, 输入 e 退出" << std::endl;

	wxUdpTest test;
	test.Open(5999);

	char temp[100];
	while (1)
	{
		std::cin >> temp;
		if (strlen(temp) == 1)
		{
			if (temp[0] == 'e' || temp[0] == 'E')
			{
				break;
			}
		}
		test.SendData(temp, strlen(temp), wxSocketAddress("127.0.0.1", 6000));
	}
	test.Close();
}

void	UdpReceiver() 
{
	std::cout << "测试UDP接收, 输入任意字符退出" << std::endl;
	wxUdpTest test;
	test.Open(6000);
	char recv = getchar();
}
