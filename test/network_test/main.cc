#include "headers.h"
#include <wx_network/wx_network.h>
#include "wx_broadcast_test.h"
#include "wx_udp_test.h"
#include "wx_tcp_test.h"

/*
	//第一个参数为可执行文件名
	//第二个参数为测试类型 b:广播 u:Udp  t:tcp
	//第三个参数为启动类型： r:数据接收端 s:数据发送端
*/

int main(int argc, char *argv[]) 
{
	if (argc < 3 )
	{
		std::cout << "参数错误" << std::endl;
		std::cout << "输入任意字符退出" << std::endl;
		char temp = getchar();
		return 0;
	}

	InitInsNetwork();

	if (argv[1][0] == 'b')
	{
		//广播测试
		if (argv[2][0] == 'r')
		{
			BroadcastReceiver();
		}
		else if (argv[2][0] == 's')
		{
			BroadcastSender();
		}
	}
	else if (argv[1][0] == 'u')
	{
		//UDP测试
		if (argv[2][0] == 'r')
		{
			UdpReceiver();
		}
		else if (argv[2][0] == 's')
		{
			UdpSender();
		}
	}
	else if (argv[1][0] == 't') 
	{
		//TCP测试
		if (argv[2][0] == 'r')
		{
			TcpServerRecv();
		}
		else if (argv[2][0] == 's')
		{
			TcpClientSender();
		}
	}

	UninitInsNetwork();
	return 0;
}