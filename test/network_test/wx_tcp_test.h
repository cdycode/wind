﻿#ifndef NETWORK_TEST_WX_TCP_TEST_H_
#define NETWORK_TEST_WX_TCP_TEST_H_

#include <wx_network/wx_i_tcp_client.h>
#include <wx_network/wx_i_tcp_server_event.h>

class wxTcpClientTest : public wxITcpClientEvent
{
public:
	wxTcpClientTest() {}
	virtual ~wxTcpClientTest() {}

	bool	Open();
	void	Close();
	void	SendData(const char* a_data);

	// 收到数据
	virtual void OnRecvData(char * apPackData, int aiPackLen) override;

	// 断开连接
	virtual void OnDisconnect(void) override;

private:
	wxITcpClient*	tcp_client = nullptr;
};

class wxITcpToyServer;
class wxTcpServerTest : public wxITcpSvrEvent 
{
public:
	wxTcpServerTest() {}
	virtual ~wxTcpServerTest() {}

	bool Open();
	void Close();

	//收到新的Socket
	virtual void OnNewClient(UINT64 aui64ClientID, const char* apSrcIPAddress, unsigned short ausBindPort) override;

	//断开连接
	virtual void OnBreakClient(UINT64 aui64ClientID) override;

	// 收到数据
	virtual void OnRecvData(UINT64 aui64ClientID, const char * apPackData, int aiPackDataLen) override;

private:
	wxITcpToyServer*	tcp_server = nullptr;
};

void	TcpClientSender();
void	TcpServerRecv();

#endif

