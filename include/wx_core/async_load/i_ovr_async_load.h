﻿#ifndef OVR_PROJECT_I_OVR_ASYNC_LOAD_H_
#define OVR_PROJECT_I_OVR_ASYNC_LOAD_H_

#include "../ovr_core.h"

/************************************************************************/
/* 此类 后期将统一处理资源异步加载  资源更新热加载的问题	 会修改为新的类名	    */
/* 异步加载 先各自管理  后期将统一到一个管理类中 通过智能指针实现内存和资源的分配和释放*/
/************************************************************************/

namespace ovr_engine 
{
	class IOvrTexture;
}
namespace ovr_core
{
	class OVR_CORE_API IOvrAsyncLoad 
	{
	public:
		IOvrAsyncLoad() {}
		virtual ~IOvrAsyncLoad() {}

		virtual bool	Loading() = 0;
	};

	class OVR_CORE_API OvrAsyncTexture : public IOvrAsyncLoad
	{
	public:
		OvrAsyncTexture() {}
		virtual ~OvrAsyncTexture() {}

		virtual bool	Loading() override;

		void	Clear() { texture = nullptr; }
		//返回值true  不需要异步加载  false 需要异步加载
		bool						SetFilePath(const OvrString& a_file_path);
		
		ovr_engine::OvrTexture*	GetTexture();
	private:
		OvrString					file_path;
		ovr_engine::OvrTexture*	texture = nullptr;
	};
}

#endif
