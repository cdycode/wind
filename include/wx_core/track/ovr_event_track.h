﻿#ifndef OVR_PROJECT_OVR_EVENT_ACTOR_TRACK_H_
#define OVR_PROJECT_OVR_EVENT_ACTOR_TRACK_H_

#include "ovr_point_track.h"

namespace ovr_core
{
	class IOvrNodeCluster;
	class OVR_CORE_API OvrEventTrack : public OvrPointTrack
	{
	public:
		OvrEventTrack(OvrSequence* a_sequence):OvrPointTrack(a_sequence) { display_name = "事件轨道"; }
		virtual ~OvrEventTrack() {}

		//在创建是调用
		virtual bool	Init() override;
		virtual void	Uninit() override;

		virtual PointType		GetPointType() override { return kPointEvent; }

		virtual bool Update(ovr::uint64 a_current_time);
		virtual void Seek(ovr::uint64 a_current_time);
		
		bool			AddKeyFrame(ovr::uint64 a_key, const OvrString& a_event_name);
		void			RmvKeyFrame(ovr::uint64 a_key);
		bool			MoveKey(ovr::uint64 a_from, ovr::uint64 a_to);
		//获取此帧处的事件
		const OvrString& GetKeyFrame(ovr::uint64 a_key);
		
	
		//获取所有的关键帧
		const std::map<ovr::uint64, OvrString>& GetAllKeyFrames() { return key_frames; }

		//获取所有的事件
		const std::list<OvrString>& GetEvents();

		bool	IsHaveEvent(const OvrString& a_event_name);

		//创建新的事件
		bool				CreateEvent(const OvrString& a_event_name);

		//创建和销毁cluster
		IOvrNodeCluster*	LoadCluster(const OvrString& a_file_name);
		void				CloseCluster(IOvrNodeCluster* a_event_cluster);
		void				SaveCluster(IOvrNodeCluster* a_event_cluster);
	private:
		
		bool	DoEvent(const OvrString& a_event_name);

	protected:
		std::map<ovr::uint64, OvrString>	key_frames;

		OvrString				current_event_name;
	};
}

#endif
