﻿#ifndef OVR_PROJECT_OVR_BACKGROUND_AUDIO_TRACK_H_
#define OVR_PROJECT_OVR_BACKGROUND_AUDIO_TRACK_H_

#include <wx_core/track/ovr_track.h>
namespace ovr_core
{
	class OVR_CORE_API OvrBackgrounAudioTrack : public OvrTrack
	{
	public:
		OvrBackgrounAudioTrack(OvrSequence* a_sequence);
		virtual ~OvrBackgrounAudioTrack() {}

	protected:
		virtual OvrTrack*	NewTrack(OvrTrackType a_track_type, const OvrString& a_tag)override;

	};
}

#endif
