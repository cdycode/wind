#ifndef OVR_CORE_OVR_POINT_TRACK_H_
#define OVR_CORE_OVR_POINT_TRACK_H_

#include "ovr_track.h"

namespace ovr_core 
{
	class OVR_CORE_API OvrPointTrack : public OvrTrack
	{
	public:
		enum PointType
		{
			kPointFloat = 0,
			kPointEvent,
		};
	public:
		OvrPointTrack(OvrSequence* a_sequence) :OvrTrack(a_sequence){}
		virtual ~OvrPointTrack() {}

		virtual OvrTrackType	GetTrackType()override { return  kTrackPoint; }

		virtual PointType		GetPointType() = 0;
	};
}

#endif
