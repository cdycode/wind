﻿#ifndef OVR_PEOJECT_OVR_TRACK_H_
#define OVR_PEOJECT_OVR_TRACK_H_

#include "ovr_clip_track.h"

namespace ovr_engine
{
	class OvrSkinnedMeshComponent;
}
namespace ovr_core
{
	class OvrTransformTrack;
	class OVR_CORE_API OvrActorTrack : public OvrTrack
	{
	public:
		OvrActorTrack(OvrSequence* a_sequence);
		virtual ~OvrActorTrack();

		void					SetActor(ovr_engine::OvrActor* a_actor) { owner_actor = a_actor; }
		ovr_engine::OvrActor*	GetActor() { return owner_actor; }
		const OvrString&		GetActorUniqueName() const;

		virtual const std::list<OvrTrackSupport>& GetSupportTrackList() override;
		virtual bool  HasSupportTrackList() const override { return true; };
	protected:
		bool		IsCanAddTrack(OvrTrackType a_track_type);
		bool		IsSupportTrack(OvrTrackType a_track_type);
		template <typename T> 
		OvrTrack* AddGroupTrack(OvrTrackType a_group_track_type, OvrTrackType a_sub_track_type);

		virtual OvrTrack*	NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) override;
	protected:
		ovr_engine::OvrActor*			owner_actor = nullptr;
	};

}

#endif