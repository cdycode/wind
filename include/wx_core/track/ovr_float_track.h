﻿#ifndef OVR_PROJECT_OVR_FLOAT_TRACK_H_
#define OVR_PROJECT_OVR_FLOAT_TRACK_H_

#include "ovr_point_track.h"
#include <wx_core/track/ovr_curve_point_manager.h>

namespace ovr_core
{
	class OVR_CORE_API OvrFloatTrack : public OvrPointTrack
	{
	public:
		OvrFloatTrack(OvrSequence* a_sequence):OvrPointTrack(a_sequence){}
		virtual ~OvrFloatTrack() {}

		virtual PointType		GetPointType() override { return kPointFloat; }

		//需要更新当前值
		virtual bool Update() override;
		virtual void Seek(ovr::uint64 a_current_time) override;

		//获取当前值
		float	GetCurrentValue() { return current_value; }

		//设置缺省值
		void	SetDefaultValue(float a_default_value);
		float	GetDefaultValue()const { return default_value; }

		//添加删除Key
		virtual void	AddKeyFrame(const ovr::uint64 a_time, float a_key_value);
		bool			RmvKeyFrame(const ovr::uint64 a_time);
		bool			GetKeyFrame(ovr::uint64 a_key, float& a_key_value) const;
		bool			MoveKey(ovr::uint64 a_from, ovr::uint64 a_to);
		const std::map<ovr::uint64, float>& GetKeyFrameMap()const { return frame_map; }

		OvrCurvePointManager* getCurvePointManager() { return &pt_manager; }

	private:
		void	FindKeyRange(ovr::uint64 a_current_time);
		bool	SetSpecificValue(ovr::uint64 a_current_time);

		void	Reset();

		//线性插值  已经确定在此范围内
		float	LerpValue(ovr::uint64 a_frame_index)const;

	private:
		float			default_value = 1.0f;
		float			current_value = 0.0f;

		std::map<ovr::uint64, float> frame_map;

		/*优化播放中的 seek时就需要重新计算*/
		ovr::uint64		current_begin_time = 0;
		ovr::uint64		current_end_time = 0;
		float			current_begin_value = 0.0f;
		float			current_end_value = 0.0f;

		OvrCurvePointManager pt_manager;
	};
}

#endif

