﻿#ifndef OVR_PROJECT_OVR_TRACK_H_
#define OVR_PROJECT_OVR_TRACK_H_

#include "../ovr_core.h"
#include <wx_base/wx_tree.h>
#include <wx_base/wx_string.h>

namespace ovr_core 
{
	class OvrSequence;
	enum OvrTrackType
	{
		kTrackCommon = 0,
		kTrackClip,
		kTrackPoint,
	};

	class OvrTrackSupport
	{
	public:
		OvrTrackType	track_type = kTrackCommon;
		bool			is_enable = false;
		OvrString		name;
		OvrString		tag;

		OvrTrackSupport(OvrTrackType a_type, bool a_is_enable, const OvrString& a_name, const OvrString& a_tag)
			:track_type(a_type)
			, is_enable(a_is_enable)
			, name(a_name)
			, tag(a_tag)
		{}

		void operator = (const OvrTrackSupport& a_track)
		{
			track_type = a_track.track_type;
			is_enable = a_track.is_enable;
			name = a_track.name;
			tag = a_track.tag;
		}
	};

	class OVR_CORE_API OvrTrack : public OvrTree<OvrTrack>
	{
	public:
		OvrTrack(OvrSequence* a_sequence);
		virtual ~OvrTrack();

		virtual OvrTrackType		GetTrackType() { return kTrackCommon;}

		//在添加轨道时调用
		virtual bool	Init() { return true; }
		virtual void	Uninit() { RmvAllTrack(); }

		//子类型
		void				SetTag(const OvrString& a_tag) { tag = a_tag; }
		const OvrString&	GetTag()const { return tag; }

		//是否可见
		bool	IsVisible()const { return is_visible; }
		void	SetVisible(bool a_is_visible) { is_visible = a_is_visible; }

		//是否是实时轨道  实时轨道需要每帧都刷新 比如动捕和面捕轨道
		virtual bool IsLive()const { return false; }

		OvrSequence*	GetOwnSequence() { return owner_sequence; }
		
		void 				SetDisplayName(const OvrString& a_display_name) { display_name = a_display_name; }
		const OvrString&	GetDisplayName()const { return display_name; }
		void				SetUniqueName(const OvrString& a_unique_name) { unique_name = a_unique_name; }
		const OvrString&	GetUniqueName()const { return unique_name; }

		virtual bool Update();
		virtual void Pause();
		virtual void Play();
		virtual void Seek(ovr::uint64 a_current_time);

		//子track
		virtual const std::list<OvrTrackSupport>& GetSupportTrackList() { return track_support_list; };
		virtual bool  HasSupportTrackList() const { return track_support_list.size() > 0; };

		OvrTrack*	AddTrack(OvrTrackType a_track_type, const OvrString& a_tag);

		//参数必须为子轨道 才能移除成功
		bool		RmvTrack(OvrTrack* a_track);

		OvrTrack*			GetTrackByType(OvrTrackType a_track_type);
		void				GetTrackListByType(OvrTrackType a_track_type, std::list<OvrTrack*> a_track_list);

	protected:
		void		RmvAllTrack();

		//根据tag获取轨道
		OvrTrack*	GetTrackByTag(const OvrString& a_tag);

		//创建一个track
		virtual OvrTrack*	NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) { return nullptr; }
		virtual void		DelTrack(OvrTrack* a_track) {}
	protected:
		OvrString		tag;	//子类型

		bool			is_visible = true;
		OvrString		display_name;
		OvrString		unique_name;
		OvrSequence*	owner_sequence;

		std::list<OvrTrackSupport> track_support_list;
	};
}

#endif
