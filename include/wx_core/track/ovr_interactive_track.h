﻿#ifndef OVR_PROJECT_OVR_INTER_ACTIVE_ACTOR_TRACK_H_
#define OVR_PROJECT_OVR_INTER_ACTIVE_ACTOR_TRACK_H_

#include "ovr_clip_track.h"

namespace ovr_core
{	
	class IOvrNodeCluster;
	class OVR_CORE_API OvrInterActiveTrack : public OvrClipTrack
	{
	public:
		OvrInterActiveTrack(OvrSequence* a_sequence);
		virtual ~OvrInterActiveTrack();

		virtual OvrTrackType	GetTrackType() override { return  kTrackCommon; }

		virtual bool Update() override;
		virtual void Play()override;
		virtual void Pause() override;
		virtual void Seek(ovr::uint64 a_current_time)override;

		virtual ovr::uint64	GetAssetDuration(const OvrString& a_file_path) override { return 0; }

		//创建新的事件
		bool				CreateCluster(const OvrString& a_event_name);

		//创建和销毁cluster
		IOvrNodeCluster*	LoadCluster(const OvrString& a_file_name);
		void				CloseCluster(IOvrNodeCluster* a_event_cluster);
		void				SaveCluster(IOvrNodeCluster* a_event_cluster);
	private:
		bool OpenInteractive();
		void CloseInteractive();

		//a_is_play 是否处于播放状态  如果播放状态  且是新打开的 就需要调用BeginPlay
		bool	GetInteractive(ovr::uint64 a_current_time, bool a_is_play);

	private:

		IOvrNodeCluster* current_intercative_area = nullptr;
	};
}

#endif
