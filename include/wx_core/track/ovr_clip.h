﻿#ifndef OVR_PROJECT_OVR_CLIP_H_
#define OVR_PROJECT_OVR_CLIP_H_

#include <wx_core/ovr_core.h>

namespace ovr_engine 
{
	class OvrTextActor;
}

namespace wx_plugin
{
	class IOvrAudioDecoder;
	class wxFrameBuffer;
}

namespace ovr_core
{
	class OvrTrack;
	class OVR_CORE_API OvrClipInfo
	{
	public:
		enum Type
		{
			kInvalid = 0,
			kSkeletalAnimation,
			kMorphAnimaition,
			kText,
			kVideo,
			kAudio,
		};

	public:
		static OvrClipInfo*	CreateClip(Type a_clip_type, OvrTrack* a_parent_track);
	public:
		OvrClipInfo()
		{}

		OvrClipInfo(const char *a_path, const ovr::uint64 a_start_time, const ovr::uint64 a_end_time)
			: file_path(a_path)
			, start_time(a_start_time)
			, end_time(a_end_time)
		{
		}

		~OvrClipInfo()
		{
		}

		//获取clip类型
		virtual Type GetType() { return kInvalid; }

		virtual bool Open() { return true; }
		virtual void Close() {}

		virtual void operator = (const OvrClipInfo& a_video_clip)
		{
			file_path = a_video_clip.file_path;
			start_time = a_video_clip.start_time;
			end_time = a_video_clip.end_time;
		}

		ovr::uint64 Length()const { return end_time - start_time; }

	public:
		ovr::uint64	timeline_position = 0;	//在时间线的位置
		ovr::uint64 start_time = 0;
		ovr::uint64 end_time = 100;
		OvrString	file_path;
	};

	class OVR_CORE_API OvrTextClip : public OvrClipInfo
	{
	public:
		OvrTextClip(OvrTrack* a_own_track);
		virtual ~OvrTextClip() {}

	public:
		
		//获取clip类型
		virtual Type GetType() { return kText; }

		virtual bool Open() override;
		virtual void Close() override;

		virtual void operator = (const OvrClipInfo& a_video_clip) override
		{
			OvrClipInfo::operator =(a_video_clip);
			const OvrTextClip* text_clip = static_cast<const OvrTextClip*>(&a_video_clip);
			text = text_clip->text;
			show_time = text_clip->show_time;
			timeline_position = text_clip->timeline_position;
		}

		void				SetText(const OvrString& a_text) { text = a_text; }
		const OvrString&	GetText()const { return text; }

		//更新text 返回true text内容有变化 否者 返回false
		bool	UpdateText(ovr::uint64 a_frame_index);

		//获得需要显示的文本  时间为绝对时间
		const OvrString&	GetCurrentText()const { return current_show; }

		//设置显示完需要的时间 单位为秒
		void	SetShowTime(float a_show_time);
		float	GetShowTime()const { return show_time; }

	private:
		OvrTrack*		own_track = nullptr;
		OvrString		text;
		float			show_time = 0.5f;

		//计算过程中的中间值
		std::wstring	unicode_text;
		ovr::uint32		current_show_num = 0;	//以unicode为计数
		OvrString		current_show;
		ovr::uint64		show_frames = 1;
	};

	class OvrAudioGroupTrack;
	class OVR_CORE_API OvrAudioClip : public OvrClipInfo
	{
	public:
		OvrAudioClip(OvrAudioGroupTrack* a_group_track)
			:own_group_track(a_group_track) {}
		virtual ~OvrAudioClip() {}

		//获取clip类型
		virtual Type GetType() { return kAudio; }

		virtual bool	Open()override;
		virtual void	Close()override;

		void	Play();
		void	Seek(ovr::uint64 a_frame_index);

		wx_plugin::wxFrameBuffer*	GetFrame();
		void							PushFrame(wx_plugin::wxFrameBuffer* a_frame);

		//音量
		void	SetVolume(float a_value) { current_volume = a_value; }
		float	GetVolume()const { return current_volume; }

	private:
		OvrAudioGroupTrack*				own_group_track = nullptr;
		wx_plugin::IOvrAudioDecoder*	current_audio_decoder = nullptr;

		float		current_volume = 1.0f;
	};
}

#endif
