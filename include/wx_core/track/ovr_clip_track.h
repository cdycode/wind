﻿#ifndef OVR_PROJECT_OVR_CLIP_TRACK_H_
#define OVR_PROJECT_OVR_CLIP_TRACK_H_

#include <map>
#include "../ovr_core.h"
#include "ovr_track.h"
#include "ovr_clip.h"

namespace ovr_core
{
	class OVR_CORE_API OvrClipTrack : public OvrTrack
	{
	public:
		OvrClipTrack(OvrSequence* a_sequence);
		virtual ~OvrClipTrack();

	public:

		virtual void	Uninit()override;

		const std::map<ovr::uint64, OvrClipInfo*>& GetClipMap()const { return clip_map; }

		virtual OvrTrackType		GetTrackType() { return kTrackClip; }
		virtual ovr::uint64	GetAssetDuration(const OvrString& a_file_path) {return 0;}

		void				SetClipType(OvrClipInfo::Type a_type) { clip_type = a_type; }
		OvrClipInfo::Type	GetClipType()const { return clip_type; }

		bool				AddClip(const ovr::uint64 a_time, const OvrString& a_file_path, ovr::uint64 a_start_time, ovr::uint64 a_end_time);
		OvrClipInfo*		AddClip(const ovr::uint64 a_time);
		bool				CloneClip(const ovr::uint64 a_time, OvrClipInfo* a_clip);
		virtual void		RmvClip(const ovr::uint64 a_time);
		virtual bool		MoveClip(const ovr::uint64 a_old_time, const ovr::uint64 a_new_time);
		virtual OvrClipInfo* Clip(ovr::uint64 a_key);

		//根据帧索引获取clip信息
		OvrClipInfo*		GetClipByTime(ovr::uint64 a_frame_index);

		//是否支持拖入
		virtual bool		IsDragIn() { return true; }
		virtual bool		IsSupportDragIn(const OvrString& a_file_path) { return false; }

	protected:
		bool	FindCurrentClip(ovr::uint64 a_current_time);
		//重置clip
		void	ResetClip();

	protected:
		std::map<ovr::uint64, OvrClipInfo*> clip_map;

		OvrClipInfo::Type	clip_type = OvrClipInfo::kInvalid;

		//当前选中的Clip
		ovr::uint64		current_clip_begin_time = 0;	//在时间线上的时间
		ovr::uint64		current_clip_end_time = 0;		//时间线上的结束时间
		ovr::uint64		current_clip_start_time = 0;	//clip在文件中的开始时间
		OvrString		current_file_path;

		OvrClipInfo*	current_clip = nullptr;
	};
}

#endif
