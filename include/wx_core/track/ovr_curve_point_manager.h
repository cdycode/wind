﻿#ifndef OVR_PROJECT_OVR_CURVE_POINT_MANAGER_H_
#define OVR_PROJECT_OVR_CURVE_POINT_MANAGER_H_

#include <wx_core/track/ovr_track.h>
#include <wx_base/wx_vector2.h>
#include <wx_base/wx_vector3.h>

namespace ovr_core
{
	class OvrCurvePointManager;

	class OVR_CORE_API OvrCtlPoint
	{
	public:
		enum CTLMode { kCMLine, kCMLJump, kCMBezier };

		OvrCtlPoint(CTLMode a_mode, ovr::uint64 a_frame, float a_value);

		bool isUserBezier();
		bool isAutoBezier();
		bool isLine() { return point_mode == kCMLine; }
		bool isJump() { return point_mode == kCMLJump; }

		CTLMode point_mode;
		bool is_move_both = true;

		OvrVector2 cur_point;

		bool is_left_valid = false;
		OvrVector2 ctl_pt_left_value;

		bool is_right_valid = false;
		OvrVector2 ctl_pt_right_value;

	protected:
		bool updateCtlPointValue(const OvrVector2& a_left_value, const OvrVector2& a_right_value, bool a_is_change_left);
		bool setMode(CTLMode a_mode);
		bool makeDevorce();
		void moveTo(ovr::uint64 a_to);

		void setCtlPoint(const OvrVector2& a_pt, bool a_is_left);
		bool resetBezier();

		friend OvrCurvePointManager;
	};

	class OVR_CORE_API OvrCurvePointManager
	{
	public:
		OvrCurvePointManager();
		~OvrCurvePointManager();

		void addPoint(ovr::uint64 a_frame, float a_value);
		void RmvPoint(ovr::uint64 a_frame);
		bool MovePoint(ovr::uint64 a_from, ovr::uint64 a_to);

		bool makeDevorce(int a_frame);
		bool setMode(int a_frame, OvrCtlPoint::CTLMode a_mode);
		bool updateCtlPointValue(int a_frame, const OvrVector2& a_left_value, const OvrVector2& a_right_value, bool a_is_change_left);

		void SetDefaultValue(float a_default_value) { default_value = a_default_value; }
		float GetValue(ovr::uint64 a_frame_index);
		void  GetValues(ovr::uint64 a_frame_begin, ovr::uint64 a_frame_end, std::vector<float>& a_values);

		bool IsSelectedPoint() { return select_point_frame != -1; }
		int  GetSelectFrame() { return select_point_frame; }
		bool SetSelectFrame(int a_frame);

		OvrCtlPoint* getPoint(ovr::uint64 a_frame);
		void getPoints(int a_frame, OvrVector2& a_pt_current, OvrVector2& a_pt_prev, OvrVector2& a_pt_next);
		const std::map<ovr::uint64, OvrCtlPoint*>& getPointsMap() { return ctl_pt_map; }

		void initCurveLine(std::vector<OvrVector3>& a_lines, int a_min_x, int a_max_x, int a_data_pos);
		void pickKeyPoint(const std::vector<OvrVector3>& a_lines, std::map<ovr::uint64, OvrVector3>& a_points, ovr::uint64 a_offset);

		bool getXRange(int& a_min_frame, int& a_max_frame);
		float getYRange(float* a_min = nullptr, float* a_max = nullptr);

		void getControlPoints(int a_frame, OvrVector2& a_pt_left, OvrVector2& a_pt_right);
		void getCtlNormalLine(int a_frame, OvrVector2& a_pt_n, OvrVector2& a_pt_current);

		bool isChanged() { return is_changed; }
		void resetChanged() { is_changed = false; }
		void setChanged();

	protected:
		OvrVector2 getControlPoints(int a_frame, bool a_is_left);

		void  Reset();
		float CalcValue(ovr::uint64 a_frame_index);
		bool  GetSpecificValue(ovr::uint64 a_current_time, float& a_value);
		void  FindKeyRange(ovr::uint64 a_current_time);

		std::map<ovr::uint64, OvrCtlPoint*> ctl_pt_map;
		int select_point_frame = -1;

		float default_value = 1.0f;
		ovr::uint64		current_begin_time = 0;
		ovr::uint64		current_end_time = 0;
		OvrCtlPoint*	current_begin_pt = nullptr;
		OvrCtlPoint*	current_end_pt = nullptr;
		OvrVector2		ctl_pt_1, ctl_pt_2;

		bool is_changed = false;
	};
}

#endif

