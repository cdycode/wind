﻿#ifndef OVR_PROJECT_OVR_MARK_ACTOR_TRACK_H_
#define OVR_PROJECT_OVR_MARK_ACTOR_TRACK_H_

#include "../ovr_core.h"

namespace ovr_core
{
	class OVR_CORE_API OvrMarkTrack
	{
	public:
		OvrMarkTrack() {}
		virtual ~OvrMarkTrack() {}
		
		void AddKeyFrame(ovr::uint64 a_key, const OvrString& a_name);
		void RmvKeyFrame(ovr::uint64 a_key);

		const OvrString& GetKeyFrame(ovr::uint64 a_key);

		const std::map<ovr::uint64, OvrString>& GetKeyFrames() { return mark_keys; }

	protected:
		std::map<ovr::uint64, OvrString> mark_keys;
	};
}

#endif
