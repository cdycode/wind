﻿#ifndef OVR_PROJECT_OVR_SCENE_H_
#define OVR_PROJECT_OVR_SCENE_H_

#include <map>
#include "ovr_core.h"
#include <wx_engine/ovr_actor_axis.h>


namespace ovr_engine 
{
	class OvrAxisSystemWorld;
	class OvrGroundPlane;
	class OvrScene;
	class OvrCameraActor;
	class OvrLight;
	class OvrComponent;
}

namespace ovr_core
{
	class OvrSceneCamera;
	class OVR_CORE_API OvrSceneContext
	{
		friend class OvrArchiveScene;
	public:
		OvrSceneContext();
		~OvrSceneContext();

		bool	Init(const OvrString& a_scene_unique_name = OvrString::empty);
		void	Uninit();

		ovr_engine::OvrScene*	GetScene() { return scene; }

	public:
		void					SetFilePath(const OvrString& a_path) { file_path = a_path; }
		const OvrString&		GetFilePath()const { return file_path; }

		void					SetUniqueName(const OvrString& a_unique_name);
		const OvrString&		GetUniqueName()const;

		ovr_engine::OvrActor*	GetActorByUniqueName(const OvrString& a_unique_name);
		void					SetDisplayName(const char* a_name) { display_name = a_name; }
		const OvrString&		GetDisplayName()const { return display_name; }


		void				SetDescription(const char* a_desc) { description = a_desc; }
		const OvrString&	GetDescription()const { return description; }

		ovr_engine::OvrActor*	CreateActor(const OvrString& a_mesh_file_path);
		ovr_engine::OvrActor*	CreateActor();

		void					AppendSequence(const OvrString& a_sequence_unique_name);

		const std::map<OvrString, ovr_engine::OvrActor*>& GetActorList()const;
		ovr_engine::OvrActor*		GetRootActor();
		ovr::uint32					GetActorCount()const;
		const std::list<OvrString>&	GetSequenceNameList()const { return sequence_name_list; }

		void						CopyActor(const OvrString& a_actor_id);

		OvrSceneCamera*				GetSceneCamera() { return scene_camera; }


		/*在渲染线程中调用*/
		void	Update();
		void	Draw();

	private:
		ovr_engine::OvrScene*	scene = nullptr;

		OvrString	file_path;		//相对路径
		OvrString	display_name;
		OvrString	description;

		std::list<OvrString>				sequence_name_list;

		//场景的相机管理
		OvrSceneCamera*		scene_camera = nullptr;
	};
}

#endif
