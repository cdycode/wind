﻿#ifndef OVR_PROJECT_OVR_SEQUENCE_H_
#define OVR_PROJECT_OVR_SEQUENCE_H_

#include "ovr_core.h"

#include <map>
#include <list>
#include <wx_base/wx_type.h>
#include <wx_base/wx_string.h>
#include <wx_engine/ovr_object.h>

#include "./track/ovr_track.h"

namespace ovr_engine 
{
	class OvrActor;
}

namespace ovr_core
{
	class OvrActorTrack;
	class OvrBackgroundAudio;
	class OvrMarkTrack;
	class OVR_CORE_API OvrSequence : public OvrTrack
	{
		friend class OvrArchiveSequence;
	public:
		OvrSequence();
		virtual ~OvrSequence();

		virtual bool	Init() override;
		virtual void	Uninit() override;

		virtual bool	Update() override;
		virtual void	Seek(ovr::uint64 a_position) override;
		virtual void	Pause() override;
		virtual void	Play() override;

		void				SetFilePath(const OvrString& a_file_path) { file_path = a_file_path; }
		void				SetSceneUniqueName(const OvrString& a_unique_name) { scene_unique_name = a_unique_name; }
		void				SetDescription(const OvrString& a_description) { description = a_description; }

		ovr::uint64			GetCurrentFrame()const { return current_frame_index; }
		const OvrString&	GetDescription()const { return description; }
		const OvrString&	GetFilePath()const { return file_path; }
		const OvrString&	GetSenceUniqueName()const { return scene_unique_name; }

		//获得背景声轨道
		OvrBackgroundAudio*	GetBackgroundAudio() { return background_audio; }
		OvrMarkTrack*		GetMarkInfo() { return mark_info; }

		//修改播放位置 在渲染线程中调用
		void				ChangePlayPosition(ovr::uint64 a_new_position);

		/************************************************************************/
		/* 播放状态控制                                                          */
		/************************************************************************/

		void	SetLoopRange(ovr::uint64 a_loop_in, ovr::uint64 a_loop_out);
		bool	IsInLoopRange(ovr::uint64 a_seek_frame_index);
		void	ClearLoopRange();

		/************************************************************************/
		/* 成员变量赋值接口                                                      */
		/************************************************************************/
		void		SetDurtion(ovr::uint64 a_durtion) { duration = a_durtion; }
		void		SetFPS(ovr::uint16 a_fps) { fps = a_fps; }

		void				SetScene(OvrSceneContext* a_scene) { current_scene = a_scene; }

		ovr::uint64			GetDurtion()const { return duration; }
		ovr::uint16			GetFPS() { return fps; }
		float				GetSeconds(ovr::uint64 a_frame_count) { return a_frame_count*1.0f / fps; }
		ovr::uint64			GetFrames(float a_seconds) { return (ovr::uint64)(a_seconds*fps); }

		OvrSceneContext*	GetScene() { return current_scene; }

	public:
	
		OvrRenderStatus	GetCurrentStatus()const { return current_status; }

		void	SetRecord(bool a_is_record) { is_record = a_is_record; }
		bool	IsRecord()const { return is_record; }

	protected:
		//添加轨道
		virtual OvrTrack*	NewTrack(OvrTrackType a_track_type, const OvrString& a_tag) override;

	private:
		void	FillSupportTrackList();

	private:
		OvrString	file_path;
		OvrString	scene_unique_name;
		OvrString	work_dir;

		ovr::uint64					current_frame_index = 0;	//当前时间
		
		OvrString					description;

		OvrBackgroundAudio*			background_audio = nullptr;
		OvrMarkTrack*				mark_info = nullptr;


		ovr::uint64		duration = 0;
		ovr::uint16		fps = 0;

		OvrSceneContext*		current_scene = nullptr;

		//循环播放区
		bool			is_loop = false;
		ovr::uint64		loop_in = 0;
		ovr::uint64		loop_out = 0;

		bool			is_record = false;

		//当前的状态
		OvrRenderStatus	current_status = kRenderPause;
	};
}

#endif
