﻿#ifndef OVR_PROJECT_OVR_BACKGROUND_AUDIO_H_
#define OVR_PROJECT_OVR_BACKGROUND_AUDIO_H_

#include "ovr_core.h"

namespace wx_plugin
{
	class wxISound;
	class IOvrAudioDecoder;
}
namespace ovr_core 
{
	class OVR_CORE_API OvrBackgroundAudio 
	{
	public:
		OvrBackgroundAudio() {}
		~OvrBackgroundAudio() {}
		
		bool	Init();
		void	Uninit();

		void	Seek(ovr::uint64 a_frame_index);
		void	Pause();
		void	Play();

		bool	Update();

		//循环播放声音
		void	PlayLoop(const OvrString& a_audio_file_path, float a_blend_time, float a_is_volume);

		//播放声音 不进行循环播放
		void	PlayOnce(const OvrString& a_audio_file_path, float a_is_volume);
		
	private:
		struct BACKGROUND_AUDIO
		{
			wx_plugin::IOvrAudioDecoder* audio_decoder;
			float current_time = 0;
			float blend_time = 0;
			float total_time = 0;//音频总时长
			float already_play_time = 0; //已结播放时间
			bool  bloopfrom_0 = false;
			void Destroy();
		};
		static  int		FillAudioData(void* a_param, void* a_buffer, int a_buffer_size);
		static  void    Mixed(void *a_dest, void* a_src, int a_size, float a_percent);
	private:
		std::vector<BACKGROUND_AUDIO> background_file;
		wx_plugin::wxISound* sound;
		void    ClearAllAudio();
		float   last_time;
		bool	bloopfrom_0;
		
	};
}

#endif
