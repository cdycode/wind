#ifndef OVR_CORE_OVR_CORE_H_
#define OVR_CORE_OVR_CORE_H_

#ifdef _WIN32
#	ifdef WXCORE_EXPORTS
#		define OVR_CORE_API __declspec(dllexport)
#	else
#		define OVR_CORE_API __declspec(dllimport)
#	endif // OVRCORE_EXPORTS

//������
#	define	OVR_CORE_NAME "wxCore"
//�ܹ�
#	ifndef _WIN64
#		define OVR_CORE_PLATFORM "_x86"
#	else
#		define OVR_CORE_PLATFORM "_x64"
#	endif

//�汾
#	ifdef _DEBUG
#		define OVR_CORE_DEBUG "_d"
#	else 
#		define OVR_CORE_DEBUG ""
#	endif

#	ifndef WXCORE_EXPORTS
#		pragma comment( lib, OVR_CORE_NAME OVR_CORE_PLATFORM OVR_CORE_DEBUG ".lib")
#	endif

#else
#	define OVR_CORE_API 
#endif // _WIN32

#include <wx_base/wx_type.h>
#include <wx_base/wx_string.h>

namespace ovr_core 
{
	enum OvrRenderStatus
	{
		kRenderPause,	//��ͣ
		kRenderPlay,	//����
		kRenderSeek,	//seek
	};

	/************************************************************************/
	/* ����״̬�ص�															*/
	/************************************************************************/
	class OvrSceneContext;
	class OvrSequence;
	

	class OVR_CORE_API OvrCore 
	{
	public:
		static bool		Init();
		static void		Uninit();
		static OvrCore*	GetIns();

	private:
		OvrCore(){}
		~OvrCore() {}

	public:

		void				SetWorkDir(const OvrString& a_work_dir) { work_dir = a_work_dir; }
		const OvrString&	GetWorkDir()const { return work_dir; }

		bool	Open();
		void	Close();

		void	Update();

	private:
		OvrString			work_dir;
		
	};
}

#endif
