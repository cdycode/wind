#ifndef OVR_PROJECT_I_OVR_NODE_CLUSTER_H_
#define OVR_PROJECT_I_OVR_NODE_CLUSTER_H_

#include <map>
#include <wx_base/wx_string.h>
#include "../ovr_core.h"

namespace ovr_core
{
	class IOvrVisualNode;
	class OVR_CORE_API IOvrNodeCluster 
	{
	public:
		enum Type
		{
			kInvalid = 0,
			kEvent,
			kArea,
		};
	public:
		IOvrNodeCluster(OvrSequence* a_own_sequence):own_sequence(a_own_sequence){}
		virtual ~IOvrNodeCluster() {}

		OvrSequence*	GetOwnSequence() { return own_sequence; }

		//记录编辑器视口的位置
		void				SetViewScale(float a_value) { view_scale = a_value; }
		void				SetViewOffset(float a_x_position, float a_y_position) { view_x = a_x_position; view_y = a_y_position; }
		float				GetViewScale()const { return view_scale; }
		float				GetViewX()const { return view_x; }
		float				GetViewY()const { return view_y; }

		//设置文件路径
		void				SetFilePath(const OvrString& a_file_path);
		const OvrString&	GetFilePath()const { return file_path; }
		void				SetDisplayName(const OvrString& a_display_name) { display_name = a_display_name; }
		const OvrString&	GetDisplayName()const { return display_name; }

		//设置唯一名
		void				SetUniqueName(const OvrString& a_unique_name) { unique_name = a_unique_name; }
		const OvrString&	GetUniqueName()const { return unique_name; }

		virtual Type		GetType() = 0;

		virtual void	BeginPlay() =0;
		virtual void	EndPlay() = 0;
		virtual void	Update() = 0;

		virtual IOvrVisualNode*		CreateNode(const OvrString& a_node_id, const OvrString& a_unique_name = "") = 0;
		virtual IOvrVisualNode*		GetNode(const OvrString& a_node_unique_name) = 0;

		//销毁node  并断开与此Node相关的所有连接
		virtual bool				DestoryNode(IOvrVisualNode* a_node) = 0;

		//获取所有的Node
		virtual const std::map<OvrString, IOvrVisualNode*>&	GetAllNodeMap()const = 0;

		//获取Node中所有的资源文件
		virtual void AppendAssetFile(std::map<OvrString, bool>& a_file_map) = 0;

	protected:
		OvrString			file_path;
		OvrString			unique_name;
		OvrString			display_name;	//与文件名一致

		//记录编辑器视口的位置
		float				view_scale = 1.0f;
		float				view_x = 0.0f;
		float				view_y = 0.0f;

		OvrSequence*		own_sequence = nullptr;
	};
}

#endif
