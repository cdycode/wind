﻿#ifndef OVR_PROJECT_I_OVR_VISUAL_NODE_H_
#define OVR_PROJECT_I_OVR_VISUAL_NODE_H_

#include <list>
#include <wx_base/wx_string.h>
#include <wx_base/wx_vector3.h>
#include <wx_base/wx_vector4.h>
#include "../ovr_core.h"

namespace ovr_core 
{
	//定义Node接口
	struct OvrNodeInterface
	{
		OvrNodeInterface() {}
		OvrNodeInterface(bool a_is_in, const OvrString& a_name, const OvrString& a_unique_name)
			:name(a_name)
			, unique_name(a_unique_name)
			, is_in(a_is_in)
		{
		}

		void operator = (const OvrNodeInterface& a_interface)
		{
			is_in = a_interface.is_in;
			name = a_interface.name;
			unique_name = a_interface.unique_name;
		}

		OvrString	name;			//显示名
		OvrString	unique_name;	//唯一名
		bool		is_in = true;	//是否是输入
	};
	
	//定义Node的参数
	class OvrNodeParam
	{
	public:
		enum Type
		{
			kParamInvalid = 0,
			kParamInt,		//整数
			kParamFloat,	//float
			kParamBool,		//bool			0 -> false  1->true
			kParamString,	//字符串
			kParamVector3,	//OvrVector3 float  以冒号隔开的字符 比如：  0.0:0.0:0.0
			kParamVector4,	//OvrVector4 float  以冒号隔开的字符 比如：  0.0:0.0:0.0；0.0
			kParamResource,	//都是需要弹出对话框进行选择的  格式为：对话框选择名 和 资源后缀 如："选择材质:材质(*.mat)" //"选择声音:声音(*.mp3 *.wav)"
			kParamActor,	//也是字符串 Actor uniquename
			kParamSequence,	//Sequence的某个位置  是字符串  格式为: sequence uniquename+帧索引   中间以冒号隔开 
			kParamColor,	//设置颜色 参数为OvrVector4  float值  0~1
		};

	public:
		OvrNodeParam() {}
		OvrNodeParam(const OvrString& a_name, const OvrString& a_unique_name, Type a_type)
			:name(a_name)
			,unique_name(a_unique_name)
			, type(a_type)
		{}
		OvrNodeParam(const OvrString& a_name, const OvrString& a_unique_name, Type a_type, const OvrString& a_value)
			:name(a_name)
			, unique_name(a_unique_name)
			, type(a_type)
			, value(a_value)
		{}

		OvrNodeParam(const OvrString& a_name, const OvrString& a_unique_name, Type a_type, const OvrString& a_type_option, const OvrString& a_value, const OvrString& a_value_display = OvrString::empty)
			:name(a_name)
			, unique_name(a_unique_name)
			, type(a_type)
			, type_option(a_type_option)
			, value(a_value)
			, value_display(a_value_display)
		{}
		void operator = (const OvrNodeParam& a_param) 
		{
			name		= a_param.name;
			unique_name = a_param.unique_name;
			type	= a_param.type;
			value	= a_param.value;
			value_display = a_param.value_display;
		}

	public:
		OvrString			name;
		OvrString			unique_name;	//唯一名
		Type				type = kParamInvalid;
		OvrString			type_option;	// "选择材质:材质(*.mat)" //"选择声音:声音(*.mp3 *.wav)"
		OvrString			value;			// 真实的值
		OvrString			value_display;	// 用于显示的值  比如Actor的displayName，value存放UniqueName
	};

	//连接信息
	class IOvrVisualNode;
	struct OvrNodeConnect 
	{
		OvrNodeConnect() {}
		OvrNodeConnect(const OvrString& a_local_interface, IOvrVisualNode* a_other_node, const OvrString& a_other_interface)
			:local_interface(a_local_interface)
			,other_node(a_other_node)
			, other_interface(a_other_interface)
		{
		}
		void operator = (const OvrNodeConnect& a_connect) 
		{
			local_interface = a_connect.local_interface;
			other_node = a_connect.other_node;
			other_interface = a_connect.other_interface;
		}

		OvrString			local_interface;		//本地的接口
		IOvrVisualNode*		other_node = nullptr;	//相连的Node
		OvrString			other_interface;		//相连Node的接口
	};

	class OVR_CORE_API IOvrVisualNode
	{
	public:
		IOvrVisualNode() {}
		virtual ~IOvrVisualNode() {}

		void				SetNodeID(const OvrString& a_node_id) { node_id = a_node_id; }
		void				SetClassCategory(const OvrString& a_category) { category = a_category; }
		void				SetUniqueName(const OvrString& a_unique_name) { unique_name = a_unique_name; }
		void				SetDisplayName(const OvrString& a_display_name) { display_name = a_display_name; }
		const OvrString&	GetNodeID()const { return node_id; }
		const OvrString&	GetUniqueName()const { return unique_name; }
		const OvrString&	GetDisplayName()const { return display_name; }
		const OvrString&	GetNodeCategory() { return category; }

		//在编辑器中的坐标
		void		SetPosition(ovr::int32 a_x, ovr::int32 a_y) { position_x = a_x; position_y = a_y; }
		ovr::int32	GetPosistionX()const { return position_x; }
		ovr::int32	GetPosistionY()const { return position_y; }

		//播放前后的准备
		virtual void BeginPlay() = 0;
		virtual void EndPlay() = 0;
		//a_delta_time 距离上帧的时间 单位为秒
		//a_interface_name 指定连接本Node的哪个接口
		virtual bool Update(float a_delta_time, const OvrString& a_interface_name) = 0;

		//获取参数信息
		virtual const std::list<OvrNodeInterface>&	GetInterface()const = 0;
		virtual const std::list<OvrNodeParam>&		GetInParamList()const = 0;
		virtual const std::list<OvrNodeParam>&		GetOutParamList()const = 0;
		virtual const std::list<OvrNodeConnect>&	GetOutConnects()const = 0;
		virtual const std::list<OvrNodeConnect>&	GetInConnects()const = 0;

		virtual OvrNodeParam::Type	GetParamType(bool a_is_in, const OvrString& a_param_name)const = 0;

		//输出链接
		virtual bool	AddOutConnect(const OvrString& a_local_interface, IOvrVisualNode* a_other_node, const OvrString& a_other_interface) = 0;
		virtual bool	RmvOutConnect(const OvrString& a_local_interface) = 0;

		//断开所有连接
		virtual void	BreakAllConnect() = 0;

		// a_is_in 是输入参数 还是出参数
		// a_name 参数名
		// a_value 取值 内容根据参数的类型
		// 返回值 0 : 修改失败  1： 修改成功  2:修改成功 并需要更新接口
		virtual ovr::uint32	SetParamValue(bool a_is_in, const OvrString& a_name, const OvrString a_value) = 0;

		//获取Node中 将使用到的资源添加到a_file_map
		virtual void AppendAssetFile(std::map<OvrString, bool>& a_file_map) = 0;

	protected:
		OvrString						node_id;			//Node的标识
		OvrString						category;			//Node的分类
		OvrString						unique_name;		//唯一名
		OvrString						display_name;		//显示名

		//在编辑器中的坐标
		ovr::int32						position_x = 0;
		ovr::int32						position_y = 0;
	};
}

#endif
