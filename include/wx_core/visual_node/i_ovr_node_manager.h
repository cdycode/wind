﻿#ifndef OVR_PROJECT_I_OVR_NODE_MANAGER_H_
#define OVR_PROJECT_I_OVR_NODE_MANAGER_H_

#include <list>
#include <wx_base/wx_string.h>
#include "../ovr_core.h"
#include "i_ovr_node_cluster.h"

namespace ovr_core 
{
	class OvrNodeInfo 
	{
	public:
		OvrNodeInfo(const OvrString& a_node_id, const OvrString& a_category) 
			:node_id(a_node_id)
			,node_category(a_category)
		{
		}
		OvrNodeInfo(const OvrNodeInfo& a_node_info) 
		{
			node_id			= a_node_info.node_id;
			node_category	= a_node_info.node_category;
		}

		void operator = (const OvrNodeInfo& a_info) 
		{
			node_id			= a_info.node_id;
			node_category	= a_info.node_category;
		}

		OvrString	node_id;		//唯一的ID	不会再改变
		OvrString	node_category;	//唯一的分类 后期可能会变化
	};

	class OVR_CORE_API IOvrNodeManager 
	{
		friend class OvrEventTrack;
		friend class OvrInterActiveTrack;
	public:
		static bool				IsLoad();
		static bool				Load(const OvrString& a_scene_unique_name);
		static void				Unload();
		static IOvrNodeManager*	GetIns();

	public:

		//获取ActorNode的Class ID
		virtual const OvrString&			GetActorNodeID()const = 0;

		virtual const OvrString&			GetEventDir()const = 0;
		virtual const OvrString&			GetAreaDir()const = 0;

		//获取所有的node
		virtual const std::list<OvrNodeInfo>&	GetNodeIDList(IOvrNodeCluster::Type a_type)const = 0;

		virtual const std::list<OvrString>&	GetClusterFileList(IOvrNodeCluster::Type a_type)const = 0;

		//创建事件
		virtual bool				CreateCluster(IOvrNodeCluster::Type a_type, const OvrString& a_file_name) = 0;
		//加载和关闭事件
		virtual IOvrNodeCluster*	LoadCluster(IOvrNodeCluster::Type a_type, const OvrString& a_file_name, OvrSequence* a_own_sequence) = 0;
		virtual void				CloseCluster(IOvrNodeCluster* a_node_cluster) = 0;
		virtual void				SaveCluster(IOvrNodeCluster* a_node_cluster) = 0;
	protected:
		IOvrNodeManager() {}
		virtual ~IOvrNodeManager() {}
	};
}

#endif
