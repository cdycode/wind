﻿#ifndef OVR_PROJECT_OVR_SCENE_CAMERA_H_
#define OVR_PROJECT_OVR_SCENE_CAMERA_H_

#include "ovr_core.h"
#include <wx_base/wx_ray3f.h>
#include "wx_engine/ovr_ray_scene_query.h"
namespace ovr_engine 
{
	class OvrScene;
	class OvrActor;
	class RaySceneQuery;
}

namespace ovr_core 
{
	class OVR_CORE_API OvrSceneCamera
	{
	public:
		OvrSceneCamera();
		~OvrSceneCamera();

		//初始化
		bool	Init(ovr_engine::OvrScene*	a_scene);
		void	Uninit();

		void 	Reshape(int a_new_width, int a_new_height);
		ovr::uint32 	GetWidth()const{ return width;}
		ovr::uint32 	GetHeight()const{ return height;}

		//设置当前的相机
		bool						SetCurrentCamera(ovr_engine::OvrActor* a_camera);

		//获取当前相机
		ovr_engine::OvrActor*			GetCurrentCamera() { return current_camera;}
		ovr_engine::OvrCameraComponent*	GetCurrentCameraComponent() { return current_camera_component; }
		//获取漫游相机
		ovr_engine::OvrActor* GetRoamCamera() { return roam_camera_actor; }

		//获取选择到的Actor
		const ovr_engine::OvrRayIntersetResult*	GetFocusActor()const;

		//根据opengl屏幕坐标 获取射线
		OvrRay3f							GeneratePickRay(float a_x_pos, float a_y_pos);
		//根据opengl屏幕坐标 获取相交的Actor 
		//a_is_force true  可以对所有的mesh进行聚焦  fale 只对启用聚焦的mesh进行聚焦
		const ovr_engine::RayIntersetResults& RayInterset(float a_x_pos, float a_y_pos, bool a_is_force = false);


		/************************************************************************/
		/* 场景漫游相关接口														*/
		/* 按键W：MoveForward	按键S：MoveBackward								*/
		/* 按键A：MoveLeft		按键D：MoveRight									*/
		/* 在编辑和播放模式下，按键鼠标右键，并移动鼠标，才可以上下和左右转动camera	*/
		/************************************************************************/
		void	MoveForward();
		void	MoveBackward();
		void	MoveLeft();
		void	MoveRight();
		void	Yaw(float a_angle);
		void	Pitch(float a_angle);

	protected:
		bool	MoveForwardBack(const float a_distance);
		bool	MoveLeftRight(const float a_distance);

	private:
		ovr_engine::OvrScene*			current_scene = nullptr;
		ovr_engine::OvrActor*			roam_camera_actor = nullptr;

		//当前使用的相机
		ovr_engine::OvrActor*			current_camera  = nullptr;
		ovr_engine::OvrCameraComponent*	current_camera_component = nullptr;

		//用于相机射线求交
		ovr_engine::OvrRaySceneQuery*		ray_scene_query = nullptr;

		ovr::uint32 	width = 100;
		ovr::uint32 	height = 100;

		float			speed_move = 0.1f;	//移动速度
	};
}

#endif
