﻿#ifndef OVR_CORE_OVR_AUDIO_COMPONENT_H__
#define OVR_CORE_OVR_AUDIO_COMPONENT_H__

#include <wx_engine/ovr_component.h>
#include "../ovr_core.h"

namespace ovr_core 
{
	enum OvrCustomCompoentType
	{
		kComponentAudio = ovr_engine::kComponentUserDefined + 1,
	};

	class OVR_CORE_API OvrAudioComponent : public ovr_engine::OvrComponent
	{
	public:
		OvrAudioComponent(ovr_engine::OvrActor* a_actor);
		virtual ~OvrAudioComponent();

		virtual ovr_engine::OvrComponentType	GetType()override { return (ovr_engine::OvrComponentType)kComponentAudio; }
		virtual void				Update() override;

		void				SetAudioFile(const OvrString& a_audio_file) { audio_file_path = a_audio_file; }
		const OvrString&	GetAudioFile()const { return audio_file_path; }
	private:
		OvrString		audio_file_path;
	};
}

#endif
