﻿#ifndef WX_NETWORK_WX_I_TCP_TOY_SERVER_H_
#define WX_NETWORK_WX_I_TCP_TOY_SERVER_H_

#include "wx_network.h"
#include "wx_i_tcp_server_event.h"

//服务器 采用select实现
class WX_NETWORK_API wxITcpToyServer
{
public:
	//创建和销毁实例
	static wxITcpToyServer* CreateIns(void);
	static void DestoryIns(wxITcpToyServer*& a_ins);

public:
	wxITcpToyServer(void){}
	virtual ~wxITcpToyServer(void){}

	// 启动
	virtual bool Open(const char* a_listen_ip, unsigned short a_listen_port, wxITcpSvrEvent* a_tcp_event) = 0;

	// 停止
	virtual void Close(void) = 0;

	// 内存输出
	virtual void Dump(void) = 0;

	// 发送TCP包到目的端口
	virtual bool SendData(UINT64 a_client_id, const char* a_data, int a_data_len) = 0;

	//断开指定连接
	virtual bool Disconnect(UINT64 a_client_id) = 0;
};

#endif

