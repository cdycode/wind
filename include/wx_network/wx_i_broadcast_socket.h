﻿#ifndef WX_NETWORK_WX_I_BROADCAST_SOCKET_H_
#define WX_NETWORK_WX_I_BROADCAST_SOCKET_H_

#include "wx_network.h"

class wxIBroadcastEvent
{
public:
	wxIBroadcastEvent(void){}
	virtual ~wxIBroadcastEvent(void){}

	// 收到数据
	virtual void BroadcastRecvData(const char * a_pack_data, int a_pack_len, const wxSocketAddress& a_from_address) = 0;
};

class WX_NETWORK_API wxIBroadcastSocket
{
public:
	//创建和销毁实例
	static wxIBroadcastSocket* CreateIns(void);
	static void DestoryIns(wxIBroadcastSocket*& a_ins);

protected:
	wxIBroadcastSocket(void){}
	virtual ~wxIBroadcastSocket(void){}

public:
	//初始化和反初始化
	virtual bool Open(wxIBroadcastEvent* a_broadcast_event, unsigned short a_bind_port, unsigned short a_broadcast_port) = 0;
	virtual void Close(void) = 0;

	//发送广播数据
	virtual bool BroadcastData(const char* a_send_data,int a_data_len) = 0;

	//发送数据给指定客户端
	virtual bool SendData(const char* a_send_data,int a_data_len, const wxSocketAddress& a_dst_address) = 0;

	//定时日志输出
	virtual void Dump(void) = 0;

	//获取Socket绑定地址
	virtual bool GetLocalAddress(wxSocketAddress& a_dst_address) = 0;
};

#endif

