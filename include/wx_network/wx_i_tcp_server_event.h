﻿#ifndef WX_NETWORK_WX_TCP_SVR_EVENT_H_
#define WX_NETWORK_WX_TCP_SVR_EVENT_H_

#include "wx_network.h"

class wxITcpSvrEvent
{
public:
	wxITcpSvrEvent(void){}
	virtual ~wxITcpSvrEvent(void){}
public:
	
	//收到新的Socket
	virtual void OnNewClient(UINT64 a_client_id, const char* a_src_ip_address, unsigned short a_bind_Port) = 0;

	//断开连接
	virtual void OnBreakClient(UINT64 a_client_id) = 0;

	// 收到数据
	virtual void OnRecvData(UINT64 a_client_id, const char * a_pack_data, int a_pack_data_len) = 0;
};

#endif
