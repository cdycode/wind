﻿#ifndef WX_NETWORK_WX_NETWORK_H_
#define WX_NETWORK_WX_NETWORK_H_

#ifdef _WIN32

//使用静态库
#define WX_NETWORK_NAME "wxNetwork"
#ifdef WXNETWORK_EXPORTS
#	define WX_NETWORK_API _declspec(dllexport) 
#else
#	define WX_NETWORK_API _declspec(dllimport) 
#endif

//架构
#ifndef _WIN64
#	define WX_NETWORK_PLATFORM "_x86"
#else
#	define WX_NETWORK_PLATFORM "_x64"
#endif

//版本
#ifdef _DEBUG
#	define WX_NETWORK_DEBUG "_d"
#else 
#	define WX_NETWORK_DEBUG 
#endif

#ifndef WX_NETWORK_EXPORTS_SELF
#	pragma comment( lib, WX_NETWORK_NAME WX_NETWORK_PLATFORM WX_NETWORK_DEBUG ".lib")
#endif

#else
#define WX_NETWORK_API 
typedef int SOCKET;
#endif //#ifdef WIN32

#ifdef _WIN32
#include <Windows.h>
#endif

//UDP 包的大小就应该是 1492 - IP头(20) - UDP头(8) = 1464(BYTES) 
//TCP 包的大小就应该是 1492 - IP头(20) - TCP头(20) = 1452(BYTES) 
//数据包最大长度
#define DEF_NET_PACKET_MAX_LEN 1452

//定义IP地址最大长度(IPV4 点分十进制)
#define DEF_WX_IP_ADDRESS_MAX_LEN 16

class WX_NETWORK_API wxSocketAddress
{
public:
	wxSocketAddress(void);
    wxSocketAddress(const char* a_ip, unsigned short a_port);
    wxSocketAddress(unsigned int a_ip, unsigned short a_port);
    wxSocketAddress(const wxSocketAddress& a_address);
	~wxSocketAddress(void);

	//赋值
	void operator = (const wxSocketAddress& a_address);

	//判断是否相等
	bool operator == (const wxSocketAddress& a_address);

public:

	//将整数IP转为点分十进制
	static bool InetV4NToP(unsigned int a_numeric_binary, char a_text_presentation[DEF_WX_IP_ADDRESS_MAX_LEN]);

	//将点分十进制转为整数IP
	static bool InetV4PToN(const char* a_text_presentation, unsigned int& a_numeric_binary);

public:
	unsigned int	ip_address;  //[DEF_INS_IP_ADDRESS_MAX_LEN];	//IP地址
	unsigned short	port;								//端口
};

//初始化和反初始化网络库
bool WX_NETWORK_API InitInsNetwork();
void WX_NETWORK_API UninitInsNetwork();

namespace wxNetUtility
{
	//获取本机名字 aiBufferLen取值 建议为256
	bool WX_NETWORK_API GetLocalHostName(char* apBuffer, int aiBufferLen);

	//根据IP获取主机名
	bool WX_NETWORK_API GetHostNameByIp(char* apBuffer, int aiBufferLen, const wxSocketAddress& aoAddress);

	//获取指定机器名的IP地址 aiAddressCnt取值建议为16 即apIPAddress为16个元素的数组
	int WX_NETWORK_API GetHostAllIp(const char* apHostName, unsigned int* apIPAddress, int aiAddressCnt);

	//获取本机所有的地址
	int WX_NETWORK_API GetLocalHostAllIp(unsigned int* apIPAddress, int aiAddressCnt);
}

#endif



