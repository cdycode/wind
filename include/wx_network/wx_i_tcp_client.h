﻿#ifndef WX_NETWORK_WX_I_TCP_CLIENT_H_
#define WX_NETWORK_WX_I_TCP_CLIENT_H_

#include "wx_network.h"

class wxITcpClientEvent
{
public:

	wxITcpClientEvent(void){}
	virtual ~wxITcpClientEvent(void){}

	// 收到数据
	virtual void OnRecvData(char * a_pack_data, int a_pack_len) = 0;

	// 断开连接
	virtual void OnDisconnect(void) = 0;
};

//Tcp连接客户端
class WX_NETWORK_API wxITcpClient
{
public:
	//创建和销毁实例
	static wxITcpClient* CreateIns(void);
	static void DestoryIns(wxITcpClient*& a_ins);

public:
	wxITcpClient(void){}
	virtual ~wxITcpClient(void){}

	//初始化和反初始化
	virtual bool Open(wxITcpClientEvent* a_client_event, bool a_is_sync = true) = 0;
	virtual void Close(void) = 0;

	//发起和断开连接
    virtual bool Connect(const wxSocketAddress& a_address) = 0;
	virtual bool Connect(const char *a_svr_addr, unsigned short a_svr_port) = 0;
    virtual bool Connect(unsigned int a_svr_addr, unsigned short a_svr_port) = 0;

	virtual void DisConnect(void) = 0;

	//是否连接上
	virtual bool IsConneted(void)  = 0;

	//发送数据
	virtual bool SendData(const char* a_send_data,int a_data_len) = 0;

	//定时日志输出
	virtual void Dump(void) = 0;
};

#endif

