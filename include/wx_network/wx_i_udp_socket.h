﻿#ifndef WX_NETWORK_WX_I_UDP_SOCKET_H_
#define WX_NETWORK_WX_I_UDP_SOCKET_H_

#include "wx_network.h"

class wxIUdpRecvEvent
{
public:
	wxIUdpRecvEvent(void){}
	virtual ~wxIUdpRecvEvent(void){}

	// 收到数据
	virtual void UdpRecvData(const char * a_pack_data, int a_pack_len, const wxSocketAddress& a_from_address) = 0;
};


class WX_NETWORK_API wxIUdpSocket
{
public:
	//创建和销毁实例
	static wxIUdpSocket* CreateIns(void);
	static void DestoryIns(wxIUdpSocket*& a_ins);

protected:
	wxIUdpSocket(void){}
	virtual ~wxIUdpSocket(void){}

public:
	//初始化和反初始化
	virtual bool Open(wxIUdpRecvEvent* a_udp_socket_event, const char* a_bind_ip = 0, unsigned short ausBindPort = 0, int a_recv_buf_size = 2048) = 0;
	virtual void Close(void) = 0;

    virtual bool JoinGroup(char* a_group_ip) = 0;

	//发送广播数据
	virtual bool SendData(const char* a_send_data,int a_data_len, const wxSocketAddress& a_dst_address) = 0;

	//定时日志输出
	virtual void Dump(void) = 0;

	//获取Socket绑定地址
	virtual bool GetLocalAddress(wxSocketAddress& a_dst_address) = 0;
};

#endif
