#ifndef  OVR_BEZIER_CURVE_H_
#define  OVR_BEZIER_CURVE_H_
#include "wx_engine/ovr_engine_define.h"
#include "wx_base/wx_vector3.h"
#include <vector>

template<class TC, typename T>
void OvrBezier2(TC& ao_pt, const TC& a_begin_pt, const TC& a_ctrl_pt, const TC& a_end_pt, T t)
{
	ao_pt = a_begin_pt*((T(1) - t)*(T(1) - t)) + a_ctrl_pt*(T(2)*t*(T(1) - t)) + a_end_pt*(t*t);
}

template<class TC, typename T>
void OvrBezierDir2(TC& ao_pt, const TC& a_begin_pt, const TC& a_ctrl_pt, const TC& a_end_pt, T t)
{
	ao_pt = a_begin_pt*(T(2)*t - T(2)) + a_ctrl_pt*(T(2) - T(4)*t) + a_end_pt*(T(2)*t);
}


template<class TC, typename T>
void OvrBezier3(TC& ao_pt, const TC& a_begin_pt, const TC& a_ctrl_pt0, const TC& a_ctrl_pt1, const TC& a_end_pt, T t)
{
	T ft2 = t*t;
	T ft3 = ft2*t;
	T ft22 = (T(1) - t)*(T(1) - t);
	T ft23 = ft22*(T(1) - t);
	ao_pt = a_begin_pt*ft23 + a_ctrl_pt0*T(3)*t*ft22 + a_ctrl_pt1*T(3)*ft2*(T(1) - t) + a_end_pt*ft3;
}

template<class TC, typename T>
void OvrBezierDir3(TC& ao_pt, const TC& a_begin_pt, const TC& a_ctrl_pt0, const TC& a_ctrl_pt1, const TC& a_end_pt, T t)
{
	T ft1 = T(6)*t - T(3)*t*t - T(3);
	T ft2 = T(9)*t*t - T(12)*t + T(3);
	T ft3 = T(6)*t - T(9)*t*t;
	T ft4 = T(3)*t*t;
	ao_pt = a_begin_pt*ft1 + a_ctrl_pt0*ft2 + a_ctrl_pt1*ft3 + a_end_pt*ft4;
}


namespace ovr_engine {

	class  OvrCtrlPoint
	{
	public:
		enum CtrlPointType {
			e_point,
			e_quadricPoint,
			e_cubicPoint
		};
	public:
		OvrCtrlPoint() { ctrl_point_type = e_point; }
		OvrCtrlPoint(const OvrCtrlPoint& a_point)
		{
			ctrl_point_type = a_point.ctrl_point_type;
			pos = a_point.pos;
		}
		~OvrCtrlPoint() {}

		CtrlPointType ctrl_point_type;
		OvrVector3  pos;
	};


	class OvrBezierCurve
	{
	public:
		OvrBezierCurve();
		~OvrBezierCurve();
	public:
		bool AddPoint(const OvrCtrlPoint& a_point);
		bool GetPoint(int segment_id, float a_pos, OvrVector3& ao_point);
		bool GetTangent(int segment_id, float a_pos, OvrVector3& ao_tanget);
		bool GetPointTanget(int segment_id, float a_pos, OvrVector3& ao_point, OvrVector3& ao_tangent);
		bool CheckCurveState();
	private:
		int points_count;
		int segment_count;
		std::vector<OvrCtrlPoint>  ctrl_points;
		std::vector<int>          segment_begin_pt_ids;
	};
}

#endif