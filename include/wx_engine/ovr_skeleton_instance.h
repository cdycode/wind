#pragma once
#include "ovr_engine_define.h"
#include "wx_engine/ovr_skeleton.h"
#include "wx_asset_manager/ovr_motion_frame.h"
namespace ovr_engine
{
	class OVR_ENGINE_API OvrSkeletonInstance : public OvrSkeleton
	{
	public:
		OvrSkeletonInstance(OvrSkeleton* a_src);
		virtual ~OvrSkeletonInstance();
		OvrSkeleton*	GetSrcSkeleton() { return src_skeleton; }
		void            UpdateAnimation(ovr_asset::OvrMotionFrame* a_frame);
	private:
		OvrSkeleton*	src_skeleton;
	};
}