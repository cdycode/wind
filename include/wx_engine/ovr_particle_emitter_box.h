#ifndef __ovr_particle_emitter_box_h__20171228__
#define __ovr_particle_emitter_box_h__20171228__

#include "OvrParticleEmitter.h"
namespace ovr_engine {
	class OVR_ENGINE_API  OvrParticleEmitterBox : public OvrParticleEmitter
	{
	public:
		OvrParticleEmitterBox();
		OvrParticleEmitterBox(const OvrVector3& a_box_size) :box_size(a_box_size) {}
		~OvrParticleEmitterBox();
	public:
		DECLARE_CLASS(OvrParticleEmitterBox)
	public:
		virtual OvrParticleEmitterType GetParticleEmitterTyep()const override { return eParticleBox; }
		void  SetBoxSize(OvrVector3 a_size);
		const OvrVector3& GetBoxSize()const { return box_size; }
		virtual void EmitterParticle(OvrParticle* ao_particle);
	protected:
		OvrVector3	box_size;
	};
}
#endif
