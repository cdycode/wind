﻿#ifndef OVR_ENGINE_OVR_ACTOR_AXIS_H_
#define OVR_ENGINE_OVR_ACTOR_AXIS_H_

#include "ovr_engine_define.h"
#include <wx_base/wx_vector3.h>
#include <wx_base/wx_quaternion.h>
#include <wx_base/wx_plane3f.h>
#include <wx_base/wx_sphere3f.h>
#include "wx_base/wx_transform.h"
class lvr_axis_base;
namespace ovr_engine 
{
	enum ActorAxisType
	{
		kActorAxisInvalid	= 0,
		kActorAxisTransform	= 1,
		kActorAxisRotate	= 2,
		kActorAxisScale		= 3
	};
	enum ActorAxisName
	{
		kAxisNameInvalid = 0,
		kAxisNameX = 1,
		kAxisNameY	= 2,
		kAxisNameZ	= 3,
	};
	class OvrCameraComponent;
	class OVR_ENGINE_API OvrActorAxis
	{
		friend class OvrActor;
	public:
		static OvrActorAxis*	GetIns();
		static void				DelIns();

	private:
		OvrActorAxis();
		~OvrActorAxis();

	public:
		//需要在opengl线程中调用
		bool	Init();
		void	Uninit();
		void	SetType(ActorAxisType a_new_type);
		void	SetTransform(const OvrVector3& a_position, const OvrQuaternion& a_quaternion);

		void			MouseMove(OvrActor* a_camera, float a_x, float a_y);
		ActorAxisName	GetAxisName();

		void			BeginPressed(OvrActor* a_camera, OvrActor* a_actor, float a_x, float a_y);
		void			EndPressed();
		

		/************************************************************************/
		/* 以下函数需要在渲染线程中调用											*/
		/************************************************************************/
		void	Update(OvrCameraComponent* a_camera);
		void	Draw(const OvrMatrix4& a_vp);

	private:
		void	GeneratePane(OvrActor* a_camera);
		void	SetActorNewPos(const OvrVector3& a_move_pos);
		void	SetActorNewScale(const OvrVector3& a_move_pos);
		void	SetActorNewRotate(const OvrVector3& a_ray_origin, const OvrVector3& a_ray_dir, float a_move_x, float a_move_y);
	private:
		ActorAxisType	axis_type;

		lvr_axis_base*	current_axis;

		lvr_axis_base*	transform_axis;
		lvr_axis_base*	rotation_axis;
		lvr_axis_base*	scale_axis;
		//lvr_sphere3f	_help_sphere;//(hit_point1,hit_point1)
		OvrPlane3f		help_plane; //(look, point);
		OvrSphere3f     help_sphere;
		//fabs(look*axis) > fabs(look* axis1)
		//press down hit_point0;
		//move rt_point  ; (rt_point-hit_point0)*axis_dir;

		//用来进行移动、缩放、旋转
		bool			is_pressed = false;
		float			pressed_x = 0.0f;
		float			pressed_y = 0.0f;
		OvrVector3		start_pos;
		OvrTransform	default_transform;    
		OvrActor*		selected_actor = nullptr;

		OvrVector3		position;
		OvrTransform    world_transform;
		OvrMatrix4      world_transform_matrix;
	};
}

#endif
