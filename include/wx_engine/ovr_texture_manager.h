#pragma once
#include "wx_engine/ovr_engine_define.h"
#include "wx_engine/ovr_singleton.h"
#include "wx_engine/ovr_resource_manager.h"
#include <map>
#include "wx_engine/ovr_texture.h"
namespace ovr_engine
{
	class OVR_ENGINE_API OvrTextureManager : public OvrResourceManager, public OvrSingleton<OvrTextureManager>
	{
	public:
		OvrTextureManager();
		~OvrTextureManager();
		bool Initialise();
		void UnInitialise();
		OvrTexture* Load(const OvrString& a_file_name);
		OvrTexture* Create(const OvrString& a_name, OvrTextureType a_type,
			int a_width, int a_height, OvrPixelFormat a_format, int a_depth = 0,
			bool is_rtt = false);
		virtual OvrResource* CreateResource(OvrString a_name, bool is_manual) override;
		OvrTexture* GetDefaultTexture();
		OvrTexture* GetDefaultTransparentTexture();
	private:
		OvrTexture* base_white;
		OvrTexture* base_transparent;
	};
}