#pragma once
#include "wx_engine/ovr_primitive_component.h"
#include "wx_engine/ovr_bounding_box.h"
#define  TEXT_BACKGROUND_X_SIZE  0.5f
#define  TEXT_BACKGROUND_Y_SIZE  0.3f
namespace ovr_engine
{
	enum OvrAlignMethod {
		eTextAlignLeft = 0x01,
		eTextAlignMiddle = 0x02,
		eTextAlignRight = 0x04,
		eTextAlignUp = 0x08,
		eTextAlignDown = 0x10,
		/*eTextAlignLeftUp = eTextAlignLeft | eTextAlignUp,
		eTextAlighLeftMiddle = eTextAlignLeft | eTextAlignMiddle,
		eTextAlignLeftDown = eTextAlignLeft | eTextAlignDown,
		eTextAlignMiddleUp = eTextAlignMiddle |eTextAlignUp,
		eTextAlignMiddleMiddle = eTextAlignMiddle | eTextAlignMiddle,
		eTextAlignMiddleDown = eTextAlignMiddle | eTextAlignDown,
		eTextAlignRightUp = eTextAlignRight | eTextAlignUp,
		eTextAlighRightMiddle = eTextAlignRight | eTextAlignMiddle,
		eTextAlignRightDown = eTextAlignRight | eTextAlignDown,*/
		eTextAll
	};
	class OvrMaterial;
	class OvrTexture;
	class OvrTextInfo;
	class OVR_ENGINE_API OvrTextComponent : public OvrPrimitiveComponent
	{
		class Text :public OvrRenderable
		{
			friend OvrTextComponent;
		public:
			Text(OvrTextComponent* a_renderer);
			virtual ~Text();
			virtual OvrMatrix4   GetWorldTransform() const override;
			virtual OvrMaterial* GetMaterial() override;
			virtual void				PreRender(OvrScene* scene, OvrRenderSystem* driver)override;
			virtual void				PostRender()override;
			virtual void		 GetRenderOperation(OvrRenderOperation& a_op) override;
			virtual float GetSquaredViewDepth(const OvrCameraComponent* a_cam);

		private:
			OvrTextComponent* text_renderer;
			OvrVertexData* vertex_data;
			OvrIndexData* index_data;
		};

	public:
		OvrTextComponent(OvrActor* a_actor);
		virtual ~OvrTextComponent();
		OvrComponentType	GetType() override { return kComponentText; }
		virtual void		Update() override {}
		virtual void		UpdateRenderQueue(OvrRenderQueue* a_queue) override;
		virtual int			Raycast(const OvrRay3f& a_ray, float& a_distance) override;
		void				SetTextColor(const OvrVector4& a_color);//the last value w is transparency ,but you cannot set the value here ;
		void				SetTextMargin(const float& a_xMargin, const float& a_yMargin);//horizontal range: left ,middle, right; vertical range: up ,middle ,down;
		void				SetAlignMethod(const OvrAlignMethod& a_align_horizontal, const OvrAlignMethod & a_align_vertical);
		void				SetWarpWidth(const float& a_warp_width);    //the distance between one font and another ,it has no dimension;
		void				SetLineWarp(const float& a_line_warp);
		void				SetTextScale(const float& a_scale);  //Times of default text font size;
		void				SetBackgroundSize(const float x_size, const float y_size); //This is the scale of the x size and y size;
		void				SetTransparentValue(const float a_transparent);
		const OvrString&	GetText()const;
		const OvrVector4&	GetTextColor()const;
		void				GetTextMargin(float& a_xSize, float& a_ySize);
		const int			GetAlignHorizontalMethod()const;
		const int			GetAlignVerticleMethod()const;
		const float			GetWarpWidth();
		const float			GetLineWarp()const;
		const float			GetTextScale()const;
		void				GetBackgroundSize(float& x_size, float& y_size);
		float				GetTransparentValue();
		OvrMaterial*		GetMaterial();
		void                SetText(const OvrString& a_text);
		virtual OvrBox3f    GetBoundingBox() override;

	protected:
		void                GenerateTextMesh(float max_num);
	protected:
		int				align_method;
		int				align_horizontal;
		int				align_verticle;
		float			x_margin;
		float			y_margin;
		float			flont_spacing;
		float			line_spacing;
		float			text_scale;
		float			x_background_size;
		float			y_background_size;
		float			transparent_value;
		OvrString		text;
		OvrVector4		color;
		Text*           render_text;
		OvrMaterial*    material;
		OvrMaterial*    text_mtl;
		OvrBoundingBox* wrie_bounding_box;
		OvrBox3f        bounding_box;
		OvrTextInfo*     info;
	};
}