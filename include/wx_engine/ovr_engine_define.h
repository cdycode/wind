#ifndef OVR_ENGINE_OVR_ENGINE_DEFINE_H_
#define OVR_ENGINE_OVR_ENGINE_DEFINE_H_
#include "wx_base/wx_type.h"
#pragma warning(disable : 4251)
#ifdef OVR_OS_ANDROID
#	define OVR_ENGINE_API
#else
#ifdef WXENGINE_EXPORTS
#	define OVR_ENGINE_API __declspec(dllexport)
#else
#	define OVR_ENGINE_API __declspec(dllimport)
#endif // OVRENGINE_EXPORTS
#endif // OVR_OS_ANDROID
#include "wx_base/wx_base.h"
#include "wx_base/wx_string.h"

#endif