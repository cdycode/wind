#ifndef __ovr_particle_force_field_h__20171228__
#define __ovr_particle_force_field_h__20171228__

#include "ovr_particle_controller.h"
#include <vector>
namespace ovr_engine {
	class OvrParticleForceField : public OvrParticleController
	{
	public:
		OvrParticleForceField();
		~OvrParticleForceField();
	public:
		void AddForce(OvrVector3 a_dir, float a_strength);
	public:
		virtual bool update(OvrParticle* ao_particle, float a_delta_time);
	private:
		void RandomHorizontalTurbulence(double a_time);
	private:
		std::vector<OvrVector3>	force_direction;
		std::vector<float>			force_strength;
		uint32_t					force_num;
	};
}
#endif
