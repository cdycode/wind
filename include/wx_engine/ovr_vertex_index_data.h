#pragma once
#include "wx_base/wx_vector2.h"
#include "wx_base/wx_vector3.h"
#include "wx_base/wx_vector4.h"
#include "wx_base/wx_type.h"
#include "wx_engine/ovr_engine_define.h"
#include "wx_engine/ovr_hardware_buffer.h"
#include <vector>
#define PI	3.141592653
#define HALF_PI 1.5707963265
#define TWO_PI 6.283185306
namespace ovr_engine
{

	enum OvrVertexElementSemantic
	{
		VES_POSITION = 0,
		VES_NORMAL = 5,
		VES_TANGENT = 2,
		VES_BINORMAL = 9,
		VES_UV1 = 3,
		VES_COLOR = 4,
		VES_UV0 = 1,
		VES_JOINT_INDICES = 6,
		VES_JOINT_WEIGHTS = 7,
		VES_COUNT = 8,
		VES_NO_SUPPORT
	};

	enum OvrVertexElementType
	{
		VET_NONE = 0,
		VET_FLOAT1 = 1,
		VET_FLOAT2 = 2,
		VET_FLOAT3 = 3,
		VET_FLOAT4 = 4,
		VET_HALF1 = 5,
		VET_HALF2 = 6,
		VET_HALF3 = 7,
		VET_HALF4 = 8,
		VET_UBYTE4 = 9,
		VET_SHORT1 = 10,
		VET_SHORT2 = 11,
		VET_SHORT4 = 12,
		VET_INT1 = 13,
		VET_INT2 = 14,
		VET_INT3 = 15,
		VET_INT4 = 16,
		VET_COUNT
	};

	class OVR_ENGINE_API OvrVertexElement
	{
	public:
		ovr::uint32 offset;
		OvrVertexElementType type;
		OvrVertexElementSemantic semantic;
	};

	class OVR_ENGINE_API OvrVertexDeclaration
	{
	public:
		typedef std::vector<OvrVertexElement> VertexElementList;
		OvrVertexDeclaration();
		virtual ~OvrVertexDeclaration();
		void AddElement(ovr::uint32 a_offset, OvrVertexElementType a_type, OvrVertexElementSemantic a_semantic);
		void InsertElement(ovr::uint32 a_pos, ovr::uint32 a_offset, OvrVertexElementType a_type, OvrVertexElementSemantic a_semantic);
		void RemoveAllElements();
		void RemoveElement(ovr::uint16 a_elem_index);
		void RemoveElement(OvrVertexElementSemantic& a_semantic);
		void ModifyElement(ovr::uint16 a_elem_index, ovr::uint32 offset, OvrVertexElementType a_type, OvrVertexElementSemantic a_semantic);
		const OvrVertexElement* FindElementBySemantic(OvrVertexElementSemantic a_sem) const;
		ovr::uint32 GetElementCount();
		const VertexElementList& GetElements() const { return elements; }
		const OvrVertexElement* GetElement(ovr::uint16 index) const;
		virtual void NotifyChanged() {}
	protected:
		VertexElementList elements;
	};

	class OvrHardwareBufferManager;
	class OVR_ENGINE_API OvrVertexData
	{
	public:
		OvrVertexData();
		virtual ~OvrVertexData();
		OvrHardwareVertexBuffer* vertex_buffer;
		OvrVertexDeclaration* declaration;
		OvrHardwareBufferManager* manager;
	};

	class OVR_ENGINE_API OvrIndexData
	{
	public:
		OvrIndexData();
		virtual ~OvrIndexData() {}
		OvrHardwareIndexBuffer* index_buffer;
	};
}
struct OvrTriangleImpl
{
	unsigned int A, B, C;
	OvrTriangleImpl(unsigned int a_A, unsigned int a_B, unsigned int a_C)
	{
		A = a_A;
		B = a_B;
		C = a_C;
	}
	OvrTriangleImpl() {}
};


struct StaticMeshVertex
{
	OvrVector3 pos;
	OvrVector2 uv0;
	OvrVector3 normal;
	OvrVector3 tangent;
	OvrVector3 binormal;
	ovr::uint8 color[4];
};

struct SkeletalMeshVertex
{
	OvrVector3 pos;
	OvrVector2 uv0;
	OvrVector3 normal;
	OvrVector3 tangent;
	OvrVector3 binormal;
	OvrVector4 weights;
	ovr::uint8 indices[4];
};


struct TempMeshVertex
{
	OvrVector3 pos;
	OvrVector3 normal;
	OvrVector2 uv;
};

