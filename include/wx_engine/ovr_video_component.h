#pragma once

#include "wx_engine/ovr_primitive_component.h"
#include "wx_engine/ovr_render_queue.h"
class lvr_movie_render;
namespace ovr_engine
{
	enum MovieType
	{
		e_movie_type_unknown = 0,
		e_movie_type_common_2d = 1,
		e_movie_type_common_2d_full,
		e_movie_type_left_right_3d,
		e_movie_type_left_right_3d_full,
		e_movie_type_right_left_3d,
		e_movie_type_right_left_3d_full,
		e_movie_type_top_bottom_3d,
		e_movie_type_top_bottom_3d_full,
		e_movie_type_bottom_top_3d,
		e_movie_type_bottom_top_3d_full,
		e_movie_type_pano_360,//11
		e_movie_type_pano_360_LR,
		e_movie_type_pano_360_RL,
		e_movie_type_pano_360_TD,
		e_movie_type_pano_360_DT,
		e_movie_type_pano_180,
		e_movie_type_pano_180_LR,
		e_movie_type_pano_180_RL,
		e_movie_type_pano_180_TD,
		e_movie_type_pano_180_DT,
		e_movie_type_pano_cb,
		e_movie_type_pano_cb_LR,
		e_movie_type_pano_cb_RL,
		e_movie_type_pano_cb_TD,
		e_movie_type_pano_cb_DT,
		e_movie_type_movie_count
	};

	enum VideoType
	{
		Video360Box,
		Video360,
		Video180,
		Video3D,
		Video2D,
	};

	class OvrScene;
	class OvrBoundingBox;
	class OvrGLMediaTexture;
	class OVR_ENGINE_API OvrVideoComponent : public OvrPrimitiveComponent
	{
	public:
		
		class Screen : public OvrRenderable
		{
		public:
			Screen(OvrVideoComponent* a_renderer);
			~Screen();
			virtual OvrMatrix4 GetWorldTransform() const override;
			virtual OvrMaterial* GetMaterial() override;
			virtual void	GetRenderOperation(OvrRenderOperation& a_op);
		public:
			OvrVertexData*  vertex_data;
			OvrIndexData*   index_data;
			OvrVideoComponent* video_renderer;
		};
	public:
		static bool		GenerateTexMatInfo(MovieType a_format, OvrVector4 ao_texmat[2]);
	public:
		OvrVideoComponent(OvrActor* a_actor);
		virtual ~OvrVideoComponent();
		virtual OvrComponentType	GetType() { return kComponentVideo; }
		VideoType           GetVideoType() { return video_type; }
		void 	            SetVideoType(VideoType a_type);
		virtual void		Update();
		virtual int			Raycast(const OvrRay3f& a_ray, float& a_distance);

		//set outside
		void				SetMovieType(MovieType a_movie_type);
		MovieType			GetMovieType();
		virtual void		UpdateRenderQueue(OvrRenderQueue* a_queue) override;
		OvrMaterial*		GetMaterial();
		void				SetKeepMovieSize(bool a_keep_movie_size);
		bool				Build();
		virtual OvrBox3f    GetBoundingBox() override;

	protected:
		void				SetScreen(Screen* a_screen);
		Screen*				GenerateRectMesh(float a_width, float a_height);//should between 0-pi/2
		Screen*				GeneratePiMesh(float a_horizontal_angle, float a_view_angle);//should between 0-pi/2
		Screen*				GenerateCubeBoxMesh(float a_radius);
		Screen*				GenerateCubeSphereMesh(float a_radius);
	protected:
		bool				is_build = false;
		MovieType			movie_type = e_movie_type_unknown;
		OvrVector4			texture_mat[2];//work with the movie type
		bool				is_change_movie_type;
		Screen*             screen;
		VideoType           video_type;
		OvrMaterial*        material;
		OvrBoundingBox*		wrie_bounding_box;
		OvrBox3f			bounding_box;
	};
}
