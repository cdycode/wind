#pragma once
#include "wx_engine/ovr_primitive_component.h"
namespace ovr_engine
{
	class OVR_ENGINE_API OvrShapeComponent : public OvrPrimitiveComponent
	{
	public:
		OvrShapeComponent(OvrActor* a_actor);
		virtual ~OvrShapeComponent();
	};
}