﻿#ifndef OVR_ENGINE_OVR_STATOC_MESH_ACTOR_H_
#define OVR_ENGINE_OVR_STATOC_MESH_ACTOR_H_

#include "wx_engine/ovr_engine_define.h"
#include "wx_base/wx_box3f.h"
#include "wx_base/wx_matrix4f.h"
#include "wx_engine/ovr_render_queue.h"
#include "wx_engine/ovr_vertex_index_data.h"
namespace ovr_engine 
{
	class OvrBoundingBox : public OvrRenderable
	{
	public:
		OvrBoundingBox(OvrActor* a_actor);
		~OvrBoundingBox();
		virtual OvrMatrix4   GetWorldTransform() const override;
		virtual OvrMaterial* GetMaterial() override;
		void				 SetupBoundingBox(const OvrBox3f& a_box);
		const OvrBox3f&		 GetBoundingBox()const { return bounding_box; }
		virtual void		 GetRenderOperation(OvrRenderOperation& a_op) override;
	private:
		void                 BuildBoundingBoxMesh(const OvrBox3f& a_box);
		void                 Reset();
	private:
		OvrVertexData*      vertex_data;
		OvrIndexData*       index_data;
		OvrBox3f			bounding_box;
		OvrActor*           parent_actor;
	};
}

#endif
