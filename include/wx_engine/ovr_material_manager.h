#pragma once
#include "wx_base/wx_string.h"
#include "wx_engine/ovr_singleton.h"
#include "wx_engine/ovr_resource_manager.h"
#include "wx_engine/ovr_engine_define.h"
#include <map>
namespace ovr_engine 
{
	class OvrMaterial;
	class OVR_ENGINE_API OvrMaterialManager : public OvrResourceManager, public OvrSingleton<OvrMaterialManager>
	{
	public:
		OvrMaterialManager();
		virtual ~OvrMaterialManager();
		bool Initialise();
		void UnInitialise();
		OvrMaterial* Load(const OvrString& a_file_name);
		OvrMaterial* Create(const OvrString& a_name);
		OvrMaterial* GetDefaultMaterial(bool is_light = true);
		virtual OvrResource* CreateResource(OvrString a_name, bool is_manual) override;

	};
}
