﻿#pragma once
#include "ovr_engine_define.h"
#include "wx_base/wx_vector4.h"
#include "wx_base/wx_matrix4f.h"
#include "wx_base/wx_box3f.h"
#include "wx_base/wx_color_value.h"
#include "wx_engine/ovr_gpu_program_parameters.h"
#include "wx_engine/ovr_material.h"
#include "wx_engine/ovr_texture.h"
#include "wx_engine/ovr_render_queue.h"
namespace ovr_engine
{
	class OVR_ENGINE_API OvrRenderSystem
	{
	public:
		struct Viewport
		{
			int x, y, x_size, y_size;
			Viewport()
				:x(0), y(0), x_size(0), y_size(0) {}
			Viewport(int a_x, int a_y, int a_x_size, int a_y_size)
				:x(a_x), y(a_y), x_size(a_x_size), y_size(a_y_size) {}
		};

		OvrRenderSystem();
		virtual~OvrRenderSystem();
		virtual bool Initialise() = 0;
		virtual void UnInitialise() = 0;
		virtual bool BeginScene(bool a_backBuffer, bool a_zBuffer, OvrColorValue a_color, Viewport a_rect) = 0;
		virtual bool EndScene() = 0;
		virtual void BindGpuProgram(OvrGpuProgram* a_program);
		virtual void UnBindGpuProgram() = 0;
		virtual void BindGpuProgramParameters(OvrGpuProgramParameters* params, ovr::uint16 mask) = 0;
		virtual void SetSceneBlend(SceneBlendFactor a_src, SceneBlendFactor a_dest) = 0;
		virtual void SetDepthCheckEnable(bool a_enable) = 0;
		virtual void SetDepthWriteEnable(bool a_enable) = 0;
		virtual void SetCullMode(CullingMode a_mode) = 0;
		virtual void Draw3DLine(const std::vector<OvrVector3>& a_vertices, const OvrMatrix4& a_view, const OvrMatrix4& a_proj, const float a_width,
			const OvrColorValue a_color = OvrColorValue(255, 255, 255, 255), bool is_strip = true) = 0;
		virtual void Draw3DPoint(const std::vector<OvrVector3>& a_vertices, const OvrMatrix4& a_view, const OvrMatrix4& a_proj, const float a_size,
			const OvrColorValue a_color = OvrColorValue(255, 255, 255, 255)) = 0;
		virtual void SetViewPort(const Viewport& a_v);
		virtual void Render(OvrRenderOperation& a_op) = 0;
		virtual const Viewport&		GetViewPort();
		virtual void ClearBuffers(bool backBuffer, bool zBuffer, bool stencilBuffer, OvrColorValue color) = 0;
	protected:
		Viewport    view_port;
		OvrGpuProgram* program;
	};
}
