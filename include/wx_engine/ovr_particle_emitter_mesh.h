#ifndef __ovr_particle_emitter_mesh_h__20171228__
#define __ovr_particle_emitter_mesh_h__20171228__

#include "OvrParticleEmitter.h"
namespace ovr_engine {
	class OVR_ENGINE_API OvrParticleEmitterMesh : public OvrParticleEmitter
	{
	public:
		OvrParticleEmitterMesh();
		~OvrParticleEmitterMesh();
	public:
		DECLARE_CLASS((OvrParticleEmitterMesh))
	public:
		virtual OvrParticleEmitterType GetParticleEmitterTyep()const override { return eParticleMesh; }
		virtual void EmitterParticle(OvrParticle* ao_particle);
	};
}
#endif
