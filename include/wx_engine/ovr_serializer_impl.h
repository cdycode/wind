#pragma once
#include <wx_asset_manager/ovr_archive_mesh.h>
#include <wx_asset_manager/ovr_archive_asset_manager.h>
#include "wx_engine/ovr_engine_define.h"
#include "wx_engine/ovr_material.h"
#include "wx_engine/ovr_gpu_program_parameters.h"
#include "wx_engine/ovr_texture.h"
#include "wx_engine/ovr_singleton.h"
namespace ovr_engine
{
	class OvrMesh;
	class OvrSkeleton;
	class OvrMaterial;
	class OvrGpuProgram;
	class OVR_ENGINE_API OvrSerializerImpl : public OvrSingleton<OvrSerializerImpl>
	{
	public:
		OvrSerializerImpl();
		~OvrSerializerImpl();
		bool ImportMesh(OvrString a_file_name, OvrMesh* dest);
		bool ImportSkeleton(OvrString a_file_name, OvrSkeleton* dest);
		bool GetImageInfo(OvrString a_file_name, OvrImage& a_info);
		bool ImportMaterial(OvrString a_file_name, OvrMaterial* dest);
		bool ExportMaterial(OvrMaterial* src);
		bool ExportMaterial(const OvrString& a_file_name, OvrMaterial* src);
	private:
		OvrString GetFullPath(const OvrString& path);
		SceneBlendFactor GetSceneBlendFactor(const OvrString& str);
		OvrString GetSceneBlendString(const SceneBlendFactor& fac);
	private:
		ovr_asset::OvrArchiveAssetManager* manager;
	};
}