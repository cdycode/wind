#ifndef __ovr_particle_h__20171228__
#define __ovr_particle_h__20171228__

#include <cstdlib>
#include "wx_engine/ovr_engine_define.h"
#include "wx_base/wx_vector4.h"
#include "wx_base/wx_quaternion.h"

namespace ovr_engine {
	class OvrParticle
	{
	public:
		OvrParticle();
		~OvrParticle();;
	public:
		OvrVector3	offset_pos;
		OvrVector3	position;
		OvrQuaternion	rotation_q;
		OvrVector3	cur_v;
		OvrVector3	direction;
		OvrVector4	color;
		float			scale_size;//in common,we only have one scale.
		float			life_now;
		float           rest_life;
		float			life_total;
		float			weight;//in kg,work with force.
	};
}

#endif
