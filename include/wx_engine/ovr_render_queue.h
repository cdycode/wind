﻿#pragma once
#include <map>
#include <vector>
#include "wx_engine/ovr_renderable.h"
#include "wx_base/wx_type.h"
#include "wx_base/wx_matrix4f.h"
#define OVR_RENDERABLE_DEFAULT_PRIORITY  100

namespace ovr_engine
{
	class OvrMaterial;
	class OvrSkeleton;
	class OvrMeshBuffer;
	class OvrActor;
	class IOvrRenderSystem;
	class OvrScene;
	class IOvrRenderObject;
	class IOvrTextureBuffer;
	class OvrCameraComponent;

	enum OrganisationMode
	{
		OM_PASS_GROUP = 1,
		OM_SORT_DESCENDING = 2,
	};
	class OvrRenderableCollectorVistor;
	class OvrRenderableCollector
	{
	public:	
		struct DepthSortDescendingLess
		{
			const OvrCameraComponent* camera;
			DepthSortDescendingLess(const OvrCameraComponent* cam)
				: camera(cam) {}
			bool operator()(const RenderablePass& a, const RenderablePass& b) const;
		};
		OvrRenderableCollector();
		typedef std::vector<OvrRenderable*> RenderableList;
		typedef std::vector<RenderablePass> RenderablePassList;
		typedef std::map<OvrMaterial*, RenderableList> PassGroupRenderableMap;
		void AddRenderable(OvrMaterial* material, OvrRenderable* rnd);
		void AddOrganisationMode(OrganisationMode a_om);
		void ResetOrganisationMode(OrganisationMode a_om);
		void Sort(OvrCameraComponent* a_cam);
		void Clear();
		void AcceptVisitor(OvrRenderableCollectorVistor* a_vistor, OrganisationMode a_om) const;
	protected:
		void AcceptVisitorGrouped(OvrRenderableCollectorVistor* a_vistor) const;
		void AcceptVisitorDescending(OvrRenderableCollectorVistor* a_vistor) const;
	public:
		PassGroupRenderableMap grouped;
		RenderablePassList sorted_descending;
		ovr::uint8 organisation_mode;
	};

	class OvrRenderQueue;
	class RenderPriorityGroup
	{
	public:
		RenderPriorityGroup(OvrRenderQueue* a_parent);
		void AddRenderable(OvrRenderable* a_rend);
		void AddOrganisationMode(OrganisationMode a_om);
		void AddSolidRenderable(OvrMaterial* a_material, OvrRenderable* a_rend);
		void AddTransparentRenderable(OvrMaterial* a_material, OvrRenderable* a_rend);
		OvrRenderableCollector* GetSolids() { return &opaques; }
		OvrRenderableCollector* GetTransparents() { return &transparents; }
		void Clear();
	private:
		OvrRenderQueue* parent;
		OvrRenderableCollector opaques;
		OvrRenderableCollector transparents;

	};
	class OvrRenderQueue
	{
	public:
		typedef std::map<ovr::uint16, RenderPriorityGroup*> RenderPriorityGroupMap;
	public:
		OvrRenderQueue();
		~OvrRenderQueue() {}
		void Clear();
		void AddRenderable(OvrRenderable* rnd);
		void AddRenderable(OvrRenderable* rnd, ovr::uint16 a_priority);
		RenderPriorityGroup* GetRenderPriorityGroup(ovr::uint16 a_id);
		RenderPriorityGroupMap& GetQueuePriorityGroupMap();
	private:
		std::map<ovr::uint16, RenderPriorityGroup*> groups;
		ovr::uint16 default_renderable_priority;
	};

	class OvrRenderableCollectorVistor
	{
	public:
		virtual void visit(OvrRenderable* a_rnd) = 0;
		virtual void visit(RenderablePass* a_rnd_pass) = 0;
		virtual void visit(OvrMaterial* material) = 0;
	};
}