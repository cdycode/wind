#pragma once
#include "wx_engine/ovr_primitive_component.h"
#include "wx_engine/ovr_bounding_box.h"
#define  TEXT_BACKGROUND_X_SIZE  0.5f
#define  TEXT_BACKGROUND_Y_SIZE  0.3f
namespace ovr_engine
{
	class OvrMaterial;
	class OvrTexture;
	class OVR_ENGINE_API OvrImageComponent : public OvrPrimitiveComponent
	{
		class Image : public OvrRenderable
		{
			friend OvrImageComponent;
		public:
			Image(OvrImageComponent* a_renderer);
			virtual ~Image();
			virtual OvrMatrix4   GetWorldTransform() const override;
			virtual OvrMaterial* GetMaterial() override;
			virtual void		 GetRenderOperation(OvrRenderOperation& a_op) override;
			virtual float GetSquaredViewDepth(const OvrCameraComponent* a_cam);

		private:
			OvrImageComponent* text_renderer;
			OvrVertexData* vertex_data;
		};
	public:
		OvrImageComponent(OvrActor* a_actor);
		virtual ~OvrImageComponent();
		OvrComponentType	GetType() override { return KComponentImage; }
		virtual void		Update() override {}
		virtual void		UpdateRenderQueue(OvrRenderQueue* a_queue) override;
		virtual int			Raycast(const OvrRay3f& a_ray, float& a_distance) override;
		void				SetBackgroundTexture(OvrTexture* a_tex);
		void				SetBackgroundSize(const float x_size, const float y_size); //This is the scale of the x size and y size;
		void				SetTransparentValue(const float a_transparent);
		OvrTexture*			GetBackgroundTexture();
		void				GetBackgroundSize(float& x_size, float& y_size);
		float				GetTransparentValue();
		OvrMaterial*		GetMaterial();
		virtual OvrBox3f    GetBoundingBox() override;
	protected:
		void				GenerateImageMesh(float a_width, float a_height);
	protected:
		float			x_background_size;
		float			y_background_size;
		float			transparent_value;
		Image*		    back_board;
		OvrMaterial*    material;
		OvrBoundingBox* wrie_bounding_box;
		OvrBox3f        bounding_box;
	};
}