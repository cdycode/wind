#ifndef __ovr_particle_emitter_circle__h__20171228__
#define __ovr_particle_emitter_circle__h__20171228__

#include "OvrParticleEmitter.h"
namespace ovr_engine {
	class OVR_ENGINE_API OvrParticleEmitterCircle : public OvrParticleEmitter
	{
	public:
		OvrParticleEmitterCircle(float a_radius) :radius(a_radius),OvrParticleEmitter() {}
		OvrParticleEmitterCircle();
		~OvrParticleEmitterCircle();
	public:
		DECLARE_CLASS(OvrParticleEmitterCircle)
	public:
		virtual OvrParticleEmitterType GetParticleEmitterTyep()const override { return eParticleCircle; }
		virtual void EmitterParticle(OvrParticle* ao_particle);
	public:
		void SetRadius(float a_radius) { radius = a_radius; }
		const float GetRadius()const { return radius; }
	private:
		float		radius;
	};
}

#endif
