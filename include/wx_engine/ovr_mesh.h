#pragma once
#include <vector>
#include <map>
#include "wx_engine/ovr_resource.h"
#include "wx_engine/ovr_skeleton.h"
#include "wx_engine/ovr_engine_define.h"
#include "wx_engine/ovr_mesh_buffer.h"
namespace ovr_engine
{
	class OvrActor;
	class OvrMeshManager;
	class OvrPose;
	class OVR_ENGINE_API OvrMesh : public OvrResource
	{
		friend OvrMeshManager;
	public:
		typedef  std::vector<OvrMeshBuffer*> MeshBufferList;
		typedef  std::vector<OvrPose*> PoseList;
		typedef  std::map<OvrString, ovr::uint32>	NameBufferMap;
	public:
		const OvrString& GetName() { return name; }

		void			SetMaterialName(OvrString a_material_name);
		void			SetMaterialName(ovr::uint32 a_index, const OvrString& a_material_name);
		OvrString		GetMaterialName(ovr::uint32 a_index);

		OvrMeshBuffer*	CreateMeshBuffer(OvrString a_name);
		OvrMeshBuffer*	CreateMeshBuffer();
		OvrMeshBuffer*	GetMeshBuffer(ovr::uint32 a_index);
		OvrMeshBuffer*	GetMeshBuffer(OvrString a_name);
		ovr::uint32		GetMeshBufferIndex(const OvrString& a_name) const;
		ovr::uint32		GetMeshBufferCount();

		void			SetBounds(const OvrBox3f& a_box) { aabb3 = a_box; }
		OvrBox3f&		GetBounds() { return aabb3; }

		void			SetSkeletonName(OvrString a_name);
		bool			HasSkeleton();
		OvrSkeleton*	GetSkeleton();

		void				SetPoseName(OvrString a_name);
		bool				HasPose();
		OvrPose*			GetPose(ovr::uint32 a_index);
		const OvrString&	GetPoseName() { return pose_name; }
		const PoseList&		GetPoseList();
		ovr::uint32			GetPoseCount();
		OvrPose*            CreatePose(ovr::uint32 handle, OvrString a_name);
		OvrPose*            CreatePose(OvrString a_target_name, OvrString a_name);

	protected:	
		OvrMesh(OvrString a_name, bool a_is_manual);
		virtual ~OvrMesh();
		virtual void LoadImpl() override;
		virtual void UnLoadImpl()override;
	private:
		OvrString		skeleton_name;
		OvrSkeleton*	skeleton;
		OvrBox3f		aabb3;
		MeshBufferList	mesh_buffer_list;
		PoseList		pose_list;
		NameBufferMap	name_buffer_map;
		OvrString       pose_name;
	};
}
