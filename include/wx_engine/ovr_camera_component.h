#pragma once
#include "wx_engine/ovr_component.h"
#include "wx_engine/ovr_engine_define.h"
namespace ovr_engine
{
	enum OvrRenderPath
	{
		kForward,
		kDeferred
	}; 
	class OvrEngine;
	class OVR_ENGINE_API OvrCameraComponent : public OvrComponent
	{
		friend OvrEngine;
	public:
		OvrCameraComponent(OvrActor* a_actor);
		virtual ~OvrCameraComponent();
		virtual OvrComponentType GetType() override { return kComponentCamera; };
		virtual void			Update() override;
		const OvrMatrix4&		GetViewMatrix() const { return view_matrix; }
		const OvrMatrix4&		GetProjectMatrix()const { return project_matrix; }
		OvrMatrix4				GetViewProjectMatrix() { return view_matrix * project_matrix; }
		void					SetFieldView(float a_field_view_rad);
		void					SetAspectRatio(float a_aspect_ratio);
		void					SetNearClip(float a_near);
		void					SetFarClip(float a_far);
		float					GetFieldView() { return field_view; }
		float					GetAspectRatio() { return aspect_ratio; }
		float					GetNearClip() { return near_dist; }
		float					GetFarClip() { return far_dist; }
		void					SetPerspective(float a_field_view_rad, float a_aspect_ratio, float a_near, float a_far);
		virtual OvrRay3f		GeneratePickRay(float a_x, float a_y);
		void					GetPlacePosition(float a_mouse_x, float a_mouse_y, OvrVector3& a_position);
		OvrRenderPath			GetRenderPath() { return render_path; }
		void					SetRenderPath(OvrRenderPath a_render_path) { render_path = render_path; }
		const OvrVector3&		GetCameraPos()const { return camera_position; }

	protected:
		void					BuildCameraLookAtMatrixLH(const OvrVector3& a_position, const OvrVector3& a_target, const OvrVector3& a_up);
		void					BuildProjectionMatrixPerspectiveFovLH(float a_field_view_rad, float a_aspect_ratio, float a_near, float a_far);
		void					Unproject(float a_x, float a_y, float a_depth, OvrVector3& a_dst);

	private:
		OvrRenderPath   render_path;
		OvrMatrix4		view_matrix;
		OvrMatrix4		project_matrix;
		OvrVector3		camera_position;
		float			field_view;
		float			aspect_ratio;
		float			near_dist;
		float			far_dist;
	};
};