﻿#pragma once
#include <vector>
#include "wx_engine/ovr_engine_define.h"
#include "wx_base/wx_ray3f.h"
namespace ovr_engine
{
	class OvrActor;
	class OvrScene;
	class OvrComponent;
	struct OvrRayIntersetResult
	{
		float distance = 0;
		OvrComponent* component = nullptr;
		bool operator < (const OvrRayIntersetResult& rhs) const
		{
			return this->distance < rhs.distance;
		}
	};
	typedef std::vector<OvrRayIntersetResult> RayIntersetResults;

	class OVR_ENGINE_API OvrRaySceneQuery
	{
	public:
		OvrRaySceneQuery(OvrScene* a_scene);
		void SetRay(const OvrRay3f& a_ray) { ray = a_ray; }
		const OvrRay3f& GetRay(void) const { return ray; }
		void SetSortByDistance(bool sort) { sort_by_distance = sort; }
		bool GetSortByDistance(void) const { return sort_by_distance; }
		virtual RayIntersetResults& Execute(bool a_force = false) = 0;

	protected:
		OvrRay3f ray;
		bool sort_by_distance;
		RayIntersetResults mResult;
		OvrScene* scene;
	};

	class OVR_ENGINE_API OvrDefaultRaySceneQuery : public OvrRaySceneQuery
	{
	public:
		OvrDefaultRaySceneQuery(OvrScene* a_scene);
		virtual RayIntersetResults& Execute(bool a_force = false) override;
	};
}