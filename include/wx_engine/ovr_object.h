#ifndef OVR_ENGINE_OVR_OBJECT_H_
#define OVR_ENGINE_OVR_OBJECT_H_

#include "ovr_engine_define.h"
#include "wx_base/wx_matrix4f.h"
namespace ovr_engine
{
	class OVR_ENGINE_API OvrObject
	{
	public:
		OvrObject();
		virtual ~OvrObject();

		virtual void				SetUniqueName(const char* a_name);
		virtual const OvrString&	GetUniqueName() { return unique_name; }

		virtual void				SetDisplayName(const char* a_name) { display_name = a_name; }
		virtual const OvrString&	GetDisplayName()const { return display_name; }
	protected:
		OvrString		unique_name;
		OvrString		display_name;
	};
}


#endif
