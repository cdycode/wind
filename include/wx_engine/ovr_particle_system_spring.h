#ifndef __ovr_particle_system_spring_h__20171228__
#define __ovr_particle_system_spring_h__20171228__

#include "ovr_particle_system_effect.h"
namespace ovr_engine {
	class OvrParticleSystemSpring : public OvrParticleSystemEffect
	{
	public:
		OvrParticleSystemSpring();
		~OvrParticleSystemSpring();
	public:
		void Init();
		void UnInit();
	};
}
#endif
