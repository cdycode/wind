#pragma once
#include "wx_engine/ovr_component.h"
#include "wx_engine/ovr_engine_define.h"

namespace ovr_engine
{
	enum OvrLightType
	{
		ePointLight = 0,
		eDirectionLight = 1,
		eSpotLight = 2,
		eLightNum
	};
	class OVR_ENGINE_API OvrLightComponent : public OvrComponent
	{
	public:
		OvrLightComponent(OvrActor* a_actor);
		virtual ~OvrLightComponent();
		virtual OvrComponentType GetType() override { return kComponentLight; }
		virtual void			Update() override;
		void					SetLightType(OvrLightType a_light_type);
		virtual OvrLightType	GetLightType();
		void					EnableCastShadow(bool a_shadow = true);
		bool					IsCastShadow();
		void					SetDiffuseColor(const OvrVector4& a_diffuse);
		void					SetAmbientColor(const OvrVector4& a_ambient);
		void					SetSpecularColor(const OvrVector4& a_spec);
		void					SetAttenuation(const OvrVector3& a_attenu);
		void					SetRange(const float a_range);
		void					SetFalloff(const float a_falloff);
		void					SetTheta(const float a_theta);
		void					SetPhi(const float a_phi);
		void					SetIntensity(const float a_intensity);
		void					SetExp(float a_exp);
		const OvrVector4&		GetAmbientColor()const;
		const OvrVector4&		GetDiffuseColor()const;
		const OvrVector4&		GetSpecularColor()const;
		const OvrVector3&		GetPosition()const;
		const OvrVector3&		GetDirection()const;
		const OvrVector3&		GetAttenuation()const;
		const OvrMatrix4&		GetProjMat()const;
		const OvrMatrix4&		GetViewMat()const;
		float					GetRange()const;
		float					GetFalloff()const;
		float					GetTheta()const;
		float					GetPhi()const;
		float					GetIntensity()const;
		float					GetExp()const;

	private:
		void					DoLightRec();
	private:
		OvrLightType			light_type;
		OvrVector4				diffuse_color;
		OvrVector4				ambient_color;
		OvrVector4				specular_color;
		OvrVector3				attenuation;
		OvrVector3				light_position;
		OvrVector3				direction;
		float					range;
		float					falloff;
		float					theta;
		float					phi;
		float					intensity;
		float					exp;
		OvrMatrix4				proj_mat;
		OvrMatrix4				view_mat;
		bool					cast_shadow;
	};

};