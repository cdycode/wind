#ifndef _OVR_PARTICLE_ACTOR_H_
#define  _OVR_PARTICLE_ACTOR_H_
#include "wx_engine/ovr_engine_define.h"
#include "wx_engine/ovr_primitive_component.h"
#include "wx_engine/ovr_render_queue.h"
namespace ovr_engine {
	class OvrParticleSystem;
	class OvrParticle;
	class OvrTexture;
	class OvrVertexData;
	class OvrActor;
	class OVR_ENGINE_API OvrParticleComponent :public OvrPrimitiveComponent
	{
	public:
		class ParticleElemnt : public OvrRenderable
		{
			friend OvrParticleComponent;
		public:
			ParticleElemnt(OvrParticleComponent* a_component);
			virtual ~ParticleElemnt();
			virtual OvrMatrix4   GetWorldTransform() const override;
			virtual OvrMatrix4*  GetSkeletonMatrix(ovr::uint32& a_num) const override { return nullptr; }
			virtual float*		 GetPoseWeights(ovr::uint32& a_num) const { return nullptr; }
			virtual OvrMaterial* GetMaterial() override;
			virtual void		 PreRender(OvrScene* scene, OvrRenderSystem* driver) override;
			virtual void		 PostRender() override;
			virtual bool		 GetCastShadowEnable() { return false; }
			virtual IOvrTextureBuffer* GetVertexAnimData() { return nullptr; }
			virtual void	GetRenderOperation(OvrRenderOperation& a_op) override;
			virtual float GetSquaredViewDepth(const OvrCameraComponent* a_cam) override;
		private:
			OvrParticleComponent* particle_render;
			OvrVertexData* vertex_data;
		};
		OvrParticleComponent(OvrActor* a_actor);
		~OvrParticleComponent();
	public:
		OvrComponentType	GetType() override { return kComponentParticle; }
		virtual int			Raycast(const OvrRay3f& a_ray, float& a_distance) override;
		virtual void UpdateRenderQueue(OvrRenderQueue* a_queue) override;
		void update(float a_time);
		virtual void Update() {}
		void SetParticleSystem(OvrParticleSystem* a_system);
		void RemoveParticlesystem();
		void SetMaxParticleNum(uint32_t a_max_particle_num);
		void SetMaterial(OvrMaterial* a_material);
		void SetTransparent(float a_transparent);
		void SetTexture(OvrTexture* a_tex);
		OvrMaterial*  GetMaterial() { return material_use; }
		const std::vector<OvrVector4>& GetParticlePosSizes() { return particle_pos_sizes; }
		const std::vector<OvrMatrix4>& GetParticleRotMats() { return particle_rot_matrices; }
		const std::vector<OvrVector4>&  GetParticleColor() { return particle_color; }
		OvrParticleSystem*   GetParticleSystem();
		void  SetParticlePause(bool a_pause);
		void  SetParticleRestart();
		virtual OvrBox3f    GetBoundingBox() override;
	private:
		void  InsideDepthSortDescendingLess(ovr_engine::OvrCameraComponent* a_cam);
		void GenerateParticleElement();
		ParticleElemnt*                                    particle_element;
		OvrParticleSystem*                                 particle_system;
		OvrMaterial*                                       material_use;
		std::vector<OvrVector4>							   particle_pos_sizes;//for position.
		std::vector<OvrMatrix4>							   particle_rot_matrices;//for normal
		std::vector<OvrVector4>							   particle_color;//for normal
		std::vector<OvrParticle*>						   cull_result;
		uint32_t										   particle_num;
		uint32_t										   max_particle_num;
		bool                                               particle_stop;
		bool                                               particle_restart;
		bool                                               initialized;
		OvrBox3f        bounding_box;
		OvrBoundingBox* wrie_bounding_box;
	};
}
#endif