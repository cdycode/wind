﻿#pragma  once
#include <map>
#include <vector>
#include "wx_base/wx_string.h"
#include "wx_engine/ovr_singleton.h"
#include "wx_engine/ovr_engine_define.h"
namespace ovr_engine
{
	class OvrGpuProgram;
	class OVR_ENGINE_API OvrGpuProgramManager : public OvrSingleton<OvrGpuProgramManager>
	{
	public:
		typedef std::vector<OvrString> GpuProgramNameList;
		virtual void Initialise() = 0;
		virtual void UnInitialise() = 0;
		GpuProgramNameList GetAllProgramNames();
		OvrGpuProgram* GetProgram(OvrString a_shader_name);
	protected:
		std::map<OvrString, OvrGpuProgram*>	program_map;
		std::vector<OvrString> program_list;
	};
}