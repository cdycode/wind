#ifndef __ovr_media_texture_h__20171109__
#define __ovr_media_texture_h__20171109__

#include "wx_engine/ovr_texture.h"
namespace ovr_engine {

	class OVR_ENGINE_API OvrGLMediaTexture : public OvrTexture
	{
	public:
		OvrGLMediaTexture(OvrString a_name);
		virtual ~OvrGLMediaTexture();
		void	UpdateTexture(ovr::int8* a_raw_data);
		virtual void CreateInternalResources();
		virtual void FreeInternalResources();
		virtual unsigned int GetTextureId();
	protected:
		virtual void LoadImpl() override {}
		virtual void UnLoadImpl() override {}
	private:
		unsigned int	texture_id;
		int				width;
		int				height;
	};

}//end namespace

#endif
