#pragma once
#include <vector>
#include <map>
#include "wx_base/wx_type.h"
#include "wx_base/wx_string.h"
#include "wx_base/wx_matrix4f.h"
#include "wx_engine/ovr_engine_define.h"
#include "wx_engine/ovr_gpu_program_constants.h"
#include "wx_base/wx_color_value.h"
#include "wx_engine/ovr_texture_unit.h"
namespace ovr_engine
{
	class OvrTexture;

	class OVR_ENGINE_API OvrGpuProgramParameters
	{
	public:

		typedef std::vector<float> FloatConstantList;
		typedef std::vector<int> IntConstantList;
		typedef std::vector<OvrTextureUnit> TextureLayerList;
	public:
		OvrGpuProgramParameters() {};
		virtual ~OvrGpuProgramParameters();
		const OvrConstant*	FindNameConstant(const OvrString& a_name);
		const OvrAutoConstantDefinition*	FindAutoConstantDefinition(const OvrString& name);
		ovr::uint32				GetAutoConstantCount();
		ovr::uint32				GetConstantCount();
		const OvrAutoConstant*		GetAutoConstant(ovr::uint32 a_index);
		const OvrConstant*	GetNameConstant(const OvrString& a_name);
		const ConstantMap*		GetConstantMap();
		const ConstantList*     GetCustomConstantList();
		const AutoConstantList* GetAutoConstantList();

		const OvrGpuProgramConstants* GetProgramConstants() { return shader_constants; }
		float*			GetFloatPointer(ovr::uint32 a_index);
		int*			GetIntPointer(ovr::uint32 a_index);
		OvrTextureUnit*	GetTextureUnit(ovr::uint32 a_index);
		OvrTextureUnit*	GetTextureUnit(const OvrString a_name);
		void UpdateAutoParameters(const OvrAutoParamSource& src, ovr::uint16 mask);
		void SetNameConstant(const OvrString& a_name, OvrTextureUnit& layer);
		void SetNameConstant(const OvrString& a_name, OvrTexture* a_tex);
		void SetNameConstant(const OvrString& a_name, const OvrVector4& a_vec4, ovr::uint32 a_size = 1);
		void SetNameConstant(const OvrString& a_name, const OvrVector3& a_vec3, ovr::uint32 a_size = 1);
		void SetNameConstant(const OvrString& a_name, const OvrMatrix4& a_mat4, ovr::uint32 a_size = 1);
		void SetNameConstant(const OvrString& a_name, const float& a_val);
		void SetProgramConstants(OvrGpuProgramConstants* a_constants);
		void Clear();
	private:
		void WriteRawConstant(ovr::uint32 a_physicalIndex, OvrTextureUnit* a_tex, ovr::uint32 a_size = 1);
		void WriteRawConstant(ovr::uint32 a_physicalIndex, const int* a_int, ovr::uint32 a_size = 1);
		void WriteRawConstant(ovr::uint32 a_physicalIndex, const float* a_float, ovr::uint32 a_size = 1);
	private:
		OvrGpuProgramConstants* shader_constants;
		FloatConstantList float_constants;
		IntConstantList int_constants;
		TextureLayerList texture_constants;
	};
}