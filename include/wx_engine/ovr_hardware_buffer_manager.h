﻿#pragma once
#include "wx_engine/ovr_singleton.h"
#include "wx_engine/ovr_engine_define.h"
namespace ovr_engine
{
	class OvrHardwareVertexBuffer;
	class OvrHardwareIndexBuffer;
	class OvrHardwareBuffer;
	class OvrVertexDeclaration;

	class OVR_ENGINE_API OvrHardwareBufferManager : public OvrSingleton<OvrHardwareBufferManager>
	{
	public:
		OvrHardwareBufferManager();
		virtual OvrVertexDeclaration* CreateVertexDeclaration() = 0;
		void DestroyVertexDeclaration(OvrVertexDeclaration* a_ro);
		virtual OvrHardwareVertexBuffer* CreateVertexBuffer(ovr::uint32 a_vertex_size, ovr::uint32 a_vcount, void* a_vdata,  bool a_static_draw = true) = 0;
		virtual OvrHardwareIndexBuffer* CreateIndexBuffer(ovr::uint32 a_icount, void* a_idata) = 0;
	};
}