﻿namespace ovr_engine
{
	class OvrRenderTexture
	{
	public:
		OvrRenderTexture() {}
		virtual ~OvrRenderTexture() {}
		virtual void BindRTT() = 0;
		virtual void Bind(int a_depth, int a_face) = 0;
		virtual void UnBind() = 0;
		virtual void UnBindRTT() = 0;
	};
}
