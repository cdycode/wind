#ifndef __ovr_particle_emitter_sphere_h__20171228__
#define __ovr_particle_emitter_sphere_h__20171228__

#include "OvrParticleEmitter.h"
namespace ovr_engine {
	class OVR_ENGINE_API OvrParticleEmitterSphere : public OvrParticleEmitter
	{
	public:
		OvrParticleEmitterSphere();
		OvrParticleEmitterSphere(float a_radius);
		~OvrParticleEmitterSphere();
	public:
		DECLARE_CLASS(OvrParticleEmitterSphere)
	public:
		virtual OvrParticleEmitterType GetParticleEmitterTyep()const override { return eParticleSphere; }
		virtual void EmitterParticle(OvrParticle* ao_particle);
		void SetRadius(const float a_radius) { radius = a_radius; }
		const float GetRadius() { return radius; }
	private:
		float radius;
	};
}
#endif
