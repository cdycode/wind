#pragma once
#include "wx_engine/ovr_shape_component.h"
#include "wx_engine/ovr_bounding_box.h"
#include "wx_engine/ovr_engine_define.h"
namespace ovr_engine
{
	class OVR_ENGINE_API OvrBoxColliderComponent : public OvrShapeComponent
	{
	public:
		OvrBoxColliderComponent(OvrActor* a_actor);
		virtual ~OvrBoxColliderComponent();
		OvrComponentType	GetType() override { return kComponentBoxCollider; }
		const OvrVector3&	GetCenter();
		const OvrVector3&	GetSize();
		void				SetCenter(const OvrVector3& a_center);
		void				SetSize(const OvrVector3& a_size);
		virtual int			Raycast(const OvrRay3f& a_ray, float& a_distance) override;
		virtual void		Update() override {}
		virtual void		UpdateRenderQueue(OvrRenderQueue* a_queue) override;
		virtual OvrBox3f    GetBoundingBox() override;
	protected:
		OvrVector3 center;
		OvrVector3 size;
		OvrBoundingBox* wrie_bouding_box;
	};
}