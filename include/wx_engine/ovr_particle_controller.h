#ifndef __ovr_particle_controller_h__20171130__
#define __ovr_particle_controller_h__20171130__
#include "ovr_particle.h"
namespace ovr_engine {
	class OvrParticleController
	{
	public:
		enum control_type
		{
			e_control_pos,
			e_control_rotation,
			e_control_scale,
			e_control_color,
			e_control_any
		};
	public:
		OvrParticleController();
		~OvrParticleController();
	public:
		virtual bool update(OvrParticle* ao_particle, float a_delta_time) = 0;
		void set_controller_type(control_type a_type);
	public:
		control_type type;
	};

}
#endif
