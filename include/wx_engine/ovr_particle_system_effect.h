#ifndef __ovr_particle_system_effect_h__20171231__
#define __ovr_particle_system_effect_h__20171231__

#include "ovr_particle_system.h"
#include "wx_base/wx_matrix4f.h"
#include "lvr_matrix3.h"
#include <map>
class lvr_render_object;
namespace ovr_engine {
	class OvrMaterial;
	typedef struct
	{
		OvrCamera*		camera;
		OvrMatrix4	view_matrix;
		OvrMatrix4	proj_matrix;
		OvrMatrix4	view_proj_matrix;
		float			time;
	}OvrNecessaryRenderData;

	class OvrParticleSystemEffect
	{
	public:
		OvrParticleSystemEffect();
		~OvrParticleSystemEffect();
	public:
		virtual void  cull(OvrCamera* a_camera);
		virtual void  update(OvrCamera* a_camera, float a_time);//camera will not use in most case.only in face camera work.
		virtual void  draw(OvrNecessaryRenderData* a_data);
		void  SetMaxParticleNum(uint32_t a_max_particle_num);
		void  SetParticleRenderObject(lvr_render_object* a_ro);
		OvrMaterial*	GetMaterial();
		void  SetMaterial(OvrMaterial* a_material);
		void  AddParticleSystem(OvrParticleSystem* a_particle_sys_part);
		void  RemoveParticleSystem(OvrParticleSystem* a_particle_sys_part);
		void  RenderShadow(OvrNecessaryRenderData* a_data, bool a_cube_case);
	public:
		std::map<OvrParticleSystem*, OvrParticleSystem*>	_particle_system_group;
		OvrMaterial*											_material;
		lvr_render_object*										_ro;
		std::vector<OvrVector4>								_particle_pos_sizes;//for position.
		std::vector<lvr_matrix3f>								_particle_rot_matrices;//for normal
		std::vector<OvrVector4>								_particle_color;//for normal
		std::vector<OvrParticle*>								_cull_result;
		uint32_t												_particle_num;
		uint32_t												_max_particle_num;
	};
}

#endif
