#pragma once
#include "wx_engine/ovr_singleton.h"
#include "wx_engine/ovr_dynamic_lib.h"
#include "wx_engine/ovr_engine_define.h"
#include <map>
namespace ovr_engine
{
	class OVR_ENGINE_API OvrDynLibManager : public OvrSingleton<OvrDynLibManager>
	{
	public:
		OvrDynLib* Load(const OvrString& a_name);
		void Unload(OvrDynLib* a_lib);
		OvrDynLib* GetByName(const OvrString& a_name);
	private:
		std::map<OvrString, OvrDynLib*> dynlib_map;
	};

}