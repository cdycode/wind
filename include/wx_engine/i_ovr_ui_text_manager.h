﻿#include <vector>
#include "wx_engine/ovr_engine_define.h"
#include "wx_base/wx_string.h"
#include "wx_base/wx_vector3.h"
#include "wx_base/wx_matrix4f.h"
namespace ovr_engine
{
	class OvrTextInfo
	{
	public:
		OvrTextInfo() :bitmap_font_id(-1)
			,x_margin(0.0f)
			,y_margin(0.0f)
			,scale(0.0f)
		,font_spacing(0.0f)
		,line_spacing(0.0f)
		,background_x_scale(0.0f)
		,background_y_scale(0.0f){}
		~OvrTextInfo() {}
		ovr::uint32 align_method;
		float   x_margin;
		float   y_margin;
		float    scale;
		float   font_spacing;
		float   line_spacing;
		float   background_x_scale;
		float   background_y_scale;
		OvrString text;
		OvrVector4 color;
		OvrVector3 pos;
		OvrVector3 up;
		OvrVector3 right;
		int bitmap_font_id;
	};
	class OVR_ENGINE_API IOvrUiTextManager
	{
	public:
		IOvrUiTextManager() {}
		~IOvrUiTextManager() {}
		virtual void  Initialise(const OvrString& font_path, bool a_inside) = 0;
		virtual void  UnInitialise() = 0;
		virtual void  AddText(const OvrTextInfo& a_text) = 0;
		virtual void  RemoveAllText() = 0;
		virtual void  Update(const OvrVector3& a_pos, const OvrVector3& a_dir, double a_time) = 0;
		virtual void  Draw(const OvrMatrix4& a_vp) = 0;
		virtual void  QueryTextId(OvrTextInfo* a_info) = 0;
	};
}