#ifndef __ovr_particle_system_snow_h__20171228__
#define __ovr_particle_system_snow_h__20171228__

#include "ovr_particle_system_effect.h"
namespace ovr_engine {
	class OvrParticleSystemSnow : public OvrParticleSystemEffect
	{
	public:
		OvrParticleSystemSnow();
		~OvrParticleSystemSnow();
	public:
		void Init();
		void UnInit();
	};
}
#endif
