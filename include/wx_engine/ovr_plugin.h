#pragma once
#include "wx_base/wx_string.h"
#include "wx_engine/ovr_engine_define.h"

namespace ovr_engine
{
	class OVR_ENGINE_API OvrPlugin
	{
	public:
		virtual void Install() = 0;
		virtual void UnInstall() = 0;
		virtual void Init() = 0;
		virtual void UnInit() = 0;
		virtual OvrString GetName() = 0;

	};
}