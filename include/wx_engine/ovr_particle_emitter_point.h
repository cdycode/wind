#ifndef __ovr_particle_emitter_point_h__20171228__
#define __ovr_particle_emitter_point_h__20171228__

#include "OvrParticleEmitter.h"
namespace ovr_engine {
	class OVR_ENGINE_API OvrParticleEmitterPoint : public OvrParticleEmitter
	{
	public:
		OvrParticleEmitterPoint();
		~OvrParticleEmitterPoint();
	public:
		DECLARE_CLASS(OvrParticleEmitterPoint)
	public:
		virtual OvrParticleEmitterType GetParticleEmitterTyep()const override { return eParticlePoint; }
		virtual void EmitterParticle(OvrParticle* ao_particle);
		void SetScatterAngle(float a_angle);
		const float GetScatterAngle()const { return rad; }
		void SetPointPos(const OvrVector3& a_pos);
		const OvrVector3& GetPointPos()const;
	};
}
#endif
