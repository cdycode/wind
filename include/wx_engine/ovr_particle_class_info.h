#ifndef OVR_PARTICLE_CLASS_INFO
#define OVR_PARTICLE_CLASS_INFO
#include "wx_engine/OvrParticleEmitter.h"
typedef ovr_engine::OvrParticleEmitter* (*ObjConstructorFun)();
	class ClassInfo
	{
	public:
		ClassInfo(const OvrString &className, ObjConstructorFun classConstructor)
			:m_className(className), m_objectConstructor(classConstructor)
		{
			ovr_engine::OvrParticleEmitter::Register(this);
		}
		virtual ~ClassInfo() {}
		ovr_engine::OvrParticleEmitter* CreateObject()const { return m_objectConstructor ? (*m_objectConstructor)() : NULL; }
		bool IsDynamic()const { return NULL != m_objectConstructor; }
		const OvrString GetClassName()const { return m_className; }
		ObjConstructorFun GetConstructor()const { return m_objectConstructor; }
	public:
		OvrString m_className;
		ObjConstructorFun m_objectConstructor;
	};
#endif