#pragma once
#include <map>
#include "wx_engine/ovr_singleton.h"
#include "wx_base/wx_string.h"
#include "wx_engine/ovr_engine_define.h"
#include "wx_engine/ovr_resource_manager.h"
namespace ovr_engine
{
	class OvrSkeleton;
	class OVR_ENGINE_API OvrSkeletonManager : public OvrResourceManager, public OvrSingleton<OvrSkeletonManager>
	{
	public:
		OvrSkeletonManager();
		~OvrSkeletonManager();
		bool Initialise();
		void UnInitialise();
		OvrSkeleton* Load(OvrString a_file_name);
		virtual OvrResource* CreateResource(OvrString a_name, bool is_manual) override;
	};

}