#pragma once
#include "wx_base/wx_box3f.h"
#include "wx_asset_manager/ovr_motion_frame.h"
#include "wx_engine/ovr_engine_define.h"
#include "wx_engine/ovr_primitive_component.h"
#include "wx_engine/ovr_render_queue.h"
#include "wx_engine/ovr_mesh_renderable.h"
namespace ovr_engine
{	
	class OvrMesh;
	class OvrMaterial;
	class OvrBoundingBox;

	class OVR_ENGINE_API OvrMeshComponent : public OvrPrimitiveComponent
	{
	public:
		typedef std::vector<OvrMaterial*> MaterialList;
		typedef std::map<ovr::uint32, ovr::uint32> MaterialIndexMap;
	public:
		OvrMeshComponent(OvrActor* a_actor);
		virtual ~OvrMeshComponent();
		virtual void             UpdateRenderQueue(OvrRenderQueue* a_queue);
		virtual int				 Raycast(const OvrRay3f& a_ray, float& a_distance) override;
		virtual OvrBox3f		 GetBoundingBox() override;
		virtual void			 SetMesh(ovr_engine::OvrMesh* a_mesh) = 0;
		OvrMesh*				 GetMesh()const;
		bool                     HasPose();
		virtual void			 SetCastShadow(bool a_shadow_cast) { is_shadow_cast = a_shadow_cast; }
		virtual bool			 GetCastShadow() const { return is_shadow_cast; }
		virtual void			 SetMaterialCount(ovr::uint32 a_num);
		ovr::uint32				 AddMaterial(OvrMaterial* a_material);
		virtual void			 SetMaterial(ovr::uint32 a_index, OvrMaterial* a_material);
		virtual OvrMaterial*	 GetMaterial(ovr::uint32 a_index);
		virtual ovr::uint32		 GetMaterialCount();
		void                     SetRenderQueueGroupPriority(ovr::uint16 a_priority);
		virtual bool			 UpdateAnimation(ovr_asset::OvrMorphFrame* a_frame);
		virtual void			 BuildRenderableList();
		virtual void			 ResetRenderableList();
	protected:
		OvrMesh*				 mesh;
		bool					 is_shadow_cast;
		MaterialList			 material_list;
		OvrBoundingBox*          wire_bounding_box;
		bool                     is_intersection_test;
		ovr::uint16              priority_id;
		ovr::uint32				 mesh_state_count;
		std::vector<OvrMeshRenderable*> renderable_list;
	};
}