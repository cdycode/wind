#ifndef __ovr_particle_emitter_h__20171228__
#define __ovr_particle_emitter_h__20171228__

#include "ovr_particle.h"
#define  DECLARE_CLASS(class_name)\
public:\
	virtual ClassInfo* GetClassInfo()const { return &m_classInfo; }\
	static ovr_engine::OvrParticleEmitter* CreateObject() { return new class_name; }\
protected:\
	static ClassInfo m_classInfo;\

#define REGISTER_CLASS(class_name)\
    ClassInfo class_name::m_classInfo(#class_name,class_name::CreateObject);

#define REFLEX_CLASS(class_name)\
(class_name*)(ovr_engine::OvrParticleEmitter::CreateObject(#class_name))

class ClassInfo;
namespace ovr_engine {
	enum OvrParticleEmitterType
	{
		eParticleBox,
		eParticleCircle,
		eParticleLine,
		eParticleMesh,
		eParticlePoint,
		eParticleSphere,
		ePartcileAll,
	};
	class OVR_ENGINE_API OvrParticleEmitter
	{
	public:
		OvrParticleEmitter();
		OvrParticleEmitter(float a_max_size, float a_min_size, float a_max_speed, float a_min_speed, float a_max_life, float a_min_life);
		virtual~OvrParticleEmitter();
	public:
		virtual OvrParticleEmitterType GetParticleEmitterTyep() const = 0;
		virtual void EmitterParticle(OvrParticle* ao_particle) = 0;
		virtual void SetInitRotateQ(const OvrQuaternion& a_q);
		virtual void SetInitAxisRad(const OvrVector3& a_axis, float a_rad);
		virtual void SetMainDir(const OvrVector3& a_dir);
		virtual void SetColor(OvrVector4 a_color);
		virtual void SetSize(float a_max, float a_min);
		virtual void SetSpeed(float a_max, float a_min);
		virtual void SetLife(float a_max, float a_min);
		virtual void SetPos(const OvrVector3& a_pos);
		virtual void SetFaceCamera(bool if_face_camera);
	public:
		virtual const OvrVector3 GetPos();
		virtual const OvrVector4 GetColor() { return color; }
		virtual const float GetMaxSize() { return max_size; }
		virtual const float GetMinSize() { return min_size; }
		virtual const float GetMaxLife() { return max_life; }
		virtual const float GetMinLife() { return min_life; }
		virtual const float GetMaxSpeed() { return max_speed; }
		virtual const float GetMinSpeed() { return min_speed; }
		virtual const float GetRad() { return rad; }
		virtual const bool GetFaceCamera() { return face_camera; }
	public:
		static bool Register(ClassInfo* pCInfo);
		static OvrParticleEmitter* CreateObject(const OvrString& className);
	protected:
		bool      face_camera;
		OvrVector4	color;
		float			max_size, min_size;
		float			max_speed, min_speed;
		float			max_life, min_life;
		OvrVector3	pos;
		OvrQuaternion	init_rot;
		OvrVector3	main_dir;
		OvrVector3  axis;
		float       rad;

	};
}

#endif
