﻿#pragma once
#include <map>
#include <vector>
#include "wx_base/wx_type.h"
#include "wx_base/wx_matrix4f.h"
#define OVR_RENDERABLE_DEFAULT_PRIORITY  100

namespace ovr_engine
{
	class OvrMaterial;
	class OvrScene;
	class OvrRenderSystem;
	class OvrCameraComponent;
	class OvrVertexData;
	class OvrIndexData;

	enum OvrPrimitiveType
	{
		EPT_TRIANGLES = 0,
		EPT_TRIANGLE_FAN,
		EPT_TRIANGLE_STRIP,
		EPT_LINES,
		EPT_LINE_STRIP,
		EPT_POINTS
	};
	class OvrRenderable;
	struct OvrRenderOperation
	{
		OvrVertexData*		vertex_data;
		OvrIndexData*		index_data;
		bool				use_indices;
		OvrPrimitiveType	primitive_type;
		const OvrRenderable* src_renderable;
		ovr::uint32			number_instances;
		bool				has_instances_data;
	};
	class OvrRenderable
	{
	public:
		OvrRenderable() {}
		virtual ~OvrRenderable() {}
		virtual OvrMatrix4			GetWorldTransform() const = 0;
		virtual OvrMatrix4*			GetSkeletonMatrix(ovr::uint32& a_num) const;
		virtual float*				GetPoseWeights(ovr::uint32& a_num) const;
		virtual OvrMaterial*		GetMaterial() = 0;
		virtual void				PreRender(OvrScene* scene, OvrRenderSystem* driver) {}
		virtual void				PostRender() {}
		virtual bool				GetCastShadowEnable();
		virtual void				GetRenderOperation(OvrRenderOperation& a_op) = 0;
		virtual float				GetSquaredViewDepth(const OvrCameraComponent* a_cam);
	}; 

	

	struct RenderablePass
	{
		RenderablePass(OvrRenderable* a_rnd, OvrMaterial* a_material);
		virtual ~RenderablePass();
		OvrRenderable* renderable;
		OvrMaterial* material;
	};
}