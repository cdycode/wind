#pragma once
#include "wx_engine/ovr_component.h"
#include "wx_base/wx_box3f.h"
namespace ovr_engine
{
	class OvrRenderQueue;
	class OvrBoundingBox;
	class OVR_ENGINE_API OvrPrimitiveComponent : public OvrComponent
	{
	public:
		OvrPrimitiveComponent(OvrActor* a_actor);
		virtual ~OvrPrimitiveComponent();
		virtual void		UpdateRenderQueue(OvrRenderQueue* a_queue) = 0;
		virtual int			Raycast(const OvrRay3f& a_ray, float& a_distance) = 0;
		virtual OvrBox3f    GetWorldBoundingBox();
		virtual OvrBox3f    GetBoundingBox() = 0;
		void				SetCollisionEnabled(bool a_enable);
		bool				GetCollisionEnabled() const;
		void				SetRenderingEnabled(bool a_enable);
		bool				GetRenderingEnabled() const;
	protected:
		bool collision_enabled;
		bool rendering_enabled;
		OvrBox3f world_aabb3;
	};
}