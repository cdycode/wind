﻿#pragma once
#include <assert.h>
#   pragma warning (push)
#   pragma warning ( disable: 4661)
namespace ovr_engine
{
	template <typename T> class OvrSingleton
	{
	protected:
		static T* s_instance;

	public:
		OvrSingleton()
		{
			assert(!s_instance);
			s_instance = static_cast<T*>(this);
		}

		~OvrSingleton()
		{
			assert(s_instance);  s_instance = nullptr;
		}

		static T* GetIns()
		{
			return s_instance;
		}
	};
}