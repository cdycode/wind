﻿#ifndef OVR_ENGINE_OVR_AXIS_SYSTEM_H_
#define OVR_ENGINE_OVR_AXIS_SYSTEM_H_

#include "ovr_engine_define.h"
#include "wx_base/wx_vector3.h"

class lvr_render_object;
namespace ovr_engine 
{
	class OvrCameraComponent;
	class OVR_ENGINE_API OvrAxisSystem
	{
	public:
		OvrAxisSystem();
		~OvrAxisSystem();

		void	Init();
		void	Uninit();

		void	SetPosition(const OvrVector3& a_position) { axis_pos = a_position; }

		void	Draw(OvrCameraComponent* a_camera);

	protected:
		lvr_render_object*	axis_system;
		OvrVector3			axis_pos;
		float				size = 0.06125f;

	};

	class OVR_ENGINE_API OvrAxisSystemWorld : public OvrAxisSystem
	{
	public:
		OvrAxisSystemWorld();
		virtual ~OvrAxisSystemWorld();

		void Update(OvrCameraComponent* a_camera);

	};
}

#endif
