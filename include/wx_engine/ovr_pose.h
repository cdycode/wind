#pragma once
#include "wx_engine/ovr_engine_define.h"
#include "wx_base/wx_vector3.h"
#include "wx_base/wx_type.h"
#include <map>
namespace ovr_engine
{
	class OVR_ENGINE_API OvrPose
	{
	public:
		OvrPose(ovr::uint32 a_handle, OvrString a_name);
		OvrPose(OvrString a_target_name, OvrString a_name);
		~OvrPose();
		const OvrString& GetName() { return name; }
		const ovr::uint32& GetTarget() { return handle; }
		const OvrString& GetTargetName() { return target_name; }
		const std::map<ovr::uint32, OvrVector3>& GetVertexOffset() { return vertex_offset_map; }
		void AddVertex(ovr::uint32 a_index, OvrVector3 a_offset);
	private:
		OvrString							name;
		ovr::uint32							handle;
		OvrString                           target_name;
		std::map<ovr::uint32, OvrVector3>	vertex_offset_map;
	};
}
