#pragma once
#include "wx_engine/ovr_engine_define.h"
#include "wx_base/wx_transform.h"
#include "wx_base/wx_ray3f.h"
#include "wx_base/wx_box3f.h"
#include <vector>
namespace ovr_engine
{	
	enum OvrComponentType
	{
		kComponentInvalid = 0,
		//����Ⱦ��������ײ
		kComponentLight,
		kComponentCamera,
		//������ײ����Ⱦ
		kComponentBoxCollider = 100,
		//��Ⱦ�ҷ�����ײ
		kComponentStaticMesh = 200,
		kComponentSkinnedMesh,
		kComponentParticleSystem,
		kComponentText,	
		KComponentImage,
		kComponentVideo,
		kComponentParticle,
		kComponentUserDefined = 1000,

	};

	class OvrActor;
	class OVR_ENGINE_API OvrComponent
	{
	public:
		OvrComponent(OvrActor* a_actor);
		virtual ~OvrComponent();
		OvrActor*					GetParentActor();
		void						SetActive(bool a_active);
		bool						IsActive() { return is_active; }
		bool						IsRealActive();
		virtual OvrComponentType	GetType() { return kComponentInvalid; }
		virtual void				Update() = 0;
		virtual OvrMatrix4			GetWorldTransform();

	protected:
		OvrActor* parent_actor;
		bool is_active;
	};
}