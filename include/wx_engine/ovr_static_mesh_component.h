#pragma once
#include "wx_engine/ovr_mesh_component.h"
namespace ovr_engine
{	
	class OVR_ENGINE_API OvrStaticMeshComponent : public OvrMeshComponent
	{
	public:
		OvrStaticMeshComponent(OvrActor* a_actor);
		virtual ~OvrStaticMeshComponent();
		virtual OvrComponentType GetType() { return kComponentStaticMesh; }
		virtual void			 Update() override;
		virtual void			 SetMesh(ovr_engine::OvrMesh* a_mesh) override;
	};
}