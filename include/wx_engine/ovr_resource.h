#pragma once
#include "wx_engine/ovr_reference_counted.h"
#include "wx_engine/ovr_engine_define.h"
namespace ovr_engine
{

	enum LoadingState
	{
		/// Not loaded
		LOADSTATE_UNLOADED,
		/// Fully loaded
		LOADSTATE_LOADED,
	};

	class OVR_ENGINE_API OvrResource : public OvrReferenceCounted
	{
	public:
		OvrResource(OvrString a_name, bool a_is_manual);
		virtual ~OvrResource();
		void UnLoad();
		void Load();
		bool IsLoaded();
		bool isManuallyLoaded() const;
		void Reload();
		OvrString GetName() { return name; }
		LoadingState GetLoadingState() { return load_state; }
		void DirtyState();
		ovr::uint32 GetStateCount() { return state_count; }

	protected:
		virtual void LoadImpl() = 0;
		virtual void UnLoadImpl() = 0;
	protected:
		bool is_manual;
		LoadingState load_state;
		OvrString name;
		ovr::uint32 state_count;
	};
}