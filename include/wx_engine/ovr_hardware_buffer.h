﻿#pragma once
#include "wx_base/wx_type.h"
namespace ovr_engine
{
	enum OvrVertexType
	{
		kStaticVertex,
		kSkinnedVertex,
		kTempVertex,
	};

	class OvrHardwareBuffer
	{
	public:
		OvrHardwareBuffer();
		virtual ~OvrHardwareBuffer() {}
		virtual void* Lock(unsigned int a_mode) = 0;
		virtual void  Unlock() = 0;
		virtual void  WriteData(int a_offset, int a_update_data_len, const void* a_datas) = 0;
		virtual ovr::uint32  GetSize();
	protected:
		ovr::uint32 size_in_byte;
	};

	class OvrHardwareVertexBuffer : public OvrHardwareBuffer
	{
	public:
		OvrHardwareVertexBuffer(ovr::uint32 a_vertex_size, ovr::uint32 a_vcount);
		virtual ~OvrHardwareVertexBuffer() {}
		ovr::uint32   GetVertexCount() { return vertex_num; }
		ovr::uint32   GetVertexSize() { return vertex_size; }
	protected:
		unsigned int	vertex_num;
		unsigned int	vertex_size;
	};

	class OvrHardwareIndexBuffer : public OvrHardwareBuffer
	{
	public:
		OvrHardwareIndexBuffer(ovr::uint32 a_icount);
		virtual ~OvrHardwareIndexBuffer() {}
		unsigned int GetIndexCount() { return index_count; }
	private:
		unsigned int index_count;
	};


}