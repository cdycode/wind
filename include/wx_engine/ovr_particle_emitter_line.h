#ifndef __ovr_particle_emitter_line_h__20171228__
#define __ovr_particle_emitter_line_h__20171228__

#include "OvrParticleEmitter.h"
namespace ovr_engine {
	class OVR_ENGINE_API OvrParticleEmitterLine : public OvrParticleEmitter
	{
	public:
		OvrParticleEmitterLine();
		OvrParticleEmitterLine(const OvrVector3& a_begin_pt,const OvrVector3& a_end_pt);
		virtual ~OvrParticleEmitterLine();
	public:
		DECLARE_CLASS(OvrParticleEmitterLine)
	public:
		virtual OvrParticleEmitterType GetParticleEmitterTyep()const override { return eParticleLine; }
		virtual void EmitterParticle(OvrParticle* ao_particle);
	public:
		void SetBeginPt(const OvrVector3& a_begin_pt);
		void SetEndPt(const OvrVector3& a_end_pt);
		const OvrVector3& GetBeginPt()const;
		const OvrVector3& GetEndPt()const;
	private:
		OvrVector3        begin_pt;
		OvrVector3        end_pt;

	};
}
#endif
