#ifndef __ovr_particle_controller_bezier_h__20171231__
#define __ovr_particle_controller_bezier_h__20171231__
#include <vector>
#include "ovr_particle_controller.h"
#include "ovr_bezier_curve.h"
namespace ovr_engine{

class OvrTimeLine
{
public:
	OvrTimeLine();
	~OvrTimeLine();
public:
	bool GetTimePos(float time_now, int& ao_segment, float& ao_pos);
	//ao_segment input last segment value;
	bool GetTimePosWithLastSegment(float time_now, int& ao_segment, float& ao_pos);
	void AddTimeMark(float time);
	void ModifyTimeMark(int id, float time);
public:
	std::vector<float>	time_marks;
	int					segment_mark_num;
};

class OvrParticleControllerBezier : public OvrParticleController
{
public:
	OvrParticleControllerBezier();
	~OvrParticleControllerBezier();
public:
	bool  update(OvrParticle* a_particle, float a_delta_time);
public:
	OvrTimeLine		_time_line;
	OvrBezierCurve	_bezier_curve;

};
}

#endif
