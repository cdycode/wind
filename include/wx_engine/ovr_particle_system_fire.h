#ifndef __ovr_particle_system_fire_h__20171228__
#define __ovr_particle_system_fire_h__20171228__

#include "ovr_particle_system_effect.h"
namespace ovr_engine {
	class OvrParticleSystemFire : public OvrParticleSystemEffect
	{
	public:
		OvrParticleSystemFire();
		~OvrParticleSystemFire();
	public:
		virtual void  update(OvrCamera* a_camera, float a_time);//camera will not use in most case.only in face camera work.
		void Init();
		void UnInit();
	public:
		OvrParticleEmitter*	_test_emitter;
	};
}
#endif
