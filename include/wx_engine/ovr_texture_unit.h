#pragma once
#include "wx_base/wx_color_value.h"
#include "wx_base/wx_matrix4f.h"
#include "wx_engine/ovr_engine_define.h"
namespace ovr_engine
{
	class OvrTexture;
	enum OvrTextureFilter
	{
		TEXTURE_FILTER_NEAREST,		// Causes significant aliasing, only for performance testing.
		TEXTURE_FILTER_BILINEAR,	// This should be used under almost all circumstances.
	};

	enum OvrTextureAddressMode
	{
		TEXTURE_ADDRESSMODE_WARP,
		TEXTURE_ADDRESSMODE_CLAMP,
		TEXTURE_ADDRESSMODE_MIRROR,
		TEXTURE_ADDRESSMODE_BORDER
	};

	struct OvrUVWAddressingMode
	{
		OvrUVWAddressingMode();
		OvrUVWAddressingMode(OvrTextureAddressMode a_mode);
		OvrUVWAddressingMode(OvrTextureAddressMode a_u, OvrTextureAddressMode a_v, OvrTextureAddressMode a_w);
		OvrTextureAddressMode u, v, w;
	};

	class OVR_ENGINE_API OvrTextureUnit
	{
	public:
		OvrTextureUnit();
		virtual ~OvrTextureUnit();
		OvrTextureUnit(OvrTexture* a_texture_ptr);
		OvrTextureUnit& operator= (const OvrTextureUnit& other);
		void SetTextureTilingAndOffset(float a_tx, float a_ty, float a_ox, float a_oy);
		void SetTextureMatrix(const OvrMatrix4& a_mat);
		void SetTexture(OvrTexture* texture_ptr);
		void SetAddressingMode(OvrTextureAddressMode a_u, OvrTextureAddressMode a_v, OvrTextureAddressMode a_w);
		void SetAddressingMode(const OvrUVWAddressingMode& a_mode);
		void SetBorderingColor(const OvrColorValue& a_color);
		void SetFilter(const OvrTextureFilter& a_filter);
		const OvrMatrix4& GetTextureMatrix() { return texture_matrix; }
		OvrTexture* GetTexture() { return texture; }
		const OvrUVWAddressingMode& GetAddressingMode() { return address_mode; }
		const OvrColorValue& GetBorderingColor() { return border_color; }
		const OvrTextureFilter& GetFilter() { return filter; }
		void SetDirty(bool a_dirty = true);
		bool IsDirty() { return param_changed; }
	private:
		OvrMatrix4 texture_matrix;
		OvrTexture* texture;
		OvrUVWAddressingMode address_mode;
		OvrTextureFilter filter;
		OvrColorValue border_color;
		bool param_changed;
	};
}