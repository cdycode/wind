#pragma once

#include <map>
#include <vector>
#include "wx_base/wx_matrix4f.h"
#include "wx_base/wx_vector3.h"
#include "wx_base/wx_string.h"
#include "wx_engine/ovr_engine_define.h"
#ifndef OVR_MAX_POINT_LIGHT_NUM
#define OVR_MAX_POINT_LIGHT_NUM 32
#endif

#ifndef OVR_MAX_SPOT_LIGHT_NUM
#define OVR_MAX_SPOT_LIGHT_NUM 32
#endif

namespace ovr_engine {
	class OvrTexture;
	class OvrCameraComponent;
	class OvrLightComponent;
	enum OvrGpuProgramConstantType
	{
		GCT_FLOAT1 = 0,
		GCT_INT1,
		GCT_VEC3,
		GCT_VEC4,
		GCT_INT4,
		GCT_MAT3,
		GCT_MAT4,
		GCT_SAMPLER2D,
		GCT_SAMPLER2D_ARRAY,
		GCT_SAMPLER_BUFFER,
		GCT_SAMPLER_CUBE,
		GCT_SAMPLER_CUBE_ARRAY,
		GCT_UNKNOWN
	};

	enum OvrGpuProgramParamVariability
	{
		GPV_GLOBAL = 1,
		GPV_PER_OBJECT = 2,
		GPV_LIGHTS = 4,
		GPV_PASS_ITERATION_NUMBER = 8,
		GPV_ALL = 0xFFFF
	};

	struct OVR_ENGINE_API OvrConstant
	{
		OvrString gpu_name;
		OvrGpuProgramConstantType const_type;
		ovr::uint32 array_size;
		ovr::uint32 element_size;
		ovr::uint32 physicalIndex;
		ovr::uint16 variability;

		static bool IsFloat(OvrGpuProgramConstantType c);
		static bool IsInt(OvrGpuProgramConstantType c);
		static bool IsSampler(OvrGpuProgramConstantType c);
		static ovr::uint32 GetElementSize(OvrGpuProgramConstantType ctype);

		bool IsFloat() const;
		bool IsInt() const;
		bool IsSampler() const;
		ovr::uint32 GetElementSize() const;
	};

	enum OvrAutoConstantType
	{
		ACT_MVP_MAT,
		ACT_MODEL_MAT,
		ACT_VIEW_MAT,
		ACT_PROJ_MAT,
		ACT_TEXM0_MAT,
		ACT_TEXM1_MAT,
		ACT_JOINTS,
		ACT_DIRECTION_LIGHT_COLOR,
		ACT_DIRECTION_LIGHT_DIR,
		ACT_DIRECTION_LIGHT_VP_MAT,
		ACT_ENV_LIGHT_COLOR,
		ACT_EYE_POSTION,
		ACT_POINT_LIGHT_COLOR,
		ACT_POINT_LIGHT_POS_RANGE,
		ACT_POINT_LIGHT_RADIUS_FOR_SHADOW,
		ACT_LIGHT_NUM,
		ACT_SPOT_LIGHT_COLOR,
		ACT_SPOT_LIGHT_POS_EXP,
		ACT_SPOT_LIGHT_DIR_CUT,
		ACT_SPOT_LIGHT_VP_MAT,
		ACT_SPOT_SHADOW_MAP,
		ACT_POINT_SHADOW_MAP,
		ACT_DIRECTION_SHADOW_MAP,
	};
	struct OvrAutoConstantDefinition
	{
		OvrAutoConstantDefinition(OvrAutoConstantType a_ac_type, OvrString a_name,
			OvrGpuProgramConstantType a_type, ovr::uint16 a_variability);
		OvrAutoConstantType ac_type;
		OvrString name;
		OvrGpuProgramConstantType const_type;
		ovr::uint16 variability;
	};
	struct OvrAutoConstant
	{
		OvrAutoConstant(OvrAutoConstantType a_type, ovr::uint16 a_mask,
			ovr::uint32 a_index);
		OvrAutoConstantType paramType;
		ovr::uint16 variability;
		ovr::uint32 physicalIndex;
	};

	typedef std::map<OvrString, OvrConstant> ConstantMap;
	typedef std::vector<OvrAutoConstant> AutoConstantList;
	typedef std::vector<OvrConstant> ConstantList;

	class OVR_ENGINE_API OvrGpuProgramConstants
	{

	public:
		OvrGpuProgramConstants();
		~OvrGpuProgramConstants();
		const OvrAutoConstantDefinition*	FindAutoConstantDefinition(const OvrString& name);
		const OvrConstant*			AddConstant(OvrString a_name, OvrGpuProgramConstantType a_type, ovr::uint32 a_size);
		ovr::uint32 GetFloatSize() { return float_size; }
		ovr::uint32 GetIntSize() { return int_size; }
		ovr::uint32 GetTextureLayerSize() { return tex_size; }
		ovr::uint32 GetAutoConstantCount();
		const OvrAutoConstant* GetAutoConstant(ovr::uint32 a_index);
		const OvrAutoConstant*	GetAutoConstant(const OvrAutoConstantType& a_type) const;
		const AutoConstantList* GetAutoConstantList() const;
		ovr::uint32				GetConstantCount();
		const ConstantMap*		GetConstantMap() { return &constant_map; }
		const OvrConstant*	GetNameConstant(const OvrString& a_name);
		const ConstantList*		GetCustomConstantList() const;
	private:
		ovr::uint32 float_size;
		ovr::uint32 int_size;
		ovr::uint32 tex_size;
		static OvrAutoConstantDefinition auto_constant_dictionary[];
		AutoConstantList auto_constant_list;
		ConstantList custom_constant_list;
		ConstantMap constant_map;
	};
	class OvrAutoParamSource
	{
	public:
		OvrMatrix4 world_matrix;
		OvrMatrix4 view_matrix;
		OvrMatrix4 projection_matrix;
		OvrMatrix4 mvp_matrix;
		OvrMatrix4 tex_mat0;
		OvrMatrix4 tex_mat1;
		OvrMatrix4* skeletal_matrix;
		ovr::uint32 skeletal_num;
		OvrVector4 direction_light_color_intensity;
		OvrVector3 direction_light_dir;
		OvrMatrix4 direction_light_vp_mat;
		OvrVector4 env_color_intensity;
		OvrVector4 point_light_color_intensity[OVR_MAX_POINT_LIGHT_NUM];
		OvrVector4 point_light_position_radius[OVR_MAX_POINT_LIGHT_NUM];
		float	   point_light_radius_for_shadow;
		OvrMatrix4 point_light_vp_mat[OVR_MAX_POINT_LIGHT_NUM];
		OvrVector4 spot_light_color_intensity[OVR_MAX_SPOT_LIGHT_NUM];
		OvrVector4 spot_light_position_exponent[OVR_MAX_SPOT_LIGHT_NUM];
		OvrVector4 spot_light_dir_cutoff[OVR_MAX_SPOT_LIGHT_NUM];
		OvrMatrix4 spot_light_vp_mat[OVR_MAX_SPOT_LIGHT_NUM];
		int		   light_nums[4];
		OvrVector3 eye_pos;
		OvrTexture* point_shadow_map;
		OvrTexture* direction_shadow_map;
		OvrTexture* spot_shadow_map;

		void SetCurrentLightList(std::vector<OvrLightComponent*>& a_lights);
		void SetCurrentCamera(OvrCameraComponent* a_camera);
	};
}