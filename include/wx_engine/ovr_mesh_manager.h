#pragma once
#include <map>
#include "ovr_mesh.h"
#include "wx_engine/ovr_resource_manager.h"
#include "wx_engine/ovr_singleton.h"
namespace ovr_engine
{
	enum MeshBuildType
	{
		kCube,
		kSphere,
		kCapsule,
		kPlane,
		kCylinder
	};
	struct MeshBuildParams
	{
		MeshBuildType type;
		float size;
		ovr::uint32 slices;
		ovr::uint32 stacks;
		float radius;
		float height;
		float scale;
	};
	class OVR_ENGINE_API OvrMeshManager : public OvrResourceManager, public OvrSingleton<OvrMeshManager>
	{
	public:
		virtual ~OvrMeshManager();
		bool	 Initialise();
		void	 UnInitialise();
		OvrMesh* Load(const OvrString& a_file_name);
		OvrMesh* Create(const OvrString& a_name);
		OvrMesh* CreateCube(OvrString a_name, float a_size);
		OvrMesh* CreatePlane(OvrString a_name, float a_size);
		OvrMesh* CreateCylinder(OvrString a_name, float scale, float r, float h, int n);
		OvrMesh* CreateSphere(OvrString a_name, float a_radius, ovr::uint32 a_slices, ovr::uint32 a_stacks);
		virtual OvrResource* CreateResource(OvrString a_name, bool is_manual) override;

	protected:
		void LoadManualCube(OvrMesh* a_mesh, const MeshBuildParams& param);
		void LoadManualPlane(OvrMesh* a_mesh, const MeshBuildParams& param);
		void LoadManualCylinder(OvrMesh* a_mesh, const MeshBuildParams& param);
		void LoadManualSphere(OvrMesh* a_mesh, const MeshBuildParams& param);
	};
}