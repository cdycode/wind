#pragma once
#include "wx_base/wx_quaternion.h"
#include "wx_base/wx_matrix4f.h"
#include <list>
#include <vector>
#include "ovr_engine_define.h"
#include "wx_engine/ovr_resource.h"
namespace ovr_engine
{
	class OvrSerializerImpl;
	//单个骨骼和单个动画数据的对应关系
	class OVR_ENGINE_API OvrJoint
	{
	public:
		OvrJoint() {}
		~OvrJoint() {}

		void	SetLocalMatrix(const OvrMatrix4& a_matrix);
		void	UpdateWorldMatrix();
		void	UpdateFinalMatrix();

		OvrString       name;
		ovr::uint32     index;
		OvrMatrix4		vertex_to_local_matrix;	//将mesh中的vertex变换到骨骼空间
		OvrMatrix4		local_to_parent_matrix;	//将mesh中的vertex变换到骨骼空间
		OvrMatrix4		bind_pose;				//绑定的姿态

		OvrJoint*				parent = nullptr;
		std::vector<OvrJoint*>	child_list;
		OvrMatrix4		local_matrix;								//骨骼动画的当前矩阵
		OvrMatrix4*		final_matrix = nullptr;						//最终的矩阵 local_matrix与parent的localmatrix相乘积 指向OvrrAnimationFrame中的矩阵
	};
	class OvrSkeletonManager;
	class OVR_ENGINE_API OvrSkeleton : public OvrResource
	{
	public:
		OvrSkeleton(OvrString a_name, bool a_is_manual);
		~OvrSkeleton();
		const OvrString GetName() { return name; }
		OvrJoint*	GetJoint(ovr::uint32 a_index);
		ovr::uint32 GetJointNum();
		void		SetJointNum(ovr::uint32 a_index);
		void		SetAutoCalculateLocalMatrix(bool a_local);
		void		UpdateMatrix();
		OvrMatrix4*	GetSkeletonMatrix();
		OvrMatrix4*	GetJointMatrix(ovr::uint32 a_joint_index);
	protected:
		virtual void LoadImpl() override;
		virtual void UnLoadImpl() override;
	protected:
		std::vector<OvrJoint>	joint_array;
		OvrMatrix4*				skeleton_matrix;
	};
}