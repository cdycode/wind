#pragma once
#include "wx_base/wx_type.h"
#include "wx_engine/ovr_resource.h"
#include "wx_base/wx_vector3.h"
#include "wx_base/wx_matrix4f.h"
namespace ovr_engine
{
	enum OvrPixelFormat
	{
		PF_DEPTH16,
		PF_DEPTH24,
		PF_R8G8B8A8,
		PF_R8G8B8,
	};

	enum OvrTextureType
	{
		TEXTURE_TYPE_2D,
		TEXTURE_TYPE_2D_ARRAY,
		TEXTURE_TYPE_CUBE,
		TEXTURE_TYPE_CUBE_ARRAY,
	};


	enum OvrTexturCubeFace
	{
		TEXTURE_CUBE_FACE_POSITIVE_X = 0,
		TEXTURE_CUBE_FACE_NEGATIVE_X = 1,
		TEXTURE_CUBE_FACE_POSITIVE_Y = 2,
		TEXTURE_CUBE_FACE_NEGATIVE_Y = 3,
		TEXTURE_CUBE_FACE_POSITIVE_Z = 4,
		TEXTURE_CUBE_FACE_NEGATIVE_Z = 5,
		TEXTURE_CUBE_FACE_NUM = 6
	};



	struct OvrImage
	{
		char		name[256];
		int			format;
		uint32_t	gl_format;
		uint32_t	gl_internal_format;
		int			width;
		int			height;
		const uint8_t*	data;
		int32_t		data_size;
		int			mipcount;
		uint32_t	data_type;
		bool		use_srgb;
		bool		image_size_stored;
		bool		is_cube_tex;
	};

	class OvrRenderTexture;
	class OVR_ENGINE_API OvrTexture : public OvrResource
	{
	public:
		OvrTexture(OvrString a_name, bool a_is_manual);
		virtual ~OvrTexture();
		OvrRenderTexture*	GetRenderTarget() { return render_target; }
		ovr::uint32			GetWidth() { return width; }
		ovr::uint32			GetHeight() { return height; }
		OvrPixelFormat		GetFormat() { return format; }
		ovr::uint32			GetDepth() { return depth; }
		ovr::uint32			GetMsaa() { return msaa; }
		bool				IsRenderTarget() { return is_render_target; }
		OvrTextureType		GetTextureType() { return type; }
		ovr::uint32			GetNumMipmaps() { return mip_map; }

		void SetTextureType(OvrTextureType a_type) { type = a_type; }
		void SetRenderTarget(bool a_rtt) { is_render_target =  a_rtt; }
		void SetWidth(ovr::uint32 a_width) { width = a_width; }
		void SetHeight(ovr::uint32 a_height) { height = a_height; }
		void SetFormat(OvrPixelFormat a_format) { format = a_format; }
		void SetNumMipmaps(ovr::uint32 a_mip_map) { mip_map = a_mip_map; }
		void SetDepth(ovr::uint32 a_depth) { depth =  a_depth; }
		void SetMsaa(ovr::uint32 a_msaa) { msaa = a_msaa; }
		virtual void BlitFromMemory(void* a_data, ovr::uint32 a_size) = 0;

		ovr::uint32 GetFaceNum();
		virtual bool LoadFromImage(const OvrImage& a_image) = 0;
		virtual void CreateInternalResources();
		virtual void FreeInternalResources();
	protected:
		virtual void LoadImpl() = 0;
		virtual void UnLoadImpl() = 0;
		virtual void CreateInternalResourcesImpl() = 0;
		virtual void FreeInternalResourcesImpl() = 0;
	protected:
		OvrRenderTexture* render_target;
		ovr::uint32 width;
		ovr::uint32 height;
		OvrPixelFormat format;
		ovr::uint32 depth;
		ovr::uint32 mip_map;
		ovr::uint32 msaa;
		bool is_render_target;
		OvrTextureType type;
		bool is_internal_created;
	};
}