#pragma once
#include "wx_engine/ovr_mesh_component.h"
#include "wx_engine/ovr_mesh_renderable.h"
namespace ovr_engine
{	
	class OvrSkeletonInstance;
	class OVR_ENGINE_API OvrSkinnedMeshComponent : public OvrMeshComponent
	{
	public:
		OvrSkinnedMeshComponent(OvrActor* a_actor);
		virtual ~OvrSkinnedMeshComponent();	
		virtual void			 Update() override;
		virtual OvrComponentType GetType() { return kComponentSkinnedMesh; }
		virtual OvrBox3f		 GetWorldBoundingBox() override;
		virtual void			 SetMesh(ovr_engine::OvrMesh* a_mesh) override;
		OvrSkeletonInstance*	 GetSkeletonInstance();
		bool					 UpdateAnimation(ovr_asset::OvrMotionFrame* a_frame);
	private:
		OvrSkeletonInstance*	skeleton_instance;
	};
}