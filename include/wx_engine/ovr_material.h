#pragma once
#include <vector>
#include "wx_base/wx_matrix4f.h"
#include "wx_engine/ovr_gpu_program.h"
#include "wx_engine/ovr_resource.h"
#include "wx_engine/ovr_engine_define.h"

namespace ovr_engine
{
	enum CullingMode
	{
		CULL_NONE,
		CULL_BACK,
		CULL_FRONT
	};

	enum SceneBlendFactor
	{
		SBF_ONE,
		SBF_ZERO,
		SBF_DEST_COLOUR,
		SBF_SOURCE_COLOUR,
		SBF_ONE_MINUS_DEST_COLOUR,
		SBF_ONE_MINUS_SOURCE_COLOUR,
		SBF_DEST_ALPHA,
		SBF_SOURCE_ALPHA,
		SBF_ONE_MINUS_DEST_ALPHA,
		SBF_ONE_MINUS_SOURCE_ALPHA

	};

	class OvrMaterialManager;
	class OVR_ENGINE_API OvrMaterial : public OvrResource
	{	
		friend OvrMaterialManager;
	public:
		const OvrString& GetName() { return name; }
		void SetProgram(const OvrString& a_program_name);
		void SetProgram(OvrGpuProgram* a_program);
		OvrGpuProgram* GetProgram();
		bool GetDepthWriteEnable() { return is_depth_write; }
		bool GetDepthCheckEnable() { return is_depth_check; }
		CullingMode GetCullMode() { return cull_mode; }
		SceneBlendFactor GetSourceBlendFactor()const { return src_factor; }
		SceneBlendFactor GetDestBlendFactor() const { return dest_factor; }
		void SetDepthWriteEnable(bool a_enable) { is_depth_write = a_enable; }
		void SetDepthCheckEnable(bool a_enable) { is_depth_check = a_enable; }
		void SetSceneBlend(SceneBlendFactor a_src, SceneBlendFactor a_dest);
		void SetCullMode(CullingMode a_mode) { cull_mode = a_mode; }
		void UpdateAutoParameters(const OvrAutoParamSource& src, ovr::uint16 mask);
		bool IsTransparent();
		bool IsSkeletonInclude() { return is_skeleton_include; }
		bool SetMainTexture(OvrTexture* a_main_tex);
		OvrTexture* GetMainTexture();
		OvrGpuProgramParameters* GetProgramParameters(){ return params; }
	protected:
		virtual void LoadImpl() override;
		virtual void UnLoadImpl() override;
	private:
		OvrMaterial(OvrString a_name, bool a_is_manual);
		virtual ~OvrMaterial();
	private:
		OvrGpuProgram* shader;
		bool is_depth_write;
		bool is_depth_check;
		bool is_skeleton_include;
		CullingMode cull_mode;
		OvrGpuProgramParameters* params;
		SceneBlendFactor src_factor;
		SceneBlendFactor dest_factor;
		OvrString main_texture_name;
	};

	
}
