﻿#pragma once
#include "wx_base/wx_matrix4f.h"
#include "wx_engine/ovr_engine_define.h"
#include "wx_engine/ovr_render_queue.h"
namespace ovr_engine 
{
	class OvrSkyBox : public OvrRenderable
	{
	public:
		OvrSkyBox();
		OvrSkyBox(float a_size);
		~OvrSkyBox();
		virtual OvrMatrix4  GetWorldTransform() const override { return OvrMatrix4(); }
		virtual OvrMaterial* GetMaterial() override { return material; }
		virtual void GetRenderOperation(OvrRenderOperation& a_op) override;
		void SetSize(float a_size);
		void SetMaterial(OvrMaterial* a_material) { material = a_material; }
	private:
		float                size;
		OvrMaterial*		 material;
		OvrVertexData*       vertex_data;
		OvrIndexData*        index_data;
	};
}
