﻿#ifndef OVR_ENGINE_ROOT_H_
#define OVR_ENGINE_ROOT_H_
#include <vector>
#include "wx_engine/ovr_plugin.h"
#include "wx_base/wx_type.h"
namespace ovr_engine {
	class IOvrUiTextManager;
}
	
namespace ovr_engine 
{
	class OvrMeshManager;
	class OvrMaterialManager;
	class OvrSkeletonManager;
	class OvrParticleSystemManager;
	class OvrTextureManager;
	class OvrLogger;
	class OvrScene;
	class OvrRenderSystem;
	class OvrSerializerImpl;
	class OvrHardwareBufferManager;
	class OvrGpuProgramManager;
	typedef std::vector<OvrScene*> SceneList;
	typedef std::vector<OvrScene*>::iterator SceneListIterator;
	class OVR_ENGINE_API OvrEngine
	{
	public:
		OvrEngine();
		virtual ~OvrEngine();
		static OvrEngine*	GetIns();
		static void			DelIns();
		bool				Init();
		void				Uninit();
		void				SetResourceLocation(const OvrString& a_path);
		const OvrString&	GetResourceLocation();

		void 				Reshape(int a_new_width, int a_new_height);
		int					GetWindowWidth() { return window_width; }
		int					GetWindowHeight() { return window_height; }

		OvrScene*			CreateScene();
		void				DestoryScene(OvrScene*& a_scene);
		ovr::uint32 		GetSceneCount();
		SceneListIterator 	GetSceneIterator() { return scene_list.begin();}
		OvrScene* 			GetScene(const OvrString& a_scene_name);

		OvrRenderSystem*			GetRenderSystem() { return render_system; }

		void				LoadPlugin(const OvrString& a_plugin_name);
		void				UnLoadPlugin(const OvrString& a_plugin_name);
		void				InstallPlugin(OvrPlugin* plugin);
		void				UnInstallPlugin(OvrPlugin* plugin);
	private:
		static OvrEngine* s_ins;
		bool	is_inited;
		int		window_width;
		int		window_height;
		SceneList	scene_list;
		OvrRenderSystem*  render_system;
		IOvrUiTextManager* ui_text_mgr;
		OvrString resource_path;
		std::vector<OvrPlugin*> plugins;
		OvrMeshManager* mesh_manager;
		OvrMaterialManager* material_manager;
		OvrTextureManager* texture_manager;
		OvrSkeletonManager* skeleton_manager;
		OvrParticleSystemManager* particle_manager;
		OvrSerializerImpl* serialize;
		OvrHardwareBufferManager* hw_manager;
		OvrGpuProgramManager* program_manager;
		OvrLogger* logger;
	};
}

#endif
