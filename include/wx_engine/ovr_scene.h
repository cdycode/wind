﻿#ifndef OVR_ENGINE_OVR_SCENE_H_
#define OVR_ENGINE_OVR_SCENE_H_
#include <map>
#include "wx_base/wx_unique_name.h"
#include "wx_engine/ovr_engine_define.h"
#include "wx_engine/ovr_gpu_program_constants.h"
#include "wx_engine/ovr_render_queue.h"
namespace ovr_engine 
{
	class OvrEngine;
	class IOvrUiTextManager;
	class OvrRenderSystem;
	class OvrMesh;
	class OvrTextActor;
	class OvrParticleActor;
	class OvrRaySceneQuery;
	class OvrSkyBox;
	class OvrComponent;
	class OvrMaterial;
	class OVR_ENGINE_API OvrScene
	{
		friend OvrActor;
		friend OvrEngine;
	public:
		typedef std::map<OvrString, OvrActor*> ActorMap;
		typedef std::vector<OvrComponent*> ComponentCollection;
		typedef std::map<int, ComponentCollection*> ComponentCollectionMap;
	public:
		class SceneRenderableCollectorVistor : public OvrRenderableCollectorVistor
		{
		public:
			virtual void visit(OvrRenderable* a_rnd);
			virtual void visit(RenderablePass* a_rnd_pass);
			virtual void visit(OvrMaterial* material);
			OvrScene* target_scene = nullptr;
			OvrMaterial* use_material = nullptr;
		};

		virtual void				SetDisplayName(const char* a_name)  { display_name = a_name; }
		virtual const OvrString&	GetDisplayName()const  { return display_name; }
		virtual const OvrString&	GetUniqueName()const { return unique_name; }
		virtual	void			 	SetUniqueName(const OvrString& a_name) { unique_name = a_name; }
		void						SetActiveCamera(OvrActor* a_camera);
		OvrActor*					GetActiveCamera() { return main_camera; }
		OvrRenderSystem*			GetRenderSystem() { return render_system; }
		OvrActor*					GetRoot();

		template <typename T>
		T*	CreateActorT(OvrVector3 a_position = OvrVector3::ZERO, OvrQuaternion a_quat = OvrQuaternion::IDENTITY, OvrVector3 a_scale = OvrVector3::UNIT_SCALE)
		{
			T* actor = new T(this);
			actor->SetPosition(a_position);
			actor->SetQuaternion(a_quat);
			actor->SetScale(a_scale);
			actor_map[actor->GetUniqueName()] = actor;
			root_actor->AddChild(actor);
			return actor;
		}	

		OvrActor*	CreateActor(OvrVector3 a_position = OvrVector3::ZERO, OvrQuaternion a_quat = OvrQuaternion::IDENTITY, OvrVector3 a_scale = OvrVector3::UNIT_SCALE);
		
		bool				DestoryActor(OvrActor* a_actor);
		OvrActor*			GetActor(const OvrString& a_unique_name);
		void				SetShadowEnable(bool a_enable) { enable_shadow = a_enable; }
		void				SetAmbientColor(const OvrVector4& a_ambient);
		const OvrVector4&	GetAmbientColor() { return ambient_color; }
		void				UpdateScene();
		void				DrawScene();
		const ActorMap&		GetActorMap() { return actor_map; }
		IOvrUiTextManager*  GetTextManager() { return ui_text_mgr; }
		OvrRaySceneQuery*	CreateRaySceneQuery();
		void				DestorySceneQuery(OvrRaySceneQuery* a_query);
		void                SetSkyBox(OvrMaterial* a_material, float a_distance = 5000.0f);
		void                NotifyComponentCreated(OvrComponent* a_component);
		void				NotifyComponentDestroyed(OvrComponent* a_component);
		ComponentCollection* GetComponentCollection(int a_type);
		ComponentCollectionMap& GetComponentCollectionMap() { return component_collection_map; }
	private:
		OvrScene(OvrRenderSystem* a_rs, IOvrUiTextManager* a_ui_mgr);
		virtual ~OvrScene();
		void PrepareRenderQueue();
		void EnsureTextureShadow();
		void RenderVisibleObjects();
		void RenderVisibleObjectsShadowMap(std::vector<OvrLightComponent*> a_lights);//add
		void RenderTextureShadowCasterQueueObjects();
		void RenderSceneToTexture();
		void RenderBasicQueueObjects();
		void RenderObjects(const OvrRenderableCollector* a_collector, OrganisationMode a_om);
		void RenderSingleObject(OvrRenderable* a_rnd, OvrMaterial* a_material);
		void FindVisableObjects();
		OvrMaterial* SetMaterial(OvrMaterial* a_material, bool a_shadowDerivation = true);
		OvrMaterial* DeriveShadowCasterMaterial(OvrMaterial* a_material);
		bool ValidateRenderableForRendering(OvrMaterial* a_material, OvrRenderable* a_rnd);
	private:
		const ovr::uint32	MAX_LIGHT_NUM = 8;
		OvrString	display_name;
		OvrString	unique_name;
		OvrActor*   root_actor;
		OvrActor*	main_camera;
		ActorMap	actor_map;

		bool		enable_shadow;
		OvrVector4	ambient_color;
		ovr::uint16 shader_param_dirty;

		OvrRenderSystem* render_system;
		OvrRenderQueue* render_queue;
		SceneRenderableCollectorVistor* collector_visitor;
		OvrAutoParamSource auto_params;
		IOvrUiTextManager* ui_text_mgr;
		bool is_prepare_shadow_tex;
		std::vector<OvrMaterial*> shadow_caster_material;
		OvrTexture*  shadow_tex;
		OvrTexture*  spot_tex_array;
		OvrTexture*  point_tex_array;
		OvrLightComponent* cur_shadow_caster_light;
		ComponentCollectionMap component_collection_map;
		OvrSkyBox* sky_box;
	};
}

#endif
