#pragma once
#include "wx_base/wx_type.h"
#include "wx_engine/ovr_engine_define.h"
namespace ovr_engine
{
	class OVR_ENGINE_API OvrReferenceCounted
	{
	public:
		OvrReferenceCounted()
			: reference_counter(1)
		{
		}

		virtual ~OvrReferenceCounted()
		{
		}

	
		void Grab() const { ++reference_counter; }

		bool Drop() const
		{
			--reference_counter;
			if (!reference_counter)
			{
				delete this;
				return true;
			}

			return false;
		}

		ovr::uint32 GetReferenceCount() const
		{
			return reference_counter;
		}
	private:
		mutable ovr::uint32 reference_counter;
	};

} 

