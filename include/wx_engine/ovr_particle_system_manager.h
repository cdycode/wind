#ifndef  _OVR_PARTICLE_SYSTEM_MANAGER_H_
#define  _OVR_PARTICLE_SYSTEM_MANAGER_H_
#include "wx_engine/ovr_engine_define.h"
#include "wx_base/wx_singleton.h"
#include <map>
#include "wx_engine/OvrParticleEmitter.h"
namespace ovr_engine {
	class OvrParticleSystem;
	class OvrParticleEmitter;
	class OVR_ENGINE_API OvrParticleSystemManager : public ovr_singleton<OvrParticleSystemManager>
	{
	public:
		OvrParticleSystemManager();
		~OvrParticleSystemManager();
		bool Initialise();
		void UnInitialise();
		OvrParticleSystem*  CreateParticleSystemByName(const OvrString& a_name, const OvrString& a_type = "OvrParticleEmitterBox");
		OvrParticleSystem* Load(const OvrString& a_file_name);
		OvrParticleSystem* GetFeatherparticleSystem();
	private:
		void InitInsideSystems();
	private:
		int  max_partcile_num;
		std::map<OvrString,OvrParticleSystem*>   particle_systems;
		OvrParticleSystem* feather_sys;
	};
}
#endif