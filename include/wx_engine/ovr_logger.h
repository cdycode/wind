#pragma once
#include "wx_engine/ovr_engine_define.h"
#include "wx_engine/ovr_singleton.h"
#include "wx_base/wx_string.h"
#include <fstream>
using namespace std;
namespace ovr_engine
{
	enum OvrLogLevel
	{
		LL_INFORMATION,
		LL_WARNING,
		LL_ERROR,
	};
	class OVR_ENGINE_API OvrLogger : public OvrSingleton<OvrLogger>
	{
	public:
		OvrLogger();
		~OvrLogger();
		void Initialise(OvrString a_file_name);
		void UnInitialise();
		OvrString GetLevelString(OvrLogLevel a_level);
		void Log(OvrString a_msg, OvrLogLevel a_level);
	private:
		ofstream out_file;
	};
}