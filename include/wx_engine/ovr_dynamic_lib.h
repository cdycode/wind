#pragma once
#include "wx_base/wx_type.h"
#include "wx_base/wx_string.h"
#include "wx_engine/ovr_logger.h"
#include "wx_engine/ovr_engine_define.h"
#include <map>

#ifdef OVR_OS_WIN32
#include <windows.h>
#	 define DYNLIB_HANDLE HMODULE
#    define DYNLIB_LOAD( a ) LoadLibraryA(a)
#    define DYNLIB_GETSYM( a, b ) GetProcAddress( a, b )
#    define DYNLIB_UNLOAD( a ) FreeLibrary( a )
#else
#include <dlfcn.h>
#    define DYNLIB_HANDLE void*
#    define DYNLIB_LOAD( a ) dlopen( a, RTLD_LAZY | RTLD_GLOBAL)
#    define DYNLIB_GETSYM( a, b ) dlsym( a, b )
#    define DYNLIB_UNLOAD( a ) dlclose( a )
#endif


namespace ovr_engine
{
	class OVR_ENGINE_API OvrDynLib
	{
	public:
		OvrDynLib(OvrString a_name);
		virtual ~OvrDynLib();
		void  Load();
		void  UnLoad();
		void* GetSymbol(const OvrString& a_name);
		const OvrString& GetName() { return name; }
	private:
		DYNLIB_HANDLE handle;
		OvrString name;
	};
}