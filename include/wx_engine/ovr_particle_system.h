#ifndef __ovr_particle_system_h__20171228__
#define __ovr_particle_system_h__20171228__

#include "ovr_particle.h"
#include "OvrParticleEmitter.h"
#include "ovr_particle_controller.h"
#include "wx_engine/ovr_engine_define.h"
#include<vector>
namespace ovr_engine {
	class OvrCamera;
	class OVR_ENGINE_API OvrParticleSystem
	{
	public:
		OvrParticleSystem();
		~OvrParticleSystem();
	public:
		virtual void update(float a_time);
		void AddParticleContrller(OvrParticleController* a_controller);
		void SetMaxParticleNum(uint32_t a_particle_num);
		void SetFaceCamera(OvrCamera* a_cam_ptr);
		void ChangeEmitter(const OvrString& a_emitter);
		const int GetMaxPartcileNum();
		std::vector<OvrParticle>&	GetParticles(uint32_t& ao_particle_num);
	public:
		OvrParticleEmitter* GetEmitter();
	private:
		std::vector<OvrParticleController*>	controllers;
		int								controller_num;
		std::vector<OvrParticle>	particles;
		OvrParticleEmitter*		emitter;
		int					current_particle_num;
		int					max_particle_num;
		float						last_time;
		OvrCamera*					camera_ptr;
	};
}
#endif
