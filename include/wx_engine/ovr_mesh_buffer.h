#ifndef OVR_ENGINE_OVR_MESH_BUFFER_H_
#define OVR_ENGINE_OVR_MESH_BUFFER_H_
#include "wx_base/wx_box3f.h"
#include "wx_base/wx_matrix4f.h"
#include "wx_base/wx_string.h"
#include "wx_engine/ovr_engine_define.h"
#include "wx_engine/ovr_reference_counted.h"
#include "wx_engine/ovr_renderable.h"
#include "wx_engine/ovr_vertex_index_data.h"
namespace ovr_asset
{
	struct OvrPose;
}
namespace ovr_engine
{
	class OvrMaterial;
	class OvrMesh;
	class OvrSerializerImpl;
	class OvrMeshManager;
	class OVR_ENGINE_API OvrMeshBuffer : public OvrReferenceCounted
	{
	public:
		OvrMeshBuffer(OvrMesh* a_mesh);
		~OvrMeshBuffer();
		const	OvrString&		GetMaterialName() ;
		void					SetMaterialName(const OvrString& a_material_name);
		bool					IsMaterialSet();
		void					SetPrimitiveType(OvrPrimitiveType a_type);
		OvrPrimitiveType		GetPrimitiveType() { return primitive_type; }
		OvrMesh*				GetParent() { return mesh; }
	public:
		OvrVertexData*			vertex_data;
		OvrIndexData*			index_data;
	private:
		OvrMesh*			mesh;
		OvrString			material_name;
		bool				is_material_set;
		OvrPrimitiveType    primitive_type;
	};
}
#endif
