﻿#pragma once
#include "wx_engine/ovr_render_queue.h"
#include "wx_asset_manager/ovr_motion_frame.h"
namespace ovr_engine 
{
	class OvrMeshComponent;
	class OvrSkinnedMeshComponent;
	class OvrStaticMeshComponent;
	class OvrCameraComponent;
	class OvrMeshRenderable : public OvrRenderable
	{
	public:
		OvrMeshRenderable(OvrMeshComponent* a_mesh_component, OvrMeshBuffer* a_mesh_buf, ovr::uint32 a_index = 0);
		virtual  ~OvrMeshRenderable();
		virtual OvrMatrix4 GetWorldTransform() const override;
		virtual OvrMatrix4* GetSkeletonMatrix(ovr::uint32& a_num) const override;
		virtual void		GetRenderOperation(OvrRenderOperation& a_op) override;
		virtual OvrMaterial* GetMaterial() override;
		virtual bool GetCastShadowEnable() override;
		virtual void SetMaterialIndex(const ovr::uint32& a_index);
		virtual float GetSquaredViewDepth(const OvrCameraComponent* a_cam) override;
		virtual void UpdateAnimation(ovr_asset::OvrMorphFrame* a_frame);
	protected:
		OvrMeshBuffer* mesh_buffer;
		ovr::uint32 material_index;
		std::vector<ovr::int32> pose_index;
		std::vector<ovr::int32> pose_weight;
		OvrMeshComponent* mesh_component;

	};
}
