﻿#pragma  once
#include <map>
#include <vector>
#include "wx_base/wx_string.h"
#include "wx_engine/ovr_engine_define.h"
#include "wx_engine/ovr_gpu_program_parameters.h"
namespace ovr_engine
{
	class OVR_ENGINE_API OvrGpuProgram
	{
	public:
		OvrGpuProgram(OvrString a_name);
		virtual OvrString		GetName() { return name; }
		virtual void			BindProgram() = 0;
		virtual void			UnBindProgram() = 0;
		virtual void			BindProgramParameters(OvrGpuProgramParameters* params, ovr::uint16 mask) = 0;
		virtual OvrGpuProgramParameters*CreateParameters();
		virtual void			DestoryParameters(OvrGpuProgramParameters* params);
		virtual const OvrGpuProgramConstants* GetShaderConstants();
	protected:
		OvrString	 name;
		OvrGpuProgramConstants* constants;
	};
}