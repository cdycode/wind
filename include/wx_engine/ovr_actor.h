#ifndef OVR_ENGINE_OVR_ACTOR_H_
#define OVR_ENGINE_OVR_ACTOR_H_
#include <type_traits>
#include <vector>
#include "wx_base/wx_vector3.h"
#include "wx_base/wx_quaternion.h"
#include <wx_base/wx_matrix4f.h>
#include <wx_base/wx_ray3f.h>
#include "wx_base/wx_box3f.h"
#include <wx_base/wx_transform.h>
#include "wx_engine/ovr_engine_define.h"
#include "wx_engine/ovr_object.h"
#include "wx_engine/ovr_component.h"
namespace ovr_engine 
{
	enum OvrTransformSpace
	{
		kSpaceLocal,
		kSpaceParent,
	};
	class OvrScene;	
	class OvrRenderQueue;
	class OVR_ENGINE_API OvrActor : public OvrObject
	{
	public:
		OvrActor(OvrScene* a_scene);
		virtual ~OvrActor();


		virtual void			SetUniqueName(const char* a_name) override;
		OvrScene*				GetScene();
		virtual void			Update(bool a_force_update = false);

		void	SetVisible(bool a_visible) { is_visable = a_visible; }
		bool	IsVisible() const { return is_visable; }

		void	SetStatic(bool a_static) { is_static = a_static; }
		bool	IsStatic() const { return is_static; }

		const OvrMatrix4&		GetLocalTransform()const { return local_matrix; }
	    const OvrMatrix4&		GetWorldTransform()const { return world_matrix; }
		OvrQuaternion           GetWorldQuaternion();
		OvrVector3	            GetWorldPosition();

		const OvrTransform&		GetTransform()const { return transform; }
		const OvrVector3&		GetPosition()const { return transform.position; }
		const OvrVector3&		GetScale()const { return transform.scale; }
		const OvrQuaternion&	GetQuaternion()const { return transform.quaternion; }
		void	SetTransform(const OvrTransform& a_transform);
		void	SetPosition(const OvrVector3& a_position);
		void	SetScale(const OvrVector3& a_scale);
		void	SetQuaternion(const OvrQuaternion& a_quaternion);
		void    ResetTransform();
		void	Yaw(float a_angle, OvrTransformSpace a_space = kSpaceLocal);
		void	Roll(float a_angle, OvrTransformSpace a_space = kSpaceLocal);
		void	Pitch(float a_angle, OvrTransformSpace a_space = kSpaceLocal);
		void	Rotate(const OvrVector3& a_axis, const float a_angle, OvrTransformSpace a_space = kSpaceLocal);
		void	Rotate(const OvrQuaternion a_quat, OvrTransformSpace a_space = kSpaceLocal);
		void	GetEulerAngle(float& a_pitch, float& a_yaw, float& a_roll);
		void	SetEulerAngle(float a_pitch, float a_yaw, float a_roll);

		bool	MoveForwardBack(const float a_distance);
		bool	MoveLeftRight(const float a_distance);

		const OvrVector3&	GetCenterPosition()const { return center_position; }

		bool		HasParent();
		OvrActor*	GetParent() { return parent; }
		OvrActor*   CreateChildActor();
		OvrActor*	GetChild(ovr::uint16 a_index);
		OvrActor*	GetChild(const OvrString& a_name);
		ovr::uint32 GetChildCount();
		bool		AddChild(OvrActor* a_actor, bool a_world_pos_stays = false);
		OvrActor*	RemoveChild(ovr::uint16 a_index);
		OvrActor*	RemoveChild(OvrActor* a_child);
		OvrActor*	RemoveChild(const OvrString& a_name);
		void		RemoveAllChild();
		
		virtual void 		FindVisibleObjects(OvrRenderQueue* a_queue);
		void 				ShowBoundingBox(bool a_is_show) { show_bounding_box = a_is_show; }
		bool 				GetShowBoundingBox() { return show_bounding_box; }

		template <typename T> 
		T*	AddComponent()
		{
			T* ret = GetComponent<T>();
			if (ret)
			{
				return nullptr;
			}
			ret = new T(this);
			components.push_back(ret);
			scene->NotifyComponentCreated(ret);
			return ret;
		}
		
		template <typename T>
		T*	GetComponent()
		{
			std::vector<OvrComponent*>::iterator it = components.begin();
			for (; it != components.end(); it++)
			{
				T* ret = dynamic_cast<T*>(*it);
				if (ret)
					return ret;
			}
			return nullptr;
		}
		
		template <typename T>
		std::vector<T*>	GetComponents()
		{
			std::vector<T*> list;
			std::vector<OvrComponent*>::iterator it = components.begin();
			for (; it != components.end(); it++)
			{
				T* ret = dynamic_cast<T*>(*it);
				if (ret)
					list.push_back(ret);
			}
			return list;
		}

		void			RemoveComponent(OvrComponent* a_component);
		void			RemoveAllComponents();
		const std::vector<OvrComponent*>& GetAllComponents() { return components; }
	protected:
		void			SetParent(OvrActor* a_actor);
		void			UpdateChild(bool a_force_update = false);
	protected:
		OvrTransform	transform;
		OvrVector3      center_position;
		OvrMatrix4		local_matrix;
		OvrMatrix4		world_matrix;

		OvrScene*       scene;
		OvrActor*       parent;
		typedef std::vector <OvrActor*> ChildList;
		ChildList		child_list;
		bool			is_visable;
		bool			is_static;
		bool			is_need_update;
		bool            show_bounding_box;
		//
		std::vector<OvrComponent*> components;
		
	};
}


#endif
