#pragma once
#include <map>
#include "wx_engine/ovr_engine_define.h"

namespace ovr_engine
{
	class OvrResource;
	class OVR_ENGINE_API OvrResourceManager
	{
	public:
		OvrResourceManager();
		virtual ~OvrResourceManager();
		virtual OvrResource* GetByName(const OvrString& a_file_name);
		virtual void Remove(const OvrString& a_name);
		virtual void Remove(OvrResource* a_mesh);
		virtual void RemoveAll();
		virtual OvrResource* CreateOrRetrieve(OvrString a_name, bool is_manual);
		virtual OvrResource* CreateResource(OvrString a_name, bool is_manual) = 0;

	protected:
		std::map<OvrString, OvrResource*> resource_map;
	};
}