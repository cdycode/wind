﻿#ifndef OVR_ASSET_SERIALIZE_OVR_MESH_H_
#define OVR_ASSET_SERIALIZE_OVR_MESH_H_

#include <vector>
#include <wx_base/wx_vector4.h>
#include <wx_base/wx_array.h>
#include <wx_base/wx_vector3.h>
#include "ovr_archive_asset.h"

namespace ovr_asset 
{
	enum OvrVertexAttribute
	{
		kVertexAttributePosition = 1 << 0,
		kVertexAttributeNormal = 1 << 1,
		kVertexAttributeTangent = 1 << 2,
		kVertexAttributeBinormal = 1 << 3,
		kVertexAttributeColor = 1 << 4,
		kVertexAttributeUV0 = 1 << 5,
		kVertexAttributeUV1 = 1 << 6,
		kVertexAttributeIndices = 1 << 7,
		kVertexAttributeWeights = 1 << 8,
		kVertexAttributeMorph = 1 << 9,
		kVertexAttributeAll = 1 << 31
	};

	struct OVR_ASSET_MANAGER_API OvrVertex
	{
		OvrVector3	position;
		OvrVector3	normal;
		OvrVector3	tangent;
		OvrVector3	binormal;
		OvrVector4	color;
		OvrArray2f	uv0;
		OvrArray2f	uv1;
		OvrArray4u8	bone_index;
		OvrArray4f	bone_weights;
		void operator = (const OvrVertex& a_vertex);
		bool operator == (const OvrVertex& a_vertex) const;

		bool	Serialize(OvrArchive& a_serialize, ovr::uint32 a_attributes);
	};

	//后面将增加包围盒、包围球相关信息
	struct OVR_ASSET_MANAGER_API OvrPivotInfo
	{
		OvrVector3	rotation_pivot;
		OvrVector3	rotation_offset;	//用途未知 来源FBX解析
		OvrVector3	scale_pivot;
		OvrVector3	scale_offset;		//用途位置 来源FBX解析

		void	operator = (const OvrPivotInfo& a_info);
		bool	Serialize(OvrArchive& a_serialize);
	};

	enum OvrSerializeSurfaceType 
	{
		kSerializeSurfaceEmpty = 0,
		kSerializeSurface,
	};

	class OVR_ASSET_MANAGER_API OvrArchiveSurface
	{
	public:
		OvrArchiveSurface();
		virtual ~OvrArchiveSurface();

		void	SetType(OvrSerializeSurfaceType a_type) { surface_type = a_type; }
		OvrSerializeSurfaceType	GetType() { return (OvrSerializeSurfaceType)surface_type; }

		ovr::uint32			AddSurface(OvrArchiveSurface* a_node) { subsurface_vector.push_back(a_node); return subsurface_vector.size() - 1; }
		OvrArchiveSurface*			GetSurface(ovr::uint32 a_index) { return subsurface_vector[a_index]; }
		ovr::uint32			GetSurfaceNum() { return subsurface_vector.size(); }

		void		SetName(const char* a_name) { node_name = a_name; }
		const char*	GetName()const { return node_name.GetStringConst(); }

		void		SetVertexAttributes(ovr::uint32 a_attributes) { vertex_attributes = a_attributes; }
		ovr::uint32	GetVertexAttributes()const { return vertex_attributes; }

		//设置数目 并分配内存
		bool	SetVertexNum(ovr::uint32 a_num);
		bool	SetTriangleNum(ovr::uint32 a_num);
		bool	SetMaterialNum(ovr::uint32 a_num);

		ovr::uint32	GetVertexNum()const { return vertex_num; }
		ovr::uint32	GetTriangleNum()const { return triangle_num; }
		ovr::uint32	GetMaterialNum()const { return material_num; }

		OvrVertex&			GetVertex(ovr::uint32 a_index);
		OvrArray3i&			GetTriangle(ovr::uint32 a_index);
		OvrString&			GetMaterialPath(ovr::uint32 a_index);

		//获取重心点相关信息
		void				SetPiovtInfo(const OvrPivotInfo& a_info) { pivot_info = a_info; }
		const OvrPivotInfo& GetPiovtInfo()const { return pivot_info; }

		//中心点位置
		void	SetCenterPosition(const OvrVector3& a_position) { center_position = a_position; }
		const OvrVector3&	GetCenterPosition()const { return center_position; }

		bool					Serialize(OvrArchive& a_archive);
	private:
		bool	SerializeVertex(OvrArchive& a_archive);
		bool	SerializeTriangle(OvrArchive& a_archive);
		bool	SerializeMaterial(OvrArchive& a_archive);
	private:
		ovr::int32						surface_type;
		OvrString						node_name;

		ovr::uint32						vertex_attributes;
		ovr::uint32						vertex_num;
		ovr::uint32						triangle_num;
		ovr::uint32						material_num;

		OvrVertex*						vertexs;
		OvrArray3i*						triangle;
		OvrString*						material_path;

		OvrPivotInfo					pivot_info;
		OvrVector3						center_position;

		std::vector<OvrArchiveSurface*>		subsurface_vector;
	};

	class OVR_ASSET_MANAGER_API OvrArchiveMesh : public OvrBinaryAsset
	{
	public:
		OvrArchiveMesh();
		virtual ~OvrArchiveMesh();

		virtual OvrAssetType	GetType()const override { return kAssetMesh; }

		//添加一个已经存在的surface
		ovr::uint32			AddSurface(OvrArchiveSurface* a_node);
		//创建一个新的Surface并添加
		OvrArchiveSurface*			AddSurface();
		//获取Surface
		OvrArchiveSurface*			GetSurface(ovr::uint32 a_index);

		ovr::uint32			GetSurfaceNum()const { return surface_vector.size(); }

		void				SetSkeletonPath(const char* a_path) { skeleton_path = a_path; }
		const OvrString&	GetSkeletonPath()const { return skeleton_path; }
		void				SetPosePath(const char* a_path) { pose_path = a_path; }
		const OvrString&	GetPosePath()const { return pose_path; }

	protected:
		virtual bool	Serialize(OvrArchive& a_archive) override;

	private:
		OvrString					skeleton_path;
		OvrString					pose_path;
		std::vector<OvrArchiveSurface*>	surface_vector;
	};
}

#endif

