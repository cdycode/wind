﻿#ifndef OVR_ASSET_SERIALIZE_OVR_SKELETAL_ANIMATION_H_
#define OVR_ASSET_SERIALIZE_OVR_SKELETAL_ANIMATION_H_

#include <wx_base/wx_matrix4f.h>
#include <wx_base/wx_string.h>
#include <vector>
#include "ovr_archive_asset.h"

namespace ovr_asset 
{
	class OvrMotionFrame;
	class OVR_ASSET_MANAGER_API OvrArchiveSkeletalAnimation : public OvrBinaryAsset
	{
	public:
		OvrArchiveSkeletalAnimation();
		virtual ~OvrArchiveSkeletalAnimation();

		virtual OvrAssetType	GetType()const override { return kAssetSkeletalAnimation; }

		void			SetFrameRate(ovr::uint32 a_frame_rate) { frame_rate = a_frame_rate; }
		void			SetDuration(float a_seconds) { duration = a_seconds; }
		ovr::uint32		GetFrameRate()const { return frame_rate; }
		float			GetDuration() { return duration; }

		void				AddJointName(const OvrString& a_name) { joint_name_list.push_back(a_name); }
		ovr::uint32			GetJointNum()const { return joint_name_list.size(); }
		const OvrString&	GetJointName(ovr::uint32 a_index) { return joint_name_list[a_index]; }

		bool						AddFrame(OvrMotionFrame* a_frame);
		ovr::uint32					GetFrameNum()const { return frame_list.size(); }
		OvrMotionFrame*	GetFrame(ovr::uint32 a_index) { return frame_list[a_index]; }

	protected:
		virtual bool	Serialize(OvrArchive& a_archive) override;

		bool	SerializeFrame(OvrArchive& a_archive, OvrMotionFrame* a_frame);

	private:
		ovr::uint32	frame_rate = 0;
		float		duration = 0.0f;
		std::vector<OvrString>						joint_name_list;
		std::vector<OvrMotionFrame*>	frame_list;
	};
}

#endif
