#ifndef OVR_ASSET_SERIALIZE_OVR_POSE_H_
#define OVR_ASSET_SERIALIZE_OVR_POSE_H_


#include <map>
#include <wx_base/wx_vector4.h>
#include <wx_base/wx_array.h>
#include <wx_base/wx_vector3.h>
#include "ovr_motion_frame.h"
#include "ovr_archive_asset.h"

namespace ovr_asset
{
	struct OvrPose
	{

		OvrPose()
		{
			name = "";
			surface_name = "";
			vertex_offset_map.clear();
		}

		~OvrPose()
		{
		}

		OvrString							name;
		OvrString							surface_name;
		std::map<ovr::uint32, OvrVector3>	vertex_offset_map;

		bool Serialize(OvrArchive& a_serialize);
	}; 

	class OVR_ASSET_MANAGER_API OvrArchivePoseContainer : public OvrBinaryAsset
	{
	public:
		OvrArchivePoseContainer();
		virtual ~OvrArchivePoseContainer();

		virtual OvrAssetType	GetType()const override { return kAssetPose; }

		bool		SetPoseNum(ovr::uint32 a_num);
		ovr::uint32	GetPoseNum()const { return pose_num; }
		OvrPose&	GetPose(ovr::uint32 a_index);

	protected:
		virtual bool	Serialize(OvrArchive& a_archive) override;

	private:
		ovr::uint32		pose_num;
		OvrPose*		poses;
	};

	class OVR_ASSET_MANAGER_API OvrArchivePoseAnimation : public OvrBinaryAsset
	{
	public:
		OvrArchivePoseAnimation();
		virtual ~OvrArchivePoseAnimation();

		virtual OvrAssetType	GetType()const override { return kAssetPoseAnimation; }

		void			SetFrameRate(ovr::uint32 a_frame_rate) { frame_rate = a_frame_rate; }
		void			SetDuration(float a_seconds) { duration = a_seconds; }
		ovr::uint32		GetFrameRate()const { return frame_rate; }
		float			GetDuration() { return duration; }

		void				AddPoseName(const OvrString& a_name) { pose_name_list.push_back(a_name); }
		ovr::uint32			GetPoseNum()const { return pose_name_list.size(); }
		const OvrString&	GetPoseName(ovr::uint32 a_index) { return pose_name_list[a_index]; }

		bool						AddFrame(OvrMorphFrame* a_frame);
		ovr::uint32					GetFrameNum()const { return frame_list.size(); }
		OvrMorphFrame*	GetFrame(ovr::uint32 a_index) { return frame_list[a_index]; }
	protected:
		virtual bool	Serialize(OvrArchive& a_archive) override;
		bool			SerializeFrame(OvrArchive& a_archive, OvrMorphFrame* a_frame);

	private:
		ovr::uint32		frame_rate = 0;
		float			duration = 0.0f;
		std::vector<OvrString>			pose_name_list;
		std::vector<OvrMorphFrame*>	frame_list;

	};
}
#endif