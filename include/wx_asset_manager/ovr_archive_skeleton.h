﻿#ifndef OVR_ASSET_SERIALIZE_OVR_SKELETON_H_
#define OVR_ASSET_SERIALIZE_OVR_SKELETON_H_

#include <wx_base/wx_matrix4f.h>
#include "ovr_archive_asset.h"

namespace ovr_asset 
{
	struct OvrArchiveJoint
	{
		OvrString	name;
		ovr::int32	parent_index;
		OvrMatrix4	bone_to_mesh_mat;	//将mesh中的vertex变换到骨骼空间
		OvrMatrix4	parent_transform;	//可以将本空间转换到父空间
		OvrMatrix4	default_pose;		//缺省的姿态

		OvrArchiveJoint()
		{
			parent_index = -1;
		}

		~OvrArchiveJoint()
		{
		}

		void	operator = (const OvrArchiveJoint& a_value) 
		{
			name				= a_value.name;
			parent_index		= a_value.parent_index;
			bone_to_mesh_mat	= a_value.bone_to_mesh_mat;
			parent_transform	= a_value.parent_transform;
			default_pose		= a_value.default_pose;
		}

		bool Serialize(OvrArchive& a_serialize);
	};

	class OVR_ASSET_MANAGER_API OvrArchiveSkeleton : public OvrBinaryAsset
	{
	public:
		OvrArchiveSkeleton();
		virtual ~OvrArchiveSkeleton();

		virtual OvrAssetType	GetType()const override { return kAssetSkeleton; }

		bool		SetJointNum(ovr::uint32 a_num);
		ovr::uint32	GetJointNum()const { return joint_num; }
		OvrArchiveJoint&	GetJoint(ovr::uint32 a_index);

	protected:
		virtual bool	Serialize(OvrArchive& a_archive) override;

	private:
		ovr::uint32		joint_num;
		OvrArchiveJoint*		joints;
	};
}

#endif
