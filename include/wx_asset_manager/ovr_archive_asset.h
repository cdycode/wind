﻿#ifndef OVR_ASSET_MANAGER_OVR_ARCHIVE_ASSET_H_
#define OVR_ASSET_MANAGER_OVR_ARCHIVE_ASSET_H_

#ifdef _MSC_VER
#pragma warning(disable : 4251)
#endif

#ifdef WXASSETMANAGER_EXPORTS
#       ifdef _MSC_VER
#		    define OVR_ASSET_MANAGER_API __declspec(dllexport)
#       else
#           define OVR_ASSET_MANAGER_API
#       endif
#	else
#       ifdef _MSC_VER
#		    define OVR_ASSET_MANAGER_API __declspec(dllimport)
#       else
#           define OVR_ASSET_MANAGER_API
#       endif
#endif // OVRASSETMANAGER_EXPORTS

#include <wx_base/wx_archive.h>

namespace ovr_asset 
{
	enum OvrAssetType
	{
		kAssetInvalid = -1,
		kAssetTexture,
		kAssetMaterial,
		kAssetMesh,
		kAssetSkeleton,
		kAssetSkeletalAnimation,
		kAssetPose,
		kAssetPoseAnimation,
	};

	class OVR_ASSET_MANAGER_API OvrArchiveAsset
	{
		friend class OvrArchiveAssetManager;
	public:

		//获取类型
		virtual OvrAssetType	GetType()const = 0;

		//保存数据到磁盘中
		virtual bool			Save() = 0;

		//设置显示名
		void				SetDisplayName(const OvrString& a_name) { display_name = a_name; }
		const OvrString&	GetDisplayName()const { return display_name; }

		//获得资源路径(相对路径)
		void				SetFilePath(const OvrString& a_file_path) { file_path = a_file_path; }
		const OvrString&	GetFilePath()const { return file_path; }

	protected:
		OvrArchiveAsset() {}
		virtual ~OvrArchiveAsset() {}

		//从磁盘中加载数据
		virtual bool		Load() = 0;

	protected:
		ovr::uint16		version_major = 0;	//save时  在子类中赋值
		ovr::uint16		version_minor = 0;	//save时  在子类中赋值
		OvrString		display_name;		//资源的显示名字
		OvrString		file_path;			//文件路径 相对路径
	};

	//序列化成二进制的文件
	class OVR_ASSET_MANAGER_API OvrBinaryAsset : public OvrArchiveAsset
	{
	protected:
		OvrBinaryAsset() {}
		virtual ~OvrBinaryAsset() {}

	public:
		//保存数据到磁盘中
		virtual bool		Save() override;

	protected:
		//从磁盘中加载数据
		virtual bool		Load() override;

		//序列化文件体
		virtual bool	Serialize(OvrArchive& a_archive) = 0;
	};
}

#endif
