﻿#ifndef OVR_ASSET_MANAGER_OVR_ARCHIVE_TEXTURE_H_
#define OVR_ASSET_MANAGER_OVR_ARCHIVE_TEXTURE_H_

#include "ovr_archive_asset.h"

namespace ovr_asset 
{
	enum OvrPixelFormat
	{
		kPixelInvalid = 0,
		kPixelAlpha,
		kPixelLuminance,
		kPixelRGB,
		kPixelRGBA,
	};

	class OVR_ASSET_MANAGER_API OvrArchiveTexture : public OvrBinaryAsset
	{
	public:
		OvrArchiveTexture();
		virtual ~OvrArchiveTexture();

		virtual OvrAssetType	GetType()const override { return kAssetTexture; };

		void				SetTextureInfo(OvrPixelFormat a_format, ovr::uint32 a_width, ovr::uint32 a_height);

		OvrPixelFormat		GetFormat()const { return (OvrPixelFormat)pixel_format; }
		ovr::uint32			GetWidth()const { return image_width; }
		ovr::uint32			GetHeight()const { return image_height; }

		//参数为原始数据 会转为ktx数据
#ifdef _MSC_VER
		bool				SetImagePixels(const char* a_buffer, ovr::uint32 a_image_size);
		bool				SetImageKtxPixels(const char* a_buffer, ovr::uint32 a_image_size);
#endif
		const ovr::uint8*	GetBuffer() { return buffer; }
		ovr::uint32			GetDataSize() { return data_size; }

		//因图片空间比较大 使用后生成相应的textureid后 需要释放内存
		void				ReleaseBuffer();

		const ovr::uint8* GetRGBABuffer();
	protected:
		virtual bool	Serialize(OvrArchive& a_archive) override;
		bool RgbaToKtxOne(unsigned char *img, int with_alpha, int width, int height, unsigned char*&dst_buf, int* dst_len, int expandedwidth, int expandedheight);

	private:
		ovr::uint32		pixel_format;
		ovr::uint32		image_width;
		ovr::uint32		image_height;

		ovr::uint32		data_size;
		ovr::uint8*		buffer;
		ovr::uint32		buffer_length;
		ovr::uint8*     rgba_buffer[6];
	};
}

#endif
