﻿#ifndef OVR_ASSET_MANAGER_OVR_ASSET_MANAGER_H_
#define OVR_ASSET_MANAGER_OVR_ASSET_MANAGER_H_

#include <map>
#include <wx_base/wx_singleton.h>
#include <wx_base/wx_string.h>
#include "ovr_archive_asset.h"

namespace ovr_asset 
{
	class OVR_ASSET_MANAGER_API OvrArchiveAssetManager : public OvrSingleton<OvrArchiveAssetManager>
	{
	public:
		static OvrAssetType			GetAssetType(const OvrString& a_file_path);

	public:
		OvrArchiveAssetManager() {}
		virtual ~OvrArchiveAssetManager() {}

	public:
		//不负责根目录的创建
		void				SetRootDir(const OvrString& a_dir);
		const OvrString&	GetRootDir()const { return root_dir;}

		//是一个相对根目录的路径
		OvrArchiveAsset*	CreateAsset(OvrAssetType a_type, const OvrString& a_file_path);

		//是一个相对根目录的路径
		OvrArchiveAsset*	LoadAsset(const OvrString& a_file_path,bool a_breload=false);
		void				DestoryAsset(OvrArchiveAsset* a_asset);
		void				DestoryAsset(const OvrString& a_file_path);
	private:
		inline OvrArchiveAsset* CreateAssetClass(OvrAssetType a_type);

	private:
		OvrString	root_dir;

		std::map<OvrString, OvrArchiveAsset*> asset_map;
	};
}

#endif
