﻿#ifndef OVR_ASSET_SERIALIZE_OVR_MATERIAL_H_
#define OVR_ASSET_SERIALIZE_OVR_MATERIAL_H_

#include <wx_base/wx_vector3.h>
#include "ovr_archive_asset.h"
#include <vector>
#include <map>
namespace ovr_asset
{
	enum OvrTextureUsage
	{
		kTextureUsageDiffuse,
		kTextureUsageEmissive,
		kTextureUsageNormal,
		kTextureUsageSpecular,
		kTextureUsageReflection,
		kTextureUsageMax
	};

	struct OvrTextureInfo
	{
		OvrString		file_path_name;
		OvrTextureUsage	usage;

		void operator = (const OvrTextureInfo& a_info);

		bool	Serialize(OvrArchive& a_serialize);
	};

	struct OvrMaterialParam
	{
		OvrVector3			ambient;    //Ka in obj.file
		OvrVector3			diffuse;    //Kd in obj.file
		OvrVector3			specular;   //Ks in obj.file
		OvrVector3			emisive;    //Ke in obj.file
		OvrVector3			bump;
		double					opacity;
		double					shininess;      //Ns in obj.file
		double					reflectivity;
		double					alpha;          //illum in obj.file

		bool	Serialize(OvrArchive& a_serialize);
	};
	struct OvrMaterialConstants_Sample2D
	{
		OvrString name;
		OvrString texture;
		OvrMaterialConstants_Sample2D(const OvrMaterialConstants_Sample2D& a_other) 
		{
			name = a_other.name;
			texture = a_other.texture;
		}
		OvrMaterialConstants_Sample2D(const OvrString& a_name, const OvrString& a_texture)
		{
			name = a_name;
			texture = a_texture;
		}

		void operator = (const OvrMaterialConstants_Sample2D& a_other) 
		{
			name = a_other.name;
			texture = a_other.texture;
		}
	};
	struct OvrMaterialConstants_Float
	{
		OvrString name;
		float value;
		OvrMaterialConstants_Float(const OvrMaterialConstants_Float& a_other)
		{
			name = a_other.name;
			value = a_other.value;
		}

		OvrMaterialConstants_Float(const OvrString& a_name, float a_value)
		{
			name = a_name;
			value = a_value;
		}
		void operator = (const OvrMaterialConstants_Float& a_other)
		{
			name = a_other.name;
			value = a_other.value;
		}
	};

	struct OvrMaterialConstants_Float4
	{
		OvrString name;
		OvrVector4 value;
		OvrMaterialConstants_Float4(const OvrMaterialConstants_Float4& a_other)
		{
			name = a_other.name;
			value = a_other.value;
		}

		OvrMaterialConstants_Float4(const OvrString& a_name, OvrVector4 a_value)
		{
			name = a_name;
			value = a_value;
		}
		void operator = (const OvrMaterialConstants_Float4& a_other)
		{
			name = a_other.name;
			value = a_other.value;
		}
	};
	
	class OVR_ASSET_MANAGER_API OvrArchiveMaterial : public OvrArchiveAsset
	{
	public:
		OvrArchiveMaterial(); //{}
		virtual ~OvrArchiveMaterial() {}

		virtual OvrAssetType	GetType()const override { return kAssetMaterial; }

		//保存数据到磁盘中
		virtual bool			Save() override;

		//Shader
		void				SetShaderName(const OvrString& a_name);
		OvrString			GetShaderName() { return shader_name; }

		//SceneBlend
		int					GetSceneBlendSrc()const { return src_str; }
		int					GetSceneBlendDest()const { return dest_str; }
		void				SetSceneBlend(int a_src, int a_dest);
		bool				IsSetSceneBlend()const { return is_set_blend; }

		//DepthWrite
		bool				IsDepthWrite() { return bdepth_write; }
		void				SetDepthWrite(bool a_bdw) { bdepth_write = a_bdw; }
		//DepthCheck
		bool				IsDepthCheck() { return bdepth_check; }
		void				SetDepthCheck(bool a_dc) { bdepth_check = a_dc; }
		//CullMode
		bool				IsCullMode() { return bcull_mode; }
		int			GetCullMode();
		void				SetCullMode(int a_cm);
		//Constant
		const std::vector<OvrMaterialConstants_Sample2D> &GetConstantSample2D()const;
		const std::vector<OvrMaterialConstants_Float>	&GetConstantFloat()const;
		const std::vector<OvrMaterialConstants_Float4>	&GetConstantVec4()const;

		bool	GetSample2DByName(const OvrString& a_name, OvrString& a_value)const;
		bool	GetFloatByName(const OvrString& a_name, float& a_value)const;
		bool	GetVector4ByName(const OvrString& a_name, OvrVector4& a_value)const;

		void SetConstantSample2D(const OvrString& a_name, const OvrString& a_value);
		void SetConstantFloat(const OvrString& a_name, float a_value);
		void SetConstantFloat4(const OvrString& a_name,const OvrVector4& a_value);
	protected:
		//从磁盘中加载数据
		virtual bool		Load() override;
	private:
		int					GetBlendModeFromString(const OvrString& a_mode);
		const OvrString&	GetBlendModeFromInt(int a_mode);
		int					GetCullModeFromString(const OvrString& a_mode);
		const OvrString&	GetCullModeFromInt(int a_mode);
	private:

		OvrString	shader_name;
		
		bool	is_set_blend = false;
		int		src_str  = 0;
		int		dest_str = 1;

		//DepthWrite
		bool	  bdepth_write = true;
		//DepthCheck
		bool	  bdepth_check = true;
		//CullMode
		bool	  bcull_mode = false;
		OvrString cull_mode;

		//Constants;
		std::vector<OvrMaterialConstants_Sample2D> constant_sample2d;
		std::vector<OvrMaterialConstants_Float>	   constants_float;
		std::vector<OvrMaterialConstants_Float4>   constants_vector4;

		static std::map<int, OvrString>			   m_blend_mode;
		static std::map<int, OvrString>			   m_cull_mode;
	};
}

#endif

