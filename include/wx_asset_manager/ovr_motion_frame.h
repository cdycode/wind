﻿#ifndef OVR_SERIALIZE_OVR_MOTION_FRAME_H_
#define OVR_SERIALIZE_OVR_MOTION_FRAME_H_

#include <wx_base/wx_matrix4f.h>
#include "wx_base/wx_string.h"
#include <vector>
#include "ovr_archive_asset.h"

namespace ovr_asset 
{
	struct OVR_ASSET_MANAGER_API OvrJointFrame
	{
		bool is_materix = true;
		ovr::uint32 joint_index;
		OvrString   joint_name;
		OvrMatrix4  joint_materix;
		OvrVector3  joint_positon;
		OvrQuaternion  joint_quaternion;
	};
	//用于输出一帧数据
	class OVR_ASSET_MANAGER_API OvrMotionFrame
	{
	public:
		OvrMotionFrame() {}
		~OvrMotionFrame() { Uinit(); }
		bool        Blend(OvrMotionFrame* a_new_frame, float a_dst_weight, OvrMotionFrame* a_src_frame, float a_src_weight);
		bool		Init(ovr::uint32 a_num);
		void		Uinit();
		ovr::uint32 GetJointNum()const;
		OvrJointFrame*	GetJointAnimationFrame(ovr::uint32 a_index);

	private:
		std::vector<OvrJointFrame> joint_frames;
	};

	struct OVR_ASSET_MANAGER_API OvrPoseFrame
	{
		ovr::uint32 pose_index;
		OvrString   pose_name;
		float       pose_weight;
	};
	//face
	class OVR_ASSET_MANAGER_API OvrMorphFrame
	{
	public:
		OvrMorphFrame() {}
		~OvrMorphFrame() { Uinit(); }

		bool		Init(ovr::uint32 a_num);
		void		Uinit();
		ovr::uint32 GetPoseNum()const;

		OvrPoseFrame*		GetPoseFrame(ovr::uint32 a_index);
	private:
		std::vector<OvrPoseFrame> pose_frames;
	};
	//face
}

#endif
