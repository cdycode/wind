#ifndef OVR_MOTION_OVR_SKELETAL_ANIMATION_SEQUENCE_H_
#define OVR_MOTION_OVR_SKELETAL_ANIMATION_SEQUENCE_H_

#include "ovr_motion.h"
#include <wx_base/wx_string.h>
namespace ovr_asset
{
	class OvrMotionFrame;
}
namespace ovr_motion 
{
	class OvrSkeletonAnimationController;
	class OVR_MOTION_API OvrSkeletalAnimationSequence 
	{
	public:
		OvrSkeletalAnimationSequence();
		~OvrSkeletalAnimationSequence();

		void	SetAnimationPath(const char* a_file) { animation_file = a_file; }
		const char* GetAnimationPath() { return animation_file.GetStringConst(); }
		bool	Build();
		void    UnBuild();
		float	GetDuration()const;

		ovr_asset::OvrMotionFrame*	GetAnimation(float a_second);
	private:

		OvrString	animation_file;
		OvrSkeletonAnimationController*		animation_controller;
	};
}

#endif
