﻿#ifndef OVR_MOTION_OVR_MOTION_H_
#define OVR_MOTION_OVR_MOTION_H_
#include "wx_base/wx_type.h"

#ifdef OVR_OS_WIN32

#ifdef WXMOTION_EXPORTS
#       ifdef _MSC_VER
#	        define OVR_MOTION_API __declspec(dllexport)
#       else
#           define OVR_MOTION_API
#       endif
#else
#       ifdef _MSC_VER
#	        define OVR_MOTION_API __declspec(dllimport)
#       else
#           define OVR_MOTION_API
#       endif
#endif

#define	OVR_MOTION_NAME "wxMotion"

//架构
#ifndef _WIN64
#	define OVR_MOTION_PLATFORM "_x86"
#else
#	define OVR_MOTION_PLATFORM "_x64"
#endif

//版本
#ifdef _DEBUG
#	define OVR_MOTION_DEBUG "_d"
#else 
#	define OVR_MOTION_DEBUG 
#endif

#ifndef WXMOTION_EXPORTS
#	pragma comment( lib, OVR_MOTION_NAME OVR_MOTION_PLATFORM OVR_MOTION_DEBUG ".lib")
#endif

#else
#define OVR_MOTION_API
#endif	//#ifdef OVR_OS_WIN32


#endif
