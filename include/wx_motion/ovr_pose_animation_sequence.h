#ifndef OVR_MOTION_OVR_POSE_ANIMATION_SEQUENCE_H_
#define OVR_MOTION_OVR_POSE_ANIMATION_SEQUENCE_H_

#include "ovr_motion.h"
#include <wx_base/wx_string.h>
namespace ovr_asset
{
    class OvrMorphFrame;
}
namespace ovr_motion 
{
	class OvrPoseAnimationController;
	class OVR_MOTION_API OvrPoseAnimationSequence 
	{
	public:
		OvrPoseAnimationSequence();
		~OvrPoseAnimationSequence();

		void	SetAnimationPath(const char* a_file) { animation_file = a_file; }
		const char* GetAnimationPath() { return animation_file.GetStringConst(); }
		bool	Build();
		void	UnBuild();
		float	GetDuration()const;

        ovr_asset::OvrMorphFrame*	GetAnimation(float a_second);
	private:

		OvrString	animation_file;
		OvrPoseAnimationController*		animation_controller;
	};
}

#endif
