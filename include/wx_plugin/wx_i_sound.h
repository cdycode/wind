﻿#ifndef WX_PLUGIN_WX_I_SOUND_H_
#define WX_PLUGIN_WX_I_SOUND_H_

#include "wx_plugin.h"

namespace wx_plugin
{
	typedef int(*FillAudioData)(void* a_param, void* a_buffer, int a_buffer_size);

	class WX_PLUGIN_API wxISound
	{
	public:
		wxISound() {}
		virtual ~wxISound() {}

		virtual void SetFillAudioDataFunc(void* a_param, FillAudioData a_function) = 0;

		virtual bool Play() = 0;
		virtual void Pause() = 0;
		virtual bool FeedSoundData(void* a_data, int a_data_size) = 0;
		virtual void SetSoundPos(float x, float y, float z) = 0;
		virtual void SetVolume(float a_vol) = 0;
		virtual bool IsPlaying() = 0;
	};

	class WX_PLUGIN_API wxISoundManager : public wxIPlugin
	{
	public:
		static wxISoundManager*	Load();
		static void					Unload();

	public:
		virtual void	Update() = 0;

		virtual wxISound*	CreateSound(int	a_channel_num , int a_sample_rate, int a_sample_bits, int a_frame_ms, bool b3D = false) = 0;
		virtual void		DestorySound(wxISound*& a_sound) = 0;
		virtual void		Set3DListener(float x, float y, float z) = 0;
		virtual bool		PlayMusic(const char* fileName, bool bLoop,float a_vol = 1) = 0;
		virtual void		StopAllMusic() = 0;
	protected:
		wxISoundManager() {}
		virtual ~wxISoundManager() {}

	};

	
}

#endif

