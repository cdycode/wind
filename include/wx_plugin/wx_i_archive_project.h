#ifndef WX_PLUGIN_I_OVR_ARCHIVE_PROJECT_H_
#define WX_PLUGIN_I_OVR_ARCHIVE_PROJECT_H_

#include "wx_plugin.h"

namespace ovr_core 
{
	class OvrSceneContext;
	class OvrArchiveMedia;
	class OVRCaptureManager;
}

namespace ovr_project 
{
	class OvrProject;
	class OvrTimelineEditor;
	class OvrSceneEditor;
}

namespace wx_plugin 
{
	class WX_PLUGIN_API wxIArchiveProject : public wxIPlugin
	{
	public:
		static wxIArchiveProject*	Load();
		static void					Unload();

	public:
		wxIArchiveProject() {}
		virtual ~wxIArchiveProject() {}

		virtual bool		CreateProject(const OvrString& a_file_full_path) = 0;
		virtual bool		SaveProject(ovr_project::OvrProject* a_project, const OvrString& a_file_path) = 0;
		virtual bool		LoadProject(ovr_project::OvrProject* a_project, const OvrString& a_file_path) = 0;

		virtual bool						CreateScene(const OvrString& a_full_path, const OvrString& a_unique_name) = 0;
		virtual bool						SaveScene(ovr_project::OvrSceneEditor* a_scene) = 0;
		virtual ovr_core::OvrSceneContext*	LoadScene(const OvrString& a_scene_file_path) = 0;
		virtual bool						AddSequenceToScene(const OvrString& a_scene_full_path, const OvrString& a_sequence_unique_name) = 0;

		virtual bool	CreateSequence(const OvrString& a_full_path, const OvrString& a_scene_unique_name, const OvrString& a_unique_name) = 0;
		virtual bool	SaveSequence(ovr_project::OvrTimelineEditor* a_sequence) = 0;
		virtual bool	LoadSequence(ovr_project::OvrTimelineEditor* a_sequence) = 0;
	};
}

#endif
