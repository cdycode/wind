﻿#ifndef WX_PLUGIN_WX_PLUGIN_MANAGER_H_
#define WX_PLUGIN_WX_PLUGIN_MANAGER_H_

#include <map>
#include "wx_plugin.h"

class OvrDllLoader;
namespace wx_plugin
{
	class wxIPlugin;
	class wxPluginInfo
	{
	public:
		wxPluginInfo() {}
		wxPluginInfo(const wxPluginInfo& a_other) 
		{
			dll_loader = a_other.dll_loader;
			plugin_ins = a_other.plugin_ins;
		}

		wxPluginInfo(OvrDllLoader* a_loader, wxIPlugin* a_ins) 
		{
			dll_loader = a_loader;
			plugin_ins = a_ins;
		}

		void operator = (const wxPluginInfo& a_other) 
		{
			dll_loader = a_other.dll_loader;
			plugin_ins = a_other.plugin_ins;
		}

		OvrDllLoader*	dll_loader = nullptr;
		wxIPlugin*		plugin_ins = nullptr;
	};

	class wxIPlugin;
	class WX_PLUGIN_API wxPluginManager
	{
	public:
		static wxPluginManager*	GetIns();
		static void					DelIns();
	protected:
		wxPluginManager();
		~wxPluginManager();

	public:
		void		LoadAll();
		void		UnloadAll();

		wxIPlugin*	Load(const OvrString& a_plugin_name);
		void		Unload(const OvrString& a_plugin_name);

	private:
		wxIPlugin*	LoadDll(const OvrString& a_plugin_name);
		void		UnloadDll(wxPluginInfo& a_plugin_info);
	private:
		std::map<OvrString, wxPluginInfo>	dll_loader_map;
	};
}

#endif
