﻿#ifndef WX_PLUGIN_WX_I_AV_MEDIA_H_
#define WX_PLUGIN_WX_I_AV_MEDIA_H_

#include <new>
#include "wx_plugin.h"

namespace wx_plugin 
{
	class wxFrameBuffer
	{
	public:
		wxFrameBuffer(void)
			:buffer(nullptr)
			, buffer_size(0)
			, data_size(0)
			, pts(0)
		{}
		~wxFrameBuffer(void)
		{
			Close();
		}

		bool Open(int a_buffer_size)
		{
			if (nullptr != buffer)
			{
				return false;
			}

			buffer = new(std::nothrow) char[a_buffer_size];
			if (nullptr != buffer)
			{
				buffer_size = a_buffer_size;
				data_size = 0;
				pts = 0;
			}

			return (nullptr != buffer);
		}
		void Close(void)
		{
			if (nullptr != buffer)
			{
				delete[]buffer;
				buffer = nullptr;
				data_size = 0;
				buffer_size = 0;
				pts = 0;
			}
		}

		void Reset(void)
		{
			data_size = 0;
			pts = 0;
		}

		int PutData(const char* a_data, int a_data_size)
		{
			int iLeftSize = buffer_size - data_size;
			int iCopySize = iLeftSize > a_data_size ? a_data_size : iLeftSize;

			memcpy(buffer + data_size, a_data, iCopySize);
			data_size += iCopySize;

			return iCopySize;
		}

		char*		buffer = nullptr;
		int			buffer_size = 0;
		int			data_size = 0;
		long long	pts = 0;
	};

	//帧率为m_iNumerator/m_iDenominator
	typedef struct STRU_VIDEO_FRAME_RATE
	{
		int		numerator = 0;			//分子
		int		denominator = 1;			//分母
	}StruVideoFrameRate;


	//象素排列方式
	enum ENU_VIDEO_PIXEL_FORMAT
	{
		kPixelUnknown = 0,
		kPixelYUV420P,		//先存储Y  然后U 最后V
		kPixelYUV422P,		//先存储Y  然后U 最后V
		kPixelUYVY422,		//UYVY
		kPixelYUYV422,		//YUYV
		kPixelRGB24 = 31,	//RGBRGB顺序存放
		kPixelBGR24,		//BGRBGR顺序存放
		kPixelARGB32,		//ARGBARGB顺序存放
		kPixelBGRA32,		//BGRABGRA顺序存放
		kPixelRGBA32,		//RGBARGBA顺序存放
		kPixelRGB565LE,		//压缩格式的RGB数据 高位5bit为R 中间6bit为G 最后5bit为B 小端
		kPixelRGB565BE,		//压缩格式的RGB数据 高位5bit为R 中间6bit为G 最后5bit为B 大端

		kPixelNum,
	};

	typedef struct STRU_VIDEO_STREAM_INFO
	{
		int							frame_width = 0;		//视频宽
		int							frame_height = 0;		//视频高
		enum ENU_VIDEO_PIXEL_FORMAT	pixel_format = kPixelUnknown;		//像素排列方式
		StruVideoFrameRate			frame_rate;								//视频的帧率
		long long					length = 0;								//播放时长  单位为毫秒

	}StruVideoStreamInfo;

	typedef struct STRU_AUDIO_STREAM_IINFO
	{
		int			channel_num = 0;			//声道数
		int			sample_rate = 0;			//音频采样频率
		int			sample_bits = 0;			//音频采样位数
		long long	length = 0;					//播放时长 单位为毫秒
	}StruAudioStreamInfo;

	//音视频数据
	typedef struct STRU_RAW_FRAME
	{
		unsigned char*	buffer = nullptr;	//存放数据的Buffer
		int				buffer_size = 0;	//Buffer的尺寸
		int				data_size = 0;		//数据长度
		long long		pts = 0;			//单位为毫秒
	}StruRawFrame;

	class WX_PLUGIN_API wxIMediaInfo
	{
	public:
		wxIMediaInfo() {}
		virtual ~wxIMediaInfo() {}

		virtual bool	Open(const char* a_file_path) = 0;

		virtual bool	IsOwnVideo()const = 0;
		virtual bool	IsOwnAudio()const = 0;

		virtual const StruVideoStreamInfo&	GetVideoInfo()const = 0;
		virtual const StruAudioStreamInfo&	GetAudioInfo()const = 0;
	};

	class WX_PLUGIN_API wxIVideoDecoder
	{
	public:
		wxIVideoDecoder() {}
		virtual ~wxIVideoDecoder() {}

		virtual bool Open(const char* a_file_path) = 0;
		virtual void Close() = 0;

		virtual bool GetFileVideoInfo(StruVideoStreamInfo& a_video_info) = 0;
		virtual bool SetVideoOutFormat(int a_width, int a_height, ENU_VIDEO_PIXEL_FORMAT a_pixel_format) = 0;

		//单位是毫秒
		virtual bool StartDecoding() = 0;
		virtual bool Seek(long long a_new_pos) = 0;
		virtual bool Resume() = 0;

		virtual wxFrameBuffer* PopFrame() = 0;
		virtual void			PushFrame(wxFrameBuffer* a_frame_buffer) = 0;

	};

	class WX_PLUGIN_API IOvrAudioDecoder
	{
	public:
		IOvrAudioDecoder() {}
		virtual ~IOvrAudioDecoder() {}

		virtual bool Open(const char* a_file_path) = 0;
		virtual void Close() = 0;

		virtual bool GetFileAudioInfo(StruAudioStreamInfo& a_video_info) = 0;
		virtual bool SetAudioOutFormat(int a_channels, int a_sample_bits, int a_sample_rate, int a_frame_ms) = 0;

		virtual bool StartDecoding() = 0;
		virtual bool Seek(long long a_new_pos) = 0;
		virtual bool Resume() = 0;

		virtual wxFrameBuffer* PopFrame() = 0;
		virtual void			PushFrame(wxFrameBuffer* a_frame_buffer) = 0;
	};

	class WX_PLUGIN_API IOvrAVEncoder
	{
	public:
		IOvrAVEncoder() {}
		virtual ~IOvrAVEncoder() {}
	
		virtual bool	Open(const OvrString& a_out_file_path) = 0;
		virtual void	Close() = 0;

		virtual void	SetVideoFormat(ENU_VIDEO_PIXEL_FORMAT a_pixel_format, ovr::uint32 a_frame_width, ovr::uint32 a_frame_height, ovr::uint32 a_frame_rate) = 0;
		virtual void	SetAudioFormat(ovr::uint32 a_channel_num, ovr::uint32 a_sample_rate, ovr::uint32 a_sample_bits) = 0;

		virtual bool	StartEncoding() = 0;
		virtual void	EndEncoding() = 0;

		virtual bool	PushVideoFrame(const char* a_raw_data, ovr::uint32 a_data_size) = 0;
		virtual bool	PushAudioFrame(const char* a_raw_data, ovr::uint32 a_data_size) = 0;
	};

	class WX_PLUGIN_API IOvrAVMedia : public wxIPlugin
	{
	public:
		static IOvrAVMedia* Load();
		static void			Unload();

	protected:
		IOvrAVMedia() {}
		virtual ~IOvrAVMedia() {}

	public:
		virtual wxIVideoDecoder*	CreateVideoDecoder() = 0;
		virtual void				DestoryVideoDecoder(wxIVideoDecoder*& a_video_decoder) = 0;
		virtual IOvrAudioDecoder*	CreateAudioDecoder()  =0;
		virtual void				DestoryAudioDecoder(IOvrAudioDecoder*& a_audio_decoder) = 0;

		virtual wxIMediaInfo*		CreateMediaInfo() = 0;
		virtual void				DestoryMediaInfo(wxIMediaInfo*& a_info) = 0;

		virtual IOvrAVEncoder*		CreateAVEncoder() = 0;
		virtual void				DestoryAVEncoder(IOvrAVEncoder*& a_encoder) = 0;
	};
}

#endif

