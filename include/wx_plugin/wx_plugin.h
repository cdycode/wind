﻿#ifndef WX_PLUGIN_WX_PLUGIN_H_
#define WX_PLUGIN_WX_PLUGIN_H_

#ifdef _WIN32
#	ifdef WXPLUGIN_EXPORTS
#		define WX_PLUGIN_API __declspec(dllexport)
#	else
#		define WX_PLUGIN_API __declspec(dllimport)
#	endif // OVR_PLUGIN_EXPORTS

//库名字
#	define	WX_PLUGIN_NAME "wxPlugin"
//架构
#	ifndef _WIN64
#		define WX_PLUGIN_PLATFORM "_x86"
#	else
#		define WX_PLUGIN_PLATFORM "_x64"
#	endif

//版本
#	ifdef _DEBUG
#		define WX_PLUGIN_DEBUG "_d"
#	else 
#		define WX_PLUGIN_DEBUG ""
#	endif

#	ifndef WXPLUGIN_EXPORTS
#		pragma comment( lib, WX_PLUGIN_NAME WX_PLUGIN_PLATFORM WX_PLUGIN_DEBUG ".lib")
#	endif

#else
#	define WX_PLUGIN_API 
#endif // _WIN32

#include <wx_base/wx_string.h>

namespace wx_plugin
{
	class WX_PLUGIN_API wxIPlugin
	{
	protected:
		wxIPlugin()
			:display_name("unknown")
		{ }

		virtual ~wxIPlugin() {}

	public:
		virtual	bool		Init() { return true; }
		virtual void		Uninit() {}

		const OvrString&	GetDisplayName()const {return display_name;}

	protected:
		OvrString	display_name;
	};
}

#endif
