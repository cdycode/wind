#ifndef OVR_BASE_OVR_COLOR_VALUE_H_
#define OVR_BASE_OVR_COLOR_VALUE_H_
#include "wx_type.h"
#include "wx_base.h"
class WX_BASE_API OvrColorValue
{
public:
	static const OvrColorValue ZERO;
	static const OvrColorValue Black;
	static const OvrColorValue White;
	static const OvrColorValue Red;
	static const OvrColorValue Green;
	static const OvrColorValue Blue;

	explicit OvrColorValue(float a_red = 1.0f,
		float a_green = 1.0f,
		float a_blue = 1.0f,
		float a_alpha = 1.0f) : r(a_red), g(a_green), b(a_blue), a(a_alpha)
	{ }

	bool operator==(const OvrColorValue& rhs) const;
	bool operator!=(const OvrColorValue& rhs) const;

	float r, g, b, a;

	void saturate()
	{
		if (r < 0)
			r = 0;
		else if (r > 1)
			r = 1;

		if (g < 0)
			g = 0;
		else if (g > 1)
			g = 1;

		if (b < 0)
			b = 0;
		else if (b > 1)
			b = 1;

		if (a < 0)
			a = 0;
		else if (a > 1)
			a = 1;
	}

	inline float operator [] (const size_t i) const
	{
		OVR_ASSERT(i < 4);

		return *(&r + i);
	}

	inline float& operator [] (const size_t i)
	{
		OVR_ASSERT(i < 4);

		return *(&r + i);
	}

	inline float* ptr()
	{
		return &r;
	}

	inline const float* ptr() const
	{
		return &r;
	}

	inline OvrColorValue operator + (const OvrColorValue& rkVector) const
	{
		OvrColorValue kSum;

		kSum.r = r + rkVector.r;
		kSum.g = g + rkVector.g;
		kSum.b = b + rkVector.b;
		kSum.a = a + rkVector.a;

		return kSum;
	}

	inline OvrColorValue operator - (const OvrColorValue& rkVector) const
	{
		OvrColorValue kDiff;

		kDiff.r = r - rkVector.r;
		kDiff.g = g - rkVector.g;
		kDiff.b = b - rkVector.b;
		kDiff.a = a - rkVector.a;

		return kDiff;
	}

	inline OvrColorValue operator * (const float fScalar) const
	{
		OvrColorValue kProd;

		kProd.r = fScalar*r;
		kProd.g = fScalar*g;
		kProd.b = fScalar*b;
		kProd.a = fScalar*a;

		return kProd;
	}

	inline OvrColorValue operator * (const OvrColorValue& rhs) const
	{
		OvrColorValue kProd;

		kProd.r = rhs.r * r;
		kProd.g = rhs.g * g;
		kProd.b = rhs.b * b;
		kProd.a = rhs.a * a;

		return kProd;
	}

	inline OvrColorValue operator / (const OvrColorValue& rhs) const
	{
		OvrColorValue kProd;

		kProd.r = r / rhs.r;
		kProd.g = g / rhs.g;
		kProd.b = b / rhs.b;
		kProd.a = a / rhs.a;

		return kProd;
	}

	inline OvrColorValue operator / (const float fScalar) const
	{
		OVR_ASSERT(fScalar != 0.0);

		OvrColorValue kDiv;

		float fInv = 1.0f / fScalar;
		kDiv.r = r * fInv;
		kDiv.g = g * fInv;
		kDiv.b = b * fInv;
		kDiv.a = a * fInv;

		return kDiv;
	}

	inline friend OvrColorValue operator * (const float fScalar, const OvrColorValue& rkVector)
	{
		OvrColorValue kProd;

		kProd.r = fScalar * rkVector.r;
		kProd.g = fScalar * rkVector.g;
		kProd.b = fScalar * rkVector.b;
		kProd.a = fScalar * rkVector.a;

		return kProd;
	}

	// arithmetic updates
	inline OvrColorValue& operator += (const OvrColorValue& rkVector)
	{
		r += rkVector.r;
		g += rkVector.g;
		b += rkVector.b;
		a += rkVector.a;

		return *this;
	}

	inline OvrColorValue& operator -= (const OvrColorValue& rkVector)
	{
		r -= rkVector.r;
		g -= rkVector.g;
		b -= rkVector.b;
		a -= rkVector.a;

		return *this;
	}

	inline OvrColorValue& operator *= (const float fScalar)
	{
		r *= fScalar;
		g *= fScalar;
		b *= fScalar;
		a *= fScalar;
		return *this;
	}

	inline OvrColorValue& operator /= (const float fScalar)
	{
		OVR_ASSERT(fScalar != 0.0);

		float fInv = 1.0f / fScalar;

		r *= fInv;
		g *= fInv;
		b *= fInv;
		a *= fInv;

		return *this;
	}
};
#endif