﻿#ifndef OVR_BASE_OVR_LOG_H_
#define OVR_BASE_OVR_LOG_H_

#include "wx_base.h"

class WX_BASE_API OvrLog
{
public:
	enum Level
	{
		kDebug = 0,
		kInfo,
		kError,
	};
public:
	
public:
	static void Format(int a_level, const char* a_tag, const char * pFmt, ...);

public:
	OvrLog() {}
	~OvrLog() {}

};


#define OvrLogD(tag, ...) OvrLog::Format(OvrLog::kDebug, tag, __VA_ARGS__)
#define OvrLogI(tag, ...) OvrLog::Format(OvrLog::kInfo, tag, __VA_ARGS__)
#define OvrLogE(tag, ...) OvrLog::Format(OvrLog::kError, tag, __VA_ARGS__)
#endif
