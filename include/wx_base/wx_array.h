﻿#ifndef OVR_BASE_OVR_ARRAY_H_
#define OVR_BASE_OVR_ARRAY_H_

#include "wx_base.h"
#include <memory.h>
#include <string.h>

template<class ArrayType, ovr::uint32 array_size >
class OvrArrayTmpl
{
public:
	OvrArrayTmpl(void)
	{
		memset(array, 0, sizeof(ArrayType) * array_size);
	}
	~OvrArrayTmpl(void)
	{
	}

	void operator = ( const OvrArrayTmpl& a_array)
	{
		for (ovr::uint32 i = 0; i < array_size; i++)
		{
			array[i] = a_array.array[i];
		}
	}

	bool operator == (const OvrArrayTmpl& a_array)const
	{
		bool is_equal = true;
		for (ovr::uint32 i = 0; i < array_size; i++)
		{
			is_equal = (is_equal && array[i] == a_array.array[i]);
		}
		return is_equal;
	}

	ArrayType& operator [](ovr::uint32 a_index)
	{
		return array[a_index];
	}

private:
	ArrayType	array[array_size];

};//class String

class WX_BASE_API OvrArray2f : public OvrArrayTmpl<float, 2>
{
public:
	OvrArray2f() {}
	~OvrArray2f() {}
};

class WX_BASE_API OvrArray3f : public OvrArrayTmpl<float, 3>
{
public:
	OvrArray3f() {}
	~OvrArray3f() {}
};

class WX_BASE_API OvrArray4f : public OvrArrayTmpl<float, 4>
{
public:
	OvrArray4f() {}
	~OvrArray4f() {}
};

class WX_BASE_API OvrArray2i : public OvrArrayTmpl<ovr::int32, 2>
{
public:
	OvrArray2i() {}
	~OvrArray2i() {}
};

class WX_BASE_API OvrArray3i : public OvrArrayTmpl<ovr::int32, 3>
{
public:
	OvrArray3i() {}
	~OvrArray3i() {}
};

class WX_BASE_API OvrArray4i : public OvrArrayTmpl<ovr::int32, 4>
{
public:
	OvrArray4i() {}
	~OvrArray4i() {}
};

class WX_BASE_API OvrArray4u8 : public OvrArrayTmpl<ovr::uint8, 4>
{
public:
	OvrArray4u8() {}
	~OvrArray4u8() {}
};

#endif
