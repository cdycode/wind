﻿#ifndef OVR_BASE_OVR_STCTIC_SERIALIZE_H
#define OVR_BASE_OVR_STCTIC_SERIALIZE_H

#include "wx_base.h"
#include "wx_type.h"
#include "wx_string.h"
#include "wx_transform.h"
#include "wx_io_interface.h"
#include "wx_vector3.h"

class WX_BASE_API OvrStaticSerialize
{
	public:
		static ovr::uint32 WriteUint8 ( const ovr::uint8 src, ovr::uint8* des );
		static ovr::uint32 WriteUint32 ( const ovr::uint32 src, ovr::uint8* des, bool aIsLittle = true );
		static ovr::uint32 WriteUint16 ( const ovr::uint16 src, ovr::uint8* des, bool aIsLittle = true );
		static ovr::uint32 WriteFloat ( const float src, ovr::uint8* des, bool aIsLittle = true );
		static ovr::uint32 WriteUint64 ( const ovr::uint64 src, ovr::uint8* des, bool aIsLittle = true );
		static ovr::uint32 WriteDouble ( const double src, ovr::uint8* des, bool aIsLittle = true );

		static ovr::uint32 ReadUint8 ( ovr::uint8* ret, const ovr::uint8* src );
		static ovr::uint32 ReadUint32 ( ovr::uint32* ret, const ovr::uint8* src, bool aIsLittle = true );
		static ovr::uint32 ReadUint16 ( ovr::uint16* ret, const ovr::uint8* src, bool aIsLittle = true );
		static ovr::uint32 ReadFloat ( float* ret, const ovr::uint8* src, bool aIsLittle = true );
		static ovr::uint32 ReadUint64 ( ovr::uint64* ret, const ovr::uint8* src, bool aIsLittle = true );
		static ovr::uint32 ReadDouble ( double* ret, const ovr::uint8* src, bool aIsLittle = true );

	public:
		static ovr::uint32 WriteData( ovr::uint8* data, ovr::uint32 dataLen, OvrIOInerface* handle );
		static ovr::uint32 WriteUint8 ( const ovr::uint8 src, OvrIOInerface* handle );
		static ovr::uint32 WriteUint32 ( const ovr::uint32 src, OvrIOInerface* handle, bool aIsLittle = true );
		static ovr::uint32 WriteUint16 ( const ovr::uint16 src, OvrIOInerface* handle, bool aIsLittle = true );
		static ovr::uint32 WriteFloat ( const float src, OvrIOInerface* handle, bool aIsLittle = true );
		static ovr::uint32 WriteUint64 ( const ovr::uint64 src, OvrIOInerface* handle, bool aIsLittle = true );
		static ovr::uint32 WriteDouble ( const double src, OvrIOInerface* handle, bool aIsLittle = true );
		static ovr::uint32 WriteVarChar (OvrString src, OvrIOInerface* handle, bool aIsLittle = true );
		static ovr::uint32 WriteVector3( OvrVector3 src, OvrIOInerface* handle, bool aIsLittle = true );
		static ovr::uint32 WriteTransform( OvrTransform src, OvrIOInerface* handle, bool aIsLittle = true );

		static ovr::uint32 ReadData( ovr::uint8* data, ovr::uint32 dataLen, OvrIOInerface* handle );
		static ovr::uint32 ReadUint8 ( ovr::uint8* ret, OvrIOInerface* handle );
		static ovr::uint32 ReadUint32 ( ovr::uint32* ret, OvrIOInerface* handle, bool aIsLittle = true );
		static ovr::uint32 ReadUint16 ( ovr::uint16* ret, OvrIOInerface* handle, bool aIsLittle = true );
		static ovr::uint32 ReadFloat ( float* ret, OvrIOInerface* handle, bool aIsLittle = true );
		static ovr::uint32 ReadUint64 ( ovr::uint64* ret, OvrIOInerface* handle, bool aIsLittle = true );
		static ovr::uint32 ReadDouble ( double* ret, OvrIOInerface* handle, bool aIsLittle = true );
		static ovr::uint32 ReadVarChar (OvrString* ret, OvrIOInerface* handle, bool aIsLittle = true );
		static ovr::uint32 ReadVector3( OvrVector3* src, OvrIOInerface* handle, bool aIsLittle = true );
		static ovr::uint32 ReadTransform( OvrTransform* src, OvrIOInerface* handle, bool aIsLittle = true );

	private:
		OvrStaticSerialize()
		{
		}

		~OvrStaticSerialize()
		{
		}

		OvrStaticSerialize( const OvrStaticSerialize& )
		{
		}

		OvrStaticSerialize& operator= ( const OvrStaticSerialize& )
		{
			return *this;
		}
};

#endif // VARYBUFFER_H
