﻿#ifndef BASE_CLASS_OVR_TIME_H_
#define BASE_CLASS_OVR_TIME_H_

#include "wx_base.h"

class WX_BASE_API OvrTime
{
public:

	static double		GetSeconds();
	static ovr::uint64	GetTicksNanos();
	static ovr::uint32	GetTicksMs();

	//单位为毫秒
	static void			SleepMs(ovr::uint32 a_sleep_ms);
};

#endif
