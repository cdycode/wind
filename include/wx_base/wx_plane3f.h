﻿#ifndef OVR_BASE_OVR_PLANE3F_H_
#define OVR_BASE_OVR_PLANE3F_H_

#include "wx_base.h"
#include "wx_vector3.h"

class WX_BASE_API OvrPlane3f
{
public:
	OvrPlane3f();
	OvrPlane3f(const OvrVector3 a_normal, float a_dist_to_origin);
	~OvrPlane3f() {}
public:
	void	SetNormal(const OvrVector3& a_normal);
	void	SetDistanceToOrigin(float a_v);

	const OvrVector3&	GetNormal()const;
	float				GetDistanceToOrigin()const;
	
	float	DistanceToPoint(const OvrVector3& a_point) const;
	bool	ProjectVector(OvrVector3& ao_dst_vector, const OvrVector3& a_src_vector)const;
	bool	Intersect(float& ao_dist, const OvrVector3 a_ray_origin, const OvrVector3 a_ray_dir)const;
private:
	union
	{
		struct
		{
			OvrVector3	_normal;
			float				_distance_to_origin;
		};
		float		_m[4];
	};

};

#endif
