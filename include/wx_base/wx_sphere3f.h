#ifndef OVR_BASE_OVR_SPHERE_H_
#define OVR_BASE_OVR_SPHERE_H_

#include "wx_base.h"
#include "wx_vector3.h"

class WX_BASE_API OvrSphere3f
{
public:
	OvrSphere3f();
	OvrSphere3f(float a_r);
	OvrSphere3f(const OvrVector3& a_pos, float a_r);
	~OvrSphere3f(){}
public:
	OvrVector3 GetPos()const;
	float GetRadius()const;
	void SetPos(const OvrVector3& a_pos);
	void SetRadius(float a_r);

	int Intersect(float & ao_dist,const OvrVector3& a_ray_origin, const OvrVector3& a_ray_dir);
private:
	OvrVector3   _pos;
	float        _radius;
};

#endif
