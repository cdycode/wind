﻿#ifndef OVR_BASECLASS_OVR_BASE_H_
#define OVR_BASECLASS_OVR_BASE_H_

#include "wx_type.h"

#ifdef OVR_OS_WIN32

#ifdef _MSC_VER
#pragma warning(disable : 4251)
#endif

#ifdef WXBASE_STATIC
#	define  WX_BASE_API 
#	define	WX_BASE_NAME "wxBaseStatic"
#else
#	ifdef WXBASE_EXPORTS
#       ifdef _MSC_VER
#		    define WX_BASE_API __declspec(dllexport)
#       else
#           define WX_BASE_API
#       endif
#	else
#       ifdef _MSC_VER
#		    define WX_BASE_API __declspec(dllimport)
#       else
#           define WX_BASE_API
#       endif
#	endif
#	define	WX_BASE_NAME "wxBase"
#endif

//架构
#ifndef _WIN64
#	define WX_BASE_PLATFORM "_x86"
#else
#	define WX_BASE_PLATFORM "_x64"
#endif

#ifdef _MSC_VER
//版本
#ifdef _DEBUG
#	define WX_BASE_DEBUG "_d"
#else 
#	define WX_BASE_DEBUG 
#endif

#ifndef WXBASE_EXPORTS_SELF
#	pragma comment( lib, WX_BASE_NAME WX_BASE_PLATFORM WX_BASE_DEBUG ".lib")
#endif
#endif

#else

#define WX_BASE_API 

#endif	//#ifdef OVR_OS_WIN32

#endif
