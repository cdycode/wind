﻿#ifndef OVR_BASE_OVR_VECTOR3_H_
#define OVR_BASE_OVR_VECTOR3_H_

#include "wx_base.h"

class WX_BASE_API OvrVector3
{
public:
	OvrVector3();
	explicit OvrVector3(float a_v);
	OvrVector3(float a_x, float a_y, float a_z);
	~OvrVector3();
public:
	float& operator[](int a_id);
	const float& operator[](int a_id) const;

	OvrVector3 operator +(const OvrVector3& a_v3) const;
	OvrVector3 operator -(const OvrVector3& a_v3) const;
	OvrVector3 operator -() const;
	float operator *(const OvrVector3& a_v3) const;
	OvrVector3 operator *(float a_scale) const;
	OvrVector3 operator /(float a_scale) const;

	void operator +=(const OvrVector3& a_v3);
	void operator -=(const OvrVector3& a_v3);
	void operator *=(float a_scale);
	void operator /=(float a_scale);
	bool operator == (const OvrVector3& a_v3)const;

	bool operator <(const OvrVector3& a_v2)const;
public:

	float length()const;
	float length_sq()const;
	float normalize();
	OvrVector3 get_normalize()const;
	OvrVector3 cross(const OvrVector3& a_v3) const;
	OvrVector3 multi(const OvrVector3& a_v3) const;

	//从a_v3到自身的插值  0 为a_v3  1为自身
	OvrVector3 lerp(const OvrVector3& a_v3,float a_pos)const;
	bool	is_nan( ) const;
public:
	union
	{
		struct{
			float _x,_y,_z;
		};
		float _m[3];
	};

	static const OvrVector3 ZERO;
	static const OvrVector3 UNIT_X;
	static const OvrVector3 UNIT_Y;
	static const OvrVector3 UNIT_Z;
	static const OvrVector3 UNIT_SCALE;
};

#endif
