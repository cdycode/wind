﻿#ifndef OVR_BASE_OVR_BOX3F_H_
#define OVR_BASE_OVR_BOX3F_H_

#include "wx_base.h"
#include "wx_vector3.h"
#include "wx_ray3f.h"
#include "wx_matrix4f.h"
enum OvrIntersectStatus
{
	kIntersectLeave,
	kIntersectPart,
	kIntersectInside
};

class WX_BASE_API OvrBox3f
{
public:
	OvrBox3f();
	OvrBox3f(const OvrVector3& a_min,const OvrVector3& a_max);
	OvrBox3f(const float a_min_x,const float a_min_y,const float a_min_z,
			const float a_max_x,const float a_max_y,const float a_max_z);
	~OvrBox3f(){}

public:
	OvrBox3f operator*(const float a_s)const;
	void operator*=(const float a_s);

public:
	OvrVector3 get_size() const;
	OvrVector3 get_center() const;
	const OvrVector3& get_min()const;
	const OvrVector3& get_max()const;

	OvrVector3& get_min();
	OvrVector3& get_max();

	void combine(const OvrBox3f& a_b);
	void translate(const OvrVector3& a_mov);
	bool contains(const OvrVector3& a_v3)const;
	void expand(const OvrVector3& a_v3);
	int	 ray_interset(const OvrRay3f& a_ray,float& ao_dist)const;
	void transform_affine(const OvrMatrix4& a_matrix);
	OvrIntersectStatus intersect(const OvrBox3f& a_b)const;
private:
	OvrVector3	_min;
	OvrVector3	_max;
};


#endif
