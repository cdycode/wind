﻿#ifndef OVR_BASE_OVR_SMALL_FILE_H_
#define OVR_BASE_OVR_SMALL_FILE_H_

#include <stdio.h>
#include "wx_base.h"
#include "wx_string.h"

class WX_BASE_API OvrSmallFile
{
public:
	OvrSmallFile() {}
	~OvrSmallFile() { Close(); }

	bool	Open(const OvrString& a_file_path);
	void	Close();

	ovr::uint32	GetFileSize()const { return file_size; }
	const char*	GetFileData()const { return file_buffer; }

	//创建文件 并写入内容
	static bool WriteFile(const OvrString& a_file_path, const char* a_data, ovr::uint32 a_data_size);

	//zip
	bool	Open(const OvrString& a_zip_path,const OvrString& a_file_path_inzip);
	bool	ExtractZip(const OvrString& a_zip_path, const OvrString& a_extract_path);
private:
	FILE*		file_handle = nullptr;
	ovr::uint32	file_size = 0;
	char*		file_buffer = nullptr;
};

#endif

