#ifndef WX_BASE_WX_LIST_H_
#define WX_BASE_WX_LIST_H_

#include <list>
#include <condition_variable>
#include <mutex>
#include "wx_type.h"

template<class ELEMENT_TYPE>
class wxSafeList
{
public:
	wxSafeList(void)
	{}
	~wxSafeList(void)
	{
		element_list.clear();
	}

	void PushBack(ELEMENT_TYPE* a_element_ptr)
	{
		if(NULL != a_element_ptr)
		{
			std::unique_lock<std::mutex> auto_lock(list_mutex);
			element_list.push_back(a_element_ptr);
		}
	}

	ELEMENT_TYPE* PopFront(void)
	{
		ELEMENT_TYPE* element_ptr = NULL;
		std::unique_lock<std::mutex> auto_lock(list_mutex);
		if (element_list.size() > 0)
		{
			element_ptr = element_list.front();
			element_list.pop_front();
		}
		return element_ptr;
	}
	unsigned short Size(void)const
	{
		return (unsigned short)element_list.size();
	}

private:
	OVR_DISALLOW_COPY_AND_ASSIGN(wxSafeList);

private:
	std::mutex					list_mutex;
	std::list<ELEMENT_TYPE*>	element_list;
};

template<class ELEMENT_TYPE>
class wxBlockSafeList
{
public:
	wxBlockSafeList(void)
		:is_working(true)
	{}

	~wxBlockSafeList(void)
	{
		if (is_working)
		{
			Close();
		}
		element_list.clear();
	}

	void Open(void)
	{
		std::unique_lock<std::mutex> auto_lock(list_mutex);
		is_working = true;
		list_condition.notify_one();
	}
	void Close(void)
	{
		std::unique_lock<std::mutex> auto_lock(list_mutex);
		is_working = false;
		list_condition.notify_one();
	}

	//将元素添加到队里后面
	bool PushBack(ELEMENT_TYPE* a_element_ptr)
	{
		if(0 != a_element_ptr)
		{
			std::unique_lock<std::mutex> auto_lock(list_mutex);
			element_list.push_back(a_element_ptr);
			if (1 == element_list.size())
			{
				list_condition.notify_one();
			}
			return true;
		}
		return false;
	}

	//弹出首元素 如果队列为空 将一直等待 等待时间单位为毫秒
	ELEMENT_TYPE* BlockPopFront()
	{
		ELEMENT_TYPE* element_ptr = NULL;
		std::unique_lock<std::mutex> auto_lock(list_mutex);
		while (is_working && NULL == element_ptr)								//只有处于工作状态才会获取
		{
			if (element_list.size() > 0)
			{
				element_ptr = element_list.front();
				element_list.pop_front();
				break;
			}

			list_condition.wait(list_mutex);//条件激活后 将再次尝试获取
		}
		
		return element_ptr;
	}

	//弹出首元素  如果队列为空 直接返回
	ELEMENT_TYPE* PopFront(void)
	{
		ELEMENT_TYPE* element_ptr = NULL;
		std::unique_lock<std::mutex> auto_lock(list_mutex);
		if (element_list.size() > 0)
		{
			element_ptr = element_list.front();
			element_list.pop_front();
		}
		return element_ptr;
	}

	unsigned short Size(void)const
	{
		return element_list.size();
	}

private:
	OVR_DISALLOW_COPY_AND_ASSIGN(wxBlockSafeList);
private:
	bool						is_working;
	std::mutex					list_mutex;
	std::condition_variable		list_condition;
	std::list<ELEMENT_TYPE*>	element_list;
};

//双队列
template<class ELEMENT_TYPE>
class wxBlockDoubleQueue
{
public:
	wxBlockDoubleQueue(void)
		:is_working(true)
		,overlap_cnt(0)
	{}

	~wxBlockDoubleQueue(void)
	{
		if (is_working)
		{
			Close();
		}
		free_list.clear();
		result_list.clear();
	}

	void Open(void)
	{
		std::unique_lock<std::mutex> auto_lock(list_mutex);
		is_working = true;
	}

	void Close(void)
	{
		std::unique_lock<std::mutex> auto_lock(list_mutex);
		is_working = false;
		free_condition.notify_one();
		result_condition.notify_one();
	}

	//将元素添加到结果队列
	void PushResultBack(ELEMENT_TYPE* a_element_ptr)
	{
		if(NULL != a_element_ptr)
		{
			std::unique_lock<std::mutex> auto_lock(list_mutex);
			result_list.push_back(a_element_ptr);
			if (1 == result_list.size())
			{
				result_condition.notify_one();
			}
		}
	}

	//将元素添加到空闲队列
	void PushFreeBack(ELEMENT_TYPE* a_element_ptr)
	{
		if(NULL != a_element_ptr)
		{
			std::unique_lock<std::mutex> auto_lock(list_mutex);
			free_list.push_back(a_element_ptr);
			if (1 == free_list.size())
			{
				free_condition.notify_one();
			}
		}
	}

	//弹出首元素 如果队列为空 将一直等待
	ELEMENT_TYPE* PopResultFront(void)
	{
		ELEMENT_TYPE* element_ptr = NULL;
		std::unique_lock<std::mutex> auto_lock(list_mutex);
		while (is_working && NULL == element_ptr)								//只有处于工作状态才会获取
		{
			if (result_list.size() > 0)
			{
				element_ptr = result_list.front();
				result_list.pop_front();
				break;
			}

			result_condition.wait(list_mutex);//条件激活后 将再次尝试获取
		}

		return element_ptr;
	}

	//弹出首元素无阻塞 都立即返回
	ELEMENT_TYPE* PopResultFrontNoBlock(void)
	{
		ELEMENT_TYPE* element_ptr = NULL;
		std::unique_lock<std::mutex> auto_lock(list_mutex);
		if (result_list.size() > 0)
		{
			element_ptr = result_list.front();
			result_list.pop_front();
		}
		return element_ptr;
	}

	//弹出首元素 如果队列为空 将一直等待
	ELEMENT_TYPE* PopFreeFront(void)
	{
		ELEMENT_TYPE* element_ptr = NULL;
		std::unique_lock<std::mutex> auto_lock(list_mutex);
		while (is_working && NULL == element_ptr)								//只有处于工作状态才会获取
		{
			if (free_list.size() > 0)
			{
				element_ptr = free_list.front();
				free_list.pop_front();
				break;
			}

			free_condition.wait(auto_lock);//条件激活后 将再次尝试获取
		}

		return element_ptr;
	}

	//弹出首元素 如果队列为空 将一直等待
	ELEMENT_TYPE* PopFreeFrontNoBlock(void)
	{
		ELEMENT_TYPE* element_ptr = NULL;
		std::unique_lock<std::mutex> auto_lock(list_mutex);
		if (free_list.size() > 0)
		{
			element_ptr = free_list.front();
			free_list.pop_front();
		}
		return element_ptr;
	}

	int GetSize(void) const
	{
		return free_list.size() + result_list.size();
	}

	int GetResultListSize(void) const
	{
		return result_list.size();
	}

	int GetFreeListSize(void) const 
	{
		return free_list.size();
	}

	//取空闲队列首元素 如果abOverlap为TRUE 将会从Result中获取
	ELEMENT_TYPE* GetFront(bool a_is_overlap = false)
	{
		ELEMENT_TYPE* pElement = NULL;
		std::unique_lock<std::mutex> auto_lock(list_mutex);
		if (free_list.size() > 0)
		{
			pElement = free_list.front();
			free_list.pop_front();
		}
		else if (a_is_overlap && result_list.size() > 0)
		{
			pElement = result_list.front();
			result_list.pop_front();
			overlap_cnt++;
		}
		return pElement;
	}

	//获取覆盖数目
	unsigned int GetOverlapCnt(void)
	{
		unsigned int lOverlapCnt = overlap_cnt;
		overlap_cnt = 0;
		return lOverlapCnt;
	}

private:
	OVR_DISALLOW_COPY_AND_ASSIGN(wxBlockDoubleQueue);
private:
	bool						is_working;
	std::mutex					list_mutex;
	std::condition_variable		free_condition;
	std::list<ELEMENT_TYPE*>	free_list;
	std::condition_variable		result_condition;
	std::list<ELEMENT_TYPE*>	result_list;

	unsigned int				overlap_cnt;
};

#endif
