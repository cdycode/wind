﻿#ifndef OVR_CODEC_OVR_BUFFER_H_
#define OVR_CODEC_OVR_BUFFER_H_

#include "wx_base.h"
class WX_BASE_API OvrBuffer
{
public:
	OvrBuffer();
	~OvrBuffer();

	bool	Reset(ovr::uint32 a_buffer_length);
	bool	Reset(char* a_buffer, ovr::uint32 a_buffer_length, ovr::uint32 a_data_size);

	char*	GetBuffer() { return buffer; }
	ovr::uint32	GetBufferLength()const { return buffer_length; }
	ovr::uint32	GetDataSize()const { return data_size; }
	void		SetDataSize(ovr::uint32 a_data_size) { data_size = a_data_size; }

private:
	void ReleaseBuffer();
private:
	char*		buffer;
	ovr::uint32	buffer_length;
	ovr::uint32	data_size;

	bool		is_buffer_own;
};

#endif
