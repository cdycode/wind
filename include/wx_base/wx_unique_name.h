﻿#ifndef OVR_BASE_OVR_UNIQUE_NAME_H_
#define OVR_BASE_OVR_UNIQUE_NAME_H_

#include "wx_string.h"
#include "wx_base.h"

class WX_BASE_API UniqueNameCreater
{
public:
	static void CreateTrackUniqueName(OvrString& string);
	static void CreateActorUniqueName(OvrString& string);
	static void CreatePlotUniqueName(OvrString& string);
	static void CreateSenceUniqueName(OvrString& string);
	static void CreateSequenceUniqueName(OvrString& string);

private:
	UniqueNameCreater();
	~UniqueNameCreater();
};

#endif 