﻿#ifndef OVR_BASE_OVR_DLL_LOADER_H_
#define OVR_BASE_OVR_DLL_LOADER_H_

#include "wx_base.h"
#include "wx_string.h"

class WX_BASE_API OvrDllLoader
{
public:
	OvrDllLoader() {}
	~OvrDllLoader() { Unload(); }

	bool	Load(const OvrString& a_dll_path);
	void	Unload();
	bool	IsLoad() const { return nullptr != dll_handle; }

	void*	GetFunctionAddress(const OvrString& a_function_name);
	
private:
	void*	dll_handle = nullptr;
};

#endif
