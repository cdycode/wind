﻿#ifndef OVR_BASE_OVR_QUATERNION_H_
#define OVR_BASE_OVR_QUATERNION_H_

#include "wx_base.h"
#include "wx_vector3.h"

//PI的定义
#define OVR_PI	3.1415926535898f
//弧度转角度
#define OVR_RAD_TO_DEG(rad)	((rad)*180.0f/OVR_PI)
//角度转弧度
#define OVR_DEG_TO_RAD(deg)	((deg)*OVR_PI/180.0f)

//欧拉角的旋转顺序：yxz
class WX_BASE_API OvrQuaternion
{
public:
	OvrQuaternion();
	OvrQuaternion(const float& a_x,const float& a_y,const float& a_z,const float& a_w);
	OvrQuaternion(const OvrVector3& a_axis,const float& a_angle_rad);
	OvrQuaternion(const OvrVector3& a_from,const OvrVector3& a_to);
	~OvrQuaternion(){}
public:
	float& operator[](int id);
	const float& operator[](int id)const;
	OvrQuaternion	operator+(const OvrQuaternion& a_q)const;
	void	operator+=(const OvrQuaternion& a_q);
	OvrQuaternion	operator-(const OvrQuaternion& a_q)const;
	void	operator-=(const OvrQuaternion& a_q);
	OvrQuaternion operator*(const float& a_s)const;
	void operator*=(const float& a_s);
	OvrQuaternion operator*(const OvrQuaternion& a_q)const;
	OvrVector3 operator*(const OvrVector3& a_q)const;

public:
	float length()const;
	float length_sq()const;
	void normalize();
	OvrQuaternion get_normalize()const;
	OvrQuaternion get_conj()const;
	void inverse();
	OvrQuaternion get_inverse()const;
	OvrVector3	get_imag()const;
	//单位为弧度 而不是角度
	OvrQuaternion& set_axis_angle(const OvrVector3& a_axis,const float& a_angle);
	void	get_axis_angle(OvrVector3& axis, float& ao_angle) const;

	//单位为弧度 而不是角度
	void	get_euler_angles(float& a, float& b, float& c) const;
public:
	static OvrQuaternion lerp(const OvrQuaternion& a_q0,const OvrQuaternion& a_q1,const float& a_pos);
	static OvrQuaternion slerp(const OvrQuaternion& a_q1,const OvrQuaternion& a_q2,const float& a_pos);

	//单位为弧度 而不是角度
	static OvrQuaternion make_euler_quat(const float& a_pitch,const float& a_yaw,const float& a_roll );
	//I do not know whether is good or not to make this public,since somebody want to directly use w,x,y,z
	//directly,it is not like vector4f or something else,but here,may be I should open it for somebody really
	//need to know who is w.may be I will close it in the near future,but now,this is my choice,I do not want to,
	//but it is like I have to.
	static OvrQuaternion make_quat_from_to(const OvrVector3& a_from, const OvrVector3& a_to);
	static OvrQuaternion make_quat_from_up_dir(const OvrVector3& a_up,const OvrVector3& a_dir);
public:
	union
	{
		struct
		{
			float w,x,y,z;
		};
		float	_m[4];
	};
	static const OvrQuaternion ZERO;
	static const OvrQuaternion IDENTITY;
};

#endif
