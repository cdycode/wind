﻿#ifndef OVR_BASE_OVR_TRANSFORM_H_
#define OVR_BASE_OVR_TRANSFORM_H_

#include "wx_base.h"
#include "wx_vector3.h"
#include "wx_quaternion.h"
#include "wx_matrix4f.h"

class WX_BASE_API OvrTransform
{
public:
	OvrTransform() 
		:position(0.0f)
		,scale(1.0f)
	{}
	~OvrTransform() {}

	void operator = (const OvrTransform& a_transform);

	void	GetMatrix(OvrMatrix4& a_matrix)const;

	OvrVector3		position;
	OvrVector3		scale;
	OvrQuaternion	quaternion;
};

#endif
