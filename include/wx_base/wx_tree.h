﻿#ifndef OVR_BASE_OVR_TREE_H_
#define OVR_BASE_OVR_TREE_H_

#include <list>

template<class T>
class OvrTree
{
public:
	OvrTree() {}
	~OvrTree() {}

	//只是设置父类
	void					SetParent(T* a_parent) { parent = a_parent;}
	T*						GetParent() { return parent; }
	const std::list<T*>&	GetChildren()const { return children; }

	//判断是否是子children
	bool			IsChild(T* a_child) 
	{
		if (nullptr == a_child)
		{
			return false;
		}

		bool is_child = false;
		for (auto it = children.begin(); it != children.end(); it++)
		{
			if (*it == a_child)
			{
				is_child = true;
				break;
			}
		}
		return is_child;
	}

	//添加到children
	bool	AddChild(T* a_child) 
	{
		if (nullptr == a_child)
		{
			return false;
		}

		if (IsChild(a_child))
		{
			return true;
		}

		children.push_back(a_child);
		a_child->SetParent((T*)this);

		return true;
	}

	//移除children
	bool RmvChild(T* a_child) 
	{
		if (nullptr == a_child)
		{
			return false;
		}

		bool is_remove = false;
		for (auto it = children.begin(); it != children.end(); it++)
		{
			if (*it == a_child)
			{
				children.erase(it);
				a_child->SetParent(nullptr);
				is_remove = true;
				break;
			}
		}
		return is_remove;
	}

protected:
	T*				parent = nullptr;
	std::list<T*>	children;
};

#endif
