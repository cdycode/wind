﻿#ifndef OVR_BASE_OVR_MATERIX4_H_
#define OVR_BASE_OVR_MATERIX4_H_

#include "wx_base.h"
#include "wx_quaternion.h"
#include "wx_vector4.h"

class WX_BASE_API OvrMatrix4
{
public:
	OvrMatrix4();
	OvrMatrix4(float a_v);
	OvrMatrix4(const OvrMatrix4& a_matrix4);
	OvrMatrix4(const OvrVector4& a_v);
	OvrMatrix4(const OvrVector4& a_v0,const OvrVector4& a_v1,const OvrVector4& a_v2,const OvrVector4& a_v3);
	OvrMatrix4(float a_m00,float a_m01,float a_m02,float a_m03,
		float a_m10,float a_m11,float a_m12,float a_m13,
		float a_m20,float a_m21,float a_m22,float a_m23,
		float a_m30,float a_m31,float a_m32,float a_m33);
	~OvrMatrix4();

public:
	float& operator[](int a_id);
	const float& operator[](int a_id)const;
	float& operator()(int a_row,int a_col);
	const float& operator()(int a_row,int a_col)const;
	void operator =(const OvrMatrix4& a_mat4);

	OvrVector4 operator*(const OvrVector4& a_vec4)const;
	OvrVector3 operator*(const OvrVector3& a_vec3)const;
	OvrMatrix4 operator*(const float& a_s) const;
	void operator*=(const float& a_s);
	OvrMatrix4 operator/(const float& a_s) const;
	void operator/=(const float& a_s);
	OvrMatrix4 operator*(const OvrMatrix4& a_mat4)const;
	OvrMatrix4 operator+(const OvrMatrix4& a_mat4)const;
	void operator+=(const OvrMatrix4& a_mat4);
	OvrMatrix4 operator-(const OvrMatrix4& a_mat4)const;
	void operator-=(const OvrMatrix4& a_mat4);
public:
	//this two method is only for working with mvp projection,should be considered.
	OvrVector3 transform_vector(const OvrVector3& a_vec3)const;
	OvrVector3 transform_point(const OvrVector3& a_vec3)const;
	float	determinant() const;
	bool inverse();
	bool get_inverse(OvrMatrix4& ao_mat4)const;
	OvrMatrix4 get_transpose()const;
	void transpose();
	OvrQuaternion to_quaternion()const;
	void from_quaternion(const OvrQuaternion& a_q);

	bool decompose(OvrVector3* ao_scale,OvrQuaternion* a_rotation,OvrVector3* a_translation)const;

	void set_translation(const OvrVector3& a_trans);
	OvrVector3 get_translation();
public:
	static void mul(const OvrMatrix4& a_mat0,const OvrMatrix4& a_mat1, OvrMatrix4& ao_mat);
	static OvrMatrix4 make_translation_matrix(const OvrVector3& a_trans);
	static OvrMatrix4 axis_angle_rotate_by_center(const OvrVector3& a_rot_center,const OvrVector3& a_axis,float a_angle_rad);
	static OvrMatrix4 make_scale_matrix_around_axis(const OvrVector3& a_axis,float a_s);
	static OvrMatrix4 make_scale_matrix(const OvrVector3& a_scales);
	static OvrMatrix4 make_rotation_matrix_around_axis(const OvrVector3& a_axis,float a_angle_rad);
	static OvrMatrix4 make_reflection_matrix_by_plane(const OvrVector3& a_normal,float a_dist_to_origin);
	static OvrMatrix4 make_view_matrix(const OvrVector3& a_pos,const OvrVector3& a_target,const OvrVector3& a_up);
	static OvrMatrix4 make_ortho_matrix(float a_width,float a_height,float a_near,float a_far);
	static OvrMatrix4 make_proj_matrix_rh(float a_view_angle_rad,float a_aspect_ratio,float a_near,float a_far);
	static OvrMatrix4 make_proj_matrix_lh(float a_view_angle_rad,float a_aspect_ratio,float a_near,float a_far);
	static OvrMatrix4 make_rotate_from_to_matrix(const OvrVector3& a_src,const OvrVector3 a_dst);

private:
	union
	{
		struct 
		{
			OvrVector4	_x,_y,_z,_d;
		};
		float _m[16];
		float _mm[4][4];
	};

};


#endif
