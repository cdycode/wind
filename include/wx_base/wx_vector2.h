﻿#ifndef OVR_BASE_OVR_VECTOR2_H_
#define OVR_BASE_OVR_VECTOR2_H_

#include "wx_base.h"

class WX_BASE_API OvrVector2
{
public:
	OvrVector2();
	explicit OvrVector2(float a_v);
	OvrVector2(float a_x, float a_y);
	~OvrVector2();
public:
	float& operator[](int a_id);
	const float& operator[](int a_id) const;

	OvrVector2 operator +(const OvrVector2& a_v2) const;
	OvrVector2 operator -(const OvrVector2& a_v2) const;
	OvrVector2 operator -() const;
	float operator *(const OvrVector2& a_v2) const;
	OvrVector2 operator *(float a_scale) const;
	OvrVector2 operator /(float a_scale) const;

	void operator +=(const OvrVector2& a_v2);
	void operator -=(const OvrVector2& a_v2);
	void operator *=(float a_scale);
	void operator /=(float a_scale);
	bool operator == (const OvrVector2& a_v2)const;

	bool operator <(const OvrVector2& a_v2)const;
public:

	float length()const;
	float length_sq()const;
	float normalize();
	OvrVector2 get_normalize()const;
	OvrVector2 multi(const OvrVector2& a_v2) const;
	bool	is_nan() const;
public:
	union
	{
		struct {
			float _x, _y;
		};
		float _m[2];
	};
};




#endif
