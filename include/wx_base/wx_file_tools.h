﻿#ifndef OVR_BASE_OVR_FILETOOLS_H
#define OVR_BASE_OVR_FILETOOLS_H

#include "wx_string.h"
#include "wx_base.h"
#include <stdio.h>

class WX_BASE_API OvrFileTools
{
	public:
		typedef enum
		{
			Type_File = 0,
			Type_Dir
		} FileType;

	public:
		/* 根据文件路径获得其路径，如该路径不存，返回false， 如果输入路径为文件夹，则输出跟输入一致，如输入为文件路径，则输出为该文件所在目录的路径 */
		static bool GetPathFromFullPath(const char* fullPath, OvrString& path);

		/* 根据路径获得文件名称（不含扩展名），如该路径不存或该路径为文件夹，返回false */
		static bool GetFileNameFromPath( const char* path, OvrString& fileName);

		/* 根据路径获得文件名称（含扩展名），如该路径不存或该路径为文件夹，返回false */
		static bool GetFileAllNameFromPath(const char* path, OvrString& fileName);

		/* 根据路径获得文件后缀，如该路径不存或该路径为文件夹，返回false */
		static bool GetFileSuffixFromPath(const OvrString& a_file_path, OvrString& a_file_suffix);

		/* 判断路径最后一个字符是否是斜杠 */
		static bool CheckPathLastIsSlash( const char* a_path );

		/*  --------------遍历当前目录下的文件夹和文件,默认按字母顺序遍历 -------------- */
		static bool TraverseFiles(const char* path, void(*FileFunc)(void*, const char*, FileType), void* handlePtr, const unsigned int filePathOffset);

		/*  --------------遍历当前目录下后缀名为exName的文件(*.*为通配),默认按字母顺序遍历 -------------- */
		static bool TraverseFiles(const char* path, const char* exName, void(*FileFunc)(void*, const char*), void* handlePtr, const unsigned int filePathOffset);

		/*  --------------深度优先递归遍历当前目录下（含子目录）所有后缀名为exName的文件(*.*为通配) -------------- */
		static bool DfsFolder(const char* path, const char* exName, void(*FileFunc)(void*, const char*), void* handlePtr, const unsigned int filePathOffset);

		/*获取目录中的所有文件 不包含子目录*/
		static ovr::uint32	GetFilesInDir(const OvrString& a_dir, std::list<OvrString>& a_file_name_list);

		/* 创建并打开指定的文件（创建目录树），只写 */
		static bool MakeFile(const char* path);

		/* 创建并打开指定的文件（创建目录树），只写 */
		static FILE* CreateFileX(const OvrString& a_file_path);

		/* 打开指定的文件（创建目录树），只读 */
		static FILE* OpenFile( const char* path );

		/* 创建指定路径的目录（创建目录树）*/
		static bool CreateFolder(const OvrString& a_dir_path);

		/*  --------------删除指定路径的文件或目录 -------------- */
		static bool DeleteFile(const OvrString& a_path);

		/*  --------------删除指定路径的文件或目录 -------------- */
		static bool DeleteFiles(const OvrString& a_path);

		//复制文件
		static bool CopyOneFile(const OvrString& a_src_file_path, const OvrString& a_dst_file_path);

		static bool	IsFileExist(const OvrString& a_file);

	private:
		static bool MakeDir( const char* path );
		static bool InitDir( const char* path );
		static bool FilterByExten(const char* fileName, const char* exName);
		static void TransformSlash(OvrString& a_string);
		static bool DfsFolderRecursion(const char* path, const char* exName, void(*FileFunc)(void*, const char*), void* handlePtr, const unsigned int rootPathLen);

	private:
		OvrFileTools()
		{
		}

		~OvrFileTools()
		{
		}

		OvrFileTools( const OvrFileTools& )
		{
		}

		OvrFileTools& operator= ( const OvrFileTools& )
		{
			return *this;
		}
};

#endif
