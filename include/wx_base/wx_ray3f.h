﻿#ifndef OVR_BASE_OVR_RAY3F_H_
#define OVR_BASE_OVR_RAY3F_H_

#include "wx_base.h"
#include "wx_vector3.h"

class WX_BASE_API OvrRay3f
{
public:
	OvrRay3f() {}
	OvrRay3f(const OvrVector3& a_origin, const OvrVector3& a_dir) { origin_point = a_origin; dir_look = a_dir; }
	~OvrRay3f() {}
public:
	OvrVector3 GetOrigin()const { return origin_point; }
	OvrVector3 GetDir()const { return dir_look; }
	void SetOrigin(const OvrVector3& a_origin) { origin_point = a_origin; }
	void SetDir(const OvrVector3& a_dir) { dir_look = a_dir; }
private:
	OvrVector3	origin_point;
	OvrVector3	dir_look;
};


#endif
