﻿#ifndef OVR_BASE_OVR_SINGLETON_H_
#define OVR_BASE_OVR_SINGLETON_H_

#include "wx_base.h"

template<class T>
class ovr_singleton 
{
public:
	static T*		GetIns() 
	{
		if (nullptr != s_instance)
		{
			return s_instance;
		}
		s_instance = new T;
		return s_instance;
	}

	static void		DelIns() 
	{
		if (nullptr != s_instance)
		{
			delete s_instance;
			s_instance = nullptr;
		}
	}

protected:
	ovr_singleton() {}
	ovr_singleton(const ovr_singleton&) {} //防止拷贝构造一个实例  
	ovr_singleton& operator=(const ovr_singleton&) { return *this; } //防止赋值出另一个实例 
	~ovr_singleton() {}

protected:
	static T*	s_instance;
};

template<class T> T* ovr_singleton<T>::s_instance = nullptr;

template<class T>
class OvrSingleton
{
public:
	static T*		GetIns()
	{
		if (nullptr != s_instance)
		{
			return s_instance;
		}
		s_instance = new T;
		return s_instance;
	}

	static void		DelIns()
	{
		if (nullptr != s_instance)
		{
			delete s_instance;
			s_instance = nullptr;
		}
	}

protected:
	OvrSingleton() {}
	OvrSingleton(const OvrSingleton&) {} //防止拷贝构造一个实例  
	OvrSingleton& operator=(const OvrSingleton&) { return *this; } //防止赋值出另一个实例 
	~OvrSingleton() {}

protected:
	static T*	s_instance;
};

template<class T> T* OvrSingleton<T>::s_instance = nullptr;

#endif
