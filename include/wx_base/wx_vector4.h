﻿#ifndef OVR_BASE_OVR_VECTOR4_H_
#define OVR_BASE_OVR_VECTOR4_H_

#include "wx_base.h"
#include "wx_vector3.h"

class WX_BASE_API OvrVector4
{
public:
	union
	{
		struct{
			float _x,_y,_z,_w;
		};
		float _m[4];
	};
	OvrVector4():_x(0),_y(0),_z(0),_w(0){}
	explicit OvrVector4(float a_v):_x(a_v),_y(a_v),_z(a_v),_w(a_v){}
	OvrVector4(float a_x,float a_y,float a_z, float a_w):_x(a_x),_y(a_y),_z(a_z),_w(a_w){}
	OvrVector4(const OvrVector3& a_v3,float a_w)
		:_x(a_v3[0]),_y(a_v3[1]),_z(a_v3[2]),_w(a_w){}
	~OvrVector4(){}
public:
	float& operator[](int a_id);
	const float& operator[](int a_id) const;

	OvrVector4 operator +(const OvrVector4& a_v4) const;
	OvrVector4 operator -(const OvrVector4& a_v4) const;
	float operator *(const OvrVector4& a_v4) const;
	OvrVector4 operator *(float a_scale) const;
	OvrVector4 operator /(float a_scale) const;

	void operator +=(const OvrVector4& a_v4);
	void operator -=(const OvrVector4& a_v4);
	void operator *=(float a_scale);
	void operator /=(float a_scale);
	bool operator == (const OvrVector4& a_v4)const;
	bool operator <(const OvrVector4& a_v2)const;
public:
	OvrVector3 xyz()const;
	float length()const;
	float length_sq()const;
	void normalize();
	OvrVector4 get_normalize()const;
	OvrVector4 cross(const OvrVector4& a_v4) const;
	OvrVector4 multi(const OvrVector4& a_v4) const;

	OvrVector4 lerp(const OvrVector4& a_v4,float a_pos)const;

	bool	is_nan( ) const;
private:
	//union
	//{
	//	struct{
	//		float _x,_y,_z,_w;
	//	};
	//	float _m[4];
	//};

public:
	static const OvrVector4	zero;
};

#endif
