﻿#ifndef OVR_BASE_OVR_IOINTERFACE_H
#define OVR_BASE_OVR_IOINTERFACE_H

#include "wx_base.h"
#include "wx_type.h"
#include <stdio.h>

class WX_BASE_API OvrIOInerface
{
	public:
		OvrIOInerface()
		{
		}

		virtual ~OvrIOInerface()
		{
		}

	public:
		virtual ovr::uint32 Append ( const void* data, ovr::uint32 len ) = 0;
		virtual ovr::uint32 Subtract ( void* dest, ovr::uint32 len ) = 0;
		virtual ovr::uint32 GetLength() = 0;
		virtual void SetPosition (ovr::uint32 pos ) = 0;
		virtual ovr::uint32 GetPosition() = 0;
};

/*-----------------------------------------------------------------------------------------------*/

class WX_BASE_API OvrIOFileHandle : public OvrIOInerface
{
	public:
		OvrIOFileHandle();
		virtual ~OvrIOFileHandle();

	public:
		bool Open( const char* filePath, const bool isWrite );

		static OvrIOFileHandle* CreateHandleFromFile( const char* filePath );
		static OvrIOFileHandle* LoadHandleFromFile( const char* filePath );

	public:
		virtual ovr::uint32 Append ( const void* data, ovr::uint32 len );
		virtual ovr::uint32 Subtract ( void* dest, ovr::uint32 len );
		virtual ovr::uint32 GetLength();
		virtual void SetPosition (ovr::uint32 pos );
		virtual ovr::uint32 GetPosition();

	private:
		inline void SetFile( FILE* file );

	private:
		OvrIOFileHandle( const OvrIOFileHandle& )
		{
		}

		OvrIOFileHandle& operator= ( const OvrIOFileHandle& )
		{
			return *this;
		}

	private:
		ovr::uint32 iLength;
		ovr::uint32 iPosition;
		FILE* iFile;
};

/*-----------------------------------------------------------------------------------------------*/

class WX_BASE_API OvrIOVarBufferHandle : public OvrIOInerface
{
	public:
		OvrIOVarBufferHandle( const ovr::uint32 capacity, bool isInert = false );
		OvrIOVarBufferHandle( const void* buffer, const ovr::uint32 len, bool isInert = false );
		virtual ~OvrIOVarBufferHandle();

	public:
		virtual ovr::uint32 Append ( const void* data, ovr::uint32 len );
		virtual ovr::uint32 Subtract ( void* dest, ovr::uint32 len );
		virtual ovr::uint32 GetLength();
		virtual void SetPosition (ovr::uint32 pos );
		virtual ovr::uint32 GetPosition();

	public:
		/* Removes `len` bytes from the beginning of the buffer. */
		void Remove (ovr::uint32 len );

		/* If `new_size` is smaller than buffer's `len`, the resize is not performed. */
		void Resize ( const ovr::uint32 new_size );
		void ClearData();

		const ovr::uint8* GetBuffer();
		void PrintHex ( const char* tag );

	private:
		OvrIOVarBufferHandle( const OvrIOVarBufferHandle& )
		{
		}

		OvrIOVarBufferHandle& operator= ( const OvrIOVarBufferHandle& )
		{
			return *this;
		}

		inline ovr::uint32 Insert ( const void* buf, ovr::uint32 len );
		inline ovr::uint32 Overlap ( const void* buf, ovr::uint32 len );

	private:
		ovr::uint8* iBuffer;
		ovr::uint32 iLength;  /* Data length. Data is located between offset 0 and iLength. */
		ovr::uint32 iCapacity; /* Buffer capacity Must be >= iLength */
		ovr::uint32 iPosition;
		bool iModel;
};

#endif
