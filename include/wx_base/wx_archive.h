﻿#ifndef OVR_BASE_OVR_ARCHIVE_H_
#define OVR_BASE_OVR_ARCHIVE_H_

#include "wx_base.h"
#include "wx_string.h"
#include "wx_buffer.h"
#include "wx_io_interface.h"
#include "wx_transform.h"
#include "wx_vector3.h"
#include "wx_vector4.h"

class WX_BASE_API OvrArchive
{
public:
	enum Type { kLoad, kStore };
public:
	OvrArchive( OvrIOInerface* a_io, bool a_little );
	virtual ~OvrArchive() {}

	//获取归档类型
	virtual Type	GetType() = 0;

	//输出 从a_buffer中取数据给传入的参数
	virtual bool Serialize(ovr::int8& a_value) = 0;
	virtual bool Serialize(ovr::int16& a_value) = 0;
	virtual bool Serialize(ovr::int32& a_value) = 0;
	virtual bool Serialize(ovr::int64& a_value) = 0;
	virtual bool Serialize(ovr::uint8& a_value) = 0;
	virtual bool Serialize(ovr::uint16& a_value) = 0;
	virtual bool Serialize(ovr::uint32& a_value) = 0;
	virtual bool Serialize(ovr::uint64& a_value) = 0;
	virtual bool Serialize(float& a_value) = 0;
	virtual bool Serialize(double& a_value) = 0;
	virtual bool SerializeBuffer(void* a_buffer, ovr::int32 a_buffer_length, ovr::int32 a_data_size) = 0;
	virtual bool Serialize(OvrString& a_string) = 0;
	virtual bool Serialize(OvrBuffer& a_buffer) = 0;
	virtual bool Serialize(OvrTransform& a_tf) = 0;
	virtual bool Serialize(OvrVector3& a_vector) = 0;
	virtual bool Serialize(OvrVector4& a_vector) = 0;

	ovr::uint32 GetLength();
	bool		Seek(ovr::uint32 pos);
	ovr::uint32 GetPosition();

private:
	OVR_DISALLOW_COPY_AND_ASSIGN(OvrArchive);

protected:
	bool		is_host_little_endian;
	OvrIOInerface* io_handle;
};

class WX_BASE_API OvrArchiveLoad : public OvrArchive
{
public:
	OvrArchiveLoad(OvrIOInerface* a_io, bool a_little = true);
	virtual ~OvrArchiveLoad() {}

	//获取归档类型
	virtual Type	GetType()override { return OvrArchive::kLoad; }

	//输出 从a_buffer中取数据给传入的参数
	virtual bool Serialize(ovr::int8& a_value) override;
	virtual bool Serialize(ovr::int16& a_value) override;
	virtual bool Serialize(ovr::int32& a_value) override;
	virtual bool Serialize(ovr::int64& a_value) override;
	virtual bool Serialize(ovr::uint8& a_value) override;
	virtual bool Serialize(ovr::uint16& a_value) override;
	virtual bool Serialize(ovr::uint32& a_value) override;
	virtual bool Serialize(ovr::uint64& a_value) override;
	virtual bool Serialize(float& a_value) override;
	virtual bool Serialize(double& a_value) override;
	virtual bool SerializeBuffer(void* a_buffer, ovr::int32 a_buffer_length, ovr::int32 a_data_size) override;
	virtual bool Serialize(OvrString& a_string) override;
	virtual bool Serialize(OvrBuffer& a_buffer) override;
	virtual bool Serialize(OvrTransform& a_tf) override;
	virtual bool Serialize(OvrVector3& a_vector) override;
	virtual bool Serialize(OvrVector4& a_vector) override;

private:
	OVR_DISALLOW_COPY_AND_ASSIGN(OvrArchiveLoad);
};

class WX_BASE_API OvrArchiveStore : public OvrArchive
{
public:
	OvrArchiveStore(OvrIOInerface* a_io, bool a_little = true);
	virtual ~OvrArchiveStore() {}

	//获取归档类型
	virtual Type	GetType()override { return OvrArchive::kStore; }

	//输出 从a_buffer中取数据给传入的参数
	virtual bool Serialize(ovr::int8& a_value) override;
	virtual bool Serialize(ovr::int16& a_value) override;
	virtual bool Serialize(ovr::int32& a_value) override;
	virtual bool Serialize(ovr::int64& a_value) override;
	virtual bool Serialize(ovr::uint8& a_value) override;
	virtual bool Serialize(ovr::uint16& a_value) override;
	virtual bool Serialize(ovr::uint32& a_value) override;
	virtual bool Serialize(ovr::uint64& a_value) override;
	virtual bool Serialize(float& a_value) override;
	virtual bool Serialize(double& a_value) override;
	virtual bool SerializeBuffer(void* a_buffer, ovr::int32 a_buffer_length, ovr::int32 a_data_size) override;
	virtual bool Serialize(OvrString& a_string) override;
	virtual bool Serialize(OvrBuffer& a_buffer) override;
	virtual bool Serialize(OvrTransform& a_tf) override;
	virtual bool Serialize(OvrVector3& a_vector) override;
	virtual bool Serialize(OvrVector4& a_vector) override;

private:
	OVR_DISALLOW_COPY_AND_ASSIGN(OvrArchiveStore);
};

#endif

