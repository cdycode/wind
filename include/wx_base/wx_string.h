﻿#ifndef OVR_BASECLASS_OVR_STRING_H_
#define OVR_BASECLASS_OVR_STRING_H_

#include "wx_base.h"
#include <string>
#include <string.h>
//#include <bits/stringfwd.h>
#include <list>

class WX_BASE_API OvrString
{
public:
	static void GbkToUtf8(const OvrString& a_in_gbk, OvrString& a_out_utf8);
	static void Utf8ToGbk(const OvrString& a_in_utf8, OvrString& a_out_gbk);
	static void GbkToUnicode(const OvrString& a_in_gbk, std::wstring& a_out_unicode);
	static void UnicodeToGbk(const std::wstring& a_in_unicode, OvrString& a_out_gbk);
	static void UnicodeToUtf8(const std::wstring& a_in_unicode, OvrString& a_out_utf8);
	static void Utf8ToUnicode(const OvrString& a_in_utf8, std::wstring& a_out_unicode);

public:
	OvrString();
	OvrString(const char* a_string);
	OvrString(const char* a_string, ovr::uint32 a_string_size);
	OvrString(const OvrString& a_string);
	explicit OvrString(ovr::uint32 a_buffer_size);
	~OvrString();

	OvrString& operator = (const char* a_src_string);
	OvrString& operator = (const OvrString& a_src_string);
	bool		operator == (const char* a_string)const;
	bool		operator == (const OvrString& a_string)const;
	bool		operator != (const char* a_string)const;
	bool		operator != (const OvrString& a_string)const;
	char&		operator [] (ovr::uint32 a_index);
	bool		operator += (const char* a_append_string);
	bool		operator += (const OvrString& a_append_string);
	OvrString	operator + (const OvrString& a_string)const;
	bool		operator < (const OvrString& a_string)const;

	//找到 返回索引 找不到 返回-1
	ovr::int32	FindLastCharPos(char a_char)const;
	OvrString	SubString(ovr::uint32 a_from_pos, ovr::uint32 a_size)const;
	//以a_char为界限  分为多个字符串 保存到a_list中 返回子字符串的个数
	ovr::uint32	Split(char a_char, std::list<OvrString>& a_list)const;

	bool AppendChar(const char c);
	bool SetString(const char* a_src_string);
	bool AppendString(const char* a_string);
	bool SetString(const char* a_src_string, ovr::uint32 len);
	const char*	GetStringConst()const { return string_buffer; }
	char*		GetString() { return string_buffer; }
	ovr::uint32	GetStringSize()const { return string_size; }
	ovr::uint32	GetBufferLength()const { return buffer_length; }
	void		UpdateStringSize() { string_size = (ovr::uint32)strlen(string_buffer); }

	//转成UTF8
	void		ToUtf8(OvrString& a_utf8)const;
	void		FromUtf8(const char* a_utf8);

	//会丢失原数据
	bool	Resize(ovr::uint32 a_new_size);
	void	Reset();
	void	Clear();	//与Reset实现一致

private:
	bool AppendString(const char* a_string, ovr::uint32 len);

private:
	char*		string_buffer = nullptr;
	ovr::uint32	string_size = 0;
	ovr::uint32	buffer_length = 0;

public:
	static const OvrString	empty;

};//class String


#endif

