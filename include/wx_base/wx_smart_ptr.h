#ifndef OVR_BASE_OVR_SMART_PTR_H_
#define OVR_BASE_OVR_SMART_PTR_H_

#include <iostream>
#include <memory>

template<typename T>
class OvrSmartPtr 
{
private:
	T* pointer			= nullptr;
	size_t* use_count	= nullptr;
public:
	OvrSmartPtr(T* a_ptr = nullptr) 
		:pointer(a_ptr) 
	{
		if (pointer) 
		{
			use_count = new size_t(1);
		}
		else 
		{
			use_count = new size_t(0);
		}
	}

	OvrSmartPtr(const OvrSmartPtr& a_smart_ptr) 
	{
		if (this != &a_smart_ptr) 
		{
			pointer = a_smart_ptr.pointer;
			use_count = a_smart_ptr.use_count;
			(*use_count)++;
		}
	}

	OvrSmartPtr& operator=(const OvrSmartPtr& a_smart_ptr) 
	{
		if (pointer == a_smart_ptr.pointer)
		{
			return *this;
		}

		if (nullptr != pointer)
		{
			(*use_count)--;
			if (use_count == 0)
			{
				delete pointer;
				delete use_count;
			}
		}

		pointer = a_smart_ptr.pointer;
		use_count = a_smart_ptr.use_count;
		(*use_count)++;
		return *this;
	}

	T& operator*() 
	{
		assert(pointer == nullptr);
		return *(pointer);
	}

	T* operator->() 
	{
		assert(pointer == nullptr);
		return pointer;
	}

	~OvrSmartPtr() 
	{
		(*use_count)--;
		if (*use_count == 0)
		{
			delete pointer;
			delete use_count;
		}
	}

	size_t UseCount() 
	{
		return *use_count;
	}

	T*	GetPtr() { return pointer; }

	//引用计数 会自动增减
	void	AddRef()
	{
		(*use_count)++;
	}
	void	Release() 
	{
		(*use_count)--;
	}
};
#endif
