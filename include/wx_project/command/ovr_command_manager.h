﻿#ifndef OVR_PROJECT_OVR_COMMAND_MANAGER_H_
#define OVR_PROJECT_OVR_COMMAND_MANAGER_H_

#include <list>
#include <mutex>
#include "../ovr_project.h"

namespace ovr_project 
{
	class OVR_PROJECT_API IOvrCommand
	{
	public:
		IOvrCommand() {}
		virtual ~IOvrCommand() {}

		virtual bool	Do() = 0;
	};

	class OVR_PROJECT_API OvrCommandManager
	{
	public:
		OvrCommandManager();
		~OvrCommandManager();

		//添加命令
		void	PushCommand(IOvrCommand* a_command);

		//执行命令
		bool	Update();

	private:
		std::mutex				command_mutex;
		std::list<IOvrCommand*>	command_list;
	};
}

#endif
