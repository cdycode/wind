﻿#ifndef OVR_PROJECT_OVR_COMMAND_ACTOR_H_
#define OVR_PROJECT_OVR_COMMAND_ACTOR_H_

#include "ovr_command_manager.h"
#include "wx_core/ovr_scene_context.h"
#include <wx_engine/ovr_video_component.h>
#include <wx_engine/ovr_light_component.h>

namespace ovr_project 
{
	class OVR_PROJECT_API OvrCommandActor : public  IOvrCommand
	{
	public:
		OvrCommandActor() {}
		virtual ~OvrCommandActor() {}

	public:
		void	SetDisplayName(const OvrString& a_display_name) { display_name = a_display_name; }
		void	SetForwardPosition(bool a_is_forward) { is_forward = a_is_forward; }
	protected:
		void	GetPlacePosition(OvrVector3& a_position);

	protected:
		bool		is_forward = false;
		OvrString	display_name;

	};

	class OVR_PROJECT_API OvrCommandLoadActor : public OvrCommandActor
	{
	public:
		OvrCommandLoadActor() {}
		virtual ~OvrCommandLoadActor() {}

		virtual bool	Do() override;

		void	SetFilePath(const OvrString& a_file_path) { mesh_file_path = a_file_path; }
		
	private:
		
		OvrString					mesh_file_path;
	};

	class OVR_PROJECT_API OvrCommandCreateActor : public OvrCommandActor
	{
	public:
		enum ActorType 
		{
			kInvalid,
			k3DAudio,
			kEmptyActor,
			kText,
			kCameraActor,
			kParticle,
		};
	public:
		OvrCommandCreateActor() {}
		virtual ~OvrCommandCreateActor() {}

		virtual bool	Do() override;

		void	SetActorType(ActorType a_type) { actor_type = a_type; }

	private:
		ActorType	actor_type = kInvalid;
	};

	class OVR_PROJECT_API OvrCreateVideoActor : public OvrCommandActor
	{
	public:
		OvrCreateVideoActor() {}
		virtual ~OvrCreateVideoActor() {}

		virtual bool	Do() override;

		void	SetVideoType(ovr_engine::VideoType a_type) { video_type = a_type; }

	private:
		ovr_engine::VideoType	video_type = ovr_engine::Video2D;
	};

	class OVR_PROJECT_API OvrCommandCreateLight : public OvrCommandActor
	{
	public:
		OvrCommandCreateLight() {}
		virtual ~OvrCommandCreateLight() {}

		virtual bool	Do() override;

		void	SetLightType(ovr_engine::OvrLightType a_type) { light_type = a_type; }

	private:
		ovr_engine::OvrLightType	light_type = ovr_engine::eLightNum;
	};

	class OVR_PROJECT_API OvrRemoveActor : public OvrCommandActor
	{
	public:
		OvrRemoveActor() {}
		virtual ~OvrRemoveActor() {}

		void	SetActorUniqueName(const OvrString& a_actor_unique_name) { actor_unique_name = a_actor_unique_name; }

		virtual bool	Do() override;

	private:
		OvrString	actor_unique_name;
	};

	class OVR_PROJECT_API OvrCopyActor : public OvrCommandActor
	{
	public:
		OvrCopyActor() {}
		virtual ~OvrCopyActor() {}

		void SetActorUniqueName(const OvrString& a_actor_unique_name) { actor_unique_name = a_actor_unique_name; }

		virtual bool Do() override;

	private:
		OvrString actor_unique_name;
	};


	class OVR_PROJECT_API OvrRelationChangedActor : public OvrCommandActor
	{
	public:
		OvrRelationChangedActor() {}
		virtual ~OvrRelationChangedActor() {}


		void SetChangeRelationName(const OvrString& a_parent, const OvrString& a_child)
		{
			parent_actor_unique_name = a_parent;
			child_actor_unique_name = a_child;
		}

		virtual bool Do() override;

	private:
		OvrString parent_actor_unique_name;
		OvrString child_actor_unique_name;
	};

	class OVR_PROJECT_API OvrHotUpdateImportFile :public OvrCommandActor
	{
	public:
		OvrHotUpdateImportFile(){}
		virtual ~OvrHotUpdateImportFile(){}
		virtual bool Do() override;
		void SetFileList(std::list<OvrString> &a_file_list)
		{
			file_to_update.splice(file_to_update.end(), a_file_list);
		}
	private:
		std::list<OvrString> file_to_update;
	};
}

#endif
