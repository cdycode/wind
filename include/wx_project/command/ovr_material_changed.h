﻿#ifndef OVR_PROJECT_OVR_MATERIAL_CHANGED_H_
#define OVR_PROJECT_OVR_MATERIAL_CHANGED_H_

#include "ovr_command_manager.h"
#include <wx_base/wx_string.h>

namespace ovr_engine 
{
	class OvrMaterial;
	class OvrTextActor;
	class OvrGpuProgram;
	class OvrParticleActor;
}

namespace ovr_project 
{
	//修改Mesh
	class OVR_PROJECT_API OvrChangeMesh : public IOvrCommand
	{
	public:
		OvrChangeMesh(ovr_engine::OvrActor* a_actor) :changed_actor(a_actor) {}
		virtual ~OvrChangeMesh() {}

		virtual bool	Do() override;

		void	SetNewMesh(const OvrString& a_new_mesh_path) { new_mesh_path = a_new_mesh_path; }

	private:
		OvrString new_mesh_path;
		ovr_engine::OvrActor*	changed_actor = nullptr;
	};


	//修改材质
	class OVR_PROJECT_API OvrChangeMaterial : public IOvrCommand
	{
	public:
		OvrChangeMaterial(ovr_engine::OvrActor* a_actor) :changed_actor(a_actor) {}
		virtual ~OvrChangeMaterial() {}

		virtual bool	Do() override;

		void	SetNewMaterial(ovr::uint32 a_index, const OvrString& a_new_material_path);

	private:
		ovr_engine::OvrActor*	changed_actor = nullptr;

		ovr::uint32			material_index = 0;
		OvrString			new_material_path;
	};

	//修改材质中的纹理
	class OVR_PROJECT_API OvrMaterialChangeTexture : public IOvrCommand
	{
	public:
		OvrMaterialChangeTexture(ovr_engine::OvrMaterial* a_material) :material(a_material) {}
		virtual ~OvrMaterialChangeTexture() {}

		virtual bool	Do() override;

		void	SetTexture(const OvrString& a_texture_name, const OvrString& a_texture_file_path);

	private:
		ovr_engine::OvrMaterial*	material = nullptr;

		OvrString		texture_name;
		OvrString		texture_file_path;
	};

	class OVR_PROJECT_API OvrTextActorChangeTexture : public IOvrCommand 
	{
	public:
		OvrTextActorChangeTexture(ovr_engine::OvrTextActor* a_actor)
			:text_actor(a_actor)
		{}
		virtual ~OvrTextActorChangeTexture() {}

		void	SetTexture(const OvrString& a_texture_file_path) { texture_file_path = a_texture_file_path; }

		virtual bool	Do() override;

	private:
		ovr_engine::OvrTextActor*	text_actor = nullptr;
		OvrString					texture_file_path;
	};

	class OVR_PROJECT_API OvrSetMaterialShader : public IOvrCommand 
	{
	public:
		OvrSetMaterialShader() {}
		virtual ~OvrSetMaterialShader() {}

		void	SetShaderInfo(ovr_engine::OvrMaterial* a_material, ovr_engine::OvrGpuProgram* a_shader);

		virtual bool	Do() override;

	private:
		ovr_engine::OvrMaterial* material = nullptr;
		ovr_engine::OvrGpuProgram* shader = nullptr;
	};


	class OVR_PROJECT_API OvrParticleActorChangeTexture : public IOvrCommand
	{
	public:
		OvrParticleActorChangeTexture(ovr_engine::OvrParticleActor* a_actor)
			:particle_actor(a_actor)
		{}
		virtual ~OvrParticleActorChangeTexture() {}

		void	SetTexture(const OvrString& a_texture_file_path) { texture_file_path = a_texture_file_path; }

		virtual bool	Do() override;

	private:
		ovr_engine::OvrParticleActor*	particle_actor = nullptr;
		OvrString						texture_file_path;
	};

}

#endif
