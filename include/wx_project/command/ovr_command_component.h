﻿#pragma once

#include "ovr_command_manager.h"
#include "wx_core/ovr_scene_context.h"
#include <wx_engine/ovr_video_component.h>
#include <wx_engine/ovr_light_component.h>

namespace ovr_project 
{
	class OvrComponentCmdAdd : public IOvrCommand
	{
	public:
		OvrComponentCmdAdd(ovr_engine::OvrActor* a_actor, int a_component_type, const OvrString& a_param);
		virtual bool Do() override;

	protected:
		ovr_engine::OvrComponent* CreateMeshComponent(bool is_static);
		int component_type;
		OvrString param;
		ovr_engine::OvrActor* actor = nullptr;
	};

	class OvrComponentCmdDel : public IOvrCommand
	{
	public:
		OvrComponentCmdDel(ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_compnent);
		virtual bool Do() override;

	protected:
		ovr_engine::OvrActor* actor = nullptr;
		ovr_engine::OvrComponent* component = nullptr;

	};

	class OvrComponentCmdVideoChangeVideoType : public IOvrCommand
	{
	public:
		OvrComponentCmdVideoChangeVideoType(ovr_engine::OvrActor* a_actor, ovr_engine::OvrVideoComponent* a_compnent, int a_new_type)
		{
			actor = a_actor;
			component = a_compnent;
			new_type = a_new_type;
		}
		virtual bool Do() override;

	protected:
		ovr_engine::OvrActor* actor = nullptr;
		ovr_engine::OvrVideoComponent* component = nullptr;

		int new_type;
	};

	class OvrComponentCmdVideoChangeMovieType : public IOvrCommand
	{
	public:
		OvrComponentCmdVideoChangeMovieType(ovr_engine::OvrActor* a_actor, ovr_engine::OvrVideoComponent* a_compnent, int a_new_type)
		{
			actor = a_actor;
			component = a_compnent;
			new_type = a_new_type;
		}
		virtual bool Do() override;

	protected:
		ovr_engine::OvrActor* actor = nullptr;
		ovr_engine::OvrVideoComponent* component = nullptr;
		int new_type;
	};
}
