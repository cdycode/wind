﻿#ifndef OVR_PROJECT_OVR_PREVIEW_RENDER_SCENE_H_
#define OVR_PROJECT_OVR_PREVIEW_RENDER_SCENE_H_

#include <mutex>

namespace ovr_engine
{
	class OvrActor;
}

namespace ovr_core 
{
	class OvrSceneContext;
}

namespace ovr_project 
{
	
	class IOvrRenderContext;

	class OVR_PROJECT_API OvrCaptureMonitor
	{
	public:
		void setActor(ovr_engine::OvrActor* a_actor) { actor = a_actor; }
		void ChangeCaptureDevice(const OvrString& a_device_name) { device_name = a_device_name; }
		virtual bool UpdateCaptureDeviceData() = 0;

	protected:
		OvrString device_name;
		ovr_engine::OvrActor* actor = nullptr;
	};

	class OVR_PROJECT_API OvrPreviewScene
	{
	public:
		static OvrPreviewScene*	GetIns();
		static void				DelIns();

	public:
		bool	Init(IOvrRenderContext* a_context);
		void	Uninit();

		void		Reshape(ovr::int32 a_new_width, ovr::int32 a_new_height);
		ovr::int32	GetWidth()const { return render_width; }
		ovr::int32	GetHeight()const { return render_height; }

		void setMonitor(OvrCaptureMonitor* a_capture_monitor);
		void CreateActor(const OvrString& a_mesh_file_path);
		void ChangeCaptureDevice(const OvrString& a_device_name);

	protected:
		OvrPreviewScene() {}
		~OvrPreviewScene() {}
		void setActor(ovr_engine::OvrActor* a_actor);
		
	private:
		void WorkThread();
		void DoRenderScene();
		void SyncFrameTime(double a_last_frame_time, double& a_current_time);

	private:
		/*保证在Init函数返回时 渲染线程已经启动 并对OvrEngine进行初始化*/
		std::mutex					init_mutex; 
		std::condition_variable		init_condition;

		bool				is_working	= false;
		std::thread*		work_thread = nullptr;

		ovr_core::OvrSceneContext* current_scene = nullptr;

		bool is_device_changed = false;
		OvrString device_name;

		bool is_actor_changed = false;
		ovr_engine::OvrActor* actor = nullptr;

		OvrCaptureMonitor *capture_monitor = nullptr;

		bool		is_size_change = false;
		ovr::int32	render_width = 100;
		ovr::int32	render_height = 100;
		IOvrRenderContext*	render_context = nullptr;

		//当前的时间
		double current_time = 0.0;	
		//上帧开始渲染的时间
		double last_frame_time = 0.0;	

		//渲染时缺省的帧率
		const ovr::uint32 kEditorFPS = 30;
		const ovr::uint32 kPreviewFPS = 60;
		ovr::uint32	current_fps = kEditorFPS;
	};
}

#endif
