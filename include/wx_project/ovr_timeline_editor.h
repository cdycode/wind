﻿#ifndef OVR_PROJECT_OVR_TIMELINE_EDITOR_H_
#define OVR_PROJECT_OVR_TIMELINE_EDITOR_H_

#include "ovr_project.h"

namespace ovr_core 
{
	class OvrSequence;
	class OvrSceneContext;
}

namespace ovr_project 
{
	class OvrActorName
	{
	public:
		OvrActorName() {}
		OvrActorName(const OvrActorName& a_name)
			:display_name(a_name.display_name)
			, unique_name(a_name.unique_name)
		{}
		OvrActorName(const OvrString& a_display_name, const OvrString& a_unique_name)
			:display_name(a_display_name)
			, unique_name(a_unique_name) {}
		~OvrActorName() {}

		void operator = (const OvrActorName& a_name)
		{
			display_name = a_name.display_name;
			unique_name = a_name.unique_name;
		}

		OvrString		display_name;
		OvrString		unique_name;
	};

	class OvrSceneEditor;
	class OVR_PROJECT_API OvrTimelineEditor 
	{
	public:
		OvrTimelineEditor() {}
		~OvrTimelineEditor() {}

		bool	IsOpen() const { return (nullptr != sequence); }
		bool	Open(const OvrString& a_file_path);
		void	Close();

		void	Update();

		ovr_core::OvrSequence*	GetSequence() { return sequence; }
		const OvrString&		GetFilePath()const;

		void	SetRecord(bool a_is_record);
		bool	IsRecord()const;

		ovr::uint64			GetCurrentFrame()const;

		void				SetScene(OvrSceneEditor* a_scene);

	public:
		//获取可以添加到轨道上的Actor
		ovr::uint32					GetAddActorList(std::list<OvrActorName>& a_actor_name);
		ovr_core::OvrActorTrack*	AddActorTrack(const OvrString& a_actor_unique_name);
		ovr_core::OvrActorTrack*	GetActorTrack(const OvrString& a_actor_unique_name) { return nullptr; }
		bool						RmvActorTrack(const OvrString& a_actor_unique_name) { return false; }

	private:
		ovr_core::OvrSequence*	sequence = nullptr;

		OvrSceneEditor*			current_scene = nullptr;
	};
}

#endif
