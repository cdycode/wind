#ifndef OVR_PROJECT_OVR_SCENE_EVENT_MANAGER_H_
#define OVR_PROJECT_OVR_SCENE_EVENT_MANAGER_H_

#include "ovr_project.h"
#include <wx_base/wx_singleton.h>

namespace ovr_core 
{
	class OvrSceneContext;
}
namespace ovr_project 
{
	class OvrSceneEditor;
	class OVR_PROJECT_API OvrSceneEventManager
	{
	public:
		OvrSceneEventManager();
		~OvrSceneEventManager();

		void	Reshape(ovr::int32 a_new_width, ovr::int32 a_new_height);

		void	MouseMove(int a_x_pos, int a_y_pos);
		
		//关于鼠标按键相关
		void	MouseLeftButtonPressed(bool a_is_pressed);

		//关于拖拽相关函数
		void	BeginDragAsset(const char* a_asset_path);
		void	EndDragAsset(bool a_is_add);
		
		void	Update(OvrSceneEditor* a_scene);

		bool	IsPressed()const { return left_btn_pressed_current;}
		float	GetMousePosX()const { return mouse_x_pos; }
		float	GetMousePosY()const { return mouse_y_pos; }

	private:
		bool	IsResponse();
		bool	DealDragEvent(OvrSceneEditor* a_scene);
		void	DealEndDrag(OvrSceneEditor* a_scene);
	private:
		float		mouse_x_pos = 0;
		float		mouse_y_pos = 0;

		//选择相关事件
		float	pressed_begin_time = 0;
		bool	is_left_btn_changed = false;
		bool	left_btn_pressed_new = false;
		bool	left_btn_pressed_current = false;

		//拖拽事件相关
		bool		is_dragging = false;				//是否拖拽一个新的文件
		bool		is_drag_asset_file_current = false;	//在渲染线程中更新
		OvrString	drag_asset_file_path;
		bool		is_add_drag_asset = false;			//是否添加到场景中
		ovr_engine::OvrActor*	current_drag_actor = nullptr;

		//临时添加
		ovr::int32	render_width = 100;
		ovr::int32	render_height = 100;
	};
}

#endif
