﻿#ifndef OVR_PROJECT_OVR_ASSET_MANAGER_H_
#define OVR_PROJECT_OVR_ASSET_MANAGER_H_

#include <array>
#include <memory>
#include "wx_project/ovr_project.h"
#include <wx_asset_manager/ovr_archive_asset.h>

namespace ovr_engine 
{
	class OvrMaterial;
}

namespace ovr_project 
{
	class OvrAssetInfo
	{
	public:
		OvrAssetInfo() : asset_type(ovr_asset::kAssetInvalid) {}
		OvrAssetInfo(ovr::uint32 a_asset_type, OvrString a_file_path)
			:asset_type(a_asset_type)
			,file_path(a_file_path)
		{
		}

		OvrAssetInfo(const OvrAssetInfo& a_info) 
		{
			asset_type = a_info.asset_type;
			file_path = a_info.file_path;
		}
		~OvrAssetInfo() {}

	public:
		ovr::uint32		asset_type;
		OvrString		file_path;

		void operator=(const OvrAssetInfo& a_info) 
		{
			asset_type = a_info.asset_type;
			file_path = a_info.file_path;
		}
	};

	//工程文件信息
	class OvrProjectFileInfo 
	{
	public:
		OvrProjectFileInfo() {}
		OvrProjectFileInfo(const OvrProjectFileInfo& a_info) 
		{
			file_path	= a_info.file_path;
			unique_name = a_info.unique_name;
		}
		OvrProjectFileInfo(const OvrString& a_file_path, const OvrString& a_unique_name) 
		{
			file_path	= a_file_path;
			unique_name = a_unique_name;
		}
		
		void operator = (const OvrProjectFileInfo& a_info) 
		{
			file_path	= a_info.file_path;
			unique_name = a_info.unique_name;
		}
	public:
		OvrString		file_path;
		OvrString		unique_name;
	};

	//class OvrImporter;
	class OvrImportManager;
	class OVR_PROJECT_API OvrAssetManager
	{
	public:
		OvrAssetManager();
		~OvrAssetManager();

		bool	Open(const char* a_project_file_path);
		void	Close();

		const OvrString&		GetWorkDir()const { return work_dir; }

		bool	IsAssetExist(const char* a_asset_file);

		// arg: a_notify_callback
		// const char* 为当前操作文件名， bool 为 是否有冲突
		// 返回值：true ， 覆盖； false ,不覆盖；
		bool	ImportFiles(
			const std::vector<std::string>& a_asset_files,
			const char* a_out_dir,
			std::function<bool(const char*, bool)> &a_notify_callback);

		int		GetImportProgress();
		void	CancelImport();

		bool	DeleteAsset(const char* a_file);
		bool	RemoveSceneFile(const char* a_id) { return true; }
		bool	RemovePlotFile(const char* a_id) { return true; }
		bool	RemoveSequenceFile(const char* a_id) { return true; }

		static bool	CreateProject(const char* a_abs_file_path);

		ovr_engine::OvrMaterial* LoadMaterial(const char* a_file_path);
		bool	CreateMat(const char* a_file_path);
		bool	CreateSence(const char* a_file_path);
		bool	CreateSequcence(const char* a_scene_unique_name, const char* a_sequence_file_path);

		bool	GetPlotFilePath(const OvrString& a_unique_name, OvrString& a_file_path);
		bool	GetSequenceFilePath(const OvrString& a_unique_name, OvrString& a_file_path);
		bool	GetSceneFilePath(const OvrString& a_unique_name, OvrString& a_file_path);
		bool	GetAssetDisplayName(const OvrString& a_file_path, OvrString& a_display_name);

		bool	RenameAssetFile(const OvrString& a_src_path, const OvrString& a_new_path);
		bool	RenamePlotFilePath(const OvrString& a_unique_name, const OvrString& a_file_path);
		bool	RenameSequenceFilePath(const OvrString& a_unique_name, const OvrString& a_file_path);
		bool	RenameSceneFilePath(const OvrString& a_unique_name, const OvrString& a_file_path);

		void	AddPlotFile(const OvrProjectFileInfo& a_plot_file_info) { plot_list.push_back(a_plot_file_info); }
		void	AddSequenceFile(const OvrProjectFileInfo& a_sequence_file_info) { sequence_list.push_back(a_sequence_file_info); }
		void	AddSceneFile(const OvrProjectFileInfo& a_scene_file_info) { scene_list.push_back(a_scene_file_info); }
		void	AddAssetFile(const OvrAssetInfo& a_asset_file_info) { asset_list.push_back(a_asset_file_info); }
		void SetPlotFileList(const std::list<OvrProjectFileInfo>& a_asset_list) { plot_list = a_asset_list; }
		void SetSequenceFileList(const std::list<OvrProjectFileInfo>& a_asset_list) { sequence_list = a_asset_list; }
		void SetSceneFileList(const std::list<OvrProjectFileInfo>& a_asset_list) { scene_list = a_asset_list; }
		void SetAssetFileList(const std::list<OvrAssetInfo>& a_asset_list) { asset_list = a_asset_list; }
		const std::list<OvrProjectFileInfo>&	GetPlotFileList()const { return plot_list; }
		const std::list<OvrProjectFileInfo>&	GetSequenceFileList()const { return sequence_list; }
		const std::list<OvrProjectFileInfo>&	GetSceneFileList()const { return scene_list; }
		const std::list<OvrAssetInfo>&	GetAssetFileList()const { return asset_list; }

		const OvrProjectFileInfo*				GetSceneInfo(const OvrString& a_file_path)const;

		bool	GetAssetFileList(const char* a_dir, std::list<OvrAssetInfo>& a_asset_list);

	private:
		
		void AppendAsset(const ovr::uint32 a_asset_type, const OvrString& a_file_path);

		static void CopyShareFiles(const char* aDestDir, const char * aSrc);

	private:

		OvrString	work_dir;

		std::list<OvrProjectFileInfo>	plot_list;
		std::list<OvrProjectFileInfo>	sequence_list;
		std::list<OvrProjectFileInfo>	scene_list;
		std::list<OvrAssetInfo>	asset_list;
		
		OvrImportManager * import_mgr = nullptr;
		//OvrImporter* importer;
	};
}

#endif
