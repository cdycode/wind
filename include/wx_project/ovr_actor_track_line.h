﻿#ifndef OVR_PROJECT_OVR_ACTOR_TRACK_LINE_H_
#define OVR_PROJECT_OVR_ACTOR_TRACK_LINE_H_

#include <map>

#include "ovr_project.h"
#include <wx_base/wx_vector3.h>
#include <wx_base/wx_color_value.h>

namespace ovr_project
{
	class OvrSceneContext;

	class OVR_PROJECT_API OvrActorTrackLine
	{
	public:
		OvrActorTrackLine() {}
		~OvrActorTrackLine();

		void setTrackLine(OvrString a_actor_id, std::vector<OvrVector3>* a_lines, std::vector<OvrVector3>* a_points, int a_pt_selected = -1, const OvrColorValue& a_color = OvrColorValue::White);
		void setSelectPointColor(int a_pt_selected = -1, const OvrColorValue& a_color = OvrColorValue::White);
		void show(bool a_is_show) { is_show = a_is_show; }
		bool isShow() { return is_show; }

		bool checkValid();
		void draw();

	protected:
		void clear();

		OvrString actor_id;

		std::vector<OvrVector3>* lines = nullptr;
		std::vector<OvrVector3>* points = nullptr;
		bool is_show = true;
		int pt_select = -1;

		const float line_size = 1;
		const float pt_size = 5;
		const float sel_pt_size = 7;
		OvrColorValue pt_select_color;
	};
}

#endif
