﻿#pragma once

#include "i_ovr_actor_property.h"

namespace ovr_engine
{
	class OvrActor;
	class OvrComponent;
}

namespace ovr_project 
{
	enum OvrActorPropertyGroupType {
		kOPTGRoot, 
		kOPTGBase, kOPTGTrans, 
		kOPTGComponent = 100,
		kOPTGStaticMesh, kOPTGSkinMesh,
		kOPTGMat,
		kOPTGLight,kOPTGVideo, kOPTGText, kOPTGTextLayout,
		kOPTGCollideBox,  kOPTGCamera,
	};

	class OVR_PROJECT_API IOvrActorPropertyGroup : public IOvrActorProperty
	{
	public:
		static IOvrActorPropertyGroup* CreateGroup(int a_group_type, ovr_engine::OvrActor* a_actor = nullptr, ovr_engine::OvrComponent* a_compnent = nullptr);

		IOvrActorPropertyGroup(IOvrActorPropertyGroup* a_parent, OvrActorPropertyGroupType a_group_type);

		virtual ~IOvrActorPropertyGroup();

		void init(ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_compnent);

		virtual bool CanBeRemoved() { return false; }
		virtual void RemoveComponent() {}

		void  Reload();
		bool  IsNeedReload()	{ return is_need_reload || is_layout_changed; }
		bool  IsLayoutChanged() { return is_layout_changed; }

		ovr_engine::OvrActor*		GetActor() { return actor; }
		OvrActorPropertyGroupType	GetGroupType() { return group_type; }
		ovr_engine::OvrComponent*	getComponent() { return component; }

		IOvrActorProperty*					 GetProperty(const OvrString& a_id);
		const std::list<IOvrActorProperty*>& GetProperties() { return property_list; }


	protected:
		void OnPropertyChange(IOvrActorProperty* a_property);

		void SetNeedReload(bool a_flag) { is_need_reload = a_flag; }
		virtual void DoReload() {}

		virtual void DoPropertyChange(IOvrActorProperty* a_property) {}
		virtual void InitValue(void*) override {}

		//从Actor加载属性
		virtual void OnInit() {}
		virtual void OnInitWithNoComponent() {}

		//应用Group的所有属性到Actor
		virtual void onApply() {}

		IOvrActorProperty* AddProperty(IOvrActorProperty* a_property);
		void clear();

		OvrActorPropertyGroupType  group_type;

		ovr_engine::OvrActor*		actor = nullptr;
		ovr_engine::OvrComponent* component = nullptr;

		bool is_need_reload = false;
		bool is_layout_changed = false;

		//属性
		std::list<IOvrActorProperty*> property_list;
		std::map<OvrString, IOvrActorProperty*> property_map;

		friend class IOvrActorProperty;
	};
}
