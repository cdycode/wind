﻿#pragma once

#include "i_ovr_actor_property_group.h"

#include <list>
#include <map>
#include <vector>

namespace ovr_project 
{
	class OvrActorPropertyComboBox;

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupRoot

	class OVR_PROJECT_API OvrActorPropertyGroupRoot : public IOvrActorPropertyGroup
	{
	public:
		static const OvrActorPropertyGroupRoot* GetSupportPropertyGroups();
		static OvrActorPropertyGroupRoot* CreatePropertyGroups(ovr_engine::OvrActor* a_actor);

		~OvrActorPropertyGroupRoot();

		//添加一个已有的group
		void AddGroup(IOvrActorPropertyGroup* a_group);

		//创建一个group并添加
		void AddGroup(int a_group_type);

		//删除一个group
		bool RemoveGroup(int a_group_type);

		//删除某个包含指定组件的group 
		bool RemoveGroupIncludeComponent(int a_com_type);

		//删除某个指定类型的group中的组件
		void RemoveComponent(int a_group_type);

		//group的获取和查找
		const std::list<IOvrActorPropertyGroup*>& GetGroups() const { return group_list; }
		bool Contains(int a_group_type);
		int  Index(int a_group_type);
		IOvrActorPropertyGroup* GetGroup(int a_group_type);

		//创建一个适配指定group的组件
		void createComponent(int a_group_type, const OvrString& a_param);

		//创建并添加一个合适的group,用于管理指定的组件 
		IOvrActorPropertyGroup* addGroupForComponent(ovr_engine::OvrComponent* a_component);

		//某个group包含什么类型的组件
		int componentTypeInGroup(int a_group_type);

		//某个组件被什么类型的group包含
		int groupTypeIncludeComponent(int a_com_type);

	protected:
		OvrActorPropertyGroupRoot(IOvrActorPropertyGroup* a_parent = nullptr);

		std::list<IOvrActorPropertyGroup*> group_list;
		std::map<int, IOvrActorPropertyGroup*> group_map;
	};	

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupFun

	class OVR_PROJECT_API OvrActorPropertyGroupFun : public IOvrActorPropertyGroup
	{
	public:
		OvrActorPropertyGroupFun(IOvrActorPropertyGroup* a_parent, OvrActorPropertyGroupType a_group_type) : IOvrActorPropertyGroup(a_parent, a_group_type) {}

		IOvrActorProperty* AddPropertyComboBox(const OvrString& a_id, const OvrString& a_name, const std::vector<OvrDataItemInt>& a_items, void* a_value = nullptr, bool a_update_flag = false);

		virtual void RemoveComponent() override;

		virtual bool CanBeRemoved() override;

	protected:
		int getComboxValue(IOvrActorProperty* a_property);
		OvrString getComboxValueTitle(IOvrActorProperty* a_property, int a_data);
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupBase

	class OVR_PROJECT_API OvrActorPropertyGroupBase : public OvrActorPropertyGroupFun
	{
	public:
		OvrActorPropertyGroupBase(IOvrActorPropertyGroup* a_parent = nullptr);

	protected:
		virtual void OnInitWithNoComponent() override;

		virtual void DoPropertyChange(IOvrActorProperty* a_property) override;
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupTrans

	class OVR_PROJECT_API OvrActorPropertyGroupTrans : public OvrActorPropertyGroupFun
	{
	public:
		OvrActorPropertyGroupTrans(IOvrActorPropertyGroup* a_parent = nullptr);

		virtual void DoReload() override;

	protected:
		virtual void OnInitWithNoComponent() override;
		virtual void DoPropertyChange(IOvrActorProperty* a_property) override;
		void InitValue();
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupMesh

	class OVR_PROJECT_API OvrActorPropertyGroupStaticMesh : public OvrActorPropertyGroupFun
	{
	public:
		OvrActorPropertyGroupStaticMesh(IOvrActorPropertyGroup* a_parent = nullptr, OvrActorPropertyGroupType a_group_type = kOPTGStaticMesh);

	protected:
		virtual void OnInit() override;
		virtual void DoPropertyChange(IOvrActorProperty* a_property) override;
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupSkinMesh

	class OVR_PROJECT_API OvrActorPropertyGroupSkinMesh : public OvrActorPropertyGroupStaticMesh
	{
	public:
		OvrActorPropertyGroupSkinMesh(IOvrActorPropertyGroup* a_parent = nullptr);

	protected:
		virtual void OnInit() override;
		virtual void DoPropertyChange(IOvrActorProperty* a_property) override;
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupMat

	class OVR_PROJECT_API OvrActorPropertyGroupMat : public OvrActorPropertyGroupFun
	{
	public:
		OvrActorPropertyGroupMat(IOvrActorPropertyGroup* a_parent = nullptr);

	protected:
		virtual void OnInit() override;
		virtual void DoPropertyChange(IOvrActorProperty* a_property) override;
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupLight

	class OVR_PROJECT_API OvrActorPropertyGroupLight : public OvrActorPropertyGroupFun
	{
	public:
		OvrActorPropertyGroupLight(IOvrActorPropertyGroup* a_parent = nullptr);

	protected:
		virtual void OnInit() override;
		virtual void DoPropertyChange(IOvrActorProperty* a_property) override;
		void		 InitContent(int a_light_type_id);
		void		 InitValue(int a_light_type_id);
		virtual void DoReload() override;
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupVideo

	class OVR_PROJECT_API OvrActorPropertyGroupVideo : public OvrActorPropertyGroupFun
	{
	public:
		OvrActorPropertyGroupVideo(IOvrActorPropertyGroup* a_parent = nullptr);

	protected:
		virtual void OnInit() override;
		virtual void DoPropertyChange(IOvrActorProperty* a_property) override;
		virtual void DoReload() override;

		void InitContent(int a_video_type_id);
		void InitContent2D();
		void InitContent3D();
		void InitContent180();
		void InitContent360();

		void InitValue(int a_video_type_id);
		void InitValue2D();
		void InitValue3D();
		void InitValue180();
		void InitValue360();

		enum DisplayMode { kDMFull, kDMRatio, kDMPano, kDMCube };
		int GetDisplayMode(int a_video_mode, int a_movie_type);

		enum SaveFormat	{ kSF2D, kSFTD, KSFLR};
		int GetSaveFormat(int a_video_mode, int a_movie_type);

		bool IsFullResolution(int a_video_mode, int a_movie_type);

		int GetMovieTypeFor2D(std::vector<int>& a_movie_types);
		int GetMovieTypeFor3D(std::vector<int>& a_movie_types);
		int GetMovieTypeFor180(std::vector<int>& a_movie_types);
		int GetMovieTypeFor360(std::vector<int>& a_movie_types);
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupText

	class OVR_PROJECT_API OvrActorPropertyGroupText : public OvrActorPropertyGroupFun
	{
	public:
		OvrActorPropertyGroupText(IOvrActorPropertyGroup* a_parent = nullptr);

	protected:
		virtual void OnInit() override;
		virtual void DoPropertyChange(IOvrActorProperty* a_property) override;
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupTextLayout

	class OVR_PROJECT_API OvrActorPropertyGroupTextLayout : public OvrActorPropertyGroupFun
	{
	public:
		OvrActorPropertyGroupTextLayout(IOvrActorPropertyGroup* a_parent = nullptr);

	protected:
		virtual void OnInit() override;
		virtual void DoPropertyChange(IOvrActorProperty* a_property) override;
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupCollideBox

	class OVR_PROJECT_API OvrActorPropertyGroupCollideBox : public OvrActorPropertyGroupFun
	{
	public:
		OvrActorPropertyGroupCollideBox(IOvrActorPropertyGroup* a_parent = nullptr);

	protected:
		virtual void OnInit() override;
		virtual void DoPropertyChange(IOvrActorProperty* a_property) override;
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyGroupCamera

	class OVR_PROJECT_API OvrActorPropertyGroupCamera : public OvrActorPropertyGroupFun
	{
	public:
		OvrActorPropertyGroupCamera(IOvrActorPropertyGroup* a_parent = nullptr);

	protected:
		virtual void OnInit() override;
		virtual void DoPropertyChange(IOvrActorProperty* a_property) override;
	};
}
