﻿#pragma once

#include "i_ovr_actor_property.h"
#include "wx_base/wx_vector2.h"
#include "wx_base/wx_vector3.h"
#include "wx_base/wx_vector4.h"

#include <vector>

namespace ovr_project 
{
	///////////////////////////////////////////////
	// class OvrActorPropertyBool

	class OVR_PROJECT_API OvrActorPropertyBool : public IOvrActorProperty
	{
	public:
		OvrActorPropertyBool(void* a_value = nullptr, IOvrActorPropertyGroup* a_parent = nullptr);
		virtual void InitValue(void* a_value) override;

		bool GetValue() { return value; }
		bool SetValue(bool a_value);

	protected:

		bool value = false;
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyFloat

	class OVR_PROJECT_API OvrActorPropertyFloat : public IOvrActorProperty
	{
	public:
		OvrActorPropertyFloat(void* a_value = nullptr, IOvrActorPropertyGroup* a_parent = nullptr);
		virtual void InitValue(void* a_value) override;

		float GetValue() { return value; }
		bool  SetValue(float a_value);

		void  SetRange(float a_min_value, float a_max_value);
		void  GetRange(float& a_min_value, float& a_max_value);

	protected:
		float value = false;

		bool is_limit_range = false;
		float min_value = 0;
		float max_value = 1;
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyVector2f

	class OVR_PROJECT_API OvrActorPropertyVector2f : public IOvrActorProperty
	{
	public:
		OvrActorPropertyVector2f(void* a_value = nullptr, IOvrActorPropertyGroup* a_parent = nullptr);
		virtual void InitValue(void* a_value) override;

		const OvrVector2& GetValue() { return value; }
		bool  SetValue(const OvrVector2& a_value);

		void  SetFormat(const OvrString& a_format) { format = a_format; }
		const OvrString& GetFormat() { return format; }

	protected:
		OvrVector2 value;
		OvrString  format;
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyVector3f

	class OVR_PROJECT_API OvrActorPropertyVector3f : public IOvrActorProperty
	{
	public:
		OvrActorPropertyVector3f(void* a_value = nullptr, IOvrActorPropertyGroup* a_parent = nullptr);
		virtual void InitValue(void* a_value) override;

		const OvrVector3& GetValue() { return value; }
		bool  SetValue(const OvrVector3& a_value);

		void  SetFormat(const OvrString& a_format) { format = a_format; }
		const OvrString& GetFormat() { return format; }

	protected:
		OvrVector3 value;
		OvrString  format;
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyVector4f

	class OVR_PROJECT_API OvrActorPropertyVector4f : public IOvrActorProperty
	{
	public:
		OvrActorPropertyVector4f(void* a_value = nullptr, IOvrActorPropertyGroup* a_parent = nullptr);
		virtual void InitValue(void* a_value) override;

		const OvrVector4& GetValue() { return value; }
		bool  SetValue(const OvrVector4& a_value);

		void  SetFormat(const OvrString& a_format) { format = a_format; }
		const OvrString& GetFormat() { return format; }

	protected:
		OvrVector4 value;
		OvrString  format;
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyString

	class OVR_PROJECT_API OvrActorPropertyString : public IOvrActorProperty
	{
	public:
		OvrActorPropertyString(void* a_value = nullptr, IOvrActorPropertyGroup* a_parent = nullptr);
		virtual void InitValue(void* a_value) override;

		const OvrString& GetValue() { return value; }
		bool  SetValue(const OvrString& a_value);

		bool  IsText() { return is_text; }
		void  SetTextFlag(bool a_is_text) { is_text = a_is_text; }

	protected:
		OvrString value;
		bool is_text = false;
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyRes

	class OVR_PROJECT_API OvrActorPropertyRes : public IOvrActorProperty
	{
	public:
		OvrActorPropertyRes(void* a_value = nullptr, IOvrActorPropertyGroup* a_parent = nullptr);
		virtual void InitValue(void* a_value) override;

		const OvrString& GetValue() { return value; }
		bool  SetValue(const OvrString& a_value);

	protected:
		OvrString value;
	};

	///////////////////////////////////////////////
	// class OvrActorPropertyComboBox

	class OVR_PROJECT_API OvrActorPropertyComboBox : public IOvrActorProperty
	{
	public:
		OvrActorPropertyComboBox(void* a_value = nullptr, IOvrActorPropertyGroup* a_parent = nullptr);
		virtual void InitValue(void* a_value) override;

		int	  GetValue() { return item_id; }
		bool  SetValue(int a_value);	
		void  SetItems(const std::vector<OvrDataItemInt>& a_items) { items = a_items; }
		const std::vector<OvrDataItemInt>& GetItems() { return items; }
		
	protected:
		int item_id = -1;
		std::vector<OvrDataItemInt> items;
	};

}
