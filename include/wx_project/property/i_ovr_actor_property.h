﻿#pragma once

#include "../ovr_project.h"

#include <map>

namespace ovr_project 
{
	enum OvrActorPropertyType {
		kOPTBool, kOPTFloat, 
		kOPTVector2f, kOPTVector3f, kOPTVector4f,
		kOPTString, kOPTComboBox, kOPTRes, kOPTGroup
	};

	class IOvrActorPropertyGroup;

	class OVR_PROJECT_API IOvrActorProperty
	{
	public:
		static IOvrActorProperty* Create(OvrActorPropertyType a_type, const OvrString& a_id, const OvrString& a_name, void* a_value = nullptr, IOvrActorPropertyGroup* a_parent = nullptr);
		
		OvrString				property_id;
		OvrString				property_name;
		OvrActorPropertyType	property_type;
		OvrString				property_type_name;

		IOvrActorPropertyGroup*	parent = nullptr;

		IOvrActorProperty(IOvrActorPropertyGroup* a_parent, OvrActorPropertyType a_property_type) { parent = a_parent; property_type = a_property_type; }
		virtual ~IOvrActorProperty();

		OvrString GetAttribute(const OvrString& a_key);
		void	  SetAttribute(const OvrString& a_key, const OvrString& a_value);

		const std::map<OvrString, OvrString>& GetAttributes() { return atts; }

		virtual void InitValue(void* a_value) = 0;

		bool IsNeedUpdateGroup() { return is_need_update_group; }
		void SetNeedUpdateGroup(bool a_flag) { is_need_update_group = a_flag; }

		bool IsAsyncLoad() { return is_async_load; }
		void SetAsyncLoad(bool a_flag) { is_async_load = a_flag; }

	protected:
		void OnValueChanged();

		std::map<OvrString, OvrString> atts;
		bool is_need_update_group = false;
		bool is_async_load = false;
	};

	struct OvrDataItemInt
	{
		int item_id;
		OvrString item_name;

		const OvrDataItemInt& operator = (const OvrDataItemInt& a_item) {
			item_id = a_item.item_id; item_name = a_item.item_name;
			return *this;
		}
	};
}
