﻿#ifndef OVR_PROJECT_I_OVR_MATERIAL_PROPERTY_H_
#define OVR_PROJECT_I_OVR_MATERIAL_PROPERTY_H_

#include "../ovr_project.h"

namespace ovr_project 
{
	//定义Node的参数
	struct OVR_PROJECT_API OvrMaterialParam
	{
	public:
		enum Type
		{
			kInvalid = 0,
			kInt,		//整数
			kFloat,		//float
			kVector3,	//OvrVector3 float  以冒号隔开的字符 比如：  0.0:0.0:0.0
			kVector4,	//OvrVector3 float  以冒号隔开的字符 比如：  0.0:0.0:0.0:0.0
			kTexture,
			kEnum,		//枚举，枚举项目采用字符串列表方式 选项之间使用:隔开 存放在reserve中
			kColor,		//rgba OvrVector4
		};

	public:
		OvrMaterialParam() {}
		OvrMaterialParam(const OvrMaterialParam& a_param) 
		{
			display_name = a_param.display_name;
			gpu_name = a_param.gpu_name;
			type = a_param.type;
			value = a_param.value;
			reserve = a_param.reserve;
		}
		OvrMaterialParam(const OvrString& a_gpu_name, Type a_type)
			:gpu_name(a_gpu_name)
			, type(a_type)
		{}
		OvrMaterialParam(const OvrString& a_gpu_name, Type a_type, const OvrString& a_value)
			:gpu_name(a_gpu_name)
			, type(a_type)
			, value(a_value)
		{}

		void operator = (const OvrMaterialParam& a_param)
		{
			display_name	= a_param.display_name;
			gpu_name		= a_param.gpu_name;
			type			= a_param.type;
			value			= a_param.value;
			reserve			= a_param.reserve;
		}

		OvrString				display_name;			// 显示名
		OvrString				gpu_name;		// shader中的名字
		Type					type = kInvalid;// 值的类型
		OvrString				value;			// 真实的值
		OvrString				reserve;		// 备选取值列表
	};

	class OVR_PROJECT_API IOvrMaterialProperty
	{
	public:
		static IOvrMaterialProperty*	Create(const OvrString& a_material_file_path);
		static void						Destory(IOvrMaterialProperty* a_ins);

	public:
		IOvrMaterialProperty() {}
		virtual ~IOvrMaterialProperty() {}

		//获取材质种类
		void				GetTypeList(std::list<OvrString>& a_name_list);
		const OvrString&	GetType()const { return current_type; }
		//获取参数列表
		const std::list<OvrMaterialParam>&	GetParamList() { return param_list; }
		// 当前 材质 路径
		const OvrString& GetMatPath()const { return material_file_path; }


		//设置材质类型
		virtual bool	SetType(const OvrString& a_name) = 0;
		
		//设置参数值
		virtual bool	SetParamValue(const OvrString& a_name, const OvrString& a_value) = 0;

		virtual bool	Save() = 0;

	protected:
		//获取参数信息
		OvrMaterialParam*	GetParam(const OvrString& a_display_name);

	protected:
		OvrString					material_file_path;
		OvrString					current_type;
		std::list<OvrMaterialParam> param_list;
	};
}

#endif
