﻿#ifndef OVR_PROJECT_OVR_PROJECT_H_
#define OVR_PROJECT_OVR_PROJECT_H_

#ifdef WXPROJECT_EXPORTS
#	define OVR_PROJECT_API __declspec(dllexport)
#else
#	define OVR_PROJECT_API __declspec(dllimport)
#endif // OVRENGINE_EXPORTS

#include <list>
#include <wx_base/wx_type.h>
#include <wx_base/wx_vector3.h>
#include <wx_base/wx_string.h>
#include <wx_core/ovr_core.h>

class OvrArchive;

namespace ovr_engine 
{
	class OvrActor;
	class OvrComponent;
}

namespace ovr_core 
{
	class OvrSequence;
	class OvrSceneContext;
}

namespace ovr_project 
{
	enum OvrAxisType
	{
		kAxisRotation = 0,
		kAxisTransform,
		kAxisScale,
	};

	class OvrSceneListener
	{
	public:
		virtual void OnAddActor(const OvrString& a_actor_unique_name) = 0;
		virtual void OnRmvActor(const OvrString& a_actor_unique_name) = 0;

		virtual void OnComponentAdd(ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_component) = 0;
		virtual void OnComponentDel(ovr_engine::OvrActor* a_actor, int a_com_type) = 0;

		virtual void OnSetSelectedItem(const char* selected_id) = 0;
		virtual void OnUpdateTransform(const char* selected_id) = 0;

		virtual void OnComponentChangeVideoType(ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_component) = 0;
		virtual void OnComponentChangeMovieType(ovr_engine::OvrActor* a_actor, ovr_engine::OvrComponent* a_component) = 0;
	};

	class IPlayerNotify
	{
	public:
		IPlayerNotify() {}
		virtual ~IPlayerNotify() {}

		//通知当前的时间  只有在播放状态时才通知， 游标将根据此值修改显示位置
		virtual void OnPlayPosition(ovr::uint64 a_frame_index) = 0;

		//通知状态变化  在UI层 Seek也按暂停处理
		virtual void OnPlayStatus(ovr_core::OvrRenderStatus a_new_status) = 0;

		virtual void OnOpenScene(ovr_core::OvrSceneContext* a_new_scene) = 0;
		virtual void OnOpenSequence(ovr_core::OvrSequence* a_new_sequence) = 0;
	};

	/************************************************************************/
	/* QT中有自己的OpenGLContext                                             */
	/* 渲染线程又要放到本工程中，故提供此类，由QT实现其子类，					*/
	/* 渲染线程通过接口对QT的OpenGLContext进行初始化							*/
	/************************************************************************/
	class IOvrRenderContext
	{
	public:
		IOvrRenderContext() {}
		virtual ~IOvrRenderContext() {}

		//初始化和反初始化 分别在渲染线程启动和退出时调用
		virtual bool	OnInit() = 0;
		virtual void	OnUninit() = 0;

		//每帧都调用
		virtual bool	OnRender() = 0;

		//判断是否有效
		virtual bool	IsValid() = 0;

		virtual ovr::uint32	GetWidth() = 0;
		virtual ovr::uint32	GetHeight() = 0;
	};

	class OvrAssetManager;
	class OvrCommandManager;
	class OvrRenderScene;
	class OvrSceneEditor;
	class OvrTimelineEditor;
	class OVR_PROJECT_API OvrProject
	{
	public:
		static OvrProject* GetIns();
		static void DelIns();

	public:
		OvrProject();
		~OvrProject();

		bool	Init();
		void	Uninit();

		//全局路径+文件名
		bool	Create(const char* a_project_file_path, IOvrRenderContext* a_context, IPlayerNotify* a_notify);
		bool	Open(const char* a_project_file_path, IOvrRenderContext* a_context, IPlayerNotify* a_notify);
		void	Close();

		void	SetListener(OvrSceneListener* a_listener) { scene_listener = a_listener; }
		OvrSceneListener*	GetListener() { return scene_listener; }

		OvrAssetManager*		GetAssetManager();
		const OvrString&		GetWorkDir()const;
		const OvrString&		GetFilePath()const { return file_path; }

		void	ResizeRenderWindow(int a_new_width, int a_new_height);
		
		// add modify and delete actor , track and set attribute not setting yet.
		bool	IsNeedSave()const { return true;  need_saveed; }
		bool	Save();
		bool	SaveAll();

		void						OpenSence(const char* a_unique_name);
		void						OpenSceneFromFile(const char* a_file_path);
		OvrSceneEditor*				GetCurrentSceneEditor();
		ovr_core::OvrSceneContext*	GetCurrentScene();
		void						CloseSence();

		void					OpenSequence(const char* a_unique_name);
		void					OpenSequenceFromFile(const char* a_file_path);
		OvrTimelineEditor*		GetCurrentTimelineEditor();
		ovr_core::OvrSequence*	GetCurrentSequence();
		void			CloseSequence();

		OvrCommandManager*	GetCommandManager();
		OvrRenderScene*		GetPlayer() { return render_thread; }

	private:
		OvrString					absolute_file_path;
		OvrString					file_path;

		OvrAssetManager*			asset_manager = nullptr;
		OvrRenderScene*				render_thread = nullptr;

		bool need_saveed			= false;

		OvrSceneListener*	scene_listener = nullptr;
	};
}


#endif
