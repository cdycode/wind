﻿#ifndef OVR_PROJECT_OVR_SCENE_EDITOR_H_
#define OVR_PROJECT_OVR_SCENE_EDITOR_H_

#include "ovr_project.h"
#include <wx_project/ovr_actor_track_line.h>

namespace ovr_core 
{
	class OvrSceneContext;
}

namespace ovr_project 
{
	class OvrIndicator;
	class OvrWorldAxisActor;
	class OvrSceneEventManager;
	class OVR_PROJECT_API OvrSceneEditor
	{
	public:
		enum Mode
		{
			kEditor,	//编辑模式
			kPreview,	//预览模式
		};
	public:
		OvrSceneEditor() {}
		~OvrSceneEditor() {}

		bool	IsOpen() const { return (nullptr != scene_context); }
		bool	Open(const OvrString& a_file_path);
		void	Close();

		void	SetMode(Mode a_mode);
		Mode	GetMode()const { return current_mode; }

		const OvrString&	GetFilePath()const;

		ovr_core::OvrSceneContext*	GetSceneContext() { return scene_context; }
		OvrSceneEventManager*		GetEventManager() { return event_manager; }
		OvrActorTrackLine&		GetActorTrackLine() { return actor_track_line; }

		void	Update();
		void	Draw();

		void	SetActorAixsType(OvrAxisType a_type);
		void	SelectedActor(float a_x_pos, float a_y_pos);
		bool	SetSelectedActor(ovr_engine::OvrActor* a_new_selected_actor);
		ovr_engine::OvrActor*	GetSelector() { return selected_actor; }
		
		void	RemoveActor(const OvrString& a_actor_unique_name);
		void	RemoveActor(ovr_engine::OvrActor* a_actor);

		ovr_engine::OvrActor*	GetActorByUniqueName(const OvrString& a_unique_name);

		bool	GetCurrentPickRay(OvrRay3f& a_ray);

		bool	IsShowInScene(ovr_engine::OvrActor* a_actor);

	private:
		bool	GeneratePickRay(float a_x_pos, float a_y_pos, OvrRay3f& a_ray);
		void	CheckSelectedActor(ovr_engine::OvrActor* a_actor);

	private:
		Mode						current_mode = kEditor;

		ovr_core::OvrSceneContext*	scene_context = nullptr;
		OvrSceneEventManager*		event_manager = nullptr;

		OvrIndicator*				indicator = nullptr;
		OvrWorldAxisActor*			world_axis = nullptr;

		ovr_engine::OvrActor*		selected_actor = nullptr;

		//绘制Actor的轨迹线
		OvrActorTrackLine			actor_track_line;
	};
}

#endif

