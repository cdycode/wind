﻿#ifndef OVR_PROJECT_OVR_RENDER_SCENE_H_
#define OVR_PROJECT_OVR_RENDER_SCENE_H_

#include "ovr_project.h"
#include <thread>
#include <wx_core/ovr_core.h>

namespace ovr_project 
{
	class OvrDelayStatus
	{
	public:
		OvrDelayStatus(ovr_core::OvrRenderStatus a_vaule)
		{
			current_status = a_vaule;
			new_status = a_vaule;
		}

		void	SetStatus(ovr_core::OvrRenderStatus a_new_status)
		{
			if (new_status == ovr_core::kRenderSeek)
			{
				is_seek = true;
			}
			new_status = a_new_status;
			is_changed = true;
		}

		void operator = (ovr_core::OvrRenderStatus a_new_status) { SetStatus(a_new_status); }
		bool operator == (ovr_core::OvrRenderStatus a_new_status) { return current_status == a_new_status; }
		bool operator != (ovr_core::OvrRenderStatus a_new_status) { return current_status != a_new_status; }

		ovr_core::OvrRenderStatus	GetStatus()const { return current_status; }
		bool IsChanged()const { return is_changed; }
		void UpdateStatus()
		{
			if (!is_changed)
			{
				return;
			}

			if (new_status == ovr_core::kRenderSeek)
			{
				//seek状态 就直接设置
				current_status = new_status;
				is_seek = false;
				is_changed = false;
			}
			else
			{
				//非Seek状态
				if (!is_seek)
				{
					//未覆盖到Seek
					current_status = new_status;
					is_changed = false;
				}
				else
				{
					//说明将Seek状态覆盖了  待下次再恢复到新状态
					current_status = ovr_core::kRenderSeek;
					is_seek = false;
				}
			}
		}
	private:
		bool						is_changed = false;
		bool						is_seek = false;		//seek比较特殊  解决Seek后理解播放的问题  避免Seek状态丢失
		ovr_core::OvrRenderStatus	current_status;
		ovr_core::OvrRenderStatus	new_status;
	};

	class OvrSceneEditor;
	class OvrTimelineEditor;
	class IOvrRenderContext;
	class FrameBase;
	class OvrCommandManager;
	class OVR_PROJECT_API OvrRenderScene
	{
	public:
		OvrRenderScene();
		~OvrRenderScene();

	public:
		bool	Init(IOvrRenderContext* a_context, IPlayerNotify* a_notify);
		void	Uninit();

		void		Reshape(ovr::int32 a_new_width, ovr::int32 a_new_height);
		ovr::int32	GetWidth()const { return render_width; }
		ovr::int32	GetHeight()const { return render_height; }

		//相对文件路径
		void				OpenScene(const OvrString& a_scene_file);
		void				CloseScene();
		OvrSceneEditor*		GetCurrentScene() { return current_scene; }

		void			OpenSequence(const OvrString& a_sequence_file);
		void			CloseSequence();
		OvrTimelineEditor*	GetCurrentSequence() { return current_sequence; }

		OvrCommandManager*	GetCommandManager() { return command_manager; }

		/************************************************************************/
		/* 播放状态控制                                                          */
		/************************************************************************/
		void	Play();
		void	Pause();
		void	Seek(ovr::uint64 a_seek_frame_index);
		void	SetRecord(bool a_is_record);
		bool	IsRecord()const;

		ovr::uint64					GetCurrentFrame()const;
		ovr_core::OvrRenderStatus	GetCurrentStatus()const { return render_status.GetStatus(); }

	private:
		void	WorkThread();
		void	DoStatusChanged();
		void	DoModeEmpty();
		
		void	SyncFrameTime(double a_last_frame_time, double& a_current_time);
		

		void	DoOpenScene();
		void	DoCloseScene();

		void	DoOpenSequence();
		void	DoCloseSequence();

		bool	IsUpdateSequence();
	private:
		
		bool				is_working	= false;
		std::thread*		work_thread = nullptr;
		IOvrRenderContext*	render_context = nullptr;
		IPlayerNotify*		player_notify = nullptr;	//通知上层当前播放位置

		bool		is_size_change = false;
		ovr::int32	render_width = 100;
		ovr::int32	render_height = 100;
		
		
		double					current_time = 0.0;	//当前的时间
		double					last_frame_time = 0.0;	//上帧开始渲染的时间
		ovr::uint32				current_fps = preview_fps;
																//渲染时缺省的帧率
		static const ovr::uint32	editor_fps = 30;
		static const ovr::uint32	preview_fps = 60;

		bool						is_open_new_scene = false;
		OvrString					new_scene_file_path;
		OvrSceneEditor*				current_scene = nullptr;

		bool						is_open_new_sequence = false;
		OvrString					new_sequence_file_path;
		OvrTimelineEditor*			current_sequence = nullptr;
		//ovr_core::OvrSequence*				current_sequence = nullptr;

		OvrCommandManager*			command_manager = nullptr;

		//sequence的帧率控制
		double					next_frame_time = 0.0;
		ovr::uint32				sequence_fps = editor_fps;


		bool			is_vr_mode = false;	//是否进行VR模式播放

		ovr::uint64		seek_frame_index = 0;	//用于保存Seek时起始时间戳， 待在渲染线程中更新到current_frame_index

		OvrDelayStatus					render_status;
	};
}

#endif
