﻿#ifndef OVR_FBX_IMPORT_OVR_FBX_IMPORT_H_
#define OVR_FBX_IMPORT_OVR_FBX_IMPORT_H_

#include <list>
#include <wx_base/wx_string.h>
#ifdef WXFBXIMPORT_EXPORTS
#	define FBX_IMPORT_API __declspec(dllexport)
#else
#	define FBX_IMPORT_API __declspec(dllimport)
#endif // OVRFBXIMPORT_EXPORTS

class OvrFbxParser;
class FBX_IMPORT_API OvrFbxImporter
{
public:
	OvrFbxImporter();
	~OvrFbxImporter();

	bool		Import(const char* a_fbx_file, const char* a_out_dir, std::list<OvrString>& a_out_file_list);
	ovr::uint32 GetPercent();
	void		CancelImport();
private:
	OvrFbxParser*	parser;
	bool			is_import;
};

#endif
