﻿#ifndef OVR_FBX_IMPORT_OVR_IMAGE_IMPORT_H_
#define OVR_FBX_IMPORT_OVR_IMAGE_IMPORT_H_

#include "ovr_fbx_import.h"

class FBX_IMPORT_API OvrImageImporter
{
public:
	OvrImageImporter() {}
	~OvrImageImporter() {}

	/************************************************************************/
	/* a_file:绝对路径														*/
	/* a_out_dir:相对工作目录的目录											*/
	/* a_out_file:输出的文件 相对工作目录的路径								*/
	/************************************************************************/
	bool		Import(const char* a_file, const char* a_out_dir, OvrString& a_out_file);
	void		SaveTextureFromLoad(const char* a_loadfilepath,const char*a_loadfilename,const char* a_savefile, bool a_balpha, int a_width, int a_height);
};

#endif
