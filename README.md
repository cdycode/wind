# wind

#### 项目介绍
VR内容制作引擎

#### 软件架构
软件架构说明

#### 编译
1.目前只提供 Visual Studio community 2015编译工程 工程文件为：build/vs2015/Wind.sln  
2.需要配置QT目录 目前的QT版本为QT5.8.0  
3.目前只支持x86 debug  
4.可执行工程为：wxEditor  
5.fbxsdk采用静态库，因为二进制文件较大，压缩成third_party/lib/libfbx.zip,编译前需要解压到当前目录  
6.运行前请配置工作目录为：..\..\bin\ 


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

#### 联系方式
有任何意见和建议，请发邮件到chendengyunmail@126.com